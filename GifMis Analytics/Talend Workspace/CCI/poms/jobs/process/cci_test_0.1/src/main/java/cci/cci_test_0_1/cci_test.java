package cci.cci_test_0_1;

import routines.Numeric;
import routines.DataOperation;
import routines.TalendDataGenerator;
import routines.TalendStringUtil;
import routines.TalendString;
import routines.StringHandling;
import routines.Relational;
import routines.TalendDate;
import routines.Mathematical;
import routines.SQLike;
import routines.system.*;
import routines.system.api.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.math.BigDecimal;
import java.io.ByteArrayOutputStream;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.io.IOException;
import java.util.Comparator;

@SuppressWarnings("unused")
/**
 * Job: cci_test Purpose: <br>
 * Description:  <br>
 * @author isa.saglam@oredata.com
 * @version 7.0.1.20180411_1414
 * @status 
 */
public class cci_test implements TalendJob {
	static {
		System.setProperty("TalendJob.log", "cci_test.log");
	}
	private static org.apache.log4j.Logger log = org.apache.log4j.Logger
			.getLogger(cci_test.class);

	protected static void logIgnoredError(String message, Throwable cause) {
		log.error(message, cause);

	}

	public final Object obj = new Object();

	// for transmiting parameters purpose
	private Object valueObject = null;

	public Object getValueObject() {
		return this.valueObject;
	}

	public void setValueObject(Object valueObject) {
		this.valueObject = valueObject;
	}

	private final static String defaultCharset = java.nio.charset.Charset
			.defaultCharset().name();

	private final static String utf8Charset = "UTF-8";

	// contains type for every context property
	public class PropertiesWithType extends java.util.Properties {
		private static final long serialVersionUID = 1L;
		private java.util.Map<String, String> propertyTypes = new java.util.HashMap<>();

		public PropertiesWithType(java.util.Properties properties) {
			super(properties);
		}

		public PropertiesWithType() {
			super();
		}

		public void setContextType(String key, String type) {
			propertyTypes.put(key, type);
		}

		public String getContextType(String key) {
			return propertyTypes.get(key);
		}
	}

	// create and load default properties
	private java.util.Properties defaultProps = new java.util.Properties();

	// create application properties with default
	public class ContextProperties extends PropertiesWithType {

		private static final long serialVersionUID = 1L;

		public ContextProperties(java.util.Properties properties) {
			super(properties);
		}

		public ContextProperties() {
			super();
		}

		public void synchronizeContext() {

		}

	}

	private ContextProperties context = new ContextProperties();

	public ContextProperties getContext() {
		return this.context;
	}

	private final String jobVersion = "0.1";
	private final String jobName = "cci_test";
	private final String projectName = "CCI";
	public Integer errorCode = null;
	private String currentComponent = "";

	private final java.util.Map<String, Object> globalMap = new java.util.HashMap<String, Object>();
	private final static java.util.Map<String, Object> junitGlobalMap = new java.util.HashMap<String, Object>();

	private final java.util.Map<String, Long> start_Hash = new java.util.HashMap<String, Long>();
	private final java.util.Map<String, Long> end_Hash = new java.util.HashMap<String, Long>();
	private final java.util.Map<String, Boolean> ok_Hash = new java.util.HashMap<String, Boolean>();
	public final java.util.List<String[]> globalBuffer = new java.util.ArrayList<String[]>();

	private RunStat runStat = new RunStat();
	private RunTrace runTrace = new RunTrace();

	// OSGi DataSource
	private final static String KEY_DB_DATASOURCES = "KEY_DB_DATASOURCES";

	private final static String KEY_DB_DATASOURCES_RAW = "KEY_DB_DATASOURCES_RAW";

	public void setDataSources(
			java.util.Map<String, javax.sql.DataSource> dataSources) {
		java.util.Map<String, routines.system.TalendDataSource> talendDataSources = new java.util.HashMap<String, routines.system.TalendDataSource>();
		for (java.util.Map.Entry<String, javax.sql.DataSource> dataSourceEntry : dataSources
				.entrySet()) {
			talendDataSources.put(
					dataSourceEntry.getKey(),
					new routines.system.TalendDataSource(dataSourceEntry
							.getValue()));
		}
		globalMap.put(KEY_DB_DATASOURCES, talendDataSources);
		globalMap
				.put(KEY_DB_DATASOURCES_RAW,
						new java.util.HashMap<String, javax.sql.DataSource>(
								dataSources));
	}

	private final java.io.ByteArrayOutputStream baos = new java.io.ByteArrayOutputStream();
	private final java.io.PrintStream errorMessagePS = new java.io.PrintStream(
			new java.io.BufferedOutputStream(baos));

	public String getExceptionStackTrace() {
		if ("failure".equals(this.getStatus())) {
			errorMessagePS.flush();
			return baos.toString();
		}
		return null;
	}

	private Exception exception;

	public Exception getException() {
		if ("failure".equals(this.getStatus())) {
			return this.exception;
		}
		return null;
	}

	private class TalendException extends Exception {

		private static final long serialVersionUID = 1L;

		private java.util.Map<String, Object> globalMap = null;
		private Exception e = null;
		private String currentComponent = null;
		private String virtualComponentName = null;

		public void setVirtualComponentName(String virtualComponentName) {
			this.virtualComponentName = virtualComponentName;
		}

		private TalendException(Exception e, String errorComponent,
				final java.util.Map<String, Object> globalMap) {
			this.currentComponent = errorComponent;
			this.globalMap = globalMap;
			this.e = e;
		}

		public Exception getException() {
			return this.e;
		}

		public String getCurrentComponent() {
			return this.currentComponent;
		}

		public String getExceptionCauseMessage(Exception e) {
			Throwable cause = e;
			String message = null;
			int i = 10;
			while (null != cause && 0 < i--) {
				message = cause.getMessage();
				if (null == message) {
					cause = cause.getCause();
				} else {
					break;
				}
			}
			if (null == message) {
				message = e.getClass().getName();
			}
			return message;
		}

		@Override
		public void printStackTrace() {
			if (!(e instanceof TalendException || e instanceof TDieException)) {
				if (virtualComponentName != null
						&& currentComponent.indexOf(virtualComponentName + "_") == 0) {
					globalMap.put(virtualComponentName + "_ERROR_MESSAGE",
							getExceptionCauseMessage(e));
				}
				globalMap.put(currentComponent + "_ERROR_MESSAGE",
						getExceptionCauseMessage(e));
				System.err.println("Exception in component " + currentComponent
						+ " (" + jobName + ")");
			}
			if (!(e instanceof TDieException)) {
				if (e instanceof TalendException) {
					e.printStackTrace();
				} else {
					e.printStackTrace();
					e.printStackTrace(errorMessagePS);
					cci_test.this.exception = e;
				}
			}
			if (!(e instanceof TalendException)) {
				try {
					for (java.lang.reflect.Method m : this.getClass()
							.getEnclosingClass().getMethods()) {
						if (m.getName().compareTo(currentComponent + "_error") == 0) {
							m.invoke(cci_test.this, new Object[] { e,
									currentComponent, globalMap });
							break;
						}
					}

					if (!(e instanceof TDieException)) {
					}
				} catch (Exception e) {
					this.e.printStackTrace();
				}
			}
		}
	}

	public void tDBConnection_1_error(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		tDBConnection_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tDBInput_2_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tDBInput_2_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tMap_1_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tDBInput_2_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tDBOutput_3_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tDBInput_2_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tDBRollback_1_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tDBRollback_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tDBConnection_1_onSubJobError(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread
				.currentThread().getId() + "", "ERROR", "",
				exception.getMessage(),
				ResumeUtil.getExceptionStackTrace(exception), "");

		try {

			if (this.execStat) {
				runStat.updateStatOnConnection("OnSubjobError2", 0, "error");
			}

			errorCode = null;
			tDBRollback_1Process(globalMap);
			if (!"failure".equals(status)) {
				status = "end";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void tDBInput_2_onSubJobError(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread
				.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(),
				ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public void tDBRollback_1_onSubJobError(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread
				.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(),
				ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public void tDBConnection_1Process(
			final java.util.Map<String, Object> globalMap)
			throws TalendException {
		globalMap.put("tDBConnection_1_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception()
						.getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				/**
				 * [tDBConnection_1 begin ] start
				 */

				globalMap.put("ENABLE_TRACES_CONNECTION_tDBConnection_1",
						Boolean.FALSE);

				ok_Hash.put("tDBConnection_1", false);
				start_Hash.put("tDBConnection_1", System.currentTimeMillis());

				currentComponent = "tDBConnection_1";

				int tos_count_tDBConnection_1 = 0;

				class BytesLimit65535_tDBConnection_1 {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_tDBConnection_1().limitLog4jByte();

				org.talend.components.api.component.ComponentDefinition def_tDBConnection_1 = new org.talend.components.jdbc.tjdbcconnection.TJDBCConnectionDefinition();

				org.talend.components.jdbc.tjdbcconnection.TJDBCConnectionProperties props_tDBConnection_1 = (org.talend.components.jdbc.tjdbcconnection.TJDBCConnectionProperties) def_tDBConnection_1
						.createRuntimeProperties();
				props_tDBConnection_1.setValue("shareConnection", false);

				props_tDBConnection_1.setValue("useDataSource", false);

				props_tDBConnection_1.setValue("useAutoCommit", false);

				props_tDBConnection_1.connection
						.setValue(
								"jdbcUrl",
								"jdbc:bigquery://https://www.googleapis.com/bigquery/v2:443;ProjectId=coca-cola-data-lake;OAuthType=0;OAuthServiceAcctEmail=ccdl-storage-and-bq@coca-cola-data-lake.iam.gserviceaccount.com;OAuthPvtKeyPath=/Users/isa/Downloads/cci-bq-jdbc.json");

				props_tDBConnection_1.connection.setValue("driverClass",
						"com.simba.googlebigquery.jdbc41.Driver");

				java.util.List<Object> tDBConnection_1_connection_driverTable_drivers = new java.util.ArrayList<Object>();

				tDBConnection_1_connection_driverTable_drivers
						.add("mvn:org.talend.libraries/google-api-client-1.24.1/6.0.0-SNAPSHOT/jar");

				tDBConnection_1_connection_driverTable_drivers
						.add("mvn:org.talend.libraries/google-api-services-bigquery-v2-rev400-1.24.1/6.0.0-SNAPSHOT/jar");

				tDBConnection_1_connection_driverTable_drivers
						.add("mvn:org.talend.libraries/google-http-client-1.24.1/6.0.0-SNAPSHOT/jar");

				tDBConnection_1_connection_driverTable_drivers
						.add("mvn:org.talend.libraries/google-http-client-jackson2-1.24.1/6.0.0-SNAPSHOT/jar");

				tDBConnection_1_connection_driverTable_drivers
						.add("mvn:org.talend.libraries/google-oauth-client-1.24.1/6.0.0-SNAPSHOT/jar");

				tDBConnection_1_connection_driverTable_drivers
						.add("mvn:org.talend.libraries/jackson-core-2.1.3/6.0.0-SNAPSHOT/jar");

				tDBConnection_1_connection_driverTable_drivers
						.add("mvn:org.talend.libraries/GoogleBigQueryJDBC41/6.0.0-SNAPSHOT/jar");

				((org.talend.daikon.properties.Properties) props_tDBConnection_1.connection.driverTable)
						.setValue("drivers",
								tDBConnection_1_connection_driverTable_drivers);

				props_tDBConnection_1.connection.userPassword.setValue(
						"useAuth", false);

				props_tDBConnection_1.connection.userPassword.setValue(
						"userId", "");

				globalMap.put("tDBConnection_1_COMPONENT_RUNTIME_PROPERTIES",
						props_tDBConnection_1);

				java.net.URL mappings_url_tDBConnection_1 = this.getClass()
						.getResource("/xmlMappings");
				globalMap.put("tDBConnection_1_MAPPINGS_URL",
						mappings_url_tDBConnection_1);

				org.talend.components.api.container.RuntimeContainer container_tDBConnection_1 = new org.talend.components.api.container.RuntimeContainer() {
					public Object getComponentData(String componentId,
							String key) {
						return globalMap.get(componentId + "_" + key);
					}

					public void setComponentData(String componentId,
							String key, Object data) {
						globalMap.put(componentId + "_" + key, data);
					}

					public String getCurrentComponentId() {
						return "tDBConnection_1";
					}

					public Object getGlobalData(String key) {
						return globalMap.get(key);
					}
				};

				int nb_line_tDBConnection_1 = 0;

				org.talend.components.api.component.ConnectorTopology topology_tDBConnection_1 = null;
				topology_tDBConnection_1 = org.talend.components.api.component.ConnectorTopology.NONE;

				org.talend.daikon.runtime.RuntimeInfo runtime_info_tDBConnection_1 = def_tDBConnection_1
						.getRuntimeInfo(
								org.talend.components.api.component.runtime.ExecutionEngine.DI,
								props_tDBConnection_1, topology_tDBConnection_1);
				java.util.Set<org.talend.components.api.component.ConnectorTopology> supported_connector_topologies_tDBConnection_1 = def_tDBConnection_1
						.getSupportedConnectorTopologies();

				org.talend.components.api.component.runtime.RuntimableRuntime componentRuntime_tDBConnection_1 = (org.talend.components.api.component.runtime.RuntimableRuntime) (Class
						.forName(runtime_info_tDBConnection_1
								.getRuntimeClassName()).newInstance());
				org.talend.daikon.properties.ValidationResult initVr_tDBConnection_1 = componentRuntime_tDBConnection_1
						.initialize(container_tDBConnection_1,
								props_tDBConnection_1);

				if (initVr_tDBConnection_1.getStatus() == org.talend.daikon.properties.ValidationResult.Result.ERROR) {
					throw new RuntimeException(
							initVr_tDBConnection_1.getMessage());
				}

				if (componentRuntime_tDBConnection_1 instanceof org.talend.components.api.component.runtime.ComponentDriverInitialization) {
					org.talend.components.api.component.runtime.ComponentDriverInitialization compDriverInitialization_tDBConnection_1 = (org.talend.components.api.component.runtime.ComponentDriverInitialization) componentRuntime_tDBConnection_1;
					compDriverInitialization_tDBConnection_1
							.runAtDriver(container_tDBConnection_1);
				}

				org.talend.components.api.component.runtime.SourceOrSink sourceOrSink_tDBConnection_1 = null;
				if (componentRuntime_tDBConnection_1 instanceof org.talend.components.api.component.runtime.SourceOrSink) {
					sourceOrSink_tDBConnection_1 = (org.talend.components.api.component.runtime.SourceOrSink) componentRuntime_tDBConnection_1;
					org.talend.daikon.properties.ValidationResult vr_tDBConnection_1 = sourceOrSink_tDBConnection_1
							.validate(container_tDBConnection_1);
					if (vr_tDBConnection_1.getStatus() == org.talend.daikon.properties.ValidationResult.Result.ERROR) {
						throw new RuntimeException(
								vr_tDBConnection_1.getMessage());
					}
				}

				/**
				 * [tDBConnection_1 begin ] stop
				 */

				/**
				 * [tDBConnection_1 main ] start
				 */

				currentComponent = "tDBConnection_1";

				tos_count_tDBConnection_1++;

				/**
				 * [tDBConnection_1 main ] stop
				 */

				/**
				 * [tDBConnection_1 process_data_begin ] start
				 */

				currentComponent = "tDBConnection_1";

				/**
				 * [tDBConnection_1 process_data_begin ] stop
				 */

				/**
				 * [tDBConnection_1 process_data_end ] start
				 */

				currentComponent = "tDBConnection_1";

				/**
				 * [tDBConnection_1 process_data_end ] stop
				 */

				if (!isChildJob
						&& (Boolean) globalMap
								.get("ENABLE_TRACES_CONNECTION_tDBConnection_1")) {
					if (globalMap.get("USE_CONDITION") != null
							&& (Boolean) globalMap.get("USE_CONDITION")) {
						if (globalMap.get("TRACE_CONDITION") != null
								&& (Boolean) globalMap.get("TRACE_CONDITION")) {
							// if next breakpoint has been clicked on UI or if
							// start job, should wait action of user.
							if (runTrace.isNextBreakpoint()) {
								runTrace.waitForUserAction();
							} else if (runTrace.isNextRow()) {
								runTrace.waitForUserAction();
							}
						} else {
							// if next row has been clicked on UI or if start
							// job, should wait action of user.
							if (runTrace.isNextRow()) {
								runTrace.waitForUserAction();
							}
						}
					} else { // no condition set
						if (runTrace.isNextRow()) {
							runTrace.waitForUserAction();
						} else {
							Thread.sleep(1000);
						}
					}

				}
				globalMap.put("USE_CONDITION", Boolean.FALSE);

				/**
				 * [tDBConnection_1 end ] start
				 */

				currentComponent = "tDBConnection_1";

				// end of generic

				ok_Hash.put("tDBConnection_1", true);
				end_Hash.put("tDBConnection_1", System.currentTimeMillis());

				/**
				 * [tDBConnection_1 end ] stop
				 */
			}// end the resume

			if (resumeEntryMethodName == null || globalResumeTicket) {
				resumeUtil
						.addLog("CHECKPOINT",
								"CONNECTION:SUBJOB_OK:tDBConnection_1:OnSubjobOk",
								"", Thread.currentThread().getId() + "", "",
								"", "", "", "");
			}

			if (execStat) {
				runStat.updateStatOnConnection("OnSubjobOk2", 0, "ok");
			}

			tDBInput_2Process(globalMap);

		} catch (java.lang.Exception e) {

			if (!(e instanceof TalendException)) {
				log.fatal(currentComponent + " " + e.getMessage(), e);
			}

			TalendException te = new TalendException(e, currentComponent,
					globalMap);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			try {

				/**
				 * [tDBConnection_1 finally ] start
				 */

				currentComponent = "tDBConnection_1";

				// finally of generic

				/**
				 * [tDBConnection_1 finally ] stop
				 */
			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("tDBConnection_1_SUBPROCESS_STATE", 1);
	}

	public static class out1Struct implements
			routines.system.IPersistableRow<out1Struct> {
		final static byte[] commonByteArrayLock_CCI_cci_test = new byte[0];
		static byte[] commonByteArray_CCI_cci_test = new byte[0];

		public String EquipmentTypeCode;

		public String getEquipmentTypeCode() {
			return this.EquipmentTypeCode;
		}

		public String LanguageCode;

		public String getLanguageCode() {
			return this.LanguageCode;
		}

		public String EquipmentTypeText;

		public String getEquipmentTypeText() {
			return this.EquipmentTypeText;
		}

		private String readString(ObjectInputStream dis) throws IOException {
			String strReturn = null;
			int length = 0;
			length = dis.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				if (length > commonByteArray_CCI_cci_test.length) {
					if (length < 1024
							&& commonByteArray_CCI_cci_test.length == 0) {
						commonByteArray_CCI_cci_test = new byte[1024];
					} else {
						commonByteArray_CCI_cci_test = new byte[2 * length];
					}
				}
				dis.readFully(commonByteArray_CCI_cci_test, 0, length);
				strReturn = new String(commonByteArray_CCI_cci_test, 0, length,
						utf8Charset);
			}
			return strReturn;
		}

		private void writeString(String str, ObjectOutputStream dos)
				throws IOException {
			if (str == null) {
				dos.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				dos.writeInt(byteArray.length);
				dos.write(byteArray);
			}
		}

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_CCI_cci_test) {

				try {

					int length = 0;

					this.EquipmentTypeCode = readString(dis);

					this.LanguageCode = readString(dis);

					this.EquipmentTypeText = readString(dis);

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

				// String

				writeString(this.EquipmentTypeCode, dos);

				// String

				writeString(this.LanguageCode, dos);

				// String

				writeString(this.EquipmentTypeText, dos);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("EquipmentTypeCode=" + EquipmentTypeCode);
			sb.append(",LanguageCode=" + LanguageCode);
			sb.append(",EquipmentTypeText=" + EquipmentTypeText);
			sb.append("]");

			return sb.toString();
		}

		public String toLogString() {
			StringBuilder sb = new StringBuilder();

			if (EquipmentTypeCode == null) {
				sb.append("<null>");
			} else {
				sb.append(EquipmentTypeCode);
			}

			sb.append("|");

			if (LanguageCode == null) {
				sb.append("<null>");
			} else {
				sb.append(LanguageCode);
			}

			sb.append("|");

			if (EquipmentTypeText == null) {
				sb.append("<null>");
			} else {
				sb.append(EquipmentTypeText);
			}

			sb.append("|");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(out1Struct other) {

			int returnValue = -1;

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(),
						object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public static class row1Struct implements
			routines.system.IPersistableRow<row1Struct> {
		final static byte[] commonByteArrayLock_CCI_cci_test = new byte[0];
		static byte[] commonByteArray_CCI_cci_test = new byte[0];

		public String EquipmentTypeCode;

		public String getEquipmentTypeCode() {
			return this.EquipmentTypeCode;
		}

		public String LanguageCode;

		public String getLanguageCode() {
			return this.LanguageCode;
		}

		public String EquipmentTypeName;

		public String getEquipmentTypeName() {
			return this.EquipmentTypeName;
		}

		private String readString(ObjectInputStream dis) throws IOException {
			String strReturn = null;
			int length = 0;
			length = dis.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				if (length > commonByteArray_CCI_cci_test.length) {
					if (length < 1024
							&& commonByteArray_CCI_cci_test.length == 0) {
						commonByteArray_CCI_cci_test = new byte[1024];
					} else {
						commonByteArray_CCI_cci_test = new byte[2 * length];
					}
				}
				dis.readFully(commonByteArray_CCI_cci_test, 0, length);
				strReturn = new String(commonByteArray_CCI_cci_test, 0, length,
						utf8Charset);
			}
			return strReturn;
		}

		private void writeString(String str, ObjectOutputStream dos)
				throws IOException {
			if (str == null) {
				dos.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				dos.writeInt(byteArray.length);
				dos.write(byteArray);
			}
		}

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_CCI_cci_test) {

				try {

					int length = 0;

					this.EquipmentTypeCode = readString(dis);

					this.LanguageCode = readString(dis);

					this.EquipmentTypeName = readString(dis);

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

				// String

				writeString(this.EquipmentTypeCode, dos);

				// String

				writeString(this.LanguageCode, dos);

				// String

				writeString(this.EquipmentTypeName, dos);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("EquipmentTypeCode=" + EquipmentTypeCode);
			sb.append(",LanguageCode=" + LanguageCode);
			sb.append(",EquipmentTypeName=" + EquipmentTypeName);
			sb.append("]");

			return sb.toString();
		}

		public String toLogString() {
			StringBuilder sb = new StringBuilder();

			if (EquipmentTypeCode == null) {
				sb.append("<null>");
			} else {
				sb.append(EquipmentTypeCode);
			}

			sb.append("|");

			if (LanguageCode == null) {
				sb.append("<null>");
			} else {
				sb.append(LanguageCode);
			}

			sb.append("|");

			if (EquipmentTypeName == null) {
				sb.append("<null>");
			} else {
				sb.append(EquipmentTypeName);
			}

			sb.append("|");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(row1Struct other) {

			int returnValue = -1;

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(),
						object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public void tDBInput_2Process(final java.util.Map<String, Object> globalMap)
			throws TalendException {
		globalMap.put("tDBInput_2_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception()
						.getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				row1Struct row1 = new row1Struct();
				out1Struct out1 = new out1Struct();

				/**
				 * [tDBOutput_3 begin ] start
				 */

				globalMap.put("ENABLE_TRACES_CONNECTION_tDBInput_2",
						Boolean.FALSE);

				ok_Hash.put("tDBOutput_3", false);
				start_Hash.put("tDBOutput_3", System.currentTimeMillis());

				currentComponent = "tDBOutput_3";

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null) {

						runStat.updateStatOnConnection("out1" + iterateId, 0, 0);

					}
				}

				int tos_count_tDBOutput_3 = 0;

				class BytesLimit65535_tDBOutput_3 {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_tDBOutput_3().limitLog4jByte();

				org.talend.components.api.component.ComponentDefinition def_tDBOutput_3 = new org.talend.components.jdbc.tjdbcoutput.TJDBCOutputDefinition();

				org.talend.components.jdbc.tjdbcoutput.TJDBCOutputProperties props_tDBOutput_3 = (org.talend.components.jdbc.tjdbcoutput.TJDBCOutputProperties) def_tDBOutput_3
						.createRuntimeProperties();
				props_tDBOutput_3
						.setValue(
								"dataAction",
								org.talend.components.jdbc.tjdbcoutput.TJDBCOutputProperties.DataAction.INSERT_OR_UPDATE);

				props_tDBOutput_3.setValue("clearDataInTable", false);

				props_tDBOutput_3.setValue("dieOnError", false);

				props_tDBOutput_3.setValue("enableFieldOptions", false);

				props_tDBOutput_3.setValue("debug", true);

				props_tDBOutput_3.referencedComponent
						.setValue(
								"referenceType",
								org.talend.components.api.properties.ComponentReferenceProperties.ReferenceType.COMPONENT_INSTANCE);

				props_tDBOutput_3.referencedComponent.setValue(
						"componentInstanceId", "tDBConnection_1");

				props_tDBOutput_3.referencedComponent.setValue(
						"referenceDefinitionName", "tJDBCConnection");

				java.util.List<Object> tDBOutput_3_connection_driverTable_drivers = new java.util.ArrayList<Object>();

				tDBOutput_3_connection_driverTable_drivers
						.add("mvn:org.talend.libraries/google-api-client-1.24.1/6.0.0-SNAPSHOT/jar");

				tDBOutput_3_connection_driverTable_drivers
						.add("mvn:org.talend.libraries/google-api-services-bigquery-v2-rev400-1.24.1/6.0.0-SNAPSHOT/jar");

				tDBOutput_3_connection_driverTable_drivers
						.add("mvn:org.talend.libraries/google-http-client-1.24.1/6.0.0-SNAPSHOT/jar");

				tDBOutput_3_connection_driverTable_drivers
						.add("mvn:org.talend.libraries/google-http-client-jackson2-1.24.1/6.0.0-SNAPSHOT/jar");

				tDBOutput_3_connection_driverTable_drivers
						.add("mvn:org.talend.libraries/google-oauth-client-1.24.1/6.0.0-SNAPSHOT/jar");

				tDBOutput_3_connection_driverTable_drivers
						.add("mvn:org.talend.libraries/jackson-core-2.1.3/6.0.0-SNAPSHOT/jar");

				tDBOutput_3_connection_driverTable_drivers
						.add("mvn:org.talend.libraries/GoogleBigQueryJDBC41/6.0.0-SNAPSHOT/jar");

				((org.talend.daikon.properties.Properties) props_tDBOutput_3.connection.driverTable)
						.setValue("drivers",
								tDBOutput_3_connection_driverTable_drivers);

				props_tDBOutput_3.connection.userPassword.setValue("useAuth",
						false);

				props_tDBOutput_3.tableSelection.setValue("tablename",
						"EDW.Dim_EquipmentTypeText");

				props_tDBOutput_3.main
						.setValue(
								"schema",
								new org.apache.avro.Schema.Parser()
										.parse("{\"type\":\"record\",\"name\":\"Dim_EquipmentTypeText\",\"fields\":[{\"name\":\"EquipmentTypeCode\",\"type\":\"string\",\"talend.field.length\":\"65535\",\"talend.field.dbType\":\"STRING\",\"talend.field.dbColumnName\":\"EquipmentTypeCode\"},{\"name\":\"LanguageCode\",\"type\":\"string\",\"talend.field.length\":\"65535\",\"talend.field.dbType\":\"STRING\",\"talend.field.dbColumnName\":\"LanguageCode\"},{\"name\":\"EquipmentTypeText\",\"type\":\"string\",\"talend.field.length\":\"65535\",\"talend.field.dbType\":\"STRING\",\"talend.field.dbColumnName\":\"EquipmentTypeText\"}]}"));

				props_tDBOutput_3.schemaFlow
						.setValue(
								"schema",
								new org.apache.avro.Schema.Parser()
										.parse("{\"type\":\"record\",\"name\":\"Dim_EquipmentTypeText\",\"fields\":[{\"name\":\"EquipmentTypeCode\",\"type\":\"string\",\"talend.field.length\":\"65535\",\"talend.field.dbType\":\"STRING\",\"talend.field.dbColumnName\":\"EquipmentTypeCode\"},{\"name\":\"LanguageCode\",\"type\":\"string\",\"talend.field.length\":\"65535\",\"talend.field.dbType\":\"STRING\",\"talend.field.dbColumnName\":\"LanguageCode\"},{\"name\":\"EquipmentTypeText\",\"type\":\"string\",\"talend.field.length\":\"65535\",\"talend.field.dbType\":\"STRING\",\"talend.field.dbColumnName\":\"EquipmentTypeText\"}]}"));

				props_tDBOutput_3.schemaReject
						.setValue(
								"schema",
								new org.apache.avro.Schema.Parser()
										.parse("{\"type\":\"record\",\"name\":\"rejectOutput\",\"fields\":[{\"name\":\"EquipmentTypeCode\",\"type\":\"string\",\"talend.field.length\":\"65535\",\"talend.field.dbType\":\"STRING\",\"talend.field.dbColumnName\":\"EquipmentTypeCode\"},{\"name\":\"LanguageCode\",\"type\":\"string\",\"talend.field.length\":\"65535\",\"talend.field.dbType\":\"STRING\",\"talend.field.dbColumnName\":\"LanguageCode\"},{\"name\":\"EquipmentTypeText\",\"type\":\"string\",\"talend.field.length\":\"65535\",\"talend.field.dbType\":\"STRING\",\"talend.field.dbColumnName\":\"EquipmentTypeText\"},{\"name\":\"errorCode\",\"type\":\"string\",\"talend.isLocked\":\"false\",\"talend.field.generated\":\"true\",\"talend.field.length\":\"255\"},{\"name\":\"errorMessage\",\"type\":\"string\",\"talend.isLocked\":\"false\",\"talend.field.generated\":\"true\",\"talend.field.length\":\"255\"}]}"));

				props_tDBOutput_3.additionalColumns.setValue("positions",
						"BEFORE");

				props_tDBOutput_3.additionalColumns.setValue(
						"referenceColumns", "EquipmentTypeCode");

				java.util.List<Object> tDBOutput_3_fieldOptions_schemaColumns = new java.util.ArrayList<Object>();

				tDBOutput_3_fieldOptions_schemaColumns.add("EquipmentTypeCode");

				tDBOutput_3_fieldOptions_schemaColumns.add("LanguageCode");

				tDBOutput_3_fieldOptions_schemaColumns.add("EquipmentTypeText");

				((org.talend.daikon.properties.Properties) props_tDBOutput_3.fieldOptions)
						.setValue("schemaColumns",
								tDBOutput_3_fieldOptions_schemaColumns);

				java.util.List<Object> tDBOutput_3_fieldOptions_updateKey = new java.util.ArrayList<Object>();

				tDBOutput_3_fieldOptions_updateKey.add(false);

				tDBOutput_3_fieldOptions_updateKey.add(false);

				tDBOutput_3_fieldOptions_updateKey.add(false);

				((org.talend.daikon.properties.Properties) props_tDBOutput_3.fieldOptions)
						.setValue("updateKey",
								tDBOutput_3_fieldOptions_updateKey);

				java.util.List<Object> tDBOutput_3_fieldOptions_deletionKey = new java.util.ArrayList<Object>();

				tDBOutput_3_fieldOptions_deletionKey.add(false);

				tDBOutput_3_fieldOptions_deletionKey.add(false);

				tDBOutput_3_fieldOptions_deletionKey.add(false);

				((org.talend.daikon.properties.Properties) props_tDBOutput_3.fieldOptions)
						.setValue("deletionKey",
								tDBOutput_3_fieldOptions_deletionKey);

				java.util.List<Object> tDBOutput_3_fieldOptions_updatable = new java.util.ArrayList<Object>();

				tDBOutput_3_fieldOptions_updatable.add(true);

				tDBOutput_3_fieldOptions_updatable.add(true);

				tDBOutput_3_fieldOptions_updatable.add(true);

				((org.talend.daikon.properties.Properties) props_tDBOutput_3.fieldOptions)
						.setValue("updatable",
								tDBOutput_3_fieldOptions_updatable);

				java.util.List<Object> tDBOutput_3_fieldOptions_insertable = new java.util.ArrayList<Object>();

				tDBOutput_3_fieldOptions_insertable.add(true);

				tDBOutput_3_fieldOptions_insertable.add(true);

				tDBOutput_3_fieldOptions_insertable.add(true);

				((org.talend.daikon.properties.Properties) props_tDBOutput_3.fieldOptions)
						.setValue("insertable",
								tDBOutput_3_fieldOptions_insertable);

				if (org.talend.components.api.properties.ComponentReferenceProperties.ReferenceType.COMPONENT_INSTANCE == props_tDBOutput_3.referencedComponent.referenceType
						.getValue()) {
					final String referencedComponentInstanceId_tDBOutput_3 = props_tDBOutput_3.referencedComponent.componentInstanceId
							.getStringValue();
					if (referencedComponentInstanceId_tDBOutput_3 != null) {
						org.talend.daikon.properties.Properties referencedComponentProperties_tDBOutput_3 = (org.talend.daikon.properties.Properties) globalMap
								.get(referencedComponentInstanceId_tDBOutput_3
										+ "_COMPONENT_RUNTIME_PROPERTIES");
						props_tDBOutput_3.referencedComponent
								.setReference(referencedComponentProperties_tDBOutput_3);
					}
				}
				globalMap.put("tDBOutput_3_COMPONENT_RUNTIME_PROPERTIES",
						props_tDBOutput_3);

				java.net.URL mappings_url_tDBOutput_3 = this.getClass()
						.getResource("/xmlMappings");
				globalMap.put("tDBOutput_3_MAPPINGS_URL",
						mappings_url_tDBOutput_3);

				org.talend.components.api.container.RuntimeContainer container_tDBOutput_3 = new org.talend.components.api.container.RuntimeContainer() {
					public Object getComponentData(String componentId,
							String key) {
						return globalMap.get(componentId + "_" + key);
					}

					public void setComponentData(String componentId,
							String key, Object data) {
						globalMap.put(componentId + "_" + key, data);
					}

					public String getCurrentComponentId() {
						return "tDBOutput_3";
					}

					public Object getGlobalData(String key) {
						return globalMap.get(key);
					}
				};

				int nb_line_tDBOutput_3 = 0;

				org.talend.components.api.component.ConnectorTopology topology_tDBOutput_3 = null;
				topology_tDBOutput_3 = org.talend.components.api.component.ConnectorTopology.INCOMING;

				org.talend.daikon.runtime.RuntimeInfo runtime_info_tDBOutput_3 = def_tDBOutput_3
						.getRuntimeInfo(
								org.talend.components.api.component.runtime.ExecutionEngine.DI,
								props_tDBOutput_3, topology_tDBOutput_3);
				java.util.Set<org.talend.components.api.component.ConnectorTopology> supported_connector_topologies_tDBOutput_3 = def_tDBOutput_3
						.getSupportedConnectorTopologies();

				org.talend.components.api.component.runtime.RuntimableRuntime componentRuntime_tDBOutput_3 = (org.talend.components.api.component.runtime.RuntimableRuntime) (Class
						.forName(runtime_info_tDBOutput_3.getRuntimeClassName())
						.newInstance());
				org.talend.daikon.properties.ValidationResult initVr_tDBOutput_3 = componentRuntime_tDBOutput_3
						.initialize(container_tDBOutput_3, props_tDBOutput_3);

				if (initVr_tDBOutput_3.getStatus() == org.talend.daikon.properties.ValidationResult.Result.ERROR) {
					throw new RuntimeException(initVr_tDBOutput_3.getMessage());
				}

				if (componentRuntime_tDBOutput_3 instanceof org.talend.components.api.component.runtime.ComponentDriverInitialization) {
					org.talend.components.api.component.runtime.ComponentDriverInitialization compDriverInitialization_tDBOutput_3 = (org.talend.components.api.component.runtime.ComponentDriverInitialization) componentRuntime_tDBOutput_3;
					compDriverInitialization_tDBOutput_3
							.runAtDriver(container_tDBOutput_3);
				}

				org.talend.components.api.component.runtime.SourceOrSink sourceOrSink_tDBOutput_3 = null;
				if (componentRuntime_tDBOutput_3 instanceof org.talend.components.api.component.runtime.SourceOrSink) {
					sourceOrSink_tDBOutput_3 = (org.talend.components.api.component.runtime.SourceOrSink) componentRuntime_tDBOutput_3;
					org.talend.daikon.properties.ValidationResult vr_tDBOutput_3 = sourceOrSink_tDBOutput_3
							.validate(container_tDBOutput_3);
					if (vr_tDBOutput_3.getStatus() == org.talend.daikon.properties.ValidationResult.Result.ERROR) {
						throw new RuntimeException(vr_tDBOutput_3.getMessage());
					}
				}

				org.talend.components.api.component.runtime.Sink sink_tDBOutput_3 = (org.talend.components.api.component.runtime.Sink) sourceOrSink_tDBOutput_3;
				org.talend.components.api.component.runtime.WriteOperation writeOperation_tDBOutput_3 = sink_tDBOutput_3
						.createWriteOperation();
				writeOperation_tDBOutput_3.initialize(container_tDBOutput_3);
				org.talend.components.api.component.runtime.Writer writer_tDBOutput_3 = writeOperation_tDBOutput_3
						.createWriter(container_tDBOutput_3);
				writer_tDBOutput_3.open("tDBOutput_3");

				resourceMap.put("writer_tDBOutput_3", writer_tDBOutput_3);

				org.talend.components.api.component.Connector c_tDBOutput_3 = null;
				for (org.talend.components.api.component.Connector currentConnector : props_tDBOutput_3
						.getAvailableConnectors(null, false)) {
					if (currentConnector.getName().equals("MAIN")) {
						c_tDBOutput_3 = currentConnector;
						break;
					}
				}
				org.apache.avro.Schema designSchema_tDBOutput_3 = props_tDBOutput_3
						.getSchema(c_tDBOutput_3, false);
				org.talend.codegen.enforcer.IncomingSchemaEnforcer incomingEnforcer_tDBOutput_3 = new org.talend.codegen.enforcer.IncomingSchemaEnforcer(
						designSchema_tDBOutput_3);
				java.lang.Iterable<?> outgoingMainRecordsList_tDBOutput_3 = new java.util.ArrayList<Object>();
				java.util.Iterator outgoingMainRecordsIt_tDBOutput_3 = null;

				/**
				 * [tDBOutput_3 begin ] stop
				 */

				/**
				 * [tMap_1 begin ] start
				 */

				globalMap.put("ENABLE_TRACES_CONNECTION_tDBInput_2",
						Boolean.FALSE);

				ok_Hash.put("tMap_1", false);
				start_Hash.put("tMap_1", System.currentTimeMillis());

				currentComponent = "tMap_1";

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null) {

						runStat.updateStatOnConnection("row1" + iterateId, 0, 0);

					}
				}

				int tos_count_tMap_1 = 0;

				if (log.isDebugEnabled())
					log.debug("tMap_1 - " + ("Start to work."));
				class BytesLimit65535_tMap_1 {
					public void limitLog4jByte() throws Exception {

						StringBuilder log4jParamters_tMap_1 = new StringBuilder();
						log4jParamters_tMap_1.append("Parameters:");
						log4jParamters_tMap_1.append("LINK_STYLE" + " = "
								+ "AUTO");
						log4jParamters_tMap_1.append(" | ");
						log4jParamters_tMap_1.append("TEMPORARY_DATA_DIRECTORY"
								+ " = " + "");
						log4jParamters_tMap_1.append(" | ");
						log4jParamters_tMap_1.append("ROWS_BUFFER_SIZE" + " = "
								+ "2000000");
						log4jParamters_tMap_1.append(" | ");
						log4jParamters_tMap_1
								.append("CHANGE_HASH_AND_EQUALS_FOR_BIGDECIMAL"
										+ " = " + "true");
						log4jParamters_tMap_1.append(" | ");
						if (log.isDebugEnabled())
							log.debug("tMap_1 - " + (log4jParamters_tMap_1));
					}
				}

				new BytesLimit65535_tMap_1().limitLog4jByte();

				// ###############################
				// # Lookup's keys initialization
				int count_row1_tMap_1 = 0;

				// ###############################

				// ###############################
				// # Vars initialization
				class Var__tMap_1__Struct {
				}
				Var__tMap_1__Struct Var__tMap_1 = new Var__tMap_1__Struct();
				// ###############################

				// ###############################
				// # Outputs initialization
				int count_out1_tMap_1 = 0;

				out1Struct out1_tmp = new out1Struct();
				// ###############################

				/**
				 * [tMap_1 begin ] stop
				 */

				/**
				 * [tDBInput_2 begin ] start
				 */

				globalMap.put("ENABLE_TRACES_CONNECTION_tDBInput_2",
						Boolean.FALSE);

				ok_Hash.put("tDBInput_2", false);
				start_Hash.put("tDBInput_2", System.currentTimeMillis());

				currentComponent = "tDBInput_2";

				int tos_count_tDBInput_2 = 0;

				class BytesLimit65535_tDBInput_2 {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_tDBInput_2().limitLog4jByte();

				org.talend.components.api.component.ComponentDefinition def_tDBInput_2 = new org.talend.components.jdbc.tjdbcinput.TJDBCInputDefinition();

				org.talend.components.jdbc.tjdbcinput.TJDBCInputProperties props_tDBInput_2 = (org.talend.components.jdbc.tjdbcinput.TJDBCInputProperties) def_tDBInput_2
						.createRuntimeProperties();
				props_tDBInput_2.setValue("sql",
						"SELECT * FROM EDW.TREQUIPMENTTYPE");

				props_tDBInput_2.setValue("useCursor", false);

				props_tDBInput_2.setValue("trimStringOrCharColumns", false);

				props_tDBInput_2.setValue("enableDBMapping", false);

				props_tDBInput_2.referencedComponent
						.setValue(
								"referenceType",
								org.talend.components.api.properties.ComponentReferenceProperties.ReferenceType.COMPONENT_INSTANCE);

				props_tDBInput_2.referencedComponent.setValue(
						"componentInstanceId", "tDBConnection_1");

				props_tDBInput_2.referencedComponent.setValue(
						"referenceDefinitionName", "tJDBCConnection");

				java.util.List<Object> tDBInput_2_connection_driverTable_drivers = new java.util.ArrayList<Object>();

				tDBInput_2_connection_driverTable_drivers
						.add("mvn:org.talend.libraries/google-api-client-1.24.1/6.0.0-SNAPSHOT/jar");

				tDBInput_2_connection_driverTable_drivers
						.add("mvn:org.talend.libraries/google-api-services-bigquery-v2-rev400-1.24.1/6.0.0-SNAPSHOT/jar");

				tDBInput_2_connection_driverTable_drivers
						.add("mvn:org.talend.libraries/google-http-client-1.24.1/6.0.0-SNAPSHOT/jar");

				tDBInput_2_connection_driverTable_drivers
						.add("mvn:org.talend.libraries/google-http-client-jackson2-1.24.1/6.0.0-SNAPSHOT/jar");

				tDBInput_2_connection_driverTable_drivers
						.add("mvn:org.talend.libraries/google-oauth-client-1.24.1/6.0.0-SNAPSHOT/jar");

				tDBInput_2_connection_driverTable_drivers
						.add("mvn:org.talend.libraries/jackson-core-2.1.3/6.0.0-SNAPSHOT/jar");

				tDBInput_2_connection_driverTable_drivers
						.add("mvn:org.talend.libraries/GoogleBigQueryJDBC41/6.0.0-SNAPSHOT/jar");

				((org.talend.daikon.properties.Properties) props_tDBInput_2.connection.driverTable)
						.setValue("drivers",
								tDBInput_2_connection_driverTable_drivers);

				props_tDBInput_2.connection.userPassword.setValue("useAuth",
						false);

				props_tDBInput_2.main
						.setValue(
								"schema",
								new org.apache.avro.Schema.Parser()
										.parse("{\"type\":\"record\",\"name\":\"TREQUIPMENTTYPE\",\"fields\":[{\"name\":\"EquipmentTypeCode\",\"type\":\"string\",\"talend.field.dbType\":\"STRING\",\"di.column.talendType\":\"id_String\",\"talend.field.pattern\":\"\",\"di.table.label\":\"EquipmentTypeCode\",\"talend.field.precision\":\"0\",\"talend.field.isKey\":\"true\",\"di.table.comment\":\"\",\"talend.field.dbColumnName\":\"EquipmentTypeCode\",\"di.prop.Comment\":\"\",\"talend.field.length\":\"65535\",\"di.column.relationshipType\":\"\",\"di.column.originalLength\":\"0\",\"di.column.relatedEntity\":\"\"},{\"name\":\"LanguageCode\",\"type\":\"string\",\"di.table.comment\":\"\",\"talend.field.dbType\":\"STRING\",\"talend.field.dbColumnName\":\"LanguageCode\",\"di.prop.Comment\":\"\",\"di.column.talendType\":\"id_String\",\"talend.field.pattern\":\"\",\"talend.field.length\":\"65535\",\"di.column.relationshipType\":\"\",\"di.column.originalLength\":\"0\",\"di.table.label\":\"LanguageCode\",\"talend.field.precision\":\"0\",\"di.column.relatedEntity\":\"\"},{\"name\":\"EquipmentTypeName\",\"type\":\"string\",\"di.table.comment\":\"\",\"talend.field.dbType\":\"STRING\",\"talend.field.dbColumnName\":\"EquipmentTypeName\",\"di.prop.Comment\":\"\",\"di.column.talendType\":\"id_String\",\"talend.field.pattern\":\"\",\"talend.field.length\":\"65535\",\"di.column.relationshipType\":\"\",\"di.column.originalLength\":\"0\",\"di.table.label\":\"EquipmentTypeName\",\"talend.field.precision\":\"0\",\"di.column.relatedEntity\":\"\"}],\"di.table.name\":\"MAIN\",\"di.table.label\":\"TREQUIPMENTTYPE\"}"));

				props_tDBInput_2.tableSelection.setValue("tablename",
						"TREQUIPMENTTYPE");

				java.util.List<Object> tDBInput_2_trimTable_trim = new java.util.ArrayList<Object>();

				tDBInput_2_trimTable_trim.add(false);

				tDBInput_2_trimTable_trim.add(false);

				tDBInput_2_trimTable_trim.add(false);

				((org.talend.daikon.properties.Properties) props_tDBInput_2.trimTable)
						.setValue("trim", tDBInput_2_trimTable_trim);

				java.util.List<Object> tDBInput_2_trimTable_columnName = new java.util.ArrayList<Object>();

				tDBInput_2_trimTable_columnName.add("EquipmentTypeCode");

				tDBInput_2_trimTable_columnName.add("LanguageCode");

				tDBInput_2_trimTable_columnName.add("EquipmentTypeName");

				((org.talend.daikon.properties.Properties) props_tDBInput_2.trimTable)
						.setValue("columnName", tDBInput_2_trimTable_columnName);

				if (org.talend.components.api.properties.ComponentReferenceProperties.ReferenceType.COMPONENT_INSTANCE == props_tDBInput_2.referencedComponent.referenceType
						.getValue()) {
					final String referencedComponentInstanceId_tDBInput_2 = props_tDBInput_2.referencedComponent.componentInstanceId
							.getStringValue();
					if (referencedComponentInstanceId_tDBInput_2 != null) {
						org.talend.daikon.properties.Properties referencedComponentProperties_tDBInput_2 = (org.talend.daikon.properties.Properties) globalMap
								.get(referencedComponentInstanceId_tDBInput_2
										+ "_COMPONENT_RUNTIME_PROPERTIES");
						props_tDBInput_2.referencedComponent
								.setReference(referencedComponentProperties_tDBInput_2);
					}
				}
				globalMap.put("tDBInput_2_COMPONENT_RUNTIME_PROPERTIES",
						props_tDBInput_2);

				java.net.URL mappings_url_tDBInput_2 = this.getClass()
						.getResource("/xmlMappings");
				globalMap.put("tDBInput_2_MAPPINGS_URL",
						mappings_url_tDBInput_2);

				org.talend.components.api.container.RuntimeContainer container_tDBInput_2 = new org.talend.components.api.container.RuntimeContainer() {
					public Object getComponentData(String componentId,
							String key) {
						return globalMap.get(componentId + "_" + key);
					}

					public void setComponentData(String componentId,
							String key, Object data) {
						globalMap.put(componentId + "_" + key, data);
					}

					public String getCurrentComponentId() {
						return "tDBInput_2";
					}

					public Object getGlobalData(String key) {
						return globalMap.get(key);
					}
				};

				int nb_line_tDBInput_2 = 0;

				org.talend.components.api.component.ConnectorTopology topology_tDBInput_2 = null;
				topology_tDBInput_2 = org.talend.components.api.component.ConnectorTopology.OUTGOING;

				org.talend.daikon.runtime.RuntimeInfo runtime_info_tDBInput_2 = def_tDBInput_2
						.getRuntimeInfo(
								org.talend.components.api.component.runtime.ExecutionEngine.DI,
								props_tDBInput_2, topology_tDBInput_2);
				java.util.Set<org.talend.components.api.component.ConnectorTopology> supported_connector_topologies_tDBInput_2 = def_tDBInput_2
						.getSupportedConnectorTopologies();

				org.talend.components.api.component.runtime.RuntimableRuntime componentRuntime_tDBInput_2 = (org.talend.components.api.component.runtime.RuntimableRuntime) (Class
						.forName(runtime_info_tDBInput_2.getRuntimeClassName())
						.newInstance());
				org.talend.daikon.properties.ValidationResult initVr_tDBInput_2 = componentRuntime_tDBInput_2
						.initialize(container_tDBInput_2, props_tDBInput_2);

				if (initVr_tDBInput_2.getStatus() == org.talend.daikon.properties.ValidationResult.Result.ERROR) {
					throw new RuntimeException(initVr_tDBInput_2.getMessage());
				}

				if (componentRuntime_tDBInput_2 instanceof org.talend.components.api.component.runtime.ComponentDriverInitialization) {
					org.talend.components.api.component.runtime.ComponentDriverInitialization compDriverInitialization_tDBInput_2 = (org.talend.components.api.component.runtime.ComponentDriverInitialization) componentRuntime_tDBInput_2;
					compDriverInitialization_tDBInput_2
							.runAtDriver(container_tDBInput_2);
				}

				org.talend.components.api.component.runtime.SourceOrSink sourceOrSink_tDBInput_2 = null;
				if (componentRuntime_tDBInput_2 instanceof org.talend.components.api.component.runtime.SourceOrSink) {
					sourceOrSink_tDBInput_2 = (org.talend.components.api.component.runtime.SourceOrSink) componentRuntime_tDBInput_2;
					org.talend.daikon.properties.ValidationResult vr_tDBInput_2 = sourceOrSink_tDBInput_2
							.validate(container_tDBInput_2);
					if (vr_tDBInput_2.getStatus() == org.talend.daikon.properties.ValidationResult.Result.ERROR) {
						throw new RuntimeException(vr_tDBInput_2.getMessage());
					}
				}

				org.talend.components.api.component.runtime.Source source_tDBInput_2 = (org.talend.components.api.component.runtime.Source) sourceOrSink_tDBInput_2;
				org.talend.components.api.component.runtime.Reader reader_tDBInput_2 = source_tDBInput_2
						.createReader(container_tDBInput_2);
				reader_tDBInput_2 = new org.talend.codegen.flowvariables.runtime.FlowVariablesReader(
						reader_tDBInput_2, container_tDBInput_2);

				boolean multi_output_is_allowed_tDBInput_2 = false;
				org.talend.components.api.component.Connector c_tDBInput_2 = null;
				for (org.talend.components.api.component.Connector currentConnector : props_tDBInput_2
						.getAvailableConnectors(null, true)) {
					if (currentConnector.getName().equals("MAIN")) {
						c_tDBInput_2 = currentConnector;
					}

					if (currentConnector.getName().equals("REJECT")) {// it's
																		// better
																		// to
																		// move
																		// the
																		// code
																		// to
																		// javajet
						multi_output_is_allowed_tDBInput_2 = true;
					}
				}
				org.apache.avro.Schema schema_tDBInput_2 = props_tDBInput_2
						.getSchema(c_tDBInput_2, true);

				org.talend.codegen.enforcer.OutgoingSchemaEnforcer outgoingEnforcer_tDBInput_2 = org.talend.codegen.enforcer.EnforcerCreator
						.createOutgoingEnforcer(schema_tDBInput_2, false);

				// Create a reusable factory that converts the output of the
				// reader to an IndexedRecord.
				org.talend.daikon.avro.converter.IndexedRecordConverter<Object, ? extends org.apache.avro.generic.IndexedRecord> factory_tDBInput_2 = null;

				// Iterate through the incoming data.
				boolean available_tDBInput_2 = reader_tDBInput_2.start();

				resourceMap.put("reader_tDBInput_2", reader_tDBInput_2);

				for (; available_tDBInput_2; available_tDBInput_2 = reader_tDBInput_2
						.advance()) {
					nb_line_tDBInput_2++;

					if (multi_output_is_allowed_tDBInput_2) {
						row1 = null;

					}

					try {
						Object data_tDBInput_2 = reader_tDBInput_2.getCurrent();

						if (multi_output_is_allowed_tDBInput_2) {
							row1 = new row1Struct();
						}

						// Construct the factory once when the first data
						// arrives.
						if (factory_tDBInput_2 == null) {
							factory_tDBInput_2 = (org.talend.daikon.avro.converter.IndexedRecordConverter<Object, ? extends org.apache.avro.generic.IndexedRecord>) new org.talend.daikon.avro.AvroRegistry()
									.createIndexedRecordConverter(data_tDBInput_2
											.getClass());
						}

						// Enforce the outgoing schema on the input.
						outgoingEnforcer_tDBInput_2
								.setWrapped(factory_tDBInput_2
										.convertToAvro(data_tDBInput_2));
						Object columnValue_0_tDBInput_2 = outgoingEnforcer_tDBInput_2
								.get(0);
						row1.EquipmentTypeCode = (String) (columnValue_0_tDBInput_2);
						Object columnValue_1_tDBInput_2 = outgoingEnforcer_tDBInput_2
								.get(1);
						row1.LanguageCode = (String) (columnValue_1_tDBInput_2);
						Object columnValue_2_tDBInput_2 = outgoingEnforcer_tDBInput_2
								.get(2);
						row1.EquipmentTypeName = (String) (columnValue_2_tDBInput_2);
					} catch (org.talend.components.api.exception.DataRejectException e_tDBInput_2) {
						java.util.Map<String, Object> info_tDBInput_2 = e_tDBInput_2
								.getRejectInfo();
						// TODO use a method instead of getting method by the
						// special key "error/errorMessage"
						Object errorMessage_tDBInput_2 = null;
						if (info_tDBInput_2.containsKey("error")) {
							errorMessage_tDBInput_2 = info_tDBInput_2
									.get("error");
						} else if (info_tDBInput_2.containsKey("errorMessage")) {
							errorMessage_tDBInput_2 = info_tDBInput_2
									.get("errorMessage");
						} else {
							errorMessage_tDBInput_2 = "Rejected but error message missing";
						}
						errorMessage_tDBInput_2 = "Row " + nb_line_tDBInput_2
								+ ": " + errorMessage_tDBInput_2;
						System.err.println(errorMessage_tDBInput_2);
						// If the record is reject, the main line record should
						// put NULL
						row1 = null;
					}
					java.lang.Iterable<?> outgoingMainRecordsList_tDBInput_2 = new java.util.ArrayList<Object>();
					java.util.Iterator outgoingMainRecordsIt_tDBInput_2 = null;

					/**
					 * [tDBInput_2 begin ] stop
					 */

					/**
					 * [tDBInput_2 main ] start
					 */

					currentComponent = "tDBInput_2";

					if (row1 != null) {
						globalMap.put("ENABLE_TRACES_CONNECTION_tDBInput_2",
								Boolean.TRUE);
						if (runTrace.isPause()) {
							while (runTrace.isPause()) {
								Thread.sleep(100);
							}
						} else {

							// here we dump the line content for trace purpose
							java.util.LinkedHashMap<String, String> runTraceData = new java.util.LinkedHashMap<String, String>();

							runTraceData.put("EquipmentTypeCode",
									String.valueOf(row1.EquipmentTypeCode));

							runTraceData.put("LanguageCode",
									String.valueOf(row1.LanguageCode));

							runTraceData.put("EquipmentTypeName",
									String.valueOf(row1.EquipmentTypeName));

							runTrace.sendTrace("row1", "tDBInput_2",
									runTraceData);
						}

					}

					tos_count_tDBInput_2++;

					/**
					 * [tDBInput_2 main ] stop
					 */

					/**
					 * [tDBInput_2 process_data_begin ] start
					 */

					currentComponent = "tDBInput_2";

					/**
					 * [tDBInput_2 process_data_begin ] stop
					 */

					/**
					 * [tMap_1 main ] start
					 */

					currentComponent = "tMap_1";

					// row1
					// row1

					if (execStat) {
						runStat.updateStatOnConnection("row1" + iterateId, 1, 1);
					}

					if (log.isTraceEnabled()) {
						log.trace("row1 - "
								+ (row1 == null ? "" : row1.toLogString()));
					}

					boolean hasCasePrimitiveKeyWithNull_tMap_1 = false;

					// ###############################
					// # Input tables (lookups)
					boolean rejectedInnerJoin_tMap_1 = false;
					boolean mainRowRejected_tMap_1 = false;

					// ###############################
					{ // start of Var scope

						// ###############################
						// # Vars tables

						Var__tMap_1__Struct Var = Var__tMap_1;// ###############################
						// ###############################
						// # Output tables

						out1 = null;

						// # Output table : 'out1'
						count_out1_tMap_1++;

						out1_tmp.EquipmentTypeCode = row1.EquipmentTypeCode;
						out1_tmp.LanguageCode = row1.LanguageCode;
						out1_tmp.EquipmentTypeText = row1.EquipmentTypeName;
						out1 = out1_tmp;
						log.debug("tMap_1 - Outputting the record "
								+ count_out1_tMap_1
								+ " of the output table 'out1'.");

						// ###############################

					} // end of Var scope

					rejectedInnerJoin_tMap_1 = false;

					if (out1 != null) {
						globalMap.put("ENABLE_TRACES_CONNECTION_tDBInput_2",
								Boolean.TRUE);
						if (runTrace.isPause()) {
							while (runTrace.isPause()) {
								Thread.sleep(100);
							}
						} else {

							// here we dump the line content for trace purpose
							java.util.LinkedHashMap<String, String> runTraceData = new java.util.LinkedHashMap<String, String>();

							runTraceData.put("EquipmentTypeCode",
									String.valueOf(out1.EquipmentTypeCode));

							runTraceData.put("LanguageCode",
									String.valueOf(out1.LanguageCode));

							runTraceData.put("EquipmentTypeText",
									String.valueOf(out1.EquipmentTypeText));

							runTrace.sendTrace("out1", "tDBInput_2",
									runTraceData);
						}

					}

					tos_count_tMap_1++;

					/**
					 * [tMap_1 main ] stop
					 */

					/**
					 * [tMap_1 process_data_begin ] start
					 */

					currentComponent = "tMap_1";

					/**
					 * [tMap_1 process_data_begin ] stop
					 */
					// Start of branch "out1"
					if (out1 != null) {

						/**
						 * [tDBOutput_3 main ] start
						 */

						currentComponent = "tDBOutput_3";

						// out1
						// out1

						if (execStat) {
							runStat.updateStatOnConnection("out1" + iterateId,
									1, 1);
						}

						if (log.isTraceEnabled()) {
							log.trace("out1 - "
									+ (out1 == null ? "" : out1.toLogString()));
						}

						incomingEnforcer_tDBOutput_3.createNewRecord();
						incomingEnforcer_tDBOutput_3.put("EquipmentTypeCode",
								out1.EquipmentTypeCode);
						incomingEnforcer_tDBOutput_3.put("LanguageCode",
								out1.LanguageCode);
						incomingEnforcer_tDBOutput_3.put("EquipmentTypeText",
								out1.EquipmentTypeText);
						org.apache.avro.generic.IndexedRecord data_tDBOutput_3 = incomingEnforcer_tDBOutput_3
								.getCurrentRecord();

						writer_tDBOutput_3.write(data_tDBOutput_3);

						nb_line_tDBOutput_3++;

						tos_count_tDBOutput_3++;

						/**
						 * [tDBOutput_3 main ] stop
						 */

						/**
						 * [tDBOutput_3 process_data_begin ] start
						 */

						currentComponent = "tDBOutput_3";

						/**
						 * [tDBOutput_3 process_data_begin ] stop
						 */

						/**
						 * [tDBOutput_3 process_data_end ] start
						 */

						currentComponent = "tDBOutput_3";

						/**
						 * [tDBOutput_3 process_data_end ] stop
						 */

					} // End of branch "out1"

					/**
					 * [tMap_1 process_data_end ] start
					 */

					currentComponent = "tMap_1";

					/**
					 * [tMap_1 process_data_end ] stop
					 */

					/**
					 * [tDBInput_2 process_data_end ] start
					 */

					currentComponent = "tDBInput_2";

					/**
					 * [tDBInput_2 process_data_end ] stop
					 */

					if (!isChildJob
							&& (Boolean) globalMap
									.get("ENABLE_TRACES_CONNECTION_tDBInput_2")) {
						if (globalMap.get("USE_CONDITION") != null
								&& (Boolean) globalMap.get("USE_CONDITION")) {
							if (globalMap.get("TRACE_CONDITION") != null
									&& (Boolean) globalMap
											.get("TRACE_CONDITION")) {
								// if next breakpoint has been clicked on UI or
								// if start job, should wait action of user.
								if (runTrace.isNextBreakpoint()) {
									runTrace.waitForUserAction();
								} else if (runTrace.isNextRow()) {
									runTrace.waitForUserAction();
								}
							} else {
								// if next row has been clicked on UI or if
								// start job, should wait action of user.
								if (runTrace.isNextRow()) {
									runTrace.waitForUserAction();
								}
							}
						} else { // no condition set
							if (runTrace.isNextRow()) {
								runTrace.waitForUserAction();
							} else {
								Thread.sleep(1000);
							}
						}

					}
					globalMap.put("USE_CONDITION", Boolean.FALSE);

					/**
					 * [tDBInput_2 end ] start
					 */

					currentComponent = "tDBInput_2";

					// end of generic

					resourceMap.put("finish_tDBInput_2", Boolean.TRUE);

				} // while
				reader_tDBInput_2.close();
				final java.util.Map<String, Object> resultMap_tDBInput_2 = reader_tDBInput_2
						.getReturnValues();
				if (resultMap_tDBInput_2 != null) {
					for (java.util.Map.Entry<String, Object> entry_tDBInput_2 : resultMap_tDBInput_2
							.entrySet()) {
						switch (entry_tDBInput_2.getKey()) {
						case org.talend.components.api.component.ComponentDefinition.RETURN_ERROR_MESSAGE:
							container_tDBInput_2.setComponentData("tDBInput_2",
									"ERROR_MESSAGE",
									entry_tDBInput_2.getValue());
							break;
						case org.talend.components.api.component.ComponentDefinition.RETURN_TOTAL_RECORD_COUNT:
							container_tDBInput_2.setComponentData("tDBInput_2",
									"NB_LINE", entry_tDBInput_2.getValue());
							break;
						case org.talend.components.api.component.ComponentDefinition.RETURN_SUCCESS_RECORD_COUNT:
							container_tDBInput_2.setComponentData("tDBInput_2",
									"NB_SUCCESS", entry_tDBInput_2.getValue());
							break;
						case org.talend.components.api.component.ComponentDefinition.RETURN_REJECT_RECORD_COUNT:
							container_tDBInput_2.setComponentData("tDBInput_2",
									"NB_REJECT", entry_tDBInput_2.getValue());
							break;
						default:
							StringBuilder studio_key_tDBInput_2 = new StringBuilder();
							for (int i_tDBInput_2 = 0; i_tDBInput_2 < entry_tDBInput_2
									.getKey().length(); i_tDBInput_2++) {
								char ch_tDBInput_2 = entry_tDBInput_2.getKey()
										.charAt(i_tDBInput_2);
								if (Character.isUpperCase(ch_tDBInput_2)
										&& i_tDBInput_2 > 0) {
									studio_key_tDBInput_2.append('_');
								}
								studio_key_tDBInput_2.append(ch_tDBInput_2);
							}
							container_tDBInput_2.setComponentData(
									"tDBInput_2",
									studio_key_tDBInput_2.toString()
											.toUpperCase(
													java.util.Locale.ENGLISH),
									entry_tDBInput_2.getValue());
							break;
						}
					}
				}

				ok_Hash.put("tDBInput_2", true);
				end_Hash.put("tDBInput_2", System.currentTimeMillis());

				/**
				 * [tDBInput_2 end ] stop
				 */

				/**
				 * [tMap_1 end ] start
				 */

				currentComponent = "tMap_1";

				// ###############################
				// # Lookup hashes releasing
				// ###############################
				log.debug("tMap_1 - Written records count in the table 'out1': "
						+ count_out1_tMap_1 + ".");

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null
							|| !((Boolean) resourceMap.get("inIterateVComp"))) {
						runStat.updateStatOnConnection("row1" + iterateId, 2, 0);
					}
				}

				if (log.isDebugEnabled())
					log.debug("tMap_1 - " + ("Done."));

				ok_Hash.put("tMap_1", true);
				end_Hash.put("tMap_1", System.currentTimeMillis());

				/**
				 * [tMap_1 end ] stop
				 */

				/**
				 * [tDBOutput_3 end ] start
				 */

				currentComponent = "tDBOutput_3";

				// end of generic

				resourceMap.put("finish_tDBOutput_3", Boolean.TRUE);

				org.talend.components.api.component.runtime.Result resultObject_tDBOutput_3 = (org.talend.components.api.component.runtime.Result) writer_tDBOutput_3
						.close();
				final java.util.Map<String, Object> resultMap_tDBOutput_3 = writer_tDBOutput_3
						.getWriteOperation()
						.finalize(
								java.util.Arrays
										.<org.talend.components.api.component.runtime.Result> asList(resultObject_tDBOutput_3),
								container_tDBOutput_3);
				if (resultMap_tDBOutput_3 != null) {
					for (java.util.Map.Entry<String, Object> entry_tDBOutput_3 : resultMap_tDBOutput_3
							.entrySet()) {
						switch (entry_tDBOutput_3.getKey()) {
						case org.talend.components.api.component.ComponentDefinition.RETURN_ERROR_MESSAGE:
							container_tDBOutput_3.setComponentData(
									"tDBOutput_3", "ERROR_MESSAGE",
									entry_tDBOutput_3.getValue());
							break;
						case org.talend.components.api.component.ComponentDefinition.RETURN_TOTAL_RECORD_COUNT:
							container_tDBOutput_3.setComponentData(
									"tDBOutput_3", "NB_LINE",
									entry_tDBOutput_3.getValue());
							break;
						case org.talend.components.api.component.ComponentDefinition.RETURN_SUCCESS_RECORD_COUNT:
							container_tDBOutput_3.setComponentData(
									"tDBOutput_3", "NB_SUCCESS",
									entry_tDBOutput_3.getValue());
							break;
						case org.talend.components.api.component.ComponentDefinition.RETURN_REJECT_RECORD_COUNT:
							container_tDBOutput_3.setComponentData(
									"tDBOutput_3", "NB_REJECT",
									entry_tDBOutput_3.getValue());
							break;
						default:
							StringBuilder studio_key_tDBOutput_3 = new StringBuilder();
							for (int i_tDBOutput_3 = 0; i_tDBOutput_3 < entry_tDBOutput_3
									.getKey().length(); i_tDBOutput_3++) {
								char ch_tDBOutput_3 = entry_tDBOutput_3
										.getKey().charAt(i_tDBOutput_3);
								if (Character.isUpperCase(ch_tDBOutput_3)
										&& i_tDBOutput_3 > 0) {
									studio_key_tDBOutput_3.append('_');
								}
								studio_key_tDBOutput_3.append(ch_tDBOutput_3);
							}
							container_tDBOutput_3.setComponentData(
									"tDBOutput_3",
									studio_key_tDBOutput_3.toString()
											.toUpperCase(
													java.util.Locale.ENGLISH),
									entry_tDBOutput_3.getValue());
							break;
						}
					}
				}

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null
							|| !((Boolean) resourceMap.get("inIterateVComp"))) {
						runStat.updateStatOnConnection("out1" + iterateId, 2, 0);
					}
				}

				ok_Hash.put("tDBOutput_3", true);
				end_Hash.put("tDBOutput_3", System.currentTimeMillis());

				/**
				 * [tDBOutput_3 end ] stop
				 */

			}// end the resume

		} catch (java.lang.Exception e) {

			if (!(e instanceof TalendException)) {
				log.fatal(currentComponent + " " + e.getMessage(), e);
			}

			TalendException te = new TalendException(e, currentComponent,
					globalMap);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			try {

				/**
				 * [tDBInput_2 finally ] start
				 */

				currentComponent = "tDBInput_2";

				// finally of generic

				if (resourceMap.get("finish_tDBInput_2") == null) {
					if (resourceMap.get("reader_tDBInput_2") != null) {
						try {
							((org.talend.components.api.component.runtime.Reader) resourceMap
									.get("reader_tDBInput_2")).close();
						} catch (java.io.IOException e_tDBInput_2) {
							String errorMessage_tDBInput_2 = "failed to release the resource in tDBInput_2 :"
									+ e_tDBInput_2.getMessage();
							System.err.println(errorMessage_tDBInput_2);
						}
					}
				}

				/**
				 * [tDBInput_2 finally ] stop
				 */

				/**
				 * [tMap_1 finally ] start
				 */

				currentComponent = "tMap_1";

				/**
				 * [tMap_1 finally ] stop
				 */

				/**
				 * [tDBOutput_3 finally ] start
				 */

				currentComponent = "tDBOutput_3";

				// finally of generic

				if (resourceMap.get("finish_tDBOutput_3") == null) {
					if (resourceMap.get("writer_tDBOutput_3") != null) {
						try {
							((org.talend.components.api.component.runtime.Writer) resourceMap
									.get("writer_tDBOutput_3")).close();
						} catch (java.io.IOException e_tDBOutput_3) {
							String errorMessage_tDBOutput_3 = "failed to release the resource in tDBOutput_3 :"
									+ e_tDBOutput_3.getMessage();
							System.err.println(errorMessage_tDBOutput_3);
						}
					}
				}

				/**
				 * [tDBOutput_3 finally ] stop
				 */

			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("tDBInput_2_SUBPROCESS_STATE", 1);
	}

	public void tDBRollback_1Process(
			final java.util.Map<String, Object> globalMap)
			throws TalendException {
		globalMap.put("tDBRollback_1_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception()
						.getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				/**
				 * [tDBRollback_1 begin ] start
				 */

				globalMap.put("ENABLE_TRACES_CONNECTION_tDBRollback_1",
						Boolean.FALSE);

				ok_Hash.put("tDBRollback_1", false);
				start_Hash.put("tDBRollback_1", System.currentTimeMillis());

				currentComponent = "tDBRollback_1";

				int tos_count_tDBRollback_1 = 0;

				class BytesLimit65535_tDBRollback_1 {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_tDBRollback_1().limitLog4jByte();

				org.talend.components.api.component.ComponentDefinition def_tDBRollback_1 = new org.talend.components.jdbc.tjdbcrollback.TJDBCRollbackDefinition();

				org.talend.components.jdbc.tjdbcrollback.TJDBCRollbackProperties props_tDBRollback_1 = (org.talend.components.jdbc.tjdbcrollback.TJDBCRollbackProperties) def_tDBRollback_1
						.createRuntimeProperties();
				props_tDBRollback_1.setValue("closeConnection", true);

				props_tDBRollback_1.referencedComponent
						.setValue(
								"referenceType",
								org.talend.components.api.properties.ComponentReferenceProperties.ReferenceType.COMPONENT_INSTANCE);

				props_tDBRollback_1.referencedComponent.setValue(
						"componentInstanceId", "tDBConnection_1");

				props_tDBRollback_1.referencedComponent.setValue(
						"referenceDefinitionName", "tJDBCConnection");

				props_tDBRollback_1.main
						.setValue(
								"schema",
								new org.apache.avro.Schema.Parser()
										.parse("{\"type\":\"record\",\"name\":\"EmptyRecord\",\"fields\":[]}"));

				props_tDBRollback_1.schemaFlow
						.setValue(
								"schema",
								new org.apache.avro.Schema.Parser()
										.parse("{\"type\":\"record\",\"name\":\"EmptyRecord\",\"fields\":[]}"));

				if (org.talend.components.api.properties.ComponentReferenceProperties.ReferenceType.COMPONENT_INSTANCE == props_tDBRollback_1.referencedComponent.referenceType
						.getValue()) {
					final String referencedComponentInstanceId_tDBRollback_1 = props_tDBRollback_1.referencedComponent.componentInstanceId
							.getStringValue();
					if (referencedComponentInstanceId_tDBRollback_1 != null) {
						org.talend.daikon.properties.Properties referencedComponentProperties_tDBRollback_1 = (org.talend.daikon.properties.Properties) globalMap
								.get(referencedComponentInstanceId_tDBRollback_1
										+ "_COMPONENT_RUNTIME_PROPERTIES");
						props_tDBRollback_1.referencedComponent
								.setReference(referencedComponentProperties_tDBRollback_1);
					}
				}
				globalMap.put("tDBRollback_1_COMPONENT_RUNTIME_PROPERTIES",
						props_tDBRollback_1);

				java.net.URL mappings_url_tDBRollback_1 = this.getClass()
						.getResource("/xmlMappings");
				globalMap.put("tDBRollback_1_MAPPINGS_URL",
						mappings_url_tDBRollback_1);

				org.talend.components.api.container.RuntimeContainer container_tDBRollback_1 = new org.talend.components.api.container.RuntimeContainer() {
					public Object getComponentData(String componentId,
							String key) {
						return globalMap.get(componentId + "_" + key);
					}

					public void setComponentData(String componentId,
							String key, Object data) {
						globalMap.put(componentId + "_" + key, data);
					}

					public String getCurrentComponentId() {
						return "tDBRollback_1";
					}

					public Object getGlobalData(String key) {
						return globalMap.get(key);
					}
				};

				int nb_line_tDBRollback_1 = 0;

				org.talend.components.api.component.ConnectorTopology topology_tDBRollback_1 = null;
				topology_tDBRollback_1 = org.talend.components.api.component.ConnectorTopology.NONE;

				org.talend.daikon.runtime.RuntimeInfo runtime_info_tDBRollback_1 = def_tDBRollback_1
						.getRuntimeInfo(
								org.talend.components.api.component.runtime.ExecutionEngine.DI,
								props_tDBRollback_1, topology_tDBRollback_1);
				java.util.Set<org.talend.components.api.component.ConnectorTopology> supported_connector_topologies_tDBRollback_1 = def_tDBRollback_1
						.getSupportedConnectorTopologies();

				org.talend.components.api.component.runtime.RuntimableRuntime componentRuntime_tDBRollback_1 = (org.talend.components.api.component.runtime.RuntimableRuntime) (Class
						.forName(runtime_info_tDBRollback_1
								.getRuntimeClassName()).newInstance());
				org.talend.daikon.properties.ValidationResult initVr_tDBRollback_1 = componentRuntime_tDBRollback_1
						.initialize(container_tDBRollback_1,
								props_tDBRollback_1);

				if (initVr_tDBRollback_1.getStatus() == org.talend.daikon.properties.ValidationResult.Result.ERROR) {
					throw new RuntimeException(
							initVr_tDBRollback_1.getMessage());
				}

				if (componentRuntime_tDBRollback_1 instanceof org.talend.components.api.component.runtime.ComponentDriverInitialization) {
					org.talend.components.api.component.runtime.ComponentDriverInitialization compDriverInitialization_tDBRollback_1 = (org.talend.components.api.component.runtime.ComponentDriverInitialization) componentRuntime_tDBRollback_1;
					compDriverInitialization_tDBRollback_1
							.runAtDriver(container_tDBRollback_1);
				}

				org.talend.components.api.component.runtime.SourceOrSink sourceOrSink_tDBRollback_1 = null;
				if (componentRuntime_tDBRollback_1 instanceof org.talend.components.api.component.runtime.SourceOrSink) {
					sourceOrSink_tDBRollback_1 = (org.talend.components.api.component.runtime.SourceOrSink) componentRuntime_tDBRollback_1;
					org.talend.daikon.properties.ValidationResult vr_tDBRollback_1 = sourceOrSink_tDBRollback_1
							.validate(container_tDBRollback_1);
					if (vr_tDBRollback_1.getStatus() == org.talend.daikon.properties.ValidationResult.Result.ERROR) {
						throw new RuntimeException(
								vr_tDBRollback_1.getMessage());
					}
				}

				/**
				 * [tDBRollback_1 begin ] stop
				 */

				/**
				 * [tDBRollback_1 main ] start
				 */

				currentComponent = "tDBRollback_1";

				tos_count_tDBRollback_1++;

				/**
				 * [tDBRollback_1 main ] stop
				 */

				/**
				 * [tDBRollback_1 process_data_begin ] start
				 */

				currentComponent = "tDBRollback_1";

				/**
				 * [tDBRollback_1 process_data_begin ] stop
				 */

				/**
				 * [tDBRollback_1 process_data_end ] start
				 */

				currentComponent = "tDBRollback_1";

				/**
				 * [tDBRollback_1 process_data_end ] stop
				 */

				if (!isChildJob
						&& (Boolean) globalMap
								.get("ENABLE_TRACES_CONNECTION_tDBRollback_1")) {
					if (globalMap.get("USE_CONDITION") != null
							&& (Boolean) globalMap.get("USE_CONDITION")) {
						if (globalMap.get("TRACE_CONDITION") != null
								&& (Boolean) globalMap.get("TRACE_CONDITION")) {
							// if next breakpoint has been clicked on UI or if
							// start job, should wait action of user.
							if (runTrace.isNextBreakpoint()) {
								runTrace.waitForUserAction();
							} else if (runTrace.isNextRow()) {
								runTrace.waitForUserAction();
							}
						} else {
							// if next row has been clicked on UI or if start
							// job, should wait action of user.
							if (runTrace.isNextRow()) {
								runTrace.waitForUserAction();
							}
						}
					} else { // no condition set
						if (runTrace.isNextRow()) {
							runTrace.waitForUserAction();
						} else {
							Thread.sleep(1000);
						}
					}

				}
				globalMap.put("USE_CONDITION", Boolean.FALSE);

				/**
				 * [tDBRollback_1 end ] start
				 */

				currentComponent = "tDBRollback_1";

				// end of generic

				resourceMap.put("finish_tDBRollback_1", Boolean.TRUE);

				ok_Hash.put("tDBRollback_1", true);
				end_Hash.put("tDBRollback_1", System.currentTimeMillis());

				/**
				 * [tDBRollback_1 end ] stop
				 */
			}// end the resume

		} catch (java.lang.Exception e) {

			if (!(e instanceof TalendException)) {
				log.fatal(currentComponent + " " + e.getMessage(), e);
			}

			TalendException te = new TalendException(e, currentComponent,
					globalMap);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			try {

				/**
				 * [tDBRollback_1 finally ] start
				 */

				currentComponent = "tDBRollback_1";

				// finally of generic

				if (resourceMap.get("finish_tDBRollback_1") == null) {
				}

				/**
				 * [tDBRollback_1 finally ] stop
				 */
			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("tDBRollback_1_SUBPROCESS_STATE", 1);
	}

	public String resuming_logs_dir_path = null;
	public String resuming_checkpoint_path = null;
	public String parent_part_launcher = null;
	private String resumeEntryMethodName = null;
	private boolean globalResumeTicket = false;

	public boolean watch = false;
	// portStats is null, it means don't execute the statistics
	public Integer portStats = null;
	public int portTraces = 4334;
	public String clientHost;
	public String defaultClientHost = "localhost";
	public String contextStr = "Default";
	public boolean isDefaultContext = true;
	public String pid = "0";
	public String rootPid = null;
	public String fatherPid = null;
	public String fatherNode = null;
	public long startTime = 0;
	public boolean isChildJob = false;
	public String log4jLevel = "";

	private boolean execStat = true;

	private ThreadLocal<java.util.Map<String, String>> threadLocal = new ThreadLocal<java.util.Map<String, String>>() {
		protected java.util.Map<String, String> initialValue() {
			java.util.Map<String, String> threadRunResultMap = new java.util.HashMap<String, String>();
			threadRunResultMap.put("errorCode", null);
			threadRunResultMap.put("status", "");
			return threadRunResultMap;
		};
	};

	private PropertiesWithType context_param = new PropertiesWithType();
	public java.util.Map<String, Object> parentContextMap = new java.util.HashMap<String, Object>();

	public String status = "";

	private final org.talend.components.common.runtime.SharedConnectionsPool connectionPool = new org.talend.components.common.runtime.SharedConnectionsPool() {
		public java.sql.Connection getDBConnection(String dbDriver, String url,
				String userName, String password, String dbConnectionName)
				throws ClassNotFoundException, java.sql.SQLException {
			return SharedDBConnection.getDBConnection(dbDriver, url, userName,
					password, dbConnectionName);
		}

		public java.sql.Connection getDBConnection(String dbDriver, String url,
				String dbConnectionName) throws ClassNotFoundException,
				java.sql.SQLException {
			return SharedDBConnection.getDBConnection(dbDriver, url,
					dbConnectionName);
		}
	};

	private static final String GLOBAL_CONNECTION_POOL_KEY = "GLOBAL_CONNECTION_POOL";

	{
		globalMap.put(GLOBAL_CONNECTION_POOL_KEY, connectionPool);
	}

	public static void main(String[] args) {
		final cci_test cci_testClass = new cci_test();

		int exitCode = cci_testClass.runJobInTOS(args);
		if (exitCode == 0) {
			log.info("TalendJob: 'cci_test' - Done.");
		}

		System.exit(exitCode);
	}

	public String[][] runJob(String[] args) {

		int exitCode = runJobInTOS(args);
		String[][] bufferValue = new String[][] { { Integer.toString(exitCode) } };

		return bufferValue;
	}

	public boolean hastBufferOutputComponent() {
		boolean hastBufferOutput = false;

		return hastBufferOutput;
	}

	public int runJobInTOS(String[] args) {
		// reset status
		status = "";

		String lastStr = "";
		for (String arg : args) {
			if (arg.equalsIgnoreCase("--context_param")) {
				lastStr = arg;
			} else if (lastStr.equals("")) {
				evalParam(arg);
			} else {
				evalParam(lastStr + " " + arg);
				lastStr = "";
			}
		}

		if (!"".equals(log4jLevel)) {
			if ("trace".equalsIgnoreCase(log4jLevel)) {
				log.setLevel(org.apache.log4j.Level.TRACE);
			} else if ("debug".equalsIgnoreCase(log4jLevel)) {
				log.setLevel(org.apache.log4j.Level.DEBUG);
			} else if ("info".equalsIgnoreCase(log4jLevel)) {
				log.setLevel(org.apache.log4j.Level.INFO);
			} else if ("warn".equalsIgnoreCase(log4jLevel)) {
				log.setLevel(org.apache.log4j.Level.WARN);
			} else if ("error".equalsIgnoreCase(log4jLevel)) {
				log.setLevel(org.apache.log4j.Level.ERROR);
			} else if ("fatal".equalsIgnoreCase(log4jLevel)) {
				log.setLevel(org.apache.log4j.Level.FATAL);
			} else if ("off".equalsIgnoreCase(log4jLevel)) {
				log.setLevel(org.apache.log4j.Level.OFF);
			}
			org.apache.log4j.Logger.getRootLogger().setLevel(log.getLevel());
		}
		log.info("TalendJob: 'cci_test' - Start.");

		if (clientHost == null) {
			clientHost = defaultClientHost;
		}

		if (pid == null || "0".equals(pid)) {
			pid = TalendString.getAsciiRandomString(6);
		}

		if (rootPid == null) {
			rootPid = pid;
		}
		if (fatherPid == null) {
			fatherPid = pid;
		} else {
			isChildJob = true;
		}

		if (portStats != null) {
			// portStats = -1; //for testing
			if (portStats < 0 || portStats > 65535) {
				// issue:10869, the portStats is invalid, so this client socket
				// can't open
				System.err.println("The statistics socket port " + portStats
						+ " is invalid.");
				execStat = false;
			}
		} else {
			execStat = false;
		}

		try {
			// call job/subjob with an existing context, like:
			// --context=production. if without this parameter, there will use
			// the default context instead.
			java.io.InputStream inContext = cci_test.class.getClassLoader()
					.getResourceAsStream(
							"cci/cci_test_0_1/contexts/" + contextStr
									+ ".properties");
			if (inContext == null) {
				inContext = cci_test.class
						.getClassLoader()
						.getResourceAsStream(
								"config/contexts/" + contextStr + ".properties");
			}
			if (inContext != null) {
				// defaultProps is in order to keep the original context value
				defaultProps.load(inContext);
				inContext.close();
				context = new ContextProperties(defaultProps);
			} else if (!isDefaultContext) {
				// print info and job continue to run, for case: context_param
				// is not empty.
				System.err.println("Could not find the context " + contextStr);
			}

			if (!context_param.isEmpty()) {
				context.putAll(context_param);
				// set types for params from parentJobs
				for (Object key : context_param.keySet()) {
					String context_key = key.toString();
					String context_type = context_param
							.getContextType(context_key);
					context.setContextType(context_key, context_type);

				}
			}
		} catch (java.io.IOException ie) {
			System.err.println("Could not load context " + contextStr);
			ie.printStackTrace();
		}

		// get context value from parent directly
		if (parentContextMap != null && !parentContextMap.isEmpty()) {
		}

		// Resume: init the resumeUtil
		resumeEntryMethodName = ResumeUtil
				.getResumeEntryMethodName(resuming_checkpoint_path);
		resumeUtil = new ResumeUtil(resuming_logs_dir_path, isChildJob, rootPid);
		resumeUtil.initCommonInfo(pid, rootPid, fatherPid, projectName,
				jobName, contextStr, jobVersion);

		List<String> parametersToEncrypt = new java.util.ArrayList<String>();
		// Resume: jobStart
		resumeUtil.addLog("JOB_STARTED", "JOB:" + jobName,
				parent_part_launcher, Thread.currentThread().getId() + "", "",
				"", "", "",
				resumeUtil.convertToJsonText(context, parametersToEncrypt));

		if (execStat) {
			try {
				runStat.openSocket(!isChildJob);
				runStat.setAllPID(rootPid, fatherPid, pid, jobName);
				runStat.startThreadStat(clientHost, portStats);
				runStat.updateStatOnJob(RunStat.JOBSTART, fatherNode);
			} catch (java.io.IOException ioException) {
				ioException.printStackTrace();
			}
		}

		try {
			runTrace.openSocket(!isChildJob);
			runTrace.startThreadTrace(clientHost, portTraces);
		} catch (java.io.IOException ioException) {
			ioException.printStackTrace();
		}

		java.util.concurrent.ConcurrentHashMap<Object, Object> concurrentHashMap = new java.util.concurrent.ConcurrentHashMap<Object, Object>();
		globalMap.put("concurrentHashMap", concurrentHashMap);

		long startUsedMemory = Runtime.getRuntime().totalMemory()
				- Runtime.getRuntime().freeMemory();
		long endUsedMemory = 0;
		long end = 0;

		startTime = System.currentTimeMillis();

		this.globalResumeTicket = true;// to run tPreJob

		this.globalResumeTicket = false;// to run others jobs

		try {
			errorCode = null;
			tDBConnection_1Process(globalMap);
			if (!"failure".equals(status)) {
				status = "end";
			}
		} catch (TalendException e_tDBConnection_1) {
			globalMap.put("tDBConnection_1_SUBPROCESS_STATE", -1);

			e_tDBConnection_1.printStackTrace();

		}

		this.globalResumeTicket = true;// to run tPostJob

		end = System.currentTimeMillis();

		if (watch) {
			System.out.println((end - startTime) + " milliseconds");
		}

		endUsedMemory = Runtime.getRuntime().totalMemory()
				- Runtime.getRuntime().freeMemory();
		if (false) {
			System.out.println((endUsedMemory - startUsedMemory)
					+ " bytes memory increase when running : cci_test");
		}

		if (execStat) {
			runStat.updateStatOnJob(RunStat.JOBEND, fatherNode);
			runStat.stopThreadStat();
		}
		runTrace.stopThreadTrace();
		int returnCode = 0;
		if (errorCode == null) {
			returnCode = status != null && status.equals("failure") ? 1 : 0;
		} else {
			returnCode = errorCode.intValue();
		}
		resumeUtil.addLog("JOB_ENDED", "JOB:" + jobName, parent_part_launcher,
				Thread.currentThread().getId() + "", "", "" + returnCode, "",
				"", "");

		return returnCode;

	}

	// only for OSGi env
	public void destroy() {
		closeSqlDbConnections();

	}

	private void closeSqlDbConnections() {
		try {
			Object obj_conn;
			obj_conn = globalMap.remove("conn_tDBConnection_1");
			if (null != obj_conn) {
				((java.sql.Connection) obj_conn).close();
			}
		} catch (java.lang.Exception e) {
		}
	}

	private java.util.Map<String, Object> getSharedConnections4REST() {
		java.util.Map<String, Object> connections = new java.util.HashMap<String, Object>();
		connections.put("conn_tDBConnection_1",
				globalMap.get("conn_tDBConnection_1"));

		return connections;
	}

	private void evalParam(String arg) {
		if (arg.startsWith("--resuming_logs_dir_path")) {
			resuming_logs_dir_path = arg.substring(25);
		} else if (arg.startsWith("--resuming_checkpoint_path")) {
			resuming_checkpoint_path = arg.substring(27);
		} else if (arg.startsWith("--parent_part_launcher")) {
			parent_part_launcher = arg.substring(23);
		} else if (arg.startsWith("--watch")) {
			watch = true;
		} else if (arg.startsWith("--stat_port=")) {
			String portStatsStr = arg.substring(12);
			if (portStatsStr != null && !portStatsStr.equals("null")) {
				portStats = Integer.parseInt(portStatsStr);
			}
		} else if (arg.startsWith("--trace_port=")) {
			portTraces = Integer.parseInt(arg.substring(13));
		} else if (arg.startsWith("--client_host=")) {
			clientHost = arg.substring(14);
		} else if (arg.startsWith("--context=")) {
			contextStr = arg.substring(10);
			isDefaultContext = false;
		} else if (arg.startsWith("--father_pid=")) {
			fatherPid = arg.substring(13);
		} else if (arg.startsWith("--root_pid=")) {
			rootPid = arg.substring(11);
		} else if (arg.startsWith("--father_node=")) {
			fatherNode = arg.substring(14);
		} else if (arg.startsWith("--pid=")) {
			pid = arg.substring(6);
		} else if (arg.startsWith("--context_type")) {
			String keyValue = arg.substring(15);
			int index = -1;
			if (keyValue != null && (index = keyValue.indexOf('=')) > -1) {
				if (fatherPid == null) {
					context_param.setContextType(keyValue.substring(0, index),
							replaceEscapeChars(keyValue.substring(index + 1)));
				} else { // the subjob won't escape the especial chars
					context_param.setContextType(keyValue.substring(0, index),
							keyValue.substring(index + 1));
				}

			}

		} else if (arg.startsWith("--context_param")) {
			String keyValue = arg.substring(16);
			int index = -1;
			if (keyValue != null && (index = keyValue.indexOf('=')) > -1) {
				if (fatherPid == null) {
					context_param.put(keyValue.substring(0, index),
							replaceEscapeChars(keyValue.substring(index + 1)));
				} else { // the subjob won't escape the especial chars
					context_param.put(keyValue.substring(0, index),
							keyValue.substring(index + 1));
				}
			}
		} else if (arg.startsWith("--log4jLevel=")) {
			log4jLevel = arg.substring(13);
		}

	}

	private static final String NULL_VALUE_EXPRESSION_IN_COMMAND_STRING_FOR_CHILD_JOB_ONLY = "<TALEND_NULL>";

	private final String[][] escapeChars = { { "\\\\", "\\" }, { "\\n", "\n" },
			{ "\\'", "\'" }, { "\\r", "\r" }, { "\\f", "\f" }, { "\\b", "\b" },
			{ "\\t", "\t" } };

	private String replaceEscapeChars(String keyValue) {

		if (keyValue == null || ("").equals(keyValue.trim())) {
			return keyValue;
		}

		StringBuilder result = new StringBuilder();
		int currIndex = 0;
		while (currIndex < keyValue.length()) {
			int index = -1;
			// judege if the left string includes escape chars
			for (String[] strArray : escapeChars) {
				index = keyValue.indexOf(strArray[0], currIndex);
				if (index >= 0) {

					result.append(keyValue.substring(currIndex,
							index + strArray[0].length()).replace(strArray[0],
							strArray[1]));
					currIndex = index + strArray[0].length();
					break;
				}
			}
			// if the left string doesn't include escape chars, append the left
			// into the result
			if (index < 0) {
				result.append(keyValue.substring(currIndex));
				currIndex = currIndex + keyValue.length();
			}
		}

		return result.toString();
	}

	public Integer getErrorCode() {
		return errorCode;
	}

	public String getStatus() {
		return status;
	}

	ResumeUtil resumeUtil = null;
}
/************************************************************************************************
 * 99998 characters generated by Talend Data Integration on the February 12,
 * 2019 11:35:42 AM EET
 ************************************************************************************************/
