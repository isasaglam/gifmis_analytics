package cci.cci_test_native_comp_0_1;

import routines.Numeric;
import routines.DataOperation;
import routines.TalendDataGenerator;
import routines.TalendStringUtil;
import routines.TalendString;
import routines.StringHandling;
import routines.Relational;
import routines.TalendDate;
import routines.Mathematical;
import routines.SQLike;
import routines.system.*;
import routines.system.api.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.math.BigDecimal;
import java.io.ByteArrayOutputStream;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.io.IOException;
import java.util.Comparator;

@SuppressWarnings("unused")
/**
 * Job: cci_test_native_comp Purpose: <br>
 * Description:  <br>
 * @author isa.saglam@oredata.com
 * @version 7.0.1.20180411_1414
 * @status 
 */
public class cci_test_native_comp implements TalendJob {
	static {
		System.setProperty("TalendJob.log", "cci_test_native_comp.log");
	}
	private static org.apache.log4j.Logger log = org.apache.log4j.Logger
			.getLogger(cci_test_native_comp.class);

	protected static void logIgnoredError(String message, Throwable cause) {
		log.error(message, cause);

	}

	public final Object obj = new Object();

	// for transmiting parameters purpose
	private Object valueObject = null;

	public Object getValueObject() {
		return this.valueObject;
	}

	public void setValueObject(Object valueObject) {
		this.valueObject = valueObject;
	}

	private final static String defaultCharset = java.nio.charset.Charset
			.defaultCharset().name();

	private final static String utf8Charset = "UTF-8";

	// contains type for every context property
	public class PropertiesWithType extends java.util.Properties {
		private static final long serialVersionUID = 1L;
		private java.util.Map<String, String> propertyTypes = new java.util.HashMap<>();

		public PropertiesWithType(java.util.Properties properties) {
			super(properties);
		}

		public PropertiesWithType() {
			super();
		}

		public void setContextType(String key, String type) {
			propertyTypes.put(key, type);
		}

		public String getContextType(String key) {
			return propertyTypes.get(key);
		}
	}

	// create and load default properties
	private java.util.Properties defaultProps = new java.util.Properties();

	// create application properties with default
	public class ContextProperties extends PropertiesWithType {

		private static final long serialVersionUID = 1L;

		public ContextProperties(java.util.Properties properties) {
			super(properties);
		}

		public ContextProperties() {
			super();
		}

		public void synchronizeContext() {

		}

	}

	private ContextProperties context = new ContextProperties();

	public ContextProperties getContext() {
		return this.context;
	}

	private final String jobVersion = "0.1";
	private final String jobName = "cci_test_native_comp";
	private final String projectName = "CCI";
	public Integer errorCode = null;
	private String currentComponent = "";

	private final java.util.Map<String, Object> globalMap = new java.util.HashMap<String, Object>();
	private final static java.util.Map<String, Object> junitGlobalMap = new java.util.HashMap<String, Object>();

	private final java.util.Map<String, Long> start_Hash = new java.util.HashMap<String, Long>();
	private final java.util.Map<String, Long> end_Hash = new java.util.HashMap<String, Long>();
	private final java.util.Map<String, Boolean> ok_Hash = new java.util.HashMap<String, Boolean>();
	public final java.util.List<String[]> globalBuffer = new java.util.ArrayList<String[]>();

	private RunStat runStat = new RunStat();

	// OSGi DataSource
	private final static String KEY_DB_DATASOURCES = "KEY_DB_DATASOURCES";

	private final static String KEY_DB_DATASOURCES_RAW = "KEY_DB_DATASOURCES_RAW";

	public void setDataSources(
			java.util.Map<String, javax.sql.DataSource> dataSources) {
		java.util.Map<String, routines.system.TalendDataSource> talendDataSources = new java.util.HashMap<String, routines.system.TalendDataSource>();
		for (java.util.Map.Entry<String, javax.sql.DataSource> dataSourceEntry : dataSources
				.entrySet()) {
			talendDataSources.put(
					dataSourceEntry.getKey(),
					new routines.system.TalendDataSource(dataSourceEntry
							.getValue()));
		}
		globalMap.put(KEY_DB_DATASOURCES, talendDataSources);
		globalMap
				.put(KEY_DB_DATASOURCES_RAW,
						new java.util.HashMap<String, javax.sql.DataSource>(
								dataSources));
	}

	private final java.io.ByteArrayOutputStream baos = new java.io.ByteArrayOutputStream();
	private final java.io.PrintStream errorMessagePS = new java.io.PrintStream(
			new java.io.BufferedOutputStream(baos));

	public String getExceptionStackTrace() {
		if ("failure".equals(this.getStatus())) {
			errorMessagePS.flush();
			return baos.toString();
		}
		return null;
	}

	private Exception exception;

	public Exception getException() {
		if ("failure".equals(this.getStatus())) {
			return this.exception;
		}
		return null;
	}

	private class TalendException extends Exception {

		private static final long serialVersionUID = 1L;

		private java.util.Map<String, Object> globalMap = null;
		private Exception e = null;
		private String currentComponent = null;
		private String virtualComponentName = null;

		public void setVirtualComponentName(String virtualComponentName) {
			this.virtualComponentName = virtualComponentName;
		}

		private TalendException(Exception e, String errorComponent,
				final java.util.Map<String, Object> globalMap) {
			this.currentComponent = errorComponent;
			this.globalMap = globalMap;
			this.e = e;
		}

		public Exception getException() {
			return this.e;
		}

		public String getCurrentComponent() {
			return this.currentComponent;
		}

		public String getExceptionCauseMessage(Exception e) {
			Throwable cause = e;
			String message = null;
			int i = 10;
			while (null != cause && 0 < i--) {
				message = cause.getMessage();
				if (null == message) {
					cause = cause.getCause();
				} else {
					break;
				}
			}
			if (null == message) {
				message = e.getClass().getName();
			}
			return message;
		}

		@Override
		public void printStackTrace() {
			if (!(e instanceof TalendException || e instanceof TDieException)) {
				if (virtualComponentName != null
						&& currentComponent.indexOf(virtualComponentName + "_") == 0) {
					globalMap.put(virtualComponentName + "_ERROR_MESSAGE",
							getExceptionCauseMessage(e));
				}
				globalMap.put(currentComponent + "_ERROR_MESSAGE",
						getExceptionCauseMessage(e));
				System.err.println("Exception in component " + currentComponent
						+ " (" + jobName + ")");
			}
			if (!(e instanceof TDieException)) {
				if (e instanceof TalendException) {
					e.printStackTrace();
				} else {
					e.printStackTrace();
					e.printStackTrace(errorMessagePS);
					cci_test_native_comp.this.exception = e;
				}
			}
			if (!(e instanceof TalendException)) {
				try {
					for (java.lang.reflect.Method m : this.getClass()
							.getEnclosingClass().getMethods()) {
						if (m.getName().compareTo(currentComponent + "_error") == 0) {
							m.invoke(cci_test_native_comp.this, new Object[] {
									e, currentComponent, globalMap });
							break;
						}
					}

					if (!(e instanceof TDieException)) {
					}
				} catch (Exception e) {
					this.e.printStackTrace();
				}
			}
		}
	}

	public void tBigQueryInput_1_error(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tBigQueryInput_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tMap_1_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tBigQueryInput_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tBigQueryOutput_1_tBQOB_error(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		tBigQueryOutput_1_tBQBE_error(exception, errorComponent, globalMap);

	}

	public void tBigQueryOutput_1_tBQBE_error(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tBigQueryInput_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tBigQueryInput_1_onSubJobError(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread
				.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(),
				ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public static class outStruct implements
			routines.system.IPersistableRow<outStruct> {
		final static byte[] commonByteArrayLock_CCI_cci_test_native_comp = new byte[0];
		static byte[] commonByteArray_CCI_cci_test_native_comp = new byte[0];

		public String EquipmentTypeCode;

		public String getEquipmentTypeCode() {
			return this.EquipmentTypeCode;
		}

		public String LanguageCode;

		public String getLanguageCode() {
			return this.LanguageCode;
		}

		public String EquipmentTypeName;

		public String getEquipmentTypeName() {
			return this.EquipmentTypeName;
		}

		private String readString(ObjectInputStream dis) throws IOException {
			String strReturn = null;
			int length = 0;
			length = dis.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				if (length > commonByteArray_CCI_cci_test_native_comp.length) {
					if (length < 1024
							&& commonByteArray_CCI_cci_test_native_comp.length == 0) {
						commonByteArray_CCI_cci_test_native_comp = new byte[1024];
					} else {
						commonByteArray_CCI_cci_test_native_comp = new byte[2 * length];
					}
				}
				dis.readFully(commonByteArray_CCI_cci_test_native_comp, 0,
						length);
				strReturn = new String(
						commonByteArray_CCI_cci_test_native_comp, 0, length,
						utf8Charset);
			}
			return strReturn;
		}

		private void writeString(String str, ObjectOutputStream dos)
				throws IOException {
			if (str == null) {
				dos.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				dos.writeInt(byteArray.length);
				dos.write(byteArray);
			}
		}

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_CCI_cci_test_native_comp) {

				try {

					int length = 0;

					this.EquipmentTypeCode = readString(dis);

					this.LanguageCode = readString(dis);

					this.EquipmentTypeName = readString(dis);

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

				// String

				writeString(this.EquipmentTypeCode, dos);

				// String

				writeString(this.LanguageCode, dos);

				// String

				writeString(this.EquipmentTypeName, dos);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("EquipmentTypeCode=" + EquipmentTypeCode);
			sb.append(",LanguageCode=" + LanguageCode);
			sb.append(",EquipmentTypeName=" + EquipmentTypeName);
			sb.append("]");

			return sb.toString();
		}

		public String toLogString() {
			StringBuilder sb = new StringBuilder();

			if (EquipmentTypeCode == null) {
				sb.append("<null>");
			} else {
				sb.append(EquipmentTypeCode);
			}

			sb.append("|");

			if (LanguageCode == null) {
				sb.append("<null>");
			} else {
				sb.append(LanguageCode);
			}

			sb.append("|");

			if (EquipmentTypeName == null) {
				sb.append("<null>");
			} else {
				sb.append(EquipmentTypeName);
			}

			sb.append("|");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(outStruct other) {

			int returnValue = -1;

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(),
						object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public static class row2Struct implements
			routines.system.IPersistableRow<row2Struct> {
		final static byte[] commonByteArrayLock_CCI_cci_test_native_comp = new byte[0];
		static byte[] commonByteArray_CCI_cci_test_native_comp = new byte[0];

		public String EquipmentTypeCode;

		public String getEquipmentTypeCode() {
			return this.EquipmentTypeCode;
		}

		public String LanguageCode;

		public String getLanguageCode() {
			return this.LanguageCode;
		}

		public String EquipmentTypeName;

		public String getEquipmentTypeName() {
			return this.EquipmentTypeName;
		}

		private String readString(ObjectInputStream dis) throws IOException {
			String strReturn = null;
			int length = 0;
			length = dis.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				if (length > commonByteArray_CCI_cci_test_native_comp.length) {
					if (length < 1024
							&& commonByteArray_CCI_cci_test_native_comp.length == 0) {
						commonByteArray_CCI_cci_test_native_comp = new byte[1024];
					} else {
						commonByteArray_CCI_cci_test_native_comp = new byte[2 * length];
					}
				}
				dis.readFully(commonByteArray_CCI_cci_test_native_comp, 0,
						length);
				strReturn = new String(
						commonByteArray_CCI_cci_test_native_comp, 0, length,
						utf8Charset);
			}
			return strReturn;
		}

		private void writeString(String str, ObjectOutputStream dos)
				throws IOException {
			if (str == null) {
				dos.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				dos.writeInt(byteArray.length);
				dos.write(byteArray);
			}
		}

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_CCI_cci_test_native_comp) {

				try {

					int length = 0;

					this.EquipmentTypeCode = readString(dis);

					this.LanguageCode = readString(dis);

					this.EquipmentTypeName = readString(dis);

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

				// String

				writeString(this.EquipmentTypeCode, dos);

				// String

				writeString(this.LanguageCode, dos);

				// String

				writeString(this.EquipmentTypeName, dos);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("EquipmentTypeCode=" + EquipmentTypeCode);
			sb.append(",LanguageCode=" + LanguageCode);
			sb.append(",EquipmentTypeName=" + EquipmentTypeName);
			sb.append("]");

			return sb.toString();
		}

		public String toLogString() {
			StringBuilder sb = new StringBuilder();

			if (EquipmentTypeCode == null) {
				sb.append("<null>");
			} else {
				sb.append(EquipmentTypeCode);
			}

			sb.append("|");

			if (LanguageCode == null) {
				sb.append("<null>");
			} else {
				sb.append(LanguageCode);
			}

			sb.append("|");

			if (EquipmentTypeName == null) {
				sb.append("<null>");
			} else {
				sb.append(EquipmentTypeName);
			}

			sb.append("|");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(row2Struct other) {

			int returnValue = -1;

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(),
						object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public void tBigQueryInput_1Process(
			final java.util.Map<String, Object> globalMap)
			throws TalendException {
		globalMap.put("tBigQueryInput_1_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;
		String currentVirtualComponent = null;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception()
						.getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				row2Struct row2 = new row2Struct();
				outStruct out = new outStruct();

				/**
				 * [tBigQueryOutput_1_tBQOB begin ] start
				 */

				ok_Hash.put("tBigQueryOutput_1_tBQOB", false);
				start_Hash.put("tBigQueryOutput_1_tBQOB",
						System.currentTimeMillis());

				currentVirtualComponent = "tBigQueryOutput_1";

				currentComponent = "tBigQueryOutput_1_tBQOB";

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null) {

						runStat.updateStatOnConnection("out" + iterateId, 0, 0);

					}
				}

				int tos_count_tBigQueryOutput_1_tBQOB = 0;

				if (log.isDebugEnabled())
					log.debug("tBigQueryOutput_1_tBQOB - " + ("Start to work."));
				class BytesLimit65535_tBigQueryOutput_1_tBQOB {
					public void limitLog4jByte() throws Exception {

						StringBuilder log4jParamters_tBigQueryOutput_1_tBQOB = new StringBuilder();
						log4jParamters_tBigQueryOutput_1_tBQOB
								.append("Parameters:");
						log4jParamters_tBigQueryOutput_1_tBQOB
								.append("FILENAME"
										+ " = "
										+ "\"/Users/isa/Documents/gifmis_bitbucket/GifMis Analytics/Talend Workspace/biquery_bulk.txt\"");
						log4jParamters_tBigQueryOutput_1_tBQOB.append(" | ");
						log4jParamters_tBigQueryOutput_1_tBQOB.append("APPEND"
								+ " = " + "false");
						log4jParamters_tBigQueryOutput_1_tBQOB.append(" | ");
						log4jParamters_tBigQueryOutput_1_tBQOB
								.append("FIELDSEPARATOR" + " = " + "\";\"");
						log4jParamters_tBigQueryOutput_1_tBQOB.append(" | ");
						log4jParamters_tBigQueryOutput_1_tBQOB.append("CREATE"
								+ " = " + "true");
						log4jParamters_tBigQueryOutput_1_tBQOB.append(" | ");
						log4jParamters_tBigQueryOutput_1_tBQOB
								.append("FLUSHONROW" + " = " + "false");
						log4jParamters_tBigQueryOutput_1_tBQOB.append(" | ");
						log4jParamters_tBigQueryOutput_1_tBQOB
								.append("CHECK_DISK_SPACE" + " = " + "false");
						log4jParamters_tBigQueryOutput_1_tBQOB.append(" | ");
						log4jParamters_tBigQueryOutput_1_tBQOB
								.append("ENCODING" + " = " + "\"ISO-8859-15\"");
						log4jParamters_tBigQueryOutput_1_tBQOB.append(" | ");
						if (log.isDebugEnabled())
							log.debug("tBigQueryOutput_1_tBQOB - "
									+ (log4jParamters_tBigQueryOutput_1_tBQOB));
					}
				}

				new BytesLimit65535_tBigQueryOutput_1_tBQOB().limitLog4jByte();

				int nb_line_tBigQueryOutput_1_tBQOB = 0;
				String rowSeparator_tBigQueryOutput_1_tBQOB = "\n";
				String fieldSeparator_tBigQueryOutput_1_tBQOB = ";";
				char escapeChar_tBigQueryOutput_1_tBQOB = "\\".charAt(0);
				char textEnclosure_tBigQueryOutput_1_tBQOB = "\"".charAt(0);

				escapeChar_tBigQueryOutput_1_tBQOB = textEnclosure_tBigQueryOutput_1_tBQOB;

				if (escapeChar_tBigQueryOutput_1_tBQOB == '\\') {
				} else if (escapeChar_tBigQueryOutput_1_tBQOB == textEnclosure_tBigQueryOutput_1_tBQOB) {
				} else {
					throw new RuntimeException(
							"The escape mode only support the '\\' or double text enclosure.");
				}

				java.io.File file_tBigQueryOutput_1_tBQOB = new java.io.File(
						"/Users/isa/Documents/gifmis_bitbucket/GifMis Analytics/Talend Workspace/biquery_bulk.txt");

				log.info("tBigQueryOutput_1_tBQOB - Creating directory for file '"
						+ file_tBigQueryOutput_1_tBQOB.getCanonicalPath()
						+ "', if the directory not exist.");

				file_tBigQueryOutput_1_tBQOB.getParentFile().mkdirs();

				com.talend.csv.CSVWriter csvWriter_tBigQueryOutput_1_tBQOB = new com.talend.csv.CSVWriter(
						new java.io.BufferedWriter(
								new java.io.OutputStreamWriter(
										new java.io.FileOutputStream(
												file_tBigQueryOutput_1_tBQOB,
												false), "ISO-8859-15")));
				resourceMap.put("csvWriter_tBigQueryOutput_1_tBQOB",
						csvWriter_tBigQueryOutput_1_tBQOB);
				csvWriter_tBigQueryOutput_1_tBQOB
						.setSeparator(fieldSeparator_tBigQueryOutput_1_tBQOB
								.charAt(0));

				csvWriter_tBigQueryOutput_1_tBQOB
						.setLineEnd(rowSeparator_tBigQueryOutput_1_tBQOB);
				csvWriter_tBigQueryOutput_1_tBQOB
						.setEscapeChar(escapeChar_tBigQueryOutput_1_tBQOB);
				csvWriter_tBigQueryOutput_1_tBQOB
						.setQuoteChar(textEnclosure_tBigQueryOutput_1_tBQOB);
				csvWriter_tBigQueryOutput_1_tBQOB
						.setQuoteStatus(com.talend.csv.CSVWriter.QuoteStatus.AUTO);

				/**
				 * [tBigQueryOutput_1_tBQOB begin ] stop
				 */

				/**
				 * [tMap_1 begin ] start
				 */

				ok_Hash.put("tMap_1", false);
				start_Hash.put("tMap_1", System.currentTimeMillis());

				currentComponent = "tMap_1";

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null) {

						runStat.updateStatOnConnection("row2" + iterateId, 0, 0);

					}
				}

				int tos_count_tMap_1 = 0;

				if (log.isDebugEnabled())
					log.debug("tMap_1 - " + ("Start to work."));
				class BytesLimit65535_tMap_1 {
					public void limitLog4jByte() throws Exception {

						StringBuilder log4jParamters_tMap_1 = new StringBuilder();
						log4jParamters_tMap_1.append("Parameters:");
						log4jParamters_tMap_1.append("LINK_STYLE" + " = "
								+ "AUTO");
						log4jParamters_tMap_1.append(" | ");
						log4jParamters_tMap_1.append("TEMPORARY_DATA_DIRECTORY"
								+ " = " + "");
						log4jParamters_tMap_1.append(" | ");
						log4jParamters_tMap_1.append("ROWS_BUFFER_SIZE" + " = "
								+ "2000000");
						log4jParamters_tMap_1.append(" | ");
						log4jParamters_tMap_1
								.append("CHANGE_HASH_AND_EQUALS_FOR_BIGDECIMAL"
										+ " = " + "true");
						log4jParamters_tMap_1.append(" | ");
						if (log.isDebugEnabled())
							log.debug("tMap_1 - " + (log4jParamters_tMap_1));
					}
				}

				new BytesLimit65535_tMap_1().limitLog4jByte();

				// ###############################
				// # Lookup's keys initialization
				int count_row2_tMap_1 = 0;

				// ###############################

				// ###############################
				// # Vars initialization
				class Var__tMap_1__Struct {
				}
				Var__tMap_1__Struct Var__tMap_1 = new Var__tMap_1__Struct();
				// ###############################

				// ###############################
				// # Outputs initialization
				int count_out_tMap_1 = 0;

				outStruct out_tmp = new outStruct();
				// ###############################

				/**
				 * [tMap_1 begin ] stop
				 */

				/**
				 * [tBigQueryInput_1 begin ] start
				 */

				ok_Hash.put("tBigQueryInput_1", false);
				start_Hash.put("tBigQueryInput_1", System.currentTimeMillis());

				currentComponent = "tBigQueryInput_1";

				int tos_count_tBigQueryInput_1 = 0;

				if (log.isDebugEnabled())
					log.debug("tBigQueryInput_1 - " + ("Start to work."));
				class BytesLimit65535_tBigQueryInput_1 {
					public void limitLog4jByte() throws Exception {

						StringBuilder log4jParamters_tBigQueryInput_1 = new StringBuilder();
						log4jParamters_tBigQueryInput_1.append("Parameters:");
						log4jParamters_tBigQueryInput_1
								.append("CLIENT_ID"
										+ " = "
										+ "\"172756275673-8mko1edktvnusf30t1e6rqdn7rgavktk.apps.googleusercontent.com\"");
						log4jParamters_tBigQueryInput_1.append(" | ");
						log4jParamters_tBigQueryInput_1
								.append("CLIENT_SECRET"
										+ " = "
										+ String.valueOf(
												"343cbd2c889a14012570c315c22ba542ea7b9453dc715fe2f4f7aba1746784ea")
												.substring(0, 4) + "...");
						log4jParamters_tBigQueryInput_1.append(" | ");
						log4jParamters_tBigQueryInput_1.append("PROJECT_ID"
								+ " = " + "\"coca-cola-data-lake\"");
						log4jParamters_tBigQueryInput_1.append(" | ");
						log4jParamters_tBigQueryInput_1
								.append("AUTHORIZATION_CODE"
										+ " = "
										+ "\"4/6gBlGFRRFpATvVP8_sQYVP4jKmxlsTGUsvS94SPwxJ457bd_n_Vfe64\"");
						log4jParamters_tBigQueryInput_1.append(" | ");
						log4jParamters_tBigQueryInput_1.append("QUERY" + " = "
								+ "\"select * from EDW.TREQUIPMENTTYPE\"");
						log4jParamters_tBigQueryInput_1.append(" | ");
						log4jParamters_tBigQueryInput_1.append("RESULT_SIZE"
								+ " = " + "SMALL");
						log4jParamters_tBigQueryInput_1.append(" | ");
						log4jParamters_tBigQueryInput_1
								.append("TOKEN_NAME"
										+ " = "
										+ "\"/Users/isa/Documents/gifmis_bitbucket/GifMis Analytics/Talend Workspace/token.properties\"");
						log4jParamters_tBigQueryInput_1.append(" | ");
						log4jParamters_tBigQueryInput_1
								.append("ADVANCED_SEPARATOR" + " = " + "false");
						log4jParamters_tBigQueryInput_1.append(" | ");
						log4jParamters_tBigQueryInput_1.append("ENCODING"
								+ " = " + "\"ISO-8859-15\"");
						log4jParamters_tBigQueryInput_1.append(" | ");
						if (log.isDebugEnabled())
							log.debug("tBigQueryInput_1 - "
									+ (log4jParamters_tBigQueryInput_1));
					}
				}

				new BytesLimit65535_tBigQueryInput_1().limitLog4jByte();

				final String CLIENT_ID_tBigQueryInput_1 = "172756275673-8mko1edktvnusf30t1e6rqdn7rgavktk.apps.googleusercontent.com";

				final String decryptedPassword_tBigQueryInput_1 = routines.system.PasswordEncryptUtil
						.decryptPassword("343cbd2c889a14012570c315c22ba542ea7b9453dc715fe2f4f7aba1746784ea");

				final String CLIENT_SECRET_tBigQueryInput_1 = "{\"web\": {\"client_id\": \""
						+ "172756275673-8mko1edktvnusf30t1e6rqdn7rgavktk.apps.googleusercontent.com"
						+ "\",\"client_secret\": \""
						+ decryptedPassword_tBigQueryInput_1
						+ "\",\"auth_uri\": \"https://accounts.google.com/o/oauth2/auth\",\"token_uri\": \"https://accounts.google.com/o/oauth2/token\"}}";
				final String PROJECT_ID_tBigQueryInput_1 = "coca-cola-data-lake";

				// Static variables for API scope, callback URI, and HTTP/JSON
				// functions
				final List<String> SCOPES_tBigQueryInput_1 = java.util.Arrays
						.asList("https://www.googleapis.com/auth/bigquery");
				final String REDIRECT_URI_tBigQueryInput_1 = "urn:ietf:wg:oauth:2.0:oob";
				final com.google.api.client.http.HttpTransport TRANSPORT_tBigQueryInput_1 = new com.google.api.client.http.javanet.NetHttpTransport();
				final com.google.api.client.json.JsonFactory JSON_FACTORY_tBigQueryInput_1 = new com.google.api.client.json.jackson2.JacksonFactory();

				com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets clientSecrets_tBigQueryInput_1 = com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets
						.load(new com.google.api.client.json.jackson2.JacksonFactory(),
								new java.io.InputStreamReader(
										new java.io.ByteArrayInputStream(
												CLIENT_SECRET_tBigQueryInput_1
														.getBytes())));

				com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow flow_tBigQueryInput_1 = null;
				com.google.api.services.bigquery.Bigquery bigqueryclient_tBigQueryInput_1 = null;
				long nb_line_tBigQueryInput_1 = 0;

				log.info("tBigQueryInput_1 - Service Account Scopes [https://www.googleapis.com/auth/bigquery]");
				log.info("tBigQueryInput_1 - Redirect uris [urn:ietf:wg:oauth:2.0:oob]");
				log.info("tBigQueryInput_1 - Attempt to load existing refresh token.");

				// Attempt to load existing refresh token
				String tokenFile_tBigQueryInput_1 = "/Users/isa/Documents/gifmis_bitbucket/GifMis Analytics/Talend Workspace/token.properties";
				java.util.Properties properties_tBigQueryInput_1 = new java.util.Properties();
				try {
					java.io.FileInputStream inputStream_tBigQueryInput_1 = new java.io.FileInputStream(
							tokenFile_tBigQueryInput_1);
					properties_tBigQueryInput_1
							.load(inputStream_tBigQueryInput_1);
					inputStream_tBigQueryInput_1.close();
				} catch (java.io.FileNotFoundException e_tBigQueryInput_1) {

					log.warn("tBigQueryInput_1 - "
							+ e_tBigQueryInput_1.getMessage());

				} catch (java.io.IOException ee_tBigQueryInput_1) {

					log.warn("tBigQueryInput_1 - "
							+ ee_tBigQueryInput_1.getMessage());

				}
				String storedRefreshToken_tBigQueryInput_1 = (String) properties_tBigQueryInput_1
						.get("refreshtoken");

				// Check to see if the an existing refresh token was loaded.
				// If so, create a credential and call refreshToken() to get a
				// new
				// access token.
				if (storedRefreshToken_tBigQueryInput_1 != null) {
					// Request a new Access token using the refresh token.
					com.google.api.client.googleapis.auth.oauth2.GoogleCredential credential_tBigQueryInput_1 = new com.google.api.client.googleapis.auth.oauth2.GoogleCredential.Builder()
							.setTransport(TRANSPORT_tBigQueryInput_1)
							.setJsonFactory(JSON_FACTORY_tBigQueryInput_1)
							.setClientSecrets(clientSecrets_tBigQueryInput_1)
							.build()
							.setFromTokenResponse(
									new com.google.api.client.auth.oauth2.TokenResponse()
											.setRefreshToken(storedRefreshToken_tBigQueryInput_1));

					credential_tBigQueryInput_1.refreshToken();

					log.info("tBigQueryInput_1 - An existing refresh token was loaded.");

					bigqueryclient_tBigQueryInput_1 = new com.google.api.services.bigquery.Bigquery.Builder(
							new com.google.api.client.http.javanet.NetHttpTransport(),
							new com.google.api.client.json.jackson2.JacksonFactory(),
							credential_tBigQueryInput_1).setApplicationName(
							"Talend").build();
				} else {

					log.info("tBigQueryInput_1 - The refresh token does not exist.");

					String authorizationCode_tBigQueryInput_1 = "4/6gBlGFRRFpATvVP8_sQYVP4jKmxlsTGUsvS94SPwxJ457bd_n_Vfe64";
					if (authorizationCode_tBigQueryInput_1 == null
							|| "".equals(authorizationCode_tBigQueryInput_1)
							|| "\"\""
									.equals(authorizationCode_tBigQueryInput_1)) {
						String authorizeUrl_tBigQueryInput_1 = new com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeRequestUrl(
								clientSecrets_tBigQueryInput_1,
								REDIRECT_URI_tBigQueryInput_1,
								SCOPES_tBigQueryInput_1).setState("").build();

						System.out
								.println("Paste this URL into a web browser to authorize BigQuery Access:\n"
										+ authorizeUrl_tBigQueryInput_1);

						log.warn("tBigQueryInput_1 - Paste this URL into a web browser to authorize BigQuery Access:\n"
								+ authorizeUrl_tBigQueryInput_1);

						throw new java.lang.Exception(
								"Authorization Code error");
					} else {

						log.info("tBigQueryInput_1 - Exchange the auth code for an access token and refesh token.");

						// Exchange the auth code for an access token and refesh
						// token
						if (flow_tBigQueryInput_1 == null) {
							flow_tBigQueryInput_1 = new com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow.Builder(
									new com.google.api.client.http.javanet.NetHttpTransport(),
									new com.google.api.client.json.jackson2.JacksonFactory(),
									clientSecrets_tBigQueryInput_1,
									SCOPES_tBigQueryInput_1)
									.setAccessType("offline")
									.setApprovalPrompt("force").build();
						}
						com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse response_tBigQueryInput_1 = flow_tBigQueryInput_1
								.newTokenRequest(
										authorizationCode_tBigQueryInput_1)
								.setRedirectUri(REDIRECT_URI_tBigQueryInput_1)
								.execute();
						com.google.api.client.auth.oauth2.Credential credential_tBigQueryInput_1 = flow_tBigQueryInput_1
								.createAndStoreCredential(
										response_tBigQueryInput_1, null);

						log.info("tBigQueryInput_1 - Store the refresh token for future use.");

						// Store the refresh token for future use.
						java.util.Properties storeProperties_tBigQueryInput_1 = new java.util.Properties();
						storeProperties_tBigQueryInput_1.setProperty(
								"refreshtoken",
								credential_tBigQueryInput_1.getRefreshToken());
						java.io.FileOutputStream outputStream_tBigQueryInput_1 = new java.io.FileOutputStream(
								tokenFile_tBigQueryInput_1);
						storeProperties_tBigQueryInput_1.store(
								outputStream_tBigQueryInput_1, null);
						if (outputStream_tBigQueryInput_1 != null) {
							outputStream_tBigQueryInput_1.close();
						}

						bigqueryclient_tBigQueryInput_1 = new com.google.api.services.bigquery.Bigquery.Builder(
								new com.google.api.client.http.javanet.NetHttpTransport(),
								new com.google.api.client.json.jackson2.JacksonFactory(),
								credential_tBigQueryInput_1).build();
					}
				}

				class BigQueryUtil_tBigQueryInput_1 {
					String projectId;
					com.google.api.services.bigquery.Bigquery bigqueryclient = null;
					String tokenFile;
					boolean useLargeResult = false;
					String tempDataset;
					String tempTable;

					public BigQueryUtil_tBigQueryInput_1(
							String projectId,
							com.google.api.services.bigquery.Bigquery bigqueryclient,
							String tokenFile) {
						this.projectId = projectId;
						this.bigqueryclient = bigqueryclient;
						this.tokenFile = tokenFile;
					}

					private String genTempName(String prefix) {
						return "temp_"
								+ prefix
								+ java.util.UUID.randomUUID().toString()
										.replaceAll("-", "")
								+ "tBigQueryInput_1".toLowerCase()
										.replaceAll("[^a-z0-9]", "0")
										.replaceAll("^[^a-z]", "a")
								+ Integer
										.toHexString(java.util.concurrent.ThreadLocalRandom
												.current().nextInt());
					}

					public void cleanup() throws Exception {
						if (useLargeResult) {
							bigqueryclient.tables()
									.delete(projectId, tempDataset, tempTable)
									.execute();
							bigqueryclient.datasets()
									.delete(projectId, tempDataset).execute();
						}
					}

					private void createDataset(String location)
							throws Exception {
						com.google.api.services.bigquery.model.Dataset dataset = new com.google.api.services.bigquery.model.Dataset()
								.setDatasetReference(new com.google.api.services.bigquery.model.DatasetReference()
										.setProjectId(projectId).setDatasetId(
												tempDataset));
						String description = "Dataset for BigQuery query job temporary table";
						dataset.setFriendlyName(description);
						dataset.setDescription(description);
						bigqueryclient.datasets().insert(projectId, dataset)
								.execute();
					}

					public com.google.api.services.bigquery.model.Job executeQuery(
							String query, boolean useLargeResult)
							throws Exception {
						com.google.api.services.bigquery.model.JobConfigurationQuery queryConfig = new com.google.api.services.bigquery.model.JobConfigurationQuery();
						queryConfig.setQuery(query);
						if (useLargeResult) {
							this.useLargeResult = true;
							tempDataset = genTempName("dataset");
							tempTable = genTempName("table");
							createDataset(null);
							queryConfig.setAllowLargeResults(true);
							queryConfig
									.setDestinationTable(new com.google.api.services.bigquery.model.TableReference()
											.setProjectId(projectId)
											.setDatasetId(tempDataset)
											.setTableId(tempTable));
						}

						com.google.api.services.bigquery.model.JobConfiguration config = new com.google.api.services.bigquery.model.JobConfiguration();
						config.setQuery(queryConfig);

						com.google.api.services.bigquery.model.Job job = new com.google.api.services.bigquery.model.Job();
						job.setConfiguration(config);

						com.google.api.services.bigquery.model.Job insert = null;
						com.google.api.services.bigquery.model.JobReference jobId = null;
						try {
							insert = bigqueryclient.jobs()
									.insert(projectId, job).execute();
							jobId = insert.getJobReference();
						} catch (com.google.api.client.googleapis.json.GoogleJsonResponseException e) {
							if (tokenFile != null) {
								try {
									java.io.File f = new java.io.File(tokenFile);
									boolean isRemoved = f.delete();
									if (isRemoved) {

										log.error("tBigQueryInput_1 - Unable to connect. This might come from the token expiration. Execute again the job with an empty authorization code.");

									} else {
										throw new java.lang.Exception();
									}
								} catch (java.lang.Exception ee) {

									log.error("tBigQueryInput_1 - Unable to connect. This might come from the token expiration. Remove the file "
											+ tokenFile
											+ " Execute again the job with an empty authorization code.");

								}
							}
							throw e;
						}

						log.info("tBigQueryInput_1 - Wait for query execution");

						// wait for query execution
						while (true) {
							com.google.api.services.bigquery.model.Job pollJob = bigqueryclient
									.jobs().get(projectId, jobId.getJobId())
									.execute();
							com.google.api.services.bigquery.model.JobStatus status = pollJob
									.getStatus();
							if (status.getState().equals("DONE")) {
								com.google.api.services.bigquery.model.ErrorProto errorProto = status
										.getErrorResult();
								if (errorProto != null) {// job failed, handle
															// it

									// Do not throw exception to avoid behavior
									// changed(because it may throw "duplicate"
									// exception which do not throw before);

									log.error("tBigQueryInput_1 - Reason: "
											+ errorProto.getReason()
											+ "\nMessage: "
											+ errorProto.getMessage());

								}// else job successful
								break;
							}
							// Pause execution for one second before polling job
							// status again, to
							// reduce unnecessary calls to the BigQUery API and
							// lower overall
							// application bandwidth.
							Thread.sleep(1000);
						}

						return insert;
					}

				}

				// Start a Query Job
				String querySql_tBigQueryInput_1 = "select * from EDW.TREQUIPMENTTYPE";
				System.out.format("Running Query : %s\n",
						querySql_tBigQueryInput_1);

				log.debug("tBigQueryInput_1 - Running Query: "
						+ querySql_tBigQueryInput_1);

				BigQueryUtil_tBigQueryInput_1 bigQueryUtil_tBigQueryInput_1 = new BigQueryUtil_tBigQueryInput_1(
						PROJECT_ID_tBigQueryInput_1,
						bigqueryclient_tBigQueryInput_1,
						tokenFile_tBigQueryInput_1);

				log.info("tBigQueryInput_1 - Try without allow large results flag");

				com.google.api.services.bigquery.model.Job insert_tBigQueryInput_1 = bigQueryUtil_tBigQueryInput_1
						.executeQuery(querySql_tBigQueryInput_1, false);

				log.info("tBigQueryInput_1 - Retrieving records from dataset.");

				String pageToken_tBigQueryInput_1 = null;
				while (true) {
					// Fetch Results
					com.google.api.services.bigquery.model.TableDataList dataList_tBigQueryInput_1 = bigqueryclient_tBigQueryInput_1
							.tabledata()
							.list(PROJECT_ID_tBigQueryInput_1,
									insert_tBigQueryInput_1.getConfiguration()
											.getQuery().getDestinationTable()
											.getDatasetId(),
									insert_tBigQueryInput_1.getConfiguration()
											.getQuery().getDestinationTable()
											.getTableId())
							.setPageToken(pageToken_tBigQueryInput_1).execute();

					List<com.google.api.services.bigquery.model.TableRow> rows_tBigQueryInput_1 = dataList_tBigQueryInput_1
							.getRows();

					if (rows_tBigQueryInput_1 == null) {
						// Means there is no record.
						rows_tBigQueryInput_1 = new java.util.ArrayList<com.google.api.services.bigquery.model.TableRow>();
					}

					for (com.google.api.services.bigquery.model.TableRow row_tBigQueryInput_1 : rows_tBigQueryInput_1) {
						java.util.List<com.google.api.services.bigquery.model.TableCell> field_tBigQueryInput_1 = row_tBigQueryInput_1
								.getF();
						Object value_tBigQueryInput_1 = null;
						nb_line_tBigQueryInput_1++;

						value_tBigQueryInput_1 = field_tBigQueryInput_1.get(0)
								.getV();
						if (com.google.api.client.util.Data
								.isNull(value_tBigQueryInput_1))
							value_tBigQueryInput_1 = null;
						if (value_tBigQueryInput_1 != null) {

							row2.EquipmentTypeCode = value_tBigQueryInput_1
									.toString();

						} else {
							row2.EquipmentTypeCode = null;
						}

						value_tBigQueryInput_1 = field_tBigQueryInput_1.get(1)
								.getV();
						if (com.google.api.client.util.Data
								.isNull(value_tBigQueryInput_1))
							value_tBigQueryInput_1 = null;
						if (value_tBigQueryInput_1 != null) {

							row2.LanguageCode = value_tBigQueryInput_1
									.toString();

						} else {
							row2.LanguageCode = null;
						}

						value_tBigQueryInput_1 = field_tBigQueryInput_1.get(2)
								.getV();
						if (com.google.api.client.util.Data
								.isNull(value_tBigQueryInput_1))
							value_tBigQueryInput_1 = null;
						if (value_tBigQueryInput_1 != null) {

							row2.EquipmentTypeName = value_tBigQueryInput_1
									.toString();

						} else {
							row2.EquipmentTypeName = null;
						}

						log.debug("tBigQueryInput_1 - Retrieving the record "
								+ (nb_line_tBigQueryInput_1) + ".");

						/**
						 * [tBigQueryInput_1 begin ] stop
						 */

						/**
						 * [tBigQueryInput_1 main ] start
						 */

						currentComponent = "tBigQueryInput_1";

						tos_count_tBigQueryInput_1++;

						/**
						 * [tBigQueryInput_1 main ] stop
						 */

						/**
						 * [tBigQueryInput_1 process_data_begin ] start
						 */

						currentComponent = "tBigQueryInput_1";

						/**
						 * [tBigQueryInput_1 process_data_begin ] stop
						 */

						/**
						 * [tMap_1 main ] start
						 */

						currentComponent = "tMap_1";

						// row2
						// row2

						if (execStat) {
							runStat.updateStatOnConnection("row2" + iterateId,
									1, 1);
						}

						if (log.isTraceEnabled()) {
							log.trace("row2 - "
									+ (row2 == null ? "" : row2.toLogString()));
						}

						boolean hasCasePrimitiveKeyWithNull_tMap_1 = false;

						// ###############################
						// # Input tables (lookups)
						boolean rejectedInnerJoin_tMap_1 = false;
						boolean mainRowRejected_tMap_1 = false;

						// ###############################
						{ // start of Var scope

							// ###############################
							// # Vars tables

							Var__tMap_1__Struct Var = Var__tMap_1;// ###############################
							// ###############################
							// # Output tables

							out = null;

							// # Output table : 'out'
							count_out_tMap_1++;

							out_tmp.EquipmentTypeCode = row2.EquipmentTypeCode;
							out_tmp.LanguageCode = row2.LanguageCode;
							out_tmp.EquipmentTypeName = row2.EquipmentTypeName;
							out = out_tmp;
							log.debug("tMap_1 - Outputting the record "
									+ count_out_tMap_1
									+ " of the output table 'out'.");

							// ###############################

						} // end of Var scope

						rejectedInnerJoin_tMap_1 = false;

						tos_count_tMap_1++;

						/**
						 * [tMap_1 main ] stop
						 */

						/**
						 * [tMap_1 process_data_begin ] start
						 */

						currentComponent = "tMap_1";

						/**
						 * [tMap_1 process_data_begin ] stop
						 */
						// Start of branch "out"
						if (out != null) {

							/**
							 * [tBigQueryOutput_1_tBQOB main ] start
							 */

							currentVirtualComponent = "tBigQueryOutput_1";

							currentComponent = "tBigQueryOutput_1_tBQOB";

							// out
							// out

							if (execStat) {
								runStat.updateStatOnConnection("out"
										+ iterateId, 1, 1);
							}

							if (log.isTraceEnabled()) {
								log.trace("out - "
										+ (out == null ? "" : out.toLogString()));
							}

							String[] row_tBigQueryOutput_1_tBQOB = new String[] {
									"\\N", "\\N", "\\N", }; // empty value must
															// be NULL('\N' in
															// bulk file)

							if (out.EquipmentTypeCode != null) {

								row_tBigQueryOutput_1_tBQOB[0] = out.EquipmentTypeCode;

							} else {
								row_tBigQueryOutput_1_tBQOB[0] = null;
							}

							if (out.LanguageCode != null) {

								row_tBigQueryOutput_1_tBQOB[1] = out.LanguageCode;

							} else {
								row_tBigQueryOutput_1_tBQOB[1] = null;
							}

							if (out.EquipmentTypeName != null) {

								row_tBigQueryOutput_1_tBQOB[2] = out.EquipmentTypeName;

							} else {
								row_tBigQueryOutput_1_tBQOB[2] = null;
							}

							csvWriter_tBigQueryOutput_1_tBQOB.writeNextEnhance(
									row_tBigQueryOutput_1_tBQOB, "");
							nb_line_tBigQueryOutput_1_tBQOB++;
							log.debug("tBigQueryOutput_1_tBQOB - Writing the record "
									+ nb_line_tBigQueryOutput_1_tBQOB + ".");

							tos_count_tBigQueryOutput_1_tBQOB++;

							/**
							 * [tBigQueryOutput_1_tBQOB main ] stop
							 */

							/**
							 * [tBigQueryOutput_1_tBQOB process_data_begin ]
							 * start
							 */

							currentVirtualComponent = "tBigQueryOutput_1";

							currentComponent = "tBigQueryOutput_1_tBQOB";

							/**
							 * [tBigQueryOutput_1_tBQOB process_data_begin ]
							 * stop
							 */

							/**
							 * [tBigQueryOutput_1_tBQOB process_data_end ] start
							 */

							currentVirtualComponent = "tBigQueryOutput_1";

							currentComponent = "tBigQueryOutput_1_tBQOB";

							/**
							 * [tBigQueryOutput_1_tBQOB process_data_end ] stop
							 */

						} // End of branch "out"

						/**
						 * [tMap_1 process_data_end ] start
						 */

						currentComponent = "tMap_1";

						/**
						 * [tMap_1 process_data_end ] stop
						 */

						/**
						 * [tBigQueryInput_1 process_data_end ] start
						 */

						currentComponent = "tBigQueryInput_1";

						/**
						 * [tBigQueryInput_1 process_data_end ] stop
						 */

						/**
						 * [tBigQueryInput_1 end ] start
						 */

						currentComponent = "tBigQueryInput_1";

					}
					pageToken_tBigQueryInput_1 = dataList_tBigQueryInput_1
							.getPageToken();
					if (null == pageToken_tBigQueryInput_1) {
						break;
					}
				}
				bigQueryUtil_tBigQueryInput_1.cleanup();

				log.debug("tBigQueryInput_1 - Retrieved records count: "
						+ nb_line_tBigQueryInput_1 + " .");

				if (log.isDebugEnabled())
					log.debug("tBigQueryInput_1 - " + ("Done."));

				ok_Hash.put("tBigQueryInput_1", true);
				end_Hash.put("tBigQueryInput_1", System.currentTimeMillis());

				/**
				 * [tBigQueryInput_1 end ] stop
				 */

				/**
				 * [tMap_1 end ] start
				 */

				currentComponent = "tMap_1";

				// ###############################
				// # Lookup hashes releasing
				// ###############################
				log.debug("tMap_1 - Written records count in the table 'out': "
						+ count_out_tMap_1 + ".");

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null
							|| !((Boolean) resourceMap.get("inIterateVComp"))) {
						runStat.updateStatOnConnection("row2" + iterateId, 2, 0);
					}
				}

				if (log.isDebugEnabled())
					log.debug("tMap_1 - " + ("Done."));

				ok_Hash.put("tMap_1", true);
				end_Hash.put("tMap_1", System.currentTimeMillis());

				/**
				 * [tMap_1 end ] stop
				 */

				/**
				 * [tBigQueryOutput_1_tBQOB end ] start
				 */

				currentVirtualComponent = "tBigQueryOutput_1";

				currentComponent = "tBigQueryOutput_1_tBQOB";

				if (csvWriter_tBigQueryOutput_1_tBQOB != null) {
					csvWriter_tBigQueryOutput_1_tBQOB.close();
				}
				resourceMap.put("finish_tBigQueryOutput_1_tBQOB", true);
				globalMap.put("tBigQueryOutput_1_tBQOB_NB_LINE",
						nb_line_tBigQueryOutput_1_tBQOB);

				log.debug("tBigQueryOutput_1_tBQOB - Written records count: "
						+ nb_line_tBigQueryOutput_1_tBQOB + " .");

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null
							|| !((Boolean) resourceMap.get("inIterateVComp"))) {
						runStat.updateStatOnConnection("out" + iterateId, 2, 0);
					}
				}

				if (log.isDebugEnabled())
					log.debug("tBigQueryOutput_1_tBQOB - " + ("Done."));

				ok_Hash.put("tBigQueryOutput_1_tBQOB", true);
				end_Hash.put("tBigQueryOutput_1_tBQOB",
						System.currentTimeMillis());

				/**
				 * [tBigQueryOutput_1_tBQOB end ] stop
				 */

				/**
				 * [tBigQueryOutput_1_tBQBE begin ] start
				 */

				ok_Hash.put("tBigQueryOutput_1_tBQBE", false);
				start_Hash.put("tBigQueryOutput_1_tBQBE",
						System.currentTimeMillis());

				currentVirtualComponent = "tBigQueryOutput_1";

				currentComponent = "tBigQueryOutput_1_tBQBE";

				int tos_count_tBigQueryOutput_1_tBQBE = 0;

				if (log.isDebugEnabled())
					log.debug("tBigQueryOutput_1_tBQBE - " + ("Start to work."));
				class BytesLimit65535_tBigQueryOutput_1_tBQBE {
					public void limitLog4jByte() throws Exception {

						StringBuilder log4jParamters_tBigQueryOutput_1_tBQBE = new StringBuilder();
						log4jParamters_tBigQueryOutput_1_tBQBE
								.append("Parameters:");
						log4jParamters_tBigQueryOutput_1_tBQBE
								.append("CLIENT_ID"
										+ " = "
										+ "\"172756275673-8mko1edktvnusf30t1e6rqdn7rgavktk.apps.googleusercontent.com\"");
						log4jParamters_tBigQueryOutput_1_tBQBE.append(" | ");
						log4jParamters_tBigQueryOutput_1_tBQBE
								.append("CLIENT_SECRET"
										+ " = "
										+ String.valueOf(
												"343cbd2c889a14012570c315c22ba542ea7b9453dc715fe2f4f7aba1746784ea")
												.substring(0, 4) + "...");
						log4jParamters_tBigQueryOutput_1_tBQBE.append(" | ");
						log4jParamters_tBigQueryOutput_1_tBQBE
								.append("PROJECT_ID" + " = "
										+ "\"coca-cola-data-lake\"");
						log4jParamters_tBigQueryOutput_1_tBQBE.append(" | ");
						log4jParamters_tBigQueryOutput_1_tBQBE
								.append("AUTHORIZATION_CODE"
										+ " = "
										+ "\"4/6gBlGFRRFpATvVP8_sQYVP4jKmxlsTGUsvS94SPwxJ457bd_n_Vfe64\"");
						log4jParamters_tBigQueryOutput_1_tBQBE.append(" | ");
						log4jParamters_tBigQueryOutput_1_tBQBE.append("DATASET"
								+ " = " + "\"EDW\"");
						log4jParamters_tBigQueryOutput_1_tBQBE.append(" | ");
						log4jParamters_tBigQueryOutput_1_tBQBE.append("TABLE"
								+ " = " + "\"Dim_EquipmentTypeText\"");
						log4jParamters_tBigQueryOutput_1_tBQBE.append(" | ");
						log4jParamters_tBigQueryOutput_1_tBQBE
								.append("CREATE_TABLE_IF_NOT_EXIST" + " = "
										+ "true");
						log4jParamters_tBigQueryOutput_1_tBQBE.append(" | ");
						log4jParamters_tBigQueryOutput_1_tBQBE
								.append("ACTION_ON_DATA" + " = " + "EMPTY");
						log4jParamters_tBigQueryOutput_1_tBQBE.append(" | ");
						log4jParamters_tBigQueryOutput_1_tBQBE
								.append("BULK_FILE_ALREADY_EXIST" + " = "
										+ "false");
						log4jParamters_tBigQueryOutput_1_tBQBE.append(" | ");
						log4jParamters_tBigQueryOutput_1_tBQBE
								.append("GS_ACCESS_KEY" + " = " + "\"\"");
						log4jParamters_tBigQueryOutput_1_tBQBE.append(" | ");
						log4jParamters_tBigQueryOutput_1_tBQBE
								.append("GS_SECRET_KEY"
										+ " = "
										+ String.valueOf("f4f7aba1746784ea")
												.substring(0, 4) + "...");
						log4jParamters_tBigQueryOutput_1_tBQBE.append(" | ");
						log4jParamters_tBigQueryOutput_1_tBQBE
								.append("GS_LOCAL_FILE"
										+ " = "
										+ "\"/Users/isa/Documents/gifmis_bitbucket/GifMis Analytics/Talend Workspace/biquery_bulk.txt\"");
						log4jParamters_tBigQueryOutput_1_tBQBE.append(" | ");
						log4jParamters_tBigQueryOutput_1_tBQBE
								.append("BUCKET_NAME" + " = " + "\"\"");
						log4jParamters_tBigQueryOutput_1_tBQBE.append(" | ");
						log4jParamters_tBigQueryOutput_1_tBQBE.append("GS_FILE"
								+ " = " + "\"gs://\"");
						log4jParamters_tBigQueryOutput_1_tBQBE.append(" | ");
						log4jParamters_tBigQueryOutput_1_tBQBE
								.append("GS_FILE_HEADER" + " = " + "0");
						log4jParamters_tBigQueryOutput_1_tBQBE.append(" | ");
						log4jParamters_tBigQueryOutput_1_tBQBE
								.append("DIE_ON_ERROR" + " = " + "false");
						log4jParamters_tBigQueryOutput_1_tBQBE.append(" | ");
						log4jParamters_tBigQueryOutput_1_tBQBE
								.append("TOKEN_NAME"
										+ " = "
										+ "\"/Users/isa/Documents/gifmis_bitbucket/GifMis Analytics/Talend Workspace/token.properties\"");
						log4jParamters_tBigQueryOutput_1_tBQBE.append(" | ");
						log4jParamters_tBigQueryOutput_1_tBQBE
								.append("SET_FIELD_DELIMITER" + " = " + "true");
						log4jParamters_tBigQueryOutput_1_tBQBE.append(" | ");
						log4jParamters_tBigQueryOutput_1_tBQBE
								.append("FIELD_DELIMITER" + " = " + "\";\"");
						log4jParamters_tBigQueryOutput_1_tBQBE.append(" | ");
						log4jParamters_tBigQueryOutput_1_tBQBE
								.append("ENCODING" + " = " + "\"ISO-8859-15\"");
						log4jParamters_tBigQueryOutput_1_tBQBE.append(" | ");
						if (log.isDebugEnabled())
							log.debug("tBigQueryOutput_1_tBQBE - "
									+ (log4jParamters_tBigQueryOutput_1_tBQBE));
					}
				}

				new BytesLimit65535_tBigQueryOutput_1_tBQBE().limitLog4jByte();

				final String CLIENT_ID_tBigQueryOutput_1_tBQBE = "172756275673-8mko1edktvnusf30t1e6rqdn7rgavktk.apps.googleusercontent.com";

				final String decryptedPassword_tBigQueryOutput_1_tBQBE = routines.system.PasswordEncryptUtil
						.decryptPassword("343cbd2c889a14012570c315c22ba542ea7b9453dc715fe2f4f7aba1746784ea");

				final String CLIENT_SECRET_tBigQueryOutput_1_tBQBE = "{\"web\": {\"client_id\": \""
						+ "172756275673-8mko1edktvnusf30t1e6rqdn7rgavktk.apps.googleusercontent.com"
						+ "\",\"client_secret\": \""
						+ decryptedPassword_tBigQueryOutput_1_tBQBE
						+ "\",\"auth_uri\": \"https://accounts.google.com/o/oauth2/auth\",\"token_uri\": \"https://accounts.google.com/o/oauth2/token\"}}";
				final String PROJECT_ID_tBigQueryOutput_1_tBQBE = "coca-cola-data-lake";

				// Static variables for API scope, callback URI, and HTTP/JSON
				// functions
				final List<String> SCOPES_tBigQueryOutput_1_tBQBE = java.util.Arrays
						.asList("https://www.googleapis.com/auth/bigquery");
				final String REDIRECT_URI_tBigQueryOutput_1_tBQBE = "urn:ietf:wg:oauth:2.0:oob";
				final com.google.api.client.http.HttpTransport TRANSPORT_tBigQueryOutput_1_tBQBE = new com.google.api.client.http.javanet.NetHttpTransport();
				final com.google.api.client.json.JsonFactory JSON_FACTORY_tBigQueryOutput_1_tBQBE = new com.google.api.client.json.jackson2.JacksonFactory();

				com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets clientSecrets_tBigQueryOutput_1_tBQBE = com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets
						.load(new com.google.api.client.json.jackson2.JacksonFactory(),
								new java.io.InputStreamReader(
										new java.io.ByteArrayInputStream(
												CLIENT_SECRET_tBigQueryOutput_1_tBQBE
														.getBytes())));

				com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow flow_tBigQueryOutput_1_tBQBE = null;
				com.google.api.services.bigquery.Bigquery bigqueryclient_tBigQueryOutput_1_tBQBE = null;
				long nb_line_tBigQueryOutput_1_tBQBE = 0;

				log.info("tBigQueryOutput_1_tBQBE - Service Account Scopes [https://www.googleapis.com/auth/bigquery]");
				log.info("tBigQueryOutput_1_tBQBE - Redirect uris [urn:ietf:wg:oauth:2.0:oob]");
				log.info("tBigQueryOutput_1_tBQBE - Attempt to load existing refresh token");

				// Attempt to load existing refresh token
				String tokenFile_tBigQueryOutput_1_tBQBE = "/Users/isa/Documents/gifmis_bitbucket/GifMis Analytics/Talend Workspace/token.properties";
				java.util.Properties properties_tBigQueryOutput_1_tBQBE = new java.util.Properties();
				try {
					java.io.FileInputStream inputStream_tBigQueryOutput_1_tBQBE = new java.io.FileInputStream(
							tokenFile_tBigQueryOutput_1_tBQBE);
					properties_tBigQueryOutput_1_tBQBE
							.load(inputStream_tBigQueryOutput_1_tBQBE);
					inputStream_tBigQueryOutput_1_tBQBE.close();
				} catch (java.io.FileNotFoundException e_tBigQueryOutput_1_tBQBE) {

					log.warn("tBigQueryOutput_1_tBQBE - "
							+ e_tBigQueryOutput_1_tBQBE.getMessage());

				} catch (java.io.IOException ee_tBigQueryOutput_1_tBQBE) {

					log.warn("tBigQueryOutput_1_tBQBE - "
							+ ee_tBigQueryOutput_1_tBQBE.getMessage());

				}
				String storedRefreshToken_tBigQueryOutput_1_tBQBE = (String) properties_tBigQueryOutput_1_tBQBE
						.get("refreshtoken");

				// Check to see if the an existing refresh token was loaded.
				// If so, create a credential and call refreshToken() to get a
				// new
				// access token.
				if (storedRefreshToken_tBigQueryOutput_1_tBQBE != null) {
					// Request a new Access token using the refresh token.
					com.google.api.client.googleapis.auth.oauth2.GoogleCredential credential_tBigQueryOutput_1_tBQBE = new com.google.api.client.googleapis.auth.oauth2.GoogleCredential.Builder()
							.setTransport(TRANSPORT_tBigQueryOutput_1_tBQBE)
							.setJsonFactory(
									JSON_FACTORY_tBigQueryOutput_1_tBQBE)
							.setClientSecrets(
									clientSecrets_tBigQueryOutput_1_tBQBE)
							.build()
							.setFromTokenResponse(
									new com.google.api.client.auth.oauth2.TokenResponse()
											.setRefreshToken(storedRefreshToken_tBigQueryOutput_1_tBQBE));

					credential_tBigQueryOutput_1_tBQBE.refreshToken();

					log.info("tBigQueryOutput_1_tBQBE - An existing refresh token was loaded.");

					bigqueryclient_tBigQueryOutput_1_tBQBE = new com.google.api.services.bigquery.Bigquery.Builder(
							new com.google.api.client.http.javanet.NetHttpTransport(),
							new com.google.api.client.json.jackson2.JacksonFactory(),
							credential_tBigQueryOutput_1_tBQBE)
							.setApplicationName("Talend").build();
				} else {

					log.info("tBigQueryOutput_1_tBQBE - The refresh token does not exist.");

					String authorizationCode_tBigQueryOutput_1_tBQBE = "4/6gBlGFRRFpATvVP8_sQYVP4jKmxlsTGUsvS94SPwxJ457bd_n_Vfe64";
					if (authorizationCode_tBigQueryOutput_1_tBQBE == null
							|| "".equals(authorizationCode_tBigQueryOutput_1_tBQBE)
							|| "\"\""
									.equals(authorizationCode_tBigQueryOutput_1_tBQBE)) {
						String authorizeUrl_tBigQueryOutput_1_tBQBE = new com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeRequestUrl(
								clientSecrets_tBigQueryOutput_1_tBQBE,
								REDIRECT_URI_tBigQueryOutput_1_tBQBE,
								SCOPES_tBigQueryOutput_1_tBQBE).setState("")
								.build();

						log.warn("tBigQueryOutput_1_tBQBE - Paste this URL into a web browser to authorize BigQuery Access:\n"
								+ authorizeUrl_tBigQueryOutput_1_tBQBE);

						System.out
								.println("Paste this URL into a web browser to authorize BigQuery Access:\n"
										+ authorizeUrl_tBigQueryOutput_1_tBQBE);
						throw new java.lang.Exception(
								"Authorization Code error");
					} else {

						log.info("tBigQueryOutput_1_tBQBE - Exchange the auth code for an access token and refesh token.");

						// Exchange the auth code for an access token and refesh
						// token
						if (flow_tBigQueryOutput_1_tBQBE == null) {
							flow_tBigQueryOutput_1_tBQBE = new com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow.Builder(
									new com.google.api.client.http.javanet.NetHttpTransport(),
									new com.google.api.client.json.jackson2.JacksonFactory(),
									clientSecrets_tBigQueryOutput_1_tBQBE,
									SCOPES_tBigQueryOutput_1_tBQBE)
									.setAccessType("offline")
									.setApprovalPrompt("force").build();
						}
						com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse response_tBigQueryOutput_1_tBQBE = flow_tBigQueryOutput_1_tBQBE
								.newTokenRequest(
										authorizationCode_tBigQueryOutput_1_tBQBE)
								.setRedirectUri(
										REDIRECT_URI_tBigQueryOutput_1_tBQBE)
								.execute();
						com.google.api.client.auth.oauth2.Credential credential_tBigQueryOutput_1_tBQBE = flow_tBigQueryOutput_1_tBQBE
								.createAndStoreCredential(
										response_tBigQueryOutput_1_tBQBE, null);

						log.info("tBigQueryOutput_1_tBQBE - Store the refresh token for future use.");

						// Store the refresh token for future use.
						java.util.Properties storeProperties_tBigQueryOutput_1_tBQBE = new java.util.Properties();
						storeProperties_tBigQueryOutput_1_tBQBE.setProperty(
								"refreshtoken",
								credential_tBigQueryOutput_1_tBQBE
										.getRefreshToken());
						java.io.FileOutputStream outputStream_tBigQueryOutput_1_tBQBE = new java.io.FileOutputStream(
								tokenFile_tBigQueryOutput_1_tBQBE);
						storeProperties_tBigQueryOutput_1_tBQBE.store(
								outputStream_tBigQueryOutput_1_tBQBE, null);
						if (outputStream_tBigQueryOutput_1_tBQBE != null) {
							outputStream_tBigQueryOutput_1_tBQBE.close();
						}

						bigqueryclient_tBigQueryOutput_1_tBQBE = new com.google.api.services.bigquery.Bigquery.Builder(
								new com.google.api.client.http.javanet.NetHttpTransport(),
								new com.google.api.client.json.jackson2.JacksonFactory(),
								credential_tBigQueryOutput_1_tBQBE).build();
					}
				}

				/* -------------------------------------- */

				final String decryptedPwd_tBigQueryOutput_1_tBQBE = routines.system.PasswordEncryptUtil
						.decryptPassword("f4f7aba1746784ea");

				org.jets3t.service.security.GSCredentials gsCredentials_tBigQueryOutput_1_tBQBE = new org.jets3t.service.security.GSCredentials(
						"", decryptedPwd_tBigQueryOutput_1_tBQBE);

				org.jets3t.service.impl.rest.httpclient.GoogleStorageService gsService_tBigQueryOutput_1_tBQBE = new org.jets3t.service.impl.rest.httpclient.GoogleStorageService(
						gsCredentials_tBigQueryOutput_1_tBQBE, " GPN:Talend",
						null);

				java.io.File fileData_tBigQueryOutput_1_tBQBE = new java.io.File(
						"/Users/isa/Documents/gifmis_bitbucket/GifMis Analytics/Talend Workspace/biquery_bulk.txt");
				org.jets3t.service.model.GSObject fileObject_tBigQueryOutput_1_tBQBE = new org.jets3t.service.model.GSObject(
						fileData_tBigQueryOutput_1_tBQBE);

				log.info("tBigQueryOutput_1_tBQBE - Upload "
						+ "/Users/isa/Documents/gifmis_bitbucket/GifMis Analytics/Talend Workspace/biquery_bulk.txt"
						+ " to Google Service Bucket: " + "");

				gsService_tBigQueryOutput_1_tBQBE.putObject("",
						fileObject_tBigQueryOutput_1_tBQBE);

				log.info("tBigQueryOutput_1_tBQBE - Upload Done.");

				/* ------------------------------------------- */

				log.info("tBigQueryOutput_1_tBQBE - Starting build a job.");

				com.google.api.services.bigquery.model.Job job_tBigQueryOutput_1_tBQBE = new com.google.api.services.bigquery.model.Job();
				job_tBigQueryOutput_1_tBQBE
						.setJobReference(new com.google.api.services.bigquery.model.JobReference()
								.setProjectId(PROJECT_ID_tBigQueryOutput_1_tBQBE));

				com.google.api.services.bigquery.model.JobConfiguration config_tBigQueryOutput_1_tBQBE = new com.google.api.services.bigquery.model.JobConfiguration();
				com.google.api.services.bigquery.model.JobConfigurationLoad queryLoad_tBigQueryOutput_1_tBQBE = new com.google.api.services.bigquery.model.JobConfigurationLoad();
				com.google.api.services.bigquery.model.TableSchema schema_tBigQueryOutput_1_tBQBE = new com.google.api.services.bigquery.model.TableSchema();

				log.info("tBigQueryOutput_1_tBQBE - Table field schema:");

				java.util.List<com.google.api.services.bigquery.model.TableFieldSchema> fields_tBigQueryOutput_1_tBQBE = new java.util.ArrayList<com.google.api.services.bigquery.model.TableFieldSchema>();

				com.google.api.services.bigquery.model.TableFieldSchema EquipmentTypeCode_tBigQueryOutput_1_tBQBE = new com.google.api.services.bigquery.model.TableFieldSchema();
				EquipmentTypeCode_tBigQueryOutput_1_tBQBE
						.setName("EquipmentTypeCode");
				EquipmentTypeCode_tBigQueryOutput_1_tBQBE.setType("string");
				fields_tBigQueryOutput_1_tBQBE
						.add(EquipmentTypeCode_tBigQueryOutput_1_tBQBE);

				log.debug("tBigQueryOutput_1_tBQBE - Field index[0] {\"name\":\"EquipmentTypeCode\",\"type\":\"string\"}");

				com.google.api.services.bigquery.model.TableFieldSchema LanguageCode_tBigQueryOutput_1_tBQBE = new com.google.api.services.bigquery.model.TableFieldSchema();
				LanguageCode_tBigQueryOutput_1_tBQBE.setName("LanguageCode");
				LanguageCode_tBigQueryOutput_1_tBQBE.setType("string");
				fields_tBigQueryOutput_1_tBQBE
						.add(LanguageCode_tBigQueryOutput_1_tBQBE);

				log.debug("tBigQueryOutput_1_tBQBE - Field index[1] {\"name\":\"LanguageCode\",\"type\":\"string\"}");

				com.google.api.services.bigquery.model.TableFieldSchema EquipmentTypeName_tBigQueryOutput_1_tBQBE = new com.google.api.services.bigquery.model.TableFieldSchema();
				EquipmentTypeName_tBigQueryOutput_1_tBQBE
						.setName("EquipmentTypeName");
				EquipmentTypeName_tBigQueryOutput_1_tBQBE.setType("string");
				fields_tBigQueryOutput_1_tBQBE
						.add(EquipmentTypeName_tBigQueryOutput_1_tBQBE);

				log.debug("tBigQueryOutput_1_tBQBE - Field index[2] {\"name\":\"EquipmentTypeName\",\"type\":\"string\"}");

				schema_tBigQueryOutput_1_tBQBE
						.setFields(fields_tBigQueryOutput_1_tBQBE);

				queryLoad_tBigQueryOutput_1_tBQBE
						.setSchema(schema_tBigQueryOutput_1_tBQBE);

				queryLoad_tBigQueryOutput_1_tBQBE
						.setCreateDisposition("CREATE_IF_NEEDED");

				queryLoad_tBigQueryOutput_1_tBQBE.setFieldDelimiter(";");

				queryLoad_tBigQueryOutput_1_tBQBE.setAllowQuotedNewlines(true);

				queryLoad_tBigQueryOutput_1_tBQBE
						.setWriteDisposition("WRITE_EMPTY");
				com.google.api.services.bigquery.model.TableReference destinationTable_tBigQueryOutput_1_tBQBE = new com.google.api.services.bigquery.model.TableReference();
				destinationTable_tBigQueryOutput_1_tBQBE
						.setProjectId(PROJECT_ID_tBigQueryOutput_1_tBQBE);
				destinationTable_tBigQueryOutput_1_tBQBE.setDatasetId("EDW");
				destinationTable_tBigQueryOutput_1_tBQBE
						.setTableId("Dim_EquipmentTypeText");

				queryLoad_tBigQueryOutput_1_tBQBE
						.setDestinationTable(destinationTable_tBigQueryOutput_1_tBQBE);
				queryLoad_tBigQueryOutput_1_tBQBE
						.setSourceUris(java.util.Arrays.asList("gs://"));
				queryLoad_tBigQueryOutput_1_tBQBE.setSkipLeadingRows(0);

				config_tBigQueryOutput_1_tBQBE
						.setLoad(queryLoad_tBigQueryOutput_1_tBQBE);

				job_tBigQueryOutput_1_tBQBE
						.setConfiguration(config_tBigQueryOutput_1_tBQBE);

				com.google.api.services.bigquery.Bigquery.Jobs.Insert insertReq_tBigQueryOutput_1_tBQBE = bigqueryclient_tBigQueryOutput_1_tBQBE
						.jobs().insert("", job_tBigQueryOutput_1_tBQBE);
				insertReq_tBigQueryOutput_1_tBQBE
						.setProjectId(PROJECT_ID_tBigQueryOutput_1_tBQBE);

				log.info("tBigQueryOutput_1_tBQBE - Build a job successfully.");
				log.info("tBigQueryOutput_1_tBQBE - Starting load the job.");

				System.out.println("Starting load job.");
				com.google.api.services.bigquery.model.Job jobExec_tBigQueryOutput_1_tBQBE = null;
				try {
					jobExec_tBigQueryOutput_1_tBQBE = insertReq_tBigQueryOutput_1_tBQBE
							.execute();
				} catch (Exception ee_tBigQueryOutput_1_tBQBE) {

					log.error("tBigQueryOutput_1_tBQBE - "
							+ ee_tBigQueryOutput_1_tBQBE.getMessage() + "\n"
							+ ee_tBigQueryOutput_1_tBQBE.getCause());

					System.err.println(ee_tBigQueryOutput_1_tBQBE.getMessage()
							+ "\n" + ee_tBigQueryOutput_1_tBQBE.getCause());

				}
				if (jobExec_tBigQueryOutput_1_tBQBE.getStatus().getState()
						.equals("RUNNING")
						|| jobExec_tBigQueryOutput_1_tBQBE.getStatus()
								.getState().equals("PENDING")) {
					com.google.api.services.bigquery.model.Job pollJob_tBigQueryOutput_1_tBQBE = bigqueryclient_tBigQueryOutput_1_tBQBE
							.jobs()
							.get(PROJECT_ID_tBigQueryOutput_1_tBQBE,
									jobExec_tBigQueryOutput_1_tBQBE
											.getJobReference().getJobId())
							.execute();
					while (pollJob_tBigQueryOutput_1_tBQBE.getStatus()
							.getState().equals("RUNNING")
							|| pollJob_tBigQueryOutput_1_tBQBE.getStatus()
									.getState().equals("PENDING")) {
						Thread.sleep(1000);
						pollJob_tBigQueryOutput_1_tBQBE = bigqueryclient_tBigQueryOutput_1_tBQBE
								.jobs()
								.get(PROJECT_ID_tBigQueryOutput_1_tBQBE,
										jobExec_tBigQueryOutput_1_tBQBE
												.getJobReference().getJobId())
								.execute();
						System.out.println(String.format(
								"Waiting on job %s ... Current status: %s",
								jobExec_tBigQueryOutput_1_tBQBE
										.getJobReference().getJobId(),
								pollJob_tBigQueryOutput_1_tBQBE.getStatus()
										.getState()));

						log.debug("tBigQueryOutput_1_tBQBE - "
								+ String.format(
										"Waiting on job %s ... Current status: %s",
										jobExec_tBigQueryOutput_1_tBQBE
												.getJobReference().getJobId(),
										pollJob_tBigQueryOutput_1_tBQBE
												.getStatus().getState()));

					}

					com.google.api.services.bigquery.model.Job doneJob_tBigQueryOutput_1_tBQBE = pollJob_tBigQueryOutput_1_tBQBE;

					if ((doneJob_tBigQueryOutput_1_tBQBE.getStatus() != null)
							&& (doneJob_tBigQueryOutput_1_tBQBE.getStatus()
									.getErrors() != null)) {
						status = "failure";
						throw new Exception(doneJob_tBigQueryOutput_1_tBQBE
								.getStatus().getErrors().toString());
					}

					System.out.println("Done: "
							+ doneJob_tBigQueryOutput_1_tBQBE.toString());
					com.google.api.services.bigquery.model.JobStatistics jobStatistics_tBigQueryOutput_1_tBQBE = doneJob_tBigQueryOutput_1_tBQBE
							.getStatistics();
					if (jobStatistics_tBigQueryOutput_1_tBQBE != null
							&& jobStatistics_tBigQueryOutput_1_tBQBE.getLoad() != null) {
						com.google.api.services.bigquery.model.JobStatistics3 loadObject_tBigQueryOutput_1_tBQBE = jobStatistics_tBigQueryOutput_1_tBQBE
								.getLoad();
						if (loadObject_tBigQueryOutput_1_tBQBE != null) {
							nb_line_tBigQueryOutput_1_tBQBE = loadObject_tBigQueryOutput_1_tBQBE
									.getOutputRows();
						}
					}

					log.info("tBigQueryOutput_1_tBQBE - Load Done: "
							+ doneJob_tBigQueryOutput_1_tBQBE.toString());
					log.info("tBigQueryOutput_1_tBQBE - "
							+ nb_line_tBigQueryOutput_1_tBQBE
							+ " records load successfully.");

				} else {

					log.error("tBigQueryOutput_1_tBQBE - Error: "
							+ jobExec_tBigQueryOutput_1_tBQBE.toString());

					System.err.println("Error: "
							+ jobExec_tBigQueryOutput_1_tBQBE.toString());

				}

				/**
				 * [tBigQueryOutput_1_tBQBE begin ] stop
				 */

				/**
				 * [tBigQueryOutput_1_tBQBE main ] start
				 */

				currentVirtualComponent = "tBigQueryOutput_1";

				currentComponent = "tBigQueryOutput_1_tBQBE";

				tos_count_tBigQueryOutput_1_tBQBE++;

				/**
				 * [tBigQueryOutput_1_tBQBE main ] stop
				 */

				/**
				 * [tBigQueryOutput_1_tBQBE process_data_begin ] start
				 */

				currentVirtualComponent = "tBigQueryOutput_1";

				currentComponent = "tBigQueryOutput_1_tBQBE";

				/**
				 * [tBigQueryOutput_1_tBQBE process_data_begin ] stop
				 */

				/**
				 * [tBigQueryOutput_1_tBQBE process_data_end ] start
				 */

				currentVirtualComponent = "tBigQueryOutput_1";

				currentComponent = "tBigQueryOutput_1_tBQBE";

				/**
				 * [tBigQueryOutput_1_tBQBE process_data_end ] stop
				 */

				/**
				 * [tBigQueryOutput_1_tBQBE end ] start
				 */

				currentVirtualComponent = "tBigQueryOutput_1";

				currentComponent = "tBigQueryOutput_1_tBQBE";

				if (log.isDebugEnabled())
					log.debug("tBigQueryOutput_1_tBQBE - " + ("Done."));

				ok_Hash.put("tBigQueryOutput_1_tBQBE", true);
				end_Hash.put("tBigQueryOutput_1_tBQBE",
						System.currentTimeMillis());

				/**
				 * [tBigQueryOutput_1_tBQBE end ] stop
				 */

			}// end the resume

		} catch (java.lang.Exception e) {

			if (!(e instanceof TalendException)) {
				log.fatal(currentComponent + " " + e.getMessage(), e);
			}

			TalendException te = new TalendException(e, currentComponent,
					globalMap);

			te.setVirtualComponentName(currentVirtualComponent);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			try {

				/**
				 * [tBigQueryInput_1 finally ] start
				 */

				currentComponent = "tBigQueryInput_1";

				/**
				 * [tBigQueryInput_1 finally ] stop
				 */

				/**
				 * [tMap_1 finally ] start
				 */

				currentComponent = "tMap_1";

				/**
				 * [tMap_1 finally ] stop
				 */

				/**
				 * [tBigQueryOutput_1_tBQOB finally ] start
				 */

				currentVirtualComponent = "tBigQueryOutput_1";

				currentComponent = "tBigQueryOutput_1_tBQOB";

				if (resourceMap.get("finish_tBigQueryOutput_1_tBQOB") == null) {
					if (resourceMap.get("csvWriter_tBigQueryOutput_1_tBQOB") != null) {
						((com.talend.csv.CSVWriter) resourceMap
								.get("csvWriter_tBigQueryOutput_1_tBQOB"))
								.close();
					}
				}

				/**
				 * [tBigQueryOutput_1_tBQOB finally ] stop
				 */

				/**
				 * [tBigQueryOutput_1_tBQBE finally ] start
				 */

				currentVirtualComponent = "tBigQueryOutput_1";

				currentComponent = "tBigQueryOutput_1_tBQBE";

				/**
				 * [tBigQueryOutput_1_tBQBE finally ] stop
				 */

			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("tBigQueryInput_1_SUBPROCESS_STATE", 1);
	}

	public String resuming_logs_dir_path = null;
	public String resuming_checkpoint_path = null;
	public String parent_part_launcher = null;
	private String resumeEntryMethodName = null;
	private boolean globalResumeTicket = false;

	public boolean watch = false;
	// portStats is null, it means don't execute the statistics
	public Integer portStats = null;
	public int portTraces = 4334;
	public String clientHost;
	public String defaultClientHost = "localhost";
	public String contextStr = "Default";
	public boolean isDefaultContext = true;
	public String pid = "0";
	public String rootPid = null;
	public String fatherPid = null;
	public String fatherNode = null;
	public long startTime = 0;
	public boolean isChildJob = false;
	public String log4jLevel = "";

	private boolean execStat = true;

	private ThreadLocal<java.util.Map<String, String>> threadLocal = new ThreadLocal<java.util.Map<String, String>>() {
		protected java.util.Map<String, String> initialValue() {
			java.util.Map<String, String> threadRunResultMap = new java.util.HashMap<String, String>();
			threadRunResultMap.put("errorCode", null);
			threadRunResultMap.put("status", "");
			return threadRunResultMap;
		};
	};

	private PropertiesWithType context_param = new PropertiesWithType();
	public java.util.Map<String, Object> parentContextMap = new java.util.HashMap<String, Object>();

	public String status = "";

	public static void main(String[] args) {
		final cci_test_native_comp cci_test_native_compClass = new cci_test_native_comp();

		int exitCode = cci_test_native_compClass.runJobInTOS(args);
		if (exitCode == 0) {
			log.info("TalendJob: 'cci_test_native_comp' - Done.");
		}

		System.exit(exitCode);
	}

	public String[][] runJob(String[] args) {

		int exitCode = runJobInTOS(args);
		String[][] bufferValue = new String[][] { { Integer.toString(exitCode) } };

		return bufferValue;
	}

	public boolean hastBufferOutputComponent() {
		boolean hastBufferOutput = false;

		return hastBufferOutput;
	}

	public int runJobInTOS(String[] args) {
		// reset status
		status = "";

		String lastStr = "";
		for (String arg : args) {
			if (arg.equalsIgnoreCase("--context_param")) {
				lastStr = arg;
			} else if (lastStr.equals("")) {
				evalParam(arg);
			} else {
				evalParam(lastStr + " " + arg);
				lastStr = "";
			}
		}

		if (!"".equals(log4jLevel)) {
			if ("trace".equalsIgnoreCase(log4jLevel)) {
				log.setLevel(org.apache.log4j.Level.TRACE);
			} else if ("debug".equalsIgnoreCase(log4jLevel)) {
				log.setLevel(org.apache.log4j.Level.DEBUG);
			} else if ("info".equalsIgnoreCase(log4jLevel)) {
				log.setLevel(org.apache.log4j.Level.INFO);
			} else if ("warn".equalsIgnoreCase(log4jLevel)) {
				log.setLevel(org.apache.log4j.Level.WARN);
			} else if ("error".equalsIgnoreCase(log4jLevel)) {
				log.setLevel(org.apache.log4j.Level.ERROR);
			} else if ("fatal".equalsIgnoreCase(log4jLevel)) {
				log.setLevel(org.apache.log4j.Level.FATAL);
			} else if ("off".equalsIgnoreCase(log4jLevel)) {
				log.setLevel(org.apache.log4j.Level.OFF);
			}
			org.apache.log4j.Logger.getRootLogger().setLevel(log.getLevel());
		}
		log.info("TalendJob: 'cci_test_native_comp' - Start.");

		if (clientHost == null) {
			clientHost = defaultClientHost;
		}

		if (pid == null || "0".equals(pid)) {
			pid = TalendString.getAsciiRandomString(6);
		}

		if (rootPid == null) {
			rootPid = pid;
		}
		if (fatherPid == null) {
			fatherPid = pid;
		} else {
			isChildJob = true;
		}

		if (portStats != null) {
			// portStats = -1; //for testing
			if (portStats < 0 || portStats > 65535) {
				// issue:10869, the portStats is invalid, so this client socket
				// can't open
				System.err.println("The statistics socket port " + portStats
						+ " is invalid.");
				execStat = false;
			}
		} else {
			execStat = false;
		}

		try {
			// call job/subjob with an existing context, like:
			// --context=production. if without this parameter, there will use
			// the default context instead.
			java.io.InputStream inContext = cci_test_native_comp.class
					.getClassLoader().getResourceAsStream(
							"cci/cci_test_native_comp_0_1/contexts/"
									+ contextStr + ".properties");
			if (inContext == null) {
				inContext = cci_test_native_comp.class
						.getClassLoader()
						.getResourceAsStream(
								"config/contexts/" + contextStr + ".properties");
			}
			if (inContext != null) {
				// defaultProps is in order to keep the original context value
				defaultProps.load(inContext);
				inContext.close();
				context = new ContextProperties(defaultProps);
			} else if (!isDefaultContext) {
				// print info and job continue to run, for case: context_param
				// is not empty.
				System.err.println("Could not find the context " + contextStr);
			}

			if (!context_param.isEmpty()) {
				context.putAll(context_param);
				// set types for params from parentJobs
				for (Object key : context_param.keySet()) {
					String context_key = key.toString();
					String context_type = context_param
							.getContextType(context_key);
					context.setContextType(context_key, context_type);

				}
			}
		} catch (java.io.IOException ie) {
			System.err.println("Could not load context " + contextStr);
			ie.printStackTrace();
		}

		// get context value from parent directly
		if (parentContextMap != null && !parentContextMap.isEmpty()) {
		}

		// Resume: init the resumeUtil
		resumeEntryMethodName = ResumeUtil
				.getResumeEntryMethodName(resuming_checkpoint_path);
		resumeUtil = new ResumeUtil(resuming_logs_dir_path, isChildJob, rootPid);
		resumeUtil.initCommonInfo(pid, rootPid, fatherPid, projectName,
				jobName, contextStr, jobVersion);

		List<String> parametersToEncrypt = new java.util.ArrayList<String>();
		// Resume: jobStart
		resumeUtil.addLog("JOB_STARTED", "JOB:" + jobName,
				parent_part_launcher, Thread.currentThread().getId() + "", "",
				"", "", "",
				resumeUtil.convertToJsonText(context, parametersToEncrypt));

		if (execStat) {
			try {
				runStat.openSocket(!isChildJob);
				runStat.setAllPID(rootPid, fatherPid, pid, jobName);
				runStat.startThreadStat(clientHost, portStats);
				runStat.updateStatOnJob(RunStat.JOBSTART, fatherNode);
			} catch (java.io.IOException ioException) {
				ioException.printStackTrace();
			}
		}

		java.util.concurrent.ConcurrentHashMap<Object, Object> concurrentHashMap = new java.util.concurrent.ConcurrentHashMap<Object, Object>();
		globalMap.put("concurrentHashMap", concurrentHashMap);

		long startUsedMemory = Runtime.getRuntime().totalMemory()
				- Runtime.getRuntime().freeMemory();
		long endUsedMemory = 0;
		long end = 0;

		startTime = System.currentTimeMillis();

		this.globalResumeTicket = true;// to run tPreJob

		this.globalResumeTicket = false;// to run others jobs

		try {
			errorCode = null;
			tBigQueryInput_1Process(globalMap);
			if (!"failure".equals(status)) {
				status = "end";
			}
		} catch (TalendException e_tBigQueryInput_1) {
			globalMap.put("tBigQueryInput_1_SUBPROCESS_STATE", -1);

			e_tBigQueryInput_1.printStackTrace();

		}

		this.globalResumeTicket = true;// to run tPostJob

		end = System.currentTimeMillis();

		if (watch) {
			System.out.println((end - startTime) + " milliseconds");
		}

		endUsedMemory = Runtime.getRuntime().totalMemory()
				- Runtime.getRuntime().freeMemory();
		if (false) {
			System.out
					.println((endUsedMemory - startUsedMemory)
							+ " bytes memory increase when running : cci_test_native_comp");
		}

		if (execStat) {
			runStat.updateStatOnJob(RunStat.JOBEND, fatherNode);
			runStat.stopThreadStat();
		}
		int returnCode = 0;
		if (errorCode == null) {
			returnCode = status != null && status.equals("failure") ? 1 : 0;
		} else {
			returnCode = errorCode.intValue();
		}
		resumeUtil.addLog("JOB_ENDED", "JOB:" + jobName, parent_part_launcher,
				Thread.currentThread().getId() + "", "", "" + returnCode, "",
				"", "");

		return returnCode;

	}

	// only for OSGi env
	public void destroy() {

	}

	private java.util.Map<String, Object> getSharedConnections4REST() {
		java.util.Map<String, Object> connections = new java.util.HashMap<String, Object>();

		return connections;
	}

	private void evalParam(String arg) {
		if (arg.startsWith("--resuming_logs_dir_path")) {
			resuming_logs_dir_path = arg.substring(25);
		} else if (arg.startsWith("--resuming_checkpoint_path")) {
			resuming_checkpoint_path = arg.substring(27);
		} else if (arg.startsWith("--parent_part_launcher")) {
			parent_part_launcher = arg.substring(23);
		} else if (arg.startsWith("--watch")) {
			watch = true;
		} else if (arg.startsWith("--stat_port=")) {
			String portStatsStr = arg.substring(12);
			if (portStatsStr != null && !portStatsStr.equals("null")) {
				portStats = Integer.parseInt(portStatsStr);
			}
		} else if (arg.startsWith("--trace_port=")) {
			portTraces = Integer.parseInt(arg.substring(13));
		} else if (arg.startsWith("--client_host=")) {
			clientHost = arg.substring(14);
		} else if (arg.startsWith("--context=")) {
			contextStr = arg.substring(10);
			isDefaultContext = false;
		} else if (arg.startsWith("--father_pid=")) {
			fatherPid = arg.substring(13);
		} else if (arg.startsWith("--root_pid=")) {
			rootPid = arg.substring(11);
		} else if (arg.startsWith("--father_node=")) {
			fatherNode = arg.substring(14);
		} else if (arg.startsWith("--pid=")) {
			pid = arg.substring(6);
		} else if (arg.startsWith("--context_type")) {
			String keyValue = arg.substring(15);
			int index = -1;
			if (keyValue != null && (index = keyValue.indexOf('=')) > -1) {
				if (fatherPid == null) {
					context_param.setContextType(keyValue.substring(0, index),
							replaceEscapeChars(keyValue.substring(index + 1)));
				} else { // the subjob won't escape the especial chars
					context_param.setContextType(keyValue.substring(0, index),
							keyValue.substring(index + 1));
				}

			}

		} else if (arg.startsWith("--context_param")) {
			String keyValue = arg.substring(16);
			int index = -1;
			if (keyValue != null && (index = keyValue.indexOf('=')) > -1) {
				if (fatherPid == null) {
					context_param.put(keyValue.substring(0, index),
							replaceEscapeChars(keyValue.substring(index + 1)));
				} else { // the subjob won't escape the especial chars
					context_param.put(keyValue.substring(0, index),
							keyValue.substring(index + 1));
				}
			}
		} else if (arg.startsWith("--log4jLevel=")) {
			log4jLevel = arg.substring(13);
		}

	}

	private static final String NULL_VALUE_EXPRESSION_IN_COMMAND_STRING_FOR_CHILD_JOB_ONLY = "<TALEND_NULL>";

	private final String[][] escapeChars = { { "\\\\", "\\" }, { "\\n", "\n" },
			{ "\\'", "\'" }, { "\\r", "\r" }, { "\\f", "\f" }, { "\\b", "\b" },
			{ "\\t", "\t" } };

	private String replaceEscapeChars(String keyValue) {

		if (keyValue == null || ("").equals(keyValue.trim())) {
			return keyValue;
		}

		StringBuilder result = new StringBuilder();
		int currIndex = 0;
		while (currIndex < keyValue.length()) {
			int index = -1;
			// judege if the left string includes escape chars
			for (String[] strArray : escapeChars) {
				index = keyValue.indexOf(strArray[0], currIndex);
				if (index >= 0) {

					result.append(keyValue.substring(currIndex,
							index + strArray[0].length()).replace(strArray[0],
							strArray[1]));
					currIndex = index + strArray[0].length();
					break;
				}
			}
			// if the left string doesn't include escape chars, append the left
			// into the result
			if (index < 0) {
				result.append(keyValue.substring(currIndex));
				currIndex = currIndex + keyValue.length();
			}
		}

		return result.toString();
	}

	public Integer getErrorCode() {
		return errorCode;
	}

	public String getStatus() {
		return status;
	}

	ResumeUtil resumeUtil = null;
}
/************************************************************************************************
 * 86387 characters generated by Talend Data Integration on the February 12,
 * 2019 1:37:08 PM EET
 ************************************************************************************************/
