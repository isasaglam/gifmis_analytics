#!/bin/sh
cd `dirname $0`
ROOT_PATH=`pwd`
java -Xms4096M -Xmx4096M -Dfile.encoding=UTF-8 -cp .:$ROOT_PATH:$ROOT_PATH/../lib/routines.jar:$ROOT_PATH/../lib/dom4j-1.6.1.jar:$ROOT_PATH/../lib/log4j-1.2.16.jar:$ROOT_PATH/../lib/mysql-connector-java-5.1.30-bin.jar:$ROOT_PATH/../lib/ojdbc7.jar:$ROOT_PATH/../lib/talend_file_enhanced_20070724.jar:$ROOT_PATH/../lib/vertica-jdbc-9.0.0-0.jar:$ROOT_PATH/transaction_type_0_1.jar: gifmis.transaction_type_0_1.transaction_type  "$@" 