
package gifmis.ledger2_12_0_1;

import routines.Numeric;
import routines.DataOperation;
import routines.TalendDataGenerator;
import routines.TalendStringUtil;
import routines.TalendString;
import routines.StringHandling;
import routines.Relational;
import routines.TalendDate;
import routines.Mathematical;
import routines.SQLike;
import routines.system.*;
import routines.system.api.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.math.BigDecimal;
import java.io.ByteArrayOutputStream;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.io.IOException;
import java.util.Comparator;
 




	//the import part of tJava_1
	//import java.util.List;


@SuppressWarnings("unused")

/**
 * Job: ledger2_12 Purpose: <br>
 * Description:  <br>
 * @author isa.saglam@oredata.com
 * @version 7.0.1.20180411_1414
 * @status 
 */
public class ledger2_12 implements TalendJob {

protected static void logIgnoredError(String message, Throwable cause) {
       System.err.println(message);
       if (cause != null) {
               cause.printStackTrace();
       }

}


	public final Object obj = new Object();

	// for transmiting parameters purpose
	private Object valueObject = null;

	public Object getValueObject() {
		return this.valueObject;
	}

	public void setValueObject(Object valueObject) {
		this.valueObject = valueObject;
	}
	
	private final static String defaultCharset = java.nio.charset.Charset.defaultCharset().name();

	
	private final static String utf8Charset = "UTF-8";
	//contains type for every context property
	public class PropertiesWithType extends java.util.Properties {
		private static final long serialVersionUID = 1L;
		private java.util.Map<String,String> propertyTypes = new java.util.HashMap<>();
		
		public PropertiesWithType(java.util.Properties properties){
			super(properties);
		}
		public PropertiesWithType(){
			super();
		}
		
		public void setContextType(String key, String type) {
			propertyTypes.put(key,type);
		}
	
		public String getContextType(String key) {
			return propertyTypes.get(key);
		}
	}
	
	// create and load default properties
	private java.util.Properties defaultProps = new java.util.Properties();
	// create application properties with default
	public class ContextProperties extends PropertiesWithType {

		private static final long serialVersionUID = 1L;

		public ContextProperties(java.util.Properties properties){
			super(properties);
		}
		public ContextProperties(){
			super();
		}

		public void synchronizeContext(){
			
			if(filePath != null){
				
					this.setProperty("filePath", filePath.toString());
				
			}
			
			if(jobLogs != null){
				
					this.setProperty("jobLogs", jobLogs.toString());
				
			}
			
			if(jobMeters != null){
				
					this.setProperty("jobMeters", jobMeters.toString());
				
			}
			
			if(jobStats != null){
				
					this.setProperty("jobStats", jobStats.toString());
				
			}
			
			if(nexusDefaultGroupID != null){
				
					this.setProperty("nexusDefaultGroupID", nexusDefaultGroupID.toString());
				
			}
			
			if(tableName != null){
				
					this.setProperty("tableName", tableName.toString());
				
			}
			
			if(MySql_AMC_AdditionalParams != null){
				
					this.setProperty("MySql_AMC_AdditionalParams", MySql_AMC_AdditionalParams.toString());
				
			}
			
			if(MySql_AMC_Database != null){
				
					this.setProperty("MySql_AMC_Database", MySql_AMC_Database.toString());
				
			}
			
			if(MySql_AMC_Login != null){
				
					this.setProperty("MySql_AMC_Login", MySql_AMC_Login.toString());
				
			}
			
			if(MySql_AMC_Password != null){
				
					this.setProperty("MySql_AMC_Password", MySql_AMC_Password.toString());
				
			}
			
			if(MySql_AMC_Port != null){
				
					this.setProperty("MySql_AMC_Port", MySql_AMC_Port.toString());
				
			}
			
			if(MySql_AMC_Server != null){
				
					this.setProperty("MySql_AMC_Server", MySql_AMC_Server.toString());
				
			}
			
			if(Oracle_AdditionalParams != null){
				
					this.setProperty("Oracle_AdditionalParams", Oracle_AdditionalParams.toString());
				
			}
			
			if(Oracle_Login != null){
				
					this.setProperty("Oracle_Login", Oracle_Login.toString());
				
			}
			
			if(Oracle_Password != null){
				
					this.setProperty("Oracle_Password", Oracle_Password.toString());
				
			}
			
			if(Oracle_Port != null){
				
					this.setProperty("Oracle_Port", Oracle_Port.toString());
				
			}
			
			if(Oracle_Schema != null){
				
					this.setProperty("Oracle_Schema", Oracle_Schema.toString());
				
			}
			
			if(Oracle_Server != null){
				
					this.setProperty("Oracle_Server", Oracle_Server.toString());
				
			}
			
			if(Oracle_ServiceName != null){
				
					this.setProperty("Oracle_ServiceName", Oracle_ServiceName.toString());
				
			}
			
			if(Vertica_ODS_AdditionalParams != null){
				
					this.setProperty("Vertica_ODS_AdditionalParams", Vertica_ODS_AdditionalParams.toString());
				
			}
			
			if(Vertica_ODS_Database != null){
				
					this.setProperty("Vertica_ODS_Database", Vertica_ODS_Database.toString());
				
			}
			
			if(Vertica_ODS_Login != null){
				
					this.setProperty("Vertica_ODS_Login", Vertica_ODS_Login.toString());
				
			}
			
			if(Vertica_ODS_Password != null){
				
					this.setProperty("Vertica_ODS_Password", Vertica_ODS_Password.toString());
				
			}
			
			if(Vertica_ODS_Port != null){
				
					this.setProperty("Vertica_ODS_Port", Vertica_ODS_Port.toString());
				
			}
			
			if(Vertica_ODS_Schema != null){
				
					this.setProperty("Vertica_ODS_Schema", Vertica_ODS_Schema.toString());
				
			}
			
			if(Vertica_ODS_Server != null){
				
					this.setProperty("Vertica_ODS_Server", Vertica_ODS_Server.toString());
				
			}
			
		}

public String filePath;
public String getFilePath(){
	return this.filePath;
}
public String jobLogs;
public String getJobLogs(){
	return this.jobLogs;
}
public String jobMeters;
public String getJobMeters(){
	return this.jobMeters;
}
public String jobStats;
public String getJobStats(){
	return this.jobStats;
}
public String nexusDefaultGroupID;
public String getNexusDefaultGroupID(){
	return this.nexusDefaultGroupID;
}
public String tableName;
public String getTableName(){
	return this.tableName;
}
public String MySql_AMC_AdditionalParams;
public String getMySql_AMC_AdditionalParams(){
	return this.MySql_AMC_AdditionalParams;
}
public String MySql_AMC_Database;
public String getMySql_AMC_Database(){
	return this.MySql_AMC_Database;
}
public String MySql_AMC_Login;
public String getMySql_AMC_Login(){
	return this.MySql_AMC_Login;
}
public java.lang.String MySql_AMC_Password;
public java.lang.String getMySql_AMC_Password(){
	return this.MySql_AMC_Password;
}
public String MySql_AMC_Port;
public String getMySql_AMC_Port(){
	return this.MySql_AMC_Port;
}
public String MySql_AMC_Server;
public String getMySql_AMC_Server(){
	return this.MySql_AMC_Server;
}
public String Oracle_AdditionalParams;
public String getOracle_AdditionalParams(){
	return this.Oracle_AdditionalParams;
}
public String Oracle_Login;
public String getOracle_Login(){
	return this.Oracle_Login;
}
public java.lang.String Oracle_Password;
public java.lang.String getOracle_Password(){
	return this.Oracle_Password;
}
public String Oracle_Port;
public String getOracle_Port(){
	return this.Oracle_Port;
}
public String Oracle_Schema;
public String getOracle_Schema(){
	return this.Oracle_Schema;
}
public String Oracle_Server;
public String getOracle_Server(){
	return this.Oracle_Server;
}
public String Oracle_ServiceName;
public String getOracle_ServiceName(){
	return this.Oracle_ServiceName;
}
public String Vertica_ODS_AdditionalParams;
public String getVertica_ODS_AdditionalParams(){
	return this.Vertica_ODS_AdditionalParams;
}
public String Vertica_ODS_Database;
public String getVertica_ODS_Database(){
	return this.Vertica_ODS_Database;
}
public String Vertica_ODS_Login;
public String getVertica_ODS_Login(){
	return this.Vertica_ODS_Login;
}
public java.lang.String Vertica_ODS_Password;
public java.lang.String getVertica_ODS_Password(){
	return this.Vertica_ODS_Password;
}
public String Vertica_ODS_Port;
public String getVertica_ODS_Port(){
	return this.Vertica_ODS_Port;
}
public String Vertica_ODS_Schema;
public String getVertica_ODS_Schema(){
	return this.Vertica_ODS_Schema;
}
public String Vertica_ODS_Server;
public String getVertica_ODS_Server(){
	return this.Vertica_ODS_Server;
}
	}
	private ContextProperties context = new ContextProperties();
	public ContextProperties getContext() {
		return this.context;
	}
	private final String jobVersion = "0.1";
	private final String jobName = "ledger2_12";
	private final String projectName = "GIFMIS";
	public Integer errorCode = null;
	private String currentComponent = "";
	
		private final java.util.Map<String, Object> globalMap = new java.util.HashMap<String, Object>();
        private final static java.util.Map<String, Object> junitGlobalMap = new java.util.HashMap<String, Object>();
	
		private final java.util.Map<String, Long> start_Hash = new java.util.HashMap<String, Long>();
		private final java.util.Map<String, Long> end_Hash = new java.util.HashMap<String, Long>();
		private final java.util.Map<String, Boolean> ok_Hash = new java.util.HashMap<String, Boolean>();
		public  final java.util.List<String[]> globalBuffer = new java.util.ArrayList<String[]>();
	

private RunStat runStat = new RunStat();

	// OSGi DataSource
	private final static String KEY_DB_DATASOURCES = "KEY_DB_DATASOURCES";
	
	private final static String KEY_DB_DATASOURCES_RAW = "KEY_DB_DATASOURCES_RAW";

	public void setDataSources(java.util.Map<String, javax.sql.DataSource> dataSources) {
		java.util.Map<String, routines.system.TalendDataSource> talendDataSources = new java.util.HashMap<String, routines.system.TalendDataSource>();
		for (java.util.Map.Entry<String, javax.sql.DataSource> dataSourceEntry : dataSources.entrySet()) {
			talendDataSources.put(dataSourceEntry.getKey(), new routines.system.TalendDataSource(dataSourceEntry.getValue()));
		}
		globalMap.put(KEY_DB_DATASOURCES, talendDataSources);
		globalMap.put(KEY_DB_DATASOURCES_RAW, new java.util.HashMap<String, javax.sql.DataSource>(dataSources));
	}

	LogCatcherUtils talendLogs_LOGS = new LogCatcherUtils();
	StatCatcherUtils talendStats_STATS = new StatCatcherUtils("_faxl4CB6EemFpJwZ3c7JOA", "0.1");
	MetterCatcherUtils talendMeter_METTER = new MetterCatcherUtils("_faxl4CB6EemFpJwZ3c7JOA", "0.1");

private final java.io.ByteArrayOutputStream baos = new java.io.ByteArrayOutputStream();
private final java.io.PrintStream errorMessagePS = new java.io.PrintStream(new java.io.BufferedOutputStream(baos));

public String getExceptionStackTrace() {
	if ("failure".equals(this.getStatus())) {
		errorMessagePS.flush();
		return baos.toString();
	}
	return null;
}

private Exception exception;

public Exception getException() {
	if ("failure".equals(this.getStatus())) {
		return this.exception;
	}
	return null;
}

private class TalendException extends Exception {

	private static final long serialVersionUID = 1L;

	private java.util.Map<String, Object> globalMap = null;
	private Exception e = null;
	private String currentComponent = null;
	private String virtualComponentName = null;
	
	public void setVirtualComponentName (String virtualComponentName){
		this.virtualComponentName = virtualComponentName;
	}

	private TalendException(Exception e, String errorComponent, final java.util.Map<String, Object> globalMap) {
		this.currentComponent= errorComponent;
		this.globalMap = globalMap;
		this.e = e;
	}

	public Exception getException() {
		return this.e;
	}

	public String getCurrentComponent() {
		return this.currentComponent;
	}

	
    public String getExceptionCauseMessage(Exception e){
        Throwable cause = e;
        String message = null;
        int i = 10;
        while (null != cause && 0 < i--) {
            message = cause.getMessage();
            if (null == message) {
                cause = cause.getCause();
            } else {
                break;          
            }
        }
        if (null == message) {
            message = e.getClass().getName();
        }   
        return message;
    }

	@Override
	public void printStackTrace() {
		if (!(e instanceof TalendException || e instanceof TDieException)) {
			if(virtualComponentName!=null && currentComponent.indexOf(virtualComponentName+"_")==0){
				globalMap.put(virtualComponentName+"_ERROR_MESSAGE",getExceptionCauseMessage(e));
			}
			globalMap.put(currentComponent+"_ERROR_MESSAGE",getExceptionCauseMessage(e));
			System.err.println("Exception in component " + currentComponent + " (" + jobName + ")");
		}
		if (!(e instanceof TDieException)) {
			if(e instanceof TalendException){
				e.printStackTrace();
			} else {
				e.printStackTrace();
				e.printStackTrace(errorMessagePS);
				ledger2_12.this.exception = e;
			}
		}
		if (!(e instanceof TalendException)) {
		try {
			for (java.lang.reflect.Method m : this.getClass().getEnclosingClass().getMethods()) {
				if (m.getName().compareTo(currentComponent + "_error") == 0) {
					m.invoke(ledger2_12.this, new Object[] { e , currentComponent, globalMap});
					break;
				}
			}

			if(!(e instanceof TDieException)){
				talendLogs_LOGS.addMessage("Java Exception", currentComponent, 6, e.getClass().getName() + ":" + e.getMessage(), 1);
				talendLogs_LOGSProcess(globalMap);
			}
				} catch (TalendException e) {
					// do nothing
				
		} catch (Exception e) {
			this.e.printStackTrace();
		}
		}
	}
}

			public void preStaLogCon_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					preStaLogCon_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tPrejob_1_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
							talendStats_STATS.addMessage("failure",errorComponent, end_Hash.get(errorComponent)-start_Hash.get(errorComponent));
							talendStats_STATSProcess(globalMap);
							
				status = "failure";
				
					tPrejob_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tJava_1_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
							talendStats_STATS.addMessage("failure",errorComponent, end_Hash.get(errorComponent)-start_Hash.get(errorComponent));
							talendStats_STATSProcess(globalMap);
							
				status = "failure";
				
					tJava_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tRedirectOutput_1_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tRedirectOutput_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tDBConnection_1_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
							talendStats_STATS.addMessage("failure",errorComponent, end_Hash.get(errorComponent)-start_Hash.get(errorComponent));
							talendStats_STATSProcess(globalMap);
							
				status = "failure";
				
					tDBConnection_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tDBInput_1_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
							talendStats_STATS.addMessage("failure",errorComponent, end_Hash.get(errorComponent)-start_Hash.get(errorComponent));
							talendStats_STATSProcess(globalMap);
							
				status = "failure";
				
					tDBInput_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tConvertType_1_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
							talendStats_STATS.addMessage("failure",errorComponent, end_Hash.get(errorComponent)-start_Hash.get(errorComponent));
							talendStats_STATSProcess(globalMap);
							
				status = "failure";
				
					tDBInput_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tSetGlobalVar_1_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
							talendStats_STATS.addMessage("failure",errorComponent, end_Hash.get(errorComponent)-start_Hash.get(errorComponent));
							talendStats_STATSProcess(globalMap);
							
				status = "failure";
				
					tDBInput_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tLogRow_1_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
							talendStats_STATS.addMessage("failure",errorComponent, end_Hash.get(errorComponent)-start_Hash.get(errorComponent));
							talendStats_STATSProcess(globalMap);
							
				status = "failure";
				
					tDBInput_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tDBInput_2_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tDBInput_2_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tMap_1_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tDBInput_2_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tDBOutput_6_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tDBInput_2_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tDBInput_7_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tDBInput_7_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tAdvancedHash_row5_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tDBInput_7_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void connectionStatsLogs_Commit_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					connectionStatsLogs_Commit_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void connectionStatsLogs_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					connectionStatsLogs_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void talendStats_STATS_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
							talendStats_DB_error(exception, errorComponent, globalMap);
						
						}
					
			public void talendStats_DB_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
							talendStats_CONSOLE_error(exception, errorComponent, globalMap);
						
						}
					
			public void talendStats_CONSOLE_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					talendStats_STATS_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void talendLogs_LOGS_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
							talendLogs_DB_error(exception, errorComponent, globalMap);
						
						}
					
			public void talendLogs_DB_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
							talendLogs_CONSOLE_error(exception, errorComponent, globalMap);
						
						}
					
			public void talendLogs_CONSOLE_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					talendLogs_LOGS_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void talendMeter_METTER_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
							talendMeter_DB_error(exception, errorComponent, globalMap);
						
						}
					
			public void talendMeter_DB_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
							talendMeter_CONSOLE_error(exception, errorComponent, globalMap);
						
						}
					
			public void talendMeter_CONSOLE_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					talendMeter_METTER_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void preStaLogCon_onSubJobError(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {

resumeUtil.addLog("SYSTEM_LOG", "NODE:"+ errorComponent, "", Thread.currentThread().getId()+ "", "FATAL", "", exception.getMessage(), ResumeUtil.getExceptionStackTrace(exception),"");

			}
			public void tPrejob_1_onSubJobError(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {

resumeUtil.addLog("SYSTEM_LOG", "NODE:"+ errorComponent, "", Thread.currentThread().getId()+ "", "FATAL", "", exception.getMessage(), ResumeUtil.getExceptionStackTrace(exception),"");

			}
			public void tJava_1_onSubJobError(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {

resumeUtil.addLog("SYSTEM_LOG", "NODE:"+ errorComponent, "", Thread.currentThread().getId()+ "", "FATAL", "", exception.getMessage(), ResumeUtil.getExceptionStackTrace(exception),"");

			}
			public void tRedirectOutput_1_onSubJobError(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {

resumeUtil.addLog("SYSTEM_LOG", "NODE:"+ errorComponent, "", Thread.currentThread().getId()+ "", "FATAL", "", exception.getMessage(), ResumeUtil.getExceptionStackTrace(exception),"");

			}
			public void tDBConnection_1_onSubJobError(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {

resumeUtil.addLog("SYSTEM_LOG", "NODE:"+ errorComponent, "", Thread.currentThread().getId()+ "", "FATAL", "", exception.getMessage(), ResumeUtil.getExceptionStackTrace(exception),"");

			}
			public void tDBInput_1_onSubJobError(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {

resumeUtil.addLog("SYSTEM_LOG", "NODE:"+ errorComponent, "", Thread.currentThread().getId()+ "", "FATAL", "", exception.getMessage(), ResumeUtil.getExceptionStackTrace(exception),"");

			}
			public void tDBInput_2_onSubJobError(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {

resumeUtil.addLog("SYSTEM_LOG", "NODE:"+ errorComponent, "", Thread.currentThread().getId()+ "", "FATAL", "", exception.getMessage(), ResumeUtil.getExceptionStackTrace(exception),"");

			}
			public void tDBInput_7_onSubJobError(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {

resumeUtil.addLog("SYSTEM_LOG", "NODE:"+ errorComponent, "", Thread.currentThread().getId()+ "", "FATAL", "", exception.getMessage(), ResumeUtil.getExceptionStackTrace(exception),"");

			}
			public void connectionStatsLogs_Commit_onSubJobError(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {

resumeUtil.addLog("SYSTEM_LOG", "NODE:"+ errorComponent, "", Thread.currentThread().getId()+ "", "FATAL", "", exception.getMessage(), ResumeUtil.getExceptionStackTrace(exception),"");

			}
			public void connectionStatsLogs_onSubJobError(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {

resumeUtil.addLog("SYSTEM_LOG", "NODE:"+ errorComponent, "", Thread.currentThread().getId()+ "", "FATAL", "", exception.getMessage(), ResumeUtil.getExceptionStackTrace(exception),"");

			}
			public void talendStats_STATS_onSubJobError(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {

resumeUtil.addLog("SYSTEM_LOG", "NODE:"+ errorComponent, "", Thread.currentThread().getId()+ "", "FATAL", "", exception.getMessage(), ResumeUtil.getExceptionStackTrace(exception),"");

			}
			public void talendLogs_LOGS_onSubJobError(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {

resumeUtil.addLog("SYSTEM_LOG", "NODE:"+ errorComponent, "", Thread.currentThread().getId()+ "", "FATAL", "", exception.getMessage(), ResumeUtil.getExceptionStackTrace(exception),"");

			}
			public void talendMeter_METTER_onSubJobError(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {

resumeUtil.addLog("SYSTEM_LOG", "NODE:"+ errorComponent, "", Thread.currentThread().getId()+ "", "FATAL", "", exception.getMessage(), ResumeUtil.getExceptionStackTrace(exception),"");

			}
			





public void preStaLogConProcess(final java.util.Map<String, Object> globalMap) throws TalendException {
	globalMap.put("preStaLogCon_SUBPROCESS_STATE", 0);

 final boolean execStat = this.execStat;
	
		String iterateId = "";
	
	
	String currentComponent = "";
	java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

	try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception().getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { //start the resume
				globalResumeTicket = true;



		


	
	/**
	 * [preStaLogCon begin ] start
	 */

	

	
		
		ok_Hash.put("preStaLogCon", false);
		start_Hash.put("preStaLogCon", System.currentTimeMillis());
		
	
	currentComponent="preStaLogCon";

	
		int tos_count_preStaLogCon = 0;
		
    	class BytesLimit65535_preStaLogCon{
    		public void limitLog4jByte() throws Exception{
    			
    		}
    	}
    	
        new BytesLimit65535_preStaLogCon().limitLog4jByte();

 



/**
 * [preStaLogCon begin ] stop
 */
	
	/**
	 * [preStaLogCon main ] start
	 */

	

	
	
	currentComponent="preStaLogCon";

	

 


	tos_count_preStaLogCon++;

/**
 * [preStaLogCon main ] stop
 */
	
	/**
	 * [preStaLogCon process_data_begin ] start
	 */

	

	
	
	currentComponent="preStaLogCon";

	

 



/**
 * [preStaLogCon process_data_begin ] stop
 */
	
	/**
	 * [preStaLogCon process_data_end ] start
	 */

	

	
	
	currentComponent="preStaLogCon";

	

 



/**
 * [preStaLogCon process_data_end ] stop
 */
	
	/**
	 * [preStaLogCon end ] start
	 */

	

	
	
	currentComponent="preStaLogCon";

	

 

ok_Hash.put("preStaLogCon", true);
end_Hash.put("preStaLogCon", System.currentTimeMillis());

				if(execStat){   
   	 				runStat.updateStatOnConnection("after_preStaLogCon_connectionStatsLogs", 0, "ok");
				}
				connectionStatsLogsProcess(globalMap);



/**
 * [preStaLogCon end ] stop
 */
				}//end the resume

				



	
			}catch(java.lang.Exception e){	
				
				TalendException te = new TalendException(e, currentComponent, globalMap);
				
				throw te;
			}catch(java.lang.Error error){	
				
					runStat.stopThreadStat();
				
				throw error;
			}finally{
				
				try{
					
	
	/**
	 * [preStaLogCon finally ] start
	 */

	

	
	
	currentComponent="preStaLogCon";

	

 



/**
 * [preStaLogCon finally ] stop
 */
				}catch(java.lang.Exception e){	
					//ignore
				}catch(java.lang.Error error){
					//ignore
				}
				resourceMap = null;
			}
		

		globalMap.put("preStaLogCon_SUBPROCESS_STATE", 1);
	}
	

public void tPrejob_1Process(final java.util.Map<String, Object> globalMap) throws TalendException {
	globalMap.put("tPrejob_1_SUBPROCESS_STATE", 0);

 final boolean execStat = this.execStat;
	
		String iterateId = "";
	
	
	String currentComponent = "";
	java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

	try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception().getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { //start the resume
				globalResumeTicket = true;



		


	
	/**
	 * [tPrejob_1 begin ] start
	 */

	

	
		
		ok_Hash.put("tPrejob_1", false);
		start_Hash.put("tPrejob_1", System.currentTimeMillis());
		
				talendStats_STATS.addMessage("begin","tPrejob_1");
				talendStats_STATSProcess(globalMap);
			
	
	currentComponent="tPrejob_1";

	
		int tos_count_tPrejob_1 = 0;
		
    	class BytesLimit65535_tPrejob_1{
    		public void limitLog4jByte() throws Exception{
    			
    		}
    	}
    	
        new BytesLimit65535_tPrejob_1().limitLog4jByte();

 



/**
 * [tPrejob_1 begin ] stop
 */
	
	/**
	 * [tPrejob_1 main ] start
	 */

	

	
	
	currentComponent="tPrejob_1";

	

 


	tos_count_tPrejob_1++;

/**
 * [tPrejob_1 main ] stop
 */
	
	/**
	 * [tPrejob_1 process_data_begin ] start
	 */

	

	
	
	currentComponent="tPrejob_1";

	

 



/**
 * [tPrejob_1 process_data_begin ] stop
 */
	
	/**
	 * [tPrejob_1 process_data_end ] start
	 */

	

	
	
	currentComponent="tPrejob_1";

	

 



/**
 * [tPrejob_1 process_data_end ] stop
 */
	
	/**
	 * [tPrejob_1 end ] start
	 */

	

	
	
	currentComponent="tPrejob_1";

	

 

ok_Hash.put("tPrejob_1", true);
end_Hash.put("tPrejob_1", System.currentTimeMillis());

talendStats_STATS.addMessage("end","tPrejob_1", end_Hash.get("tPrejob_1")-start_Hash.get("tPrejob_1"));
talendStats_STATSProcess(globalMap);
				if(execStat){   
   	 				runStat.updateStatOnConnection("OnComponentOk1", 0, "ok");
				}
				tJava_1Process(globalMap);



/**
 * [tPrejob_1 end ] stop
 */
				}//end the resume

				



	
			}catch(java.lang.Exception e){	
				
				TalendException te = new TalendException(e, currentComponent, globalMap);
				
				throw te;
			}catch(java.lang.Error error){	
				
					runStat.stopThreadStat();
				
				throw error;
			}finally{
				
				try{
					
	
	/**
	 * [tPrejob_1 finally ] start
	 */

	

	
	
	currentComponent="tPrejob_1";

	

 



/**
 * [tPrejob_1 finally ] stop
 */
				}catch(java.lang.Exception e){	
					//ignore
				}catch(java.lang.Error error){
					//ignore
				}
				resourceMap = null;
			}
		

		globalMap.put("tPrejob_1_SUBPROCESS_STATE", 1);
	}
	

public void tJava_1Process(final java.util.Map<String, Object> globalMap) throws TalendException {
	globalMap.put("tJava_1_SUBPROCESS_STATE", 0);

 final boolean execStat = this.execStat;
	
		String iterateId = "";
	
	
	String currentComponent = "";
	java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

	try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception().getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { //start the resume
				globalResumeTicket = true;



		


	
	/**
	 * [tJava_1 begin ] start
	 */

	

	
		
		ok_Hash.put("tJava_1", false);
		start_Hash.put("tJava_1", System.currentTimeMillis());
		
				talendStats_STATS.addMessage("begin","tJava_1");
				talendStats_STATSProcess(globalMap);
			
	
	currentComponent="tJava_1";

	
		int tos_count_tJava_1 = 0;
		
    	class BytesLimit65535_tJava_1{
    		public void limitLog4jByte() throws Exception{
    			
    		}
    	}
    	
        new BytesLimit65535_tJava_1().limitLog4jByte();


String foo = "bar";
String os = System.getProperty("os.name").toLowerCase();
context.tableName="LEDGER_TRANSACTION2";

System.out.println("Operating System Is: "+os);
System.out.println("Table Name Is: "+ context.tableName);

if(os.indexOf("win") >= 0){
context.filePath=System.getProperty("user.home")+"/Logs";
}else if (os.indexOf("mac os x")>=0){ 
context.filePath=System.getProperty("user.home")+"/Documents/Logs";
}else {
context.filePath="/Talend/Jobs/Logs";
}
 



/**
 * [tJava_1 begin ] stop
 */
	
	/**
	 * [tJava_1 main ] start
	 */

	

	
	
	currentComponent="tJava_1";

	

 


	tos_count_tJava_1++;

/**
 * [tJava_1 main ] stop
 */
	
	/**
	 * [tJava_1 process_data_begin ] start
	 */

	

	
	
	currentComponent="tJava_1";

	

 



/**
 * [tJava_1 process_data_begin ] stop
 */
	
	/**
	 * [tJava_1 process_data_end ] start
	 */

	

	
	
	currentComponent="tJava_1";

	

 



/**
 * [tJava_1 process_data_end ] stop
 */
	
	/**
	 * [tJava_1 end ] start
	 */

	

	
	
	currentComponent="tJava_1";

	

 

ok_Hash.put("tJava_1", true);
end_Hash.put("tJava_1", System.currentTimeMillis());

talendStats_STATS.addMessage("end","tJava_1", end_Hash.get("tJava_1")-start_Hash.get("tJava_1"));
talendStats_STATSProcess(globalMap);



/**
 * [tJava_1 end ] stop
 */
				}//end the resume

				
				    			if(resumeEntryMethodName == null || globalResumeTicket){
				    				resumeUtil.addLog("CHECKPOINT", "CONNECTION:SUBJOB_OK:tJava_1:OnSubjobOk", "", Thread.currentThread().getId() + "", "", "", "", "", "");
								}	    				    			
					    	
								if(execStat){    	
									runStat.updateStatOnConnection("OnSubjobOk3", 0, "ok");
								} 
							
							tRedirectOutput_1Process(globalMap); 
						



	
			}catch(java.lang.Exception e){	
				
				TalendException te = new TalendException(e, currentComponent, globalMap);
				
				throw te;
			}catch(java.lang.Error error){	
				
					runStat.stopThreadStat();
				
				throw error;
			}finally{
				
				try{
					
	
	/**
	 * [tJava_1 finally ] start
	 */

	

	
	
	currentComponent="tJava_1";

	

 



/**
 * [tJava_1 finally ] stop
 */
				}catch(java.lang.Exception e){	
					//ignore
				}catch(java.lang.Error error){
					//ignore
				}
				resourceMap = null;
			}
		

		globalMap.put("tJava_1_SUBPROCESS_STATE", 1);
	}
	

public void tRedirectOutput_1Process(final java.util.Map<String, Object> globalMap) throws TalendException {
	globalMap.put("tRedirectOutput_1_SUBPROCESS_STATE", 0);

 final boolean execStat = this.execStat;
	
		String iterateId = "";
	
	
	String currentComponent = "";
	java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

	try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception().getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { //start the resume
				globalResumeTicket = true;



		


	
	/**
	 * [tRedirectOutput_1 begin ] start
	 */

	

	
		
		ok_Hash.put("tRedirectOutput_1", false);
		start_Hash.put("tRedirectOutput_1", System.currentTimeMillis());
		
	
	currentComponent="tRedirectOutput_1";

	
		int tos_count_tRedirectOutput_1 = 0;
		
    	class BytesLimit65535_tRedirectOutput_1{
    		public void limitLog4jByte() throws Exception{
    			
    		}
    	}
    	
        new BytesLimit65535_tRedirectOutput_1().limitLog4jByte();
String fileOutName_tRedirectOutput_1 = (new java.io.File(context.filePath+"/"+jobName+"_console")).getAbsolutePath().replace("\\","/");
java.io.File fileOut_tRedirectOutput_1 = new java.io.File(fileOutName_tRedirectOutput_1);
System.setOut(new java.io.PrintStream(new java.io.FileOutputStream(fileOut_tRedirectOutput_1, true), true));
System.out.println("Job Started at : " + TalendDate.formatDate("yyyy-MM-dd HH:mm:ss", new Date(startTime)) + "");
String fileErrName_tRedirectOutput_1 = (new java.io.File(context.filePath+"/"+jobName+"_error")).getAbsolutePath().replace("\\","/");
java.io.File fileErr_tRedirectOutput_1 = new java.io.File(fileErrName_tRedirectOutput_1);
System.setErr(new java.io.PrintStream(new java.io.FileOutputStream(fileErr_tRedirectOutput_1, true), true));
System.err.println("Job Started at : " + TalendDate.formatDate("yyyy-MM-dd HH:mm:ss", new Date(startTime)) + "");
 



/**
 * [tRedirectOutput_1 begin ] stop
 */
	
	/**
	 * [tRedirectOutput_1 main ] start
	 */

	

	
	
	currentComponent="tRedirectOutput_1";

	

 


	tos_count_tRedirectOutput_1++;

/**
 * [tRedirectOutput_1 main ] stop
 */
	
	/**
	 * [tRedirectOutput_1 process_data_begin ] start
	 */

	

	
	
	currentComponent="tRedirectOutput_1";

	

 



/**
 * [tRedirectOutput_1 process_data_begin ] stop
 */
	
	/**
	 * [tRedirectOutput_1 process_data_end ] start
	 */

	

	
	
	currentComponent="tRedirectOutput_1";

	

 



/**
 * [tRedirectOutput_1 process_data_end ] stop
 */
	
	/**
	 * [tRedirectOutput_1 end ] start
	 */

	

	
	
	currentComponent="tRedirectOutput_1";

	

 

ok_Hash.put("tRedirectOutput_1", true);
end_Hash.put("tRedirectOutput_1", System.currentTimeMillis());

				if(execStat){   
   	 				runStat.updateStatOnConnection("OnComponentOk2", 0, "ok");
				}
				tDBConnection_1Process(globalMap);



/**
 * [tRedirectOutput_1 end ] stop
 */
				}//end the resume

				



	
			}catch(java.lang.Exception e){	
				
				TalendException te = new TalendException(e, currentComponent, globalMap);
				
				throw te;
			}catch(java.lang.Error error){	
				
					runStat.stopThreadStat();
				
				throw error;
			}finally{
				
				try{
					
	
	/**
	 * [tRedirectOutput_1 finally ] start
	 */

	

	
	
	currentComponent="tRedirectOutput_1";

	

 



/**
 * [tRedirectOutput_1 finally ] stop
 */
				}catch(java.lang.Exception e){	
					//ignore
				}catch(java.lang.Error error){
					//ignore
				}
				resourceMap = null;
			}
		

		globalMap.put("tRedirectOutput_1_SUBPROCESS_STATE", 1);
	}
	

public void tDBConnection_1Process(final java.util.Map<String, Object> globalMap) throws TalendException {
	globalMap.put("tDBConnection_1_SUBPROCESS_STATE", 0);

 final boolean execStat = this.execStat;
	
		String iterateId = "";
	
	
	String currentComponent = "";
	java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

	try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception().getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { //start the resume
				globalResumeTicket = true;



		


	
	/**
	 * [tDBConnection_1 begin ] start
	 */

	

	
		
		ok_Hash.put("tDBConnection_1", false);
		start_Hash.put("tDBConnection_1", System.currentTimeMillis());
		
				talendStats_STATS.addMessage("begin","tDBConnection_1");
				talendStats_STATSProcess(globalMap);
			
	
	currentComponent="tDBConnection_1";

	
		int tos_count_tDBConnection_1 = 0;
		
    	class BytesLimit65535_tDBConnection_1{
    		public void limitLog4jByte() throws Exception{
    			
    		}
    	}
    	
        new BytesLimit65535_tDBConnection_1().limitLog4jByte();


	
				String url_tDBConnection_1 = "jdbc:vertica://" + context.Vertica_ODS_Server + ":" + context.Vertica_ODS_Port + "/" + context.Vertica_ODS_Database + "?" + context.Vertica_ODS_AdditionalParams;

	String dbUser_tDBConnection_1 = context.Vertica_ODS_Login;
	
	
		
	final String decryptedPassword_tDBConnection_1 = context.Vertica_ODS_Password; 
		String dbPwd_tDBConnection_1 = decryptedPassword_tDBConnection_1;
	

	java.sql.Connection conn_tDBConnection_1 = null;
	
	
			String sharedConnectionName_tDBConnection_1 = "conVertica";
			conn_tDBConnection_1 = SharedDBConnection.getDBConnection("com.vertica.jdbc.Driver",url_tDBConnection_1,dbUser_tDBConnection_1 , dbPwd_tDBConnection_1 , sharedConnectionName_tDBConnection_1);
	if (null != conn_tDBConnection_1) {
		
			conn_tDBConnection_1.setAutoCommit(true);
	}

	globalMap.put("conn_tDBConnection_1",conn_tDBConnection_1);
	globalMap.put("dbschema_tDBConnection_1", context.Vertica_ODS_Schema);
	globalMap.put("db_tDBConnection_1",context.Vertica_ODS_Database);

 



/**
 * [tDBConnection_1 begin ] stop
 */
	
	/**
	 * [tDBConnection_1 main ] start
	 */

	

	
	
	currentComponent="tDBConnection_1";

	

 


	tos_count_tDBConnection_1++;

/**
 * [tDBConnection_1 main ] stop
 */
	
	/**
	 * [tDBConnection_1 process_data_begin ] start
	 */

	

	
	
	currentComponent="tDBConnection_1";

	

 



/**
 * [tDBConnection_1 process_data_begin ] stop
 */
	
	/**
	 * [tDBConnection_1 process_data_end ] start
	 */

	

	
	
	currentComponent="tDBConnection_1";

	

 



/**
 * [tDBConnection_1 process_data_end ] stop
 */
	
	/**
	 * [tDBConnection_1 end ] start
	 */

	

	
	
	currentComponent="tDBConnection_1";

	

 

ok_Hash.put("tDBConnection_1", true);
end_Hash.put("tDBConnection_1", System.currentTimeMillis());

talendStats_STATS.addMessage("end","tDBConnection_1", end_Hash.get("tDBConnection_1")-start_Hash.get("tDBConnection_1"));
talendStats_STATSProcess(globalMap);



/**
 * [tDBConnection_1 end ] stop
 */
				}//end the resume

				
				    			if(resumeEntryMethodName == null || globalResumeTicket){
				    				resumeUtil.addLog("CHECKPOINT", "CONNECTION:SUBJOB_OK:tDBConnection_1:OnSubjobOk", "", Thread.currentThread().getId() + "", "", "", "", "", "");
								}	    				    			
					    	
								if(execStat){    	
									runStat.updateStatOnConnection("OnSubjobOk1", 0, "ok");
								} 
							
							tDBInput_1Process(globalMap); 
						



	
			}catch(java.lang.Exception e){	
				
				TalendException te = new TalendException(e, currentComponent, globalMap);
				
				throw te;
			}catch(java.lang.Error error){	
				
					runStat.stopThreadStat();
				
				throw error;
			}finally{
				
				try{
					
	
	/**
	 * [tDBConnection_1 finally ] start
	 */

	

	
	
	currentComponent="tDBConnection_1";

	

 



/**
 * [tDBConnection_1 finally ] stop
 */
				}catch(java.lang.Exception e){	
					//ignore
				}catch(java.lang.Error error){
					//ignore
				}
				resourceMap = null;
			}
		

		globalMap.put("tDBConnection_1_SUBPROCESS_STATE", 1);
	}
	


public static class row1Struct implements routines.system.IPersistableRow<row1Struct> {
    final static byte[] commonByteArrayLock_GIFMIS_ledger2_12 = new byte[0];
    static byte[] commonByteArray_GIFMIS_ledger2_12 = new byte[0];

	
			    public String max_date;

				public String getMax_date () {
					return this.max_date;
				}
				



	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_GIFMIS_ledger2_12.length) {
				if(length < 1024 && commonByteArray_GIFMIS_ledger2_12.length == 0) {
   					commonByteArray_GIFMIS_ledger2_12 = new byte[1024];
				} else {
   					commonByteArray_GIFMIS_ledger2_12 = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_GIFMIS_ledger2_12, 0, length);
			strReturn = new String(commonByteArray_GIFMIS_ledger2_12, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_GIFMIS_ledger2_12) {

        	try {

        		int length = 0;
		
					this.max_date = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// String
				
						writeString(this.max_date,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("max_date="+max_date);
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(row1Struct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}

public static class row4Struct implements routines.system.IPersistableRow<row4Struct> {
    final static byte[] commonByteArrayLock_GIFMIS_ledger2_12 = new byte[0];
    static byte[] commonByteArray_GIFMIS_ledger2_12 = new byte[0];

	
			    public String max_date;

				public String getMax_date () {
					return this.max_date;
				}
				



	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_GIFMIS_ledger2_12.length) {
				if(length < 1024 && commonByteArray_GIFMIS_ledger2_12.length == 0) {
   					commonByteArray_GIFMIS_ledger2_12 = new byte[1024];
				} else {
   					commonByteArray_GIFMIS_ledger2_12 = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_GIFMIS_ledger2_12, 0, length);
			strReturn = new String(commonByteArray_GIFMIS_ledger2_12, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_GIFMIS_ledger2_12) {

        	try {

        		int length = 0;
		
					this.max_date = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// String
				
						writeString(this.max_date,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("max_date="+max_date);
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(row4Struct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}

public static class row2Struct implements routines.system.IPersistableRow<row2Struct> {
    final static byte[] commonByteArrayLock_GIFMIS_ledger2_12 = new byte[0];
    static byte[] commonByteArray_GIFMIS_ledger2_12 = new byte[0];

	
			    public java.util.Date max_date;

				public java.util.Date getMax_date () {
					return this.max_date;
				}
				



	private java.util.Date readDate(ObjectInputStream dis) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(dis.readLong());
		}
		return dateReturn;
	}

    private void writeDate(java.util.Date date1, ObjectOutputStream dos) throws IOException{
		if(date1 == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeLong(date1.getTime());
    	}
    }

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_GIFMIS_ledger2_12) {

        	try {

        		int length = 0;
		
					this.max_date = readDate(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// java.util.Date
				
						writeDate(this.max_date,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("max_date="+String.valueOf(max_date));
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(row2Struct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}
public void tDBInput_1Process(final java.util.Map<String, Object> globalMap) throws TalendException {
	globalMap.put("tDBInput_1_SUBPROCESS_STATE", 0);

 final boolean execStat = this.execStat;
	
		String iterateId = "";
	
	
	String currentComponent = "";
	java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

	try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception().getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { //start the resume
				globalResumeTicket = true;



		row2Struct row2 = new row2Struct();
row4Struct row4 = new row4Struct();
row4Struct row1 = row4;






	
	/**
	 * [tLogRow_1 begin ] start
	 */

	

	
		
		ok_Hash.put("tLogRow_1", false);
		start_Hash.put("tLogRow_1", System.currentTimeMillis());
		
				talendStats_STATS.addMessage("begin","tLogRow_1");
				talendStats_STATSProcess(globalMap);
			
	
	currentComponent="tLogRow_1";

	
			if (execStat) {
				if(resourceMap.get("inIterateVComp") == null){
					
						runStat.updateStatOnConnection("row1" + iterateId, 0, 0);
					
				}
			} 

		
		int tos_count_tLogRow_1 = 0;
		
    	class BytesLimit65535_tLogRow_1{
    		public void limitLog4jByte() throws Exception{
    			
    		}
    	}
    	
        new BytesLimit65535_tLogRow_1().limitLog4jByte();

	///////////////////////
	
         class Util_tLogRow_1 {

        String[] des_top = { ".", ".", "-", "+" };

        String[] des_head = { "|=", "=|", "-", "+" };

        String[] des_bottom = { "'", "'", "-", "+" };

        String name="";

        java.util.List<String[]> list = new java.util.ArrayList<String[]>();

        int[] colLengths = new int[1];

        public void addRow(String[] row) {

            for (int i = 0; i < 1; i++) {
                if (row[i]!=null) {
                  colLengths[i] = Math.max(colLengths[i], row[i].length());
                }
            }
            list.add(row);
        }

        public void setTableName(String name) {

            this.name = name;
        }

            public StringBuilder format() {
            
                StringBuilder sb = new StringBuilder();
  
            
                    sb.append(print(des_top));
    
                    int totals = 0;
                    for (int i = 0; i < colLengths.length; i++) {
                        totals = totals + colLengths[i];
                    }
    
                    // name
                    sb.append("|");
                    int k = 0;
                    for (k = 0; k < (totals + 0 - name.length()) / 2; k++) {
                        sb.append(' ');
                    }
                    sb.append(name);
                    for (int i = 0; i < totals + 0 - name.length() - k; i++) {
                        sb.append(' ');
                    }
                    sb.append("|\n");

                    // head and rows
                    sb.append(print(des_head));
                    for (int i = 0; i < list.size(); i++) {
    
                        String[] row = list.get(i);
    
                        java.util.Formatter formatter = new java.util.Formatter(new StringBuilder());
                        
                        StringBuilder sbformat = new StringBuilder();                                             
        			        sbformat.append("|%1$-");
        			        sbformat.append(colLengths[0]);
        			        sbformat.append("s");
        			                      
                        sbformat.append("|\n");                    
       
                        formatter.format(sbformat.toString(), (Object[])row);	
                                
                        sb.append(formatter.toString());
                        if (i == 0)
                            sb.append(print(des_head)); // print the head
                    }
    
                    // end
                    sb.append(print(des_bottom));
                    return sb;
                }
            

            private StringBuilder print(String[] fillChars) {
                StringBuilder sb = new StringBuilder();
                //first column
                sb.append(fillChars[0]);	                

                  
                    //last column
                    for (int i = 0; i < colLengths[0] - fillChars[0].length() - fillChars[1].length()+2; i++) {
                        sb.append(fillChars[2]);
                    }         
                sb.append(fillChars[1]);
                sb.append("\n");               
                return sb;
            }
            
            public boolean isTableEmpty(){
            	if (list.size() > 1)
            		return false;
            	return true;
            }
        }
        Util_tLogRow_1 util_tLogRow_1 = new Util_tLogRow_1();
        util_tLogRow_1.setTableName("tLogRow_1");
        util_tLogRow_1.addRow(new String[]{"max_date",});        
 		StringBuilder strBuffer_tLogRow_1 = null;
		int nb_line_tLogRow_1 = 0;
///////////////////////    			



 



/**
 * [tLogRow_1 begin ] stop
 */



	
	/**
	 * [tSetGlobalVar_1 begin ] start
	 */

	

	
		
		ok_Hash.put("tSetGlobalVar_1", false);
		start_Hash.put("tSetGlobalVar_1", System.currentTimeMillis());
		
				talendStats_STATS.addMessage("begin","tSetGlobalVar_1");
				talendStats_STATSProcess(globalMap);
			
	
	currentComponent="tSetGlobalVar_1";

	
			if (execStat) {
				if(resourceMap.get("inIterateVComp") == null){
					
						runStat.updateStatOnConnection("row4" + iterateId, 0, 0);
					
				}
			} 

		
		int tos_count_tSetGlobalVar_1 = 0;
		
    	class BytesLimit65535_tSetGlobalVar_1{
    		public void limitLog4jByte() throws Exception{
    			
    		}
    	}
    	
        new BytesLimit65535_tSetGlobalVar_1().limitLog4jByte();

 



/**
 * [tSetGlobalVar_1 begin ] stop
 */



	
	/**
	 * [tConvertType_1 begin ] start
	 */

	

	
		
		ok_Hash.put("tConvertType_1", false);
		start_Hash.put("tConvertType_1", System.currentTimeMillis());
		
				talendStats_STATS.addMessage("begin","tConvertType_1");
				talendStats_STATSProcess(globalMap);
			
	
	currentComponent="tConvertType_1";

	
			if (execStat) {
				if(resourceMap.get("inIterateVComp") == null){
					
						runStat.updateStatOnConnection("row2" + iterateId, 0, 0);
					
				}
			} 

		
		int tos_count_tConvertType_1 = 0;
		
    	class BytesLimit65535_tConvertType_1{
    		public void limitLog4jByte() throws Exception{
    			
    		}
    	}
    	
        new BytesLimit65535_tConvertType_1().limitLog4jByte();
	int nb_line_tConvertType_1 = 0;  
 



/**
 * [tConvertType_1 begin ] stop
 */



	
	/**
	 * [tDBInput_1 begin ] start
	 */

	

	
		
		ok_Hash.put("tDBInput_1", false);
		start_Hash.put("tDBInput_1", System.currentTimeMillis());
		
				talendStats_STATS.addMessage("begin","tDBInput_1");
				talendStats_STATSProcess(globalMap);
			
	
	currentComponent="tDBInput_1";

	
		int tos_count_tDBInput_1 = 0;
		
    	class BytesLimit65535_tDBInput_1{
    		public void limitLog4jByte() throws Exception{
    			
    		}
    	}
    	
        new BytesLimit65535_tDBInput_1().limitLog4jByte();
	
    
	
		    int nb_line_tDBInput_1 = 0;
		    java.sql.Connection conn_tDBInput_1 = null;
		        conn_tDBInput_1 = (java.sql.Connection)globalMap.get("conn_tDBConnection_1");
				
		    
			java.sql.Statement stmt_tDBInput_1 = conn_tDBInput_1.createStatement();

		    String dbquery_tDBInput_1 = "SELECT IFNULL(MAX(updated_date),'1900-01-01') as max_date from ODS."+context.tableName;
			

            	globalMap.put("tDBInput_1_QUERY",dbquery_tDBInput_1);
		    java.sql.ResultSet rs_tDBInput_1 = null;

		    try {
		    	rs_tDBInput_1 = stmt_tDBInput_1.executeQuery(dbquery_tDBInput_1);
		    	java.sql.ResultSetMetaData rsmd_tDBInput_1 = rs_tDBInput_1.getMetaData();
		    	int colQtyInRs_tDBInput_1 = rsmd_tDBInput_1.getColumnCount();

		    String tmpContent_tDBInput_1 = null;
		    
		    
		    while (rs_tDBInput_1.next()) {
		        nb_line_tDBInput_1++;
		        
							if(colQtyInRs_tDBInput_1 < 1) {
								row2.max_date = null;
							} else {
										
			row2.max_date = routines.system.JDBCUtil.getDate(rs_tDBInput_1, 1);
		                    }
					


 



/**
 * [tDBInput_1 begin ] stop
 */
	
	/**
	 * [tDBInput_1 main ] start
	 */

	

	
	
	currentComponent="tDBInput_1";

	

 


	tos_count_tDBInput_1++;

/**
 * [tDBInput_1 main ] stop
 */
	
	/**
	 * [tDBInput_1 process_data_begin ] start
	 */

	

	
	
	currentComponent="tDBInput_1";

	

 



/**
 * [tDBInput_1 process_data_begin ] stop
 */

	
	/**
	 * [tConvertType_1 main ] start
	 */

	

	
	
	currentComponent="tConvertType_1";

	

			//row2
			//row2


			
				if(execStat){
					runStat.updateStatOnConnection("row2"+iterateId,1, 1);
				} 
			

		


  row4 = new row4Struct();
  boolean bHasError_tConvertType_1 = false;             
          try {
              row4.max_date=TypeConvert.Date2String(row2.max_date, "yyyy-MM-dd HH:mm:ss");
        	            
          } catch(java.lang.Exception e){
            bHasError_tConvertType_1 = true;            
              System.err.println(e.getMessage());          
          }
      if (bHasError_tConvertType_1) {row4 = null;}

  nb_line_tConvertType_1 ++ ;
 


	tos_count_tConvertType_1++;

/**
 * [tConvertType_1 main ] stop
 */
	
	/**
	 * [tConvertType_1 process_data_begin ] start
	 */

	

	
	
	currentComponent="tConvertType_1";

	

 



/**
 * [tConvertType_1 process_data_begin ] stop
 */
// Start of branch "row4"
if(row4 != null) { 



	
	/**
	 * [tSetGlobalVar_1 main ] start
	 */

	

	
	
	currentComponent="tSetGlobalVar_1";

	

			//row4
			//row4


			
				if(execStat){
					runStat.updateStatOnConnection("row4"+iterateId,1, 1);
				} 
			

		

globalMap.put("myKey", row4.max_date);

 
     row1 = row4;


	tos_count_tSetGlobalVar_1++;

/**
 * [tSetGlobalVar_1 main ] stop
 */
	
	/**
	 * [tSetGlobalVar_1 process_data_begin ] start
	 */

	

	
	
	currentComponent="tSetGlobalVar_1";

	

 



/**
 * [tSetGlobalVar_1 process_data_begin ] stop
 */

	
	/**
	 * [tLogRow_1 main ] start
	 */

	

	
	
	currentComponent="tLogRow_1";

	

			//row1
			//row1


			
				if(execStat){
					runStat.updateStatOnConnection("row1"+iterateId,1, 1);
				} 
			

		
///////////////////////		
						

				
				String[] row_tLogRow_1 = new String[1];
   				
	    		if(row1.max_date != null) { //              
                 row_tLogRow_1[0]=    						    
				                String.valueOf(row1.max_date)			
					          ;	
							
	    		} //			
    			 

				util_tLogRow_1.addRow(row_tLogRow_1);	
				nb_line_tLogRow_1++;
//////

//////                    
                    
///////////////////////    			

 


	tos_count_tLogRow_1++;

/**
 * [tLogRow_1 main ] stop
 */
	
	/**
	 * [tLogRow_1 process_data_begin ] start
	 */

	

	
	
	currentComponent="tLogRow_1";

	

 



/**
 * [tLogRow_1 process_data_begin ] stop
 */
	
	/**
	 * [tLogRow_1 process_data_end ] start
	 */

	

	
	
	currentComponent="tLogRow_1";

	

 



/**
 * [tLogRow_1 process_data_end ] stop
 */



	
	/**
	 * [tSetGlobalVar_1 process_data_end ] start
	 */

	

	
	
	currentComponent="tSetGlobalVar_1";

	

 



/**
 * [tSetGlobalVar_1 process_data_end ] stop
 */

} // End of branch "row4"




	
	/**
	 * [tConvertType_1 process_data_end ] start
	 */

	

	
	
	currentComponent="tConvertType_1";

	

 



/**
 * [tConvertType_1 process_data_end ] stop
 */



	
	/**
	 * [tDBInput_1 process_data_end ] start
	 */

	

	
	
	currentComponent="tDBInput_1";

	

 



/**
 * [tDBInput_1 process_data_end ] stop
 */
	
	/**
	 * [tDBInput_1 end ] start
	 */

	

	
	
	currentComponent="tDBInput_1";

	

	}
}finally{
	stmt_tDBInput_1.close();

}
globalMap.put("tDBInput_1_NB_LINE",nb_line_tDBInput_1);

 

ok_Hash.put("tDBInput_1", true);
end_Hash.put("tDBInput_1", System.currentTimeMillis());

talendStats_STATS.addMessage("end","tDBInput_1", end_Hash.get("tDBInput_1")-start_Hash.get("tDBInput_1"));
talendStats_STATSProcess(globalMap);



/**
 * [tDBInput_1 end ] stop
 */

	
	/**
	 * [tConvertType_1 end ] start
	 */

	

	
	
	currentComponent="tConvertType_1";

	
      globalMap.put("tConvertType_1_NB_LINE", nb_line_tConvertType_1);
			if(execStat){
				if(resourceMap.get("inIterateVComp") == null || !((Boolean)resourceMap.get("inIterateVComp"))){
			 		runStat.updateStatOnConnection("row2"+iterateId,2, 0); 
			 	}
			}
		
 

ok_Hash.put("tConvertType_1", true);
end_Hash.put("tConvertType_1", System.currentTimeMillis());

talendStats_STATS.addMessage("end","tConvertType_1", end_Hash.get("tConvertType_1")-start_Hash.get("tConvertType_1"));
talendStats_STATSProcess(globalMap);



/**
 * [tConvertType_1 end ] stop
 */

	
	/**
	 * [tSetGlobalVar_1 end ] start
	 */

	

	
	
	currentComponent="tSetGlobalVar_1";

	

			if(execStat){
				if(resourceMap.get("inIterateVComp") == null || !((Boolean)resourceMap.get("inIterateVComp"))){
			 		runStat.updateStatOnConnection("row4"+iterateId,2, 0); 
			 	}
			}
		
 

ok_Hash.put("tSetGlobalVar_1", true);
end_Hash.put("tSetGlobalVar_1", System.currentTimeMillis());

talendStats_STATS.addMessage("end","tSetGlobalVar_1", end_Hash.get("tSetGlobalVar_1")-start_Hash.get("tSetGlobalVar_1"));
talendStats_STATSProcess(globalMap);



/**
 * [tSetGlobalVar_1 end ] stop
 */

	
	/**
	 * [tLogRow_1 end ] start
	 */

	

	
	
	currentComponent="tLogRow_1";

	


//////

                    
                    java.io.PrintStream consoleOut_tLogRow_1 = null;
                    if (globalMap.get("tLogRow_CONSOLE")!=null)
                    {
                    	consoleOut_tLogRow_1 = (java.io.PrintStream) globalMap.get("tLogRow_CONSOLE");
                    }
                    else
                    {
                    	consoleOut_tLogRow_1 = new java.io.PrintStream(new java.io.BufferedOutputStream(System.out));
                    	globalMap.put("tLogRow_CONSOLE",consoleOut_tLogRow_1);
                    }
                    
                    consoleOut_tLogRow_1.println(util_tLogRow_1.format().toString());
                    consoleOut_tLogRow_1.flush();
//////
globalMap.put("tLogRow_1_NB_LINE",nb_line_tLogRow_1);

///////////////////////    			

			if(execStat){
				if(resourceMap.get("inIterateVComp") == null || !((Boolean)resourceMap.get("inIterateVComp"))){
			 		runStat.updateStatOnConnection("row1"+iterateId,2, 0); 
			 	}
			}
		
 

ok_Hash.put("tLogRow_1", true);
end_Hash.put("tLogRow_1", System.currentTimeMillis());

talendStats_STATS.addMessage("end","tLogRow_1", end_Hash.get("tLogRow_1")-start_Hash.get("tLogRow_1"));
talendStats_STATSProcess(globalMap);



/**
 * [tLogRow_1 end ] stop
 */









				}//end the resume

				
				    			if(resumeEntryMethodName == null || globalResumeTicket){
				    				resumeUtil.addLog("CHECKPOINT", "CONNECTION:SUBJOB_OK:tDBInput_1:OnSubjobOk", "", Thread.currentThread().getId() + "", "", "", "", "", "");
								}	    				    			
					    	
								if(execStat){    	
									runStat.updateStatOnConnection("OnSubjobOk2", 0, "ok");
								} 
							
							tDBInput_2Process(globalMap); 
						



	
			}catch(java.lang.Exception e){	
				
				TalendException te = new TalendException(e, currentComponent, globalMap);
				
				throw te;
			}catch(java.lang.Error error){	
				
					runStat.stopThreadStat();
				
				throw error;
			}finally{
				
				try{
					
	
	/**
	 * [tDBInput_1 finally ] start
	 */

	

	
	
	currentComponent="tDBInput_1";

	

 



/**
 * [tDBInput_1 finally ] stop
 */

	
	/**
	 * [tConvertType_1 finally ] start
	 */

	

	
	
	currentComponent="tConvertType_1";

	

 



/**
 * [tConvertType_1 finally ] stop
 */

	
	/**
	 * [tSetGlobalVar_1 finally ] start
	 */

	

	
	
	currentComponent="tSetGlobalVar_1";

	

 



/**
 * [tSetGlobalVar_1 finally ] stop
 */

	
	/**
	 * [tLogRow_1 finally ] start
	 */

	

	
	
	currentComponent="tLogRow_1";

	

 



/**
 * [tLogRow_1 finally ] stop
 */









				}catch(java.lang.Exception e){	
					//ignore
				}catch(java.lang.Error error){
					//ignore
				}
				resourceMap = null;
			}
		

		globalMap.put("tDBInput_1_SUBPROCESS_STATE", 1);
	}
	


public static class outInsertStruct implements routines.system.IPersistableRow<outInsertStruct> {
    final static byte[] commonByteArrayLock_GIFMIS_ledger2_12 = new byte[0];
    static byte[] commonByteArray_GIFMIS_ledger2_12 = new byte[0];
	protected static final int DEFAULT_HASHCODE = 1;
    protected static final int PRIME = 31;
    protected int hashCode = DEFAULT_HASHCODE;
    public boolean hashCodeDirty = true;

    public String loopKey;



	
			    public long ID;

				public long getID () {
					return this.ID;
				}
				
			    public long REF_ORGANIZATION;

				public long getREF_ORGANIZATION () {
					return this.REF_ORGANIZATION;
				}
				
			    public long REF_LEDGER_DOCUMENT;

				public long getREF_LEDGER_DOCUMENT () {
					return this.REF_LEDGER_DOCUMENT;
				}
				
			    public long REF_LEDGER_PERIOD;

				public long getREF_LEDGER_PERIOD () {
					return this.REF_LEDGER_PERIOD;
				}
				
			    public long REF_TRANSACTION_TYPE;

				public long getREF_TRANSACTION_TYPE () {
					return this.REF_TRANSACTION_TYPE;
				}
				
			    public long LINE_NO;

				public long getLINE_NO () {
					return this.LINE_NO;
				}
				
			    public java.util.Date TRANSACTION_DATE;

				public java.util.Date getTRANSACTION_DATE () {
					return this.TRANSACTION_DATE;
				}
				
			    public long REF_ACCOUNT;

				public long getREF_ACCOUNT () {
					return this.REF_ACCOUNT;
				}
				
			    public long REF_CURRENCY;

				public long getREF_CURRENCY () {
					return this.REF_CURRENCY;
				}
				
			    public BigDecimal CURRENCY_RATE;

				public BigDecimal getCURRENCY_RATE () {
					return this.CURRENCY_RATE;
				}
				
			    public BigDecimal AMOUNT;

				public BigDecimal getAMOUNT () {
					return this.AMOUNT;
				}
				
			    public BigDecimal AMOUNT_BASE;

				public BigDecimal getAMOUNT_BASE () {
					return this.AMOUNT_BASE;
				}
				
			    public long IS_DEBIT;

				public long getIS_DEBIT () {
					return this.IS_DEBIT;
				}
				
			    public BigDecimal QUANTITY;

				public BigDecimal getQUANTITY () {
					return this.QUANTITY;
				}
				
			    public Long REF_PROGRAM;

				public Long getREF_PROGRAM () {
					return this.REF_PROGRAM;
				}
				
			    public Long REF_FUND;

				public Long getREF_FUND () {
					return this.REF_FUND;
				}
				
			    public Long REF_ORG_DEPARTMENT;

				public Long getREF_ORG_DEPARTMENT () {
					return this.REF_ORG_DEPARTMENT;
				}
				
			    public Long REF_REGION;

				public Long getREF_REGION () {
					return this.REF_REGION;
				}
				
			    public Long REF_FUNCTION;

				public Long getREF_FUNCTION () {
					return this.REF_FUNCTION;
				}
				
			    public String DESCRIPTION;

				public String getDESCRIPTION () {
					return this.DESCRIPTION;
				}
				
			    public long REF_CLIENT;

				public long getREF_CLIENT () {
					return this.REF_CLIENT;
				}
				
			    public long VERSION;

				public long getVERSION () {
					return this.VERSION;
				}
				
			    public long CREATED_BY;

				public long getCREATED_BY () {
					return this.CREATED_BY;
				}
				
			    public long UPDATED_BY;

				public long getUPDATED_BY () {
					return this.UPDATED_BY;
				}
				
			    public java.util.Date CREATED_DATE;

				public java.util.Date getCREATED_DATE () {
					return this.CREATED_DATE;
				}
				
			    public java.util.Date UPDATED_DATE;

				public java.util.Date getUPDATED_DATE () {
					return this.UPDATED_DATE;
				}
				
			    public long REF_ORGANIZATION_ORIGINAL;

				public long getREF_ORGANIZATION_ORIGINAL () {
					return this.REF_ORGANIZATION_ORIGINAL;
				}
				
			    public long LINE_NO_SUB;

				public long getLINE_NO_SUB () {
					return this.LINE_NO_SUB;
				}
				
			    public long LINE_NO_QUEUE;

				public long getLINE_NO_QUEUE () {
					return this.LINE_NO_QUEUE;
				}
				
			    public String REF_ORIG_TABLE;

				public String getREF_ORIG_TABLE () {
					return this.REF_ORIG_TABLE;
				}
				
			    public long REF_ORIG_TABLE_ID;

				public long getREF_ORIG_TABLE_ID () {
					return this.REF_ORIG_TABLE_ID;
				}
				
			    public String GL_TYPE;

				public String getGL_TYPE () {
					return this.GL_TYPE;
				}
				
			    public long COMMITMENT_TYPE;

				public long getCOMMITMENT_TYPE () {
					return this.COMMITMENT_TYPE;
				}
				
			    public java.util.Date etl_date;

				public java.util.Date getEtl_date () {
					return this.etl_date;
				}
				


	@Override
	public int hashCode() {
		if (this.hashCodeDirty) {
			final int prime = PRIME;
			int result = DEFAULT_HASHCODE;
	
							result = prime * result + (int) this.ID;
						
    		this.hashCode = result;
    		this.hashCodeDirty = false;
		}
		return this.hashCode;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		final outInsertStruct other = (outInsertStruct) obj;
		
						if (this.ID != other.ID)
							return false;
					

		return true;
    }

	public void copyDataTo(outInsertStruct other) {

		other.ID = this.ID;
	            other.REF_ORGANIZATION = this.REF_ORGANIZATION;
	            other.REF_LEDGER_DOCUMENT = this.REF_LEDGER_DOCUMENT;
	            other.REF_LEDGER_PERIOD = this.REF_LEDGER_PERIOD;
	            other.REF_TRANSACTION_TYPE = this.REF_TRANSACTION_TYPE;
	            other.LINE_NO = this.LINE_NO;
	            other.TRANSACTION_DATE = this.TRANSACTION_DATE;
	            other.REF_ACCOUNT = this.REF_ACCOUNT;
	            other.REF_CURRENCY = this.REF_CURRENCY;
	            other.CURRENCY_RATE = this.CURRENCY_RATE;
	            other.AMOUNT = this.AMOUNT;
	            other.AMOUNT_BASE = this.AMOUNT_BASE;
	            other.IS_DEBIT = this.IS_DEBIT;
	            other.QUANTITY = this.QUANTITY;
	            other.REF_PROGRAM = this.REF_PROGRAM;
	            other.REF_FUND = this.REF_FUND;
	            other.REF_ORG_DEPARTMENT = this.REF_ORG_DEPARTMENT;
	            other.REF_REGION = this.REF_REGION;
	            other.REF_FUNCTION = this.REF_FUNCTION;
	            other.DESCRIPTION = this.DESCRIPTION;
	            other.REF_CLIENT = this.REF_CLIENT;
	            other.VERSION = this.VERSION;
	            other.CREATED_BY = this.CREATED_BY;
	            other.UPDATED_BY = this.UPDATED_BY;
	            other.CREATED_DATE = this.CREATED_DATE;
	            other.UPDATED_DATE = this.UPDATED_DATE;
	            other.REF_ORGANIZATION_ORIGINAL = this.REF_ORGANIZATION_ORIGINAL;
	            other.LINE_NO_SUB = this.LINE_NO_SUB;
	            other.LINE_NO_QUEUE = this.LINE_NO_QUEUE;
	            other.REF_ORIG_TABLE = this.REF_ORIG_TABLE;
	            other.REF_ORIG_TABLE_ID = this.REF_ORIG_TABLE_ID;
	            other.GL_TYPE = this.GL_TYPE;
	            other.COMMITMENT_TYPE = this.COMMITMENT_TYPE;
	            other.etl_date = this.etl_date;
	            
	}

	public void copyKeysDataTo(outInsertStruct other) {

		other.ID = this.ID;
	            	
	}




	private java.util.Date readDate(ObjectInputStream dis) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(dis.readLong());
		}
		return dateReturn;
	}

    private void writeDate(java.util.Date date1, ObjectOutputStream dos) throws IOException{
		if(date1 == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeLong(date1.getTime());
    	}
    }

	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_GIFMIS_ledger2_12.length) {
				if(length < 1024 && commonByteArray_GIFMIS_ledger2_12.length == 0) {
   					commonByteArray_GIFMIS_ledger2_12 = new byte[1024];
				} else {
   					commonByteArray_GIFMIS_ledger2_12 = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_GIFMIS_ledger2_12, 0, length);
			strReturn = new String(commonByteArray_GIFMIS_ledger2_12, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_GIFMIS_ledger2_12) {

        	try {

        		int length = 0;
		
			        this.ID = dis.readLong();
					
			        this.REF_ORGANIZATION = dis.readLong();
					
			        this.REF_LEDGER_DOCUMENT = dis.readLong();
					
			        this.REF_LEDGER_PERIOD = dis.readLong();
					
			        this.REF_TRANSACTION_TYPE = dis.readLong();
					
			        this.LINE_NO = dis.readLong();
					
					this.TRANSACTION_DATE = readDate(dis);
					
			        this.REF_ACCOUNT = dis.readLong();
					
			        this.REF_CURRENCY = dis.readLong();
					
						this.CURRENCY_RATE = (BigDecimal) dis.readObject();
					
						this.AMOUNT = (BigDecimal) dis.readObject();
					
						this.AMOUNT_BASE = (BigDecimal) dis.readObject();
					
			        this.IS_DEBIT = dis.readLong();
					
						this.QUANTITY = (BigDecimal) dis.readObject();
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.REF_PROGRAM = null;
           				} else {
           			    	this.REF_PROGRAM = dis.readLong();
           				}
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.REF_FUND = null;
           				} else {
           			    	this.REF_FUND = dis.readLong();
           				}
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.REF_ORG_DEPARTMENT = null;
           				} else {
           			    	this.REF_ORG_DEPARTMENT = dis.readLong();
           				}
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.REF_REGION = null;
           				} else {
           			    	this.REF_REGION = dis.readLong();
           				}
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.REF_FUNCTION = null;
           				} else {
           			    	this.REF_FUNCTION = dis.readLong();
           				}
					
					this.DESCRIPTION = readString(dis);
					
			        this.REF_CLIENT = dis.readLong();
					
			        this.VERSION = dis.readLong();
					
			        this.CREATED_BY = dis.readLong();
					
			        this.UPDATED_BY = dis.readLong();
					
					this.CREATED_DATE = readDate(dis);
					
					this.UPDATED_DATE = readDate(dis);
					
			        this.REF_ORGANIZATION_ORIGINAL = dis.readLong();
					
			        this.LINE_NO_SUB = dis.readLong();
					
			        this.LINE_NO_QUEUE = dis.readLong();
					
					this.REF_ORIG_TABLE = readString(dis);
					
			        this.REF_ORIG_TABLE_ID = dis.readLong();
					
					this.GL_TYPE = readString(dis);
					
			        this.COMMITMENT_TYPE = dis.readLong();
					
					this.etl_date = readDate(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		
			} catch(ClassNotFoundException eCNFE) {
				 throw new RuntimeException(eCNFE);
		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// long
				
		            	dos.writeLong(this.ID);
					
					// long
				
		            	dos.writeLong(this.REF_ORGANIZATION);
					
					// long
				
		            	dos.writeLong(this.REF_LEDGER_DOCUMENT);
					
					// long
				
		            	dos.writeLong(this.REF_LEDGER_PERIOD);
					
					// long
				
		            	dos.writeLong(this.REF_TRANSACTION_TYPE);
					
					// long
				
		            	dos.writeLong(this.LINE_NO);
					
					// java.util.Date
				
						writeDate(this.TRANSACTION_DATE,dos);
					
					// long
				
		            	dos.writeLong(this.REF_ACCOUNT);
					
					// long
				
		            	dos.writeLong(this.REF_CURRENCY);
					
					// BigDecimal
				
       			    	dos.writeObject(this.CURRENCY_RATE);
					
					// BigDecimal
				
       			    	dos.writeObject(this.AMOUNT);
					
					// BigDecimal
				
       			    	dos.writeObject(this.AMOUNT_BASE);
					
					// long
				
		            	dos.writeLong(this.IS_DEBIT);
					
					// BigDecimal
				
       			    	dos.writeObject(this.QUANTITY);
					
					// Long
				
						if(this.REF_PROGRAM == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeLong(this.REF_PROGRAM);
		            	}
					
					// Long
				
						if(this.REF_FUND == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeLong(this.REF_FUND);
		            	}
					
					// Long
				
						if(this.REF_ORG_DEPARTMENT == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeLong(this.REF_ORG_DEPARTMENT);
		            	}
					
					// Long
				
						if(this.REF_REGION == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeLong(this.REF_REGION);
		            	}
					
					// Long
				
						if(this.REF_FUNCTION == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeLong(this.REF_FUNCTION);
		            	}
					
					// String
				
						writeString(this.DESCRIPTION,dos);
					
					// long
				
		            	dos.writeLong(this.REF_CLIENT);
					
					// long
				
		            	dos.writeLong(this.VERSION);
					
					// long
				
		            	dos.writeLong(this.CREATED_BY);
					
					// long
				
		            	dos.writeLong(this.UPDATED_BY);
					
					// java.util.Date
				
						writeDate(this.CREATED_DATE,dos);
					
					// java.util.Date
				
						writeDate(this.UPDATED_DATE,dos);
					
					// long
				
		            	dos.writeLong(this.REF_ORGANIZATION_ORIGINAL);
					
					// long
				
		            	dos.writeLong(this.LINE_NO_SUB);
					
					// long
				
		            	dos.writeLong(this.LINE_NO_QUEUE);
					
					// String
				
						writeString(this.REF_ORIG_TABLE,dos);
					
					// long
				
		            	dos.writeLong(this.REF_ORIG_TABLE_ID);
					
					// String
				
						writeString(this.GL_TYPE,dos);
					
					// long
				
		            	dos.writeLong(this.COMMITMENT_TYPE);
					
					// java.util.Date
				
						writeDate(this.etl_date,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("ID="+String.valueOf(ID));
		sb.append(",REF_ORGANIZATION="+String.valueOf(REF_ORGANIZATION));
		sb.append(",REF_LEDGER_DOCUMENT="+String.valueOf(REF_LEDGER_DOCUMENT));
		sb.append(",REF_LEDGER_PERIOD="+String.valueOf(REF_LEDGER_PERIOD));
		sb.append(",REF_TRANSACTION_TYPE="+String.valueOf(REF_TRANSACTION_TYPE));
		sb.append(",LINE_NO="+String.valueOf(LINE_NO));
		sb.append(",TRANSACTION_DATE="+String.valueOf(TRANSACTION_DATE));
		sb.append(",REF_ACCOUNT="+String.valueOf(REF_ACCOUNT));
		sb.append(",REF_CURRENCY="+String.valueOf(REF_CURRENCY));
		sb.append(",CURRENCY_RATE="+String.valueOf(CURRENCY_RATE));
		sb.append(",AMOUNT="+String.valueOf(AMOUNT));
		sb.append(",AMOUNT_BASE="+String.valueOf(AMOUNT_BASE));
		sb.append(",IS_DEBIT="+String.valueOf(IS_DEBIT));
		sb.append(",QUANTITY="+String.valueOf(QUANTITY));
		sb.append(",REF_PROGRAM="+String.valueOf(REF_PROGRAM));
		sb.append(",REF_FUND="+String.valueOf(REF_FUND));
		sb.append(",REF_ORG_DEPARTMENT="+String.valueOf(REF_ORG_DEPARTMENT));
		sb.append(",REF_REGION="+String.valueOf(REF_REGION));
		sb.append(",REF_FUNCTION="+String.valueOf(REF_FUNCTION));
		sb.append(",DESCRIPTION="+DESCRIPTION);
		sb.append(",REF_CLIENT="+String.valueOf(REF_CLIENT));
		sb.append(",VERSION="+String.valueOf(VERSION));
		sb.append(",CREATED_BY="+String.valueOf(CREATED_BY));
		sb.append(",UPDATED_BY="+String.valueOf(UPDATED_BY));
		sb.append(",CREATED_DATE="+String.valueOf(CREATED_DATE));
		sb.append(",UPDATED_DATE="+String.valueOf(UPDATED_DATE));
		sb.append(",REF_ORGANIZATION_ORIGINAL="+String.valueOf(REF_ORGANIZATION_ORIGINAL));
		sb.append(",LINE_NO_SUB="+String.valueOf(LINE_NO_SUB));
		sb.append(",LINE_NO_QUEUE="+String.valueOf(LINE_NO_QUEUE));
		sb.append(",REF_ORIG_TABLE="+REF_ORIG_TABLE);
		sb.append(",REF_ORIG_TABLE_ID="+String.valueOf(REF_ORIG_TABLE_ID));
		sb.append(",GL_TYPE="+GL_TYPE);
		sb.append(",COMMITMENT_TYPE="+String.valueOf(COMMITMENT_TYPE));
		sb.append(",etl_date="+String.valueOf(etl_date));
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(outInsertStruct other) {

		int returnValue = -1;
		
						returnValue = checkNullsAndCompare(this.ID, other.ID);
						if(returnValue != 0) {
							return returnValue;
						}

					
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}

public static class row3Struct implements routines.system.IPersistableRow<row3Struct> {
    final static byte[] commonByteArrayLock_GIFMIS_ledger2_12 = new byte[0];
    static byte[] commonByteArray_GIFMIS_ledger2_12 = new byte[0];

	
			    public long ID;

				public long getID () {
					return this.ID;
				}
				
			    public long REF_ORGANIZATION;

				public long getREF_ORGANIZATION () {
					return this.REF_ORGANIZATION;
				}
				
			    public long REF_LEDGER_DOCUMENT;

				public long getREF_LEDGER_DOCUMENT () {
					return this.REF_LEDGER_DOCUMENT;
				}
				
			    public long REF_LEDGER_PERIOD;

				public long getREF_LEDGER_PERIOD () {
					return this.REF_LEDGER_PERIOD;
				}
				
			    public long REF_TRANSACTION_TYPE;

				public long getREF_TRANSACTION_TYPE () {
					return this.REF_TRANSACTION_TYPE;
				}
				
			    public long LINE_NO;

				public long getLINE_NO () {
					return this.LINE_NO;
				}
				
			    public java.util.Date TRANSACTION_DATE;

				public java.util.Date getTRANSACTION_DATE () {
					return this.TRANSACTION_DATE;
				}
				
			    public long REF_ACCOUNT;

				public long getREF_ACCOUNT () {
					return this.REF_ACCOUNT;
				}
				
			    public long REF_CURRENCY;

				public long getREF_CURRENCY () {
					return this.REF_CURRENCY;
				}
				
			    public BigDecimal CURRENCY_RATE;

				public BigDecimal getCURRENCY_RATE () {
					return this.CURRENCY_RATE;
				}
				
			    public BigDecimal AMOUNT;

				public BigDecimal getAMOUNT () {
					return this.AMOUNT;
				}
				
			    public BigDecimal AMOUNT_BASE;

				public BigDecimal getAMOUNT_BASE () {
					return this.AMOUNT_BASE;
				}
				
			    public long IS_DEBIT;

				public long getIS_DEBIT () {
					return this.IS_DEBIT;
				}
				
			    public BigDecimal QUANTITY;

				public BigDecimal getQUANTITY () {
					return this.QUANTITY;
				}
				
			    public Long REF_PROGRAM;

				public Long getREF_PROGRAM () {
					return this.REF_PROGRAM;
				}
				
			    public Long REF_FUND;

				public Long getREF_FUND () {
					return this.REF_FUND;
				}
				
			    public Long REF_ORG_DEPARTMENT;

				public Long getREF_ORG_DEPARTMENT () {
					return this.REF_ORG_DEPARTMENT;
				}
				
			    public Long REF_REGION;

				public Long getREF_REGION () {
					return this.REF_REGION;
				}
				
			    public Long REF_FUNCTION;

				public Long getREF_FUNCTION () {
					return this.REF_FUNCTION;
				}
				
			    public String DESCRIPTION;

				public String getDESCRIPTION () {
					return this.DESCRIPTION;
				}
				
			    public long REF_CLIENT;

				public long getREF_CLIENT () {
					return this.REF_CLIENT;
				}
				
			    public long VERSION;

				public long getVERSION () {
					return this.VERSION;
				}
				
			    public long CREATED_BY;

				public long getCREATED_BY () {
					return this.CREATED_BY;
				}
				
			    public long UPDATED_BY;

				public long getUPDATED_BY () {
					return this.UPDATED_BY;
				}
				
			    public java.util.Date CREATED_DATE;

				public java.util.Date getCREATED_DATE () {
					return this.CREATED_DATE;
				}
				
			    public java.util.Date UPDATED_DATE;

				public java.util.Date getUPDATED_DATE () {
					return this.UPDATED_DATE;
				}
				
			    public long REF_ORGANIZATION_ORIGINAL;

				public long getREF_ORGANIZATION_ORIGINAL () {
					return this.REF_ORGANIZATION_ORIGINAL;
				}
				
			    public long LINE_NO_SUB;

				public long getLINE_NO_SUB () {
					return this.LINE_NO_SUB;
				}
				
			    public long LINE_NO_QUEUE;

				public long getLINE_NO_QUEUE () {
					return this.LINE_NO_QUEUE;
				}
				
			    public String REF_ORIG_TABLE;

				public String getREF_ORIG_TABLE () {
					return this.REF_ORIG_TABLE;
				}
				
			    public long REF_ORIG_TABLE_ID;

				public long getREF_ORIG_TABLE_ID () {
					return this.REF_ORIG_TABLE_ID;
				}
				
			    public String GL_TYPE;

				public String getGL_TYPE () {
					return this.GL_TYPE;
				}
				
			    public long COMMITMENT_TYPE;

				public long getCOMMITMENT_TYPE () {
					return this.COMMITMENT_TYPE;
				}
				



	private java.util.Date readDate(ObjectInputStream dis) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(dis.readLong());
		}
		return dateReturn;
	}

    private void writeDate(java.util.Date date1, ObjectOutputStream dos) throws IOException{
		if(date1 == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeLong(date1.getTime());
    	}
    }

	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_GIFMIS_ledger2_12.length) {
				if(length < 1024 && commonByteArray_GIFMIS_ledger2_12.length == 0) {
   					commonByteArray_GIFMIS_ledger2_12 = new byte[1024];
				} else {
   					commonByteArray_GIFMIS_ledger2_12 = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_GIFMIS_ledger2_12, 0, length);
			strReturn = new String(commonByteArray_GIFMIS_ledger2_12, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_GIFMIS_ledger2_12) {

        	try {

        		int length = 0;
		
			        this.ID = dis.readLong();
					
			        this.REF_ORGANIZATION = dis.readLong();
					
			        this.REF_LEDGER_DOCUMENT = dis.readLong();
					
			        this.REF_LEDGER_PERIOD = dis.readLong();
					
			        this.REF_TRANSACTION_TYPE = dis.readLong();
					
			        this.LINE_NO = dis.readLong();
					
					this.TRANSACTION_DATE = readDate(dis);
					
			        this.REF_ACCOUNT = dis.readLong();
					
			        this.REF_CURRENCY = dis.readLong();
					
						this.CURRENCY_RATE = (BigDecimal) dis.readObject();
					
						this.AMOUNT = (BigDecimal) dis.readObject();
					
						this.AMOUNT_BASE = (BigDecimal) dis.readObject();
					
			        this.IS_DEBIT = dis.readLong();
					
						this.QUANTITY = (BigDecimal) dis.readObject();
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.REF_PROGRAM = null;
           				} else {
           			    	this.REF_PROGRAM = dis.readLong();
           				}
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.REF_FUND = null;
           				} else {
           			    	this.REF_FUND = dis.readLong();
           				}
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.REF_ORG_DEPARTMENT = null;
           				} else {
           			    	this.REF_ORG_DEPARTMENT = dis.readLong();
           				}
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.REF_REGION = null;
           				} else {
           			    	this.REF_REGION = dis.readLong();
           				}
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.REF_FUNCTION = null;
           				} else {
           			    	this.REF_FUNCTION = dis.readLong();
           				}
					
					this.DESCRIPTION = readString(dis);
					
			        this.REF_CLIENT = dis.readLong();
					
			        this.VERSION = dis.readLong();
					
			        this.CREATED_BY = dis.readLong();
					
			        this.UPDATED_BY = dis.readLong();
					
					this.CREATED_DATE = readDate(dis);
					
					this.UPDATED_DATE = readDate(dis);
					
			        this.REF_ORGANIZATION_ORIGINAL = dis.readLong();
					
			        this.LINE_NO_SUB = dis.readLong();
					
			        this.LINE_NO_QUEUE = dis.readLong();
					
					this.REF_ORIG_TABLE = readString(dis);
					
			        this.REF_ORIG_TABLE_ID = dis.readLong();
					
					this.GL_TYPE = readString(dis);
					
			        this.COMMITMENT_TYPE = dis.readLong();
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		
			} catch(ClassNotFoundException eCNFE) {
				 throw new RuntimeException(eCNFE);
		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// long
				
		            	dos.writeLong(this.ID);
					
					// long
				
		            	dos.writeLong(this.REF_ORGANIZATION);
					
					// long
				
		            	dos.writeLong(this.REF_LEDGER_DOCUMENT);
					
					// long
				
		            	dos.writeLong(this.REF_LEDGER_PERIOD);
					
					// long
				
		            	dos.writeLong(this.REF_TRANSACTION_TYPE);
					
					// long
				
		            	dos.writeLong(this.LINE_NO);
					
					// java.util.Date
				
						writeDate(this.TRANSACTION_DATE,dos);
					
					// long
				
		            	dos.writeLong(this.REF_ACCOUNT);
					
					// long
				
		            	dos.writeLong(this.REF_CURRENCY);
					
					// BigDecimal
				
       			    	dos.writeObject(this.CURRENCY_RATE);
					
					// BigDecimal
				
       			    	dos.writeObject(this.AMOUNT);
					
					// BigDecimal
				
       			    	dos.writeObject(this.AMOUNT_BASE);
					
					// long
				
		            	dos.writeLong(this.IS_DEBIT);
					
					// BigDecimal
				
       			    	dos.writeObject(this.QUANTITY);
					
					// Long
				
						if(this.REF_PROGRAM == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeLong(this.REF_PROGRAM);
		            	}
					
					// Long
				
						if(this.REF_FUND == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeLong(this.REF_FUND);
		            	}
					
					// Long
				
						if(this.REF_ORG_DEPARTMENT == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeLong(this.REF_ORG_DEPARTMENT);
		            	}
					
					// Long
				
						if(this.REF_REGION == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeLong(this.REF_REGION);
		            	}
					
					// Long
				
						if(this.REF_FUNCTION == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeLong(this.REF_FUNCTION);
		            	}
					
					// String
				
						writeString(this.DESCRIPTION,dos);
					
					// long
				
		            	dos.writeLong(this.REF_CLIENT);
					
					// long
				
		            	dos.writeLong(this.VERSION);
					
					// long
				
		            	dos.writeLong(this.CREATED_BY);
					
					// long
				
		            	dos.writeLong(this.UPDATED_BY);
					
					// java.util.Date
				
						writeDate(this.CREATED_DATE,dos);
					
					// java.util.Date
				
						writeDate(this.UPDATED_DATE,dos);
					
					// long
				
		            	dos.writeLong(this.REF_ORGANIZATION_ORIGINAL);
					
					// long
				
		            	dos.writeLong(this.LINE_NO_SUB);
					
					// long
				
		            	dos.writeLong(this.LINE_NO_QUEUE);
					
					// String
				
						writeString(this.REF_ORIG_TABLE,dos);
					
					// long
				
		            	dos.writeLong(this.REF_ORIG_TABLE_ID);
					
					// String
				
						writeString(this.GL_TYPE,dos);
					
					// long
				
		            	dos.writeLong(this.COMMITMENT_TYPE);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("ID="+String.valueOf(ID));
		sb.append(",REF_ORGANIZATION="+String.valueOf(REF_ORGANIZATION));
		sb.append(",REF_LEDGER_DOCUMENT="+String.valueOf(REF_LEDGER_DOCUMENT));
		sb.append(",REF_LEDGER_PERIOD="+String.valueOf(REF_LEDGER_PERIOD));
		sb.append(",REF_TRANSACTION_TYPE="+String.valueOf(REF_TRANSACTION_TYPE));
		sb.append(",LINE_NO="+String.valueOf(LINE_NO));
		sb.append(",TRANSACTION_DATE="+String.valueOf(TRANSACTION_DATE));
		sb.append(",REF_ACCOUNT="+String.valueOf(REF_ACCOUNT));
		sb.append(",REF_CURRENCY="+String.valueOf(REF_CURRENCY));
		sb.append(",CURRENCY_RATE="+String.valueOf(CURRENCY_RATE));
		sb.append(",AMOUNT="+String.valueOf(AMOUNT));
		sb.append(",AMOUNT_BASE="+String.valueOf(AMOUNT_BASE));
		sb.append(",IS_DEBIT="+String.valueOf(IS_DEBIT));
		sb.append(",QUANTITY="+String.valueOf(QUANTITY));
		sb.append(",REF_PROGRAM="+String.valueOf(REF_PROGRAM));
		sb.append(",REF_FUND="+String.valueOf(REF_FUND));
		sb.append(",REF_ORG_DEPARTMENT="+String.valueOf(REF_ORG_DEPARTMENT));
		sb.append(",REF_REGION="+String.valueOf(REF_REGION));
		sb.append(",REF_FUNCTION="+String.valueOf(REF_FUNCTION));
		sb.append(",DESCRIPTION="+DESCRIPTION);
		sb.append(",REF_CLIENT="+String.valueOf(REF_CLIENT));
		sb.append(",VERSION="+String.valueOf(VERSION));
		sb.append(",CREATED_BY="+String.valueOf(CREATED_BY));
		sb.append(",UPDATED_BY="+String.valueOf(UPDATED_BY));
		sb.append(",CREATED_DATE="+String.valueOf(CREATED_DATE));
		sb.append(",UPDATED_DATE="+String.valueOf(UPDATED_DATE));
		sb.append(",REF_ORGANIZATION_ORIGINAL="+String.valueOf(REF_ORGANIZATION_ORIGINAL));
		sb.append(",LINE_NO_SUB="+String.valueOf(LINE_NO_SUB));
		sb.append(",LINE_NO_QUEUE="+String.valueOf(LINE_NO_QUEUE));
		sb.append(",REF_ORIG_TABLE="+REF_ORIG_TABLE);
		sb.append(",REF_ORIG_TABLE_ID="+String.valueOf(REF_ORIG_TABLE_ID));
		sb.append(",GL_TYPE="+GL_TYPE);
		sb.append(",COMMITMENT_TYPE="+String.valueOf(COMMITMENT_TYPE));
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(row3Struct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}

public static class after_tDBInput_2Struct implements routines.system.IPersistableRow<after_tDBInput_2Struct> {
    final static byte[] commonByteArrayLock_GIFMIS_ledger2_12 = new byte[0];
    static byte[] commonByteArray_GIFMIS_ledger2_12 = new byte[0];
	protected static final int DEFAULT_HASHCODE = 1;
    protected static final int PRIME = 31;
    protected int hashCode = DEFAULT_HASHCODE;
    public boolean hashCodeDirty = true;

    public String loopKey;



	
			    public long ID;

				public long getID () {
					return this.ID;
				}
				
			    public long REF_ORGANIZATION;

				public long getREF_ORGANIZATION () {
					return this.REF_ORGANIZATION;
				}
				
			    public long REF_LEDGER_DOCUMENT;

				public long getREF_LEDGER_DOCUMENT () {
					return this.REF_LEDGER_DOCUMENT;
				}
				
			    public long REF_LEDGER_PERIOD;

				public long getREF_LEDGER_PERIOD () {
					return this.REF_LEDGER_PERIOD;
				}
				
			    public long REF_TRANSACTION_TYPE;

				public long getREF_TRANSACTION_TYPE () {
					return this.REF_TRANSACTION_TYPE;
				}
				
			    public long LINE_NO;

				public long getLINE_NO () {
					return this.LINE_NO;
				}
				
			    public java.util.Date TRANSACTION_DATE;

				public java.util.Date getTRANSACTION_DATE () {
					return this.TRANSACTION_DATE;
				}
				
			    public long REF_ACCOUNT;

				public long getREF_ACCOUNT () {
					return this.REF_ACCOUNT;
				}
				
			    public long REF_CURRENCY;

				public long getREF_CURRENCY () {
					return this.REF_CURRENCY;
				}
				
			    public BigDecimal CURRENCY_RATE;

				public BigDecimal getCURRENCY_RATE () {
					return this.CURRENCY_RATE;
				}
				
			    public BigDecimal AMOUNT;

				public BigDecimal getAMOUNT () {
					return this.AMOUNT;
				}
				
			    public BigDecimal AMOUNT_BASE;

				public BigDecimal getAMOUNT_BASE () {
					return this.AMOUNT_BASE;
				}
				
			    public long IS_DEBIT;

				public long getIS_DEBIT () {
					return this.IS_DEBIT;
				}
				
			    public BigDecimal QUANTITY;

				public BigDecimal getQUANTITY () {
					return this.QUANTITY;
				}
				
			    public Long REF_PROGRAM;

				public Long getREF_PROGRAM () {
					return this.REF_PROGRAM;
				}
				
			    public Long REF_FUND;

				public Long getREF_FUND () {
					return this.REF_FUND;
				}
				
			    public Long REF_ORG_DEPARTMENT;

				public Long getREF_ORG_DEPARTMENT () {
					return this.REF_ORG_DEPARTMENT;
				}
				
			    public Long REF_REGION;

				public Long getREF_REGION () {
					return this.REF_REGION;
				}
				
			    public Long REF_FUNCTION;

				public Long getREF_FUNCTION () {
					return this.REF_FUNCTION;
				}
				
			    public String DESCRIPTION;

				public String getDESCRIPTION () {
					return this.DESCRIPTION;
				}
				
			    public long REF_CLIENT;

				public long getREF_CLIENT () {
					return this.REF_CLIENT;
				}
				
			    public long VERSION;

				public long getVERSION () {
					return this.VERSION;
				}
				
			    public long CREATED_BY;

				public long getCREATED_BY () {
					return this.CREATED_BY;
				}
				
			    public long UPDATED_BY;

				public long getUPDATED_BY () {
					return this.UPDATED_BY;
				}
				
			    public java.util.Date CREATED_DATE;

				public java.util.Date getCREATED_DATE () {
					return this.CREATED_DATE;
				}
				
			    public java.util.Date UPDATED_DATE;

				public java.util.Date getUPDATED_DATE () {
					return this.UPDATED_DATE;
				}
				
			    public long REF_ORGANIZATION_ORIGINAL;

				public long getREF_ORGANIZATION_ORIGINAL () {
					return this.REF_ORGANIZATION_ORIGINAL;
				}
				
			    public long LINE_NO_SUB;

				public long getLINE_NO_SUB () {
					return this.LINE_NO_SUB;
				}
				
			    public long LINE_NO_QUEUE;

				public long getLINE_NO_QUEUE () {
					return this.LINE_NO_QUEUE;
				}
				
			    public String REF_ORIG_TABLE;

				public String getREF_ORIG_TABLE () {
					return this.REF_ORIG_TABLE;
				}
				
			    public long REF_ORIG_TABLE_ID;

				public long getREF_ORIG_TABLE_ID () {
					return this.REF_ORIG_TABLE_ID;
				}
				
			    public String GL_TYPE;

				public String getGL_TYPE () {
					return this.GL_TYPE;
				}
				
			    public long COMMITMENT_TYPE;

				public long getCOMMITMENT_TYPE () {
					return this.COMMITMENT_TYPE;
				}
				


	@Override
	public int hashCode() {
		if (this.hashCodeDirty) {
			final int prime = PRIME;
			int result = DEFAULT_HASHCODE;
	
							result = prime * result + (int) this.ID;
						
    		this.hashCode = result;
    		this.hashCodeDirty = false;
		}
		return this.hashCode;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		final after_tDBInput_2Struct other = (after_tDBInput_2Struct) obj;
		
						if (this.ID != other.ID)
							return false;
					

		return true;
    }

	public void copyDataTo(after_tDBInput_2Struct other) {

		other.ID = this.ID;
	            other.REF_ORGANIZATION = this.REF_ORGANIZATION;
	            other.REF_LEDGER_DOCUMENT = this.REF_LEDGER_DOCUMENT;
	            other.REF_LEDGER_PERIOD = this.REF_LEDGER_PERIOD;
	            other.REF_TRANSACTION_TYPE = this.REF_TRANSACTION_TYPE;
	            other.LINE_NO = this.LINE_NO;
	            other.TRANSACTION_DATE = this.TRANSACTION_DATE;
	            other.REF_ACCOUNT = this.REF_ACCOUNT;
	            other.REF_CURRENCY = this.REF_CURRENCY;
	            other.CURRENCY_RATE = this.CURRENCY_RATE;
	            other.AMOUNT = this.AMOUNT;
	            other.AMOUNT_BASE = this.AMOUNT_BASE;
	            other.IS_DEBIT = this.IS_DEBIT;
	            other.QUANTITY = this.QUANTITY;
	            other.REF_PROGRAM = this.REF_PROGRAM;
	            other.REF_FUND = this.REF_FUND;
	            other.REF_ORG_DEPARTMENT = this.REF_ORG_DEPARTMENT;
	            other.REF_REGION = this.REF_REGION;
	            other.REF_FUNCTION = this.REF_FUNCTION;
	            other.DESCRIPTION = this.DESCRIPTION;
	            other.REF_CLIENT = this.REF_CLIENT;
	            other.VERSION = this.VERSION;
	            other.CREATED_BY = this.CREATED_BY;
	            other.UPDATED_BY = this.UPDATED_BY;
	            other.CREATED_DATE = this.CREATED_DATE;
	            other.UPDATED_DATE = this.UPDATED_DATE;
	            other.REF_ORGANIZATION_ORIGINAL = this.REF_ORGANIZATION_ORIGINAL;
	            other.LINE_NO_SUB = this.LINE_NO_SUB;
	            other.LINE_NO_QUEUE = this.LINE_NO_QUEUE;
	            other.REF_ORIG_TABLE = this.REF_ORIG_TABLE;
	            other.REF_ORIG_TABLE_ID = this.REF_ORIG_TABLE_ID;
	            other.GL_TYPE = this.GL_TYPE;
	            other.COMMITMENT_TYPE = this.COMMITMENT_TYPE;
	            
	}

	public void copyKeysDataTo(after_tDBInput_2Struct other) {

		other.ID = this.ID;
	            	
	}




	private java.util.Date readDate(ObjectInputStream dis) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(dis.readLong());
		}
		return dateReturn;
	}

    private void writeDate(java.util.Date date1, ObjectOutputStream dos) throws IOException{
		if(date1 == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeLong(date1.getTime());
    	}
    }

	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_GIFMIS_ledger2_12.length) {
				if(length < 1024 && commonByteArray_GIFMIS_ledger2_12.length == 0) {
   					commonByteArray_GIFMIS_ledger2_12 = new byte[1024];
				} else {
   					commonByteArray_GIFMIS_ledger2_12 = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_GIFMIS_ledger2_12, 0, length);
			strReturn = new String(commonByteArray_GIFMIS_ledger2_12, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_GIFMIS_ledger2_12) {

        	try {

        		int length = 0;
		
			        this.ID = dis.readLong();
					
			        this.REF_ORGANIZATION = dis.readLong();
					
			        this.REF_LEDGER_DOCUMENT = dis.readLong();
					
			        this.REF_LEDGER_PERIOD = dis.readLong();
					
			        this.REF_TRANSACTION_TYPE = dis.readLong();
					
			        this.LINE_NO = dis.readLong();
					
					this.TRANSACTION_DATE = readDate(dis);
					
			        this.REF_ACCOUNT = dis.readLong();
					
			        this.REF_CURRENCY = dis.readLong();
					
						this.CURRENCY_RATE = (BigDecimal) dis.readObject();
					
						this.AMOUNT = (BigDecimal) dis.readObject();
					
						this.AMOUNT_BASE = (BigDecimal) dis.readObject();
					
			        this.IS_DEBIT = dis.readLong();
					
						this.QUANTITY = (BigDecimal) dis.readObject();
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.REF_PROGRAM = null;
           				} else {
           			    	this.REF_PROGRAM = dis.readLong();
           				}
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.REF_FUND = null;
           				} else {
           			    	this.REF_FUND = dis.readLong();
           				}
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.REF_ORG_DEPARTMENT = null;
           				} else {
           			    	this.REF_ORG_DEPARTMENT = dis.readLong();
           				}
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.REF_REGION = null;
           				} else {
           			    	this.REF_REGION = dis.readLong();
           				}
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.REF_FUNCTION = null;
           				} else {
           			    	this.REF_FUNCTION = dis.readLong();
           				}
					
					this.DESCRIPTION = readString(dis);
					
			        this.REF_CLIENT = dis.readLong();
					
			        this.VERSION = dis.readLong();
					
			        this.CREATED_BY = dis.readLong();
					
			        this.UPDATED_BY = dis.readLong();
					
					this.CREATED_DATE = readDate(dis);
					
					this.UPDATED_DATE = readDate(dis);
					
			        this.REF_ORGANIZATION_ORIGINAL = dis.readLong();
					
			        this.LINE_NO_SUB = dis.readLong();
					
			        this.LINE_NO_QUEUE = dis.readLong();
					
					this.REF_ORIG_TABLE = readString(dis);
					
			        this.REF_ORIG_TABLE_ID = dis.readLong();
					
					this.GL_TYPE = readString(dis);
					
			        this.COMMITMENT_TYPE = dis.readLong();
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		
			} catch(ClassNotFoundException eCNFE) {
				 throw new RuntimeException(eCNFE);
		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// long
				
		            	dos.writeLong(this.ID);
					
					// long
				
		            	dos.writeLong(this.REF_ORGANIZATION);
					
					// long
				
		            	dos.writeLong(this.REF_LEDGER_DOCUMENT);
					
					// long
				
		            	dos.writeLong(this.REF_LEDGER_PERIOD);
					
					// long
				
		            	dos.writeLong(this.REF_TRANSACTION_TYPE);
					
					// long
				
		            	dos.writeLong(this.LINE_NO);
					
					// java.util.Date
				
						writeDate(this.TRANSACTION_DATE,dos);
					
					// long
				
		            	dos.writeLong(this.REF_ACCOUNT);
					
					// long
				
		            	dos.writeLong(this.REF_CURRENCY);
					
					// BigDecimal
				
       			    	dos.writeObject(this.CURRENCY_RATE);
					
					// BigDecimal
				
       			    	dos.writeObject(this.AMOUNT);
					
					// BigDecimal
				
       			    	dos.writeObject(this.AMOUNT_BASE);
					
					// long
				
		            	dos.writeLong(this.IS_DEBIT);
					
					// BigDecimal
				
       			    	dos.writeObject(this.QUANTITY);
					
					// Long
				
						if(this.REF_PROGRAM == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeLong(this.REF_PROGRAM);
		            	}
					
					// Long
				
						if(this.REF_FUND == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeLong(this.REF_FUND);
		            	}
					
					// Long
				
						if(this.REF_ORG_DEPARTMENT == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeLong(this.REF_ORG_DEPARTMENT);
		            	}
					
					// Long
				
						if(this.REF_REGION == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeLong(this.REF_REGION);
		            	}
					
					// Long
				
						if(this.REF_FUNCTION == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeLong(this.REF_FUNCTION);
		            	}
					
					// String
				
						writeString(this.DESCRIPTION,dos);
					
					// long
				
		            	dos.writeLong(this.REF_CLIENT);
					
					// long
				
		            	dos.writeLong(this.VERSION);
					
					// long
				
		            	dos.writeLong(this.CREATED_BY);
					
					// long
				
		            	dos.writeLong(this.UPDATED_BY);
					
					// java.util.Date
				
						writeDate(this.CREATED_DATE,dos);
					
					// java.util.Date
				
						writeDate(this.UPDATED_DATE,dos);
					
					// long
				
		            	dos.writeLong(this.REF_ORGANIZATION_ORIGINAL);
					
					// long
				
		            	dos.writeLong(this.LINE_NO_SUB);
					
					// long
				
		            	dos.writeLong(this.LINE_NO_QUEUE);
					
					// String
				
						writeString(this.REF_ORIG_TABLE,dos);
					
					// long
				
		            	dos.writeLong(this.REF_ORIG_TABLE_ID);
					
					// String
				
						writeString(this.GL_TYPE,dos);
					
					// long
				
		            	dos.writeLong(this.COMMITMENT_TYPE);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("ID="+String.valueOf(ID));
		sb.append(",REF_ORGANIZATION="+String.valueOf(REF_ORGANIZATION));
		sb.append(",REF_LEDGER_DOCUMENT="+String.valueOf(REF_LEDGER_DOCUMENT));
		sb.append(",REF_LEDGER_PERIOD="+String.valueOf(REF_LEDGER_PERIOD));
		sb.append(",REF_TRANSACTION_TYPE="+String.valueOf(REF_TRANSACTION_TYPE));
		sb.append(",LINE_NO="+String.valueOf(LINE_NO));
		sb.append(",TRANSACTION_DATE="+String.valueOf(TRANSACTION_DATE));
		sb.append(",REF_ACCOUNT="+String.valueOf(REF_ACCOUNT));
		sb.append(",REF_CURRENCY="+String.valueOf(REF_CURRENCY));
		sb.append(",CURRENCY_RATE="+String.valueOf(CURRENCY_RATE));
		sb.append(",AMOUNT="+String.valueOf(AMOUNT));
		sb.append(",AMOUNT_BASE="+String.valueOf(AMOUNT_BASE));
		sb.append(",IS_DEBIT="+String.valueOf(IS_DEBIT));
		sb.append(",QUANTITY="+String.valueOf(QUANTITY));
		sb.append(",REF_PROGRAM="+String.valueOf(REF_PROGRAM));
		sb.append(",REF_FUND="+String.valueOf(REF_FUND));
		sb.append(",REF_ORG_DEPARTMENT="+String.valueOf(REF_ORG_DEPARTMENT));
		sb.append(",REF_REGION="+String.valueOf(REF_REGION));
		sb.append(",REF_FUNCTION="+String.valueOf(REF_FUNCTION));
		sb.append(",DESCRIPTION="+DESCRIPTION);
		sb.append(",REF_CLIENT="+String.valueOf(REF_CLIENT));
		sb.append(",VERSION="+String.valueOf(VERSION));
		sb.append(",CREATED_BY="+String.valueOf(CREATED_BY));
		sb.append(",UPDATED_BY="+String.valueOf(UPDATED_BY));
		sb.append(",CREATED_DATE="+String.valueOf(CREATED_DATE));
		sb.append(",UPDATED_DATE="+String.valueOf(UPDATED_DATE));
		sb.append(",REF_ORGANIZATION_ORIGINAL="+String.valueOf(REF_ORGANIZATION_ORIGINAL));
		sb.append(",LINE_NO_SUB="+String.valueOf(LINE_NO_SUB));
		sb.append(",LINE_NO_QUEUE="+String.valueOf(LINE_NO_QUEUE));
		sb.append(",REF_ORIG_TABLE="+REF_ORIG_TABLE);
		sb.append(",REF_ORIG_TABLE_ID="+String.valueOf(REF_ORIG_TABLE_ID));
		sb.append(",GL_TYPE="+GL_TYPE);
		sb.append(",COMMITMENT_TYPE="+String.valueOf(COMMITMENT_TYPE));
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(after_tDBInput_2Struct other) {

		int returnValue = -1;
		
						returnValue = checkNullsAndCompare(this.ID, other.ID);
						if(returnValue != 0) {
							return returnValue;
						}

					
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}
public void tDBInput_2Process(final java.util.Map<String, Object> globalMap) throws TalendException {
	globalMap.put("tDBInput_2_SUBPROCESS_STATE", 0);

 final boolean execStat = this.execStat;
	
		String iterateId = "";
	
	
	String currentComponent = "";
	java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

	try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception().getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { //start the resume
				globalResumeTicket = true;


		tDBInput_7Process(globalMap);

		row3Struct row3 = new row3Struct();
outInsertStruct outInsert = new outInsertStruct();





	
	/**
	 * [tDBOutput_6 begin ] start
	 */

	

	
		
		ok_Hash.put("tDBOutput_6", false);
		start_Hash.put("tDBOutput_6", System.currentTimeMillis());
		
	
	currentComponent="tDBOutput_6";

	
			if (execStat) {
				if(resourceMap.get("inIterateVComp") == null){
					
						runStat.updateStatOnConnection("outInsert" + iterateId, 0, 0);
					
				}
			} 

		
		int tos_count_tDBOutput_6 = 0;
		
    	class BytesLimit65535_tDBOutput_6{
    		public void limitLog4jByte() throws Exception{
    			
    		}
    	}
    	
        new BytesLimit65535_tDBOutput_6().limitLog4jByte();




int nb_line_tDBOutput_6 = 0;
int nb_line_update_tDBOutput_6 = 0;
int nb_line_inserted_tDBOutput_6 = 0;
int nb_line_deleted_tDBOutput_6 = 0;
int nb_line_rejected_tDBOutput_6 = 0;
int nb_line_copied_tDBOutput_6 = 0;

int deletedCount_tDBOutput_6=0;
int updatedCount_tDBOutput_6=0;
int insertedCount_tDBOutput_6=0;
int rejectedCount_tDBOutput_6=0;
int copiedCount_tDBOutput_6=0;

String tableName_tDBOutput_6 = null;
String dbschema_tDBOutput_6 = null;
boolean whetherReject_tDBOutput_6 = false;

		    java.sql.Connection conn_tDBOutput_6 = null;
		   
		   		 dbschema_tDBOutput_6 = context.Vertica_ODS_Schema;
			
					String driverClass_tDBOutput_6 = "com.vertica.jdbc.Driver";
			
				java.lang.Class.forName(driverClass_tDBOutput_6);
        		
				
				String url_tDBOutput_6 = "jdbc:vertica://" + context.Vertica_ODS_Server + ":" + context.Vertica_ODS_Port + "/" + context.Vertica_ODS_Database + "?" + context.Vertica_ODS_AdditionalParams;
				

				String dbUser_tDBOutput_6 = context.Vertica_ODS_Login;
				

				
	final String decryptedPassword_tDBOutput_6 = context.Vertica_ODS_Password; 

				String dbPwd_tDBOutput_6 = decryptedPassword_tDBOutput_6;
				
				conn_tDBOutput_6 = java.sql.DriverManager.getConnection(url_tDBOutput_6,dbUser_tDBOutput_6,dbPwd_tDBOutput_6);
				
				resourceMap.put("conn_tDBOutput_6", conn_tDBOutput_6);
			

if(dbschema_tDBOutput_6 == null || dbschema_tDBOutput_6.trim().length() == 0) {
    tableName_tDBOutput_6 = "LEDGER_TRANSACTION2";
} else {
    tableName_tDBOutput_6 = dbschema_tDBOutput_6 + "." + "LEDGER_TRANSACTION2";
}
conn_tDBOutput_6.setAutoCommit(false);

int commitEvery_tDBOutput_6 = 1000;

int commitCounter_tDBOutput_6 = 0;
   int batchSize_tDBOutput_6 = 1000;
   int batchSizeCounter_tDBOutput_6=0;

	int count_tDBOutput_6=0;
	
        String insert_tDBOutput_6 = "INSERT INTO " + tableName_tDBOutput_6 + " (ID,REF_ORGANIZATION,REF_LEDGER_DOCUMENT,REF_LEDGER_PERIOD,REF_TRANSACTION_TYPE,LINE_NO,TRANSACTION_DATE,REF_ACCOUNT,REF_CURRENCY,CURRENCY_RATE,AMOUNT,AMOUNT_BASE,IS_DEBIT,QUANTITY,REF_PROGRAM,REF_FUND,REF_ORG_DEPARTMENT,REF_REGION,REF_FUNCTION,DESCRIPTION,REF_CLIENT,VERSION,CREATED_BY,UPDATED_BY,CREATED_DATE,UPDATED_DATE,REF_ORGANIZATION_ORIGINAL,LINE_NO_SUB,LINE_NO_QUEUE,REF_ORIG_TABLE,REF_ORIG_TABLE_ID,GL_TYPE,COMMITMENT_TYPE,etl_date) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        java.sql.PreparedStatement pstmt_tDBOutput_6 = conn_tDBOutput_6.prepareStatement(insert_tDBOutput_6);
        int batchCount_tDBOutput_6 = 0;

 



/**
 * [tDBOutput_6 begin ] stop
 */



	
	/**
	 * [tMap_1 begin ] start
	 */

	

	
		
		ok_Hash.put("tMap_1", false);
		start_Hash.put("tMap_1", System.currentTimeMillis());
		
	
	currentComponent="tMap_1";

	
			if (execStat) {
				if(resourceMap.get("inIterateVComp") == null){
					
						runStat.updateStatOnConnection("row3" + iterateId, 0, 0);
					
				}
			} 

		
		int tos_count_tMap_1 = 0;
		
    	class BytesLimit65535_tMap_1{
    		public void limitLog4jByte() throws Exception{
    			
    		}
    	}
    	
        new BytesLimit65535_tMap_1().limitLog4jByte();




// ###############################
// # Lookup's keys initialization
	
		org.talend.designer.components.lookup.memory.AdvancedMemoryLookup<row5Struct> tHash_Lookup_row5 = (org.talend.designer.components.lookup.memory.AdvancedMemoryLookup<row5Struct>) 
				((org.talend.designer.components.lookup.memory.AdvancedMemoryLookup<row5Struct>) 
					globalMap.get( "tHash_Lookup_row5" ))
					;					
					
	

row5Struct row5HashKey = new row5Struct();
row5Struct row5Default = new row5Struct();
// ###############################        

// ###############################
// # Vars initialization
class  Var__tMap_1__Struct  {
}
Var__tMap_1__Struct Var__tMap_1 = new Var__tMap_1__Struct();
// ###############################

// ###############################
// # Outputs initialization
outInsertStruct outInsert_tmp = new outInsertStruct();
// ###############################

        
        



        









 



/**
 * [tMap_1 begin ] stop
 */



	
	/**
	 * [tDBInput_2 begin ] start
	 */

	

	
		
		ok_Hash.put("tDBInput_2", false);
		start_Hash.put("tDBInput_2", System.currentTimeMillis());
		
	
	currentComponent="tDBInput_2";

	
		int tos_count_tDBInput_2 = 0;
		
    	class BytesLimit65535_tDBInput_2{
    		public void limitLog4jByte() throws Exception{
    			
    		}
    	}
    	
        new BytesLimit65535_tDBInput_2().limitLog4jByte();
	


	
		    int nb_line_tDBInput_2 = 0;
		    java.sql.Connection conn_tDBInput_2 = null;
				String driverClass_tDBInput_2 = "oracle.jdbc.OracleDriver";
				java.lang.Class.forName(driverClass_tDBInput_2);
				
			String url_tDBInput_2 = null;
				url_tDBInput_2 = "jdbc:oracle:thin:@(description=(address=(protocol=tcp)(host=" + context.Oracle_Server + ")(port=" + context.Oracle_Port + "))(connect_data=(service_name=" + context.Oracle_ServiceName + ")))";

				String dbUser_tDBInput_2 = context.Oracle_Login;

				

				
	final String decryptedPassword_tDBInput_2 = context.Oracle_Password; 

				String dbPwd_tDBInput_2 = decryptedPassword_tDBInput_2;

				
					java.util.Properties atnParamsPrope_tDBInput_2 = new java.util.Properties();
					atnParamsPrope_tDBInput_2.put("user",dbUser_tDBInput_2);
					atnParamsPrope_tDBInput_2.put("password",dbPwd_tDBInput_2);
                    if(context.Oracle_AdditionalParams != null && !"\"\"".equals(context.Oracle_AdditionalParams) && !"".equals(context.Oracle_AdditionalParams)){
                        atnParamsPrope_tDBInput_2.load(new java.io.ByteArrayInputStream(context.Oracle_AdditionalParams.replace("&", "\n").getBytes()));
                    }
					conn_tDBInput_2 = java.sql.DriverManager.getConnection(url_tDBInput_2, atnParamsPrope_tDBInput_2);
				java.sql.Statement stmtGetTZ_tDBInput_2 = conn_tDBInput_2.createStatement();
				java.sql.ResultSet rsGetTZ_tDBInput_2 = stmtGetTZ_tDBInput_2.executeQuery("select sessiontimezone from dual");
				String sessionTimezone_tDBInput_2 = java.util.TimeZone.getDefault().getID();
				while (rsGetTZ_tDBInput_2.next()) {
					sessionTimezone_tDBInput_2 = rsGetTZ_tDBInput_2.getString(1);
				}
                                if (!(conn_tDBInput_2 instanceof oracle.jdbc.OracleConnection) &&
                                        conn_tDBInput_2.isWrapperFor(oracle.jdbc.OracleConnection.class)) {
                                    if (conn_tDBInput_2.unwrap(oracle.jdbc.OracleConnection.class) != null) {
                                        ((oracle.jdbc.OracleConnection)conn_tDBInput_2.unwrap(oracle.jdbc.OracleConnection.class)).setSessionTimeZone(sessionTimezone_tDBInput_2);
                                    }
                                } else {
                                    ((oracle.jdbc.OracleConnection)conn_tDBInput_2).setSessionTimeZone(sessionTimezone_tDBInput_2);
                                }
		    
			java.sql.Statement stmt_tDBInput_2 = conn_tDBInput_2.createStatement();

		    String dbquery_tDBInput_2 = "select * from PFM.LEDGER_TRANSACTION  where EXTRACT(YEAR FROM transaction_date)=2017 and EXTRACT(month FROM transaction"
+"_date)=12";
			

            	globalMap.put("tDBInput_2_QUERY",dbquery_tDBInput_2);
		    java.sql.ResultSet rs_tDBInput_2 = null;

		    try {
		    	rs_tDBInput_2 = stmt_tDBInput_2.executeQuery(dbquery_tDBInput_2);
		    	java.sql.ResultSetMetaData rsmd_tDBInput_2 = rs_tDBInput_2.getMetaData();
		    	int colQtyInRs_tDBInput_2 = rsmd_tDBInput_2.getColumnCount();

		    String tmpContent_tDBInput_2 = null;
		    
		    
		    while (rs_tDBInput_2.next()) {
		        nb_line_tDBInput_2++;
		        
							if(colQtyInRs_tDBInput_2 < 1) {
								row3.ID = 0;
							} else {
		                          
					if(rs_tDBInput_2.getObject(1) != null) {
						row3.ID = rs_tDBInput_2.getLong(1);
					} else {
				
 	                	throw new RuntimeException("Null value in non-Nullable column");
					}
		                    }
							if(colQtyInRs_tDBInput_2 < 2) {
								row3.REF_ORGANIZATION = 0;
							} else {
		                          
					if(rs_tDBInput_2.getObject(2) != null) {
						row3.REF_ORGANIZATION = rs_tDBInput_2.getLong(2);
					} else {
				
 	                	throw new RuntimeException("Null value in non-Nullable column");
					}
		                    }
							if(colQtyInRs_tDBInput_2 < 3) {
								row3.REF_LEDGER_DOCUMENT = 0;
							} else {
		                          
					if(rs_tDBInput_2.getObject(3) != null) {
						row3.REF_LEDGER_DOCUMENT = rs_tDBInput_2.getLong(3);
					} else {
				
 	                	throw new RuntimeException("Null value in non-Nullable column");
					}
		                    }
							if(colQtyInRs_tDBInput_2 < 4) {
								row3.REF_LEDGER_PERIOD = 0;
							} else {
		                          
					if(rs_tDBInput_2.getObject(4) != null) {
						row3.REF_LEDGER_PERIOD = rs_tDBInput_2.getLong(4);
					} else {
				
 	                	throw new RuntimeException("Null value in non-Nullable column");
					}
		                    }
							if(colQtyInRs_tDBInput_2 < 5) {
								row3.REF_TRANSACTION_TYPE = 0;
							} else {
		                          
					if(rs_tDBInput_2.getObject(5) != null) {
						row3.REF_TRANSACTION_TYPE = rs_tDBInput_2.getLong(5);
					} else {
				
 	                	throw new RuntimeException("Null value in non-Nullable column");
					}
		                    }
							if(colQtyInRs_tDBInput_2 < 6) {
								row3.LINE_NO = 0;
							} else {
		                          
					if(rs_tDBInput_2.getObject(6) != null) {
						row3.LINE_NO = rs_tDBInput_2.getLong(6);
					} else {
				
 	                	throw new RuntimeException("Null value in non-Nullable column");
					}
		                    }
							if(colQtyInRs_tDBInput_2 < 7) {
								row3.TRANSACTION_DATE = null;
							} else {
										
			row3.TRANSACTION_DATE = routines.system.JDBCUtil.getDate(rs_tDBInput_2, 7);
		                    }
							if(colQtyInRs_tDBInput_2 < 8) {
								row3.REF_ACCOUNT = 0;
							} else {
		                          
					if(rs_tDBInput_2.getObject(8) != null) {
						row3.REF_ACCOUNT = rs_tDBInput_2.getLong(8);
					} else {
				
 	                	throw new RuntimeException("Null value in non-Nullable column");
					}
		                    }
							if(colQtyInRs_tDBInput_2 < 9) {
								row3.REF_CURRENCY = 0;
							} else {
		                          
					if(rs_tDBInput_2.getObject(9) != null) {
						row3.REF_CURRENCY = rs_tDBInput_2.getLong(9);
					} else {
				
 	                	throw new RuntimeException("Null value in non-Nullable column");
					}
		                    }
							if(colQtyInRs_tDBInput_2 < 10) {
								row3.CURRENCY_RATE = null;
							} else {
		                          
					if(rs_tDBInput_2.getObject(10) != null) {
						row3.CURRENCY_RATE = rs_tDBInput_2.getBigDecimal(10);
					} else {
				
 	                	throw new RuntimeException("Null value in non-Nullable column");
					}
		                    }
							if(colQtyInRs_tDBInput_2 < 11) {
								row3.AMOUNT = null;
							} else {
		                          
					if(rs_tDBInput_2.getObject(11) != null) {
						row3.AMOUNT = rs_tDBInput_2.getBigDecimal(11);
					} else {
				
 	                	throw new RuntimeException("Null value in non-Nullable column");
					}
		                    }
							if(colQtyInRs_tDBInput_2 < 12) {
								row3.AMOUNT_BASE = null;
							} else {
		                          
					if(rs_tDBInput_2.getObject(12) != null) {
						row3.AMOUNT_BASE = rs_tDBInput_2.getBigDecimal(12);
					} else {
				
 	                	throw new RuntimeException("Null value in non-Nullable column");
					}
		                    }
							if(colQtyInRs_tDBInput_2 < 13) {
								row3.IS_DEBIT = 0;
							} else {
		                          
					if(rs_tDBInput_2.getObject(13) != null) {
						row3.IS_DEBIT = rs_tDBInput_2.getLong(13);
					} else {
				
 	                	throw new RuntimeException("Null value in non-Nullable column");
					}
		                    }
							if(colQtyInRs_tDBInput_2 < 14) {
								row3.QUANTITY = null;
							} else {
		                          
					if(rs_tDBInput_2.getObject(14) != null) {
						row3.QUANTITY = rs_tDBInput_2.getBigDecimal(14);
					} else {
				
 	                	throw new RuntimeException("Null value in non-Nullable column");
					}
		                    }
							if(colQtyInRs_tDBInput_2 < 15) {
								row3.REF_PROGRAM = null;
							} else {
		                          
					if(rs_tDBInput_2.getObject(15) != null) {
						row3.REF_PROGRAM = rs_tDBInput_2.getLong(15);
					} else {
				
						row3.REF_PROGRAM = null;
					}
		                    }
							if(colQtyInRs_tDBInput_2 < 16) {
								row3.REF_FUND = null;
							} else {
		                          
					if(rs_tDBInput_2.getObject(16) != null) {
						row3.REF_FUND = rs_tDBInput_2.getLong(16);
					} else {
				
						row3.REF_FUND = null;
					}
		                    }
							if(colQtyInRs_tDBInput_2 < 17) {
								row3.REF_ORG_DEPARTMENT = null;
							} else {
		                          
					if(rs_tDBInput_2.getObject(17) != null) {
						row3.REF_ORG_DEPARTMENT = rs_tDBInput_2.getLong(17);
					} else {
				
						row3.REF_ORG_DEPARTMENT = null;
					}
		                    }
							if(colQtyInRs_tDBInput_2 < 18) {
								row3.REF_REGION = null;
							} else {
		                          
					if(rs_tDBInput_2.getObject(18) != null) {
						row3.REF_REGION = rs_tDBInput_2.getLong(18);
					} else {
				
						row3.REF_REGION = null;
					}
		                    }
							if(colQtyInRs_tDBInput_2 < 19) {
								row3.REF_FUNCTION = null;
							} else {
		                          
					if(rs_tDBInput_2.getObject(19) != null) {
						row3.REF_FUNCTION = rs_tDBInput_2.getLong(19);
					} else {
				
						row3.REF_FUNCTION = null;
					}
		                    }
							if(colQtyInRs_tDBInput_2 < 20) {
								row3.DESCRIPTION = null;
							} else {
	                         		
        	row3.DESCRIPTION = routines.system.JDBCUtil.getString(rs_tDBInput_2, 20, false);
		                    }
							if(colQtyInRs_tDBInput_2 < 21) {
								row3.REF_CLIENT = 0;
							} else {
		                          
					if(rs_tDBInput_2.getObject(21) != null) {
						row3.REF_CLIENT = rs_tDBInput_2.getLong(21);
					} else {
				
 	                	throw new RuntimeException("Null value in non-Nullable column");
					}
		                    }
							if(colQtyInRs_tDBInput_2 < 22) {
								row3.VERSION = 0;
							} else {
		                          
					if(rs_tDBInput_2.getObject(22) != null) {
						row3.VERSION = rs_tDBInput_2.getLong(22);
					} else {
				
 	                	throw new RuntimeException("Null value in non-Nullable column");
					}
		                    }
							if(colQtyInRs_tDBInput_2 < 23) {
								row3.CREATED_BY = 0;
							} else {
		                          
					if(rs_tDBInput_2.getObject(23) != null) {
						row3.CREATED_BY = rs_tDBInput_2.getLong(23);
					} else {
				
 	                	throw new RuntimeException("Null value in non-Nullable column");
					}
		                    }
							if(colQtyInRs_tDBInput_2 < 24) {
								row3.UPDATED_BY = 0;
							} else {
		                          
					if(rs_tDBInput_2.getObject(24) != null) {
						row3.UPDATED_BY = rs_tDBInput_2.getLong(24);
					} else {
				
 	                	throw new RuntimeException("Null value in non-Nullable column");
					}
		                    }
							if(colQtyInRs_tDBInput_2 < 25) {
								row3.CREATED_DATE = null;
							} else {
										
			row3.CREATED_DATE = routines.system.JDBCUtil.getDate(rs_tDBInput_2, 25);
		                    }
							if(colQtyInRs_tDBInput_2 < 26) {
								row3.UPDATED_DATE = null;
							} else {
										
			row3.UPDATED_DATE = routines.system.JDBCUtil.getDate(rs_tDBInput_2, 26);
		                    }
							if(colQtyInRs_tDBInput_2 < 27) {
								row3.REF_ORGANIZATION_ORIGINAL = 0;
							} else {
		                          
					if(rs_tDBInput_2.getObject(27) != null) {
						row3.REF_ORGANIZATION_ORIGINAL = rs_tDBInput_2.getLong(27);
					} else {
				
 	                	throw new RuntimeException("Null value in non-Nullable column");
					}
		                    }
							if(colQtyInRs_tDBInput_2 < 28) {
								row3.LINE_NO_SUB = 0;
							} else {
		                          
					if(rs_tDBInput_2.getObject(28) != null) {
						row3.LINE_NO_SUB = rs_tDBInput_2.getLong(28);
					} else {
				
 	                	throw new RuntimeException("Null value in non-Nullable column");
					}
		                    }
							if(colQtyInRs_tDBInput_2 < 29) {
								row3.LINE_NO_QUEUE = 0;
							} else {
		                          
					if(rs_tDBInput_2.getObject(29) != null) {
						row3.LINE_NO_QUEUE = rs_tDBInput_2.getLong(29);
					} else {
				
 	                	throw new RuntimeException("Null value in non-Nullable column");
					}
		                    }
							if(colQtyInRs_tDBInput_2 < 30) {
								row3.REF_ORIG_TABLE = null;
							} else {
	                         		
        	row3.REF_ORIG_TABLE = routines.system.JDBCUtil.getString(rs_tDBInput_2, 30, false);
		                    }
							if(colQtyInRs_tDBInput_2 < 31) {
								row3.REF_ORIG_TABLE_ID = 0;
							} else {
		                          
					if(rs_tDBInput_2.getObject(31) != null) {
						row3.REF_ORIG_TABLE_ID = rs_tDBInput_2.getLong(31);
					} else {
				
 	                	throw new RuntimeException("Null value in non-Nullable column");
					}
		                    }
							if(colQtyInRs_tDBInput_2 < 32) {
								row3.GL_TYPE = null;
							} else {
	                         		
        	row3.GL_TYPE = routines.system.JDBCUtil.getString(rs_tDBInput_2, 32, false);
		                    }
							if(colQtyInRs_tDBInput_2 < 33) {
								row3.COMMITMENT_TYPE = 0;
							} else {
		                          
					if(rs_tDBInput_2.getObject(33) != null) {
						row3.COMMITMENT_TYPE = rs_tDBInput_2.getLong(33);
					} else {
				
 	                	throw new RuntimeException("Null value in non-Nullable column");
					}
		                    }
					




 



/**
 * [tDBInput_2 begin ] stop
 */
	
	/**
	 * [tDBInput_2 main ] start
	 */

	

	
	
	currentComponent="tDBInput_2";

	

 


	tos_count_tDBInput_2++;

/**
 * [tDBInput_2 main ] stop
 */
	
	/**
	 * [tDBInput_2 process_data_begin ] start
	 */

	

	
	
	currentComponent="tDBInput_2";

	

 



/**
 * [tDBInput_2 process_data_begin ] stop
 */

	
	/**
	 * [tMap_1 main ] start
	 */

	

	
	
	currentComponent="tMap_1";

	

			//row3
			//row3


			
				if(execStat){
					runStat.updateStatOnConnection("row3"+iterateId,1, 1);
				} 
			

		

			


		
		
		boolean hasCasePrimitiveKeyWithNull_tMap_1 = false;
		
        // ###############################
        // # Input tables (lookups)
		  boolean rejectedInnerJoin_tMap_1 = false;
		  boolean mainRowRejected_tMap_1 = false;
            				    								  
		

				///////////////////////////////////////////////
				// Starting Lookup Table "row5" 
				///////////////////////////////////////////////


				
				
                            
 					    boolean forceLooprow5 = false;
       		  	    	
       		  	    	
 							row5Struct row5ObjectFromLookup = null;
                          
		           		  	if(!rejectedInnerJoin_tMap_1) { // G_TM_M_020

								
								hasCasePrimitiveKeyWithNull_tMap_1 = false;
								
	                        		    	Object exprKeyValue_row5__ID = routines.system.TypeConvert.Long2Integer(row3.ID );
	                        		    	if(exprKeyValue_row5__ID == null) {
	                        		    		hasCasePrimitiveKeyWithNull_tMap_1 = true;
	                        		    	} else {
                        		    			row5HashKey.ID = (int)(Integer) exprKeyValue_row5__ID;
                        		    		}
                        		    		

								
		                        	row5HashKey.hashCodeDirty = true;
                        		
	  					
	  							
	
		  							if(!hasCasePrimitiveKeyWithNull_tMap_1) { // G_TM_M_091
		  							
			  					
			  					
			  					
	  					
		  							tHash_Lookup_row5.lookup( row5HashKey );

	  							

	  							

			  						} // G_TM_M_091
			  						
			  					

 								
								  
								  if(hasCasePrimitiveKeyWithNull_tMap_1 || !tHash_Lookup_row5.hasNext()) { // G_TM_M_090

  								
		  				
	  								
			  							rejectedInnerJoin_tMap_1 = true;
	  								
						
									
  									  		
 								
								  
								  } // G_TM_M_090

  								



							} // G_TM_M_020
			           		  	  
							
				           		if(tHash_Lookup_row5 != null && tHash_Lookup_row5.getCount(row5HashKey) > 1) { // G 071
			  							
			  						
									 		
									//System.out.println("WARNING: UNIQUE MATCH is configured for the lookup 'row5' and it contains more one result from keys :  row5.ID = '" + row5HashKey.ID + "'");
								} // G 071
							

							row5Struct row5 = null;
                    		  	 
							   
                    		  	 
	       		  	    	row5Struct fromLookup_row5 = null;
							row5 = row5Default;
										 
							
								 
							
							
								if (tHash_Lookup_row5 !=null && tHash_Lookup_row5.hasNext()) { // G 099
								
							
								
								fromLookup_row5 = tHash_Lookup_row5.next();

							
							
								} // G 099
							
							

							if(fromLookup_row5 != null) {
								row5 = fromLookup_row5;
							}
							
							
							
			  							
								
	                    		  	
		                    
	            	
	            	
	            // ###############################
        { // start of Var scope
        
	        // ###############################
        	// # Vars tables
        
Var__tMap_1__Struct Var = Var__tMap_1;// ###############################
        // ###############################
        // # Output tables

outInsert = null;

if(!rejectedInnerJoin_tMap_1 ) {
} // closing inner join bracket (1)
// ###### START REJECTS ##### 

// # Output reject table : 'outInsert'
// # Filter conditions 
if( rejectedInnerJoin_tMap_1 ) {
outInsert_tmp.ID = row3.ID ;
outInsert_tmp.REF_ORGANIZATION = row3.REF_ORGANIZATION ;
outInsert_tmp.REF_LEDGER_DOCUMENT = row3.REF_LEDGER_DOCUMENT ;
outInsert_tmp.REF_LEDGER_PERIOD = row3.REF_LEDGER_PERIOD ;
outInsert_tmp.REF_TRANSACTION_TYPE = row3.REF_TRANSACTION_TYPE ;
outInsert_tmp.LINE_NO = row3.LINE_NO ;
outInsert_tmp.TRANSACTION_DATE = row3.TRANSACTION_DATE ;
outInsert_tmp.REF_ACCOUNT = row3.REF_ACCOUNT ;
outInsert_tmp.REF_CURRENCY = row3.REF_CURRENCY ;
outInsert_tmp.CURRENCY_RATE = row3.CURRENCY_RATE ;
outInsert_tmp.AMOUNT = row3.AMOUNT ;
outInsert_tmp.AMOUNT_BASE = row3.AMOUNT_BASE ;
outInsert_tmp.IS_DEBIT = row3.IS_DEBIT ;
outInsert_tmp.QUANTITY = row3.QUANTITY ;
outInsert_tmp.REF_PROGRAM = row3.REF_PROGRAM ;
outInsert_tmp.REF_FUND = row3.REF_FUND ;
outInsert_tmp.REF_ORG_DEPARTMENT = row3.REF_ORG_DEPARTMENT ;
outInsert_tmp.REF_REGION = row3.REF_REGION ;
outInsert_tmp.REF_FUNCTION = row3.REF_FUNCTION ;
outInsert_tmp.DESCRIPTION = row3.DESCRIPTION ;
outInsert_tmp.REF_CLIENT = row3.REF_CLIENT ;
outInsert_tmp.VERSION = row3.VERSION ;
outInsert_tmp.CREATED_BY = row3.CREATED_BY ;
outInsert_tmp.UPDATED_BY = row3.UPDATED_BY ;
outInsert_tmp.CREATED_DATE = row3.CREATED_DATE ;
outInsert_tmp.UPDATED_DATE = row3.UPDATED_DATE ;
outInsert_tmp.REF_ORGANIZATION_ORIGINAL = row3.REF_ORGANIZATION_ORIGINAL ;
outInsert_tmp.LINE_NO_SUB = row3.LINE_NO_SUB ;
outInsert_tmp.LINE_NO_QUEUE = row3.LINE_NO_QUEUE ;
outInsert_tmp.REF_ORIG_TABLE = row3.REF_ORIG_TABLE ;
outInsert_tmp.REF_ORIG_TABLE_ID = row3.REF_ORIG_TABLE_ID ;
outInsert_tmp.GL_TYPE = row3.GL_TYPE ;
outInsert_tmp.COMMITMENT_TYPE = row3.COMMITMENT_TYPE ;
outInsert_tmp.etl_date = TalendDate.parseDate("yyyy-MM-dd HH:mm:ss",TalendDate.getDate("yyyy-MM-DD hh:mm:ss")) ;
outInsert = outInsert_tmp;
} // closing filter/reject
// ###############################

} // end of Var scope

rejectedInnerJoin_tMap_1 = false;










 


	tos_count_tMap_1++;

/**
 * [tMap_1 main ] stop
 */
	
	/**
	 * [tMap_1 process_data_begin ] start
	 */

	

	
	
	currentComponent="tMap_1";

	

 



/**
 * [tMap_1 process_data_begin ] stop
 */
// Start of branch "outInsert"
if(outInsert != null) { 



	
	/**
	 * [tDBOutput_6 main ] start
	 */

	

	
	
	currentComponent="tDBOutput_6";

	

			//outInsert
			//outInsert


			
				if(execStat){
					runStat.updateStatOnConnection("outInsert"+iterateId,1, 1);
				} 
			

		



            whetherReject_tDBOutput_6 = false;
                        pstmt_tDBOutput_6.setLong(1, outInsert.ID);

                        pstmt_tDBOutput_6.setLong(2, outInsert.REF_ORGANIZATION);

                        pstmt_tDBOutput_6.setLong(3, outInsert.REF_LEDGER_DOCUMENT);

                        pstmt_tDBOutput_6.setLong(4, outInsert.REF_LEDGER_PERIOD);

                        pstmt_tDBOutput_6.setLong(5, outInsert.REF_TRANSACTION_TYPE);

                        pstmt_tDBOutput_6.setLong(6, outInsert.LINE_NO);

                        if(outInsert.TRANSACTION_DATE != null) {
pstmt_tDBOutput_6.setTimestamp(7, new java.sql.Timestamp(outInsert.TRANSACTION_DATE.getTime()));
} else {
pstmt_tDBOutput_6.setNull(7, java.sql.Types.TIMESTAMP);
}

                        pstmt_tDBOutput_6.setLong(8, outInsert.REF_ACCOUNT);

                        pstmt_tDBOutput_6.setLong(9, outInsert.REF_CURRENCY);

                        pstmt_tDBOutput_6.setBigDecimal(10, outInsert.CURRENCY_RATE);

                        pstmt_tDBOutput_6.setBigDecimal(11, outInsert.AMOUNT);

                        pstmt_tDBOutput_6.setBigDecimal(12, outInsert.AMOUNT_BASE);

                        pstmt_tDBOutput_6.setLong(13, outInsert.IS_DEBIT);

                        pstmt_tDBOutput_6.setBigDecimal(14, outInsert.QUANTITY);

                        if(outInsert.REF_PROGRAM == null) {
pstmt_tDBOutput_6.setNull(15, java.sql.Types.INTEGER);
} else {pstmt_tDBOutput_6.setLong(15, outInsert.REF_PROGRAM);
}

                        if(outInsert.REF_FUND == null) {
pstmt_tDBOutput_6.setNull(16, java.sql.Types.INTEGER);
} else {pstmt_tDBOutput_6.setLong(16, outInsert.REF_FUND);
}

                        if(outInsert.REF_ORG_DEPARTMENT == null) {
pstmt_tDBOutput_6.setNull(17, java.sql.Types.INTEGER);
} else {pstmt_tDBOutput_6.setLong(17, outInsert.REF_ORG_DEPARTMENT);
}

                        if(outInsert.REF_REGION == null) {
pstmt_tDBOutput_6.setNull(18, java.sql.Types.INTEGER);
} else {pstmt_tDBOutput_6.setLong(18, outInsert.REF_REGION);
}

                        if(outInsert.REF_FUNCTION == null) {
pstmt_tDBOutput_6.setNull(19, java.sql.Types.INTEGER);
} else {pstmt_tDBOutput_6.setLong(19, outInsert.REF_FUNCTION);
}

                        if(outInsert.DESCRIPTION == null) {
pstmt_tDBOutput_6.setNull(20, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_6.setString(20, outInsert.DESCRIPTION);
}

                        pstmt_tDBOutput_6.setLong(21, outInsert.REF_CLIENT);

                        pstmt_tDBOutput_6.setLong(22, outInsert.VERSION);

                        pstmt_tDBOutput_6.setLong(23, outInsert.CREATED_BY);

                        pstmt_tDBOutput_6.setLong(24, outInsert.UPDATED_BY);

                        if(outInsert.CREATED_DATE != null) {
pstmt_tDBOutput_6.setTimestamp(25, new java.sql.Timestamp(outInsert.CREATED_DATE.getTime()));
} else {
pstmt_tDBOutput_6.setNull(25, java.sql.Types.TIMESTAMP);
}

                        if(outInsert.UPDATED_DATE != null) {
pstmt_tDBOutput_6.setTimestamp(26, new java.sql.Timestamp(outInsert.UPDATED_DATE.getTime()));
} else {
pstmt_tDBOutput_6.setNull(26, java.sql.Types.TIMESTAMP);
}

                        pstmt_tDBOutput_6.setLong(27, outInsert.REF_ORGANIZATION_ORIGINAL);

                        pstmt_tDBOutput_6.setLong(28, outInsert.LINE_NO_SUB);

                        pstmt_tDBOutput_6.setLong(29, outInsert.LINE_NO_QUEUE);

                        if(outInsert.REF_ORIG_TABLE == null) {
pstmt_tDBOutput_6.setNull(30, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_6.setString(30, outInsert.REF_ORIG_TABLE);
}

                        pstmt_tDBOutput_6.setLong(31, outInsert.REF_ORIG_TABLE_ID);

                        if(outInsert.GL_TYPE == null) {
pstmt_tDBOutput_6.setNull(32, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_6.setString(32, outInsert.GL_TYPE);
}

                        pstmt_tDBOutput_6.setLong(33, outInsert.COMMITMENT_TYPE);

                        if(outInsert.etl_date != null) {
pstmt_tDBOutput_6.setTimestamp(34, new java.sql.Timestamp(outInsert.etl_date.getTime()));
} else {
pstmt_tDBOutput_6.setNull(34, java.sql.Types.TIMESTAMP);
}

            pstmt_tDBOutput_6.addBatch();
            nb_line_tDBOutput_6++;
                batchSizeCounter_tDBOutput_6++;
                if ((batchSize_tDBOutput_6 > 0) && (batchSize_tDBOutput_6 <= batchSizeCounter_tDBOutput_6)) {
                try {
                        pstmt_tDBOutput_6.executeBatch();
                            insertedCount_tDBOutput_6 += pstmt_tDBOutput_6. getUpdateCount();
                        batchSizeCounter_tDBOutput_6 = 0;
                }catch (java.sql.SQLException e){
                        throw(e);
                }
            }
                commitCounter_tDBOutput_6++;
                if(commitEvery_tDBOutput_6 <= commitCounter_tDBOutput_6) {

                try {
                            boolean isCountResult_tDBOutput_6 = false;
                            if(batchSizeCounter_tDBOutput_6 > 0){
                                pstmt_tDBOutput_6.executeBatch();
                                isCountResult_tDBOutput_6 = true;
                                batchSizeCounter_tDBOutput_6 = 0;
                            }
                        if(isCountResult_tDBOutput_6){
                        insertedCount_tDBOutput_6 += pstmt_tDBOutput_6.getUpdateCount();
                        }
                }catch (java.sql.SQLException e){
                        throw(e);

                }
                    conn_tDBOutput_6.commit();
                    commitCounter_tDBOutput_6=0;
                }

 


	tos_count_tDBOutput_6++;

/**
 * [tDBOutput_6 main ] stop
 */
	
	/**
	 * [tDBOutput_6 process_data_begin ] start
	 */

	

	
	
	currentComponent="tDBOutput_6";

	

 



/**
 * [tDBOutput_6 process_data_begin ] stop
 */
	
	/**
	 * [tDBOutput_6 process_data_end ] start
	 */

	

	
	
	currentComponent="tDBOutput_6";

	

 



/**
 * [tDBOutput_6 process_data_end ] stop
 */

} // End of branch "outInsert"




	
	/**
	 * [tMap_1 process_data_end ] start
	 */

	

	
	
	currentComponent="tMap_1";

	

 



/**
 * [tMap_1 process_data_end ] stop
 */



	
	/**
	 * [tDBInput_2 process_data_end ] start
	 */

	

	
	
	currentComponent="tDBInput_2";

	

 



/**
 * [tDBInput_2 process_data_end ] stop
 */
	
	/**
	 * [tDBInput_2 end ] start
	 */

	

	
	
	currentComponent="tDBInput_2";

	

}
}finally{
stmt_tDBInput_2.close();

	if(conn_tDBInput_2 != null && !conn_tDBInput_2.isClosed()) {
	
			conn_tDBInput_2.close();
			
	}
	
}

globalMap.put("tDBInput_2_NB_LINE",nb_line_tDBInput_2);
 

ok_Hash.put("tDBInput_2", true);
end_Hash.put("tDBInput_2", System.currentTimeMillis());




/**
 * [tDBInput_2 end ] stop
 */

	
	/**
	 * [tMap_1 end ] start
	 */

	

	
	
	currentComponent="tMap_1";

	


// ###############################
// # Lookup hashes releasing
					if(tHash_Lookup_row5 != null) {
						tHash_Lookup_row5.endGet();
					}
					globalMap.remove( "tHash_Lookup_row5" );

					
					
				
// ###############################      





			if(execStat){
				if(resourceMap.get("inIterateVComp") == null || !((Boolean)resourceMap.get("inIterateVComp"))){
			 		runStat.updateStatOnConnection("row3"+iterateId,2, 0); 
			 	}
			}
		
 

ok_Hash.put("tMap_1", true);
end_Hash.put("tMap_1", System.currentTimeMillis());




/**
 * [tMap_1 end ] stop
 */

	
	/**
	 * [tDBOutput_6 end ] start
	 */

	

	
	
	currentComponent="tDBOutput_6";

	



			if (pstmt_tDBOutput_6 !=null && batchSizeCounter_tDBOutput_6 > 0 ) {  
	try {
			
			pstmt_tDBOutput_6.executeBatch();
			
			
				insertedCount_tDBOutput_6 +=pstmt_tDBOutput_6.getUpdateCount(); 
			
	}catch (java.sql.BatchUpdateException e){
		
			throw(e);
							
	}
	}
	
		
		pstmt_tDBOutput_6 = conn_tDBOutput_6.prepareStatement("COMMIT;");
		pstmt_tDBOutput_6.executeUpdate();
	
			if(pstmt_tDBOutput_6 != null) {
	
				pstmt_tDBOutput_6.close();
				
			}
		
			if (commitCounter_tDBOutput_6 > 0 ) {
				
				conn_tDBOutput_6.commit();
				
			}
				
		
		conn_tDBOutput_6.close();
		
		resourceMap.put("finish_tDBOutput_6", true);
	

	nb_line_deleted_tDBOutput_6=nb_line_deleted_tDBOutput_6+ deletedCount_tDBOutput_6;
	nb_line_update_tDBOutput_6=nb_line_update_tDBOutput_6 + updatedCount_tDBOutput_6;
	nb_line_inserted_tDBOutput_6=nb_line_inserted_tDBOutput_6 + insertedCount_tDBOutput_6;
	nb_line_rejected_tDBOutput_6=nb_line_rejected_tDBOutput_6 + rejectedCount_tDBOutput_6;
	
        globalMap.put("tDBOutput_6_NB_LINE",nb_line_tDBOutput_6);
        globalMap.put("tDBOutput_6_NB_LINE_UPDATED",nb_line_update_tDBOutput_6);
        globalMap.put("tDBOutput_6_NB_LINE_INSERTED",nb_line_inserted_tDBOutput_6);
        globalMap.put("tDBOutput_6_NB_LINE_DELETED",nb_line_deleted_tDBOutput_6);
        globalMap.put("tDBOutput_6_NB_LINE_REJECTED", nb_line_rejected_tDBOutput_6);
    
	

	nb_line_copied_tDBOutput_6=nb_line_copied_tDBOutput_6 + copiedCount_tDBOutput_6;
	globalMap.put("tDBOutput_6_NB_LINE_COPIED",nb_line_copied_tDBOutput_6);

			if(execStat){
				if(resourceMap.get("inIterateVComp") == null || !((Boolean)resourceMap.get("inIterateVComp"))){
			 		runStat.updateStatOnConnection("outInsert"+iterateId,2, 0); 
			 	}
			}
		
 

ok_Hash.put("tDBOutput_6", true);
end_Hash.put("tDBOutput_6", System.currentTimeMillis());




/**
 * [tDBOutput_6 end ] stop
 */






				}//end the resume

				



	
			}catch(java.lang.Exception e){	
				
				TalendException te = new TalendException(e, currentComponent, globalMap);
				
				throw te;
			}catch(java.lang.Error error){	
				
					runStat.stopThreadStat();
				
				throw error;
			}finally{
				
					     			//free memory for "tMap_1"
					     			globalMap.remove("tHash_Lookup_row5"); 
				     			
				try{
					
	
	/**
	 * [tDBInput_2 finally ] start
	 */

	

	
	
	currentComponent="tDBInput_2";

	

 



/**
 * [tDBInput_2 finally ] stop
 */

	
	/**
	 * [tMap_1 finally ] start
	 */

	

	
	
	currentComponent="tMap_1";

	

 



/**
 * [tMap_1 finally ] stop
 */

	
	/**
	 * [tDBOutput_6 finally ] start
	 */

	

	
	
	currentComponent="tDBOutput_6";

	



	
		if(resourceMap.get("finish_tDBOutput_6")==null){
			if(resourceMap.get("conn_tDBOutput_6")!=null){
				try {
					
					
					java.sql.Connection ctn_tDBOutput_6 = (java.sql.Connection)resourceMap.get("conn_tDBOutput_6");
					
					
            		
					ctn_tDBOutput_6.close();
					
				} catch (java.sql.SQLException sqlEx_tDBOutput_6) {
					String errorMessage_tDBOutput_6 = "failed to close the connection in tDBOutput_6 :" + sqlEx_tDBOutput_6.getMessage();
					
					System.err.println(errorMessage_tDBOutput_6);
				}
			}
		}
	

 



/**
 * [tDBOutput_6 finally ] stop
 */






				}catch(java.lang.Exception e){	
					//ignore
				}catch(java.lang.Error error){
					//ignore
				}
				resourceMap = null;
			}
		

		globalMap.put("tDBInput_2_SUBPROCESS_STATE", 1);
	}
	


public static class row5Struct implements routines.system.IPersistableComparableLookupRow<row5Struct> {
    final static byte[] commonByteArrayLock_GIFMIS_ledger2_12 = new byte[0];
    static byte[] commonByteArray_GIFMIS_ledger2_12 = new byte[0];
	protected static final int DEFAULT_HASHCODE = 1;
    protected static final int PRIME = 31;
    protected int hashCode = DEFAULT_HASHCODE;
    public boolean hashCodeDirty = true;

    public String loopKey;



	
			    public int ID;

				public int getID () {
					return this.ID;
				}
				
			    public int REF_ORGANIZATION;

				public int getREF_ORGANIZATION () {
					return this.REF_ORGANIZATION;
				}
				
			    public int REF_LEDGER_DOCUMENT;

				public int getREF_LEDGER_DOCUMENT () {
					return this.REF_LEDGER_DOCUMENT;
				}
				
			    public int REF_LEDGER_PERIOD;

				public int getREF_LEDGER_PERIOD () {
					return this.REF_LEDGER_PERIOD;
				}
				
			    public int REF_TRANSACTION_TYPE;

				public int getREF_TRANSACTION_TYPE () {
					return this.REF_TRANSACTION_TYPE;
				}
				
			    public int LINE_NO;

				public int getLINE_NO () {
					return this.LINE_NO;
				}
				
			    public java.util.Date TRANSACTION_DATE;

				public java.util.Date getTRANSACTION_DATE () {
					return this.TRANSACTION_DATE;
				}
				
			    public int REF_ACCOUNT;

				public int getREF_ACCOUNT () {
					return this.REF_ACCOUNT;
				}
				
			    public int REF_CURRENCY;

				public int getREF_CURRENCY () {
					return this.REF_CURRENCY;
				}
				
			    public BigDecimal CURRENCY_RATE;

				public BigDecimal getCURRENCY_RATE () {
					return this.CURRENCY_RATE;
				}
				
			    public BigDecimal AMOUNT;

				public BigDecimal getAMOUNT () {
					return this.AMOUNT;
				}
				
			    public BigDecimal AMOUNT_BASE;

				public BigDecimal getAMOUNT_BASE () {
					return this.AMOUNT_BASE;
				}
				
			    public int IS_DEBIT;

				public int getIS_DEBIT () {
					return this.IS_DEBIT;
				}
				
			    public BigDecimal QUANTITY;

				public BigDecimal getQUANTITY () {
					return this.QUANTITY;
				}
				
			    public Integer REF_PROGRAM;

				public Integer getREF_PROGRAM () {
					return this.REF_PROGRAM;
				}
				
			    public Integer REF_FUND;

				public Integer getREF_FUND () {
					return this.REF_FUND;
				}
				
			    public Integer REF_ORG_DEPARTMENT;

				public Integer getREF_ORG_DEPARTMENT () {
					return this.REF_ORG_DEPARTMENT;
				}
				
			    public Integer REF_REGION;

				public Integer getREF_REGION () {
					return this.REF_REGION;
				}
				
			    public Integer REF_FUNCTION;

				public Integer getREF_FUNCTION () {
					return this.REF_FUNCTION;
				}
				
			    public String DESCRIPTION;

				public String getDESCRIPTION () {
					return this.DESCRIPTION;
				}
				
			    public int REF_CLIENT;

				public int getREF_CLIENT () {
					return this.REF_CLIENT;
				}
				
			    public int VERSION;

				public int getVERSION () {
					return this.VERSION;
				}
				
			    public int CREATED_BY;

				public int getCREATED_BY () {
					return this.CREATED_BY;
				}
				
			    public int UPDATED_BY;

				public int getUPDATED_BY () {
					return this.UPDATED_BY;
				}
				
			    public java.util.Date CREATED_DATE;

				public java.util.Date getCREATED_DATE () {
					return this.CREATED_DATE;
				}
				
			    public java.util.Date UPDATED_DATE;

				public java.util.Date getUPDATED_DATE () {
					return this.UPDATED_DATE;
				}
				
			    public int REF_ORGANIZATION_ORIGINAL;

				public int getREF_ORGANIZATION_ORIGINAL () {
					return this.REF_ORGANIZATION_ORIGINAL;
				}
				
			    public int LINE_NO_SUB;

				public int getLINE_NO_SUB () {
					return this.LINE_NO_SUB;
				}
				
			    public int LINE_NO_QUEUE;

				public int getLINE_NO_QUEUE () {
					return this.LINE_NO_QUEUE;
				}
				
			    public String REF_ORIG_TABLE;

				public String getREF_ORIG_TABLE () {
					return this.REF_ORIG_TABLE;
				}
				
			    public int REF_ORIG_TABLE_ID;

				public int getREF_ORIG_TABLE_ID () {
					return this.REF_ORIG_TABLE_ID;
				}
				
			    public String GL_TYPE;

				public String getGL_TYPE () {
					return this.GL_TYPE;
				}
				
			    public int COMMITMENT_TYPE;

				public int getCOMMITMENT_TYPE () {
					return this.COMMITMENT_TYPE;
				}
				
			    public java.util.Date etl_date;

				public java.util.Date getEtl_date () {
					return this.etl_date;
				}
				


	@Override
	public int hashCode() {
		if (this.hashCodeDirty) {
			final int prime = PRIME;
			int result = DEFAULT_HASHCODE;
	
							result = prime * result + (int) this.ID;
						
    		this.hashCode = result;
    		this.hashCodeDirty = false;
		}
		return this.hashCode;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		final row5Struct other = (row5Struct) obj;
		
						if (this.ID != other.ID)
							return false;
					

		return true;
    }

	public void copyDataTo(row5Struct other) {

		other.ID = this.ID;
	            other.REF_ORGANIZATION = this.REF_ORGANIZATION;
	            other.REF_LEDGER_DOCUMENT = this.REF_LEDGER_DOCUMENT;
	            other.REF_LEDGER_PERIOD = this.REF_LEDGER_PERIOD;
	            other.REF_TRANSACTION_TYPE = this.REF_TRANSACTION_TYPE;
	            other.LINE_NO = this.LINE_NO;
	            other.TRANSACTION_DATE = this.TRANSACTION_DATE;
	            other.REF_ACCOUNT = this.REF_ACCOUNT;
	            other.REF_CURRENCY = this.REF_CURRENCY;
	            other.CURRENCY_RATE = this.CURRENCY_RATE;
	            other.AMOUNT = this.AMOUNT;
	            other.AMOUNT_BASE = this.AMOUNT_BASE;
	            other.IS_DEBIT = this.IS_DEBIT;
	            other.QUANTITY = this.QUANTITY;
	            other.REF_PROGRAM = this.REF_PROGRAM;
	            other.REF_FUND = this.REF_FUND;
	            other.REF_ORG_DEPARTMENT = this.REF_ORG_DEPARTMENT;
	            other.REF_REGION = this.REF_REGION;
	            other.REF_FUNCTION = this.REF_FUNCTION;
	            other.DESCRIPTION = this.DESCRIPTION;
	            other.REF_CLIENT = this.REF_CLIENT;
	            other.VERSION = this.VERSION;
	            other.CREATED_BY = this.CREATED_BY;
	            other.UPDATED_BY = this.UPDATED_BY;
	            other.CREATED_DATE = this.CREATED_DATE;
	            other.UPDATED_DATE = this.UPDATED_DATE;
	            other.REF_ORGANIZATION_ORIGINAL = this.REF_ORGANIZATION_ORIGINAL;
	            other.LINE_NO_SUB = this.LINE_NO_SUB;
	            other.LINE_NO_QUEUE = this.LINE_NO_QUEUE;
	            other.REF_ORIG_TABLE = this.REF_ORIG_TABLE;
	            other.REF_ORIG_TABLE_ID = this.REF_ORIG_TABLE_ID;
	            other.GL_TYPE = this.GL_TYPE;
	            other.COMMITMENT_TYPE = this.COMMITMENT_TYPE;
	            other.etl_date = this.etl_date;
	            
	}

	public void copyKeysDataTo(row5Struct other) {

		other.ID = this.ID;
	            	
	}




	private java.util.Date readDate(DataInputStream dis, ObjectInputStream ois) throws IOException{
		java.util.Date dateReturn = null;
		int length = 0;
        length = dis.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(dis.readLong());
		}
		return dateReturn;
	}

	private void writeDate(java.util.Date date1, DataOutputStream dos, ObjectOutputStream oos) throws IOException{
		if(date1 == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeLong(date1.getTime());
    	}
	}
	private Integer readInteger(DataInputStream dis, ObjectInputStream ois) throws IOException{
		Integer intReturn;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			intReturn = null;
		} else {
			intReturn = dis.readInt();
		}
		return intReturn;
	}

	private void writeInteger(Integer intNum, DataOutputStream dos, ObjectOutputStream oos) throws IOException{
		if(intNum == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeInt(intNum);
    	}
	}

	private String readString(DataInputStream dis, ObjectInputStream ois) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			byte[] byteArray = new byte[length];
			dis.read(byteArray);
			strReturn = new String(byteArray, utf8Charset);
		}
		return strReturn;
	}

	private void writeString(String str, DataOutputStream dos, ObjectOutputStream oos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
	}

    public void readKeysData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_GIFMIS_ledger2_12) {

        	try {

        		int length = 0;
		
			        this.ID = dis.readInt();
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeKeysData(ObjectOutputStream dos) {
        try {

		
					// int
				
		            	dos.writeInt(this.ID);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }



    /**
     * Fill Values data by reading ObjectInputStream.
     */
    public void readValuesData(DataInputStream dis, ObjectInputStream ois) {
        try {

			int length = 0;
		
			            this.REF_ORGANIZATION = dis.readInt();
					
			            this.REF_LEDGER_DOCUMENT = dis.readInt();
					
			            this.REF_LEDGER_PERIOD = dis.readInt();
					
			            this.REF_TRANSACTION_TYPE = dis.readInt();
					
			            this.LINE_NO = dis.readInt();
					
						this.TRANSACTION_DATE = readDate(dis,ois);
					
			            this.REF_ACCOUNT = dis.readInt();
					
			            this.REF_CURRENCY = dis.readInt();
					
       			    	this.CURRENCY_RATE = (BigDecimal) ois.readObject();
					
       			    	this.AMOUNT = (BigDecimal) ois.readObject();
					
       			    	this.AMOUNT_BASE = (BigDecimal) ois.readObject();
					
			            this.IS_DEBIT = dis.readInt();
					
       			    	this.QUANTITY = (BigDecimal) ois.readObject();
					
						this.REF_PROGRAM = readInteger(dis,ois);
					
						this.REF_FUND = readInteger(dis,ois);
					
						this.REF_ORG_DEPARTMENT = readInteger(dis,ois);
					
						this.REF_REGION = readInteger(dis,ois);
					
						this.REF_FUNCTION = readInteger(dis,ois);
					
						this.DESCRIPTION = readString(dis,ois);
					
			            this.REF_CLIENT = dis.readInt();
					
			            this.VERSION = dis.readInt();
					
			            this.CREATED_BY = dis.readInt();
					
			            this.UPDATED_BY = dis.readInt();
					
						this.CREATED_DATE = readDate(dis,ois);
					
						this.UPDATED_DATE = readDate(dis,ois);
					
			            this.REF_ORGANIZATION_ORIGINAL = dis.readInt();
					
			            this.LINE_NO_SUB = dis.readInt();
					
			            this.LINE_NO_QUEUE = dis.readInt();
					
						this.REF_ORIG_TABLE = readString(dis,ois);
					
			            this.REF_ORIG_TABLE_ID = dis.readInt();
					
						this.GL_TYPE = readString(dis,ois);
					
			            this.COMMITMENT_TYPE = dis.readInt();
					
						this.etl_date = readDate(dis,ois);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		
			} catch(ClassNotFoundException eCNFE) {
				 throw new RuntimeException(eCNFE);
		

        }

		

    }

    /**
     * Return a byte array which represents Values data.
     */
    public void writeValuesData(DataOutputStream dos, ObjectOutputStream oos) {
        try {

		
		            	dos.writeInt(this.REF_ORGANIZATION);
					
		            	dos.writeInt(this.REF_LEDGER_DOCUMENT);
					
		            	dos.writeInt(this.REF_LEDGER_PERIOD);
					
		            	dos.writeInt(this.REF_TRANSACTION_TYPE);
					
		            	dos.writeInt(this.LINE_NO);
					
						writeDate(this.TRANSACTION_DATE, dos, oos);
					
		            	dos.writeInt(this.REF_ACCOUNT);
					
		            	dos.writeInt(this.REF_CURRENCY);
					
       			    	oos.writeObject(this.CURRENCY_RATE);
					
       			    	oos.writeObject(this.AMOUNT);
					
       			    	oos.writeObject(this.AMOUNT_BASE);
					
		            	dos.writeInt(this.IS_DEBIT);
					
       			    	oos.writeObject(this.QUANTITY);
					
					writeInteger(this.REF_PROGRAM, dos, oos);
					
					writeInteger(this.REF_FUND, dos, oos);
					
					writeInteger(this.REF_ORG_DEPARTMENT, dos, oos);
					
					writeInteger(this.REF_REGION, dos, oos);
					
					writeInteger(this.REF_FUNCTION, dos, oos);
					
						writeString(this.DESCRIPTION, dos, oos);
					
		            	dos.writeInt(this.REF_CLIENT);
					
		            	dos.writeInt(this.VERSION);
					
		            	dos.writeInt(this.CREATED_BY);
					
		            	dos.writeInt(this.UPDATED_BY);
					
						writeDate(this.CREATED_DATE, dos, oos);
					
						writeDate(this.UPDATED_DATE, dos, oos);
					
		            	dos.writeInt(this.REF_ORGANIZATION_ORIGINAL);
					
		            	dos.writeInt(this.LINE_NO_SUB);
					
		            	dos.writeInt(this.LINE_NO_QUEUE);
					
						writeString(this.REF_ORIG_TABLE, dos, oos);
					
		            	dos.writeInt(this.REF_ORIG_TABLE_ID);
					
						writeString(this.GL_TYPE, dos, oos);
					
		            	dos.writeInt(this.COMMITMENT_TYPE);
					
						writeDate(this.etl_date, dos, oos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        	}

    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("ID="+String.valueOf(ID));
		sb.append(",REF_ORGANIZATION="+String.valueOf(REF_ORGANIZATION));
		sb.append(",REF_LEDGER_DOCUMENT="+String.valueOf(REF_LEDGER_DOCUMENT));
		sb.append(",REF_LEDGER_PERIOD="+String.valueOf(REF_LEDGER_PERIOD));
		sb.append(",REF_TRANSACTION_TYPE="+String.valueOf(REF_TRANSACTION_TYPE));
		sb.append(",LINE_NO="+String.valueOf(LINE_NO));
		sb.append(",TRANSACTION_DATE="+String.valueOf(TRANSACTION_DATE));
		sb.append(",REF_ACCOUNT="+String.valueOf(REF_ACCOUNT));
		sb.append(",REF_CURRENCY="+String.valueOf(REF_CURRENCY));
		sb.append(",CURRENCY_RATE="+String.valueOf(CURRENCY_RATE));
		sb.append(",AMOUNT="+String.valueOf(AMOUNT));
		sb.append(",AMOUNT_BASE="+String.valueOf(AMOUNT_BASE));
		sb.append(",IS_DEBIT="+String.valueOf(IS_DEBIT));
		sb.append(",QUANTITY="+String.valueOf(QUANTITY));
		sb.append(",REF_PROGRAM="+String.valueOf(REF_PROGRAM));
		sb.append(",REF_FUND="+String.valueOf(REF_FUND));
		sb.append(",REF_ORG_DEPARTMENT="+String.valueOf(REF_ORG_DEPARTMENT));
		sb.append(",REF_REGION="+String.valueOf(REF_REGION));
		sb.append(",REF_FUNCTION="+String.valueOf(REF_FUNCTION));
		sb.append(",DESCRIPTION="+DESCRIPTION);
		sb.append(",REF_CLIENT="+String.valueOf(REF_CLIENT));
		sb.append(",VERSION="+String.valueOf(VERSION));
		sb.append(",CREATED_BY="+String.valueOf(CREATED_BY));
		sb.append(",UPDATED_BY="+String.valueOf(UPDATED_BY));
		sb.append(",CREATED_DATE="+String.valueOf(CREATED_DATE));
		sb.append(",UPDATED_DATE="+String.valueOf(UPDATED_DATE));
		sb.append(",REF_ORGANIZATION_ORIGINAL="+String.valueOf(REF_ORGANIZATION_ORIGINAL));
		sb.append(",LINE_NO_SUB="+String.valueOf(LINE_NO_SUB));
		sb.append(",LINE_NO_QUEUE="+String.valueOf(LINE_NO_QUEUE));
		sb.append(",REF_ORIG_TABLE="+REF_ORIG_TABLE);
		sb.append(",REF_ORIG_TABLE_ID="+String.valueOf(REF_ORIG_TABLE_ID));
		sb.append(",GL_TYPE="+GL_TYPE);
		sb.append(",COMMITMENT_TYPE="+String.valueOf(COMMITMENT_TYPE));
		sb.append(",etl_date="+String.valueOf(etl_date));
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(row5Struct other) {

		int returnValue = -1;
		
						returnValue = checkNullsAndCompare(this.ID, other.ID);
						if(returnValue != 0) {
							return returnValue;
						}

					
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}
public void tDBInput_7Process(final java.util.Map<String, Object> globalMap) throws TalendException {
	globalMap.put("tDBInput_7_SUBPROCESS_STATE", 0);

 final boolean execStat = this.execStat;
	
		String iterateId = "";
	
	
	String currentComponent = "";
	java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

	try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception().getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { //start the resume
				globalResumeTicket = true;



		row5Struct row5 = new row5Struct();




	
	/**
	 * [tAdvancedHash_row5 begin ] start
	 */

	

	
		
		ok_Hash.put("tAdvancedHash_row5", false);
		start_Hash.put("tAdvancedHash_row5", System.currentTimeMillis());
		
	
	currentComponent="tAdvancedHash_row5";

	
			if (execStat) {
				if(resourceMap.get("inIterateVComp") == null){
					
						runStat.updateStatOnConnection("row5" + iterateId, 0, 0);
					
				}
			} 

		
		int tos_count_tAdvancedHash_row5 = 0;
		
    	class BytesLimit65535_tAdvancedHash_row5{
    		public void limitLog4jByte() throws Exception{
    			
    		}
    	}
    	
        new BytesLimit65535_tAdvancedHash_row5().limitLog4jByte();

			   		// connection name:row5
			   		// source node:tDBInput_7 - inputs:(after_tDBInput_2) outputs:(row5,row5) | target node:tAdvancedHash_row5 - inputs:(row5) outputs:()
			   		// linked node: tMap_1 - inputs:(row3,row5) outputs:(outInsert)
			   
			   		org.talend.designer.components.lookup.common.ICommonLookup.MATCHING_MODE matchingModeEnum_row5 = 
			   			org.talend.designer.components.lookup.common.ICommonLookup.MATCHING_MODE.UNIQUE_MATCH;
			   			
			   
	   			org.talend.designer.components.lookup.memory.AdvancedMemoryLookup<row5Struct> tHash_Lookup_row5 =org.talend.designer.components.lookup.memory.AdvancedMemoryLookup.
	   						<row5Struct>getLookup(matchingModeEnum_row5);
	   						   
		   	   	   globalMap.put("tHash_Lookup_row5", tHash_Lookup_row5);
		   	   	   
				
           

 



/**
 * [tAdvancedHash_row5 begin ] stop
 */



	
	/**
	 * [tDBInput_7 begin ] start
	 */

	

	
		
		ok_Hash.put("tDBInput_7", false);
		start_Hash.put("tDBInput_7", System.currentTimeMillis());
		
	
	currentComponent="tDBInput_7";

	
		int tos_count_tDBInput_7 = 0;
		
    	class BytesLimit65535_tDBInput_7{
    		public void limitLog4jByte() throws Exception{
    			
    		}
    	}
    	
        new BytesLimit65535_tDBInput_7().limitLog4jByte();
	
    
	
		    int nb_line_tDBInput_7 = 0;
		    java.sql.Connection conn_tDBInput_7 = null;
				String driverClass_tDBInput_7 = "com.vertica.jdbc.Driver";
			    java.lang.Class.forName(driverClass_tDBInput_7);
			   	String dbUser_tDBInput_7 = context.Vertica_ODS_Login;
			   	
        		
        		
        		
	final String decryptedPassword_tDBInput_7 = context.Vertica_ODS_Password; 
			   	
		        String dbPwd_tDBInput_7 = decryptedPassword_tDBInput_7;
		        
				
				String url_tDBInput_7 = "jdbc:vertica://" + context.Vertica_ODS_Server + ":" + context.Vertica_ODS_Port + "/" + context.Vertica_ODS_Database + "?" + context.Vertica_ODS_AdditionalParams;
				
				conn_tDBInput_7 = java.sql.DriverManager.getConnection(url_tDBInput_7,dbUser_tDBInput_7,dbPwd_tDBInput_7);
		        
		    
			java.sql.Statement stmt_tDBInput_7 = conn_tDBInput_7.createStatement();

		    String dbquery_tDBInput_7 = "select * from ODS."+context.tableName +" where EXTRACT(YEAR FROM transaction_date)=2017  and EXTRACT(month FROM transaction_date)=12";
			

            	globalMap.put("tDBInput_7_QUERY",dbquery_tDBInput_7);
		    java.sql.ResultSet rs_tDBInput_7 = null;

		    try {
		    	rs_tDBInput_7 = stmt_tDBInput_7.executeQuery(dbquery_tDBInput_7);
		    	java.sql.ResultSetMetaData rsmd_tDBInput_7 = rs_tDBInput_7.getMetaData();
		    	int colQtyInRs_tDBInput_7 = rsmd_tDBInput_7.getColumnCount();

		    String tmpContent_tDBInput_7 = null;
		    
		    
		    while (rs_tDBInput_7.next()) {
		        nb_line_tDBInput_7++;
		        
							if(colQtyInRs_tDBInput_7 < 1) {
								row5.ID = 0;
							} else {
		                          
            if(rs_tDBInput_7.getObject(1) != null) {
                row5.ID = rs_tDBInput_7.getInt(1);
            } else {
                    throw new RuntimeException("Null value in non-Nullable column");
            }
		                    }
							if(colQtyInRs_tDBInput_7 < 2) {
								row5.REF_ORGANIZATION = 0;
							} else {
		                          
            if(rs_tDBInput_7.getObject(2) != null) {
                row5.REF_ORGANIZATION = rs_tDBInput_7.getInt(2);
            } else {
                    throw new RuntimeException("Null value in non-Nullable column");
            }
		                    }
							if(colQtyInRs_tDBInput_7 < 3) {
								row5.REF_LEDGER_DOCUMENT = 0;
							} else {
		                          
            if(rs_tDBInput_7.getObject(3) != null) {
                row5.REF_LEDGER_DOCUMENT = rs_tDBInput_7.getInt(3);
            } else {
                    throw new RuntimeException("Null value in non-Nullable column");
            }
		                    }
							if(colQtyInRs_tDBInput_7 < 4) {
								row5.REF_LEDGER_PERIOD = 0;
							} else {
		                          
            if(rs_tDBInput_7.getObject(4) != null) {
                row5.REF_LEDGER_PERIOD = rs_tDBInput_7.getInt(4);
            } else {
                    throw new RuntimeException("Null value in non-Nullable column");
            }
		                    }
							if(colQtyInRs_tDBInput_7 < 5) {
								row5.REF_TRANSACTION_TYPE = 0;
							} else {
		                          
            if(rs_tDBInput_7.getObject(5) != null) {
                row5.REF_TRANSACTION_TYPE = rs_tDBInput_7.getInt(5);
            } else {
                    throw new RuntimeException("Null value in non-Nullable column");
            }
		                    }
							if(colQtyInRs_tDBInput_7 < 6) {
								row5.LINE_NO = 0;
							} else {
		                          
            if(rs_tDBInput_7.getObject(6) != null) {
                row5.LINE_NO = rs_tDBInput_7.getInt(6);
            } else {
                    throw new RuntimeException("Null value in non-Nullable column");
            }
		                    }
							if(colQtyInRs_tDBInput_7 < 7) {
								row5.TRANSACTION_DATE = null;
							} else {
										
            if(rs_tDBInput_7.getObject(7) != null) {
                row5.TRANSACTION_DATE = rs_tDBInput_7.getDate(7);
            } else {
                    throw new RuntimeException("Null value in non-Nullable column");
            }
		                    }
							if(colQtyInRs_tDBInput_7 < 8) {
								row5.REF_ACCOUNT = 0;
							} else {
		                          
            if(rs_tDBInput_7.getObject(8) != null) {
                row5.REF_ACCOUNT = rs_tDBInput_7.getInt(8);
            } else {
                    throw new RuntimeException("Null value in non-Nullable column");
            }
		                    }
							if(colQtyInRs_tDBInput_7 < 9) {
								row5.REF_CURRENCY = 0;
							} else {
		                          
            if(rs_tDBInput_7.getObject(9) != null) {
                row5.REF_CURRENCY = rs_tDBInput_7.getInt(9);
            } else {
                    throw new RuntimeException("Null value in non-Nullable column");
            }
		                    }
							if(colQtyInRs_tDBInput_7 < 10) {
								row5.CURRENCY_RATE = null;
							} else {
		                          
            if(rs_tDBInput_7.getObject(10) != null) {
                row5.CURRENCY_RATE = rs_tDBInput_7.getBigDecimal(10);
            } else {
                    throw new RuntimeException("Null value in non-Nullable column");
            }
		                    }
							if(colQtyInRs_tDBInput_7 < 11) {
								row5.AMOUNT = null;
							} else {
		                          
            if(rs_tDBInput_7.getObject(11) != null) {
                row5.AMOUNT = rs_tDBInput_7.getBigDecimal(11);
            } else {
                    throw new RuntimeException("Null value in non-Nullable column");
            }
		                    }
							if(colQtyInRs_tDBInput_7 < 12) {
								row5.AMOUNT_BASE = null;
							} else {
		                          
            if(rs_tDBInput_7.getObject(12) != null) {
                row5.AMOUNT_BASE = rs_tDBInput_7.getBigDecimal(12);
            } else {
                    throw new RuntimeException("Null value in non-Nullable column");
            }
		                    }
							if(colQtyInRs_tDBInput_7 < 13) {
								row5.IS_DEBIT = 0;
							} else {
		                          
            if(rs_tDBInput_7.getObject(13) != null) {
                row5.IS_DEBIT = rs_tDBInput_7.getInt(13);
            } else {
                    throw new RuntimeException("Null value in non-Nullable column");
            }
		                    }
							if(colQtyInRs_tDBInput_7 < 14) {
								row5.QUANTITY = null;
							} else {
		                          
            if(rs_tDBInput_7.getObject(14) != null) {
                row5.QUANTITY = rs_tDBInput_7.getBigDecimal(14);
            } else {
                    throw new RuntimeException("Null value in non-Nullable column");
            }
		                    }
							if(colQtyInRs_tDBInput_7 < 15) {
								row5.REF_PROGRAM = null;
							} else {
		                          
            if(rs_tDBInput_7.getObject(15) != null) {
                row5.REF_PROGRAM = rs_tDBInput_7.getInt(15);
            } else {
                    row5.REF_PROGRAM = null;
            }
		                    }
							if(colQtyInRs_tDBInput_7 < 16) {
								row5.REF_FUND = null;
							} else {
		                          
            if(rs_tDBInput_7.getObject(16) != null) {
                row5.REF_FUND = rs_tDBInput_7.getInt(16);
            } else {
                    row5.REF_FUND = null;
            }
		                    }
							if(colQtyInRs_tDBInput_7 < 17) {
								row5.REF_ORG_DEPARTMENT = null;
							} else {
		                          
            if(rs_tDBInput_7.getObject(17) != null) {
                row5.REF_ORG_DEPARTMENT = rs_tDBInput_7.getInt(17);
            } else {
                    row5.REF_ORG_DEPARTMENT = null;
            }
		                    }
							if(colQtyInRs_tDBInput_7 < 18) {
								row5.REF_REGION = null;
							} else {
		                          
            if(rs_tDBInput_7.getObject(18) != null) {
                row5.REF_REGION = rs_tDBInput_7.getInt(18);
            } else {
                    row5.REF_REGION = null;
            }
		                    }
							if(colQtyInRs_tDBInput_7 < 19) {
								row5.REF_FUNCTION = null;
							} else {
		                          
            if(rs_tDBInput_7.getObject(19) != null) {
                row5.REF_FUNCTION = rs_tDBInput_7.getInt(19);
            } else {
                    row5.REF_FUNCTION = null;
            }
		                    }
							if(colQtyInRs_tDBInput_7 < 20) {
								row5.DESCRIPTION = null;
							} else {
	                         		
        	row5.DESCRIPTION = routines.system.JDBCUtil.getString(rs_tDBInput_7, 20, false);
		                    }
							if(colQtyInRs_tDBInput_7 < 21) {
								row5.REF_CLIENT = 0;
							} else {
		                          
            if(rs_tDBInput_7.getObject(21) != null) {
                row5.REF_CLIENT = rs_tDBInput_7.getInt(21);
            } else {
                    throw new RuntimeException("Null value in non-Nullable column");
            }
		                    }
							if(colQtyInRs_tDBInput_7 < 22) {
								row5.VERSION = 0;
							} else {
		                          
            if(rs_tDBInput_7.getObject(22) != null) {
                row5.VERSION = rs_tDBInput_7.getInt(22);
            } else {
                    throw new RuntimeException("Null value in non-Nullable column");
            }
		                    }
							if(colQtyInRs_tDBInput_7 < 23) {
								row5.CREATED_BY = 0;
							} else {
		                          
            if(rs_tDBInput_7.getObject(23) != null) {
                row5.CREATED_BY = rs_tDBInput_7.getInt(23);
            } else {
                    throw new RuntimeException("Null value in non-Nullable column");
            }
		                    }
							if(colQtyInRs_tDBInput_7 < 24) {
								row5.UPDATED_BY = 0;
							} else {
		                          
            if(rs_tDBInput_7.getObject(24) != null) {
                row5.UPDATED_BY = rs_tDBInput_7.getInt(24);
            } else {
                    throw new RuntimeException("Null value in non-Nullable column");
            }
		                    }
							if(colQtyInRs_tDBInput_7 < 25) {
								row5.CREATED_DATE = null;
							} else {
										
            if(rs_tDBInput_7.getObject(25) != null) {
                row5.CREATED_DATE = rs_tDBInput_7.getDate(25);
            } else {
                    throw new RuntimeException("Null value in non-Nullable column");
            }
		                    }
							if(colQtyInRs_tDBInput_7 < 26) {
								row5.UPDATED_DATE = null;
							} else {
										
            if(rs_tDBInput_7.getObject(26) != null) {
                row5.UPDATED_DATE = rs_tDBInput_7.getDate(26);
            } else {
                    throw new RuntimeException("Null value in non-Nullable column");
            }
		                    }
							if(colQtyInRs_tDBInput_7 < 27) {
								row5.REF_ORGANIZATION_ORIGINAL = 0;
							} else {
		                          
            if(rs_tDBInput_7.getObject(27) != null) {
                row5.REF_ORGANIZATION_ORIGINAL = rs_tDBInput_7.getInt(27);
            } else {
                    throw new RuntimeException("Null value in non-Nullable column");
            }
		                    }
							if(colQtyInRs_tDBInput_7 < 28) {
								row5.LINE_NO_SUB = 0;
							} else {
		                          
            if(rs_tDBInput_7.getObject(28) != null) {
                row5.LINE_NO_SUB = rs_tDBInput_7.getInt(28);
            } else {
                    throw new RuntimeException("Null value in non-Nullable column");
            }
		                    }
							if(colQtyInRs_tDBInput_7 < 29) {
								row5.LINE_NO_QUEUE = 0;
							} else {
		                          
            if(rs_tDBInput_7.getObject(29) != null) {
                row5.LINE_NO_QUEUE = rs_tDBInput_7.getInt(29);
            } else {
                    throw new RuntimeException("Null value in non-Nullable column");
            }
		                    }
							if(colQtyInRs_tDBInput_7 < 30) {
								row5.REF_ORIG_TABLE = null;
							} else {
	                         		
        	row5.REF_ORIG_TABLE = routines.system.JDBCUtil.getString(rs_tDBInput_7, 30, false);
		                    }
							if(colQtyInRs_tDBInput_7 < 31) {
								row5.REF_ORIG_TABLE_ID = 0;
							} else {
		                          
            if(rs_tDBInput_7.getObject(31) != null) {
                row5.REF_ORIG_TABLE_ID = rs_tDBInput_7.getInt(31);
            } else {
                    throw new RuntimeException("Null value in non-Nullable column");
            }
		                    }
							if(colQtyInRs_tDBInput_7 < 32) {
								row5.GL_TYPE = null;
							} else {
	                         		
        	row5.GL_TYPE = routines.system.JDBCUtil.getString(rs_tDBInput_7, 32, false);
		                    }
							if(colQtyInRs_tDBInput_7 < 33) {
								row5.COMMITMENT_TYPE = 0;
							} else {
		                          
            if(rs_tDBInput_7.getObject(33) != null) {
                row5.COMMITMENT_TYPE = rs_tDBInput_7.getInt(33);
            } else {
                    throw new RuntimeException("Null value in non-Nullable column");
            }
		                    }
							if(colQtyInRs_tDBInput_7 < 34) {
								row5.etl_date = null;
							} else {
										
			row5.etl_date = routines.system.JDBCUtil.getDate(rs_tDBInput_7, 34);
		                    }
					


 



/**
 * [tDBInput_7 begin ] stop
 */
	
	/**
	 * [tDBInput_7 main ] start
	 */

	

	
	
	currentComponent="tDBInput_7";

	

 


	tos_count_tDBInput_7++;

/**
 * [tDBInput_7 main ] stop
 */
	
	/**
	 * [tDBInput_7 process_data_begin ] start
	 */

	

	
	
	currentComponent="tDBInput_7";

	

 



/**
 * [tDBInput_7 process_data_begin ] stop
 */

	
	/**
	 * [tAdvancedHash_row5 main ] start
	 */

	

	
	
	currentComponent="tAdvancedHash_row5";

	

			//row5
			//row5


			
				if(execStat){
					runStat.updateStatOnConnection("row5"+iterateId,1, 1);
				} 
			

		


			   
			   

					row5Struct row5_HashRow = new row5Struct();
		   	   	   
				
				row5_HashRow.ID = row5.ID;
				
				row5_HashRow.REF_ORGANIZATION = row5.REF_ORGANIZATION;
				
				row5_HashRow.REF_LEDGER_DOCUMENT = row5.REF_LEDGER_DOCUMENT;
				
				row5_HashRow.REF_LEDGER_PERIOD = row5.REF_LEDGER_PERIOD;
				
				row5_HashRow.REF_TRANSACTION_TYPE = row5.REF_TRANSACTION_TYPE;
				
				row5_HashRow.LINE_NO = row5.LINE_NO;
				
				row5_HashRow.TRANSACTION_DATE = row5.TRANSACTION_DATE;
				
				row5_HashRow.REF_ACCOUNT = row5.REF_ACCOUNT;
				
				row5_HashRow.REF_CURRENCY = row5.REF_CURRENCY;
				
				row5_HashRow.CURRENCY_RATE = row5.CURRENCY_RATE;
				
				row5_HashRow.AMOUNT = row5.AMOUNT;
				
				row5_HashRow.AMOUNT_BASE = row5.AMOUNT_BASE;
				
				row5_HashRow.IS_DEBIT = row5.IS_DEBIT;
				
				row5_HashRow.QUANTITY = row5.QUANTITY;
				
				row5_HashRow.REF_PROGRAM = row5.REF_PROGRAM;
				
				row5_HashRow.REF_FUND = row5.REF_FUND;
				
				row5_HashRow.REF_ORG_DEPARTMENT = row5.REF_ORG_DEPARTMENT;
				
				row5_HashRow.REF_REGION = row5.REF_REGION;
				
				row5_HashRow.REF_FUNCTION = row5.REF_FUNCTION;
				
				row5_HashRow.DESCRIPTION = row5.DESCRIPTION;
				
				row5_HashRow.REF_CLIENT = row5.REF_CLIENT;
				
				row5_HashRow.VERSION = row5.VERSION;
				
				row5_HashRow.CREATED_BY = row5.CREATED_BY;
				
				row5_HashRow.UPDATED_BY = row5.UPDATED_BY;
				
				row5_HashRow.CREATED_DATE = row5.CREATED_DATE;
				
				row5_HashRow.UPDATED_DATE = row5.UPDATED_DATE;
				
				row5_HashRow.REF_ORGANIZATION_ORIGINAL = row5.REF_ORGANIZATION_ORIGINAL;
				
				row5_HashRow.LINE_NO_SUB = row5.LINE_NO_SUB;
				
				row5_HashRow.LINE_NO_QUEUE = row5.LINE_NO_QUEUE;
				
				row5_HashRow.REF_ORIG_TABLE = row5.REF_ORIG_TABLE;
				
				row5_HashRow.REF_ORIG_TABLE_ID = row5.REF_ORIG_TABLE_ID;
				
				row5_HashRow.GL_TYPE = row5.GL_TYPE;
				
				row5_HashRow.COMMITMENT_TYPE = row5.COMMITMENT_TYPE;
				
				row5_HashRow.etl_date = row5.etl_date;
				
			tHash_Lookup_row5.put(row5_HashRow);
			
            




 


	tos_count_tAdvancedHash_row5++;

/**
 * [tAdvancedHash_row5 main ] stop
 */
	
	/**
	 * [tAdvancedHash_row5 process_data_begin ] start
	 */

	

	
	
	currentComponent="tAdvancedHash_row5";

	

 



/**
 * [tAdvancedHash_row5 process_data_begin ] stop
 */
	
	/**
	 * [tAdvancedHash_row5 process_data_end ] start
	 */

	

	
	
	currentComponent="tAdvancedHash_row5";

	

 



/**
 * [tAdvancedHash_row5 process_data_end ] stop
 */



	
	/**
	 * [tDBInput_7 process_data_end ] start
	 */

	

	
	
	currentComponent="tDBInput_7";

	

 



/**
 * [tDBInput_7 process_data_end ] stop
 */
	
	/**
	 * [tDBInput_7 end ] start
	 */

	

	
	
	currentComponent="tDBInput_7";

	

	}
}finally{
	stmt_tDBInput_7.close();

		if(conn_tDBInput_7 != null && !conn_tDBInput_7.isClosed()) {
			
			conn_tDBInput_7.close();
			
		}
}
globalMap.put("tDBInput_7_NB_LINE",nb_line_tDBInput_7);

 

ok_Hash.put("tDBInput_7", true);
end_Hash.put("tDBInput_7", System.currentTimeMillis());




/**
 * [tDBInput_7 end ] stop
 */

	
	/**
	 * [tAdvancedHash_row5 end ] start
	 */

	

	
	
	currentComponent="tAdvancedHash_row5";

	

tHash_Lookup_row5.endPut();

			if(execStat){
				if(resourceMap.get("inIterateVComp") == null || !((Boolean)resourceMap.get("inIterateVComp"))){
			 		runStat.updateStatOnConnection("row5"+iterateId,2, 0); 
			 	}
			}
		
 

ok_Hash.put("tAdvancedHash_row5", true);
end_Hash.put("tAdvancedHash_row5", System.currentTimeMillis());




/**
 * [tAdvancedHash_row5 end ] stop
 */



				}//end the resume

				



	
			}catch(java.lang.Exception e){	
				
				TalendException te = new TalendException(e, currentComponent, globalMap);
				
				throw te;
			}catch(java.lang.Error error){	
				
					runStat.stopThreadStat();
				
				throw error;
			}finally{
				
				try{
					
	
	/**
	 * [tDBInput_7 finally ] start
	 */

	

	
	
	currentComponent="tDBInput_7";

	

 



/**
 * [tDBInput_7 finally ] stop
 */

	
	/**
	 * [tAdvancedHash_row5 finally ] start
	 */

	

	
	
	currentComponent="tAdvancedHash_row5";

	

 



/**
 * [tAdvancedHash_row5 finally ] stop
 */



				}catch(java.lang.Exception e){	
					//ignore
				}catch(java.lang.Error error){
					//ignore
				}
				resourceMap = null;
			}
		

		globalMap.put("tDBInput_7_SUBPROCESS_STATE", 1);
	}
	

public void connectionStatsLogs_CommitProcess(final java.util.Map<String, Object> globalMap) throws TalendException {
	globalMap.put("connectionStatsLogs_Commit_SUBPROCESS_STATE", 0);

 final boolean execStat = this.execStat;
	
		String iterateId = "";
	
	
	String currentComponent = "";
	java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

	try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception().getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { //start the resume
				globalResumeTicket = true;





	
	/**
	 * [connectionStatsLogs_Commit begin ] start
	 */

	

	
		
		ok_Hash.put("connectionStatsLogs_Commit", false);
		start_Hash.put("connectionStatsLogs_Commit", System.currentTimeMillis());
		
	
	currentComponent="connectionStatsLogs_Commit";

	
		int tos_count_connectionStatsLogs_Commit = 0;
		
    	class BytesLimit65535_connectionStatsLogs_Commit{
    		public void limitLog4jByte() throws Exception{
    			
    		}
    	}
    	
        new BytesLimit65535_connectionStatsLogs_Commit().limitLog4jByte();

 



/**
 * [connectionStatsLogs_Commit begin ] stop
 */
	
	/**
	 * [connectionStatsLogs_Commit main ] start
	 */

	

	
	
	currentComponent="connectionStatsLogs_Commit";

	

	java.sql.Connection conn_connectionStatsLogs_Commit = (java.sql.Connection)globalMap.get("conn_connectionStatsLogs");

if(conn_connectionStatsLogs_Commit != null && !conn_connectionStatsLogs_Commit.isClosed()) {
	
			
			conn_connectionStatsLogs_Commit.commit();
			
	
}

 


	tos_count_connectionStatsLogs_Commit++;

/**
 * [connectionStatsLogs_Commit main ] stop
 */
	
	/**
	 * [connectionStatsLogs_Commit process_data_begin ] start
	 */

	

	
	
	currentComponent="connectionStatsLogs_Commit";

	

 



/**
 * [connectionStatsLogs_Commit process_data_begin ] stop
 */
	
	/**
	 * [connectionStatsLogs_Commit process_data_end ] start
	 */

	

	
	
	currentComponent="connectionStatsLogs_Commit";

	

 



/**
 * [connectionStatsLogs_Commit process_data_end ] stop
 */
	
	/**
	 * [connectionStatsLogs_Commit end ] start
	 */

	

	
	
	currentComponent="connectionStatsLogs_Commit";

	

 

ok_Hash.put("connectionStatsLogs_Commit", true);
end_Hash.put("connectionStatsLogs_Commit", System.currentTimeMillis());




/**
 * [connectionStatsLogs_Commit end ] stop
 */
				}//end the resume

				



	
			}catch(java.lang.Exception e){	
				
				TalendException te = new TalendException(e, currentComponent, globalMap);
				
				throw te;
			}catch(java.lang.Error error){	
				
					runStat.stopThreadStat();
				
				throw error;
			}finally{
				
				try{
					
	
	/**
	 * [connectionStatsLogs_Commit finally ] start
	 */

	

	
	
	currentComponent="connectionStatsLogs_Commit";

	

 



/**
 * [connectionStatsLogs_Commit finally ] stop
 */
				}catch(java.lang.Exception e){	
					//ignore
				}catch(java.lang.Error error){
					//ignore
				}
				resourceMap = null;
			}
		

		globalMap.put("connectionStatsLogs_Commit_SUBPROCESS_STATE", 1);
	}
	

public void connectionStatsLogsProcess(final java.util.Map<String, Object> globalMap) throws TalendException {
	globalMap.put("connectionStatsLogs_SUBPROCESS_STATE", 0);

 final boolean execStat = this.execStat;
	
		String iterateId = "";
	
	
	String currentComponent = "";
	java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

	try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception().getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { //start the resume
				globalResumeTicket = true;





	
	/**
	 * [connectionStatsLogs begin ] start
	 */

	

	
		
		ok_Hash.put("connectionStatsLogs", false);
		start_Hash.put("connectionStatsLogs", System.currentTimeMillis());
		
	
	currentComponent="connectionStatsLogs";

	
		int tos_count_connectionStatsLogs = 0;
		
    	class BytesLimit65535_connectionStatsLogs{
    		public void limitLog4jByte() throws Exception{
    			
    		}
    	}
    	
        new BytesLimit65535_connectionStatsLogs().limitLog4jByte();
	

	
		String properties_connectionStatsLogs = "noDatetimeStringSync=true";
		if (properties_connectionStatsLogs == null || properties_connectionStatsLogs.trim().length() == 0) {
			properties_connectionStatsLogs += "rewriteBatchedStatements=true";
		}else if (properties_connectionStatsLogs != null && !properties_connectionStatsLogs.contains("rewriteBatchedStatements")) {
			properties_connectionStatsLogs += "&rewriteBatchedStatements=true";
		}
		
		String url_connectionStatsLogs = "jdbc:mysql://" + "localhost" + ":" + "3306" + "/" + "amc" + "?" + properties_connectionStatsLogs;

	String dbUser_connectionStatsLogs = "etluser";
	
	
		 
	final String decryptedPassword_connectionStatsLogs = routines.system.PasswordEncryptUtil.decryptPassword("72e87cf3a913e55bf4f7aba1746784ea");
		String dbPwd_connectionStatsLogs = decryptedPassword_connectionStatsLogs;
	

	java.sql.Connection conn_connectionStatsLogs = null;
	
	
			String sharedConnectionName_connectionStatsLogs = "StatsAndLog_Shared_Connection";
			conn_connectionStatsLogs = SharedDBConnection.getDBConnection("org.gjt.mm.mysql.Driver",url_connectionStatsLogs,dbUser_connectionStatsLogs , dbPwd_connectionStatsLogs , sharedConnectionName_connectionStatsLogs);
	if (null != conn_connectionStatsLogs) {
		
			conn_connectionStatsLogs.setAutoCommit(false);
	}

	globalMap.put("conn_connectionStatsLogs",conn_connectionStatsLogs);

	globalMap.put("db_connectionStatsLogs","amc");
 



/**
 * [connectionStatsLogs begin ] stop
 */
	
	/**
	 * [connectionStatsLogs main ] start
	 */

	

	
	
	currentComponent="connectionStatsLogs";

	

 


	tos_count_connectionStatsLogs++;

/**
 * [connectionStatsLogs main ] stop
 */
	
	/**
	 * [connectionStatsLogs process_data_begin ] start
	 */

	

	
	
	currentComponent="connectionStatsLogs";

	

 



/**
 * [connectionStatsLogs process_data_begin ] stop
 */
	
	/**
	 * [connectionStatsLogs process_data_end ] start
	 */

	

	
	
	currentComponent="connectionStatsLogs";

	

 



/**
 * [connectionStatsLogs process_data_end ] stop
 */
	
	/**
	 * [connectionStatsLogs end ] start
	 */

	

	
	
	currentComponent="connectionStatsLogs";

	

 

ok_Hash.put("connectionStatsLogs", true);
end_Hash.put("connectionStatsLogs", System.currentTimeMillis());




/**
 * [connectionStatsLogs end ] stop
 */
				}//end the resume

				



	
			}catch(java.lang.Exception e){	
				
				TalendException te = new TalendException(e, currentComponent, globalMap);
				
				throw te;
			}catch(java.lang.Error error){	
				
					runStat.stopThreadStat();
				
				throw error;
			}finally{
				
				try{
					
	
	/**
	 * [connectionStatsLogs finally ] start
	 */

	

	
	
	currentComponent="connectionStatsLogs";

	

 



/**
 * [connectionStatsLogs finally ] stop
 */
				}catch(java.lang.Exception e){	
					//ignore
				}catch(java.lang.Error error){
					//ignore
				}
				resourceMap = null;
			}
		

		globalMap.put("connectionStatsLogs_SUBPROCESS_STATE", 1);
	}
	


public static class row_talendStats_DBStruct implements routines.system.IPersistableRow<row_talendStats_DBStruct> {
    final static byte[] commonByteArrayLock_GIFMIS_ledger2_12 = new byte[0];
    static byte[] commonByteArray_GIFMIS_ledger2_12 = new byte[0];

	
			    public java.util.Date moment;

				public java.util.Date getMoment () {
					return this.moment;
				}
				
			    public String pid;

				public String getPid () {
					return this.pid;
				}
				
			    public String father_pid;

				public String getFather_pid () {
					return this.father_pid;
				}
				
			    public String root_pid;

				public String getRoot_pid () {
					return this.root_pid;
				}
				
			    public Long system_pid;

				public Long getSystem_pid () {
					return this.system_pid;
				}
				
			    public String project;

				public String getProject () {
					return this.project;
				}
				
			    public String job;

				public String getJob () {
					return this.job;
				}
				
			    public String job_repository_id;

				public String getJob_repository_id () {
					return this.job_repository_id;
				}
				
			    public String job_version;

				public String getJob_version () {
					return this.job_version;
				}
				
			    public String context;

				public String getContext () {
					return this.context;
				}
				
			    public String origin;

				public String getOrigin () {
					return this.origin;
				}
				
			    public String message_type;

				public String getMessage_type () {
					return this.message_type;
				}
				
			    public String message;

				public String getMessage () {
					return this.message;
				}
				
			    public Long duration;

				public Long getDuration () {
					return this.duration;
				}
				



	private java.util.Date readDate(ObjectInputStream dis) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(dis.readLong());
		}
		return dateReturn;
	}

    private void writeDate(java.util.Date date1, ObjectOutputStream dos) throws IOException{
		if(date1 == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeLong(date1.getTime());
    	}
    }

	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_GIFMIS_ledger2_12.length) {
				if(length < 1024 && commonByteArray_GIFMIS_ledger2_12.length == 0) {
   					commonByteArray_GIFMIS_ledger2_12 = new byte[1024];
				} else {
   					commonByteArray_GIFMIS_ledger2_12 = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_GIFMIS_ledger2_12, 0, length);
			strReturn = new String(commonByteArray_GIFMIS_ledger2_12, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_GIFMIS_ledger2_12) {

        	try {

        		int length = 0;
		
					this.moment = readDate(dis);
					
					this.pid = readString(dis);
					
					this.father_pid = readString(dis);
					
					this.root_pid = readString(dis);
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.system_pid = null;
           				} else {
           			    	this.system_pid = dis.readLong();
           				}
					
					this.project = readString(dis);
					
					this.job = readString(dis);
					
					this.job_repository_id = readString(dis);
					
					this.job_version = readString(dis);
					
					this.context = readString(dis);
					
					this.origin = readString(dis);
					
					this.message_type = readString(dis);
					
					this.message = readString(dis);
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.duration = null;
           				} else {
           			    	this.duration = dis.readLong();
           				}
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// java.util.Date
				
						writeDate(this.moment,dos);
					
					// String
				
						writeString(this.pid,dos);
					
					// String
				
						writeString(this.father_pid,dos);
					
					// String
				
						writeString(this.root_pid,dos);
					
					// Long
				
						if(this.system_pid == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeLong(this.system_pid);
		            	}
					
					// String
				
						writeString(this.project,dos);
					
					// String
				
						writeString(this.job,dos);
					
					// String
				
						writeString(this.job_repository_id,dos);
					
					// String
				
						writeString(this.job_version,dos);
					
					// String
				
						writeString(this.context,dos);
					
					// String
				
						writeString(this.origin,dos);
					
					// String
				
						writeString(this.message_type,dos);
					
					// String
				
						writeString(this.message,dos);
					
					// Long
				
						if(this.duration == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeLong(this.duration);
		            	}
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("moment="+String.valueOf(moment));
		sb.append(",pid="+pid);
		sb.append(",father_pid="+father_pid);
		sb.append(",root_pid="+root_pid);
		sb.append(",system_pid="+String.valueOf(system_pid));
		sb.append(",project="+project);
		sb.append(",job="+job);
		sb.append(",job_repository_id="+job_repository_id);
		sb.append(",job_version="+job_version);
		sb.append(",context="+context);
		sb.append(",origin="+origin);
		sb.append(",message_type="+message_type);
		sb.append(",message="+message);
		sb.append(",duration="+String.valueOf(duration));
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(row_talendStats_DBStruct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}

public static class row_talendStats_STATSStruct implements routines.system.IPersistableRow<row_talendStats_STATSStruct> {
    final static byte[] commonByteArrayLock_GIFMIS_ledger2_12 = new byte[0];
    static byte[] commonByteArray_GIFMIS_ledger2_12 = new byte[0];

	
			    public java.util.Date moment;

				public java.util.Date getMoment () {
					return this.moment;
				}
				
			    public String pid;

				public String getPid () {
					return this.pid;
				}
				
			    public String father_pid;

				public String getFather_pid () {
					return this.father_pid;
				}
				
			    public String root_pid;

				public String getRoot_pid () {
					return this.root_pid;
				}
				
			    public Long system_pid;

				public Long getSystem_pid () {
					return this.system_pid;
				}
				
			    public String project;

				public String getProject () {
					return this.project;
				}
				
			    public String job;

				public String getJob () {
					return this.job;
				}
				
			    public String job_repository_id;

				public String getJob_repository_id () {
					return this.job_repository_id;
				}
				
			    public String job_version;

				public String getJob_version () {
					return this.job_version;
				}
				
			    public String context;

				public String getContext () {
					return this.context;
				}
				
			    public String origin;

				public String getOrigin () {
					return this.origin;
				}
				
			    public String message_type;

				public String getMessage_type () {
					return this.message_type;
				}
				
			    public String message;

				public String getMessage () {
					return this.message;
				}
				
			    public Long duration;

				public Long getDuration () {
					return this.duration;
				}
				



	private java.util.Date readDate(ObjectInputStream dis) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(dis.readLong());
		}
		return dateReturn;
	}

    private void writeDate(java.util.Date date1, ObjectOutputStream dos) throws IOException{
		if(date1 == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeLong(date1.getTime());
    	}
    }

	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_GIFMIS_ledger2_12.length) {
				if(length < 1024 && commonByteArray_GIFMIS_ledger2_12.length == 0) {
   					commonByteArray_GIFMIS_ledger2_12 = new byte[1024];
				} else {
   					commonByteArray_GIFMIS_ledger2_12 = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_GIFMIS_ledger2_12, 0, length);
			strReturn = new String(commonByteArray_GIFMIS_ledger2_12, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_GIFMIS_ledger2_12) {

        	try {

        		int length = 0;
		
					this.moment = readDate(dis);
					
					this.pid = readString(dis);
					
					this.father_pid = readString(dis);
					
					this.root_pid = readString(dis);
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.system_pid = null;
           				} else {
           			    	this.system_pid = dis.readLong();
           				}
					
					this.project = readString(dis);
					
					this.job = readString(dis);
					
					this.job_repository_id = readString(dis);
					
					this.job_version = readString(dis);
					
					this.context = readString(dis);
					
					this.origin = readString(dis);
					
					this.message_type = readString(dis);
					
					this.message = readString(dis);
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.duration = null;
           				} else {
           			    	this.duration = dis.readLong();
           				}
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// java.util.Date
				
						writeDate(this.moment,dos);
					
					// String
				
						writeString(this.pid,dos);
					
					// String
				
						writeString(this.father_pid,dos);
					
					// String
				
						writeString(this.root_pid,dos);
					
					// Long
				
						if(this.system_pid == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeLong(this.system_pid);
		            	}
					
					// String
				
						writeString(this.project,dos);
					
					// String
				
						writeString(this.job,dos);
					
					// String
				
						writeString(this.job_repository_id,dos);
					
					// String
				
						writeString(this.job_version,dos);
					
					// String
				
						writeString(this.context,dos);
					
					// String
				
						writeString(this.origin,dos);
					
					// String
				
						writeString(this.message_type,dos);
					
					// String
				
						writeString(this.message,dos);
					
					// Long
				
						if(this.duration == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeLong(this.duration);
		            	}
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("moment="+String.valueOf(moment));
		sb.append(",pid="+pid);
		sb.append(",father_pid="+father_pid);
		sb.append(",root_pid="+root_pid);
		sb.append(",system_pid="+String.valueOf(system_pid));
		sb.append(",project="+project);
		sb.append(",job="+job);
		sb.append(",job_repository_id="+job_repository_id);
		sb.append(",job_version="+job_version);
		sb.append(",context="+context);
		sb.append(",origin="+origin);
		sb.append(",message_type="+message_type);
		sb.append(",message="+message);
		sb.append(",duration="+String.valueOf(duration));
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(row_talendStats_STATSStruct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}
public void talendStats_STATSProcess(final java.util.Map<String, Object> globalMap) throws TalendException {
	globalMap.put("talendStats_STATS_SUBPROCESS_STATE", 0);

 final boolean execStat = this.execStat;
		String currentVirtualComponent = null;
	
		String iterateId = "";
	
	
	String currentComponent = "";
	java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

	try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception().getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { //start the resume
				globalResumeTicket = true;



		row_talendStats_STATSStruct row_talendStats_STATS = new row_talendStats_STATSStruct();
row_talendStats_DBStruct row_talendStats_DB = new row_talendStats_DBStruct();





	
	/**
	 * [talendStats_CONSOLE begin ] start
	 */

	

	
		
		ok_Hash.put("talendStats_CONSOLE", false);
		start_Hash.put("talendStats_CONSOLE", System.currentTimeMillis());
		
	
		currentVirtualComponent = "talendStats_CONSOLE";
	
	currentComponent="talendStats_CONSOLE";

	
			if (execStat) {
				if(resourceMap.get("inIterateVComp") == null){
					
						runStat.updateStatOnConnection("Main" + iterateId, 0, 0);
					
				}
			} 

		
		int tos_count_talendStats_CONSOLE = 0;
		
    	class BytesLimit65535_talendStats_CONSOLE{
    		public void limitLog4jByte() throws Exception{
    			
    		}
    	}
    	
        new BytesLimit65535_talendStats_CONSOLE().limitLog4jByte();

	///////////////////////
	
		final String OUTPUT_FIELD_SEPARATOR_talendStats_CONSOLE = "|";
		java.io.PrintStream consoleOut_talendStats_CONSOLE = null;	

 		StringBuilder strBuffer_talendStats_CONSOLE = null;
		int nb_line_talendStats_CONSOLE = 0;
///////////////////////    			



 



/**
 * [talendStats_CONSOLE begin ] stop
 */



	
	/**
	 * [talendStats_DB begin ] start
	 */

	

	
		
		ok_Hash.put("talendStats_DB", false);
		start_Hash.put("talendStats_DB", System.currentTimeMillis());
		
	
		currentVirtualComponent = "talendStats_DB";
	
	currentComponent="talendStats_DB";

	
			if (execStat) {
				if(resourceMap.get("inIterateVComp") == null){
					
						runStat.updateStatOnConnection("Main" + iterateId, 0, 0);
					
				}
			} 

		
		int tos_count_talendStats_DB = 0;
		
    	class BytesLimit65535_talendStats_DB{
    		public void limitLog4jByte() throws Exception{
    			
    		}
    	}
    	
        new BytesLimit65535_talendStats_DB().limitLog4jByte();






int nb_line_talendStats_DB = 0;
int nb_line_update_talendStats_DB = 0;
int nb_line_inserted_talendStats_DB = 0;
int nb_line_deleted_talendStats_DB = 0;
int nb_line_rejected_talendStats_DB = 0;

int deletedCount_talendStats_DB=0;
int updatedCount_talendStats_DB=0;
int insertedCount_talendStats_DB=0;

int rejectedCount_talendStats_DB=0;

String tableName_talendStats_DB = "jobStats";
boolean whetherReject_talendStats_DB = false;

java.util.Calendar calendar_talendStats_DB = java.util.Calendar.getInstance();
calendar_talendStats_DB.set(1, 0, 1, 0, 0, 0);
long year1_talendStats_DB = calendar_talendStats_DB.getTime().getTime();
calendar_talendStats_DB.set(10000, 0, 1, 0, 0, 0);
long year10000_talendStats_DB = calendar_talendStats_DB.getTime().getTime();
long date_talendStats_DB;

java.sql.Connection conn_talendStats_DB = null;
	conn_talendStats_DB = (java.sql.Connection)globalMap.get("conn_connectionStatsLogs");
	

int count_talendStats_DB=0;
    	

                    // [%connection%][checktable][tableName]
                    String keyCheckTable_talendStats_DB = conn_talendStats_DB + "[checktable]" + "[" + "jobStats" + "]";

                if(GlobalResource.resourceMap.get(keyCheckTable_talendStats_DB)== null){//}

                    synchronized (GlobalResource.resourceLockMap.get(keyCheckTable_talendStats_DB)) {//}
                        if(GlobalResource.resourceMap.get(keyCheckTable_talendStats_DB)== null){//}
                                java.sql.DatabaseMetaData dbMetaData_talendStats_DB = conn_talendStats_DB.getMetaData();
                                java.sql.ResultSet rsTable_talendStats_DB = dbMetaData_talendStats_DB.getTables(null, null, null, new String[]{"TABLE"});
                                boolean whetherExist_talendStats_DB = false;
                                while(rsTable_talendStats_DB.next()) {
                                    String table_talendStats_DB = rsTable_talendStats_DB.getString("TABLE_NAME");
                                    if(table_talendStats_DB.equalsIgnoreCase("jobStats")) {
                                        whetherExist_talendStats_DB = true;
                                        break;
                                    }
                                }
                                rsTable_talendStats_DB.close();
                                if(!whetherExist_talendStats_DB) {
                                    java.sql.Statement stmtCreate_talendStats_DB = conn_talendStats_DB.createStatement();
                                        stmtCreate_talendStats_DB.execute("CREATE TABLE `" + tableName_talendStats_DB + "`(`moment` DATETIME ,`pid` VARCHAR(20)  ,`father_pid` VARCHAR(20)  ,`root_pid` VARCHAR(20)  ,`system_pid` BIGINT(8)  ,`project` VARCHAR(50)  ,`job` VARCHAR(255)  ,`job_repository_id` VARCHAR(255)  ,`job_version` VARCHAR(255)  ,`context` VARCHAR(50)  ,`origin` VARCHAR(255)  ,`message_type` VARCHAR(255)  ,`message` VARCHAR(255)  ,`duration` BIGINT(8)  )");
                                    stmtCreate_talendStats_DB.close();
                                }
                            GlobalResource.resourceMap.put(keyCheckTable_talendStats_DB, true);
            //{{{
                        } // end of if
                    } // end synchronized
                }

		        String insert_talendStats_DB = "INSERT INTO `" + "jobStats" + "` (`moment`,`pid`,`father_pid`,`root_pid`,`system_pid`,`project`,`job`,`job_repository_id`,`job_version`,`context`,`origin`,`message_type`,`message`,`duration`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
				    


                		java.sql.PreparedStatement pstmt_talendStats_DB = null;
                        // [%connection%][psmt][tableName]
                        String keyPsmt_talendStats_DB = conn_talendStats_DB + "[psmt]" + "[" + "jobStats" + "]";
                        pstmt_talendStats_DB = SharedDBPreparedStatement.getSharedPreparedStatement(conn_talendStats_DB,insert_talendStats_DB,keyPsmt_talendStats_DB);


 



/**
 * [talendStats_DB begin ] stop
 */



	
	/**
	 * [talendStats_STATS begin ] start
	 */

	

	
		
		ok_Hash.put("talendStats_STATS", false);
		start_Hash.put("talendStats_STATS", System.currentTimeMillis());
		
	
		currentVirtualComponent = "talendStats_STATS";
	
	currentComponent="talendStats_STATS";

	
		int tos_count_talendStats_STATS = 0;
		
    	class BytesLimit65535_talendStats_STATS{
    		public void limitLog4jByte() throws Exception{
    			
    		}
    	}
    	
        new BytesLimit65535_talendStats_STATS().limitLog4jByte();

	for (StatCatcherUtils.StatCatcherMessage scm : talendStats_STATS.getMessages()) {
		row_talendStats_STATS.pid = pid;
		row_talendStats_STATS.root_pid = rootPid;
		row_talendStats_STATS.father_pid = fatherPid;	
    	row_talendStats_STATS.project = projectName;
    	row_talendStats_STATS.job = jobName;
    	row_talendStats_STATS.context = contextStr;
		row_talendStats_STATS.origin = (scm.getOrigin()==null || scm.getOrigin().length()<1 ? null : scm.getOrigin());
		row_talendStats_STATS.message = scm.getMessage();
		row_talendStats_STATS.duration = scm.getDuration();
		row_talendStats_STATS.moment = scm.getMoment();
		row_talendStats_STATS.message_type = scm.getMessageType();
		row_talendStats_STATS.job_version = scm.getJobVersion();
		row_talendStats_STATS.job_repository_id = scm.getJobId();
		row_talendStats_STATS.system_pid = scm.getSystemPid();

 



/**
 * [talendStats_STATS begin ] stop
 */
	
	/**
	 * [talendStats_STATS main ] start
	 */

	

	
	
		currentVirtualComponent = "talendStats_STATS";
	
	currentComponent="talendStats_STATS";

	

 


	tos_count_talendStats_STATS++;

/**
 * [talendStats_STATS main ] stop
 */
	
	/**
	 * [talendStats_STATS process_data_begin ] start
	 */

	

	
	
		currentVirtualComponent = "talendStats_STATS";
	
	currentComponent="talendStats_STATS";

	

 



/**
 * [talendStats_STATS process_data_begin ] stop
 */

	
	/**
	 * [talendStats_DB main ] start
	 */

	

	
	
		currentVirtualComponent = "talendStats_DB";
	
	currentComponent="talendStats_DB";

	

			//Main
			//row_talendStats_STATS


			
				if(execStat){
					runStat.updateStatOnConnection("Main"+iterateId,1, 1);
				} 
			

		



            row_talendStats_DB = null;
        whetherReject_talendStats_DB = false;
                        if(row_talendStats_STATS.moment != null) {
date_talendStats_DB = row_talendStats_STATS.moment.getTime();
if(date_talendStats_DB < year1_talendStats_DB || date_talendStats_DB >= year10000_talendStats_DB) {
pstmt_talendStats_DB.setString(1, "0000-00-00 00:00:00");
} else {pstmt_talendStats_DB.setTimestamp(1, new java.sql.Timestamp(date_talendStats_DB));
}
} else {
pstmt_talendStats_DB.setNull(1, java.sql.Types.DATE);
}

                        if(row_talendStats_STATS.pid == null) {
pstmt_talendStats_DB.setNull(2, java.sql.Types.VARCHAR);
} else {pstmt_talendStats_DB.setString(2, row_talendStats_STATS.pid);
}

                        if(row_talendStats_STATS.father_pid == null) {
pstmt_talendStats_DB.setNull(3, java.sql.Types.VARCHAR);
} else {pstmt_talendStats_DB.setString(3, row_talendStats_STATS.father_pid);
}

                        if(row_talendStats_STATS.root_pid == null) {
pstmt_talendStats_DB.setNull(4, java.sql.Types.VARCHAR);
} else {pstmt_talendStats_DB.setString(4, row_talendStats_STATS.root_pid);
}

                        if(row_talendStats_STATS.system_pid == null) {
pstmt_talendStats_DB.setNull(5, java.sql.Types.INTEGER);
} else {pstmt_talendStats_DB.setLong(5, row_talendStats_STATS.system_pid);
}

                        if(row_talendStats_STATS.project == null) {
pstmt_talendStats_DB.setNull(6, java.sql.Types.VARCHAR);
} else {pstmt_talendStats_DB.setString(6, row_talendStats_STATS.project);
}

                        if(row_talendStats_STATS.job == null) {
pstmt_talendStats_DB.setNull(7, java.sql.Types.VARCHAR);
} else {pstmt_talendStats_DB.setString(7, row_talendStats_STATS.job);
}

                        if(row_talendStats_STATS.job_repository_id == null) {
pstmt_talendStats_DB.setNull(8, java.sql.Types.VARCHAR);
} else {pstmt_talendStats_DB.setString(8, row_talendStats_STATS.job_repository_id);
}

                        if(row_talendStats_STATS.job_version == null) {
pstmt_talendStats_DB.setNull(9, java.sql.Types.VARCHAR);
} else {pstmt_talendStats_DB.setString(9, row_talendStats_STATS.job_version);
}

                        if(row_talendStats_STATS.context == null) {
pstmt_talendStats_DB.setNull(10, java.sql.Types.VARCHAR);
} else {pstmt_talendStats_DB.setString(10, row_talendStats_STATS.context);
}

                        if(row_talendStats_STATS.origin == null) {
pstmt_talendStats_DB.setNull(11, java.sql.Types.VARCHAR);
} else {pstmt_talendStats_DB.setString(11, row_talendStats_STATS.origin);
}

                        if(row_talendStats_STATS.message_type == null) {
pstmt_talendStats_DB.setNull(12, java.sql.Types.VARCHAR);
} else {pstmt_talendStats_DB.setString(12, row_talendStats_STATS.message_type);
}

                        if(row_talendStats_STATS.message == null) {
pstmt_talendStats_DB.setNull(13, java.sql.Types.VARCHAR);
} else {pstmt_talendStats_DB.setString(13, row_talendStats_STATS.message);
}

                        if(row_talendStats_STATS.duration == null) {
pstmt_talendStats_DB.setNull(14, java.sql.Types.INTEGER);
} else {pstmt_talendStats_DB.setLong(14, row_talendStats_STATS.duration);
}

                try {
                    nb_line_talendStats_DB++;
                    insertedCount_talendStats_DB = insertedCount_talendStats_DB + pstmt_talendStats_DB.executeUpdate();
                } catch(java.lang.Exception e) {
                    whetherReject_talendStats_DB = true;
                            System.err.print(e.getMessage());
                }
            if(!whetherReject_talendStats_DB) {
                            row_talendStats_DB = new row_talendStats_DBStruct();
                                row_talendStats_DB.moment = row_talendStats_STATS.moment;
                                row_talendStats_DB.pid = row_talendStats_STATS.pid;
                                row_talendStats_DB.father_pid = row_talendStats_STATS.father_pid;
                                row_talendStats_DB.root_pid = row_talendStats_STATS.root_pid;
                                row_talendStats_DB.system_pid = row_talendStats_STATS.system_pid;
                                row_talendStats_DB.project = row_talendStats_STATS.project;
                                row_talendStats_DB.job = row_talendStats_STATS.job;
                                row_talendStats_DB.job_repository_id = row_talendStats_STATS.job_repository_id;
                                row_talendStats_DB.job_version = row_talendStats_STATS.job_version;
                                row_talendStats_DB.context = row_talendStats_STATS.context;
                                row_talendStats_DB.origin = row_talendStats_STATS.origin;
                                row_talendStats_DB.message_type = row_talendStats_STATS.message_type;
                                row_talendStats_DB.message = row_talendStats_STATS.message;
                                row_talendStats_DB.duration = row_talendStats_STATS.duration;
            }

 


	tos_count_talendStats_DB++;

/**
 * [talendStats_DB main ] stop
 */
	
	/**
	 * [talendStats_DB process_data_begin ] start
	 */

	

	
	
		currentVirtualComponent = "talendStats_DB";
	
	currentComponent="talendStats_DB";

	

 



/**
 * [talendStats_DB process_data_begin ] stop
 */
// Start of branch "row_talendStats_DB"
if(row_talendStats_DB != null) { 



	
	/**
	 * [talendStats_CONSOLE main ] start
	 */

	

	
	
		currentVirtualComponent = "talendStats_CONSOLE";
	
	currentComponent="talendStats_CONSOLE";

	

			//Main
			//row_talendStats_DB


			
				if(execStat){
					runStat.updateStatOnConnection("Main"+iterateId,1, 1);
				} 
			

		
///////////////////////		
						



				strBuffer_talendStats_CONSOLE = new StringBuilder();




   				
	    		if(row_talendStats_DB.moment != null) { //              
                    							
       
				strBuffer_talendStats_CONSOLE.append(
								FormatterUtils.format_Date(row_talendStats_DB.moment, "yyyy-MM-dd HH:mm:ss")				
				);


							
	    		} //  			

    			strBuffer_talendStats_CONSOLE.append("|");
    			


   				
	    		if(row_talendStats_DB.pid != null) { //              
                    							
       
				strBuffer_talendStats_CONSOLE.append(
				                String.valueOf(row_talendStats_DB.pid)							
				);


							
	    		} //  			

    			strBuffer_talendStats_CONSOLE.append("|");
    			


   				
	    		if(row_talendStats_DB.father_pid != null) { //              
                    							
       
				strBuffer_talendStats_CONSOLE.append(
				                String.valueOf(row_talendStats_DB.father_pid)							
				);


							
	    		} //  			

    			strBuffer_talendStats_CONSOLE.append("|");
    			


   				
	    		if(row_talendStats_DB.root_pid != null) { //              
                    							
       
				strBuffer_talendStats_CONSOLE.append(
				                String.valueOf(row_talendStats_DB.root_pid)							
				);


							
	    		} //  			

    			strBuffer_talendStats_CONSOLE.append("|");
    			


   				
	    		if(row_talendStats_DB.system_pid != null) { //              
                    							
       
				strBuffer_talendStats_CONSOLE.append(
				                String.valueOf(row_talendStats_DB.system_pid)							
				);


							
	    		} //  			

    			strBuffer_talendStats_CONSOLE.append("|");
    			


   				
	    		if(row_talendStats_DB.project != null) { //              
                    							
       
				strBuffer_talendStats_CONSOLE.append(
				                String.valueOf(row_talendStats_DB.project)							
				);


							
	    		} //  			

    			strBuffer_talendStats_CONSOLE.append("|");
    			


   				
	    		if(row_talendStats_DB.job != null) { //              
                    							
       
				strBuffer_talendStats_CONSOLE.append(
				                String.valueOf(row_talendStats_DB.job)							
				);


							
	    		} //  			

    			strBuffer_talendStats_CONSOLE.append("|");
    			


   				
	    		if(row_talendStats_DB.job_repository_id != null) { //              
                    							
       
				strBuffer_talendStats_CONSOLE.append(
				                String.valueOf(row_talendStats_DB.job_repository_id)							
				);


							
	    		} //  			

    			strBuffer_talendStats_CONSOLE.append("|");
    			


   				
	    		if(row_talendStats_DB.job_version != null) { //              
                    							
       
				strBuffer_talendStats_CONSOLE.append(
				                String.valueOf(row_talendStats_DB.job_version)							
				);


							
	    		} //  			

    			strBuffer_talendStats_CONSOLE.append("|");
    			


   				
	    		if(row_talendStats_DB.context != null) { //              
                    							
       
				strBuffer_talendStats_CONSOLE.append(
				                String.valueOf(row_talendStats_DB.context)							
				);


							
	    		} //  			

    			strBuffer_talendStats_CONSOLE.append("|");
    			


   				
	    		if(row_talendStats_DB.origin != null) { //              
                    							
       
				strBuffer_talendStats_CONSOLE.append(
				                String.valueOf(row_talendStats_DB.origin)							
				);


							
	    		} //  			

    			strBuffer_talendStats_CONSOLE.append("|");
    			


   				
	    		if(row_talendStats_DB.message_type != null) { //              
                    							
       
				strBuffer_talendStats_CONSOLE.append(
				                String.valueOf(row_talendStats_DB.message_type)							
				);


							
	    		} //  			

    			strBuffer_talendStats_CONSOLE.append("|");
    			


   				
	    		if(row_talendStats_DB.message != null) { //              
                    							
       
				strBuffer_talendStats_CONSOLE.append(
				                String.valueOf(row_talendStats_DB.message)							
				);


							
	    		} //  			

    			strBuffer_talendStats_CONSOLE.append("|");
    			


   				
	    		if(row_talendStats_DB.duration != null) { //              
                    							
       
				strBuffer_talendStats_CONSOLE.append(
				                String.valueOf(row_talendStats_DB.duration)							
				);


							
	    		} //  			
 

                    if (globalMap.get("tLogRow_CONSOLE")!=null)
                    {
                    	consoleOut_talendStats_CONSOLE = (java.io.PrintStream) globalMap.get("tLogRow_CONSOLE");
                    }
                    else
                    {
                    	consoleOut_talendStats_CONSOLE = new java.io.PrintStream(new java.io.BufferedOutputStream(System.out));
                    	globalMap.put("tLogRow_CONSOLE",consoleOut_talendStats_CONSOLE);
                    }
                    consoleOut_talendStats_CONSOLE.println(strBuffer_talendStats_CONSOLE.toString());
                    consoleOut_talendStats_CONSOLE.flush();
                    nb_line_talendStats_CONSOLE++;
//////

//////                    
                    
///////////////////////    			

 


	tos_count_talendStats_CONSOLE++;

/**
 * [talendStats_CONSOLE main ] stop
 */
	
	/**
	 * [talendStats_CONSOLE process_data_begin ] start
	 */

	

	
	
		currentVirtualComponent = "talendStats_CONSOLE";
	
	currentComponent="talendStats_CONSOLE";

	

 



/**
 * [talendStats_CONSOLE process_data_begin ] stop
 */
	
	/**
	 * [talendStats_CONSOLE process_data_end ] start
	 */

	

	
	
		currentVirtualComponent = "talendStats_CONSOLE";
	
	currentComponent="talendStats_CONSOLE";

	

 



/**
 * [talendStats_CONSOLE process_data_end ] stop
 */

} // End of branch "row_talendStats_DB"




	
	/**
	 * [talendStats_DB process_data_end ] start
	 */

	

	
	
		currentVirtualComponent = "talendStats_DB";
	
	currentComponent="talendStats_DB";

	

 



/**
 * [talendStats_DB process_data_end ] stop
 */



	
	/**
	 * [talendStats_STATS process_data_end ] start
	 */

	

	
	
		currentVirtualComponent = "talendStats_STATS";
	
	currentComponent="talendStats_STATS";

	

 



/**
 * [talendStats_STATS process_data_end ] stop
 */
	
	/**
	 * [talendStats_STATS end ] start
	 */

	

	
	
		currentVirtualComponent = "talendStats_STATS";
	
	currentComponent="talendStats_STATS";

	

	}


 

ok_Hash.put("talendStats_STATS", true);
end_Hash.put("talendStats_STATS", System.currentTimeMillis());




/**
 * [talendStats_STATS end ] stop
 */

	
	/**
	 * [talendStats_DB end ] start
	 */

	

	
	
		currentVirtualComponent = "talendStats_DB";
	
	currentComponent="talendStats_DB";

	



	

        if(pstmt_talendStats_DB != null) {
			
				SharedDBPreparedStatement.releasePreparedStatement(keyPsmt_talendStats_DB);
			
        }


	nb_line_deleted_talendStats_DB=nb_line_deleted_talendStats_DB+ deletedCount_talendStats_DB;
	nb_line_update_talendStats_DB=nb_line_update_talendStats_DB + updatedCount_talendStats_DB;
	nb_line_inserted_talendStats_DB=nb_line_inserted_talendStats_DB + insertedCount_talendStats_DB;
	nb_line_rejected_talendStats_DB=nb_line_rejected_talendStats_DB + rejectedCount_talendStats_DB;
	
        globalMap.put("talendStats_DB_NB_LINE",nb_line_talendStats_DB);
        globalMap.put("talendStats_DB_NB_LINE_UPDATED",nb_line_update_talendStats_DB);
        globalMap.put("talendStats_DB_NB_LINE_INSERTED",nb_line_inserted_talendStats_DB);
        globalMap.put("talendStats_DB_NB_LINE_DELETED",nb_line_deleted_talendStats_DB);
        globalMap.put("talendStats_DB_NB_LINE_REJECTED", nb_line_rejected_talendStats_DB);
    
	

			if(execStat){
				if(resourceMap.get("inIterateVComp") == null || !((Boolean)resourceMap.get("inIterateVComp"))){
			 		runStat.updateStatOnConnection("Main"+iterateId,2, 0); 
			 	}
			}
		
 

ok_Hash.put("talendStats_DB", true);
end_Hash.put("talendStats_DB", System.currentTimeMillis());




/**
 * [talendStats_DB end ] stop
 */

	
	/**
	 * [talendStats_CONSOLE end ] start
	 */

	

	
	
		currentVirtualComponent = "talendStats_CONSOLE";
	
	currentComponent="talendStats_CONSOLE";

	


//////
//////
globalMap.put("talendStats_CONSOLE_NB_LINE",nb_line_talendStats_CONSOLE);

///////////////////////    			

			if(execStat){
				if(resourceMap.get("inIterateVComp") == null || !((Boolean)resourceMap.get("inIterateVComp"))){
			 		runStat.updateStatOnConnection("Main"+iterateId,2, 0); 
			 	}
			}
		
 

ok_Hash.put("talendStats_CONSOLE", true);
end_Hash.put("talendStats_CONSOLE", System.currentTimeMillis());




/**
 * [talendStats_CONSOLE end ] stop
 */






				}//end the resume

				
				    			if(resumeEntryMethodName == null || globalResumeTicket){
				    				resumeUtil.addLog("CHECKPOINT", "CONNECTION:SUBJOB_OK:talendStats_STATS:sub_ok_talendStats_connectionStatsLogs_Commit", "", Thread.currentThread().getId() + "", "", "", "", "", "");
								}	    				    			
					    	
								if(execStat){    	
									runStat.updateStatOnConnection("sub_ok_talendStats_connectionStatsLogs_Commit", 0, "ok");
								} 
							
							connectionStatsLogs_CommitProcess(globalMap); 
						



	
			}catch(java.lang.Exception e){	
				
				TalendException te = new TalendException(e, currentComponent, globalMap);
				
					te.setVirtualComponentName(currentVirtualComponent);
				
				throw te;
			}catch(java.lang.Error error){	
				
					runStat.stopThreadStat();
				
				throw error;
			}finally{
				
				try{
					
	
	/**
	 * [talendStats_STATS finally ] start
	 */

	

	
	
		currentVirtualComponent = "talendStats_STATS";
	
	currentComponent="talendStats_STATS";

	

 



/**
 * [talendStats_STATS finally ] stop
 */

	
	/**
	 * [talendStats_DB finally ] start
	 */

	

	
	
		currentVirtualComponent = "talendStats_DB";
	
	currentComponent="talendStats_DB";

	



	

 



/**
 * [talendStats_DB finally ] stop
 */

	
	/**
	 * [talendStats_CONSOLE finally ] start
	 */

	

	
	
		currentVirtualComponent = "talendStats_CONSOLE";
	
	currentComponent="talendStats_CONSOLE";

	

 



/**
 * [talendStats_CONSOLE finally ] stop
 */






				}catch(java.lang.Exception e){	
					//ignore
				}catch(java.lang.Error error){
					//ignore
				}
				resourceMap = null;
			}
		

		globalMap.put("talendStats_STATS_SUBPROCESS_STATE", 1);
	}
	


public static class row_talendLogs_DBStruct implements routines.system.IPersistableRow<row_talendLogs_DBStruct> {
    final static byte[] commonByteArrayLock_GIFMIS_ledger2_12 = new byte[0];
    static byte[] commonByteArray_GIFMIS_ledger2_12 = new byte[0];

	
			    public java.util.Date moment;

				public java.util.Date getMoment () {
					return this.moment;
				}
				
			    public String pid;

				public String getPid () {
					return this.pid;
				}
				
			    public String root_pid;

				public String getRoot_pid () {
					return this.root_pid;
				}
				
			    public String father_pid;

				public String getFather_pid () {
					return this.father_pid;
				}
				
			    public String project;

				public String getProject () {
					return this.project;
				}
				
			    public String job;

				public String getJob () {
					return this.job;
				}
				
			    public String context;

				public String getContext () {
					return this.context;
				}
				
			    public Integer priority;

				public Integer getPriority () {
					return this.priority;
				}
				
			    public String type;

				public String getType () {
					return this.type;
				}
				
			    public String origin;

				public String getOrigin () {
					return this.origin;
				}
				
			    public String message;

				public String getMessage () {
					return this.message;
				}
				
			    public Integer code;

				public Integer getCode () {
					return this.code;
				}
				



	private java.util.Date readDate(ObjectInputStream dis) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(dis.readLong());
		}
		return dateReturn;
	}

    private void writeDate(java.util.Date date1, ObjectOutputStream dos) throws IOException{
		if(date1 == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeLong(date1.getTime());
    	}
    }

	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_GIFMIS_ledger2_12.length) {
				if(length < 1024 && commonByteArray_GIFMIS_ledger2_12.length == 0) {
   					commonByteArray_GIFMIS_ledger2_12 = new byte[1024];
				} else {
   					commonByteArray_GIFMIS_ledger2_12 = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_GIFMIS_ledger2_12, 0, length);
			strReturn = new String(commonByteArray_GIFMIS_ledger2_12, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }
	private Integer readInteger(ObjectInputStream dis) throws IOException{
		Integer intReturn;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			intReturn = null;
		} else {
	    	intReturn = dis.readInt();
		}
		return intReturn;
	}

	private void writeInteger(Integer intNum, ObjectOutputStream dos) throws IOException{
		if(intNum == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeInt(intNum);
    	}
	}

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_GIFMIS_ledger2_12) {

        	try {

        		int length = 0;
		
					this.moment = readDate(dis);
					
					this.pid = readString(dis);
					
					this.root_pid = readString(dis);
					
					this.father_pid = readString(dis);
					
					this.project = readString(dis);
					
					this.job = readString(dis);
					
					this.context = readString(dis);
					
						this.priority = readInteger(dis);
					
					this.type = readString(dis);
					
					this.origin = readString(dis);
					
					this.message = readString(dis);
					
						this.code = readInteger(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// java.util.Date
				
						writeDate(this.moment,dos);
					
					// String
				
						writeString(this.pid,dos);
					
					// String
				
						writeString(this.root_pid,dos);
					
					// String
				
						writeString(this.father_pid,dos);
					
					// String
				
						writeString(this.project,dos);
					
					// String
				
						writeString(this.job,dos);
					
					// String
				
						writeString(this.context,dos);
					
					// Integer
				
						writeInteger(this.priority,dos);
					
					// String
				
						writeString(this.type,dos);
					
					// String
				
						writeString(this.origin,dos);
					
					// String
				
						writeString(this.message,dos);
					
					// Integer
				
						writeInteger(this.code,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("moment="+String.valueOf(moment));
		sb.append(",pid="+pid);
		sb.append(",root_pid="+root_pid);
		sb.append(",father_pid="+father_pid);
		sb.append(",project="+project);
		sb.append(",job="+job);
		sb.append(",context="+context);
		sb.append(",priority="+String.valueOf(priority));
		sb.append(",type="+type);
		sb.append(",origin="+origin);
		sb.append(",message="+message);
		sb.append(",code="+String.valueOf(code));
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(row_talendLogs_DBStruct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}

public static class row_talendLogs_LOGSStruct implements routines.system.IPersistableRow<row_talendLogs_LOGSStruct> {
    final static byte[] commonByteArrayLock_GIFMIS_ledger2_12 = new byte[0];
    static byte[] commonByteArray_GIFMIS_ledger2_12 = new byte[0];

	
			    public java.util.Date moment;

				public java.util.Date getMoment () {
					return this.moment;
				}
				
			    public String pid;

				public String getPid () {
					return this.pid;
				}
				
			    public String root_pid;

				public String getRoot_pid () {
					return this.root_pid;
				}
				
			    public String father_pid;

				public String getFather_pid () {
					return this.father_pid;
				}
				
			    public String project;

				public String getProject () {
					return this.project;
				}
				
			    public String job;

				public String getJob () {
					return this.job;
				}
				
			    public String context;

				public String getContext () {
					return this.context;
				}
				
			    public Integer priority;

				public Integer getPriority () {
					return this.priority;
				}
				
			    public String type;

				public String getType () {
					return this.type;
				}
				
			    public String origin;

				public String getOrigin () {
					return this.origin;
				}
				
			    public String message;

				public String getMessage () {
					return this.message;
				}
				
			    public Integer code;

				public Integer getCode () {
					return this.code;
				}
				



	private java.util.Date readDate(ObjectInputStream dis) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(dis.readLong());
		}
		return dateReturn;
	}

    private void writeDate(java.util.Date date1, ObjectOutputStream dos) throws IOException{
		if(date1 == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeLong(date1.getTime());
    	}
    }

	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_GIFMIS_ledger2_12.length) {
				if(length < 1024 && commonByteArray_GIFMIS_ledger2_12.length == 0) {
   					commonByteArray_GIFMIS_ledger2_12 = new byte[1024];
				} else {
   					commonByteArray_GIFMIS_ledger2_12 = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_GIFMIS_ledger2_12, 0, length);
			strReturn = new String(commonByteArray_GIFMIS_ledger2_12, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }
	private Integer readInteger(ObjectInputStream dis) throws IOException{
		Integer intReturn;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			intReturn = null;
		} else {
	    	intReturn = dis.readInt();
		}
		return intReturn;
	}

	private void writeInteger(Integer intNum, ObjectOutputStream dos) throws IOException{
		if(intNum == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeInt(intNum);
    	}
	}

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_GIFMIS_ledger2_12) {

        	try {

        		int length = 0;
		
					this.moment = readDate(dis);
					
					this.pid = readString(dis);
					
					this.root_pid = readString(dis);
					
					this.father_pid = readString(dis);
					
					this.project = readString(dis);
					
					this.job = readString(dis);
					
					this.context = readString(dis);
					
						this.priority = readInteger(dis);
					
					this.type = readString(dis);
					
					this.origin = readString(dis);
					
					this.message = readString(dis);
					
						this.code = readInteger(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// java.util.Date
				
						writeDate(this.moment,dos);
					
					// String
				
						writeString(this.pid,dos);
					
					// String
				
						writeString(this.root_pid,dos);
					
					// String
				
						writeString(this.father_pid,dos);
					
					// String
				
						writeString(this.project,dos);
					
					// String
				
						writeString(this.job,dos);
					
					// String
				
						writeString(this.context,dos);
					
					// Integer
				
						writeInteger(this.priority,dos);
					
					// String
				
						writeString(this.type,dos);
					
					// String
				
						writeString(this.origin,dos);
					
					// String
				
						writeString(this.message,dos);
					
					// Integer
				
						writeInteger(this.code,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("moment="+String.valueOf(moment));
		sb.append(",pid="+pid);
		sb.append(",root_pid="+root_pid);
		sb.append(",father_pid="+father_pid);
		sb.append(",project="+project);
		sb.append(",job="+job);
		sb.append(",context="+context);
		sb.append(",priority="+String.valueOf(priority));
		sb.append(",type="+type);
		sb.append(",origin="+origin);
		sb.append(",message="+message);
		sb.append(",code="+String.valueOf(code));
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(row_talendLogs_LOGSStruct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}
public void talendLogs_LOGSProcess(final java.util.Map<String, Object> globalMap) throws TalendException {
	globalMap.put("talendLogs_LOGS_SUBPROCESS_STATE", 0);

 final boolean execStat = this.execStat;
		String currentVirtualComponent = null;
	
		String iterateId = "";
	
	
	String currentComponent = "";
	java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

	try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception().getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { //start the resume
				globalResumeTicket = true;



		row_talendLogs_LOGSStruct row_talendLogs_LOGS = new row_talendLogs_LOGSStruct();
row_talendLogs_DBStruct row_talendLogs_DB = new row_talendLogs_DBStruct();





	
	/**
	 * [talendLogs_CONSOLE begin ] start
	 */

	

	
		
		ok_Hash.put("talendLogs_CONSOLE", false);
		start_Hash.put("talendLogs_CONSOLE", System.currentTimeMillis());
		
	
		currentVirtualComponent = "talendLogs_CONSOLE";
	
	currentComponent="talendLogs_CONSOLE";

	
			if (execStat) {
				if(resourceMap.get("inIterateVComp") == null){
					
						runStat.updateStatOnConnection("Main" + iterateId, 0, 0);
					
				}
			} 

		
		int tos_count_talendLogs_CONSOLE = 0;
		
    	class BytesLimit65535_talendLogs_CONSOLE{
    		public void limitLog4jByte() throws Exception{
    			
    		}
    	}
    	
        new BytesLimit65535_talendLogs_CONSOLE().limitLog4jByte();

	///////////////////////
	
		final String OUTPUT_FIELD_SEPARATOR_talendLogs_CONSOLE = "|";
		java.io.PrintStream consoleOut_talendLogs_CONSOLE = null;	

 		StringBuilder strBuffer_talendLogs_CONSOLE = null;
		int nb_line_talendLogs_CONSOLE = 0;
///////////////////////    			



 



/**
 * [talendLogs_CONSOLE begin ] stop
 */



	
	/**
	 * [talendLogs_DB begin ] start
	 */

	

	
		
		ok_Hash.put("talendLogs_DB", false);
		start_Hash.put("talendLogs_DB", System.currentTimeMillis());
		
	
		currentVirtualComponent = "talendLogs_DB";
	
	currentComponent="talendLogs_DB";

	
			if (execStat) {
				if(resourceMap.get("inIterateVComp") == null){
					
						runStat.updateStatOnConnection("Main" + iterateId, 0, 0);
					
				}
			} 

		
		int tos_count_talendLogs_DB = 0;
		
    	class BytesLimit65535_talendLogs_DB{
    		public void limitLog4jByte() throws Exception{
    			
    		}
    	}
    	
        new BytesLimit65535_talendLogs_DB().limitLog4jByte();






int nb_line_talendLogs_DB = 0;
int nb_line_update_talendLogs_DB = 0;
int nb_line_inserted_talendLogs_DB = 0;
int nb_line_deleted_talendLogs_DB = 0;
int nb_line_rejected_talendLogs_DB = 0;

int deletedCount_talendLogs_DB=0;
int updatedCount_talendLogs_DB=0;
int insertedCount_talendLogs_DB=0;

int rejectedCount_talendLogs_DB=0;

String tableName_talendLogs_DB = "jobLogs";
boolean whetherReject_talendLogs_DB = false;

java.util.Calendar calendar_talendLogs_DB = java.util.Calendar.getInstance();
calendar_talendLogs_DB.set(1, 0, 1, 0, 0, 0);
long year1_talendLogs_DB = calendar_talendLogs_DB.getTime().getTime();
calendar_talendLogs_DB.set(10000, 0, 1, 0, 0, 0);
long year10000_talendLogs_DB = calendar_talendLogs_DB.getTime().getTime();
long date_talendLogs_DB;

java.sql.Connection conn_talendLogs_DB = null;
	conn_talendLogs_DB = (java.sql.Connection)globalMap.get("conn_connectionStatsLogs");
	

int count_talendLogs_DB=0;
    	

                    // [%connection%][checktable][tableName]
                    String keyCheckTable_talendLogs_DB = conn_talendLogs_DB + "[checktable]" + "[" + "jobLogs" + "]";

                if(GlobalResource.resourceMap.get(keyCheckTable_talendLogs_DB)== null){//}

                    synchronized (GlobalResource.resourceLockMap.get(keyCheckTable_talendLogs_DB)) {//}
                        if(GlobalResource.resourceMap.get(keyCheckTable_talendLogs_DB)== null){//}
                                java.sql.DatabaseMetaData dbMetaData_talendLogs_DB = conn_talendLogs_DB.getMetaData();
                                java.sql.ResultSet rsTable_talendLogs_DB = dbMetaData_talendLogs_DB.getTables(null, null, null, new String[]{"TABLE"});
                                boolean whetherExist_talendLogs_DB = false;
                                while(rsTable_talendLogs_DB.next()) {
                                    String table_talendLogs_DB = rsTable_talendLogs_DB.getString("TABLE_NAME");
                                    if(table_talendLogs_DB.equalsIgnoreCase("jobLogs")) {
                                        whetherExist_talendLogs_DB = true;
                                        break;
                                    }
                                }
                                rsTable_talendLogs_DB.close();
                                if(!whetherExist_talendLogs_DB) {
                                    java.sql.Statement stmtCreate_talendLogs_DB = conn_talendLogs_DB.createStatement();
                                        stmtCreate_talendLogs_DB.execute("CREATE TABLE `" + tableName_talendLogs_DB + "`(`moment` DATETIME ,`pid` VARCHAR(20)  ,`root_pid` VARCHAR(20)  ,`father_pid` VARCHAR(20)  ,`project` VARCHAR(50)  ,`job` VARCHAR(255)  ,`context` VARCHAR(50)  ,`priority` INT(3)  ,`type` VARCHAR(255)  ,`origin` VARCHAR(255)  ,`message` VARCHAR(255)  ,`code` INT(3)  )");
                                    stmtCreate_talendLogs_DB.close();
                                }
                            GlobalResource.resourceMap.put(keyCheckTable_talendLogs_DB, true);
            //{{{
                        } // end of if
                    } // end synchronized
                }

		        String insert_talendLogs_DB = "INSERT INTO `" + "jobLogs" + "` (`moment`,`pid`,`root_pid`,`father_pid`,`project`,`job`,`context`,`priority`,`type`,`origin`,`message`,`code`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
				    


                		java.sql.PreparedStatement pstmt_talendLogs_DB = null;
                        // [%connection%][psmt][tableName]
                        String keyPsmt_talendLogs_DB = conn_talendLogs_DB + "[psmt]" + "[" + "jobLogs" + "]";
                        pstmt_talendLogs_DB = SharedDBPreparedStatement.getSharedPreparedStatement(conn_talendLogs_DB,insert_talendLogs_DB,keyPsmt_talendLogs_DB);


 



/**
 * [talendLogs_DB begin ] stop
 */



	
	/**
	 * [talendLogs_LOGS begin ] start
	 */

	

	
		
		ok_Hash.put("talendLogs_LOGS", false);
		start_Hash.put("talendLogs_LOGS", System.currentTimeMillis());
		
	
		currentVirtualComponent = "talendLogs_LOGS";
	
	currentComponent="talendLogs_LOGS";

	
		int tos_count_talendLogs_LOGS = 0;
		
    	class BytesLimit65535_talendLogs_LOGS{
    		public void limitLog4jByte() throws Exception{
    			
    		}
    	}
    	
        new BytesLimit65535_talendLogs_LOGS().limitLog4jByte();

try {
	for (LogCatcherUtils.LogCatcherMessage lcm : talendLogs_LOGS.getMessages()) {
		row_talendLogs_LOGS.type = lcm.getType();
		row_talendLogs_LOGS.origin = (lcm.getOrigin()==null || lcm.getOrigin().length()<1 ? null : lcm.getOrigin());
		row_talendLogs_LOGS.priority = lcm.getPriority();
		row_talendLogs_LOGS.message = lcm.getMessage();
		row_talendLogs_LOGS.code = lcm.getCode();
		
		row_talendLogs_LOGS.moment = java.util.Calendar.getInstance().getTime();
	
    	row_talendLogs_LOGS.pid = pid;
		row_talendLogs_LOGS.root_pid = rootPid;
		row_talendLogs_LOGS.father_pid = fatherPid;
	
    	row_talendLogs_LOGS.project = projectName;
    	row_talendLogs_LOGS.job = jobName;
    	row_talendLogs_LOGS.context = contextStr;
    		
 



/**
 * [talendLogs_LOGS begin ] stop
 */
	
	/**
	 * [talendLogs_LOGS main ] start
	 */

	

	
	
		currentVirtualComponent = "talendLogs_LOGS";
	
	currentComponent="talendLogs_LOGS";

	

 


	tos_count_talendLogs_LOGS++;

/**
 * [talendLogs_LOGS main ] stop
 */
	
	/**
	 * [talendLogs_LOGS process_data_begin ] start
	 */

	

	
	
		currentVirtualComponent = "talendLogs_LOGS";
	
	currentComponent="talendLogs_LOGS";

	

 



/**
 * [talendLogs_LOGS process_data_begin ] stop
 */

	
	/**
	 * [talendLogs_DB main ] start
	 */

	

	
	
		currentVirtualComponent = "talendLogs_DB";
	
	currentComponent="talendLogs_DB";

	

			//Main
			//row_talendLogs_LOGS


			
				if(execStat){
					runStat.updateStatOnConnection("Main"+iterateId,1, 1);
				} 
			

		



            row_talendLogs_DB = null;
        whetherReject_talendLogs_DB = false;
                        if(row_talendLogs_LOGS.moment != null) {
date_talendLogs_DB = row_talendLogs_LOGS.moment.getTime();
if(date_talendLogs_DB < year1_talendLogs_DB || date_talendLogs_DB >= year10000_talendLogs_DB) {
pstmt_talendLogs_DB.setString(1, "0000-00-00 00:00:00");
} else {pstmt_talendLogs_DB.setTimestamp(1, new java.sql.Timestamp(date_talendLogs_DB));
}
} else {
pstmt_talendLogs_DB.setNull(1, java.sql.Types.DATE);
}

                        if(row_talendLogs_LOGS.pid == null) {
pstmt_talendLogs_DB.setNull(2, java.sql.Types.VARCHAR);
} else {pstmt_talendLogs_DB.setString(2, row_talendLogs_LOGS.pid);
}

                        if(row_talendLogs_LOGS.root_pid == null) {
pstmt_talendLogs_DB.setNull(3, java.sql.Types.VARCHAR);
} else {pstmt_talendLogs_DB.setString(3, row_talendLogs_LOGS.root_pid);
}

                        if(row_talendLogs_LOGS.father_pid == null) {
pstmt_talendLogs_DB.setNull(4, java.sql.Types.VARCHAR);
} else {pstmt_talendLogs_DB.setString(4, row_talendLogs_LOGS.father_pid);
}

                        if(row_talendLogs_LOGS.project == null) {
pstmt_talendLogs_DB.setNull(5, java.sql.Types.VARCHAR);
} else {pstmt_talendLogs_DB.setString(5, row_talendLogs_LOGS.project);
}

                        if(row_talendLogs_LOGS.job == null) {
pstmt_talendLogs_DB.setNull(6, java.sql.Types.VARCHAR);
} else {pstmt_talendLogs_DB.setString(6, row_talendLogs_LOGS.job);
}

                        if(row_talendLogs_LOGS.context == null) {
pstmt_talendLogs_DB.setNull(7, java.sql.Types.VARCHAR);
} else {pstmt_talendLogs_DB.setString(7, row_talendLogs_LOGS.context);
}

                        if(row_talendLogs_LOGS.priority == null) {
pstmt_talendLogs_DB.setNull(8, java.sql.Types.INTEGER);
} else {pstmt_talendLogs_DB.setInt(8, row_talendLogs_LOGS.priority);
}

                        if(row_talendLogs_LOGS.type == null) {
pstmt_talendLogs_DB.setNull(9, java.sql.Types.VARCHAR);
} else {pstmt_talendLogs_DB.setString(9, row_talendLogs_LOGS.type);
}

                        if(row_talendLogs_LOGS.origin == null) {
pstmt_talendLogs_DB.setNull(10, java.sql.Types.VARCHAR);
} else {pstmt_talendLogs_DB.setString(10, row_talendLogs_LOGS.origin);
}

                        if(row_talendLogs_LOGS.message == null) {
pstmt_talendLogs_DB.setNull(11, java.sql.Types.VARCHAR);
} else {pstmt_talendLogs_DB.setString(11, row_talendLogs_LOGS.message);
}

                        if(row_talendLogs_LOGS.code == null) {
pstmt_talendLogs_DB.setNull(12, java.sql.Types.INTEGER);
} else {pstmt_talendLogs_DB.setInt(12, row_talendLogs_LOGS.code);
}

                try {
                    nb_line_talendLogs_DB++;
                    insertedCount_talendLogs_DB = insertedCount_talendLogs_DB + pstmt_talendLogs_DB.executeUpdate();
                } catch(java.lang.Exception e) {
                    whetherReject_talendLogs_DB = true;
                            System.err.print(e.getMessage());
                }
            if(!whetherReject_talendLogs_DB) {
                            row_talendLogs_DB = new row_talendLogs_DBStruct();
                                row_talendLogs_DB.moment = row_talendLogs_LOGS.moment;
                                row_talendLogs_DB.pid = row_talendLogs_LOGS.pid;
                                row_talendLogs_DB.root_pid = row_talendLogs_LOGS.root_pid;
                                row_talendLogs_DB.father_pid = row_talendLogs_LOGS.father_pid;
                                row_talendLogs_DB.project = row_talendLogs_LOGS.project;
                                row_talendLogs_DB.job = row_talendLogs_LOGS.job;
                                row_talendLogs_DB.context = row_talendLogs_LOGS.context;
                                row_talendLogs_DB.priority = row_talendLogs_LOGS.priority;
                                row_talendLogs_DB.type = row_talendLogs_LOGS.type;
                                row_talendLogs_DB.origin = row_talendLogs_LOGS.origin;
                                row_talendLogs_DB.message = row_talendLogs_LOGS.message;
                                row_talendLogs_DB.code = row_talendLogs_LOGS.code;
            }

 


	tos_count_talendLogs_DB++;

/**
 * [talendLogs_DB main ] stop
 */
	
	/**
	 * [talendLogs_DB process_data_begin ] start
	 */

	

	
	
		currentVirtualComponent = "talendLogs_DB";
	
	currentComponent="talendLogs_DB";

	

 



/**
 * [talendLogs_DB process_data_begin ] stop
 */
// Start of branch "row_talendLogs_DB"
if(row_talendLogs_DB != null) { 



	
	/**
	 * [talendLogs_CONSOLE main ] start
	 */

	

	
	
		currentVirtualComponent = "talendLogs_CONSOLE";
	
	currentComponent="talendLogs_CONSOLE";

	

			//Main
			//row_talendLogs_DB


			
				if(execStat){
					runStat.updateStatOnConnection("Main"+iterateId,1, 1);
				} 
			

		
///////////////////////		
						



				strBuffer_talendLogs_CONSOLE = new StringBuilder();




   				
	    		if(row_talendLogs_DB.moment != null) { //              
                    							
       
				strBuffer_talendLogs_CONSOLE.append(
								FormatterUtils.format_Date(row_talendLogs_DB.moment, "yyyy-MM-dd HH:mm:ss")				
				);


							
	    		} //  			

    			strBuffer_talendLogs_CONSOLE.append("|");
    			


   				
	    		if(row_talendLogs_DB.pid != null) { //              
                    							
       
				strBuffer_talendLogs_CONSOLE.append(
				                String.valueOf(row_talendLogs_DB.pid)							
				);


							
	    		} //  			

    			strBuffer_talendLogs_CONSOLE.append("|");
    			


   				
	    		if(row_talendLogs_DB.root_pid != null) { //              
                    							
       
				strBuffer_talendLogs_CONSOLE.append(
				                String.valueOf(row_talendLogs_DB.root_pid)							
				);


							
	    		} //  			

    			strBuffer_talendLogs_CONSOLE.append("|");
    			


   				
	    		if(row_talendLogs_DB.father_pid != null) { //              
                    							
       
				strBuffer_talendLogs_CONSOLE.append(
				                String.valueOf(row_talendLogs_DB.father_pid)							
				);


							
	    		} //  			

    			strBuffer_talendLogs_CONSOLE.append("|");
    			


   				
	    		if(row_talendLogs_DB.project != null) { //              
                    							
       
				strBuffer_talendLogs_CONSOLE.append(
				                String.valueOf(row_talendLogs_DB.project)							
				);


							
	    		} //  			

    			strBuffer_talendLogs_CONSOLE.append("|");
    			


   				
	    		if(row_talendLogs_DB.job != null) { //              
                    							
       
				strBuffer_talendLogs_CONSOLE.append(
				                String.valueOf(row_talendLogs_DB.job)							
				);


							
	    		} //  			

    			strBuffer_talendLogs_CONSOLE.append("|");
    			


   				
	    		if(row_talendLogs_DB.context != null) { //              
                    							
       
				strBuffer_talendLogs_CONSOLE.append(
				                String.valueOf(row_talendLogs_DB.context)							
				);


							
	    		} //  			

    			strBuffer_talendLogs_CONSOLE.append("|");
    			


   				
	    		if(row_talendLogs_DB.priority != null) { //              
                    							
       
				strBuffer_talendLogs_CONSOLE.append(
				                String.valueOf(row_talendLogs_DB.priority)							
				);


							
	    		} //  			

    			strBuffer_talendLogs_CONSOLE.append("|");
    			


   				
	    		if(row_talendLogs_DB.type != null) { //              
                    							
       
				strBuffer_talendLogs_CONSOLE.append(
				                String.valueOf(row_talendLogs_DB.type)							
				);


							
	    		} //  			

    			strBuffer_talendLogs_CONSOLE.append("|");
    			


   				
	    		if(row_talendLogs_DB.origin != null) { //              
                    							
       
				strBuffer_talendLogs_CONSOLE.append(
				                String.valueOf(row_talendLogs_DB.origin)							
				);


							
	    		} //  			

    			strBuffer_talendLogs_CONSOLE.append("|");
    			


   				
	    		if(row_talendLogs_DB.message != null) { //              
                    							
       
				strBuffer_talendLogs_CONSOLE.append(
				                String.valueOf(row_talendLogs_DB.message)							
				);


							
	    		} //  			

    			strBuffer_talendLogs_CONSOLE.append("|");
    			


   				
	    		if(row_talendLogs_DB.code != null) { //              
                    							
       
				strBuffer_talendLogs_CONSOLE.append(
				                String.valueOf(row_talendLogs_DB.code)							
				);


							
	    		} //  			
 

                    if (globalMap.get("tLogRow_CONSOLE")!=null)
                    {
                    	consoleOut_talendLogs_CONSOLE = (java.io.PrintStream) globalMap.get("tLogRow_CONSOLE");
                    }
                    else
                    {
                    	consoleOut_talendLogs_CONSOLE = new java.io.PrintStream(new java.io.BufferedOutputStream(System.out));
                    	globalMap.put("tLogRow_CONSOLE",consoleOut_talendLogs_CONSOLE);
                    }
                    consoleOut_talendLogs_CONSOLE.println(strBuffer_talendLogs_CONSOLE.toString());
                    consoleOut_talendLogs_CONSOLE.flush();
                    nb_line_talendLogs_CONSOLE++;
//////

//////                    
                    
///////////////////////    			

 


	tos_count_talendLogs_CONSOLE++;

/**
 * [talendLogs_CONSOLE main ] stop
 */
	
	/**
	 * [talendLogs_CONSOLE process_data_begin ] start
	 */

	

	
	
		currentVirtualComponent = "talendLogs_CONSOLE";
	
	currentComponent="talendLogs_CONSOLE";

	

 



/**
 * [talendLogs_CONSOLE process_data_begin ] stop
 */
	
	/**
	 * [talendLogs_CONSOLE process_data_end ] start
	 */

	

	
	
		currentVirtualComponent = "talendLogs_CONSOLE";
	
	currentComponent="talendLogs_CONSOLE";

	

 



/**
 * [talendLogs_CONSOLE process_data_end ] stop
 */

} // End of branch "row_talendLogs_DB"




	
	/**
	 * [talendLogs_DB process_data_end ] start
	 */

	

	
	
		currentVirtualComponent = "talendLogs_DB";
	
	currentComponent="talendLogs_DB";

	

 



/**
 * [talendLogs_DB process_data_end ] stop
 */



	
	/**
	 * [talendLogs_LOGS process_data_end ] start
	 */

	

	
	
		currentVirtualComponent = "talendLogs_LOGS";
	
	currentComponent="talendLogs_LOGS";

	

 



/**
 * [talendLogs_LOGS process_data_end ] stop
 */
	
	/**
	 * [talendLogs_LOGS end ] start
	 */

	

	
	
		currentVirtualComponent = "talendLogs_LOGS";
	
	currentComponent="talendLogs_LOGS";

	
	}
} catch (Exception e_talendLogs_LOGS) {
	logIgnoredError(String.format("talendLogs_LOGS - tLogCatcher failed to process log message(s) due to internal error: %s", e_talendLogs_LOGS), e_talendLogs_LOGS);
}

 

ok_Hash.put("talendLogs_LOGS", true);
end_Hash.put("talendLogs_LOGS", System.currentTimeMillis());




/**
 * [talendLogs_LOGS end ] stop
 */

	
	/**
	 * [talendLogs_DB end ] start
	 */

	

	
	
		currentVirtualComponent = "talendLogs_DB";
	
	currentComponent="talendLogs_DB";

	



	

        if(pstmt_talendLogs_DB != null) {
			
				SharedDBPreparedStatement.releasePreparedStatement(keyPsmt_talendLogs_DB);
			
        }


	nb_line_deleted_talendLogs_DB=nb_line_deleted_talendLogs_DB+ deletedCount_talendLogs_DB;
	nb_line_update_talendLogs_DB=nb_line_update_talendLogs_DB + updatedCount_talendLogs_DB;
	nb_line_inserted_talendLogs_DB=nb_line_inserted_talendLogs_DB + insertedCount_talendLogs_DB;
	nb_line_rejected_talendLogs_DB=nb_line_rejected_talendLogs_DB + rejectedCount_talendLogs_DB;
	
        globalMap.put("talendLogs_DB_NB_LINE",nb_line_talendLogs_DB);
        globalMap.put("talendLogs_DB_NB_LINE_UPDATED",nb_line_update_talendLogs_DB);
        globalMap.put("talendLogs_DB_NB_LINE_INSERTED",nb_line_inserted_talendLogs_DB);
        globalMap.put("talendLogs_DB_NB_LINE_DELETED",nb_line_deleted_talendLogs_DB);
        globalMap.put("talendLogs_DB_NB_LINE_REJECTED", nb_line_rejected_talendLogs_DB);
    
	

			if(execStat){
				if(resourceMap.get("inIterateVComp") == null || !((Boolean)resourceMap.get("inIterateVComp"))){
			 		runStat.updateStatOnConnection("Main"+iterateId,2, 0); 
			 	}
			}
		
 

ok_Hash.put("talendLogs_DB", true);
end_Hash.put("talendLogs_DB", System.currentTimeMillis());




/**
 * [talendLogs_DB end ] stop
 */

	
	/**
	 * [talendLogs_CONSOLE end ] start
	 */

	

	
	
		currentVirtualComponent = "talendLogs_CONSOLE";
	
	currentComponent="talendLogs_CONSOLE";

	


//////
//////
globalMap.put("talendLogs_CONSOLE_NB_LINE",nb_line_talendLogs_CONSOLE);

///////////////////////    			

			if(execStat){
				if(resourceMap.get("inIterateVComp") == null || !((Boolean)resourceMap.get("inIterateVComp"))){
			 		runStat.updateStatOnConnection("Main"+iterateId,2, 0); 
			 	}
			}
		
 

ok_Hash.put("talendLogs_CONSOLE", true);
end_Hash.put("talendLogs_CONSOLE", System.currentTimeMillis());




/**
 * [talendLogs_CONSOLE end ] stop
 */






				}//end the resume

				
				    			if(resumeEntryMethodName == null || globalResumeTicket){
				    				resumeUtil.addLog("CHECKPOINT", "CONNECTION:SUBJOB_OK:talendLogs_LOGS:sub_ok_talendLogs_connectionStatsLogs_Commit", "", Thread.currentThread().getId() + "", "", "", "", "", "");
								}	    				    			
					    	
								if(execStat){    	
									runStat.updateStatOnConnection("sub_ok_talendLogs_connectionStatsLogs_Commit", 0, "ok");
								} 
							
							connectionStatsLogs_CommitProcess(globalMap); 
						



	
			}catch(java.lang.Exception e){	
				
				TalendException te = new TalendException(e, currentComponent, globalMap);
				
					te.setVirtualComponentName(currentVirtualComponent);
				
				throw te;
			}catch(java.lang.Error error){	
				
					runStat.stopThreadStat();
				
				throw error;
			}finally{
				
				try{
					
	
	/**
	 * [talendLogs_LOGS finally ] start
	 */

	

	
	
		currentVirtualComponent = "talendLogs_LOGS";
	
	currentComponent="talendLogs_LOGS";

	

 



/**
 * [talendLogs_LOGS finally ] stop
 */

	
	/**
	 * [talendLogs_DB finally ] start
	 */

	

	
	
		currentVirtualComponent = "talendLogs_DB";
	
	currentComponent="talendLogs_DB";

	



	

 



/**
 * [talendLogs_DB finally ] stop
 */

	
	/**
	 * [talendLogs_CONSOLE finally ] start
	 */

	

	
	
		currentVirtualComponent = "talendLogs_CONSOLE";
	
	currentComponent="talendLogs_CONSOLE";

	

 



/**
 * [talendLogs_CONSOLE finally ] stop
 */






				}catch(java.lang.Exception e){	
					//ignore
				}catch(java.lang.Error error){
					//ignore
				}
				resourceMap = null;
			}
		

		globalMap.put("talendLogs_LOGS_SUBPROCESS_STATE", 1);
	}
	


public static class row_talendMeter_DBStruct implements routines.system.IPersistableRow<row_talendMeter_DBStruct> {
    final static byte[] commonByteArrayLock_GIFMIS_ledger2_12 = new byte[0];
    static byte[] commonByteArray_GIFMIS_ledger2_12 = new byte[0];

	
			    public java.util.Date moment;

				public java.util.Date getMoment () {
					return this.moment;
				}
				
			    public String pid;

				public String getPid () {
					return this.pid;
				}
				
			    public String father_pid;

				public String getFather_pid () {
					return this.father_pid;
				}
				
			    public String root_pid;

				public String getRoot_pid () {
					return this.root_pid;
				}
				
			    public Long system_pid;

				public Long getSystem_pid () {
					return this.system_pid;
				}
				
			    public String project;

				public String getProject () {
					return this.project;
				}
				
			    public String job;

				public String getJob () {
					return this.job;
				}
				
			    public String job_repository_id;

				public String getJob_repository_id () {
					return this.job_repository_id;
				}
				
			    public String job_version;

				public String getJob_version () {
					return this.job_version;
				}
				
			    public String context;

				public String getContext () {
					return this.context;
				}
				
			    public String origin;

				public String getOrigin () {
					return this.origin;
				}
				
			    public String label;

				public String getLabel () {
					return this.label;
				}
				
			    public Integer count;

				public Integer getCount () {
					return this.count;
				}
				
			    public Integer reference;

				public Integer getReference () {
					return this.reference;
				}
				
			    public String thresholds;

				public String getThresholds () {
					return this.thresholds;
				}
				



	private java.util.Date readDate(ObjectInputStream dis) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(dis.readLong());
		}
		return dateReturn;
	}

    private void writeDate(java.util.Date date1, ObjectOutputStream dos) throws IOException{
		if(date1 == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeLong(date1.getTime());
    	}
    }

	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_GIFMIS_ledger2_12.length) {
				if(length < 1024 && commonByteArray_GIFMIS_ledger2_12.length == 0) {
   					commonByteArray_GIFMIS_ledger2_12 = new byte[1024];
				} else {
   					commonByteArray_GIFMIS_ledger2_12 = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_GIFMIS_ledger2_12, 0, length);
			strReturn = new String(commonByteArray_GIFMIS_ledger2_12, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }
	private Integer readInteger(ObjectInputStream dis) throws IOException{
		Integer intReturn;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			intReturn = null;
		} else {
	    	intReturn = dis.readInt();
		}
		return intReturn;
	}

	private void writeInteger(Integer intNum, ObjectOutputStream dos) throws IOException{
		if(intNum == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeInt(intNum);
    	}
	}

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_GIFMIS_ledger2_12) {

        	try {

        		int length = 0;
		
					this.moment = readDate(dis);
					
					this.pid = readString(dis);
					
					this.father_pid = readString(dis);
					
					this.root_pid = readString(dis);
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.system_pid = null;
           				} else {
           			    	this.system_pid = dis.readLong();
           				}
					
					this.project = readString(dis);
					
					this.job = readString(dis);
					
					this.job_repository_id = readString(dis);
					
					this.job_version = readString(dis);
					
					this.context = readString(dis);
					
					this.origin = readString(dis);
					
					this.label = readString(dis);
					
						this.count = readInteger(dis);
					
						this.reference = readInteger(dis);
					
					this.thresholds = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// java.util.Date
				
						writeDate(this.moment,dos);
					
					// String
				
						writeString(this.pid,dos);
					
					// String
				
						writeString(this.father_pid,dos);
					
					// String
				
						writeString(this.root_pid,dos);
					
					// Long
				
						if(this.system_pid == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeLong(this.system_pid);
		            	}
					
					// String
				
						writeString(this.project,dos);
					
					// String
				
						writeString(this.job,dos);
					
					// String
				
						writeString(this.job_repository_id,dos);
					
					// String
				
						writeString(this.job_version,dos);
					
					// String
				
						writeString(this.context,dos);
					
					// String
				
						writeString(this.origin,dos);
					
					// String
				
						writeString(this.label,dos);
					
					// Integer
				
						writeInteger(this.count,dos);
					
					// Integer
				
						writeInteger(this.reference,dos);
					
					// String
				
						writeString(this.thresholds,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("moment="+String.valueOf(moment));
		sb.append(",pid="+pid);
		sb.append(",father_pid="+father_pid);
		sb.append(",root_pid="+root_pid);
		sb.append(",system_pid="+String.valueOf(system_pid));
		sb.append(",project="+project);
		sb.append(",job="+job);
		sb.append(",job_repository_id="+job_repository_id);
		sb.append(",job_version="+job_version);
		sb.append(",context="+context);
		sb.append(",origin="+origin);
		sb.append(",label="+label);
		sb.append(",count="+String.valueOf(count));
		sb.append(",reference="+String.valueOf(reference));
		sb.append(",thresholds="+thresholds);
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(row_talendMeter_DBStruct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}

public static class row_talendMeter_METTERStruct implements routines.system.IPersistableRow<row_talendMeter_METTERStruct> {
    final static byte[] commonByteArrayLock_GIFMIS_ledger2_12 = new byte[0];
    static byte[] commonByteArray_GIFMIS_ledger2_12 = new byte[0];

	
			    public java.util.Date moment;

				public java.util.Date getMoment () {
					return this.moment;
				}
				
			    public String pid;

				public String getPid () {
					return this.pid;
				}
				
			    public String father_pid;

				public String getFather_pid () {
					return this.father_pid;
				}
				
			    public String root_pid;

				public String getRoot_pid () {
					return this.root_pid;
				}
				
			    public Long system_pid;

				public Long getSystem_pid () {
					return this.system_pid;
				}
				
			    public String project;

				public String getProject () {
					return this.project;
				}
				
			    public String job;

				public String getJob () {
					return this.job;
				}
				
			    public String job_repository_id;

				public String getJob_repository_id () {
					return this.job_repository_id;
				}
				
			    public String job_version;

				public String getJob_version () {
					return this.job_version;
				}
				
			    public String context;

				public String getContext () {
					return this.context;
				}
				
			    public String origin;

				public String getOrigin () {
					return this.origin;
				}
				
			    public String label;

				public String getLabel () {
					return this.label;
				}
				
			    public Integer count;

				public Integer getCount () {
					return this.count;
				}
				
			    public Integer reference;

				public Integer getReference () {
					return this.reference;
				}
				
			    public String thresholds;

				public String getThresholds () {
					return this.thresholds;
				}
				



	private java.util.Date readDate(ObjectInputStream dis) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(dis.readLong());
		}
		return dateReturn;
	}

    private void writeDate(java.util.Date date1, ObjectOutputStream dos) throws IOException{
		if(date1 == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeLong(date1.getTime());
    	}
    }

	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_GIFMIS_ledger2_12.length) {
				if(length < 1024 && commonByteArray_GIFMIS_ledger2_12.length == 0) {
   					commonByteArray_GIFMIS_ledger2_12 = new byte[1024];
				} else {
   					commonByteArray_GIFMIS_ledger2_12 = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_GIFMIS_ledger2_12, 0, length);
			strReturn = new String(commonByteArray_GIFMIS_ledger2_12, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }
	private Integer readInteger(ObjectInputStream dis) throws IOException{
		Integer intReturn;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			intReturn = null;
		} else {
	    	intReturn = dis.readInt();
		}
		return intReturn;
	}

	private void writeInteger(Integer intNum, ObjectOutputStream dos) throws IOException{
		if(intNum == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeInt(intNum);
    	}
	}

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_GIFMIS_ledger2_12) {

        	try {

        		int length = 0;
		
					this.moment = readDate(dis);
					
					this.pid = readString(dis);
					
					this.father_pid = readString(dis);
					
					this.root_pid = readString(dis);
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.system_pid = null;
           				} else {
           			    	this.system_pid = dis.readLong();
           				}
					
					this.project = readString(dis);
					
					this.job = readString(dis);
					
					this.job_repository_id = readString(dis);
					
					this.job_version = readString(dis);
					
					this.context = readString(dis);
					
					this.origin = readString(dis);
					
					this.label = readString(dis);
					
						this.count = readInteger(dis);
					
						this.reference = readInteger(dis);
					
					this.thresholds = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// java.util.Date
				
						writeDate(this.moment,dos);
					
					// String
				
						writeString(this.pid,dos);
					
					// String
				
						writeString(this.father_pid,dos);
					
					// String
				
						writeString(this.root_pid,dos);
					
					// Long
				
						if(this.system_pid == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeLong(this.system_pid);
		            	}
					
					// String
				
						writeString(this.project,dos);
					
					// String
				
						writeString(this.job,dos);
					
					// String
				
						writeString(this.job_repository_id,dos);
					
					// String
				
						writeString(this.job_version,dos);
					
					// String
				
						writeString(this.context,dos);
					
					// String
				
						writeString(this.origin,dos);
					
					// String
				
						writeString(this.label,dos);
					
					// Integer
				
						writeInteger(this.count,dos);
					
					// Integer
				
						writeInteger(this.reference,dos);
					
					// String
				
						writeString(this.thresholds,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("moment="+String.valueOf(moment));
		sb.append(",pid="+pid);
		sb.append(",father_pid="+father_pid);
		sb.append(",root_pid="+root_pid);
		sb.append(",system_pid="+String.valueOf(system_pid));
		sb.append(",project="+project);
		sb.append(",job="+job);
		sb.append(",job_repository_id="+job_repository_id);
		sb.append(",job_version="+job_version);
		sb.append(",context="+context);
		sb.append(",origin="+origin);
		sb.append(",label="+label);
		sb.append(",count="+String.valueOf(count));
		sb.append(",reference="+String.valueOf(reference));
		sb.append(",thresholds="+thresholds);
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(row_talendMeter_METTERStruct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}
public void talendMeter_METTERProcess(final java.util.Map<String, Object> globalMap) throws TalendException {
	globalMap.put("talendMeter_METTER_SUBPROCESS_STATE", 0);

 final boolean execStat = this.execStat;
		String currentVirtualComponent = null;
	
		String iterateId = "";
	
	
	String currentComponent = "";
	java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

	try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception().getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { //start the resume
				globalResumeTicket = true;



		row_talendMeter_METTERStruct row_talendMeter_METTER = new row_talendMeter_METTERStruct();
row_talendMeter_DBStruct row_talendMeter_DB = new row_talendMeter_DBStruct();





	
	/**
	 * [talendMeter_CONSOLE begin ] start
	 */

	

	
		
		ok_Hash.put("talendMeter_CONSOLE", false);
		start_Hash.put("talendMeter_CONSOLE", System.currentTimeMillis());
		
	
		currentVirtualComponent = "talendMeter_CONSOLE";
	
	currentComponent="talendMeter_CONSOLE";

	
			if (execStat) {
				if(resourceMap.get("inIterateVComp") == null){
					
						runStat.updateStatOnConnection("Main" + iterateId, 0, 0);
					
				}
			} 

		
		int tos_count_talendMeter_CONSOLE = 0;
		
    	class BytesLimit65535_talendMeter_CONSOLE{
    		public void limitLog4jByte() throws Exception{
    			
    		}
    	}
    	
        new BytesLimit65535_talendMeter_CONSOLE().limitLog4jByte();

	///////////////////////
	
		final String OUTPUT_FIELD_SEPARATOR_talendMeter_CONSOLE = "|";
		java.io.PrintStream consoleOut_talendMeter_CONSOLE = null;	

 		StringBuilder strBuffer_talendMeter_CONSOLE = null;
		int nb_line_talendMeter_CONSOLE = 0;
///////////////////////    			



 



/**
 * [talendMeter_CONSOLE begin ] stop
 */



	
	/**
	 * [talendMeter_DB begin ] start
	 */

	

	
		
		ok_Hash.put("talendMeter_DB", false);
		start_Hash.put("talendMeter_DB", System.currentTimeMillis());
		
	
		currentVirtualComponent = "talendMeter_DB";
	
	currentComponent="talendMeter_DB";

	
			if (execStat) {
				if(resourceMap.get("inIterateVComp") == null){
					
						runStat.updateStatOnConnection("Main" + iterateId, 0, 0);
					
				}
			} 

		
		int tos_count_talendMeter_DB = 0;
		
    	class BytesLimit65535_talendMeter_DB{
    		public void limitLog4jByte() throws Exception{
    			
    		}
    	}
    	
        new BytesLimit65535_talendMeter_DB().limitLog4jByte();






int nb_line_talendMeter_DB = 0;
int nb_line_update_talendMeter_DB = 0;
int nb_line_inserted_talendMeter_DB = 0;
int nb_line_deleted_talendMeter_DB = 0;
int nb_line_rejected_talendMeter_DB = 0;

int deletedCount_talendMeter_DB=0;
int updatedCount_talendMeter_DB=0;
int insertedCount_talendMeter_DB=0;

int rejectedCount_talendMeter_DB=0;

String tableName_talendMeter_DB = "jobMeters";
boolean whetherReject_talendMeter_DB = false;

java.util.Calendar calendar_talendMeter_DB = java.util.Calendar.getInstance();
calendar_talendMeter_DB.set(1, 0, 1, 0, 0, 0);
long year1_talendMeter_DB = calendar_talendMeter_DB.getTime().getTime();
calendar_talendMeter_DB.set(10000, 0, 1, 0, 0, 0);
long year10000_talendMeter_DB = calendar_talendMeter_DB.getTime().getTime();
long date_talendMeter_DB;

java.sql.Connection conn_talendMeter_DB = null;
	conn_talendMeter_DB = (java.sql.Connection)globalMap.get("conn_connectionStatsLogs");
	

int count_talendMeter_DB=0;
    	

                    // [%connection%][checktable][tableName]
                    String keyCheckTable_talendMeter_DB = conn_talendMeter_DB + "[checktable]" + "[" + "jobMeters" + "]";

                if(GlobalResource.resourceMap.get(keyCheckTable_talendMeter_DB)== null){//}

                    synchronized (GlobalResource.resourceLockMap.get(keyCheckTable_talendMeter_DB)) {//}
                        if(GlobalResource.resourceMap.get(keyCheckTable_talendMeter_DB)== null){//}
                                java.sql.DatabaseMetaData dbMetaData_talendMeter_DB = conn_talendMeter_DB.getMetaData();
                                java.sql.ResultSet rsTable_talendMeter_DB = dbMetaData_talendMeter_DB.getTables(null, null, null, new String[]{"TABLE"});
                                boolean whetherExist_talendMeter_DB = false;
                                while(rsTable_talendMeter_DB.next()) {
                                    String table_talendMeter_DB = rsTable_talendMeter_DB.getString("TABLE_NAME");
                                    if(table_talendMeter_DB.equalsIgnoreCase("jobMeters")) {
                                        whetherExist_talendMeter_DB = true;
                                        break;
                                    }
                                }
                                rsTable_talendMeter_DB.close();
                                if(!whetherExist_talendMeter_DB) {
                                    java.sql.Statement stmtCreate_talendMeter_DB = conn_talendMeter_DB.createStatement();
                                        stmtCreate_talendMeter_DB.execute("CREATE TABLE `" + tableName_talendMeter_DB + "`(`moment` DATETIME ,`pid` VARCHAR(20)  ,`father_pid` VARCHAR(20)  ,`root_pid` VARCHAR(20)  ,`system_pid` BIGINT(8)  ,`project` VARCHAR(50)  ,`job` VARCHAR(255)  ,`job_repository_id` VARCHAR(255)  ,`job_version` VARCHAR(255)  ,`context` VARCHAR(50)  ,`origin` VARCHAR(255)  ,`label` VARCHAR(255)  ,`count` INT(3)  ,`reference` INT(3)  ,`thresholds` VARCHAR(255)  )");
                                    stmtCreate_talendMeter_DB.close();
                                }
                            GlobalResource.resourceMap.put(keyCheckTable_talendMeter_DB, true);
            //{{{
                        } // end of if
                    } // end synchronized
                }

		        String insert_talendMeter_DB = "INSERT INTO `" + "jobMeters" + "` (`moment`,`pid`,`father_pid`,`root_pid`,`system_pid`,`project`,`job`,`job_repository_id`,`job_version`,`context`,`origin`,`label`,`count`,`reference`,`thresholds`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
				    


                		java.sql.PreparedStatement pstmt_talendMeter_DB = null;
                        // [%connection%][psmt][tableName]
                        String keyPsmt_talendMeter_DB = conn_talendMeter_DB + "[psmt]" + "[" + "jobMeters" + "]";
                        pstmt_talendMeter_DB = SharedDBPreparedStatement.getSharedPreparedStatement(conn_talendMeter_DB,insert_talendMeter_DB,keyPsmt_talendMeter_DB);


 



/**
 * [talendMeter_DB begin ] stop
 */



	
	/**
	 * [talendMeter_METTER begin ] start
	 */

	

	
		
		ok_Hash.put("talendMeter_METTER", false);
		start_Hash.put("talendMeter_METTER", System.currentTimeMillis());
		
	
		currentVirtualComponent = "talendMeter_METTER";
	
	currentComponent="talendMeter_METTER";

	
		int tos_count_talendMeter_METTER = 0;
		
    	class BytesLimit65535_talendMeter_METTER{
    		public void limitLog4jByte() throws Exception{
    			
    		}
    	}
    	
        new BytesLimit65535_talendMeter_METTER().limitLog4jByte();

	for (MetterCatcherUtils.MetterCatcherMessage mcm : talendMeter_METTER.getMessages()) {
		row_talendMeter_METTER.pid = pid;
		row_talendMeter_METTER.root_pid = rootPid;
		row_talendMeter_METTER.father_pid = fatherPid;	
        row_talendMeter_METTER.project = projectName;
        row_talendMeter_METTER.job = jobName;
        row_talendMeter_METTER.context = contextStr;
		row_talendMeter_METTER.origin = (mcm.getOrigin()==null || mcm.getOrigin().length()<1 ? null : mcm.getOrigin());
		row_talendMeter_METTER.moment = mcm.getMoment();
		row_talendMeter_METTER.job_version = mcm.getJobVersion();
		row_talendMeter_METTER.job_repository_id = mcm.getJobId();
		row_talendMeter_METTER.system_pid = mcm.getSystemPid();
		row_talendMeter_METTER.label = mcm.getLabel();
		row_talendMeter_METTER.count = mcm.getCount();
		row_talendMeter_METTER.reference = talendMeter_METTER.getConnLinesCount(mcm.getReferense()+"_count");
		row_talendMeter_METTER.thresholds = mcm.getThresholds();
		

 



/**
 * [talendMeter_METTER begin ] stop
 */
	
	/**
	 * [talendMeter_METTER main ] start
	 */

	

	
	
		currentVirtualComponent = "talendMeter_METTER";
	
	currentComponent="talendMeter_METTER";

	

 


	tos_count_talendMeter_METTER++;

/**
 * [talendMeter_METTER main ] stop
 */
	
	/**
	 * [talendMeter_METTER process_data_begin ] start
	 */

	

	
	
		currentVirtualComponent = "talendMeter_METTER";
	
	currentComponent="talendMeter_METTER";

	

 



/**
 * [talendMeter_METTER process_data_begin ] stop
 */

	
	/**
	 * [talendMeter_DB main ] start
	 */

	

	
	
		currentVirtualComponent = "talendMeter_DB";
	
	currentComponent="talendMeter_DB";

	

			//Main
			//row_talendMeter_METTER


			
				if(execStat){
					runStat.updateStatOnConnection("Main"+iterateId,1, 1);
				} 
			

		



            row_talendMeter_DB = null;
        whetherReject_talendMeter_DB = false;
                        if(row_talendMeter_METTER.moment != null) {
date_talendMeter_DB = row_talendMeter_METTER.moment.getTime();
if(date_talendMeter_DB < year1_talendMeter_DB || date_talendMeter_DB >= year10000_talendMeter_DB) {
pstmt_talendMeter_DB.setString(1, "0000-00-00 00:00:00");
} else {pstmt_talendMeter_DB.setTimestamp(1, new java.sql.Timestamp(date_talendMeter_DB));
}
} else {
pstmt_talendMeter_DB.setNull(1, java.sql.Types.DATE);
}

                        if(row_talendMeter_METTER.pid == null) {
pstmt_talendMeter_DB.setNull(2, java.sql.Types.VARCHAR);
} else {pstmt_talendMeter_DB.setString(2, row_talendMeter_METTER.pid);
}

                        if(row_talendMeter_METTER.father_pid == null) {
pstmt_talendMeter_DB.setNull(3, java.sql.Types.VARCHAR);
} else {pstmt_talendMeter_DB.setString(3, row_talendMeter_METTER.father_pid);
}

                        if(row_talendMeter_METTER.root_pid == null) {
pstmt_talendMeter_DB.setNull(4, java.sql.Types.VARCHAR);
} else {pstmt_talendMeter_DB.setString(4, row_talendMeter_METTER.root_pid);
}

                        if(row_talendMeter_METTER.system_pid == null) {
pstmt_talendMeter_DB.setNull(5, java.sql.Types.INTEGER);
} else {pstmt_talendMeter_DB.setLong(5, row_talendMeter_METTER.system_pid);
}

                        if(row_talendMeter_METTER.project == null) {
pstmt_talendMeter_DB.setNull(6, java.sql.Types.VARCHAR);
} else {pstmt_talendMeter_DB.setString(6, row_talendMeter_METTER.project);
}

                        if(row_talendMeter_METTER.job == null) {
pstmt_talendMeter_DB.setNull(7, java.sql.Types.VARCHAR);
} else {pstmt_talendMeter_DB.setString(7, row_talendMeter_METTER.job);
}

                        if(row_talendMeter_METTER.job_repository_id == null) {
pstmt_talendMeter_DB.setNull(8, java.sql.Types.VARCHAR);
} else {pstmt_talendMeter_DB.setString(8, row_talendMeter_METTER.job_repository_id);
}

                        if(row_talendMeter_METTER.job_version == null) {
pstmt_talendMeter_DB.setNull(9, java.sql.Types.VARCHAR);
} else {pstmt_talendMeter_DB.setString(9, row_talendMeter_METTER.job_version);
}

                        if(row_talendMeter_METTER.context == null) {
pstmt_talendMeter_DB.setNull(10, java.sql.Types.VARCHAR);
} else {pstmt_talendMeter_DB.setString(10, row_talendMeter_METTER.context);
}

                        if(row_talendMeter_METTER.origin == null) {
pstmt_talendMeter_DB.setNull(11, java.sql.Types.VARCHAR);
} else {pstmt_talendMeter_DB.setString(11, row_talendMeter_METTER.origin);
}

                        if(row_talendMeter_METTER.label == null) {
pstmt_talendMeter_DB.setNull(12, java.sql.Types.VARCHAR);
} else {pstmt_talendMeter_DB.setString(12, row_talendMeter_METTER.label);
}

                        if(row_talendMeter_METTER.count == null) {
pstmt_talendMeter_DB.setNull(13, java.sql.Types.INTEGER);
} else {pstmt_talendMeter_DB.setInt(13, row_talendMeter_METTER.count);
}

                        if(row_talendMeter_METTER.reference == null) {
pstmt_talendMeter_DB.setNull(14, java.sql.Types.INTEGER);
} else {pstmt_talendMeter_DB.setInt(14, row_talendMeter_METTER.reference);
}

                        if(row_talendMeter_METTER.thresholds == null) {
pstmt_talendMeter_DB.setNull(15, java.sql.Types.VARCHAR);
} else {pstmt_talendMeter_DB.setString(15, row_talendMeter_METTER.thresholds);
}

                try {
                    nb_line_talendMeter_DB++;
                    insertedCount_talendMeter_DB = insertedCount_talendMeter_DB + pstmt_talendMeter_DB.executeUpdate();
                } catch(java.lang.Exception e) {
                    whetherReject_talendMeter_DB = true;
                            System.err.print(e.getMessage());
                }
            if(!whetherReject_talendMeter_DB) {
                            row_talendMeter_DB = new row_talendMeter_DBStruct();
                                row_talendMeter_DB.moment = row_talendMeter_METTER.moment;
                                row_talendMeter_DB.pid = row_talendMeter_METTER.pid;
                                row_talendMeter_DB.father_pid = row_talendMeter_METTER.father_pid;
                                row_talendMeter_DB.root_pid = row_talendMeter_METTER.root_pid;
                                row_talendMeter_DB.system_pid = row_talendMeter_METTER.system_pid;
                                row_talendMeter_DB.project = row_talendMeter_METTER.project;
                                row_talendMeter_DB.job = row_talendMeter_METTER.job;
                                row_talendMeter_DB.job_repository_id = row_talendMeter_METTER.job_repository_id;
                                row_talendMeter_DB.job_version = row_talendMeter_METTER.job_version;
                                row_talendMeter_DB.context = row_talendMeter_METTER.context;
                                row_talendMeter_DB.origin = row_talendMeter_METTER.origin;
                                row_talendMeter_DB.label = row_talendMeter_METTER.label;
                                row_talendMeter_DB.count = row_talendMeter_METTER.count;
                                row_talendMeter_DB.reference = row_talendMeter_METTER.reference;
                                row_talendMeter_DB.thresholds = row_talendMeter_METTER.thresholds;
            }

 


	tos_count_talendMeter_DB++;

/**
 * [talendMeter_DB main ] stop
 */
	
	/**
	 * [talendMeter_DB process_data_begin ] start
	 */

	

	
	
		currentVirtualComponent = "talendMeter_DB";
	
	currentComponent="talendMeter_DB";

	

 



/**
 * [talendMeter_DB process_data_begin ] stop
 */
// Start of branch "row_talendMeter_DB"
if(row_talendMeter_DB != null) { 



	
	/**
	 * [talendMeter_CONSOLE main ] start
	 */

	

	
	
		currentVirtualComponent = "talendMeter_CONSOLE";
	
	currentComponent="talendMeter_CONSOLE";

	

			//Main
			//row_talendMeter_DB


			
				if(execStat){
					runStat.updateStatOnConnection("Main"+iterateId,1, 1);
				} 
			

		
///////////////////////		
						



				strBuffer_talendMeter_CONSOLE = new StringBuilder();




   				
	    		if(row_talendMeter_DB.moment != null) { //              
                    							
       
				strBuffer_talendMeter_CONSOLE.append(
								FormatterUtils.format_Date(row_talendMeter_DB.moment, "yyyy-MM-dd HH:mm:ss")				
				);


							
	    		} //  			

    			strBuffer_talendMeter_CONSOLE.append("|");
    			


   				
	    		if(row_talendMeter_DB.pid != null) { //              
                    							
       
				strBuffer_talendMeter_CONSOLE.append(
				                String.valueOf(row_talendMeter_DB.pid)							
				);


							
	    		} //  			

    			strBuffer_talendMeter_CONSOLE.append("|");
    			


   				
	    		if(row_talendMeter_DB.father_pid != null) { //              
                    							
       
				strBuffer_talendMeter_CONSOLE.append(
				                String.valueOf(row_talendMeter_DB.father_pid)							
				);


							
	    		} //  			

    			strBuffer_talendMeter_CONSOLE.append("|");
    			


   				
	    		if(row_talendMeter_DB.root_pid != null) { //              
                    							
       
				strBuffer_talendMeter_CONSOLE.append(
				                String.valueOf(row_talendMeter_DB.root_pid)							
				);


							
	    		} //  			

    			strBuffer_talendMeter_CONSOLE.append("|");
    			


   				
	    		if(row_talendMeter_DB.system_pid != null) { //              
                    							
       
				strBuffer_talendMeter_CONSOLE.append(
				                String.valueOf(row_talendMeter_DB.system_pid)							
				);


							
	    		} //  			

    			strBuffer_talendMeter_CONSOLE.append("|");
    			


   				
	    		if(row_talendMeter_DB.project != null) { //              
                    							
       
				strBuffer_talendMeter_CONSOLE.append(
				                String.valueOf(row_talendMeter_DB.project)							
				);


							
	    		} //  			

    			strBuffer_talendMeter_CONSOLE.append("|");
    			


   				
	    		if(row_talendMeter_DB.job != null) { //              
                    							
       
				strBuffer_talendMeter_CONSOLE.append(
				                String.valueOf(row_talendMeter_DB.job)							
				);


							
	    		} //  			

    			strBuffer_talendMeter_CONSOLE.append("|");
    			


   				
	    		if(row_talendMeter_DB.job_repository_id != null) { //              
                    							
       
				strBuffer_talendMeter_CONSOLE.append(
				                String.valueOf(row_talendMeter_DB.job_repository_id)							
				);


							
	    		} //  			

    			strBuffer_talendMeter_CONSOLE.append("|");
    			


   				
	    		if(row_talendMeter_DB.job_version != null) { //              
                    							
       
				strBuffer_talendMeter_CONSOLE.append(
				                String.valueOf(row_talendMeter_DB.job_version)							
				);


							
	    		} //  			

    			strBuffer_talendMeter_CONSOLE.append("|");
    			


   				
	    		if(row_talendMeter_DB.context != null) { //              
                    							
       
				strBuffer_talendMeter_CONSOLE.append(
				                String.valueOf(row_talendMeter_DB.context)							
				);


							
	    		} //  			

    			strBuffer_talendMeter_CONSOLE.append("|");
    			


   				
	    		if(row_talendMeter_DB.origin != null) { //              
                    							
       
				strBuffer_talendMeter_CONSOLE.append(
				                String.valueOf(row_talendMeter_DB.origin)							
				);


							
	    		} //  			

    			strBuffer_talendMeter_CONSOLE.append("|");
    			


   				
	    		if(row_talendMeter_DB.label != null) { //              
                    							
       
				strBuffer_talendMeter_CONSOLE.append(
				                String.valueOf(row_talendMeter_DB.label)							
				);


							
	    		} //  			

    			strBuffer_talendMeter_CONSOLE.append("|");
    			


   				
	    		if(row_talendMeter_DB.count != null) { //              
                    							
       
				strBuffer_talendMeter_CONSOLE.append(
				                String.valueOf(row_talendMeter_DB.count)							
				);


							
	    		} //  			

    			strBuffer_talendMeter_CONSOLE.append("|");
    			


   				
	    		if(row_talendMeter_DB.reference != null) { //              
                    							
       
				strBuffer_talendMeter_CONSOLE.append(
				                String.valueOf(row_talendMeter_DB.reference)							
				);


							
	    		} //  			

    			strBuffer_talendMeter_CONSOLE.append("|");
    			


   				
	    		if(row_talendMeter_DB.thresholds != null) { //              
                    							
       
				strBuffer_talendMeter_CONSOLE.append(
				                String.valueOf(row_talendMeter_DB.thresholds)							
				);


							
	    		} //  			
 

                    if (globalMap.get("tLogRow_CONSOLE")!=null)
                    {
                    	consoleOut_talendMeter_CONSOLE = (java.io.PrintStream) globalMap.get("tLogRow_CONSOLE");
                    }
                    else
                    {
                    	consoleOut_talendMeter_CONSOLE = new java.io.PrintStream(new java.io.BufferedOutputStream(System.out));
                    	globalMap.put("tLogRow_CONSOLE",consoleOut_talendMeter_CONSOLE);
                    }
                    consoleOut_talendMeter_CONSOLE.println(strBuffer_talendMeter_CONSOLE.toString());
                    consoleOut_talendMeter_CONSOLE.flush();
                    nb_line_talendMeter_CONSOLE++;
//////

//////                    
                    
///////////////////////    			

 


	tos_count_talendMeter_CONSOLE++;

/**
 * [talendMeter_CONSOLE main ] stop
 */
	
	/**
	 * [talendMeter_CONSOLE process_data_begin ] start
	 */

	

	
	
		currentVirtualComponent = "talendMeter_CONSOLE";
	
	currentComponent="talendMeter_CONSOLE";

	

 



/**
 * [talendMeter_CONSOLE process_data_begin ] stop
 */
	
	/**
	 * [talendMeter_CONSOLE process_data_end ] start
	 */

	

	
	
		currentVirtualComponent = "talendMeter_CONSOLE";
	
	currentComponent="talendMeter_CONSOLE";

	

 



/**
 * [talendMeter_CONSOLE process_data_end ] stop
 */

} // End of branch "row_talendMeter_DB"




	
	/**
	 * [talendMeter_DB process_data_end ] start
	 */

	

	
	
		currentVirtualComponent = "talendMeter_DB";
	
	currentComponent="talendMeter_DB";

	

 



/**
 * [talendMeter_DB process_data_end ] stop
 */



	
	/**
	 * [talendMeter_METTER process_data_end ] start
	 */

	

	
	
		currentVirtualComponent = "talendMeter_METTER";
	
	currentComponent="talendMeter_METTER";

	

 



/**
 * [talendMeter_METTER process_data_end ] stop
 */
	
	/**
	 * [talendMeter_METTER end ] start
	 */

	

	
	
		currentVirtualComponent = "talendMeter_METTER";
	
	currentComponent="talendMeter_METTER";

	

	}


 

ok_Hash.put("talendMeter_METTER", true);
end_Hash.put("talendMeter_METTER", System.currentTimeMillis());




/**
 * [talendMeter_METTER end ] stop
 */

	
	/**
	 * [talendMeter_DB end ] start
	 */

	

	
	
		currentVirtualComponent = "talendMeter_DB";
	
	currentComponent="talendMeter_DB";

	



	

        if(pstmt_talendMeter_DB != null) {
			
				SharedDBPreparedStatement.releasePreparedStatement(keyPsmt_talendMeter_DB);
			
        }


	nb_line_deleted_talendMeter_DB=nb_line_deleted_talendMeter_DB+ deletedCount_talendMeter_DB;
	nb_line_update_talendMeter_DB=nb_line_update_talendMeter_DB + updatedCount_talendMeter_DB;
	nb_line_inserted_talendMeter_DB=nb_line_inserted_talendMeter_DB + insertedCount_talendMeter_DB;
	nb_line_rejected_talendMeter_DB=nb_line_rejected_talendMeter_DB + rejectedCount_talendMeter_DB;
	
        globalMap.put("talendMeter_DB_NB_LINE",nb_line_talendMeter_DB);
        globalMap.put("talendMeter_DB_NB_LINE_UPDATED",nb_line_update_talendMeter_DB);
        globalMap.put("talendMeter_DB_NB_LINE_INSERTED",nb_line_inserted_talendMeter_DB);
        globalMap.put("talendMeter_DB_NB_LINE_DELETED",nb_line_deleted_talendMeter_DB);
        globalMap.put("talendMeter_DB_NB_LINE_REJECTED", nb_line_rejected_talendMeter_DB);
    
	

			if(execStat){
				if(resourceMap.get("inIterateVComp") == null || !((Boolean)resourceMap.get("inIterateVComp"))){
			 		runStat.updateStatOnConnection("Main"+iterateId,2, 0); 
			 	}
			}
		
 

ok_Hash.put("talendMeter_DB", true);
end_Hash.put("talendMeter_DB", System.currentTimeMillis());




/**
 * [talendMeter_DB end ] stop
 */

	
	/**
	 * [talendMeter_CONSOLE end ] start
	 */

	

	
	
		currentVirtualComponent = "talendMeter_CONSOLE";
	
	currentComponent="talendMeter_CONSOLE";

	


//////
//////
globalMap.put("talendMeter_CONSOLE_NB_LINE",nb_line_talendMeter_CONSOLE);

///////////////////////    			

			if(execStat){
				if(resourceMap.get("inIterateVComp") == null || !((Boolean)resourceMap.get("inIterateVComp"))){
			 		runStat.updateStatOnConnection("Main"+iterateId,2, 0); 
			 	}
			}
		
 

ok_Hash.put("talendMeter_CONSOLE", true);
end_Hash.put("talendMeter_CONSOLE", System.currentTimeMillis());




/**
 * [talendMeter_CONSOLE end ] stop
 */






				}//end the resume

				
				    			if(resumeEntryMethodName == null || globalResumeTicket){
				    				resumeUtil.addLog("CHECKPOINT", "CONNECTION:SUBJOB_OK:talendMeter_METTER:sub_ok_talendMeter_connectionStatsLogs_Commit", "", Thread.currentThread().getId() + "", "", "", "", "", "");
								}	    				    			
					    	
								if(execStat){    	
									runStat.updateStatOnConnection("sub_ok_talendMeter_connectionStatsLogs_Commit", 0, "ok");
								} 
							
							connectionStatsLogs_CommitProcess(globalMap); 
						



	
			}catch(java.lang.Exception e){	
				
				TalendException te = new TalendException(e, currentComponent, globalMap);
				
					te.setVirtualComponentName(currentVirtualComponent);
				
				throw te;
			}catch(java.lang.Error error){	
				
					runStat.stopThreadStat();
				
				throw error;
			}finally{
				
				try{
					
	
	/**
	 * [talendMeter_METTER finally ] start
	 */

	

	
	
		currentVirtualComponent = "talendMeter_METTER";
	
	currentComponent="talendMeter_METTER";

	

 



/**
 * [talendMeter_METTER finally ] stop
 */

	
	/**
	 * [talendMeter_DB finally ] start
	 */

	

	
	
		currentVirtualComponent = "talendMeter_DB";
	
	currentComponent="talendMeter_DB";

	



	

 



/**
 * [talendMeter_DB finally ] stop
 */

	
	/**
	 * [talendMeter_CONSOLE finally ] start
	 */

	

	
	
		currentVirtualComponent = "talendMeter_CONSOLE";
	
	currentComponent="talendMeter_CONSOLE";

	

 



/**
 * [talendMeter_CONSOLE finally ] stop
 */






				}catch(java.lang.Exception e){	
					//ignore
				}catch(java.lang.Error error){
					//ignore
				}
				resourceMap = null;
			}
		

		globalMap.put("talendMeter_METTER_SUBPROCESS_STATE", 1);
	}
	
    public String resuming_logs_dir_path = null;
    public String resuming_checkpoint_path = null;
    public String parent_part_launcher = null;
    private String resumeEntryMethodName = null;
    private boolean globalResumeTicket = false;

    public boolean watch = false;
    // portStats is null, it means don't execute the statistics
    public Integer portStats = null;
    public int portTraces = 4334;
    public String clientHost;
    public String defaultClientHost = "localhost";
    public String contextStr = "PR";
    public boolean isDefaultContext = true;
    public String pid = "0";
    public String rootPid = null;
    public String fatherPid = null;
    public String fatherNode = null;
    public long startTime = 0;
    public boolean isChildJob = false;
    public String log4jLevel = "";

    private boolean execStat = true;

    private ThreadLocal<java.util.Map<String, String>> threadLocal = new ThreadLocal<java.util.Map<String, String>>() {
        protected java.util.Map<String, String> initialValue() {
            java.util.Map<String,String> threadRunResultMap = new java.util.HashMap<String, String>();
            threadRunResultMap.put("errorCode", null);
            threadRunResultMap.put("status", "");
            return threadRunResultMap;
        };
    };


    private PropertiesWithType context_param = new PropertiesWithType();
    public java.util.Map<String, Object> parentContextMap = new java.util.HashMap<String, Object>();

    public String status= "";
    

    public static void main(String[] args){
        final ledger2_12 ledger2_12Class = new ledger2_12();

        int exitCode = ledger2_12Class.runJobInTOS(args);

        System.exit(exitCode);
    }


    public String[][] runJob(String[] args) {

        int exitCode = runJobInTOS(args);
        String[][] bufferValue = new String[][] { { Integer.toString(exitCode) } };

        return bufferValue;
    }

    public boolean hastBufferOutputComponent() {
		boolean hastBufferOutput = false;
    	
        return hastBufferOutput;
    }

    public int runJobInTOS(String[] args) {
	   	// reset status
	   	status = "";

        String lastStr = "";
        for (String arg : args) {
            if (arg.equalsIgnoreCase("--context_param")) {
                lastStr = arg;
            } else if (lastStr.equals("")) {
                evalParam(arg);
            } else {
                evalParam(lastStr + " " + arg);
                lastStr = "";
            }
        }


        if(clientHost == null) {
            clientHost = defaultClientHost;
        }

        if(pid == null || "0".equals(pid)) {
            pid = TalendString.getAsciiRandomString(6);
        }

        if (rootPid==null) {
            rootPid = pid;
        }
        if (fatherPid==null) {
            fatherPid = pid;
        }else{
            isChildJob = true;
        }

        if (portStats != null) {
            // portStats = -1; //for testing
            if (portStats < 0 || portStats > 65535) {
                // issue:10869, the portStats is invalid, so this client socket can't open
                System.err.println("The statistics socket port " + portStats + " is invalid.");
                execStat = false;
            }
        } else {
            execStat = false;
        }

        try {
            //call job/subjob with an existing context, like: --context=production. if without this parameter, there will use the default context instead.
            java.io.InputStream inContext = ledger2_12.class.getClassLoader().getResourceAsStream("gifmis/ledger2_12_0_1/contexts/" + contextStr + ".properties");
            if (inContext == null) {
                inContext = ledger2_12.class.getClassLoader().getResourceAsStream("config/contexts/" + contextStr + ".properties");
            }
            if (inContext != null) {
                //defaultProps is in order to keep the original context value
                defaultProps.load(inContext);
                inContext.close();
                context = new ContextProperties(defaultProps);
            } else if (!isDefaultContext) {
                //print info and job continue to run, for case: context_param is not empty.
                System.err.println("Could not find the context " + contextStr);
            }

            if(!context_param.isEmpty()) {
                context.putAll(context_param);
				//set types for params from parentJobs
				for (Object key: context_param.keySet()){
					String context_key = key.toString();
					String context_type = context_param.getContextType(context_key);
					context.setContextType(context_key, context_type);

				}
            }
				    context.setContextType("filePath", "id_String");
				
                context.filePath=(String) context.getProperty("filePath");
				    context.setContextType("jobLogs", "id_String");
				
                context.jobLogs=(String) context.getProperty("jobLogs");
				    context.setContextType("jobMeters", "id_String");
				
                context.jobMeters=(String) context.getProperty("jobMeters");
				    context.setContextType("jobStats", "id_String");
				
                context.jobStats=(String) context.getProperty("jobStats");
				    context.setContextType("nexusDefaultGroupID", "id_String");
				
                context.nexusDefaultGroupID=(String) context.getProperty("nexusDefaultGroupID");
				    context.setContextType("tableName", "id_String");
				
                context.tableName=(String) context.getProperty("tableName");
				    context.setContextType("MySql_AMC_AdditionalParams", "id_String");
				
                context.MySql_AMC_AdditionalParams=(String) context.getProperty("MySql_AMC_AdditionalParams");
				    context.setContextType("MySql_AMC_Database", "id_String");
				
                context.MySql_AMC_Database=(String) context.getProperty("MySql_AMC_Database");
				    context.setContextType("MySql_AMC_Login", "id_String");
				
                context.MySql_AMC_Login=(String) context.getProperty("MySql_AMC_Login");
				    context.setContextType("MySql_AMC_Password", "id_Password");
				
            		String pwd_MySql_AMC_Password_value = context.getProperty("MySql_AMC_Password");
            		context.MySql_AMC_Password = null;
            		if(pwd_MySql_AMC_Password_value!=null) {
            			if(context_param.containsKey("MySql_AMC_Password")) {//no need to decrypt if it come from program argument or parent job runtime
            				context.MySql_AMC_Password = pwd_MySql_AMC_Password_value;
            			} else if (!pwd_MySql_AMC_Password_value.isEmpty()) {
            				try {
            					context.MySql_AMC_Password = routines.system.PasswordEncryptUtil.decryptPassword(pwd_MySql_AMC_Password_value);
            					context.put("MySql_AMC_Password",context.MySql_AMC_Password);
            				} catch (java.lang.RuntimeException e) {
            					//do nothing
            				}
            			}
            		}
				    context.setContextType("MySql_AMC_Port", "id_String");
				
                context.MySql_AMC_Port=(String) context.getProperty("MySql_AMC_Port");
				    context.setContextType("MySql_AMC_Server", "id_String");
				
                context.MySql_AMC_Server=(String) context.getProperty("MySql_AMC_Server");
				    context.setContextType("Oracle_AdditionalParams", "id_String");
				
                context.Oracle_AdditionalParams=(String) context.getProperty("Oracle_AdditionalParams");
				    context.setContextType("Oracle_Login", "id_String");
				
                context.Oracle_Login=(String) context.getProperty("Oracle_Login");
				    context.setContextType("Oracle_Password", "id_Password");
				
            		String pwd_Oracle_Password_value = context.getProperty("Oracle_Password");
            		context.Oracle_Password = null;
            		if(pwd_Oracle_Password_value!=null) {
            			if(context_param.containsKey("Oracle_Password")) {//no need to decrypt if it come from program argument or parent job runtime
            				context.Oracle_Password = pwd_Oracle_Password_value;
            			} else if (!pwd_Oracle_Password_value.isEmpty()) {
            				try {
            					context.Oracle_Password = routines.system.PasswordEncryptUtil.decryptPassword(pwd_Oracle_Password_value);
            					context.put("Oracle_Password",context.Oracle_Password);
            				} catch (java.lang.RuntimeException e) {
            					//do nothing
            				}
            			}
            		}
				    context.setContextType("Oracle_Port", "id_String");
				
                context.Oracle_Port=(String) context.getProperty("Oracle_Port");
				    context.setContextType("Oracle_Schema", "id_String");
				
                context.Oracle_Schema=(String) context.getProperty("Oracle_Schema");
				    context.setContextType("Oracle_Server", "id_String");
				
                context.Oracle_Server=(String) context.getProperty("Oracle_Server");
				    context.setContextType("Oracle_ServiceName", "id_String");
				
                context.Oracle_ServiceName=(String) context.getProperty("Oracle_ServiceName");
				    context.setContextType("Vertica_ODS_AdditionalParams", "id_String");
				
                context.Vertica_ODS_AdditionalParams=(String) context.getProperty("Vertica_ODS_AdditionalParams");
				    context.setContextType("Vertica_ODS_Database", "id_String");
				
                context.Vertica_ODS_Database=(String) context.getProperty("Vertica_ODS_Database");
				    context.setContextType("Vertica_ODS_Login", "id_String");
				
                context.Vertica_ODS_Login=(String) context.getProperty("Vertica_ODS_Login");
				    context.setContextType("Vertica_ODS_Password", "id_Password");
				
            		String pwd_Vertica_ODS_Password_value = context.getProperty("Vertica_ODS_Password");
            		context.Vertica_ODS_Password = null;
            		if(pwd_Vertica_ODS_Password_value!=null) {
            			if(context_param.containsKey("Vertica_ODS_Password")) {//no need to decrypt if it come from program argument or parent job runtime
            				context.Vertica_ODS_Password = pwd_Vertica_ODS_Password_value;
            			} else if (!pwd_Vertica_ODS_Password_value.isEmpty()) {
            				try {
            					context.Vertica_ODS_Password = routines.system.PasswordEncryptUtil.decryptPassword(pwd_Vertica_ODS_Password_value);
            					context.put("Vertica_ODS_Password",context.Vertica_ODS_Password);
            				} catch (java.lang.RuntimeException e) {
            					//do nothing
            				}
            			}
            		}
				    context.setContextType("Vertica_ODS_Port", "id_String");
				
                context.Vertica_ODS_Port=(String) context.getProperty("Vertica_ODS_Port");
				    context.setContextType("Vertica_ODS_Schema", "id_String");
				
                context.Vertica_ODS_Schema=(String) context.getProperty("Vertica_ODS_Schema");
				    context.setContextType("Vertica_ODS_Server", "id_String");
				
                context.Vertica_ODS_Server=(String) context.getProperty("Vertica_ODS_Server");
        } catch (java.io.IOException ie) {
            System.err.println("Could not load context "+contextStr);
            ie.printStackTrace();
        }


        // get context value from parent directly
        if (parentContextMap != null && !parentContextMap.isEmpty()) {if (parentContextMap.containsKey("filePath")) {
                context.filePath = (String) parentContextMap.get("filePath");
            }if (parentContextMap.containsKey("jobLogs")) {
                context.jobLogs = (String) parentContextMap.get("jobLogs");
            }if (parentContextMap.containsKey("jobMeters")) {
                context.jobMeters = (String) parentContextMap.get("jobMeters");
            }if (parentContextMap.containsKey("jobStats")) {
                context.jobStats = (String) parentContextMap.get("jobStats");
            }if (parentContextMap.containsKey("nexusDefaultGroupID")) {
                context.nexusDefaultGroupID = (String) parentContextMap.get("nexusDefaultGroupID");
            }if (parentContextMap.containsKey("tableName")) {
                context.tableName = (String) parentContextMap.get("tableName");
            }if (parentContextMap.containsKey("MySql_AMC_AdditionalParams")) {
                context.MySql_AMC_AdditionalParams = (String) parentContextMap.get("MySql_AMC_AdditionalParams");
            }if (parentContextMap.containsKey("MySql_AMC_Database")) {
                context.MySql_AMC_Database = (String) parentContextMap.get("MySql_AMC_Database");
            }if (parentContextMap.containsKey("MySql_AMC_Login")) {
                context.MySql_AMC_Login = (String) parentContextMap.get("MySql_AMC_Login");
            }if (parentContextMap.containsKey("MySql_AMC_Password")) {
                context.MySql_AMC_Password = (java.lang.String) parentContextMap.get("MySql_AMC_Password");
            }if (parentContextMap.containsKey("MySql_AMC_Port")) {
                context.MySql_AMC_Port = (String) parentContextMap.get("MySql_AMC_Port");
            }if (parentContextMap.containsKey("MySql_AMC_Server")) {
                context.MySql_AMC_Server = (String) parentContextMap.get("MySql_AMC_Server");
            }if (parentContextMap.containsKey("Oracle_AdditionalParams")) {
                context.Oracle_AdditionalParams = (String) parentContextMap.get("Oracle_AdditionalParams");
            }if (parentContextMap.containsKey("Oracle_Login")) {
                context.Oracle_Login = (String) parentContextMap.get("Oracle_Login");
            }if (parentContextMap.containsKey("Oracle_Password")) {
                context.Oracle_Password = (java.lang.String) parentContextMap.get("Oracle_Password");
            }if (parentContextMap.containsKey("Oracle_Port")) {
                context.Oracle_Port = (String) parentContextMap.get("Oracle_Port");
            }if (parentContextMap.containsKey("Oracle_Schema")) {
                context.Oracle_Schema = (String) parentContextMap.get("Oracle_Schema");
            }if (parentContextMap.containsKey("Oracle_Server")) {
                context.Oracle_Server = (String) parentContextMap.get("Oracle_Server");
            }if (parentContextMap.containsKey("Oracle_ServiceName")) {
                context.Oracle_ServiceName = (String) parentContextMap.get("Oracle_ServiceName");
            }if (parentContextMap.containsKey("Vertica_ODS_AdditionalParams")) {
                context.Vertica_ODS_AdditionalParams = (String) parentContextMap.get("Vertica_ODS_AdditionalParams");
            }if (parentContextMap.containsKey("Vertica_ODS_Database")) {
                context.Vertica_ODS_Database = (String) parentContextMap.get("Vertica_ODS_Database");
            }if (parentContextMap.containsKey("Vertica_ODS_Login")) {
                context.Vertica_ODS_Login = (String) parentContextMap.get("Vertica_ODS_Login");
            }if (parentContextMap.containsKey("Vertica_ODS_Password")) {
                context.Vertica_ODS_Password = (java.lang.String) parentContextMap.get("Vertica_ODS_Password");
            }if (parentContextMap.containsKey("Vertica_ODS_Port")) {
                context.Vertica_ODS_Port = (String) parentContextMap.get("Vertica_ODS_Port");
            }if (parentContextMap.containsKey("Vertica_ODS_Schema")) {
                context.Vertica_ODS_Schema = (String) parentContextMap.get("Vertica_ODS_Schema");
            }if (parentContextMap.containsKey("Vertica_ODS_Server")) {
                context.Vertica_ODS_Server = (String) parentContextMap.get("Vertica_ODS_Server");
            }
        }

        //Resume: init the resumeUtil
        resumeEntryMethodName = ResumeUtil.getResumeEntryMethodName(resuming_checkpoint_path);
        resumeUtil = new ResumeUtil(resuming_logs_dir_path, isChildJob, rootPid);
        resumeUtil.initCommonInfo(pid, rootPid, fatherPid, projectName, jobName, contextStr, jobVersion);

		List<String> parametersToEncrypt = new java.util.ArrayList<String>();
			parametersToEncrypt.add("MySql_AMC_Password");
			parametersToEncrypt.add("Oracle_Password");
			parametersToEncrypt.add("Vertica_ODS_Password");
        //Resume: jobStart
        resumeUtil.addLog("JOB_STARTED", "JOB:" + jobName, parent_part_launcher, Thread.currentThread().getId() + "", "","","","",resumeUtil.convertToJsonText(context,parametersToEncrypt));

if(execStat) {
    try {
        runStat.openSocket(!isChildJob);
        runStat.setAllPID(rootPid, fatherPid, pid, jobName);
        runStat.startThreadStat(clientHost, portStats);
        runStat.updateStatOnJob(RunStat.JOBSTART, fatherNode);
    } catch (java.io.IOException ioException) {
        ioException.printStackTrace();
    }
}



	
	    java.util.concurrent.ConcurrentHashMap<Object, Object> concurrentHashMap = new java.util.concurrent.ConcurrentHashMap<Object, Object>();
	    globalMap.put("concurrentHashMap", concurrentHashMap);
	

    long startUsedMemory = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
    long endUsedMemory = 0;
    long end = 0;

    startTime = System.currentTimeMillis();
        talendStats_STATS.addMessage("begin");




this.globalResumeTicket = true;//to run tPreJob

try {
errorCode = null;preStaLogConProcess(globalMap);
if(!"failure".equals(status)) { status = "end"; }
}catch (TalendException e_preStaLogCon) {
globalMap.put("preStaLogCon_SUBPROCESS_STATE", -1);

e_preStaLogCon.printStackTrace();

}
try {
errorCode = null;tPrejob_1Process(globalMap);
if(!"failure".equals(status)) { status = "end"; }
}catch (TalendException e_tPrejob_1) {
globalMap.put("tPrejob_1_SUBPROCESS_STATE", -1);

e_tPrejob_1.printStackTrace();

}


        try {
            talendStats_STATSProcess(globalMap);
        } catch (java.lang.Exception e) {
            e.printStackTrace();
        }

this.globalResumeTicket = false;//to run others jobs


this.globalResumeTicket = true;//to run tPostJob




        end = System.currentTimeMillis();

        if (watch) {
            System.out.println((end-startTime)+" milliseconds");
        }

        endUsedMemory = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
        if (false) {
            System.out.println((endUsedMemory - startUsedMemory) + " bytes memory increase when running : ledger2_12");
        }
        talendStats_STATS.addMessage(status==""?"end":status, (end-startTime));
        try {
            talendStats_STATSProcess(globalMap);
        } catch (java.lang.Exception e) {
            e.printStackTrace();
        }



if (execStat) {
    runStat.updateStatOnJob(RunStat.JOBEND, fatherNode);
    runStat.stopThreadStat();
}
    int returnCode = 0;
    if(errorCode == null) {
         returnCode = status != null && status.equals("failure") ? 1 : 0;
    } else {
         returnCode = errorCode.intValue();
    }
    resumeUtil.addLog("JOB_ENDED", "JOB:" + jobName, parent_part_launcher, Thread.currentThread().getId() + "", "","" + returnCode,"","","");

    return returnCode;

  }

    // only for OSGi env
    public void destroy() {
    closeSqlDbConnections();


    }



    private void closeSqlDbConnections() {
        try {
            Object obj_conn;
            obj_conn = globalMap.remove("conn_connectionStatsLogs");
            if (null != obj_conn) {
                ((java.sql.Connection) obj_conn).close();
            }
            obj_conn = globalMap.remove("conn_tDBConnection_1");
            if (null != obj_conn) {
                ((java.sql.Connection) obj_conn).close();
            }
        } catch (java.lang.Exception e) {
        }
    }











    private java.util.Map<String, Object> getSharedConnections4REST() {
        java.util.Map<String, Object> connections = new java.util.HashMap<String, Object>();
            connections.put("conn_connectionStatsLogs", globalMap.get("conn_connectionStatsLogs"));
            connections.put("conn_tDBConnection_1", globalMap.get("conn_tDBConnection_1"));







        return connections;
    }

    private void evalParam(String arg) {
        if (arg.startsWith("--resuming_logs_dir_path")) {
            resuming_logs_dir_path = arg.substring(25);
        } else if (arg.startsWith("--resuming_checkpoint_path")) {
            resuming_checkpoint_path = arg.substring(27);
        } else if (arg.startsWith("--parent_part_launcher")) {
            parent_part_launcher = arg.substring(23);
        } else if (arg.startsWith("--watch")) {
            watch = true;
        } else if (arg.startsWith("--stat_port=")) {
            String portStatsStr = arg.substring(12);
            if (portStatsStr != null && !portStatsStr.equals("null")) {
                portStats = Integer.parseInt(portStatsStr);
            }
        } else if (arg.startsWith("--trace_port=")) {
            portTraces = Integer.parseInt(arg.substring(13));
        } else if (arg.startsWith("--client_host=")) {
            clientHost = arg.substring(14);
        } else if (arg.startsWith("--context=")) {
            contextStr = arg.substring(10);
            isDefaultContext = false;
        } else if (arg.startsWith("--father_pid=")) {
            fatherPid = arg.substring(13);
        } else if (arg.startsWith("--root_pid=")) {
            rootPid = arg.substring(11);
        } else if (arg.startsWith("--father_node=")) {
            fatherNode = arg.substring(14);
        } else if (arg.startsWith("--pid=")) {
            pid = arg.substring(6);
        } else if (arg.startsWith("--context_type")) {
            String keyValue = arg.substring(15);
			int index = -1;
            if (keyValue != null && (index = keyValue.indexOf('=')) > -1) {
                if (fatherPid==null) {
                    context_param.setContextType(keyValue.substring(0, index), replaceEscapeChars(keyValue.substring(index + 1)));
                } else { // the subjob won't escape the especial chars
                    context_param.setContextType(keyValue.substring(0, index), keyValue.substring(index + 1) );
                }

            }

		} else if (arg.startsWith("--context_param")) {
            String keyValue = arg.substring(16);
            int index = -1;
            if (keyValue != null && (index = keyValue.indexOf('=')) > -1) {
                if (fatherPid==null) {
                    context_param.put(keyValue.substring(0, index), replaceEscapeChars(keyValue.substring(index + 1)));
                } else { // the subjob won't escape the especial chars
                    context_param.put(keyValue.substring(0, index), keyValue.substring(index + 1) );
                }
            }
        }else if (arg.startsWith("--log4jLevel=")) {
            log4jLevel = arg.substring(13);
		}

    }
    
    private static final String NULL_VALUE_EXPRESSION_IN_COMMAND_STRING_FOR_CHILD_JOB_ONLY = "<TALEND_NULL>";

    private final String[][] escapeChars = {
        {"\\\\","\\"},{"\\n","\n"},{"\\'","\'"},{"\\r","\r"},
        {"\\f","\f"},{"\\b","\b"},{"\\t","\t"}
        };
    private String replaceEscapeChars (String keyValue) {

		if (keyValue == null || ("").equals(keyValue.trim())) {
			return keyValue;
		}

		StringBuilder result = new StringBuilder();
		int currIndex = 0;
		while (currIndex < keyValue.length()) {
			int index = -1;
			// judege if the left string includes escape chars
			for (String[] strArray : escapeChars) {
				index = keyValue.indexOf(strArray[0],currIndex);
				if (index>=0) {

					result.append(keyValue.substring(currIndex, index + strArray[0].length()).replace(strArray[0], strArray[1]));
					currIndex = index + strArray[0].length();
					break;
				}
			}
			// if the left string doesn't include escape chars, append the left into the result
			if (index < 0) {
				result.append(keyValue.substring(currIndex));
				currIndex = currIndex + keyValue.length();
			}
		}

		return result.toString();
    }

    public Integer getErrorCode() {
        return errorCode;
    }


    public String getStatus() {
        return status;
    }

    ResumeUtil resumeUtil = null;
}
/************************************************************************************************
 *     344427 characters generated by Talend Data Integration 
 *     on the January 28, 2019 3:42:34 PM EET
 ************************************************************************************************/