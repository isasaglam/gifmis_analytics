package gifmis.paymentrequestalljobs_0_1;

import routines.Numeric;
import routines.DataOperation;
import routines.TalendDataGenerator;
import routines.TalendStringUtil;
import routines.TalendString;
import routines.StringHandling;
import routines.Relational;
import routines.TalendDate;
import routines.Mathematical;
import routines.SQLike;
import routines.system.*;
import routines.system.api.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.math.BigDecimal;
import java.io.ByteArrayOutputStream;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.io.IOException;
import java.util.Comparator;

//the import part of tJava_1
//import java.util.List;

@SuppressWarnings("unused")
/**
 * Job: PaymentRequestAllJobs Purpose: <br>
 * Description:  <br>
 * @author isa.saglam@oredata.com
 * @version 7.0.1.20180411_1414
 * @status 
 */
public class PaymentRequestAllJobs implements TalendJob {

	protected static void logIgnoredError(String message, Throwable cause) {
		System.err.println(message);
		if (cause != null) {
			cause.printStackTrace();
		}

	}

	public final Object obj = new Object();

	// for transmiting parameters purpose
	private Object valueObject = null;

	public Object getValueObject() {
		return this.valueObject;
	}

	public void setValueObject(Object valueObject) {
		this.valueObject = valueObject;
	}

	private final static String defaultCharset = java.nio.charset.Charset
			.defaultCharset().name();

	private final static String utf8Charset = "UTF-8";

	// contains type for every context property
	public class PropertiesWithType extends java.util.Properties {
		private static final long serialVersionUID = 1L;
		private java.util.Map<String, String> propertyTypes = new java.util.HashMap<>();

		public PropertiesWithType(java.util.Properties properties) {
			super(properties);
		}

		public PropertiesWithType() {
			super();
		}

		public void setContextType(String key, String type) {
			propertyTypes.put(key, type);
		}

		public String getContextType(String key) {
			return propertyTypes.get(key);
		}
	}

	// create and load default properties
	private java.util.Properties defaultProps = new java.util.Properties();

	// create application properties with default
	public class ContextProperties extends PropertiesWithType {

		private static final long serialVersionUID = 1L;

		public ContextProperties(java.util.Properties properties) {
			super(properties);
		}

		public ContextProperties() {
			super();
		}

		public void synchronizeContext() {

			if (filePath != null) {

				this.setProperty("filePath", filePath.toString());

			}

			if (tableName != null) {

				this.setProperty("tableName", tableName.toString());

			}

			if (Vertica_DWH_AdditionalParams != null) {

				this.setProperty("Vertica_DWH_AdditionalParams",
						Vertica_DWH_AdditionalParams.toString());

			}

			if (Vertica_DWH_Database != null) {

				this.setProperty("Vertica_DWH_Database",
						Vertica_DWH_Database.toString());

			}

			if (Vertica_DWH_Login != null) {

				this.setProperty("Vertica_DWH_Login",
						Vertica_DWH_Login.toString());

			}

			if (Vertica_DWH_Password != null) {

				this.setProperty("Vertica_DWH_Password",
						Vertica_DWH_Password.toString());

			}

			if (Vertica_DWH_Port != null) {

				this.setProperty("Vertica_DWH_Port",
						Vertica_DWH_Port.toString());

			}

			if (Vertica_DWH_Schema != null) {

				this.setProperty("Vertica_DWH_Schema",
						Vertica_DWH_Schema.toString());

			}

			if (Vertica_DWH_Server != null) {

				this.setProperty("Vertica_DWH_Server",
						Vertica_DWH_Server.toString());

			}

			if (Vertica_ODS_AdditionalParams != null) {

				this.setProperty("Vertica_ODS_AdditionalParams",
						Vertica_ODS_AdditionalParams.toString());

			}

			if (Vertica_ODS_Database != null) {

				this.setProperty("Vertica_ODS_Database",
						Vertica_ODS_Database.toString());

			}

			if (Vertica_ODS_Login != null) {

				this.setProperty("Vertica_ODS_Login",
						Vertica_ODS_Login.toString());

			}

			if (Vertica_ODS_Password != null) {

				this.setProperty("Vertica_ODS_Password",
						Vertica_ODS_Password.toString());

			}

			if (Vertica_ODS_Port != null) {

				this.setProperty("Vertica_ODS_Port",
						Vertica_ODS_Port.toString());

			}

			if (Vertica_ODS_Schema != null) {

				this.setProperty("Vertica_ODS_Schema",
						Vertica_ODS_Schema.toString());

			}

			if (Vertica_ODS_Server != null) {

				this.setProperty("Vertica_ODS_Server",
						Vertica_ODS_Server.toString());

			}

		}

		public String filePath;

		public String getFilePath() {
			return this.filePath;
		}

		public String tableName;

		public String getTableName() {
			return this.tableName;
		}

		public String Vertica_DWH_AdditionalParams;

		public String getVertica_DWH_AdditionalParams() {
			return this.Vertica_DWH_AdditionalParams;
		}

		public String Vertica_DWH_Database;

		public String getVertica_DWH_Database() {
			return this.Vertica_DWH_Database;
		}

		public String Vertica_DWH_Login;

		public String getVertica_DWH_Login() {
			return this.Vertica_DWH_Login;
		}

		public java.lang.String Vertica_DWH_Password;

		public java.lang.String getVertica_DWH_Password() {
			return this.Vertica_DWH_Password;
		}

		public String Vertica_DWH_Port;

		public String getVertica_DWH_Port() {
			return this.Vertica_DWH_Port;
		}

		public String Vertica_DWH_Schema;

		public String getVertica_DWH_Schema() {
			return this.Vertica_DWH_Schema;
		}

		public String Vertica_DWH_Server;

		public String getVertica_DWH_Server() {
			return this.Vertica_DWH_Server;
		}

		public String Vertica_ODS_AdditionalParams;

		public String getVertica_ODS_AdditionalParams() {
			return this.Vertica_ODS_AdditionalParams;
		}

		public String Vertica_ODS_Database;

		public String getVertica_ODS_Database() {
			return this.Vertica_ODS_Database;
		}

		public String Vertica_ODS_Login;

		public String getVertica_ODS_Login() {
			return this.Vertica_ODS_Login;
		}

		public java.lang.String Vertica_ODS_Password;

		public java.lang.String getVertica_ODS_Password() {
			return this.Vertica_ODS_Password;
		}

		public String Vertica_ODS_Port;

		public String getVertica_ODS_Port() {
			return this.Vertica_ODS_Port;
		}

		public String Vertica_ODS_Schema;

		public String getVertica_ODS_Schema() {
			return this.Vertica_ODS_Schema;
		}

		public String Vertica_ODS_Server;

		public String getVertica_ODS_Server() {
			return this.Vertica_ODS_Server;
		}
	}

	private ContextProperties context = new ContextProperties();

	public ContextProperties getContext() {
		return this.context;
	}

	private final String jobVersion = "0.1";
	private final String jobName = "PaymentRequestAllJobs";
	private final String projectName = "GIFMIS";
	public Integer errorCode = null;
	private String currentComponent = "";

	private final java.util.Map<String, Object> globalMap = new java.util.HashMap<String, Object>();
	private final static java.util.Map<String, Object> junitGlobalMap = new java.util.HashMap<String, Object>();

	private final java.util.Map<String, Long> start_Hash = new java.util.HashMap<String, Long>();
	private final java.util.Map<String, Long> end_Hash = new java.util.HashMap<String, Long>();
	private final java.util.Map<String, Boolean> ok_Hash = new java.util.HashMap<String, Boolean>();
	public final java.util.List<String[]> globalBuffer = new java.util.ArrayList<String[]>();

	private RunStat runStat = new RunStat();

	// OSGi DataSource
	private final static String KEY_DB_DATASOURCES = "KEY_DB_DATASOURCES";

	private final static String KEY_DB_DATASOURCES_RAW = "KEY_DB_DATASOURCES_RAW";

	public void setDataSources(
			java.util.Map<String, javax.sql.DataSource> dataSources) {
		java.util.Map<String, routines.system.TalendDataSource> talendDataSources = new java.util.HashMap<String, routines.system.TalendDataSource>();
		for (java.util.Map.Entry<String, javax.sql.DataSource> dataSourceEntry : dataSources
				.entrySet()) {
			talendDataSources.put(
					dataSourceEntry.getKey(),
					new routines.system.TalendDataSource(dataSourceEntry
							.getValue()));
		}
		globalMap.put(KEY_DB_DATASOURCES, talendDataSources);
		globalMap
				.put(KEY_DB_DATASOURCES_RAW,
						new java.util.HashMap<String, javax.sql.DataSource>(
								dataSources));
	}

	LogCatcherUtils talendLogs_LOGS = new LogCatcherUtils();
	StatCatcherUtils talendStats_STATS = new StatCatcherUtils(
			"_2Vt8EB1jEemZ8bmSTsn_gw", "0.1");
	MetterCatcherUtils talendMeter_METTER = new MetterCatcherUtils(
			"_2Vt8EB1jEemZ8bmSTsn_gw", "0.1");

	private final java.io.ByteArrayOutputStream baos = new java.io.ByteArrayOutputStream();
	private final java.io.PrintStream errorMessagePS = new java.io.PrintStream(
			new java.io.BufferedOutputStream(baos));

	public String getExceptionStackTrace() {
		if ("failure".equals(this.getStatus())) {
			errorMessagePS.flush();
			return baos.toString();
		}
		return null;
	}

	private Exception exception;

	public Exception getException() {
		if ("failure".equals(this.getStatus())) {
			return this.exception;
		}
		return null;
	}

	private class TalendException extends Exception {

		private static final long serialVersionUID = 1L;

		private java.util.Map<String, Object> globalMap = null;
		private Exception e = null;
		private String currentComponent = null;
		private String virtualComponentName = null;

		public void setVirtualComponentName(String virtualComponentName) {
			this.virtualComponentName = virtualComponentName;
		}

		private TalendException(Exception e, String errorComponent,
				final java.util.Map<String, Object> globalMap) {
			this.currentComponent = errorComponent;
			this.globalMap = globalMap;
			this.e = e;
		}

		public Exception getException() {
			return this.e;
		}

		public String getCurrentComponent() {
			return this.currentComponent;
		}

		public String getExceptionCauseMessage(Exception e) {
			Throwable cause = e;
			String message = null;
			int i = 10;
			while (null != cause && 0 < i--) {
				message = cause.getMessage();
				if (null == message) {
					cause = cause.getCause();
				} else {
					break;
				}
			}
			if (null == message) {
				message = e.getClass().getName();
			}
			return message;
		}

		@Override
		public void printStackTrace() {
			if (!(e instanceof TalendException || e instanceof TDieException)) {
				if (virtualComponentName != null
						&& currentComponent.indexOf(virtualComponentName + "_") == 0) {
					globalMap.put(virtualComponentName + "_ERROR_MESSAGE",
							getExceptionCauseMessage(e));
				}
				globalMap.put(currentComponent + "_ERROR_MESSAGE",
						getExceptionCauseMessage(e));
				System.err.println("Exception in component " + currentComponent
						+ " (" + jobName + ")");
			}
			if (!(e instanceof TDieException)) {
				if (e instanceof TalendException) {
					e.printStackTrace();
				} else {
					e.printStackTrace();
					e.printStackTrace(errorMessagePS);
					PaymentRequestAllJobs.this.exception = e;
				}
			}
			if (!(e instanceof TalendException)) {
				try {
					for (java.lang.reflect.Method m : this.getClass()
							.getEnclosingClass().getMethods()) {
						if (m.getName().compareTo(currentComponent + "_error") == 0) {
							m.invoke(PaymentRequestAllJobs.this, new Object[] {
									e, currentComponent, globalMap });
							break;
						}
					}

					if (!(e instanceof TDieException)) {
						talendLogs_LOGS.addMessage("Java Exception",
								currentComponent, 6, e.getClass().getName()
										+ ":" + e.getMessage(), 1);
						talendLogs_LOGSProcess(globalMap);
					}
				} catch (TalendException e) {
					// do nothing

				} catch (Exception e) {
					this.e.printStackTrace();
				}
			}
		}
	}

	public void preStaLogCon_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		preStaLogCon_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tPrejob_1_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		talendStats_STATS.addMessage("failure", errorComponent,
				end_Hash.get(errorComponent) - start_Hash.get(errorComponent));
		talendStats_STATSProcess(globalMap);

		status = "failure";

		tPrejob_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tJava_1_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		talendStats_STATS.addMessage("failure", errorComponent,
				end_Hash.get(errorComponent) - start_Hash.get(errorComponent));
		talendStats_STATSProcess(globalMap);

		status = "failure";

		tJava_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tRedirectOutput_1_error(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tRedirectOutput_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tRunJob_1_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tRunJob_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tRunJob_2_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tRunJob_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tRunJob_3_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tRunJob_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tRunJob_4_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tRunJob_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tRunJob_5_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tRunJob_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tRunJob_6_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tRunJob_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tRunJob_7_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tRunJob_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tRunJob_8_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tRunJob_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tRunJob_9_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tRunJob_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tRunJob_10_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tRunJob_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tRunJob_11_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tRunJob_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tRunJob_12_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tRunJob_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void connectionStatsLogs_Commit_error(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		connectionStatsLogs_Commit_onSubJobError(exception, errorComponent,
				globalMap);
	}

	public void connectionStatsLogs_error(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		connectionStatsLogs_onSubJobError(exception, errorComponent, globalMap);
	}

	public void talendStats_STATS_error(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		talendStats_DB_error(exception, errorComponent, globalMap);

	}

	public void talendStats_DB_error(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		talendStats_CONSOLE_error(exception, errorComponent, globalMap);

	}

	public void talendStats_CONSOLE_error(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		talendStats_STATS_onSubJobError(exception, errorComponent, globalMap);
	}

	public void talendLogs_LOGS_error(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		talendLogs_DB_error(exception, errorComponent, globalMap);

	}

	public void talendLogs_DB_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap)
			throws TalendException {

		talendLogs_CONSOLE_error(exception, errorComponent, globalMap);

	}

	public void talendLogs_CONSOLE_error(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		talendLogs_LOGS_onSubJobError(exception, errorComponent, globalMap);
	}

	public void talendMeter_METTER_error(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		talendMeter_DB_error(exception, errorComponent, globalMap);

	}

	public void talendMeter_DB_error(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		talendMeter_CONSOLE_error(exception, errorComponent, globalMap);

	}

	public void talendMeter_CONSOLE_error(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		talendMeter_METTER_onSubJobError(exception, errorComponent, globalMap);
	}

	public void preStaLogCon_onSubJobError(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread
				.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(),
				ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public void tPrejob_1_onSubJobError(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread
				.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(),
				ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public void tJava_1_onSubJobError(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread
				.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(),
				ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public void tRedirectOutput_1_onSubJobError(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread
				.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(),
				ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public void tRunJob_1_onSubJobError(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread
				.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(),
				ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public void connectionStatsLogs_Commit_onSubJobError(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread
				.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(),
				ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public void connectionStatsLogs_onSubJobError(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread
				.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(),
				ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public void talendStats_STATS_onSubJobError(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread
				.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(),
				ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public void talendLogs_LOGS_onSubJobError(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread
				.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(),
				ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public void talendMeter_METTER_onSubJobError(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread
				.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(),
				ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public void preStaLogConProcess(
			final java.util.Map<String, Object> globalMap)
			throws TalendException {
		globalMap.put("preStaLogCon_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception()
						.getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				/**
				 * [preStaLogCon begin ] start
				 */

				ok_Hash.put("preStaLogCon", false);
				start_Hash.put("preStaLogCon", System.currentTimeMillis());

				currentComponent = "preStaLogCon";

				int tos_count_preStaLogCon = 0;

				class BytesLimit65535_preStaLogCon {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_preStaLogCon().limitLog4jByte();

				/**
				 * [preStaLogCon begin ] stop
				 */

				/**
				 * [preStaLogCon main ] start
				 */

				currentComponent = "preStaLogCon";

				tos_count_preStaLogCon++;

				/**
				 * [preStaLogCon main ] stop
				 */

				/**
				 * [preStaLogCon process_data_begin ] start
				 */

				currentComponent = "preStaLogCon";

				/**
				 * [preStaLogCon process_data_begin ] stop
				 */

				/**
				 * [preStaLogCon process_data_end ] start
				 */

				currentComponent = "preStaLogCon";

				/**
				 * [preStaLogCon process_data_end ] stop
				 */

				/**
				 * [preStaLogCon end ] start
				 */

				currentComponent = "preStaLogCon";

				ok_Hash.put("preStaLogCon", true);
				end_Hash.put("preStaLogCon", System.currentTimeMillis());

				if (execStat) {
					runStat.updateStatOnConnection(
							"after_preStaLogCon_connectionStatsLogs", 0, "ok");
				}
				connectionStatsLogsProcess(globalMap);

				/**
				 * [preStaLogCon end ] stop
				 */
			}// end the resume

		} catch (java.lang.Exception e) {

			TalendException te = new TalendException(e, currentComponent,
					globalMap);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			try {

				/**
				 * [preStaLogCon finally ] start
				 */

				currentComponent = "preStaLogCon";

				/**
				 * [preStaLogCon finally ] stop
				 */
			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("preStaLogCon_SUBPROCESS_STATE", 1);
	}

	public void tPrejob_1Process(final java.util.Map<String, Object> globalMap)
			throws TalendException {
		globalMap.put("tPrejob_1_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception()
						.getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				/**
				 * [tPrejob_1 begin ] start
				 */

				ok_Hash.put("tPrejob_1", false);
				start_Hash.put("tPrejob_1", System.currentTimeMillis());

				talendStats_STATS.addMessage("begin", "tPrejob_1");
				talendStats_STATSProcess(globalMap);

				currentComponent = "tPrejob_1";

				int tos_count_tPrejob_1 = 0;

				class BytesLimit65535_tPrejob_1 {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_tPrejob_1().limitLog4jByte();

				/**
				 * [tPrejob_1 begin ] stop
				 */

				/**
				 * [tPrejob_1 main ] start
				 */

				currentComponent = "tPrejob_1";

				tos_count_tPrejob_1++;

				/**
				 * [tPrejob_1 main ] stop
				 */

				/**
				 * [tPrejob_1 process_data_begin ] start
				 */

				currentComponent = "tPrejob_1";

				/**
				 * [tPrejob_1 process_data_begin ] stop
				 */

				/**
				 * [tPrejob_1 process_data_end ] start
				 */

				currentComponent = "tPrejob_1";

				/**
				 * [tPrejob_1 process_data_end ] stop
				 */

				/**
				 * [tPrejob_1 end ] start
				 */

				currentComponent = "tPrejob_1";

				ok_Hash.put("tPrejob_1", true);
				end_Hash.put("tPrejob_1", System.currentTimeMillis());

				talendStats_STATS
						.addMessage(
								"end",
								"tPrejob_1",
								end_Hash.get("tPrejob_1")
										- start_Hash.get("tPrejob_1"));
				talendStats_STATSProcess(globalMap);
				if (execStat) {
					runStat.updateStatOnConnection("OnComponentOk1", 0, "ok");
				}
				tJava_1Process(globalMap);

				/**
				 * [tPrejob_1 end ] stop
				 */
			}// end the resume

		} catch (java.lang.Exception e) {

			TalendException te = new TalendException(e, currentComponent,
					globalMap);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			try {

				/**
				 * [tPrejob_1 finally ] start
				 */

				currentComponent = "tPrejob_1";

				/**
				 * [tPrejob_1 finally ] stop
				 */
			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("tPrejob_1_SUBPROCESS_STATE", 1);
	}

	public void tJava_1Process(final java.util.Map<String, Object> globalMap)
			throws TalendException {
		globalMap.put("tJava_1_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception()
						.getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				/**
				 * [tJava_1 begin ] start
				 */

				ok_Hash.put("tJava_1", false);
				start_Hash.put("tJava_1", System.currentTimeMillis());

				talendStats_STATS.addMessage("begin", "tJava_1");
				talendStats_STATSProcess(globalMap);

				currentComponent = "tJava_1";

				int tos_count_tJava_1 = 0;

				class BytesLimit65535_tJava_1 {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_tJava_1().limitLog4jByte();

				String foo = "bar";
				String os = System.getProperty("os.name").toLowerCase();
				context.tableName = "dimPaymentRequestAllJobs";

				System.out.println("Operating System Is: " + os);
				System.out.println("Table Name Is: " + context.tableName);

				if (os.indexOf("win") >= 0) {
					context.filePath = System.getProperty("user.home")
							+ "/Logs";
				} else if (os.indexOf("mac os x") >= 0) {
					context.filePath = System.getProperty("user.home")
							+ "/Documents/Logs";
				} else {
					context.filePath = "/Talend/Jobs/Logs";
				}

				/**
				 * [tJava_1 begin ] stop
				 */

				/**
				 * [tJava_1 main ] start
				 */

				currentComponent = "tJava_1";

				tos_count_tJava_1++;

				/**
				 * [tJava_1 main ] stop
				 */

				/**
				 * [tJava_1 process_data_begin ] start
				 */

				currentComponent = "tJava_1";

				/**
				 * [tJava_1 process_data_begin ] stop
				 */

				/**
				 * [tJava_1 process_data_end ] start
				 */

				currentComponent = "tJava_1";

				/**
				 * [tJava_1 process_data_end ] stop
				 */

				/**
				 * [tJava_1 end ] start
				 */

				currentComponent = "tJava_1";

				ok_Hash.put("tJava_1", true);
				end_Hash.put("tJava_1", System.currentTimeMillis());

				talendStats_STATS.addMessage("end", "tJava_1",
						end_Hash.get("tJava_1") - start_Hash.get("tJava_1"));
				talendStats_STATSProcess(globalMap);

				/**
				 * [tJava_1 end ] stop
				 */
			}// end the resume

			if (resumeEntryMethodName == null || globalResumeTicket) {
				resumeUtil.addLog("CHECKPOINT",
						"CONNECTION:SUBJOB_OK:tJava_1:OnSubjobOk", "", Thread
								.currentThread().getId() + "", "", "", "", "",
						"");
			}

			if (execStat) {
				runStat.updateStatOnConnection("OnSubjobOk1", 0, "ok");
			}

			tRedirectOutput_1Process(globalMap);

		} catch (java.lang.Exception e) {

			TalendException te = new TalendException(e, currentComponent,
					globalMap);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			try {

				/**
				 * [tJava_1 finally ] start
				 */

				currentComponent = "tJava_1";

				/**
				 * [tJava_1 finally ] stop
				 */
			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("tJava_1_SUBPROCESS_STATE", 1);
	}

	public void tRedirectOutput_1Process(
			final java.util.Map<String, Object> globalMap)
			throws TalendException {
		globalMap.put("tRedirectOutput_1_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception()
						.getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				/**
				 * [tRedirectOutput_1 begin ] start
				 */

				ok_Hash.put("tRedirectOutput_1", false);
				start_Hash.put("tRedirectOutput_1", System.currentTimeMillis());

				currentComponent = "tRedirectOutput_1";

				int tos_count_tRedirectOutput_1 = 0;

				class BytesLimit65535_tRedirectOutput_1 {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_tRedirectOutput_1().limitLog4jByte();
				String fileOutName_tRedirectOutput_1 = (new java.io.File(
						context.filePath + "/" + jobName + "_console"))
						.getAbsolutePath().replace("\\", "/");
				java.io.File fileOut_tRedirectOutput_1 = new java.io.File(
						fileOutName_tRedirectOutput_1);
				System.setOut(new java.io.PrintStream(
						new java.io.FileOutputStream(fileOut_tRedirectOutput_1,
								true), true));
				System.out.println("Job Started at : "
						+ TalendDate.formatDate("yyyy-MM-dd HH:mm:ss",
								new Date(startTime)) + "");
				String fileErrName_tRedirectOutput_1 = (new java.io.File(
						context.filePath + "/" + jobName + "_error"))
						.getAbsolutePath().replace("\\", "/");
				java.io.File fileErr_tRedirectOutput_1 = new java.io.File(
						fileErrName_tRedirectOutput_1);
				System.setErr(new java.io.PrintStream(
						new java.io.FileOutputStream(fileErr_tRedirectOutput_1,
								true), true));
				System.err.println("Job Started at : "
						+ TalendDate.formatDate("yyyy-MM-dd HH:mm:ss",
								new Date(startTime)) + "");

				/**
				 * [tRedirectOutput_1 begin ] stop
				 */

				/**
				 * [tRedirectOutput_1 main ] start
				 */

				currentComponent = "tRedirectOutput_1";

				tos_count_tRedirectOutput_1++;

				/**
				 * [tRedirectOutput_1 main ] stop
				 */

				/**
				 * [tRedirectOutput_1 process_data_begin ] start
				 */

				currentComponent = "tRedirectOutput_1";

				/**
				 * [tRedirectOutput_1 process_data_begin ] stop
				 */

				/**
				 * [tRedirectOutput_1 process_data_end ] start
				 */

				currentComponent = "tRedirectOutput_1";

				/**
				 * [tRedirectOutput_1 process_data_end ] stop
				 */

				/**
				 * [tRedirectOutput_1 end ] start
				 */

				currentComponent = "tRedirectOutput_1";

				ok_Hash.put("tRedirectOutput_1", true);
				end_Hash.put("tRedirectOutput_1", System.currentTimeMillis());

				if (execStat) {
					runStat.updateStatOnConnection("OnComponentOk2", 0, "ok");
				}
				tRunJob_1Process(globalMap);

				/**
				 * [tRedirectOutput_1 end ] stop
				 */
			}// end the resume

		} catch (java.lang.Exception e) {

			TalendException te = new TalendException(e, currentComponent,
					globalMap);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			try {

				/**
				 * [tRedirectOutput_1 finally ] start
				 */

				currentComponent = "tRedirectOutput_1";

				/**
				 * [tRedirectOutput_1 finally ] stop
				 */
			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("tRedirectOutput_1_SUBPROCESS_STATE", 1);
	}

	public static class row11Struct implements
			routines.system.IPersistableRow<row11Struct> {
		final static byte[] commonByteArrayLock_GIFMIS_PaymentRequestAllJobs = new byte[0];
		static byte[] commonByteArray_GIFMIS_PaymentRequestAllJobs = new byte[0];

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_GIFMIS_PaymentRequestAllJobs) {

				try {

					int length = 0;

				}

				finally {
				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

			}

			finally {
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("]");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(row11Struct other) {

			int returnValue = -1;

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(),
						object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public static class row10Struct implements
			routines.system.IPersistableRow<row10Struct> {
		final static byte[] commonByteArrayLock_GIFMIS_PaymentRequestAllJobs = new byte[0];
		static byte[] commonByteArray_GIFMIS_PaymentRequestAllJobs = new byte[0];

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_GIFMIS_PaymentRequestAllJobs) {

				try {

					int length = 0;

				}

				finally {
				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

			}

			finally {
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("]");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(row10Struct other) {

			int returnValue = -1;

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(),
						object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public static class row9Struct implements
			routines.system.IPersistableRow<row9Struct> {
		final static byte[] commonByteArrayLock_GIFMIS_PaymentRequestAllJobs = new byte[0];
		static byte[] commonByteArray_GIFMIS_PaymentRequestAllJobs = new byte[0];

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_GIFMIS_PaymentRequestAllJobs) {

				try {

					int length = 0;

				}

				finally {
				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

			}

			finally {
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("]");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(row9Struct other) {

			int returnValue = -1;

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(),
						object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public static class row8Struct implements
			routines.system.IPersistableRow<row8Struct> {
		final static byte[] commonByteArrayLock_GIFMIS_PaymentRequestAllJobs = new byte[0];
		static byte[] commonByteArray_GIFMIS_PaymentRequestAllJobs = new byte[0];

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_GIFMIS_PaymentRequestAllJobs) {

				try {

					int length = 0;

				}

				finally {
				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

			}

			finally {
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("]");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(row8Struct other) {

			int returnValue = -1;

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(),
						object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public static class row7Struct implements
			routines.system.IPersistableRow<row7Struct> {
		final static byte[] commonByteArrayLock_GIFMIS_PaymentRequestAllJobs = new byte[0];
		static byte[] commonByteArray_GIFMIS_PaymentRequestAllJobs = new byte[0];

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_GIFMIS_PaymentRequestAllJobs) {

				try {

					int length = 0;

				}

				finally {
				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

			}

			finally {
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("]");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(row7Struct other) {

			int returnValue = -1;

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(),
						object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public static class row6Struct implements
			routines.system.IPersistableRow<row6Struct> {
		final static byte[] commonByteArrayLock_GIFMIS_PaymentRequestAllJobs = new byte[0];
		static byte[] commonByteArray_GIFMIS_PaymentRequestAllJobs = new byte[0];

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_GIFMIS_PaymentRequestAllJobs) {

				try {

					int length = 0;

				}

				finally {
				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

			}

			finally {
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("]");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(row6Struct other) {

			int returnValue = -1;

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(),
						object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public static class row5Struct implements
			routines.system.IPersistableRow<row5Struct> {
		final static byte[] commonByteArrayLock_GIFMIS_PaymentRequestAllJobs = new byte[0];
		static byte[] commonByteArray_GIFMIS_PaymentRequestAllJobs = new byte[0];

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_GIFMIS_PaymentRequestAllJobs) {

				try {

					int length = 0;

				}

				finally {
				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

			}

			finally {
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("]");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(row5Struct other) {

			int returnValue = -1;

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(),
						object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public static class row4Struct implements
			routines.system.IPersistableRow<row4Struct> {
		final static byte[] commonByteArrayLock_GIFMIS_PaymentRequestAllJobs = new byte[0];
		static byte[] commonByteArray_GIFMIS_PaymentRequestAllJobs = new byte[0];

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_GIFMIS_PaymentRequestAllJobs) {

				try {

					int length = 0;

				}

				finally {
				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

			}

			finally {
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("]");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(row4Struct other) {

			int returnValue = -1;

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(),
						object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public static class row3Struct implements
			routines.system.IPersistableRow<row3Struct> {
		final static byte[] commonByteArrayLock_GIFMIS_PaymentRequestAllJobs = new byte[0];
		static byte[] commonByteArray_GIFMIS_PaymentRequestAllJobs = new byte[0];

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_GIFMIS_PaymentRequestAllJobs) {

				try {

					int length = 0;

				}

				finally {
				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

			}

			finally {
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("]");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(row3Struct other) {

			int returnValue = -1;

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(),
						object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public static class row2Struct implements
			routines.system.IPersistableRow<row2Struct> {
		final static byte[] commonByteArrayLock_GIFMIS_PaymentRequestAllJobs = new byte[0];
		static byte[] commonByteArray_GIFMIS_PaymentRequestAllJobs = new byte[0];

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_GIFMIS_PaymentRequestAllJobs) {

				try {

					int length = 0;

				}

				finally {
				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

			}

			finally {
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("]");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(row2Struct other) {

			int returnValue = -1;

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(),
						object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public static class row1Struct implements
			routines.system.IPersistableRow<row1Struct> {
		final static byte[] commonByteArrayLock_GIFMIS_PaymentRequestAllJobs = new byte[0];
		static byte[] commonByteArray_GIFMIS_PaymentRequestAllJobs = new byte[0];

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_GIFMIS_PaymentRequestAllJobs) {

				try {

					int length = 0;

				}

				finally {
				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

			}

			finally {
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("]");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(row1Struct other) {

			int returnValue = -1;

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(),
						object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public void tRunJob_1Process(final java.util.Map<String, Object> globalMap)
			throws TalendException {
		globalMap.put("tRunJob_1_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception()
						.getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				row1Struct row1 = new row1Struct();
				row2Struct row2 = new row2Struct();
				row3Struct row3 = new row3Struct();
				row4Struct row4 = new row4Struct();
				row5Struct row5 = new row5Struct();
				row6Struct row6 = new row6Struct();
				row7Struct row7 = new row7Struct();
				row8Struct row8 = new row8Struct();
				row9Struct row9 = new row9Struct();
				row10Struct row10 = new row10Struct();
				row11Struct row11 = new row11Struct();

				/**
				 * [tRunJob_12 begin ] start
				 */

				ok_Hash.put("tRunJob_12", false);
				start_Hash.put("tRunJob_12", System.currentTimeMillis());

				currentComponent = "tRunJob_12";

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null) {

						runStat.updateStatOnConnection("row11" + iterateId, 0,
								0);

					}
				}

				int tos_count_tRunJob_12 = 0;

				class BytesLimit65535_tRunJob_12 {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_tRunJob_12().limitLog4jByte();

				/**
				 * [tRunJob_12 begin ] stop
				 */

				/**
				 * [tRunJob_11 begin ] start
				 */

				ok_Hash.put("tRunJob_11", false);
				start_Hash.put("tRunJob_11", System.currentTimeMillis());

				currentComponent = "tRunJob_11";

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null) {

						runStat.updateStatOnConnection("row10" + iterateId, 0,
								0);

					}
				}

				int tos_count_tRunJob_11 = 0;

				class BytesLimit65535_tRunJob_11 {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_tRunJob_11().limitLog4jByte();

				/**
				 * [tRunJob_11 begin ] stop
				 */

				/**
				 * [tRunJob_10 begin ] start
				 */

				ok_Hash.put("tRunJob_10", false);
				start_Hash.put("tRunJob_10", System.currentTimeMillis());

				currentComponent = "tRunJob_10";

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null) {

						runStat.updateStatOnConnection("row9" + iterateId, 0, 0);

					}
				}

				int tos_count_tRunJob_10 = 0;

				class BytesLimit65535_tRunJob_10 {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_tRunJob_10().limitLog4jByte();

				/**
				 * [tRunJob_10 begin ] stop
				 */

				/**
				 * [tRunJob_9 begin ] start
				 */

				ok_Hash.put("tRunJob_9", false);
				start_Hash.put("tRunJob_9", System.currentTimeMillis());

				currentComponent = "tRunJob_9";

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null) {

						runStat.updateStatOnConnection("row8" + iterateId, 0, 0);

					}
				}

				int tos_count_tRunJob_9 = 0;

				class BytesLimit65535_tRunJob_9 {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_tRunJob_9().limitLog4jByte();

				/**
				 * [tRunJob_9 begin ] stop
				 */

				/**
				 * [tRunJob_8 begin ] start
				 */

				ok_Hash.put("tRunJob_8", false);
				start_Hash.put("tRunJob_8", System.currentTimeMillis());

				currentComponent = "tRunJob_8";

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null) {

						runStat.updateStatOnConnection("row7" + iterateId, 0, 0);

					}
				}

				int tos_count_tRunJob_8 = 0;

				class BytesLimit65535_tRunJob_8 {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_tRunJob_8().limitLog4jByte();

				/**
				 * [tRunJob_8 begin ] stop
				 */

				/**
				 * [tRunJob_7 begin ] start
				 */

				ok_Hash.put("tRunJob_7", false);
				start_Hash.put("tRunJob_7", System.currentTimeMillis());

				currentComponent = "tRunJob_7";

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null) {

						runStat.updateStatOnConnection("row6" + iterateId, 0, 0);

					}
				}

				int tos_count_tRunJob_7 = 0;

				class BytesLimit65535_tRunJob_7 {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_tRunJob_7().limitLog4jByte();

				/**
				 * [tRunJob_7 begin ] stop
				 */

				/**
				 * [tRunJob_6 begin ] start
				 */

				ok_Hash.put("tRunJob_6", false);
				start_Hash.put("tRunJob_6", System.currentTimeMillis());

				currentComponent = "tRunJob_6";

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null) {

						runStat.updateStatOnConnection("row5" + iterateId, 0, 0);

					}
				}

				int tos_count_tRunJob_6 = 0;

				class BytesLimit65535_tRunJob_6 {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_tRunJob_6().limitLog4jByte();

				/**
				 * [tRunJob_6 begin ] stop
				 */

				/**
				 * [tRunJob_5 begin ] start
				 */

				ok_Hash.put("tRunJob_5", false);
				start_Hash.put("tRunJob_5", System.currentTimeMillis());

				currentComponent = "tRunJob_5";

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null) {

						runStat.updateStatOnConnection("row4" + iterateId, 0, 0);

					}
				}

				int tos_count_tRunJob_5 = 0;

				class BytesLimit65535_tRunJob_5 {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_tRunJob_5().limitLog4jByte();

				/**
				 * [tRunJob_5 begin ] stop
				 */

				/**
				 * [tRunJob_4 begin ] start
				 */

				ok_Hash.put("tRunJob_4", false);
				start_Hash.put("tRunJob_4", System.currentTimeMillis());

				currentComponent = "tRunJob_4";

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null) {

						runStat.updateStatOnConnection("row3" + iterateId, 0, 0);

					}
				}

				int tos_count_tRunJob_4 = 0;

				class BytesLimit65535_tRunJob_4 {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_tRunJob_4().limitLog4jByte();

				/**
				 * [tRunJob_4 begin ] stop
				 */

				/**
				 * [tRunJob_3 begin ] start
				 */

				ok_Hash.put("tRunJob_3", false);
				start_Hash.put("tRunJob_3", System.currentTimeMillis());

				currentComponent = "tRunJob_3";

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null) {

						runStat.updateStatOnConnection("row2" + iterateId, 0, 0);

					}
				}

				int tos_count_tRunJob_3 = 0;

				class BytesLimit65535_tRunJob_3 {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_tRunJob_3().limitLog4jByte();

				/**
				 * [tRunJob_3 begin ] stop
				 */

				/**
				 * [tRunJob_2 begin ] start
				 */

				ok_Hash.put("tRunJob_2", false);
				start_Hash.put("tRunJob_2", System.currentTimeMillis());

				currentComponent = "tRunJob_2";

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null) {

						runStat.updateStatOnConnection("row1" + iterateId, 0, 0);

					}
				}

				int tos_count_tRunJob_2 = 0;

				class BytesLimit65535_tRunJob_2 {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_tRunJob_2().limitLog4jByte();

				/**
				 * [tRunJob_2 begin ] stop
				 */

				/**
				 * [tRunJob_1 begin ] start
				 */

				ok_Hash.put("tRunJob_1", false);
				start_Hash.put("tRunJob_1", System.currentTimeMillis());

				currentComponent = "tRunJob_1";

				int tos_count_tRunJob_1 = 0;

				class BytesLimit65535_tRunJob_1 {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_tRunJob_1().limitLog4jByte();

				/**
				 * [tRunJob_1 begin ] stop
				 */

				/**
				 * [tRunJob_1 main ] start
				 */

				currentComponent = "tRunJob_1";

				java.util.List<String> paraList_tRunJob_1 = new java.util.ArrayList<String>();

				paraList_tRunJob_1.add("--father_pid=" + pid);

				paraList_tRunJob_1.add("--root_pid=" + rootPid);

				paraList_tRunJob_1.add("--father_node=tRunJob_1");

				paraList_tRunJob_1.add("--context=PR");

				// for feature:10589

				paraList_tRunJob_1.add("--stat_port=" + portStats);

				if (resuming_logs_dir_path != null) {
					paraList_tRunJob_1.add("--resuming_logs_dir_path="
							+ resuming_logs_dir_path);
				}
				String childResumePath_tRunJob_1 = ResumeUtil
						.getChildJobCheckPointPath(resuming_checkpoint_path);
				String tRunJobName_tRunJob_1 = ResumeUtil
						.getRighttRunJob(resuming_checkpoint_path);
				if ("tRunJob_1".equals(tRunJobName_tRunJob_1)
						&& childResumePath_tRunJob_1 != null) {
					paraList_tRunJob_1
							.add("--resuming_checkpoint_path="
									+ ResumeUtil
											.getChildJobCheckPointPath(resuming_checkpoint_path));
				}
				paraList_tRunJob_1.add("--parent_part_launcher=JOB:" + jobName
						+ "/NODE:tRunJob_1");

				java.util.Map<String, Object> parentContextMap_tRunJob_1 = new java.util.HashMap<String, Object>();

				Object obj_tRunJob_1 = null;

				gifmis.dimpaymentrequestaie_0_1.dimPaymentRequestAIE childJob_tRunJob_1 = new gifmis.dimpaymentrequestaie_0_1.dimPaymentRequestAIE();
				// pass DataSources
				java.util.Map<String, routines.system.TalendDataSource> talendDataSources_tRunJob_1 = (java.util.Map<String, routines.system.TalendDataSource>) globalMap
						.get(KEY_DB_DATASOURCES);
				if (null != talendDataSources_tRunJob_1) {
					java.util.Map<String, javax.sql.DataSource> dataSources_tRunJob_1 = new java.util.HashMap<String, javax.sql.DataSource>();
					for (java.util.Map.Entry<String, routines.system.TalendDataSource> talendDataSourceEntry_tRunJob_1 : talendDataSources_tRunJob_1
							.entrySet()) {
						dataSources_tRunJob_1.put(
								talendDataSourceEntry_tRunJob_1.getKey(),
								talendDataSourceEntry_tRunJob_1.getValue()
										.getRawDataSource());
					}
					childJob_tRunJob_1.setDataSources(dataSources_tRunJob_1);
				}

				childJob_tRunJob_1.parentContextMap = parentContextMap_tRunJob_1;

				String[][] childReturn_tRunJob_1 = childJob_tRunJob_1
						.runJob((String[]) paraList_tRunJob_1
								.toArray(new String[paraList_tRunJob_1.size()]));

				errorCode = childJob_tRunJob_1.getErrorCode();

				if (childJob_tRunJob_1.getErrorCode() == null) {
					globalMap.put(
							"tRunJob_1_CHILD_RETURN_CODE",
							childJob_tRunJob_1.getStatus() != null
									&& ("failure").equals(childJob_tRunJob_1
											.getStatus()) ? 1 : 0);
				} else {
					globalMap.put("tRunJob_1_CHILD_RETURN_CODE",
							childJob_tRunJob_1.getErrorCode());
				}
				if (childJob_tRunJob_1.getExceptionStackTrace() != null) {
					globalMap.put("tRunJob_1_CHILD_EXCEPTION_STACKTRACE",
							childJob_tRunJob_1.getExceptionStackTrace());
				}

				if (childJob_tRunJob_1.getErrorCode() != null
						|| ("failure").equals(childJob_tRunJob_1.getStatus())) {
					throw new RuntimeException("Child job running failed.\n"
							+ childJob_tRunJob_1.getException().getClass()
									.getName() + ": "
							+ childJob_tRunJob_1.getException().getMessage());
				}

				for (String[] item_tRunJob_1 : childReturn_tRunJob_1) {
					if (childJob_tRunJob_1.hastBufferOutputComponent() || true) {

					}

					tos_count_tRunJob_1++;

					/**
					 * [tRunJob_1 main ] stop
					 */

					/**
					 * [tRunJob_1 process_data_begin ] start
					 */

					currentComponent = "tRunJob_1";

					/**
					 * [tRunJob_1 process_data_begin ] stop
					 */

					/**
					 * [tRunJob_2 main ] start
					 */

					currentComponent = "tRunJob_2";

					// row1
					// row1

					if (execStat) {
						runStat.updateStatOnConnection("row1" + iterateId, 1, 1);
					}

					java.util.List<String> paraList_tRunJob_2 = new java.util.ArrayList<String>();

					paraList_tRunJob_2.add("--father_pid=" + pid);

					paraList_tRunJob_2.add("--root_pid=" + rootPid);

					paraList_tRunJob_2.add("--father_node=tRunJob_2");

					paraList_tRunJob_2.add("--context=PR");

					// for feature:10589

					paraList_tRunJob_2.add("--stat_port=" + portStats);

					if (resuming_logs_dir_path != null) {
						paraList_tRunJob_2.add("--resuming_logs_dir_path="
								+ resuming_logs_dir_path);
					}
					String childResumePath_tRunJob_2 = ResumeUtil
							.getChildJobCheckPointPath(resuming_checkpoint_path);
					String tRunJobName_tRunJob_2 = ResumeUtil
							.getRighttRunJob(resuming_checkpoint_path);
					if ("tRunJob_2".equals(tRunJobName_tRunJob_2)
							&& childResumePath_tRunJob_2 != null) {
						paraList_tRunJob_2
								.add("--resuming_checkpoint_path="
										+ ResumeUtil
												.getChildJobCheckPointPath(resuming_checkpoint_path));
					}
					paraList_tRunJob_2.add("--parent_part_launcher=JOB:"
							+ jobName + "/NODE:tRunJob_2");

					java.util.Map<String, Object> parentContextMap_tRunJob_2 = new java.util.HashMap<String, Object>();

					Object obj_tRunJob_2 = null;

					gifmis.dimpaymentrequestbudget_0_1.dimPaymentRequestBudget childJob_tRunJob_2 = new gifmis.dimpaymentrequestbudget_0_1.dimPaymentRequestBudget();
					// pass DataSources
					java.util.Map<String, routines.system.TalendDataSource> talendDataSources_tRunJob_2 = (java.util.Map<String, routines.system.TalendDataSource>) globalMap
							.get(KEY_DB_DATASOURCES);
					if (null != talendDataSources_tRunJob_2) {
						java.util.Map<String, javax.sql.DataSource> dataSources_tRunJob_2 = new java.util.HashMap<String, javax.sql.DataSource>();
						for (java.util.Map.Entry<String, routines.system.TalendDataSource> talendDataSourceEntry_tRunJob_2 : talendDataSources_tRunJob_2
								.entrySet()) {
							dataSources_tRunJob_2.put(
									talendDataSourceEntry_tRunJob_2.getKey(),
									talendDataSourceEntry_tRunJob_2.getValue()
											.getRawDataSource());
						}
						childJob_tRunJob_2
								.setDataSources(dataSources_tRunJob_2);
					}

					childJob_tRunJob_2.parentContextMap = parentContextMap_tRunJob_2;

					String[][] childReturn_tRunJob_2 = childJob_tRunJob_2
							.runJob((String[]) paraList_tRunJob_2
									.toArray(new String[paraList_tRunJob_2
											.size()]));

					errorCode = childJob_tRunJob_2.getErrorCode();

					if (childJob_tRunJob_2.getErrorCode() == null) {
						globalMap.put(
								"tRunJob_2_CHILD_RETURN_CODE",
								childJob_tRunJob_2.getStatus() != null
										&& ("failure")
												.equals(childJob_tRunJob_2
														.getStatus()) ? 1 : 0);
					} else {
						globalMap.put("tRunJob_2_CHILD_RETURN_CODE",
								childJob_tRunJob_2.getErrorCode());
					}
					if (childJob_tRunJob_2.getExceptionStackTrace() != null) {
						globalMap.put("tRunJob_2_CHILD_EXCEPTION_STACKTRACE",
								childJob_tRunJob_2.getExceptionStackTrace());
					}

					if (childJob_tRunJob_2.getErrorCode() != null
							|| ("failure").equals(childJob_tRunJob_2
									.getStatus())) {
						throw new RuntimeException(
								"Child job running failed.\n"
										+ childJob_tRunJob_2.getException()
												.getClass().getName()
										+ ": "
										+ childJob_tRunJob_2.getException()
												.getMessage());
					}

					tos_count_tRunJob_2++;

					/**
					 * [tRunJob_2 main ] stop
					 */

					/**
					 * [tRunJob_2 process_data_begin ] start
					 */

					currentComponent = "tRunJob_2";

					/**
					 * [tRunJob_2 process_data_begin ] stop
					 */

					/**
					 * [tRunJob_3 main ] start
					 */

					currentComponent = "tRunJob_3";

					// row2
					// row2

					if (execStat) {
						runStat.updateStatOnConnection("row2" + iterateId, 1, 1);
					}

					java.util.List<String> paraList_tRunJob_3 = new java.util.ArrayList<String>();

					paraList_tRunJob_3.add("--father_pid=" + pid);

					paraList_tRunJob_3.add("--root_pid=" + rootPid);

					paraList_tRunJob_3.add("--father_node=tRunJob_3");

					paraList_tRunJob_3.add("--context=PR");

					// for feature:10589

					paraList_tRunJob_3.add("--stat_port=" + portStats);

					if (resuming_logs_dir_path != null) {
						paraList_tRunJob_3.add("--resuming_logs_dir_path="
								+ resuming_logs_dir_path);
					}
					String childResumePath_tRunJob_3 = ResumeUtil
							.getChildJobCheckPointPath(resuming_checkpoint_path);
					String tRunJobName_tRunJob_3 = ResumeUtil
							.getRighttRunJob(resuming_checkpoint_path);
					if ("tRunJob_3".equals(tRunJobName_tRunJob_3)
							&& childResumePath_tRunJob_3 != null) {
						paraList_tRunJob_3
								.add("--resuming_checkpoint_path="
										+ ResumeUtil
												.getChildJobCheckPointPath(resuming_checkpoint_path));
					}
					paraList_tRunJob_3.add("--parent_part_launcher=JOB:"
							+ jobName + "/NODE:tRunJob_3");

					java.util.Map<String, Object> parentContextMap_tRunJob_3 = new java.util.HashMap<String, Object>();

					Object obj_tRunJob_3 = null;

					gifmis.dimpaymentrequestexpendituresbrs_0_1.dimPaymentRequestExpendituresBRS childJob_tRunJob_3 = new gifmis.dimpaymentrequestexpendituresbrs_0_1.dimPaymentRequestExpendituresBRS();
					// pass DataSources
					java.util.Map<String, routines.system.TalendDataSource> talendDataSources_tRunJob_3 = (java.util.Map<String, routines.system.TalendDataSource>) globalMap
							.get(KEY_DB_DATASOURCES);
					if (null != talendDataSources_tRunJob_3) {
						java.util.Map<String, javax.sql.DataSource> dataSources_tRunJob_3 = new java.util.HashMap<String, javax.sql.DataSource>();
						for (java.util.Map.Entry<String, routines.system.TalendDataSource> talendDataSourceEntry_tRunJob_3 : talendDataSources_tRunJob_3
								.entrySet()) {
							dataSources_tRunJob_3.put(
									talendDataSourceEntry_tRunJob_3.getKey(),
									talendDataSourceEntry_tRunJob_3.getValue()
											.getRawDataSource());
						}
						childJob_tRunJob_3
								.setDataSources(dataSources_tRunJob_3);
					}

					childJob_tRunJob_3.parentContextMap = parentContextMap_tRunJob_3;

					String[][] childReturn_tRunJob_3 = childJob_tRunJob_3
							.runJob((String[]) paraList_tRunJob_3
									.toArray(new String[paraList_tRunJob_3
											.size()]));

					errorCode = childJob_tRunJob_3.getErrorCode();

					if (childJob_tRunJob_3.getErrorCode() == null) {
						globalMap.put(
								"tRunJob_3_CHILD_RETURN_CODE",
								childJob_tRunJob_3.getStatus() != null
										&& ("failure")
												.equals(childJob_tRunJob_3
														.getStatus()) ? 1 : 0);
					} else {
						globalMap.put("tRunJob_3_CHILD_RETURN_CODE",
								childJob_tRunJob_3.getErrorCode());
					}
					if (childJob_tRunJob_3.getExceptionStackTrace() != null) {
						globalMap.put("tRunJob_3_CHILD_EXCEPTION_STACKTRACE",
								childJob_tRunJob_3.getExceptionStackTrace());
					}

					if (childJob_tRunJob_3.getErrorCode() != null
							|| ("failure").equals(childJob_tRunJob_3
									.getStatus())) {
						throw new RuntimeException(
								"Child job running failed.\n"
										+ childJob_tRunJob_3.getException()
												.getClass().getName()
										+ ": "
										+ childJob_tRunJob_3.getException()
												.getMessage());
					}

					tos_count_tRunJob_3++;

					/**
					 * [tRunJob_3 main ] stop
					 */

					/**
					 * [tRunJob_3 process_data_begin ] start
					 */

					currentComponent = "tRunJob_3";

					/**
					 * [tRunJob_3 process_data_begin ] stop
					 */

					/**
					 * [tRunJob_4 main ] start
					 */

					currentComponent = "tRunJob_4";

					// row3
					// row3

					if (execStat) {
						runStat.updateStatOnConnection("row3" + iterateId, 1, 1);
					}

					java.util.List<String> paraList_tRunJob_4 = new java.util.ArrayList<String>();

					paraList_tRunJob_4.add("--father_pid=" + pid);

					paraList_tRunJob_4.add("--root_pid=" + rootPid);

					paraList_tRunJob_4.add("--father_node=tRunJob_4");

					paraList_tRunJob_4.add("--context=PR");

					// for feature:10589

					paraList_tRunJob_4.add("--stat_port=" + portStats);

					if (resuming_logs_dir_path != null) {
						paraList_tRunJob_4.add("--resuming_logs_dir_path="
								+ resuming_logs_dir_path);
					}
					String childResumePath_tRunJob_4 = ResumeUtil
							.getChildJobCheckPointPath(resuming_checkpoint_path);
					String tRunJobName_tRunJob_4 = ResumeUtil
							.getRighttRunJob(resuming_checkpoint_path);
					if ("tRunJob_4".equals(tRunJobName_tRunJob_4)
							&& childResumePath_tRunJob_4 != null) {
						paraList_tRunJob_4
								.add("--resuming_checkpoint_path="
										+ ResumeUtil
												.getChildJobCheckPointPath(resuming_checkpoint_path));
					}
					paraList_tRunJob_4.add("--parent_part_launcher=JOB:"
							+ jobName + "/NODE:tRunJob_4");

					java.util.Map<String, Object> parentContextMap_tRunJob_4 = new java.util.HashMap<String, Object>();

					Object obj_tRunJob_4 = null;

					gifmis.dimpaymentrequestexpendituresbrsbl_0_1.dimPaymentRequestExpendituresBRSBL childJob_tRunJob_4 = new gifmis.dimpaymentrequestexpendituresbrsbl_0_1.dimPaymentRequestExpendituresBRSBL();
					// pass DataSources
					java.util.Map<String, routines.system.TalendDataSource> talendDataSources_tRunJob_4 = (java.util.Map<String, routines.system.TalendDataSource>) globalMap
							.get(KEY_DB_DATASOURCES);
					if (null != talendDataSources_tRunJob_4) {
						java.util.Map<String, javax.sql.DataSource> dataSources_tRunJob_4 = new java.util.HashMap<String, javax.sql.DataSource>();
						for (java.util.Map.Entry<String, routines.system.TalendDataSource> talendDataSourceEntry_tRunJob_4 : talendDataSources_tRunJob_4
								.entrySet()) {
							dataSources_tRunJob_4.put(
									talendDataSourceEntry_tRunJob_4.getKey(),
									talendDataSourceEntry_tRunJob_4.getValue()
											.getRawDataSource());
						}
						childJob_tRunJob_4
								.setDataSources(dataSources_tRunJob_4);
					}

					childJob_tRunJob_4.parentContextMap = parentContextMap_tRunJob_4;

					String[][] childReturn_tRunJob_4 = childJob_tRunJob_4
							.runJob((String[]) paraList_tRunJob_4
									.toArray(new String[paraList_tRunJob_4
											.size()]));

					errorCode = childJob_tRunJob_4.getErrorCode();

					if (childJob_tRunJob_4.getErrorCode() == null) {
						globalMap.put(
								"tRunJob_4_CHILD_RETURN_CODE",
								childJob_tRunJob_4.getStatus() != null
										&& ("failure")
												.equals(childJob_tRunJob_4
														.getStatus()) ? 1 : 0);
					} else {
						globalMap.put("tRunJob_4_CHILD_RETURN_CODE",
								childJob_tRunJob_4.getErrorCode());
					}
					if (childJob_tRunJob_4.getExceptionStackTrace() != null) {
						globalMap.put("tRunJob_4_CHILD_EXCEPTION_STACKTRACE",
								childJob_tRunJob_4.getExceptionStackTrace());
					}

					if (childJob_tRunJob_4.getErrorCode() != null
							|| ("failure").equals(childJob_tRunJob_4
									.getStatus())) {
						throw new RuntimeException(
								"Child job running failed.\n"
										+ childJob_tRunJob_4.getException()
												.getClass().getName()
										+ ": "
										+ childJob_tRunJob_4.getException()
												.getMessage());
					}

					tos_count_tRunJob_4++;

					/**
					 * [tRunJob_4 main ] stop
					 */

					/**
					 * [tRunJob_4 process_data_begin ] start
					 */

					currentComponent = "tRunJob_4";

					/**
					 * [tRunJob_4 process_data_begin ] stop
					 */

					/**
					 * [tRunJob_5 main ] start
					 */

					currentComponent = "tRunJob_5";

					// row4
					// row4

					if (execStat) {
						runStat.updateStatOnConnection("row4" + iterateId, 1, 1);
					}

					java.util.List<String> paraList_tRunJob_5 = new java.util.ArrayList<String>();

					paraList_tRunJob_5.add("--father_pid=" + pid);

					paraList_tRunJob_5.add("--root_pid=" + rootPid);

					paraList_tRunJob_5.add("--father_node=tRunJob_5");

					paraList_tRunJob_5.add("--context=PR");

					// for feature:10589

					paraList_tRunJob_5.add("--stat_port=" + portStats);

					if (resuming_logs_dir_path != null) {
						paraList_tRunJob_5.add("--resuming_logs_dir_path="
								+ resuming_logs_dir_path);
					}
					String childResumePath_tRunJob_5 = ResumeUtil
							.getChildJobCheckPointPath(resuming_checkpoint_path);
					String tRunJobName_tRunJob_5 = ResumeUtil
							.getRighttRunJob(resuming_checkpoint_path);
					if ("tRunJob_5".equals(tRunJobName_tRunJob_5)
							&& childResumePath_tRunJob_5 != null) {
						paraList_tRunJob_5
								.add("--resuming_checkpoint_path="
										+ ResumeUtil
												.getChildJobCheckPointPath(resuming_checkpoint_path));
					}
					paraList_tRunJob_5.add("--parent_part_launcher=JOB:"
							+ jobName + "/NODE:tRunJob_5");

					java.util.Map<String, Object> parentContextMap_tRunJob_5 = new java.util.HashMap<String, Object>();

					Object obj_tRunJob_5 = null;

					gifmis.dimpaymentrequestexpenditurespilep_0_1.dimPaymentRequestExpendituresPILEP childJob_tRunJob_5 = new gifmis.dimpaymentrequestexpenditurespilep_0_1.dimPaymentRequestExpendituresPILEP();
					// pass DataSources
					java.util.Map<String, routines.system.TalendDataSource> talendDataSources_tRunJob_5 = (java.util.Map<String, routines.system.TalendDataSource>) globalMap
							.get(KEY_DB_DATASOURCES);
					if (null != talendDataSources_tRunJob_5) {
						java.util.Map<String, javax.sql.DataSource> dataSources_tRunJob_5 = new java.util.HashMap<String, javax.sql.DataSource>();
						for (java.util.Map.Entry<String, routines.system.TalendDataSource> talendDataSourceEntry_tRunJob_5 : talendDataSources_tRunJob_5
								.entrySet()) {
							dataSources_tRunJob_5.put(
									talendDataSourceEntry_tRunJob_5.getKey(),
									talendDataSourceEntry_tRunJob_5.getValue()
											.getRawDataSource());
						}
						childJob_tRunJob_5
								.setDataSources(dataSources_tRunJob_5);
					}

					childJob_tRunJob_5.parentContextMap = parentContextMap_tRunJob_5;

					String[][] childReturn_tRunJob_5 = childJob_tRunJob_5
							.runJob((String[]) paraList_tRunJob_5
									.toArray(new String[paraList_tRunJob_5
											.size()]));

					errorCode = childJob_tRunJob_5.getErrorCode();

					if (childJob_tRunJob_5.getErrorCode() == null) {
						globalMap.put(
								"tRunJob_5_CHILD_RETURN_CODE",
								childJob_tRunJob_5.getStatus() != null
										&& ("failure")
												.equals(childJob_tRunJob_5
														.getStatus()) ? 1 : 0);
					} else {
						globalMap.put("tRunJob_5_CHILD_RETURN_CODE",
								childJob_tRunJob_5.getErrorCode());
					}
					if (childJob_tRunJob_5.getExceptionStackTrace() != null) {
						globalMap.put("tRunJob_5_CHILD_EXCEPTION_STACKTRACE",
								childJob_tRunJob_5.getExceptionStackTrace());
					}

					if (childJob_tRunJob_5.getErrorCode() != null
							|| ("failure").equals(childJob_tRunJob_5
									.getStatus())) {
						throw new RuntimeException(
								"Child job running failed.\n"
										+ childJob_tRunJob_5.getException()
												.getClass().getName()
										+ ": "
										+ childJob_tRunJob_5.getException()
												.getMessage());
					}

					tos_count_tRunJob_5++;

					/**
					 * [tRunJob_5 main ] stop
					 */

					/**
					 * [tRunJob_5 process_data_begin ] start
					 */

					currentComponent = "tRunJob_5";

					/**
					 * [tRunJob_5 process_data_begin ] stop
					 */

					/**
					 * [tRunJob_6 main ] start
					 */

					currentComponent = "tRunJob_6";

					// row5
					// row5

					if (execStat) {
						runStat.updateStatOnConnection("row5" + iterateId, 1, 1);
					}

					java.util.List<String> paraList_tRunJob_6 = new java.util.ArrayList<String>();

					paraList_tRunJob_6.add("--father_pid=" + pid);

					paraList_tRunJob_6.add("--root_pid=" + rootPid);

					paraList_tRunJob_6.add("--father_node=tRunJob_6");

					paraList_tRunJob_6.add("--context=PR");

					// for feature:10589

					paraList_tRunJob_6.add("--stat_port=" + portStats);

					if (resuming_logs_dir_path != null) {
						paraList_tRunJob_6.add("--resuming_logs_dir_path="
								+ resuming_logs_dir_path);
					}
					String childResumePath_tRunJob_6 = ResumeUtil
							.getChildJobCheckPointPath(resuming_checkpoint_path);
					String tRunJobName_tRunJob_6 = ResumeUtil
							.getRighttRunJob(resuming_checkpoint_path);
					if ("tRunJob_6".equals(tRunJobName_tRunJob_6)
							&& childResumePath_tRunJob_6 != null) {
						paraList_tRunJob_6
								.add("--resuming_checkpoint_path="
										+ ResumeUtil
												.getChildJobCheckPointPath(resuming_checkpoint_path));
					}
					paraList_tRunJob_6.add("--parent_part_launcher=JOB:"
							+ jobName + "/NODE:tRunJob_6");

					java.util.Map<String, Object> parentContextMap_tRunJob_6 = new java.util.HashMap<String, Object>();

					Object obj_tRunJob_6 = null;

					gifmis.dimpaymentrequestexpenditurespo_0_1.dimPaymentRequestExpendituresPO childJob_tRunJob_6 = new gifmis.dimpaymentrequestexpenditurespo_0_1.dimPaymentRequestExpendituresPO();
					// pass DataSources
					java.util.Map<String, routines.system.TalendDataSource> talendDataSources_tRunJob_6 = (java.util.Map<String, routines.system.TalendDataSource>) globalMap
							.get(KEY_DB_DATASOURCES);
					if (null != talendDataSources_tRunJob_6) {
						java.util.Map<String, javax.sql.DataSource> dataSources_tRunJob_6 = new java.util.HashMap<String, javax.sql.DataSource>();
						for (java.util.Map.Entry<String, routines.system.TalendDataSource> talendDataSourceEntry_tRunJob_6 : talendDataSources_tRunJob_6
								.entrySet()) {
							dataSources_tRunJob_6.put(
									talendDataSourceEntry_tRunJob_6.getKey(),
									talendDataSourceEntry_tRunJob_6.getValue()
											.getRawDataSource());
						}
						childJob_tRunJob_6
								.setDataSources(dataSources_tRunJob_6);
					}

					childJob_tRunJob_6.parentContextMap = parentContextMap_tRunJob_6;

					String[][] childReturn_tRunJob_6 = childJob_tRunJob_6
							.runJob((String[]) paraList_tRunJob_6
									.toArray(new String[paraList_tRunJob_6
											.size()]));

					errorCode = childJob_tRunJob_6.getErrorCode();

					if (childJob_tRunJob_6.getErrorCode() == null) {
						globalMap.put(
								"tRunJob_6_CHILD_RETURN_CODE",
								childJob_tRunJob_6.getStatus() != null
										&& ("failure")
												.equals(childJob_tRunJob_6
														.getStatus()) ? 1 : 0);
					} else {
						globalMap.put("tRunJob_6_CHILD_RETURN_CODE",
								childJob_tRunJob_6.getErrorCode());
					}
					if (childJob_tRunJob_6.getExceptionStackTrace() != null) {
						globalMap.put("tRunJob_6_CHILD_EXCEPTION_STACKTRACE",
								childJob_tRunJob_6.getExceptionStackTrace());
					}

					if (childJob_tRunJob_6.getErrorCode() != null
							|| ("failure").equals(childJob_tRunJob_6
									.getStatus())) {
						throw new RuntimeException(
								"Child job running failed.\n"
										+ childJob_tRunJob_6.getException()
												.getClass().getName()
										+ ": "
										+ childJob_tRunJob_6.getException()
												.getMessage());
					}

					tos_count_tRunJob_6++;

					/**
					 * [tRunJob_6 main ] stop
					 */

					/**
					 * [tRunJob_6 process_data_begin ] start
					 */

					currentComponent = "tRunJob_6";

					/**
					 * [tRunJob_6 process_data_begin ] stop
					 */

					/**
					 * [tRunJob_7 main ] start
					 */

					currentComponent = "tRunJob_7";

					// row6
					// row6

					if (execStat) {
						runStat.updateStatOnConnection("row6" + iterateId, 1, 1);
					}

					java.util.List<String> paraList_tRunJob_7 = new java.util.ArrayList<String>();

					paraList_tRunJob_7.add("--father_pid=" + pid);

					paraList_tRunJob_7.add("--root_pid=" + rootPid);

					paraList_tRunJob_7.add("--father_node=tRunJob_7");

					paraList_tRunJob_7.add("--context=PR");

					// for feature:10589

					paraList_tRunJob_7.add("--stat_port=" + portStats);

					if (resuming_logs_dir_path != null) {
						paraList_tRunJob_7.add("--resuming_logs_dir_path="
								+ resuming_logs_dir_path);
					}
					String childResumePath_tRunJob_7 = ResumeUtil
							.getChildJobCheckPointPath(resuming_checkpoint_path);
					String tRunJobName_tRunJob_7 = ResumeUtil
							.getRighttRunJob(resuming_checkpoint_path);
					if ("tRunJob_7".equals(tRunJobName_tRunJob_7)
							&& childResumePath_tRunJob_7 != null) {
						paraList_tRunJob_7
								.add("--resuming_checkpoint_path="
										+ ResumeUtil
												.getChildJobCheckPointPath(resuming_checkpoint_path));
					}
					paraList_tRunJob_7.add("--parent_part_launcher=JOB:"
							+ jobName + "/NODE:tRunJob_7");

					java.util.Map<String, Object> parentContextMap_tRunJob_7 = new java.util.HashMap<String, Object>();

					Object obj_tRunJob_7 = null;

					gifmis.dimpaymentrequestreleasesbrs_0_1.dimPaymentRequestReleasesBRS childJob_tRunJob_7 = new gifmis.dimpaymentrequestreleasesbrs_0_1.dimPaymentRequestReleasesBRS();
					// pass DataSources
					java.util.Map<String, routines.system.TalendDataSource> talendDataSources_tRunJob_7 = (java.util.Map<String, routines.system.TalendDataSource>) globalMap
							.get(KEY_DB_DATASOURCES);
					if (null != talendDataSources_tRunJob_7) {
						java.util.Map<String, javax.sql.DataSource> dataSources_tRunJob_7 = new java.util.HashMap<String, javax.sql.DataSource>();
						for (java.util.Map.Entry<String, routines.system.TalendDataSource> talendDataSourceEntry_tRunJob_7 : talendDataSources_tRunJob_7
								.entrySet()) {
							dataSources_tRunJob_7.put(
									talendDataSourceEntry_tRunJob_7.getKey(),
									talendDataSourceEntry_tRunJob_7.getValue()
											.getRawDataSource());
						}
						childJob_tRunJob_7
								.setDataSources(dataSources_tRunJob_7);
					}

					childJob_tRunJob_7.parentContextMap = parentContextMap_tRunJob_7;

					String[][] childReturn_tRunJob_7 = childJob_tRunJob_7
							.runJob((String[]) paraList_tRunJob_7
									.toArray(new String[paraList_tRunJob_7
											.size()]));

					errorCode = childJob_tRunJob_7.getErrorCode();

					if (childJob_tRunJob_7.getErrorCode() == null) {
						globalMap.put(
								"tRunJob_7_CHILD_RETURN_CODE",
								childJob_tRunJob_7.getStatus() != null
										&& ("failure")
												.equals(childJob_tRunJob_7
														.getStatus()) ? 1 : 0);
					} else {
						globalMap.put("tRunJob_7_CHILD_RETURN_CODE",
								childJob_tRunJob_7.getErrorCode());
					}
					if (childJob_tRunJob_7.getExceptionStackTrace() != null) {
						globalMap.put("tRunJob_7_CHILD_EXCEPTION_STACKTRACE",
								childJob_tRunJob_7.getExceptionStackTrace());
					}

					if (childJob_tRunJob_7.getErrorCode() != null
							|| ("failure").equals(childJob_tRunJob_7
									.getStatus())) {
						throw new RuntimeException(
								"Child job running failed.\n"
										+ childJob_tRunJob_7.getException()
												.getClass().getName()
										+ ": "
										+ childJob_tRunJob_7.getException()
												.getMessage());
					}

					tos_count_tRunJob_7++;

					/**
					 * [tRunJob_7 main ] stop
					 */

					/**
					 * [tRunJob_7 process_data_begin ] start
					 */

					currentComponent = "tRunJob_7";

					/**
					 * [tRunJob_7 process_data_begin ] stop
					 */

					/**
					 * [tRunJob_8 main ] start
					 */

					currentComponent = "tRunJob_8";

					// row7
					// row7

					if (execStat) {
						runStat.updateStatOnConnection("row7" + iterateId, 1, 1);
					}

					java.util.List<String> paraList_tRunJob_8 = new java.util.ArrayList<String>();

					paraList_tRunJob_8.add("--father_pid=" + pid);

					paraList_tRunJob_8.add("--root_pid=" + rootPid);

					paraList_tRunJob_8.add("--father_node=tRunJob_8");

					paraList_tRunJob_8.add("--context=PR");

					// for feature:10589

					paraList_tRunJob_8.add("--stat_port=" + portStats);

					if (resuming_logs_dir_path != null) {
						paraList_tRunJob_8.add("--resuming_logs_dir_path="
								+ resuming_logs_dir_path);
					}
					String childResumePath_tRunJob_8 = ResumeUtil
							.getChildJobCheckPointPath(resuming_checkpoint_path);
					String tRunJobName_tRunJob_8 = ResumeUtil
							.getRighttRunJob(resuming_checkpoint_path);
					if ("tRunJob_8".equals(tRunJobName_tRunJob_8)
							&& childResumePath_tRunJob_8 != null) {
						paraList_tRunJob_8
								.add("--resuming_checkpoint_path="
										+ ResumeUtil
												.getChildJobCheckPointPath(resuming_checkpoint_path));
					}
					paraList_tRunJob_8.add("--parent_part_launcher=JOB:"
							+ jobName + "/NODE:tRunJob_8");

					java.util.Map<String, Object> parentContextMap_tRunJob_8 = new java.util.HashMap<String, Object>();

					Object obj_tRunJob_8 = null;

					gifmis.dimpaymentrequestreleasesbrsrf_0_1.dimPaymentRequestReleasesBRSRF childJob_tRunJob_8 = new gifmis.dimpaymentrequestreleasesbrsrf_0_1.dimPaymentRequestReleasesBRSRF();
					// pass DataSources
					java.util.Map<String, routines.system.TalendDataSource> talendDataSources_tRunJob_8 = (java.util.Map<String, routines.system.TalendDataSource>) globalMap
							.get(KEY_DB_DATASOURCES);
					if (null != talendDataSources_tRunJob_8) {
						java.util.Map<String, javax.sql.DataSource> dataSources_tRunJob_8 = new java.util.HashMap<String, javax.sql.DataSource>();
						for (java.util.Map.Entry<String, routines.system.TalendDataSource> talendDataSourceEntry_tRunJob_8 : talendDataSources_tRunJob_8
								.entrySet()) {
							dataSources_tRunJob_8.put(
									talendDataSourceEntry_tRunJob_8.getKey(),
									talendDataSourceEntry_tRunJob_8.getValue()
											.getRawDataSource());
						}
						childJob_tRunJob_8
								.setDataSources(dataSources_tRunJob_8);
					}

					childJob_tRunJob_8.parentContextMap = parentContextMap_tRunJob_8;

					String[][] childReturn_tRunJob_8 = childJob_tRunJob_8
							.runJob((String[]) paraList_tRunJob_8
									.toArray(new String[paraList_tRunJob_8
											.size()]));

					errorCode = childJob_tRunJob_8.getErrorCode();

					if (childJob_tRunJob_8.getErrorCode() == null) {
						globalMap.put(
								"tRunJob_8_CHILD_RETURN_CODE",
								childJob_tRunJob_8.getStatus() != null
										&& ("failure")
												.equals(childJob_tRunJob_8
														.getStatus()) ? 1 : 0);
					} else {
						globalMap.put("tRunJob_8_CHILD_RETURN_CODE",
								childJob_tRunJob_8.getErrorCode());
					}
					if (childJob_tRunJob_8.getExceptionStackTrace() != null) {
						globalMap.put("tRunJob_8_CHILD_EXCEPTION_STACKTRACE",
								childJob_tRunJob_8.getExceptionStackTrace());
					}

					if (childJob_tRunJob_8.getErrorCode() != null
							|| ("failure").equals(childJob_tRunJob_8
									.getStatus())) {
						throw new RuntimeException(
								"Child job running failed.\n"
										+ childJob_tRunJob_8.getException()
												.getClass().getName()
										+ ": "
										+ childJob_tRunJob_8.getException()
												.getMessage());
					}

					tos_count_tRunJob_8++;

					/**
					 * [tRunJob_8 main ] stop
					 */

					/**
					 * [tRunJob_8 process_data_begin ] start
					 */

					currentComponent = "tRunJob_8";

					/**
					 * [tRunJob_8 process_data_begin ] stop
					 */

					/**
					 * [tRunJob_9 main ] start
					 */

					currentComponent = "tRunJob_9";

					// row8
					// row8

					if (execStat) {
						runStat.updateStatOnConnection("row8" + iterateId, 1, 1);
					}

					java.util.List<String> paraList_tRunJob_9 = new java.util.ArrayList<String>();

					paraList_tRunJob_9.add("--father_pid=" + pid);

					paraList_tRunJob_9.add("--root_pid=" + rootPid);

					paraList_tRunJob_9.add("--father_node=tRunJob_9");

					paraList_tRunJob_9.add("--context=PR");

					// for feature:10589

					paraList_tRunJob_9.add("--stat_port=" + portStats);

					if (resuming_logs_dir_path != null) {
						paraList_tRunJob_9.add("--resuming_logs_dir_path="
								+ resuming_logs_dir_path);
					}
					String childResumePath_tRunJob_9 = ResumeUtil
							.getChildJobCheckPointPath(resuming_checkpoint_path);
					String tRunJobName_tRunJob_9 = ResumeUtil
							.getRighttRunJob(resuming_checkpoint_path);
					if ("tRunJob_9".equals(tRunJobName_tRunJob_9)
							&& childResumePath_tRunJob_9 != null) {
						paraList_tRunJob_9
								.add("--resuming_checkpoint_path="
										+ ResumeUtil
												.getChildJobCheckPointPath(resuming_checkpoint_path));
					}
					paraList_tRunJob_9.add("--parent_part_launcher=JOB:"
							+ jobName + "/NODE:tRunJob_9");

					java.util.Map<String, Object> parentContextMap_tRunJob_9 = new java.util.HashMap<String, Object>();

					Object obj_tRunJob_9 = null;

					gifmis.dimpaymentrequestreleasespo_0_1.dimPaymentRequestReleasesPO childJob_tRunJob_9 = new gifmis.dimpaymentrequestreleasespo_0_1.dimPaymentRequestReleasesPO();
					// pass DataSources
					java.util.Map<String, routines.system.TalendDataSource> talendDataSources_tRunJob_9 = (java.util.Map<String, routines.system.TalendDataSource>) globalMap
							.get(KEY_DB_DATASOURCES);
					if (null != talendDataSources_tRunJob_9) {
						java.util.Map<String, javax.sql.DataSource> dataSources_tRunJob_9 = new java.util.HashMap<String, javax.sql.DataSource>();
						for (java.util.Map.Entry<String, routines.system.TalendDataSource> talendDataSourceEntry_tRunJob_9 : talendDataSources_tRunJob_9
								.entrySet()) {
							dataSources_tRunJob_9.put(
									talendDataSourceEntry_tRunJob_9.getKey(),
									talendDataSourceEntry_tRunJob_9.getValue()
											.getRawDataSource());
						}
						childJob_tRunJob_9
								.setDataSources(dataSources_tRunJob_9);
					}

					childJob_tRunJob_9.parentContextMap = parentContextMap_tRunJob_9;

					String[][] childReturn_tRunJob_9 = childJob_tRunJob_9
							.runJob((String[]) paraList_tRunJob_9
									.toArray(new String[paraList_tRunJob_9
											.size()]));

					errorCode = childJob_tRunJob_9.getErrorCode();

					if (childJob_tRunJob_9.getErrorCode() == null) {
						globalMap.put(
								"tRunJob_9_CHILD_RETURN_CODE",
								childJob_tRunJob_9.getStatus() != null
										&& ("failure")
												.equals(childJob_tRunJob_9
														.getStatus()) ? 1 : 0);
					} else {
						globalMap.put("tRunJob_9_CHILD_RETURN_CODE",
								childJob_tRunJob_9.getErrorCode());
					}
					if (childJob_tRunJob_9.getExceptionStackTrace() != null) {
						globalMap.put("tRunJob_9_CHILD_EXCEPTION_STACKTRACE",
								childJob_tRunJob_9.getExceptionStackTrace());
					}

					if (childJob_tRunJob_9.getErrorCode() != null
							|| ("failure").equals(childJob_tRunJob_9
									.getStatus())) {
						throw new RuntimeException(
								"Child job running failed.\n"
										+ childJob_tRunJob_9.getException()
												.getClass().getName()
										+ ": "
										+ childJob_tRunJob_9.getException()
												.getMessage());
					}

					tos_count_tRunJob_9++;

					/**
					 * [tRunJob_9 main ] stop
					 */

					/**
					 * [tRunJob_9 process_data_begin ] start
					 */

					currentComponent = "tRunJob_9";

					/**
					 * [tRunJob_9 process_data_begin ] stop
					 */

					/**
					 * [tRunJob_10 main ] start
					 */

					currentComponent = "tRunJob_10";

					// row9
					// row9

					if (execStat) {
						runStat.updateStatOnConnection("row9" + iterateId, 1, 1);
					}

					java.util.List<String> paraList_tRunJob_10 = new java.util.ArrayList<String>();

					paraList_tRunJob_10.add("--father_pid=" + pid);

					paraList_tRunJob_10.add("--root_pid=" + rootPid);

					paraList_tRunJob_10.add("--father_node=tRunJob_10");

					paraList_tRunJob_10.add("--context=PR");

					// for feature:10589

					paraList_tRunJob_10.add("--stat_port=" + portStats);

					if (resuming_logs_dir_path != null) {
						paraList_tRunJob_10.add("--resuming_logs_dir_path="
								+ resuming_logs_dir_path);
					}
					String childResumePath_tRunJob_10 = ResumeUtil
							.getChildJobCheckPointPath(resuming_checkpoint_path);
					String tRunJobName_tRunJob_10 = ResumeUtil
							.getRighttRunJob(resuming_checkpoint_path);
					if ("tRunJob_10".equals(tRunJobName_tRunJob_10)
							&& childResumePath_tRunJob_10 != null) {
						paraList_tRunJob_10
								.add("--resuming_checkpoint_path="
										+ ResumeUtil
												.getChildJobCheckPointPath(resuming_checkpoint_path));
					}
					paraList_tRunJob_10.add("--parent_part_launcher=JOB:"
							+ jobName + "/NODE:tRunJob_10");

					java.util.Map<String, Object> parentContextMap_tRunJob_10 = new java.util.HashMap<String, Object>();

					Object obj_tRunJob_10 = null;

					gifmis.dimpaymentrequesttransitpaymentsmda_0_1.dimPaymentRequestTransitPaymentsMDA childJob_tRunJob_10 = new gifmis.dimpaymentrequesttransitpaymentsmda_0_1.dimPaymentRequestTransitPaymentsMDA();
					// pass DataSources
					java.util.Map<String, routines.system.TalendDataSource> talendDataSources_tRunJob_10 = (java.util.Map<String, routines.system.TalendDataSource>) globalMap
							.get(KEY_DB_DATASOURCES);
					if (null != talendDataSources_tRunJob_10) {
						java.util.Map<String, javax.sql.DataSource> dataSources_tRunJob_10 = new java.util.HashMap<String, javax.sql.DataSource>();
						for (java.util.Map.Entry<String, routines.system.TalendDataSource> talendDataSourceEntry_tRunJob_10 : talendDataSources_tRunJob_10
								.entrySet()) {
							dataSources_tRunJob_10.put(
									talendDataSourceEntry_tRunJob_10.getKey(),
									talendDataSourceEntry_tRunJob_10.getValue()
											.getRawDataSource());
						}
						childJob_tRunJob_10
								.setDataSources(dataSources_tRunJob_10);
					}

					childJob_tRunJob_10.parentContextMap = parentContextMap_tRunJob_10;

					String[][] childReturn_tRunJob_10 = childJob_tRunJob_10
							.runJob((String[]) paraList_tRunJob_10
									.toArray(new String[paraList_tRunJob_10
											.size()]));

					errorCode = childJob_tRunJob_10.getErrorCode();

					if (childJob_tRunJob_10.getErrorCode() == null) {
						globalMap.put(
								"tRunJob_10_CHILD_RETURN_CODE",
								childJob_tRunJob_10.getStatus() != null
										&& ("failure")
												.equals(childJob_tRunJob_10
														.getStatus()) ? 1 : 0);
					} else {
						globalMap.put("tRunJob_10_CHILD_RETURN_CODE",
								childJob_tRunJob_10.getErrorCode());
					}
					if (childJob_tRunJob_10.getExceptionStackTrace() != null) {
						globalMap.put("tRunJob_10_CHILD_EXCEPTION_STACKTRACE",
								childJob_tRunJob_10.getExceptionStackTrace());
					}

					if (childJob_tRunJob_10.getErrorCode() != null
							|| ("failure").equals(childJob_tRunJob_10
									.getStatus())) {
						throw new RuntimeException(
								"Child job running failed.\n"
										+ childJob_tRunJob_10.getException()
												.getClass().getName()
										+ ": "
										+ childJob_tRunJob_10.getException()
												.getMessage());
					}

					tos_count_tRunJob_10++;

					/**
					 * [tRunJob_10 main ] stop
					 */

					/**
					 * [tRunJob_10 process_data_begin ] start
					 */

					currentComponent = "tRunJob_10";

					/**
					 * [tRunJob_10 process_data_begin ] stop
					 */

					/**
					 * [tRunJob_11 main ] start
					 */

					currentComponent = "tRunJob_11";

					// row10
					// row10

					if (execStat) {
						runStat.updateStatOnConnection("row10" + iterateId, 1,
								1);
					}

					java.util.List<String> paraList_tRunJob_11 = new java.util.ArrayList<String>();

					paraList_tRunJob_11.add("--father_pid=" + pid);

					paraList_tRunJob_11.add("--root_pid=" + rootPid);

					paraList_tRunJob_11.add("--father_node=tRunJob_11");

					paraList_tRunJob_11.add("--context=PR");

					// for feature:10589

					paraList_tRunJob_11.add("--stat_port=" + portStats);

					if (resuming_logs_dir_path != null) {
						paraList_tRunJob_11.add("--resuming_logs_dir_path="
								+ resuming_logs_dir_path);
					}
					String childResumePath_tRunJob_11 = ResumeUtil
							.getChildJobCheckPointPath(resuming_checkpoint_path);
					String tRunJobName_tRunJob_11 = ResumeUtil
							.getRighttRunJob(resuming_checkpoint_path);
					if ("tRunJob_11".equals(tRunJobName_tRunJob_11)
							&& childResumePath_tRunJob_11 != null) {
						paraList_tRunJob_11
								.add("--resuming_checkpoint_path="
										+ ResumeUtil
												.getChildJobCheckPointPath(resuming_checkpoint_path));
					}
					paraList_tRunJob_11.add("--parent_part_launcher=JOB:"
							+ jobName + "/NODE:tRunJob_11");

					java.util.Map<String, Object> parentContextMap_tRunJob_11 = new java.util.HashMap<String, Object>();

					Object obj_tRunJob_11 = null;

					gifmis.dimpaymentrequesttransitpaymentsregular_0_1.dimPaymentRequestTransitPaymentsRegular childJob_tRunJob_11 = new gifmis.dimpaymentrequesttransitpaymentsregular_0_1.dimPaymentRequestTransitPaymentsRegular();
					// pass DataSources
					java.util.Map<String, routines.system.TalendDataSource> talendDataSources_tRunJob_11 = (java.util.Map<String, routines.system.TalendDataSource>) globalMap
							.get(KEY_DB_DATASOURCES);
					if (null != talendDataSources_tRunJob_11) {
						java.util.Map<String, javax.sql.DataSource> dataSources_tRunJob_11 = new java.util.HashMap<String, javax.sql.DataSource>();
						for (java.util.Map.Entry<String, routines.system.TalendDataSource> talendDataSourceEntry_tRunJob_11 : talendDataSources_tRunJob_11
								.entrySet()) {
							dataSources_tRunJob_11.put(
									talendDataSourceEntry_tRunJob_11.getKey(),
									talendDataSourceEntry_tRunJob_11.getValue()
											.getRawDataSource());
						}
						childJob_tRunJob_11
								.setDataSources(dataSources_tRunJob_11);
					}

					childJob_tRunJob_11.parentContextMap = parentContextMap_tRunJob_11;

					String[][] childReturn_tRunJob_11 = childJob_tRunJob_11
							.runJob((String[]) paraList_tRunJob_11
									.toArray(new String[paraList_tRunJob_11
											.size()]));

					errorCode = childJob_tRunJob_11.getErrorCode();

					if (childJob_tRunJob_11.getErrorCode() == null) {
						globalMap.put(
								"tRunJob_11_CHILD_RETURN_CODE",
								childJob_tRunJob_11.getStatus() != null
										&& ("failure")
												.equals(childJob_tRunJob_11
														.getStatus()) ? 1 : 0);
					} else {
						globalMap.put("tRunJob_11_CHILD_RETURN_CODE",
								childJob_tRunJob_11.getErrorCode());
					}
					if (childJob_tRunJob_11.getExceptionStackTrace() != null) {
						globalMap.put("tRunJob_11_CHILD_EXCEPTION_STACKTRACE",
								childJob_tRunJob_11.getExceptionStackTrace());
					}

					if (childJob_tRunJob_11.getErrorCode() != null
							|| ("failure").equals(childJob_tRunJob_11
									.getStatus())) {
						throw new RuntimeException(
								"Child job running failed.\n"
										+ childJob_tRunJob_11.getException()
												.getClass().getName()
										+ ": "
										+ childJob_tRunJob_11.getException()
												.getMessage());
					}

					tos_count_tRunJob_11++;

					/**
					 * [tRunJob_11 main ] stop
					 */

					/**
					 * [tRunJob_11 process_data_begin ] start
					 */

					currentComponent = "tRunJob_11";

					/**
					 * [tRunJob_11 process_data_begin ] stop
					 */

					/**
					 * [tRunJob_12 main ] start
					 */

					currentComponent = "tRunJob_12";

					// row11
					// row11

					if (execStat) {
						runStat.updateStatOnConnection("row11" + iterateId, 1,
								1);
					}

					java.util.List<String> paraList_tRunJob_12 = new java.util.ArrayList<String>();

					paraList_tRunJob_12.add("--father_pid=" + pid);

					paraList_tRunJob_12.add("--root_pid=" + rootPid);

					paraList_tRunJob_12.add("--father_node=tRunJob_12");

					paraList_tRunJob_12.add("--context=PR");

					// for feature:10589

					paraList_tRunJob_12.add("--stat_port=" + portStats);

					if (resuming_logs_dir_path != null) {
						paraList_tRunJob_12.add("--resuming_logs_dir_path="
								+ resuming_logs_dir_path);
					}
					String childResumePath_tRunJob_12 = ResumeUtil
							.getChildJobCheckPointPath(resuming_checkpoint_path);
					String tRunJobName_tRunJob_12 = ResumeUtil
							.getRighttRunJob(resuming_checkpoint_path);
					if ("tRunJob_12".equals(tRunJobName_tRunJob_12)
							&& childResumePath_tRunJob_12 != null) {
						paraList_tRunJob_12
								.add("--resuming_checkpoint_path="
										+ ResumeUtil
												.getChildJobCheckPointPath(resuming_checkpoint_path));
					}
					paraList_tRunJob_12.add("--parent_part_launcher=JOB:"
							+ jobName + "/NODE:tRunJob_12");

					java.util.Map<String, Object> parentContextMap_tRunJob_12 = new java.util.HashMap<String, Object>();

					Object obj_tRunJob_12 = null;

					gifmis.dimpaymentrequestwarrant_0_1.dimPaymentRequestWarrant childJob_tRunJob_12 = new gifmis.dimpaymentrequestwarrant_0_1.dimPaymentRequestWarrant();
					// pass DataSources
					java.util.Map<String, routines.system.TalendDataSource> talendDataSources_tRunJob_12 = (java.util.Map<String, routines.system.TalendDataSource>) globalMap
							.get(KEY_DB_DATASOURCES);
					if (null != talendDataSources_tRunJob_12) {
						java.util.Map<String, javax.sql.DataSource> dataSources_tRunJob_12 = new java.util.HashMap<String, javax.sql.DataSource>();
						for (java.util.Map.Entry<String, routines.system.TalendDataSource> talendDataSourceEntry_tRunJob_12 : talendDataSources_tRunJob_12
								.entrySet()) {
							dataSources_tRunJob_12.put(
									talendDataSourceEntry_tRunJob_12.getKey(),
									talendDataSourceEntry_tRunJob_12.getValue()
											.getRawDataSource());
						}
						childJob_tRunJob_12
								.setDataSources(dataSources_tRunJob_12);
					}

					childJob_tRunJob_12.parentContextMap = parentContextMap_tRunJob_12;

					String[][] childReturn_tRunJob_12 = childJob_tRunJob_12
							.runJob((String[]) paraList_tRunJob_12
									.toArray(new String[paraList_tRunJob_12
											.size()]));

					errorCode = childJob_tRunJob_12.getErrorCode();

					if (childJob_tRunJob_12.getErrorCode() == null) {
						globalMap.put(
								"tRunJob_12_CHILD_RETURN_CODE",
								childJob_tRunJob_12.getStatus() != null
										&& ("failure")
												.equals(childJob_tRunJob_12
														.getStatus()) ? 1 : 0);
					} else {
						globalMap.put("tRunJob_12_CHILD_RETURN_CODE",
								childJob_tRunJob_12.getErrorCode());
					}
					if (childJob_tRunJob_12.getExceptionStackTrace() != null) {
						globalMap.put("tRunJob_12_CHILD_EXCEPTION_STACKTRACE",
								childJob_tRunJob_12.getExceptionStackTrace());
					}

					if (childJob_tRunJob_12.getErrorCode() != null
							|| ("failure").equals(childJob_tRunJob_12
									.getStatus())) {
						throw new RuntimeException(
								"Child job running failed.\n"
										+ childJob_tRunJob_12.getException()
												.getClass().getName()
										+ ": "
										+ childJob_tRunJob_12.getException()
												.getMessage());
					}

					tos_count_tRunJob_12++;

					/**
					 * [tRunJob_12 main ] stop
					 */

					/**
					 * [tRunJob_12 process_data_begin ] start
					 */

					currentComponent = "tRunJob_12";

					/**
					 * [tRunJob_12 process_data_begin ] stop
					 */

					/**
					 * [tRunJob_12 process_data_end ] start
					 */

					currentComponent = "tRunJob_12";

					/**
					 * [tRunJob_12 process_data_end ] stop
					 */

					/**
					 * [tRunJob_11 process_data_end ] start
					 */

					currentComponent = "tRunJob_11";

					/**
					 * [tRunJob_11 process_data_end ] stop
					 */

					/**
					 * [tRunJob_10 process_data_end ] start
					 */

					currentComponent = "tRunJob_10";

					/**
					 * [tRunJob_10 process_data_end ] stop
					 */

					/**
					 * [tRunJob_9 process_data_end ] start
					 */

					currentComponent = "tRunJob_9";

					/**
					 * [tRunJob_9 process_data_end ] stop
					 */

					/**
					 * [tRunJob_8 process_data_end ] start
					 */

					currentComponent = "tRunJob_8";

					/**
					 * [tRunJob_8 process_data_end ] stop
					 */

					/**
					 * [tRunJob_7 process_data_end ] start
					 */

					currentComponent = "tRunJob_7";

					/**
					 * [tRunJob_7 process_data_end ] stop
					 */

					/**
					 * [tRunJob_6 process_data_end ] start
					 */

					currentComponent = "tRunJob_6";

					/**
					 * [tRunJob_6 process_data_end ] stop
					 */

					/**
					 * [tRunJob_5 process_data_end ] start
					 */

					currentComponent = "tRunJob_5";

					/**
					 * [tRunJob_5 process_data_end ] stop
					 */

					/**
					 * [tRunJob_4 process_data_end ] start
					 */

					currentComponent = "tRunJob_4";

					/**
					 * [tRunJob_4 process_data_end ] stop
					 */

					/**
					 * [tRunJob_3 process_data_end ] start
					 */

					currentComponent = "tRunJob_3";

					/**
					 * [tRunJob_3 process_data_end ] stop
					 */

					/**
					 * [tRunJob_2 process_data_end ] start
					 */

					currentComponent = "tRunJob_2";

					/**
					 * [tRunJob_2 process_data_end ] stop
					 */

				} // C_01

				/**
				 * [tRunJob_1 process_data_end ] start
				 */

				currentComponent = "tRunJob_1";

				/**
				 * [tRunJob_1 process_data_end ] stop
				 */

				/**
				 * [tRunJob_1 end ] start
				 */

				currentComponent = "tRunJob_1";

				ok_Hash.put("tRunJob_1", true);
				end_Hash.put("tRunJob_1", System.currentTimeMillis());

				/**
				 * [tRunJob_1 end ] stop
				 */

				/**
				 * [tRunJob_2 end ] start
				 */

				currentComponent = "tRunJob_2";

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null
							|| !((Boolean) resourceMap.get("inIterateVComp"))) {
						runStat.updateStatOnConnection("row1" + iterateId, 2, 0);
					}
				}

				ok_Hash.put("tRunJob_2", true);
				end_Hash.put("tRunJob_2", System.currentTimeMillis());

				/**
				 * [tRunJob_2 end ] stop
				 */

				/**
				 * [tRunJob_3 end ] start
				 */

				currentComponent = "tRunJob_3";

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null
							|| !((Boolean) resourceMap.get("inIterateVComp"))) {
						runStat.updateStatOnConnection("row2" + iterateId, 2, 0);
					}
				}

				ok_Hash.put("tRunJob_3", true);
				end_Hash.put("tRunJob_3", System.currentTimeMillis());

				/**
				 * [tRunJob_3 end ] stop
				 */

				/**
				 * [tRunJob_4 end ] start
				 */

				currentComponent = "tRunJob_4";

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null
							|| !((Boolean) resourceMap.get("inIterateVComp"))) {
						runStat.updateStatOnConnection("row3" + iterateId, 2, 0);
					}
				}

				ok_Hash.put("tRunJob_4", true);
				end_Hash.put("tRunJob_4", System.currentTimeMillis());

				/**
				 * [tRunJob_4 end ] stop
				 */

				/**
				 * [tRunJob_5 end ] start
				 */

				currentComponent = "tRunJob_5";

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null
							|| !((Boolean) resourceMap.get("inIterateVComp"))) {
						runStat.updateStatOnConnection("row4" + iterateId, 2, 0);
					}
				}

				ok_Hash.put("tRunJob_5", true);
				end_Hash.put("tRunJob_5", System.currentTimeMillis());

				/**
				 * [tRunJob_5 end ] stop
				 */

				/**
				 * [tRunJob_6 end ] start
				 */

				currentComponent = "tRunJob_6";

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null
							|| !((Boolean) resourceMap.get("inIterateVComp"))) {
						runStat.updateStatOnConnection("row5" + iterateId, 2, 0);
					}
				}

				ok_Hash.put("tRunJob_6", true);
				end_Hash.put("tRunJob_6", System.currentTimeMillis());

				/**
				 * [tRunJob_6 end ] stop
				 */

				/**
				 * [tRunJob_7 end ] start
				 */

				currentComponent = "tRunJob_7";

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null
							|| !((Boolean) resourceMap.get("inIterateVComp"))) {
						runStat.updateStatOnConnection("row6" + iterateId, 2, 0);
					}
				}

				ok_Hash.put("tRunJob_7", true);
				end_Hash.put("tRunJob_7", System.currentTimeMillis());

				/**
				 * [tRunJob_7 end ] stop
				 */

				/**
				 * [tRunJob_8 end ] start
				 */

				currentComponent = "tRunJob_8";

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null
							|| !((Boolean) resourceMap.get("inIterateVComp"))) {
						runStat.updateStatOnConnection("row7" + iterateId, 2, 0);
					}
				}

				ok_Hash.put("tRunJob_8", true);
				end_Hash.put("tRunJob_8", System.currentTimeMillis());

				/**
				 * [tRunJob_8 end ] stop
				 */

				/**
				 * [tRunJob_9 end ] start
				 */

				currentComponent = "tRunJob_9";

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null
							|| !((Boolean) resourceMap.get("inIterateVComp"))) {
						runStat.updateStatOnConnection("row8" + iterateId, 2, 0);
					}
				}

				ok_Hash.put("tRunJob_9", true);
				end_Hash.put("tRunJob_9", System.currentTimeMillis());

				/**
				 * [tRunJob_9 end ] stop
				 */

				/**
				 * [tRunJob_10 end ] start
				 */

				currentComponent = "tRunJob_10";

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null
							|| !((Boolean) resourceMap.get("inIterateVComp"))) {
						runStat.updateStatOnConnection("row9" + iterateId, 2, 0);
					}
				}

				ok_Hash.put("tRunJob_10", true);
				end_Hash.put("tRunJob_10", System.currentTimeMillis());

				/**
				 * [tRunJob_10 end ] stop
				 */

				/**
				 * [tRunJob_11 end ] start
				 */

				currentComponent = "tRunJob_11";

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null
							|| !((Boolean) resourceMap.get("inIterateVComp"))) {
						runStat.updateStatOnConnection("row10" + iterateId, 2,
								0);
					}
				}

				ok_Hash.put("tRunJob_11", true);
				end_Hash.put("tRunJob_11", System.currentTimeMillis());

				/**
				 * [tRunJob_11 end ] stop
				 */

				/**
				 * [tRunJob_12 end ] start
				 */

				currentComponent = "tRunJob_12";

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null
							|| !((Boolean) resourceMap.get("inIterateVComp"))) {
						runStat.updateStatOnConnection("row11" + iterateId, 2,
								0);
					}
				}

				ok_Hash.put("tRunJob_12", true);
				end_Hash.put("tRunJob_12", System.currentTimeMillis());

				/**
				 * [tRunJob_12 end ] stop
				 */

			}// end the resume

		} catch (java.lang.Exception e) {

			TalendException te = new TalendException(e, currentComponent,
					globalMap);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			try {

				/**
				 * [tRunJob_1 finally ] start
				 */

				currentComponent = "tRunJob_1";

				/**
				 * [tRunJob_1 finally ] stop
				 */

				/**
				 * [tRunJob_2 finally ] start
				 */

				currentComponent = "tRunJob_2";

				/**
				 * [tRunJob_2 finally ] stop
				 */

				/**
				 * [tRunJob_3 finally ] start
				 */

				currentComponent = "tRunJob_3";

				/**
				 * [tRunJob_3 finally ] stop
				 */

				/**
				 * [tRunJob_4 finally ] start
				 */

				currentComponent = "tRunJob_4";

				/**
				 * [tRunJob_4 finally ] stop
				 */

				/**
				 * [tRunJob_5 finally ] start
				 */

				currentComponent = "tRunJob_5";

				/**
				 * [tRunJob_5 finally ] stop
				 */

				/**
				 * [tRunJob_6 finally ] start
				 */

				currentComponent = "tRunJob_6";

				/**
				 * [tRunJob_6 finally ] stop
				 */

				/**
				 * [tRunJob_7 finally ] start
				 */

				currentComponent = "tRunJob_7";

				/**
				 * [tRunJob_7 finally ] stop
				 */

				/**
				 * [tRunJob_8 finally ] start
				 */

				currentComponent = "tRunJob_8";

				/**
				 * [tRunJob_8 finally ] stop
				 */

				/**
				 * [tRunJob_9 finally ] start
				 */

				currentComponent = "tRunJob_9";

				/**
				 * [tRunJob_9 finally ] stop
				 */

				/**
				 * [tRunJob_10 finally ] start
				 */

				currentComponent = "tRunJob_10";

				/**
				 * [tRunJob_10 finally ] stop
				 */

				/**
				 * [tRunJob_11 finally ] start
				 */

				currentComponent = "tRunJob_11";

				/**
				 * [tRunJob_11 finally ] stop
				 */

				/**
				 * [tRunJob_12 finally ] start
				 */

				currentComponent = "tRunJob_12";

				/**
				 * [tRunJob_12 finally ] stop
				 */

			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("tRunJob_1_SUBPROCESS_STATE", 1);
	}

	public void connectionStatsLogs_CommitProcess(
			final java.util.Map<String, Object> globalMap)
			throws TalendException {
		globalMap.put("connectionStatsLogs_Commit_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception()
						.getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				/**
				 * [connectionStatsLogs_Commit begin ] start
				 */

				ok_Hash.put("connectionStatsLogs_Commit", false);
				start_Hash.put("connectionStatsLogs_Commit",
						System.currentTimeMillis());

				currentComponent = "connectionStatsLogs_Commit";

				int tos_count_connectionStatsLogs_Commit = 0;

				class BytesLimit65535_connectionStatsLogs_Commit {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_connectionStatsLogs_Commit()
						.limitLog4jByte();

				/**
				 * [connectionStatsLogs_Commit begin ] stop
				 */

				/**
				 * [connectionStatsLogs_Commit main ] start
				 */

				currentComponent = "connectionStatsLogs_Commit";

				java.sql.Connection conn_connectionStatsLogs_Commit = (java.sql.Connection) globalMap
						.get("conn_connectionStatsLogs");

				if (conn_connectionStatsLogs_Commit != null
						&& !conn_connectionStatsLogs_Commit.isClosed()) {

					conn_connectionStatsLogs_Commit.commit();

				}

				tos_count_connectionStatsLogs_Commit++;

				/**
				 * [connectionStatsLogs_Commit main ] stop
				 */

				/**
				 * [connectionStatsLogs_Commit process_data_begin ] start
				 */

				currentComponent = "connectionStatsLogs_Commit";

				/**
				 * [connectionStatsLogs_Commit process_data_begin ] stop
				 */

				/**
				 * [connectionStatsLogs_Commit process_data_end ] start
				 */

				currentComponent = "connectionStatsLogs_Commit";

				/**
				 * [connectionStatsLogs_Commit process_data_end ] stop
				 */

				/**
				 * [connectionStatsLogs_Commit end ] start
				 */

				currentComponent = "connectionStatsLogs_Commit";

				ok_Hash.put("connectionStatsLogs_Commit", true);
				end_Hash.put("connectionStatsLogs_Commit",
						System.currentTimeMillis());

				/**
				 * [connectionStatsLogs_Commit end ] stop
				 */
			}// end the resume

		} catch (java.lang.Exception e) {

			TalendException te = new TalendException(e, currentComponent,
					globalMap);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			try {

				/**
				 * [connectionStatsLogs_Commit finally ] start
				 */

				currentComponent = "connectionStatsLogs_Commit";

				/**
				 * [connectionStatsLogs_Commit finally ] stop
				 */
			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("connectionStatsLogs_Commit_SUBPROCESS_STATE", 1);
	}

	public void connectionStatsLogsProcess(
			final java.util.Map<String, Object> globalMap)
			throws TalendException {
		globalMap.put("connectionStatsLogs_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception()
						.getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				/**
				 * [connectionStatsLogs begin ] start
				 */

				ok_Hash.put("connectionStatsLogs", false);
				start_Hash.put("connectionStatsLogs",
						System.currentTimeMillis());

				currentComponent = "connectionStatsLogs";

				int tos_count_connectionStatsLogs = 0;

				class BytesLimit65535_connectionStatsLogs {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_connectionStatsLogs().limitLog4jByte();

				String properties_connectionStatsLogs = "noDatetimeStringSync=true";
				if (properties_connectionStatsLogs == null
						|| properties_connectionStatsLogs.trim().length() == 0) {
					properties_connectionStatsLogs += "rewriteBatchedStatements=true";
				} else if (properties_connectionStatsLogs != null
						&& !properties_connectionStatsLogs
								.contains("rewriteBatchedStatements")) {
					properties_connectionStatsLogs += "&rewriteBatchedStatements=true";
				}

				String url_connectionStatsLogs = "jdbc:mysql://" + "localhost"
						+ ":" + "3306" + "/" + "amc" + "?"
						+ properties_connectionStatsLogs;

				String dbUser_connectionStatsLogs = "etluser";

				final String decryptedPassword_connectionStatsLogs = routines.system.PasswordEncryptUtil
						.decryptPassword("72e87cf3a913e55bf4f7aba1746784ea");
				String dbPwd_connectionStatsLogs = decryptedPassword_connectionStatsLogs;

				java.sql.Connection conn_connectionStatsLogs = null;

				String sharedConnectionName_connectionStatsLogs = "StatsAndLog_Shared_Connection";
				conn_connectionStatsLogs = SharedDBConnection.getDBConnection(
						"org.gjt.mm.mysql.Driver", url_connectionStatsLogs,
						dbUser_connectionStatsLogs, dbPwd_connectionStatsLogs,
						sharedConnectionName_connectionStatsLogs);
				if (null != conn_connectionStatsLogs) {

					conn_connectionStatsLogs.setAutoCommit(false);
				}

				globalMap.put("conn_connectionStatsLogs",
						conn_connectionStatsLogs);

				globalMap.put("db_connectionStatsLogs", "amc");

				/**
				 * [connectionStatsLogs begin ] stop
				 */

				/**
				 * [connectionStatsLogs main ] start
				 */

				currentComponent = "connectionStatsLogs";

				tos_count_connectionStatsLogs++;

				/**
				 * [connectionStatsLogs main ] stop
				 */

				/**
				 * [connectionStatsLogs process_data_begin ] start
				 */

				currentComponent = "connectionStatsLogs";

				/**
				 * [connectionStatsLogs process_data_begin ] stop
				 */

				/**
				 * [connectionStatsLogs process_data_end ] start
				 */

				currentComponent = "connectionStatsLogs";

				/**
				 * [connectionStatsLogs process_data_end ] stop
				 */

				/**
				 * [connectionStatsLogs end ] start
				 */

				currentComponent = "connectionStatsLogs";

				ok_Hash.put("connectionStatsLogs", true);
				end_Hash.put("connectionStatsLogs", System.currentTimeMillis());

				/**
				 * [connectionStatsLogs end ] stop
				 */
			}// end the resume

		} catch (java.lang.Exception e) {

			TalendException te = new TalendException(e, currentComponent,
					globalMap);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			try {

				/**
				 * [connectionStatsLogs finally ] start
				 */

				currentComponent = "connectionStatsLogs";

				/**
				 * [connectionStatsLogs finally ] stop
				 */
			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("connectionStatsLogs_SUBPROCESS_STATE", 1);
	}

	public static class row_talendStats_DBStruct implements
			routines.system.IPersistableRow<row_talendStats_DBStruct> {
		final static byte[] commonByteArrayLock_GIFMIS_PaymentRequestAllJobs = new byte[0];
		static byte[] commonByteArray_GIFMIS_PaymentRequestAllJobs = new byte[0];

		public java.util.Date moment;

		public java.util.Date getMoment() {
			return this.moment;
		}

		public String pid;

		public String getPid() {
			return this.pid;
		}

		public String father_pid;

		public String getFather_pid() {
			return this.father_pid;
		}

		public String root_pid;

		public String getRoot_pid() {
			return this.root_pid;
		}

		public Long system_pid;

		public Long getSystem_pid() {
			return this.system_pid;
		}

		public String project;

		public String getProject() {
			return this.project;
		}

		public String job;

		public String getJob() {
			return this.job;
		}

		public String job_repository_id;

		public String getJob_repository_id() {
			return this.job_repository_id;
		}

		public String job_version;

		public String getJob_version() {
			return this.job_version;
		}

		public String context;

		public String getContext() {
			return this.context;
		}

		public String origin;

		public String getOrigin() {
			return this.origin;
		}

		public String message_type;

		public String getMessage_type() {
			return this.message_type;
		}

		public String message;

		public String getMessage() {
			return this.message;
		}

		public Long duration;

		public Long getDuration() {
			return this.duration;
		}

		private java.util.Date readDate(ObjectInputStream dis)
				throws IOException {
			java.util.Date dateReturn = null;
			int length = 0;
			length = dis.readByte();
			if (length == -1) {
				dateReturn = null;
			} else {
				dateReturn = new Date(dis.readLong());
			}
			return dateReturn;
		}

		private void writeDate(java.util.Date date1, ObjectOutputStream dos)
				throws IOException {
			if (date1 == null) {
				dos.writeByte(-1);
			} else {
				dos.writeByte(0);
				dos.writeLong(date1.getTime());
			}
		}

		private String readString(ObjectInputStream dis) throws IOException {
			String strReturn = null;
			int length = 0;
			length = dis.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				if (length > commonByteArray_GIFMIS_PaymentRequestAllJobs.length) {
					if (length < 1024
							&& commonByteArray_GIFMIS_PaymentRequestAllJobs.length == 0) {
						commonByteArray_GIFMIS_PaymentRequestAllJobs = new byte[1024];
					} else {
						commonByteArray_GIFMIS_PaymentRequestAllJobs = new byte[2 * length];
					}
				}
				dis.readFully(commonByteArray_GIFMIS_PaymentRequestAllJobs, 0,
						length);
				strReturn = new String(
						commonByteArray_GIFMIS_PaymentRequestAllJobs, 0,
						length, utf8Charset);
			}
			return strReturn;
		}

		private void writeString(String str, ObjectOutputStream dos)
				throws IOException {
			if (str == null) {
				dos.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				dos.writeInt(byteArray.length);
				dos.write(byteArray);
			}
		}

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_GIFMIS_PaymentRequestAllJobs) {

				try {

					int length = 0;

					this.moment = readDate(dis);

					this.pid = readString(dis);

					this.father_pid = readString(dis);

					this.root_pid = readString(dis);

					length = dis.readByte();
					if (length == -1) {
						this.system_pid = null;
					} else {
						this.system_pid = dis.readLong();
					}

					this.project = readString(dis);

					this.job = readString(dis);

					this.job_repository_id = readString(dis);

					this.job_version = readString(dis);

					this.context = readString(dis);

					this.origin = readString(dis);

					this.message_type = readString(dis);

					this.message = readString(dis);

					length = dis.readByte();
					if (length == -1) {
						this.duration = null;
					} else {
						this.duration = dis.readLong();
					}

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

				// java.util.Date

				writeDate(this.moment, dos);

				// String

				writeString(this.pid, dos);

				// String

				writeString(this.father_pid, dos);

				// String

				writeString(this.root_pid, dos);

				// Long

				if (this.system_pid == null) {
					dos.writeByte(-1);
				} else {
					dos.writeByte(0);
					dos.writeLong(this.system_pid);
				}

				// String

				writeString(this.project, dos);

				// String

				writeString(this.job, dos);

				// String

				writeString(this.job_repository_id, dos);

				// String

				writeString(this.job_version, dos);

				// String

				writeString(this.context, dos);

				// String

				writeString(this.origin, dos);

				// String

				writeString(this.message_type, dos);

				// String

				writeString(this.message, dos);

				// Long

				if (this.duration == null) {
					dos.writeByte(-1);
				} else {
					dos.writeByte(0);
					dos.writeLong(this.duration);
				}

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("moment=" + String.valueOf(moment));
			sb.append(",pid=" + pid);
			sb.append(",father_pid=" + father_pid);
			sb.append(",root_pid=" + root_pid);
			sb.append(",system_pid=" + String.valueOf(system_pid));
			sb.append(",project=" + project);
			sb.append(",job=" + job);
			sb.append(",job_repository_id=" + job_repository_id);
			sb.append(",job_version=" + job_version);
			sb.append(",context=" + context);
			sb.append(",origin=" + origin);
			sb.append(",message_type=" + message_type);
			sb.append(",message=" + message);
			sb.append(",duration=" + String.valueOf(duration));
			sb.append("]");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(row_talendStats_DBStruct other) {

			int returnValue = -1;

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(),
						object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public static class row_talendStats_STATSStruct implements
			routines.system.IPersistableRow<row_talendStats_STATSStruct> {
		final static byte[] commonByteArrayLock_GIFMIS_PaymentRequestAllJobs = new byte[0];
		static byte[] commonByteArray_GIFMIS_PaymentRequestAllJobs = new byte[0];

		public java.util.Date moment;

		public java.util.Date getMoment() {
			return this.moment;
		}

		public String pid;

		public String getPid() {
			return this.pid;
		}

		public String father_pid;

		public String getFather_pid() {
			return this.father_pid;
		}

		public String root_pid;

		public String getRoot_pid() {
			return this.root_pid;
		}

		public Long system_pid;

		public Long getSystem_pid() {
			return this.system_pid;
		}

		public String project;

		public String getProject() {
			return this.project;
		}

		public String job;

		public String getJob() {
			return this.job;
		}

		public String job_repository_id;

		public String getJob_repository_id() {
			return this.job_repository_id;
		}

		public String job_version;

		public String getJob_version() {
			return this.job_version;
		}

		public String context;

		public String getContext() {
			return this.context;
		}

		public String origin;

		public String getOrigin() {
			return this.origin;
		}

		public String message_type;

		public String getMessage_type() {
			return this.message_type;
		}

		public String message;

		public String getMessage() {
			return this.message;
		}

		public Long duration;

		public Long getDuration() {
			return this.duration;
		}

		private java.util.Date readDate(ObjectInputStream dis)
				throws IOException {
			java.util.Date dateReturn = null;
			int length = 0;
			length = dis.readByte();
			if (length == -1) {
				dateReturn = null;
			} else {
				dateReturn = new Date(dis.readLong());
			}
			return dateReturn;
		}

		private void writeDate(java.util.Date date1, ObjectOutputStream dos)
				throws IOException {
			if (date1 == null) {
				dos.writeByte(-1);
			} else {
				dos.writeByte(0);
				dos.writeLong(date1.getTime());
			}
		}

		private String readString(ObjectInputStream dis) throws IOException {
			String strReturn = null;
			int length = 0;
			length = dis.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				if (length > commonByteArray_GIFMIS_PaymentRequestAllJobs.length) {
					if (length < 1024
							&& commonByteArray_GIFMIS_PaymentRequestAllJobs.length == 0) {
						commonByteArray_GIFMIS_PaymentRequestAllJobs = new byte[1024];
					} else {
						commonByteArray_GIFMIS_PaymentRequestAllJobs = new byte[2 * length];
					}
				}
				dis.readFully(commonByteArray_GIFMIS_PaymentRequestAllJobs, 0,
						length);
				strReturn = new String(
						commonByteArray_GIFMIS_PaymentRequestAllJobs, 0,
						length, utf8Charset);
			}
			return strReturn;
		}

		private void writeString(String str, ObjectOutputStream dos)
				throws IOException {
			if (str == null) {
				dos.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				dos.writeInt(byteArray.length);
				dos.write(byteArray);
			}
		}

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_GIFMIS_PaymentRequestAllJobs) {

				try {

					int length = 0;

					this.moment = readDate(dis);

					this.pid = readString(dis);

					this.father_pid = readString(dis);

					this.root_pid = readString(dis);

					length = dis.readByte();
					if (length == -1) {
						this.system_pid = null;
					} else {
						this.system_pid = dis.readLong();
					}

					this.project = readString(dis);

					this.job = readString(dis);

					this.job_repository_id = readString(dis);

					this.job_version = readString(dis);

					this.context = readString(dis);

					this.origin = readString(dis);

					this.message_type = readString(dis);

					this.message = readString(dis);

					length = dis.readByte();
					if (length == -1) {
						this.duration = null;
					} else {
						this.duration = dis.readLong();
					}

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

				// java.util.Date

				writeDate(this.moment, dos);

				// String

				writeString(this.pid, dos);

				// String

				writeString(this.father_pid, dos);

				// String

				writeString(this.root_pid, dos);

				// Long

				if (this.system_pid == null) {
					dos.writeByte(-1);
				} else {
					dos.writeByte(0);
					dos.writeLong(this.system_pid);
				}

				// String

				writeString(this.project, dos);

				// String

				writeString(this.job, dos);

				// String

				writeString(this.job_repository_id, dos);

				// String

				writeString(this.job_version, dos);

				// String

				writeString(this.context, dos);

				// String

				writeString(this.origin, dos);

				// String

				writeString(this.message_type, dos);

				// String

				writeString(this.message, dos);

				// Long

				if (this.duration == null) {
					dos.writeByte(-1);
				} else {
					dos.writeByte(0);
					dos.writeLong(this.duration);
				}

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("moment=" + String.valueOf(moment));
			sb.append(",pid=" + pid);
			sb.append(",father_pid=" + father_pid);
			sb.append(",root_pid=" + root_pid);
			sb.append(",system_pid=" + String.valueOf(system_pid));
			sb.append(",project=" + project);
			sb.append(",job=" + job);
			sb.append(",job_repository_id=" + job_repository_id);
			sb.append(",job_version=" + job_version);
			sb.append(",context=" + context);
			sb.append(",origin=" + origin);
			sb.append(",message_type=" + message_type);
			sb.append(",message=" + message);
			sb.append(",duration=" + String.valueOf(duration));
			sb.append("]");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(row_talendStats_STATSStruct other) {

			int returnValue = -1;

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(),
						object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public void talendStats_STATSProcess(
			final java.util.Map<String, Object> globalMap)
			throws TalendException {
		globalMap.put("talendStats_STATS_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;
		String currentVirtualComponent = null;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception()
						.getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				row_talendStats_STATSStruct row_talendStats_STATS = new row_talendStats_STATSStruct();
				row_talendStats_DBStruct row_talendStats_DB = new row_talendStats_DBStruct();

				/**
				 * [talendStats_CONSOLE begin ] start
				 */

				ok_Hash.put("talendStats_CONSOLE", false);
				start_Hash.put("talendStats_CONSOLE",
						System.currentTimeMillis());

				currentVirtualComponent = "talendStats_CONSOLE";

				currentComponent = "talendStats_CONSOLE";

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null) {

						runStat.updateStatOnConnection("Main" + iterateId, 0, 0);

					}
				}

				int tos_count_talendStats_CONSOLE = 0;

				class BytesLimit65535_talendStats_CONSOLE {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_talendStats_CONSOLE().limitLog4jByte();

				// /////////////////////

				final String OUTPUT_FIELD_SEPARATOR_talendStats_CONSOLE = "|";
				java.io.PrintStream consoleOut_talendStats_CONSOLE = null;

				StringBuilder strBuffer_talendStats_CONSOLE = null;
				int nb_line_talendStats_CONSOLE = 0;
				// /////////////////////

				/**
				 * [talendStats_CONSOLE begin ] stop
				 */

				/**
				 * [talendStats_DB begin ] start
				 */

				ok_Hash.put("talendStats_DB", false);
				start_Hash.put("talendStats_DB", System.currentTimeMillis());

				currentVirtualComponent = "talendStats_DB";

				currentComponent = "talendStats_DB";

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null) {

						runStat.updateStatOnConnection("Main" + iterateId, 0, 0);

					}
				}

				int tos_count_talendStats_DB = 0;

				class BytesLimit65535_talendStats_DB {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_talendStats_DB().limitLog4jByte();

				int nb_line_talendStats_DB = 0;
				int nb_line_update_talendStats_DB = 0;
				int nb_line_inserted_talendStats_DB = 0;
				int nb_line_deleted_talendStats_DB = 0;
				int nb_line_rejected_talendStats_DB = 0;

				int deletedCount_talendStats_DB = 0;
				int updatedCount_talendStats_DB = 0;
				int insertedCount_talendStats_DB = 0;

				int rejectedCount_talendStats_DB = 0;

				String tableName_talendStats_DB = "jobStats";
				boolean whetherReject_talendStats_DB = false;

				java.util.Calendar calendar_talendStats_DB = java.util.Calendar
						.getInstance();
				calendar_talendStats_DB.set(1, 0, 1, 0, 0, 0);
				long year1_talendStats_DB = calendar_talendStats_DB.getTime()
						.getTime();
				calendar_talendStats_DB.set(10000, 0, 1, 0, 0, 0);
				long year10000_talendStats_DB = calendar_talendStats_DB
						.getTime().getTime();
				long date_talendStats_DB;

				java.sql.Connection conn_talendStats_DB = null;
				conn_talendStats_DB = (java.sql.Connection) globalMap
						.get("conn_connectionStatsLogs");

				int count_talendStats_DB = 0;

				// [%connection%][checktable][tableName]
				String keyCheckTable_talendStats_DB = conn_talendStats_DB
						+ "[checktable]" + "[" + "jobStats" + "]";

				if (GlobalResource.resourceMap
						.get(keyCheckTable_talendStats_DB) == null) {// }

					synchronized (GlobalResource.resourceLockMap
							.get(keyCheckTable_talendStats_DB)) {// }
						if (GlobalResource.resourceMap
								.get(keyCheckTable_talendStats_DB) == null) {// }
							java.sql.DatabaseMetaData dbMetaData_talendStats_DB = conn_talendStats_DB
									.getMetaData();
							java.sql.ResultSet rsTable_talendStats_DB = dbMetaData_talendStats_DB
									.getTables(null, null, null,
											new String[] { "TABLE" });
							boolean whetherExist_talendStats_DB = false;
							while (rsTable_talendStats_DB.next()) {
								String table_talendStats_DB = rsTable_talendStats_DB
										.getString("TABLE_NAME");
								if (table_talendStats_DB
										.equalsIgnoreCase("jobStats")) {
									whetherExist_talendStats_DB = true;
									break;
								}
							}
							rsTable_talendStats_DB.close();
							if (!whetherExist_talendStats_DB) {
								java.sql.Statement stmtCreate_talendStats_DB = conn_talendStats_DB
										.createStatement();
								stmtCreate_talendStats_DB
										.execute("CREATE TABLE `"
												+ tableName_talendStats_DB
												+ "`(`moment` DATETIME ,`pid` VARCHAR(20)  ,`father_pid` VARCHAR(20)  ,`root_pid` VARCHAR(20)  ,`system_pid` BIGINT(8)  ,`project` VARCHAR(50)  ,`job` VARCHAR(255)  ,`job_repository_id` VARCHAR(255)  ,`job_version` VARCHAR(255)  ,`context` VARCHAR(50)  ,`origin` VARCHAR(255)  ,`message_type` VARCHAR(255)  ,`message` VARCHAR(255)  ,`duration` BIGINT(8)  )");
								stmtCreate_talendStats_DB.close();
							}
							GlobalResource.resourceMap.put(
									keyCheckTable_talendStats_DB, true);
							// {{{
						} // end of if
					} // end synchronized
				}

				String insert_talendStats_DB = "INSERT INTO `"
						+ "jobStats"
						+ "` (`moment`,`pid`,`father_pid`,`root_pid`,`system_pid`,`project`,`job`,`job_repository_id`,`job_version`,`context`,`origin`,`message_type`,`message`,`duration`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

				java.sql.PreparedStatement pstmt_talendStats_DB = null;
				// [%connection%][psmt][tableName]
				String keyPsmt_talendStats_DB = conn_talendStats_DB + "[psmt]"
						+ "[" + "jobStats" + "]";
				pstmt_talendStats_DB = SharedDBPreparedStatement
						.getSharedPreparedStatement(conn_talendStats_DB,
								insert_talendStats_DB, keyPsmt_talendStats_DB);

				/**
				 * [talendStats_DB begin ] stop
				 */

				/**
				 * [talendStats_STATS begin ] start
				 */

				ok_Hash.put("talendStats_STATS", false);
				start_Hash.put("talendStats_STATS", System.currentTimeMillis());

				currentVirtualComponent = "talendStats_STATS";

				currentComponent = "talendStats_STATS";

				int tos_count_talendStats_STATS = 0;

				class BytesLimit65535_talendStats_STATS {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_talendStats_STATS().limitLog4jByte();

				for (StatCatcherUtils.StatCatcherMessage scm : talendStats_STATS
						.getMessages()) {
					row_talendStats_STATS.pid = pid;
					row_talendStats_STATS.root_pid = rootPid;
					row_talendStats_STATS.father_pid = fatherPid;
					row_talendStats_STATS.project = projectName;
					row_talendStats_STATS.job = jobName;
					row_talendStats_STATS.context = contextStr;
					row_talendStats_STATS.origin = (scm.getOrigin() == null
							|| scm.getOrigin().length() < 1 ? null : scm
							.getOrigin());
					row_talendStats_STATS.message = scm.getMessage();
					row_talendStats_STATS.duration = scm.getDuration();
					row_talendStats_STATS.moment = scm.getMoment();
					row_talendStats_STATS.message_type = scm.getMessageType();
					row_talendStats_STATS.job_version = scm.getJobVersion();
					row_talendStats_STATS.job_repository_id = scm.getJobId();
					row_talendStats_STATS.system_pid = scm.getSystemPid();

					/**
					 * [talendStats_STATS begin ] stop
					 */

					/**
					 * [talendStats_STATS main ] start
					 */

					currentVirtualComponent = "talendStats_STATS";

					currentComponent = "talendStats_STATS";

					tos_count_talendStats_STATS++;

					/**
					 * [talendStats_STATS main ] stop
					 */

					/**
					 * [talendStats_STATS process_data_begin ] start
					 */

					currentVirtualComponent = "talendStats_STATS";

					currentComponent = "talendStats_STATS";

					/**
					 * [talendStats_STATS process_data_begin ] stop
					 */

					/**
					 * [talendStats_DB main ] start
					 */

					currentVirtualComponent = "talendStats_DB";

					currentComponent = "talendStats_DB";

					// Main
					// row_talendStats_STATS

					if (execStat) {
						runStat.updateStatOnConnection("Main" + iterateId, 1, 1);
					}

					row_talendStats_DB = null;
					whetherReject_talendStats_DB = false;
					if (row_talendStats_STATS.moment != null) {
						date_talendStats_DB = row_talendStats_STATS.moment
								.getTime();
						if (date_talendStats_DB < year1_talendStats_DB
								|| date_talendStats_DB >= year10000_talendStats_DB) {
							pstmt_talendStats_DB.setString(1,
									"0000-00-00 00:00:00");
						} else {
							pstmt_talendStats_DB
									.setTimestamp(1, new java.sql.Timestamp(
											date_talendStats_DB));
						}
					} else {
						pstmt_talendStats_DB.setNull(1, java.sql.Types.DATE);
					}

					if (row_talendStats_STATS.pid == null) {
						pstmt_talendStats_DB.setNull(2, java.sql.Types.VARCHAR);
					} else {
						pstmt_talendStats_DB.setString(2,
								row_talendStats_STATS.pid);
					}

					if (row_talendStats_STATS.father_pid == null) {
						pstmt_talendStats_DB.setNull(3, java.sql.Types.VARCHAR);
					} else {
						pstmt_talendStats_DB.setString(3,
								row_talendStats_STATS.father_pid);
					}

					if (row_talendStats_STATS.root_pid == null) {
						pstmt_talendStats_DB.setNull(4, java.sql.Types.VARCHAR);
					} else {
						pstmt_talendStats_DB.setString(4,
								row_talendStats_STATS.root_pid);
					}

					if (row_talendStats_STATS.system_pid == null) {
						pstmt_talendStats_DB.setNull(5, java.sql.Types.INTEGER);
					} else {
						pstmt_talendStats_DB.setLong(5,
								row_talendStats_STATS.system_pid);
					}

					if (row_talendStats_STATS.project == null) {
						pstmt_talendStats_DB.setNull(6, java.sql.Types.VARCHAR);
					} else {
						pstmt_talendStats_DB.setString(6,
								row_talendStats_STATS.project);
					}

					if (row_talendStats_STATS.job == null) {
						pstmt_talendStats_DB.setNull(7, java.sql.Types.VARCHAR);
					} else {
						pstmt_talendStats_DB.setString(7,
								row_talendStats_STATS.job);
					}

					if (row_talendStats_STATS.job_repository_id == null) {
						pstmt_talendStats_DB.setNull(8, java.sql.Types.VARCHAR);
					} else {
						pstmt_talendStats_DB.setString(8,
								row_talendStats_STATS.job_repository_id);
					}

					if (row_talendStats_STATS.job_version == null) {
						pstmt_talendStats_DB.setNull(9, java.sql.Types.VARCHAR);
					} else {
						pstmt_talendStats_DB.setString(9,
								row_talendStats_STATS.job_version);
					}

					if (row_talendStats_STATS.context == null) {
						pstmt_talendStats_DB
								.setNull(10, java.sql.Types.VARCHAR);
					} else {
						pstmt_talendStats_DB.setString(10,
								row_talendStats_STATS.context);
					}

					if (row_talendStats_STATS.origin == null) {
						pstmt_talendStats_DB
								.setNull(11, java.sql.Types.VARCHAR);
					} else {
						pstmt_talendStats_DB.setString(11,
								row_talendStats_STATS.origin);
					}

					if (row_talendStats_STATS.message_type == null) {
						pstmt_talendStats_DB
								.setNull(12, java.sql.Types.VARCHAR);
					} else {
						pstmt_talendStats_DB.setString(12,
								row_talendStats_STATS.message_type);
					}

					if (row_talendStats_STATS.message == null) {
						pstmt_talendStats_DB
								.setNull(13, java.sql.Types.VARCHAR);
					} else {
						pstmt_talendStats_DB.setString(13,
								row_talendStats_STATS.message);
					}

					if (row_talendStats_STATS.duration == null) {
						pstmt_talendStats_DB
								.setNull(14, java.sql.Types.INTEGER);
					} else {
						pstmt_talendStats_DB.setLong(14,
								row_talendStats_STATS.duration);
					}

					try {
						nb_line_talendStats_DB++;
						insertedCount_talendStats_DB = insertedCount_talendStats_DB
								+ pstmt_talendStats_DB.executeUpdate();
					} catch (java.lang.Exception e) {
						whetherReject_talendStats_DB = true;
						System.err.print(e.getMessage());
					}
					if (!whetherReject_talendStats_DB) {
						row_talendStats_DB = new row_talendStats_DBStruct();
						row_talendStats_DB.moment = row_talendStats_STATS.moment;
						row_talendStats_DB.pid = row_talendStats_STATS.pid;
						row_talendStats_DB.father_pid = row_talendStats_STATS.father_pid;
						row_talendStats_DB.root_pid = row_talendStats_STATS.root_pid;
						row_talendStats_DB.system_pid = row_talendStats_STATS.system_pid;
						row_talendStats_DB.project = row_talendStats_STATS.project;
						row_talendStats_DB.job = row_talendStats_STATS.job;
						row_talendStats_DB.job_repository_id = row_talendStats_STATS.job_repository_id;
						row_talendStats_DB.job_version = row_talendStats_STATS.job_version;
						row_talendStats_DB.context = row_talendStats_STATS.context;
						row_talendStats_DB.origin = row_talendStats_STATS.origin;
						row_talendStats_DB.message_type = row_talendStats_STATS.message_type;
						row_talendStats_DB.message = row_talendStats_STATS.message;
						row_talendStats_DB.duration = row_talendStats_STATS.duration;
					}

					tos_count_talendStats_DB++;

					/**
					 * [talendStats_DB main ] stop
					 */

					/**
					 * [talendStats_DB process_data_begin ] start
					 */

					currentVirtualComponent = "talendStats_DB";

					currentComponent = "talendStats_DB";

					/**
					 * [talendStats_DB process_data_begin ] stop
					 */
					// Start of branch "row_talendStats_DB"
					if (row_talendStats_DB != null) {

						/**
						 * [talendStats_CONSOLE main ] start
						 */

						currentVirtualComponent = "talendStats_CONSOLE";

						currentComponent = "talendStats_CONSOLE";

						// Main
						// row_talendStats_DB

						if (execStat) {
							runStat.updateStatOnConnection("Main" + iterateId,
									1, 1);
						}

						// /////////////////////

						strBuffer_talendStats_CONSOLE = new StringBuilder();

						if (row_talendStats_DB.moment != null) { //

							strBuffer_talendStats_CONSOLE.append(FormatterUtils
									.format_Date(row_talendStats_DB.moment,
											"yyyy-MM-dd HH:mm:ss"));

						} //

						strBuffer_talendStats_CONSOLE.append("|");

						if (row_talendStats_DB.pid != null) { //

							strBuffer_talendStats_CONSOLE.append(String
									.valueOf(row_talendStats_DB.pid));

						} //

						strBuffer_talendStats_CONSOLE.append("|");

						if (row_talendStats_DB.father_pid != null) { //

							strBuffer_talendStats_CONSOLE.append(String
									.valueOf(row_talendStats_DB.father_pid));

						} //

						strBuffer_talendStats_CONSOLE.append("|");

						if (row_talendStats_DB.root_pid != null) { //

							strBuffer_talendStats_CONSOLE.append(String
									.valueOf(row_talendStats_DB.root_pid));

						} //

						strBuffer_talendStats_CONSOLE.append("|");

						if (row_talendStats_DB.system_pid != null) { //

							strBuffer_talendStats_CONSOLE.append(String
									.valueOf(row_talendStats_DB.system_pid));

						} //

						strBuffer_talendStats_CONSOLE.append("|");

						if (row_talendStats_DB.project != null) { //

							strBuffer_talendStats_CONSOLE.append(String
									.valueOf(row_talendStats_DB.project));

						} //

						strBuffer_talendStats_CONSOLE.append("|");

						if (row_talendStats_DB.job != null) { //

							strBuffer_talendStats_CONSOLE.append(String
									.valueOf(row_talendStats_DB.job));

						} //

						strBuffer_talendStats_CONSOLE.append("|");

						if (row_talendStats_DB.job_repository_id != null) { //

							strBuffer_talendStats_CONSOLE
									.append(String
											.valueOf(row_talendStats_DB.job_repository_id));

						} //

						strBuffer_talendStats_CONSOLE.append("|");

						if (row_talendStats_DB.job_version != null) { //

							strBuffer_talendStats_CONSOLE.append(String
									.valueOf(row_talendStats_DB.job_version));

						} //

						strBuffer_talendStats_CONSOLE.append("|");

						if (row_talendStats_DB.context != null) { //

							strBuffer_talendStats_CONSOLE.append(String
									.valueOf(row_talendStats_DB.context));

						} //

						strBuffer_talendStats_CONSOLE.append("|");

						if (row_talendStats_DB.origin != null) { //

							strBuffer_talendStats_CONSOLE.append(String
									.valueOf(row_talendStats_DB.origin));

						} //

						strBuffer_talendStats_CONSOLE.append("|");

						if (row_talendStats_DB.message_type != null) { //

							strBuffer_talendStats_CONSOLE.append(String
									.valueOf(row_talendStats_DB.message_type));

						} //

						strBuffer_talendStats_CONSOLE.append("|");

						if (row_talendStats_DB.message != null) { //

							strBuffer_talendStats_CONSOLE.append(String
									.valueOf(row_talendStats_DB.message));

						} //

						strBuffer_talendStats_CONSOLE.append("|");

						if (row_talendStats_DB.duration != null) { //

							strBuffer_talendStats_CONSOLE.append(String
									.valueOf(row_talendStats_DB.duration));

						} //

						if (globalMap.get("tLogRow_CONSOLE") != null) {
							consoleOut_talendStats_CONSOLE = (java.io.PrintStream) globalMap
									.get("tLogRow_CONSOLE");
						} else {
							consoleOut_talendStats_CONSOLE = new java.io.PrintStream(
									new java.io.BufferedOutputStream(System.out));
							globalMap.put("tLogRow_CONSOLE",
									consoleOut_talendStats_CONSOLE);
						}
						consoleOut_talendStats_CONSOLE
								.println(strBuffer_talendStats_CONSOLE
										.toString());
						consoleOut_talendStats_CONSOLE.flush();
						nb_line_talendStats_CONSOLE++;
						// ////

						// ////

						// /////////////////////

						tos_count_talendStats_CONSOLE++;

						/**
						 * [talendStats_CONSOLE main ] stop
						 */

						/**
						 * [talendStats_CONSOLE process_data_begin ] start
						 */

						currentVirtualComponent = "talendStats_CONSOLE";

						currentComponent = "talendStats_CONSOLE";

						/**
						 * [talendStats_CONSOLE process_data_begin ] stop
						 */

						/**
						 * [talendStats_CONSOLE process_data_end ] start
						 */

						currentVirtualComponent = "talendStats_CONSOLE";

						currentComponent = "talendStats_CONSOLE";

						/**
						 * [talendStats_CONSOLE process_data_end ] stop
						 */

					} // End of branch "row_talendStats_DB"

					/**
					 * [talendStats_DB process_data_end ] start
					 */

					currentVirtualComponent = "talendStats_DB";

					currentComponent = "talendStats_DB";

					/**
					 * [talendStats_DB process_data_end ] stop
					 */

					/**
					 * [talendStats_STATS process_data_end ] start
					 */

					currentVirtualComponent = "talendStats_STATS";

					currentComponent = "talendStats_STATS";

					/**
					 * [talendStats_STATS process_data_end ] stop
					 */

					/**
					 * [talendStats_STATS end ] start
					 */

					currentVirtualComponent = "talendStats_STATS";

					currentComponent = "talendStats_STATS";

				}

				ok_Hash.put("talendStats_STATS", true);
				end_Hash.put("talendStats_STATS", System.currentTimeMillis());

				/**
				 * [talendStats_STATS end ] stop
				 */

				/**
				 * [talendStats_DB end ] start
				 */

				currentVirtualComponent = "talendStats_DB";

				currentComponent = "talendStats_DB";

				if (pstmt_talendStats_DB != null) {

					SharedDBPreparedStatement
							.releasePreparedStatement(keyPsmt_talendStats_DB);

				}

				nb_line_deleted_talendStats_DB = nb_line_deleted_talendStats_DB
						+ deletedCount_talendStats_DB;
				nb_line_update_talendStats_DB = nb_line_update_talendStats_DB
						+ updatedCount_talendStats_DB;
				nb_line_inserted_talendStats_DB = nb_line_inserted_talendStats_DB
						+ insertedCount_talendStats_DB;
				nb_line_rejected_talendStats_DB = nb_line_rejected_talendStats_DB
						+ rejectedCount_talendStats_DB;

				globalMap.put("talendStats_DB_NB_LINE", nb_line_talendStats_DB);
				globalMap.put("talendStats_DB_NB_LINE_UPDATED",
						nb_line_update_talendStats_DB);
				globalMap.put("talendStats_DB_NB_LINE_INSERTED",
						nb_line_inserted_talendStats_DB);
				globalMap.put("talendStats_DB_NB_LINE_DELETED",
						nb_line_deleted_talendStats_DB);
				globalMap.put("talendStats_DB_NB_LINE_REJECTED",
						nb_line_rejected_talendStats_DB);

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null
							|| !((Boolean) resourceMap.get("inIterateVComp"))) {
						runStat.updateStatOnConnection("Main" + iterateId, 2, 0);
					}
				}

				ok_Hash.put("talendStats_DB", true);
				end_Hash.put("talendStats_DB", System.currentTimeMillis());

				/**
				 * [talendStats_DB end ] stop
				 */

				/**
				 * [talendStats_CONSOLE end ] start
				 */

				currentVirtualComponent = "talendStats_CONSOLE";

				currentComponent = "talendStats_CONSOLE";

				// ////
				// ////
				globalMap.put("talendStats_CONSOLE_NB_LINE",
						nb_line_talendStats_CONSOLE);

				// /////////////////////

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null
							|| !((Boolean) resourceMap.get("inIterateVComp"))) {
						runStat.updateStatOnConnection("Main" + iterateId, 2, 0);
					}
				}

				ok_Hash.put("talendStats_CONSOLE", true);
				end_Hash.put("talendStats_CONSOLE", System.currentTimeMillis());

				/**
				 * [talendStats_CONSOLE end ] stop
				 */

			}// end the resume

			if (resumeEntryMethodName == null || globalResumeTicket) {
				resumeUtil
						.addLog("CHECKPOINT",
								"CONNECTION:SUBJOB_OK:talendStats_STATS:sub_ok_talendStats_connectionStatsLogs_Commit",
								"", Thread.currentThread().getId() + "", "",
								"", "", "", "");
			}

			if (execStat) {
				runStat.updateStatOnConnection(
						"sub_ok_talendStats_connectionStatsLogs_Commit", 0,
						"ok");
			}

			connectionStatsLogs_CommitProcess(globalMap);

		} catch (java.lang.Exception e) {

			TalendException te = new TalendException(e, currentComponent,
					globalMap);

			te.setVirtualComponentName(currentVirtualComponent);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			try {

				/**
				 * [talendStats_STATS finally ] start
				 */

				currentVirtualComponent = "talendStats_STATS";

				currentComponent = "talendStats_STATS";

				/**
				 * [talendStats_STATS finally ] stop
				 */

				/**
				 * [talendStats_DB finally ] start
				 */

				currentVirtualComponent = "talendStats_DB";

				currentComponent = "talendStats_DB";

				/**
				 * [talendStats_DB finally ] stop
				 */

				/**
				 * [talendStats_CONSOLE finally ] start
				 */

				currentVirtualComponent = "talendStats_CONSOLE";

				currentComponent = "talendStats_CONSOLE";

				/**
				 * [talendStats_CONSOLE finally ] stop
				 */

			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("talendStats_STATS_SUBPROCESS_STATE", 1);
	}

	public static class row_talendLogs_DBStruct implements
			routines.system.IPersistableRow<row_talendLogs_DBStruct> {
		final static byte[] commonByteArrayLock_GIFMIS_PaymentRequestAllJobs = new byte[0];
		static byte[] commonByteArray_GIFMIS_PaymentRequestAllJobs = new byte[0];

		public java.util.Date moment;

		public java.util.Date getMoment() {
			return this.moment;
		}

		public String pid;

		public String getPid() {
			return this.pid;
		}

		public String root_pid;

		public String getRoot_pid() {
			return this.root_pid;
		}

		public String father_pid;

		public String getFather_pid() {
			return this.father_pid;
		}

		public String project;

		public String getProject() {
			return this.project;
		}

		public String job;

		public String getJob() {
			return this.job;
		}

		public String context;

		public String getContext() {
			return this.context;
		}

		public Integer priority;

		public Integer getPriority() {
			return this.priority;
		}

		public String type;

		public String getType() {
			return this.type;
		}

		public String origin;

		public String getOrigin() {
			return this.origin;
		}

		public String message;

		public String getMessage() {
			return this.message;
		}

		public Integer code;

		public Integer getCode() {
			return this.code;
		}

		private java.util.Date readDate(ObjectInputStream dis)
				throws IOException {
			java.util.Date dateReturn = null;
			int length = 0;
			length = dis.readByte();
			if (length == -1) {
				dateReturn = null;
			} else {
				dateReturn = new Date(dis.readLong());
			}
			return dateReturn;
		}

		private void writeDate(java.util.Date date1, ObjectOutputStream dos)
				throws IOException {
			if (date1 == null) {
				dos.writeByte(-1);
			} else {
				dos.writeByte(0);
				dos.writeLong(date1.getTime());
			}
		}

		private String readString(ObjectInputStream dis) throws IOException {
			String strReturn = null;
			int length = 0;
			length = dis.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				if (length > commonByteArray_GIFMIS_PaymentRequestAllJobs.length) {
					if (length < 1024
							&& commonByteArray_GIFMIS_PaymentRequestAllJobs.length == 0) {
						commonByteArray_GIFMIS_PaymentRequestAllJobs = new byte[1024];
					} else {
						commonByteArray_GIFMIS_PaymentRequestAllJobs = new byte[2 * length];
					}
				}
				dis.readFully(commonByteArray_GIFMIS_PaymentRequestAllJobs, 0,
						length);
				strReturn = new String(
						commonByteArray_GIFMIS_PaymentRequestAllJobs, 0,
						length, utf8Charset);
			}
			return strReturn;
		}

		private void writeString(String str, ObjectOutputStream dos)
				throws IOException {
			if (str == null) {
				dos.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				dos.writeInt(byteArray.length);
				dos.write(byteArray);
			}
		}

		private Integer readInteger(ObjectInputStream dis) throws IOException {
			Integer intReturn;
			int length = 0;
			length = dis.readByte();
			if (length == -1) {
				intReturn = null;
			} else {
				intReturn = dis.readInt();
			}
			return intReturn;
		}

		private void writeInteger(Integer intNum, ObjectOutputStream dos)
				throws IOException {
			if (intNum == null) {
				dos.writeByte(-1);
			} else {
				dos.writeByte(0);
				dos.writeInt(intNum);
			}
		}

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_GIFMIS_PaymentRequestAllJobs) {

				try {

					int length = 0;

					this.moment = readDate(dis);

					this.pid = readString(dis);

					this.root_pid = readString(dis);

					this.father_pid = readString(dis);

					this.project = readString(dis);

					this.job = readString(dis);

					this.context = readString(dis);

					this.priority = readInteger(dis);

					this.type = readString(dis);

					this.origin = readString(dis);

					this.message = readString(dis);

					this.code = readInteger(dis);

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

				// java.util.Date

				writeDate(this.moment, dos);

				// String

				writeString(this.pid, dos);

				// String

				writeString(this.root_pid, dos);

				// String

				writeString(this.father_pid, dos);

				// String

				writeString(this.project, dos);

				// String

				writeString(this.job, dos);

				// String

				writeString(this.context, dos);

				// Integer

				writeInteger(this.priority, dos);

				// String

				writeString(this.type, dos);

				// String

				writeString(this.origin, dos);

				// String

				writeString(this.message, dos);

				// Integer

				writeInteger(this.code, dos);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("moment=" + String.valueOf(moment));
			sb.append(",pid=" + pid);
			sb.append(",root_pid=" + root_pid);
			sb.append(",father_pid=" + father_pid);
			sb.append(",project=" + project);
			sb.append(",job=" + job);
			sb.append(",context=" + context);
			sb.append(",priority=" + String.valueOf(priority));
			sb.append(",type=" + type);
			sb.append(",origin=" + origin);
			sb.append(",message=" + message);
			sb.append(",code=" + String.valueOf(code));
			sb.append("]");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(row_talendLogs_DBStruct other) {

			int returnValue = -1;

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(),
						object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public static class row_talendLogs_LOGSStruct implements
			routines.system.IPersistableRow<row_talendLogs_LOGSStruct> {
		final static byte[] commonByteArrayLock_GIFMIS_PaymentRequestAllJobs = new byte[0];
		static byte[] commonByteArray_GIFMIS_PaymentRequestAllJobs = new byte[0];

		public java.util.Date moment;

		public java.util.Date getMoment() {
			return this.moment;
		}

		public String pid;

		public String getPid() {
			return this.pid;
		}

		public String root_pid;

		public String getRoot_pid() {
			return this.root_pid;
		}

		public String father_pid;

		public String getFather_pid() {
			return this.father_pid;
		}

		public String project;

		public String getProject() {
			return this.project;
		}

		public String job;

		public String getJob() {
			return this.job;
		}

		public String context;

		public String getContext() {
			return this.context;
		}

		public Integer priority;

		public Integer getPriority() {
			return this.priority;
		}

		public String type;

		public String getType() {
			return this.type;
		}

		public String origin;

		public String getOrigin() {
			return this.origin;
		}

		public String message;

		public String getMessage() {
			return this.message;
		}

		public Integer code;

		public Integer getCode() {
			return this.code;
		}

		private java.util.Date readDate(ObjectInputStream dis)
				throws IOException {
			java.util.Date dateReturn = null;
			int length = 0;
			length = dis.readByte();
			if (length == -1) {
				dateReturn = null;
			} else {
				dateReturn = new Date(dis.readLong());
			}
			return dateReturn;
		}

		private void writeDate(java.util.Date date1, ObjectOutputStream dos)
				throws IOException {
			if (date1 == null) {
				dos.writeByte(-1);
			} else {
				dos.writeByte(0);
				dos.writeLong(date1.getTime());
			}
		}

		private String readString(ObjectInputStream dis) throws IOException {
			String strReturn = null;
			int length = 0;
			length = dis.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				if (length > commonByteArray_GIFMIS_PaymentRequestAllJobs.length) {
					if (length < 1024
							&& commonByteArray_GIFMIS_PaymentRequestAllJobs.length == 0) {
						commonByteArray_GIFMIS_PaymentRequestAllJobs = new byte[1024];
					} else {
						commonByteArray_GIFMIS_PaymentRequestAllJobs = new byte[2 * length];
					}
				}
				dis.readFully(commonByteArray_GIFMIS_PaymentRequestAllJobs, 0,
						length);
				strReturn = new String(
						commonByteArray_GIFMIS_PaymentRequestAllJobs, 0,
						length, utf8Charset);
			}
			return strReturn;
		}

		private void writeString(String str, ObjectOutputStream dos)
				throws IOException {
			if (str == null) {
				dos.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				dos.writeInt(byteArray.length);
				dos.write(byteArray);
			}
		}

		private Integer readInteger(ObjectInputStream dis) throws IOException {
			Integer intReturn;
			int length = 0;
			length = dis.readByte();
			if (length == -1) {
				intReturn = null;
			} else {
				intReturn = dis.readInt();
			}
			return intReturn;
		}

		private void writeInteger(Integer intNum, ObjectOutputStream dos)
				throws IOException {
			if (intNum == null) {
				dos.writeByte(-1);
			} else {
				dos.writeByte(0);
				dos.writeInt(intNum);
			}
		}

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_GIFMIS_PaymentRequestAllJobs) {

				try {

					int length = 0;

					this.moment = readDate(dis);

					this.pid = readString(dis);

					this.root_pid = readString(dis);

					this.father_pid = readString(dis);

					this.project = readString(dis);

					this.job = readString(dis);

					this.context = readString(dis);

					this.priority = readInteger(dis);

					this.type = readString(dis);

					this.origin = readString(dis);

					this.message = readString(dis);

					this.code = readInteger(dis);

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

				// java.util.Date

				writeDate(this.moment, dos);

				// String

				writeString(this.pid, dos);

				// String

				writeString(this.root_pid, dos);

				// String

				writeString(this.father_pid, dos);

				// String

				writeString(this.project, dos);

				// String

				writeString(this.job, dos);

				// String

				writeString(this.context, dos);

				// Integer

				writeInteger(this.priority, dos);

				// String

				writeString(this.type, dos);

				// String

				writeString(this.origin, dos);

				// String

				writeString(this.message, dos);

				// Integer

				writeInteger(this.code, dos);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("moment=" + String.valueOf(moment));
			sb.append(",pid=" + pid);
			sb.append(",root_pid=" + root_pid);
			sb.append(",father_pid=" + father_pid);
			sb.append(",project=" + project);
			sb.append(",job=" + job);
			sb.append(",context=" + context);
			sb.append(",priority=" + String.valueOf(priority));
			sb.append(",type=" + type);
			sb.append(",origin=" + origin);
			sb.append(",message=" + message);
			sb.append(",code=" + String.valueOf(code));
			sb.append("]");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(row_talendLogs_LOGSStruct other) {

			int returnValue = -1;

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(),
						object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public void talendLogs_LOGSProcess(
			final java.util.Map<String, Object> globalMap)
			throws TalendException {
		globalMap.put("talendLogs_LOGS_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;
		String currentVirtualComponent = null;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception()
						.getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				row_talendLogs_LOGSStruct row_talendLogs_LOGS = new row_talendLogs_LOGSStruct();
				row_talendLogs_DBStruct row_talendLogs_DB = new row_talendLogs_DBStruct();

				/**
				 * [talendLogs_CONSOLE begin ] start
				 */

				ok_Hash.put("talendLogs_CONSOLE", false);
				start_Hash
						.put("talendLogs_CONSOLE", System.currentTimeMillis());

				currentVirtualComponent = "talendLogs_CONSOLE";

				currentComponent = "talendLogs_CONSOLE";

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null) {

						runStat.updateStatOnConnection("Main" + iterateId, 0, 0);

					}
				}

				int tos_count_talendLogs_CONSOLE = 0;

				class BytesLimit65535_talendLogs_CONSOLE {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_talendLogs_CONSOLE().limitLog4jByte();

				// /////////////////////

				final String OUTPUT_FIELD_SEPARATOR_talendLogs_CONSOLE = "|";
				java.io.PrintStream consoleOut_talendLogs_CONSOLE = null;

				StringBuilder strBuffer_talendLogs_CONSOLE = null;
				int nb_line_talendLogs_CONSOLE = 0;
				// /////////////////////

				/**
				 * [talendLogs_CONSOLE begin ] stop
				 */

				/**
				 * [talendLogs_DB begin ] start
				 */

				ok_Hash.put("talendLogs_DB", false);
				start_Hash.put("talendLogs_DB", System.currentTimeMillis());

				currentVirtualComponent = "talendLogs_DB";

				currentComponent = "talendLogs_DB";

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null) {

						runStat.updateStatOnConnection("Main" + iterateId, 0, 0);

					}
				}

				int tos_count_talendLogs_DB = 0;

				class BytesLimit65535_talendLogs_DB {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_talendLogs_DB().limitLog4jByte();

				int nb_line_talendLogs_DB = 0;
				int nb_line_update_talendLogs_DB = 0;
				int nb_line_inserted_talendLogs_DB = 0;
				int nb_line_deleted_talendLogs_DB = 0;
				int nb_line_rejected_talendLogs_DB = 0;

				int deletedCount_talendLogs_DB = 0;
				int updatedCount_talendLogs_DB = 0;
				int insertedCount_talendLogs_DB = 0;

				int rejectedCount_talendLogs_DB = 0;

				String tableName_talendLogs_DB = "jobLogs";
				boolean whetherReject_talendLogs_DB = false;

				java.util.Calendar calendar_talendLogs_DB = java.util.Calendar
						.getInstance();
				calendar_talendLogs_DB.set(1, 0, 1, 0, 0, 0);
				long year1_talendLogs_DB = calendar_talendLogs_DB.getTime()
						.getTime();
				calendar_talendLogs_DB.set(10000, 0, 1, 0, 0, 0);
				long year10000_talendLogs_DB = calendar_talendLogs_DB.getTime()
						.getTime();
				long date_talendLogs_DB;

				java.sql.Connection conn_talendLogs_DB = null;
				conn_talendLogs_DB = (java.sql.Connection) globalMap
						.get("conn_connectionStatsLogs");

				int count_talendLogs_DB = 0;

				// [%connection%][checktable][tableName]
				String keyCheckTable_talendLogs_DB = conn_talendLogs_DB
						+ "[checktable]" + "[" + "jobLogs" + "]";

				if (GlobalResource.resourceMap.get(keyCheckTable_talendLogs_DB) == null) {// }

					synchronized (GlobalResource.resourceLockMap
							.get(keyCheckTable_talendLogs_DB)) {// }
						if (GlobalResource.resourceMap
								.get(keyCheckTable_talendLogs_DB) == null) {// }
							java.sql.DatabaseMetaData dbMetaData_talendLogs_DB = conn_talendLogs_DB
									.getMetaData();
							java.sql.ResultSet rsTable_talendLogs_DB = dbMetaData_talendLogs_DB
									.getTables(null, null, null,
											new String[] { "TABLE" });
							boolean whetherExist_talendLogs_DB = false;
							while (rsTable_talendLogs_DB.next()) {
								String table_talendLogs_DB = rsTable_talendLogs_DB
										.getString("TABLE_NAME");
								if (table_talendLogs_DB
										.equalsIgnoreCase("jobLogs")) {
									whetherExist_talendLogs_DB = true;
									break;
								}
							}
							rsTable_talendLogs_DB.close();
							if (!whetherExist_talendLogs_DB) {
								java.sql.Statement stmtCreate_talendLogs_DB = conn_talendLogs_DB
										.createStatement();
								stmtCreate_talendLogs_DB
										.execute("CREATE TABLE `"
												+ tableName_talendLogs_DB
												+ "`(`moment` DATETIME ,`pid` VARCHAR(20)  ,`root_pid` VARCHAR(20)  ,`father_pid` VARCHAR(20)  ,`project` VARCHAR(50)  ,`job` VARCHAR(255)  ,`context` VARCHAR(50)  ,`priority` INT(3)  ,`type` VARCHAR(255)  ,`origin` VARCHAR(255)  ,`message` VARCHAR(255)  ,`code` INT(3)  )");
								stmtCreate_talendLogs_DB.close();
							}
							GlobalResource.resourceMap.put(
									keyCheckTable_talendLogs_DB, true);
							// {{{
						} // end of if
					} // end synchronized
				}

				String insert_talendLogs_DB = "INSERT INTO `"
						+ "jobLogs"
						+ "` (`moment`,`pid`,`root_pid`,`father_pid`,`project`,`job`,`context`,`priority`,`type`,`origin`,`message`,`code`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";

				java.sql.PreparedStatement pstmt_talendLogs_DB = null;
				// [%connection%][psmt][tableName]
				String keyPsmt_talendLogs_DB = conn_talendLogs_DB + "[psmt]"
						+ "[" + "jobLogs" + "]";
				pstmt_talendLogs_DB = SharedDBPreparedStatement
						.getSharedPreparedStatement(conn_talendLogs_DB,
								insert_talendLogs_DB, keyPsmt_talendLogs_DB);

				/**
				 * [talendLogs_DB begin ] stop
				 */

				/**
				 * [talendLogs_LOGS begin ] start
				 */

				ok_Hash.put("talendLogs_LOGS", false);
				start_Hash.put("talendLogs_LOGS", System.currentTimeMillis());

				currentVirtualComponent = "talendLogs_LOGS";

				currentComponent = "talendLogs_LOGS";

				int tos_count_talendLogs_LOGS = 0;

				class BytesLimit65535_talendLogs_LOGS {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_talendLogs_LOGS().limitLog4jByte();

				try {
					for (LogCatcherUtils.LogCatcherMessage lcm : talendLogs_LOGS
							.getMessages()) {
						row_talendLogs_LOGS.type = lcm.getType();
						row_talendLogs_LOGS.origin = (lcm.getOrigin() == null
								|| lcm.getOrigin().length() < 1 ? null : lcm
								.getOrigin());
						row_talendLogs_LOGS.priority = lcm.getPriority();
						row_talendLogs_LOGS.message = lcm.getMessage();
						row_talendLogs_LOGS.code = lcm.getCode();

						row_talendLogs_LOGS.moment = java.util.Calendar
								.getInstance().getTime();

						row_talendLogs_LOGS.pid = pid;
						row_talendLogs_LOGS.root_pid = rootPid;
						row_talendLogs_LOGS.father_pid = fatherPid;

						row_talendLogs_LOGS.project = projectName;
						row_talendLogs_LOGS.job = jobName;
						row_talendLogs_LOGS.context = contextStr;

						/**
						 * [talendLogs_LOGS begin ] stop
						 */

						/**
						 * [talendLogs_LOGS main ] start
						 */

						currentVirtualComponent = "talendLogs_LOGS";

						currentComponent = "talendLogs_LOGS";

						tos_count_talendLogs_LOGS++;

						/**
						 * [talendLogs_LOGS main ] stop
						 */

						/**
						 * [talendLogs_LOGS process_data_begin ] start
						 */

						currentVirtualComponent = "talendLogs_LOGS";

						currentComponent = "talendLogs_LOGS";

						/**
						 * [talendLogs_LOGS process_data_begin ] stop
						 */

						/**
						 * [talendLogs_DB main ] start
						 */

						currentVirtualComponent = "talendLogs_DB";

						currentComponent = "talendLogs_DB";

						// Main
						// row_talendLogs_LOGS

						if (execStat) {
							runStat.updateStatOnConnection("Main" + iterateId,
									1, 1);
						}

						row_talendLogs_DB = null;
						whetherReject_talendLogs_DB = false;
						if (row_talendLogs_LOGS.moment != null) {
							date_talendLogs_DB = row_talendLogs_LOGS.moment
									.getTime();
							if (date_talendLogs_DB < year1_talendLogs_DB
									|| date_talendLogs_DB >= year10000_talendLogs_DB) {
								pstmt_talendLogs_DB.setString(1,
										"0000-00-00 00:00:00");
							} else {
								pstmt_talendLogs_DB.setTimestamp(1,
										new java.sql.Timestamp(
												date_talendLogs_DB));
							}
						} else {
							pstmt_talendLogs_DB.setNull(1, java.sql.Types.DATE);
						}

						if (row_talendLogs_LOGS.pid == null) {
							pstmt_talendLogs_DB.setNull(2,
									java.sql.Types.VARCHAR);
						} else {
							pstmt_talendLogs_DB.setString(2,
									row_talendLogs_LOGS.pid);
						}

						if (row_talendLogs_LOGS.root_pid == null) {
							pstmt_talendLogs_DB.setNull(3,
									java.sql.Types.VARCHAR);
						} else {
							pstmt_talendLogs_DB.setString(3,
									row_talendLogs_LOGS.root_pid);
						}

						if (row_talendLogs_LOGS.father_pid == null) {
							pstmt_talendLogs_DB.setNull(4,
									java.sql.Types.VARCHAR);
						} else {
							pstmt_talendLogs_DB.setString(4,
									row_talendLogs_LOGS.father_pid);
						}

						if (row_talendLogs_LOGS.project == null) {
							pstmt_talendLogs_DB.setNull(5,
									java.sql.Types.VARCHAR);
						} else {
							pstmt_talendLogs_DB.setString(5,
									row_talendLogs_LOGS.project);
						}

						if (row_talendLogs_LOGS.job == null) {
							pstmt_talendLogs_DB.setNull(6,
									java.sql.Types.VARCHAR);
						} else {
							pstmt_talendLogs_DB.setString(6,
									row_talendLogs_LOGS.job);
						}

						if (row_talendLogs_LOGS.context == null) {
							pstmt_talendLogs_DB.setNull(7,
									java.sql.Types.VARCHAR);
						} else {
							pstmt_talendLogs_DB.setString(7,
									row_talendLogs_LOGS.context);
						}

						if (row_talendLogs_LOGS.priority == null) {
							pstmt_talendLogs_DB.setNull(8,
									java.sql.Types.INTEGER);
						} else {
							pstmt_talendLogs_DB.setInt(8,
									row_talendLogs_LOGS.priority);
						}

						if (row_talendLogs_LOGS.type == null) {
							pstmt_talendLogs_DB.setNull(9,
									java.sql.Types.VARCHAR);
						} else {
							pstmt_talendLogs_DB.setString(9,
									row_talendLogs_LOGS.type);
						}

						if (row_talendLogs_LOGS.origin == null) {
							pstmt_talendLogs_DB.setNull(10,
									java.sql.Types.VARCHAR);
						} else {
							pstmt_talendLogs_DB.setString(10,
									row_talendLogs_LOGS.origin);
						}

						if (row_talendLogs_LOGS.message == null) {
							pstmt_talendLogs_DB.setNull(11,
									java.sql.Types.VARCHAR);
						} else {
							pstmt_talendLogs_DB.setString(11,
									row_talendLogs_LOGS.message);
						}

						if (row_talendLogs_LOGS.code == null) {
							pstmt_talendLogs_DB.setNull(12,
									java.sql.Types.INTEGER);
						} else {
							pstmt_talendLogs_DB.setInt(12,
									row_talendLogs_LOGS.code);
						}

						try {
							nb_line_talendLogs_DB++;
							insertedCount_talendLogs_DB = insertedCount_talendLogs_DB
									+ pstmt_talendLogs_DB.executeUpdate();
						} catch (java.lang.Exception e) {
							whetherReject_talendLogs_DB = true;
							System.err.print(e.getMessage());
						}
						if (!whetherReject_talendLogs_DB) {
							row_talendLogs_DB = new row_talendLogs_DBStruct();
							row_talendLogs_DB.moment = row_talendLogs_LOGS.moment;
							row_talendLogs_DB.pid = row_talendLogs_LOGS.pid;
							row_talendLogs_DB.root_pid = row_talendLogs_LOGS.root_pid;
							row_talendLogs_DB.father_pid = row_talendLogs_LOGS.father_pid;
							row_talendLogs_DB.project = row_talendLogs_LOGS.project;
							row_talendLogs_DB.job = row_talendLogs_LOGS.job;
							row_talendLogs_DB.context = row_talendLogs_LOGS.context;
							row_talendLogs_DB.priority = row_talendLogs_LOGS.priority;
							row_talendLogs_DB.type = row_talendLogs_LOGS.type;
							row_talendLogs_DB.origin = row_talendLogs_LOGS.origin;
							row_talendLogs_DB.message = row_talendLogs_LOGS.message;
							row_talendLogs_DB.code = row_talendLogs_LOGS.code;
						}

						tos_count_talendLogs_DB++;

						/**
						 * [talendLogs_DB main ] stop
						 */

						/**
						 * [talendLogs_DB process_data_begin ] start
						 */

						currentVirtualComponent = "talendLogs_DB";

						currentComponent = "talendLogs_DB";

						/**
						 * [talendLogs_DB process_data_begin ] stop
						 */
						// Start of branch "row_talendLogs_DB"
						if (row_talendLogs_DB != null) {

							/**
							 * [talendLogs_CONSOLE main ] start
							 */

							currentVirtualComponent = "talendLogs_CONSOLE";

							currentComponent = "talendLogs_CONSOLE";

							// Main
							// row_talendLogs_DB

							if (execStat) {
								runStat.updateStatOnConnection("Main"
										+ iterateId, 1, 1);
							}

							// /////////////////////

							strBuffer_talendLogs_CONSOLE = new StringBuilder();

							if (row_talendLogs_DB.moment != null) { //

								strBuffer_talendLogs_CONSOLE
										.append(FormatterUtils.format_Date(
												row_talendLogs_DB.moment,
												"yyyy-MM-dd HH:mm:ss"));

							} //

							strBuffer_talendLogs_CONSOLE.append("|");

							if (row_talendLogs_DB.pid != null) { //

								strBuffer_talendLogs_CONSOLE.append(String
										.valueOf(row_talendLogs_DB.pid));

							} //

							strBuffer_talendLogs_CONSOLE.append("|");

							if (row_talendLogs_DB.root_pid != null) { //

								strBuffer_talendLogs_CONSOLE.append(String
										.valueOf(row_talendLogs_DB.root_pid));

							} //

							strBuffer_talendLogs_CONSOLE.append("|");

							if (row_talendLogs_DB.father_pid != null) { //

								strBuffer_talendLogs_CONSOLE.append(String
										.valueOf(row_talendLogs_DB.father_pid));

							} //

							strBuffer_talendLogs_CONSOLE.append("|");

							if (row_talendLogs_DB.project != null) { //

								strBuffer_talendLogs_CONSOLE.append(String
										.valueOf(row_talendLogs_DB.project));

							} //

							strBuffer_talendLogs_CONSOLE.append("|");

							if (row_talendLogs_DB.job != null) { //

								strBuffer_talendLogs_CONSOLE.append(String
										.valueOf(row_talendLogs_DB.job));

							} //

							strBuffer_talendLogs_CONSOLE.append("|");

							if (row_talendLogs_DB.context != null) { //

								strBuffer_talendLogs_CONSOLE.append(String
										.valueOf(row_talendLogs_DB.context));

							} //

							strBuffer_talendLogs_CONSOLE.append("|");

							if (row_talendLogs_DB.priority != null) { //

								strBuffer_talendLogs_CONSOLE.append(String
										.valueOf(row_talendLogs_DB.priority));

							} //

							strBuffer_talendLogs_CONSOLE.append("|");

							if (row_talendLogs_DB.type != null) { //

								strBuffer_talendLogs_CONSOLE.append(String
										.valueOf(row_talendLogs_DB.type));

							} //

							strBuffer_talendLogs_CONSOLE.append("|");

							if (row_talendLogs_DB.origin != null) { //

								strBuffer_talendLogs_CONSOLE.append(String
										.valueOf(row_talendLogs_DB.origin));

							} //

							strBuffer_talendLogs_CONSOLE.append("|");

							if (row_talendLogs_DB.message != null) { //

								strBuffer_talendLogs_CONSOLE.append(String
										.valueOf(row_talendLogs_DB.message));

							} //

							strBuffer_talendLogs_CONSOLE.append("|");

							if (row_talendLogs_DB.code != null) { //

								strBuffer_talendLogs_CONSOLE.append(String
										.valueOf(row_talendLogs_DB.code));

							} //

							if (globalMap.get("tLogRow_CONSOLE") != null) {
								consoleOut_talendLogs_CONSOLE = (java.io.PrintStream) globalMap
										.get("tLogRow_CONSOLE");
							} else {
								consoleOut_talendLogs_CONSOLE = new java.io.PrintStream(
										new java.io.BufferedOutputStream(
												System.out));
								globalMap.put("tLogRow_CONSOLE",
										consoleOut_talendLogs_CONSOLE);
							}
							consoleOut_talendLogs_CONSOLE
									.println(strBuffer_talendLogs_CONSOLE
											.toString());
							consoleOut_talendLogs_CONSOLE.flush();
							nb_line_talendLogs_CONSOLE++;
							// ////

							// ////

							// /////////////////////

							tos_count_talendLogs_CONSOLE++;

							/**
							 * [talendLogs_CONSOLE main ] stop
							 */

							/**
							 * [talendLogs_CONSOLE process_data_begin ] start
							 */

							currentVirtualComponent = "talendLogs_CONSOLE";

							currentComponent = "talendLogs_CONSOLE";

							/**
							 * [talendLogs_CONSOLE process_data_begin ] stop
							 */

							/**
							 * [talendLogs_CONSOLE process_data_end ] start
							 */

							currentVirtualComponent = "talendLogs_CONSOLE";

							currentComponent = "talendLogs_CONSOLE";

							/**
							 * [talendLogs_CONSOLE process_data_end ] stop
							 */

						} // End of branch "row_talendLogs_DB"

						/**
						 * [talendLogs_DB process_data_end ] start
						 */

						currentVirtualComponent = "talendLogs_DB";

						currentComponent = "talendLogs_DB";

						/**
						 * [talendLogs_DB process_data_end ] stop
						 */

						/**
						 * [talendLogs_LOGS process_data_end ] start
						 */

						currentVirtualComponent = "talendLogs_LOGS";

						currentComponent = "talendLogs_LOGS";

						/**
						 * [talendLogs_LOGS process_data_end ] stop
						 */

						/**
						 * [talendLogs_LOGS end ] start
						 */

						currentVirtualComponent = "talendLogs_LOGS";

						currentComponent = "talendLogs_LOGS";

					}
				} catch (Exception e_talendLogs_LOGS) {
					logIgnoredError(
							String.format(
									"talendLogs_LOGS - tLogCatcher failed to process log message(s) due to internal error: %s",
									e_talendLogs_LOGS), e_talendLogs_LOGS);
				}

				ok_Hash.put("talendLogs_LOGS", true);
				end_Hash.put("talendLogs_LOGS", System.currentTimeMillis());

				/**
				 * [talendLogs_LOGS end ] stop
				 */

				/**
				 * [talendLogs_DB end ] start
				 */

				currentVirtualComponent = "talendLogs_DB";

				currentComponent = "talendLogs_DB";

				if (pstmt_talendLogs_DB != null) {

					SharedDBPreparedStatement
							.releasePreparedStatement(keyPsmt_talendLogs_DB);

				}

				nb_line_deleted_talendLogs_DB = nb_line_deleted_talendLogs_DB
						+ deletedCount_talendLogs_DB;
				nb_line_update_talendLogs_DB = nb_line_update_talendLogs_DB
						+ updatedCount_talendLogs_DB;
				nb_line_inserted_talendLogs_DB = nb_line_inserted_talendLogs_DB
						+ insertedCount_talendLogs_DB;
				nb_line_rejected_talendLogs_DB = nb_line_rejected_talendLogs_DB
						+ rejectedCount_talendLogs_DB;

				globalMap.put("talendLogs_DB_NB_LINE", nb_line_talendLogs_DB);
				globalMap.put("talendLogs_DB_NB_LINE_UPDATED",
						nb_line_update_talendLogs_DB);
				globalMap.put("talendLogs_DB_NB_LINE_INSERTED",
						nb_line_inserted_talendLogs_DB);
				globalMap.put("talendLogs_DB_NB_LINE_DELETED",
						nb_line_deleted_talendLogs_DB);
				globalMap.put("talendLogs_DB_NB_LINE_REJECTED",
						nb_line_rejected_talendLogs_DB);

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null
							|| !((Boolean) resourceMap.get("inIterateVComp"))) {
						runStat.updateStatOnConnection("Main" + iterateId, 2, 0);
					}
				}

				ok_Hash.put("talendLogs_DB", true);
				end_Hash.put("talendLogs_DB", System.currentTimeMillis());

				/**
				 * [talendLogs_DB end ] stop
				 */

				/**
				 * [talendLogs_CONSOLE end ] start
				 */

				currentVirtualComponent = "talendLogs_CONSOLE";

				currentComponent = "talendLogs_CONSOLE";

				// ////
				// ////
				globalMap.put("talendLogs_CONSOLE_NB_LINE",
						nb_line_talendLogs_CONSOLE);

				// /////////////////////

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null
							|| !((Boolean) resourceMap.get("inIterateVComp"))) {
						runStat.updateStatOnConnection("Main" + iterateId, 2, 0);
					}
				}

				ok_Hash.put("talendLogs_CONSOLE", true);
				end_Hash.put("talendLogs_CONSOLE", System.currentTimeMillis());

				/**
				 * [talendLogs_CONSOLE end ] stop
				 */

			}// end the resume

			if (resumeEntryMethodName == null || globalResumeTicket) {
				resumeUtil
						.addLog("CHECKPOINT",
								"CONNECTION:SUBJOB_OK:talendLogs_LOGS:sub_ok_talendLogs_connectionStatsLogs_Commit",
								"", Thread.currentThread().getId() + "", "",
								"", "", "", "");
			}

			if (execStat) {
				runStat.updateStatOnConnection(
						"sub_ok_talendLogs_connectionStatsLogs_Commit", 0, "ok");
			}

			connectionStatsLogs_CommitProcess(globalMap);

		} catch (java.lang.Exception e) {

			TalendException te = new TalendException(e, currentComponent,
					globalMap);

			te.setVirtualComponentName(currentVirtualComponent);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			try {

				/**
				 * [talendLogs_LOGS finally ] start
				 */

				currentVirtualComponent = "talendLogs_LOGS";

				currentComponent = "talendLogs_LOGS";

				/**
				 * [talendLogs_LOGS finally ] stop
				 */

				/**
				 * [talendLogs_DB finally ] start
				 */

				currentVirtualComponent = "talendLogs_DB";

				currentComponent = "talendLogs_DB";

				/**
				 * [talendLogs_DB finally ] stop
				 */

				/**
				 * [talendLogs_CONSOLE finally ] start
				 */

				currentVirtualComponent = "talendLogs_CONSOLE";

				currentComponent = "talendLogs_CONSOLE";

				/**
				 * [talendLogs_CONSOLE finally ] stop
				 */

			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("talendLogs_LOGS_SUBPROCESS_STATE", 1);
	}

	public static class row_talendMeter_DBStruct implements
			routines.system.IPersistableRow<row_talendMeter_DBStruct> {
		final static byte[] commonByteArrayLock_GIFMIS_PaymentRequestAllJobs = new byte[0];
		static byte[] commonByteArray_GIFMIS_PaymentRequestAllJobs = new byte[0];

		public java.util.Date moment;

		public java.util.Date getMoment() {
			return this.moment;
		}

		public String pid;

		public String getPid() {
			return this.pid;
		}

		public String father_pid;

		public String getFather_pid() {
			return this.father_pid;
		}

		public String root_pid;

		public String getRoot_pid() {
			return this.root_pid;
		}

		public Long system_pid;

		public Long getSystem_pid() {
			return this.system_pid;
		}

		public String project;

		public String getProject() {
			return this.project;
		}

		public String job;

		public String getJob() {
			return this.job;
		}

		public String job_repository_id;

		public String getJob_repository_id() {
			return this.job_repository_id;
		}

		public String job_version;

		public String getJob_version() {
			return this.job_version;
		}

		public String context;

		public String getContext() {
			return this.context;
		}

		public String origin;

		public String getOrigin() {
			return this.origin;
		}

		public String label;

		public String getLabel() {
			return this.label;
		}

		public Integer count;

		public Integer getCount() {
			return this.count;
		}

		public Integer reference;

		public Integer getReference() {
			return this.reference;
		}

		public String thresholds;

		public String getThresholds() {
			return this.thresholds;
		}

		private java.util.Date readDate(ObjectInputStream dis)
				throws IOException {
			java.util.Date dateReturn = null;
			int length = 0;
			length = dis.readByte();
			if (length == -1) {
				dateReturn = null;
			} else {
				dateReturn = new Date(dis.readLong());
			}
			return dateReturn;
		}

		private void writeDate(java.util.Date date1, ObjectOutputStream dos)
				throws IOException {
			if (date1 == null) {
				dos.writeByte(-1);
			} else {
				dos.writeByte(0);
				dos.writeLong(date1.getTime());
			}
		}

		private String readString(ObjectInputStream dis) throws IOException {
			String strReturn = null;
			int length = 0;
			length = dis.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				if (length > commonByteArray_GIFMIS_PaymentRequestAllJobs.length) {
					if (length < 1024
							&& commonByteArray_GIFMIS_PaymentRequestAllJobs.length == 0) {
						commonByteArray_GIFMIS_PaymentRequestAllJobs = new byte[1024];
					} else {
						commonByteArray_GIFMIS_PaymentRequestAllJobs = new byte[2 * length];
					}
				}
				dis.readFully(commonByteArray_GIFMIS_PaymentRequestAllJobs, 0,
						length);
				strReturn = new String(
						commonByteArray_GIFMIS_PaymentRequestAllJobs, 0,
						length, utf8Charset);
			}
			return strReturn;
		}

		private void writeString(String str, ObjectOutputStream dos)
				throws IOException {
			if (str == null) {
				dos.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				dos.writeInt(byteArray.length);
				dos.write(byteArray);
			}
		}

		private Integer readInteger(ObjectInputStream dis) throws IOException {
			Integer intReturn;
			int length = 0;
			length = dis.readByte();
			if (length == -1) {
				intReturn = null;
			} else {
				intReturn = dis.readInt();
			}
			return intReturn;
		}

		private void writeInteger(Integer intNum, ObjectOutputStream dos)
				throws IOException {
			if (intNum == null) {
				dos.writeByte(-1);
			} else {
				dos.writeByte(0);
				dos.writeInt(intNum);
			}
		}

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_GIFMIS_PaymentRequestAllJobs) {

				try {

					int length = 0;

					this.moment = readDate(dis);

					this.pid = readString(dis);

					this.father_pid = readString(dis);

					this.root_pid = readString(dis);

					length = dis.readByte();
					if (length == -1) {
						this.system_pid = null;
					} else {
						this.system_pid = dis.readLong();
					}

					this.project = readString(dis);

					this.job = readString(dis);

					this.job_repository_id = readString(dis);

					this.job_version = readString(dis);

					this.context = readString(dis);

					this.origin = readString(dis);

					this.label = readString(dis);

					this.count = readInteger(dis);

					this.reference = readInteger(dis);

					this.thresholds = readString(dis);

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

				// java.util.Date

				writeDate(this.moment, dos);

				// String

				writeString(this.pid, dos);

				// String

				writeString(this.father_pid, dos);

				// String

				writeString(this.root_pid, dos);

				// Long

				if (this.system_pid == null) {
					dos.writeByte(-1);
				} else {
					dos.writeByte(0);
					dos.writeLong(this.system_pid);
				}

				// String

				writeString(this.project, dos);

				// String

				writeString(this.job, dos);

				// String

				writeString(this.job_repository_id, dos);

				// String

				writeString(this.job_version, dos);

				// String

				writeString(this.context, dos);

				// String

				writeString(this.origin, dos);

				// String

				writeString(this.label, dos);

				// Integer

				writeInteger(this.count, dos);

				// Integer

				writeInteger(this.reference, dos);

				// String

				writeString(this.thresholds, dos);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("moment=" + String.valueOf(moment));
			sb.append(",pid=" + pid);
			sb.append(",father_pid=" + father_pid);
			sb.append(",root_pid=" + root_pid);
			sb.append(",system_pid=" + String.valueOf(system_pid));
			sb.append(",project=" + project);
			sb.append(",job=" + job);
			sb.append(",job_repository_id=" + job_repository_id);
			sb.append(",job_version=" + job_version);
			sb.append(",context=" + context);
			sb.append(",origin=" + origin);
			sb.append(",label=" + label);
			sb.append(",count=" + String.valueOf(count));
			sb.append(",reference=" + String.valueOf(reference));
			sb.append(",thresholds=" + thresholds);
			sb.append("]");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(row_talendMeter_DBStruct other) {

			int returnValue = -1;

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(),
						object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public static class row_talendMeter_METTERStruct implements
			routines.system.IPersistableRow<row_talendMeter_METTERStruct> {
		final static byte[] commonByteArrayLock_GIFMIS_PaymentRequestAllJobs = new byte[0];
		static byte[] commonByteArray_GIFMIS_PaymentRequestAllJobs = new byte[0];

		public java.util.Date moment;

		public java.util.Date getMoment() {
			return this.moment;
		}

		public String pid;

		public String getPid() {
			return this.pid;
		}

		public String father_pid;

		public String getFather_pid() {
			return this.father_pid;
		}

		public String root_pid;

		public String getRoot_pid() {
			return this.root_pid;
		}

		public Long system_pid;

		public Long getSystem_pid() {
			return this.system_pid;
		}

		public String project;

		public String getProject() {
			return this.project;
		}

		public String job;

		public String getJob() {
			return this.job;
		}

		public String job_repository_id;

		public String getJob_repository_id() {
			return this.job_repository_id;
		}

		public String job_version;

		public String getJob_version() {
			return this.job_version;
		}

		public String context;

		public String getContext() {
			return this.context;
		}

		public String origin;

		public String getOrigin() {
			return this.origin;
		}

		public String label;

		public String getLabel() {
			return this.label;
		}

		public Integer count;

		public Integer getCount() {
			return this.count;
		}

		public Integer reference;

		public Integer getReference() {
			return this.reference;
		}

		public String thresholds;

		public String getThresholds() {
			return this.thresholds;
		}

		private java.util.Date readDate(ObjectInputStream dis)
				throws IOException {
			java.util.Date dateReturn = null;
			int length = 0;
			length = dis.readByte();
			if (length == -1) {
				dateReturn = null;
			} else {
				dateReturn = new Date(dis.readLong());
			}
			return dateReturn;
		}

		private void writeDate(java.util.Date date1, ObjectOutputStream dos)
				throws IOException {
			if (date1 == null) {
				dos.writeByte(-1);
			} else {
				dos.writeByte(0);
				dos.writeLong(date1.getTime());
			}
		}

		private String readString(ObjectInputStream dis) throws IOException {
			String strReturn = null;
			int length = 0;
			length = dis.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				if (length > commonByteArray_GIFMIS_PaymentRequestAllJobs.length) {
					if (length < 1024
							&& commonByteArray_GIFMIS_PaymentRequestAllJobs.length == 0) {
						commonByteArray_GIFMIS_PaymentRequestAllJobs = new byte[1024];
					} else {
						commonByteArray_GIFMIS_PaymentRequestAllJobs = new byte[2 * length];
					}
				}
				dis.readFully(commonByteArray_GIFMIS_PaymentRequestAllJobs, 0,
						length);
				strReturn = new String(
						commonByteArray_GIFMIS_PaymentRequestAllJobs, 0,
						length, utf8Charset);
			}
			return strReturn;
		}

		private void writeString(String str, ObjectOutputStream dos)
				throws IOException {
			if (str == null) {
				dos.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				dos.writeInt(byteArray.length);
				dos.write(byteArray);
			}
		}

		private Integer readInteger(ObjectInputStream dis) throws IOException {
			Integer intReturn;
			int length = 0;
			length = dis.readByte();
			if (length == -1) {
				intReturn = null;
			} else {
				intReturn = dis.readInt();
			}
			return intReturn;
		}

		private void writeInteger(Integer intNum, ObjectOutputStream dos)
				throws IOException {
			if (intNum == null) {
				dos.writeByte(-1);
			} else {
				dos.writeByte(0);
				dos.writeInt(intNum);
			}
		}

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_GIFMIS_PaymentRequestAllJobs) {

				try {

					int length = 0;

					this.moment = readDate(dis);

					this.pid = readString(dis);

					this.father_pid = readString(dis);

					this.root_pid = readString(dis);

					length = dis.readByte();
					if (length == -1) {
						this.system_pid = null;
					} else {
						this.system_pid = dis.readLong();
					}

					this.project = readString(dis);

					this.job = readString(dis);

					this.job_repository_id = readString(dis);

					this.job_version = readString(dis);

					this.context = readString(dis);

					this.origin = readString(dis);

					this.label = readString(dis);

					this.count = readInteger(dis);

					this.reference = readInteger(dis);

					this.thresholds = readString(dis);

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

				// java.util.Date

				writeDate(this.moment, dos);

				// String

				writeString(this.pid, dos);

				// String

				writeString(this.father_pid, dos);

				// String

				writeString(this.root_pid, dos);

				// Long

				if (this.system_pid == null) {
					dos.writeByte(-1);
				} else {
					dos.writeByte(0);
					dos.writeLong(this.system_pid);
				}

				// String

				writeString(this.project, dos);

				// String

				writeString(this.job, dos);

				// String

				writeString(this.job_repository_id, dos);

				// String

				writeString(this.job_version, dos);

				// String

				writeString(this.context, dos);

				// String

				writeString(this.origin, dos);

				// String

				writeString(this.label, dos);

				// Integer

				writeInteger(this.count, dos);

				// Integer

				writeInteger(this.reference, dos);

				// String

				writeString(this.thresholds, dos);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("moment=" + String.valueOf(moment));
			sb.append(",pid=" + pid);
			sb.append(",father_pid=" + father_pid);
			sb.append(",root_pid=" + root_pid);
			sb.append(",system_pid=" + String.valueOf(system_pid));
			sb.append(",project=" + project);
			sb.append(",job=" + job);
			sb.append(",job_repository_id=" + job_repository_id);
			sb.append(",job_version=" + job_version);
			sb.append(",context=" + context);
			sb.append(",origin=" + origin);
			sb.append(",label=" + label);
			sb.append(",count=" + String.valueOf(count));
			sb.append(",reference=" + String.valueOf(reference));
			sb.append(",thresholds=" + thresholds);
			sb.append("]");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(row_talendMeter_METTERStruct other) {

			int returnValue = -1;

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(),
						object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public void talendMeter_METTERProcess(
			final java.util.Map<String, Object> globalMap)
			throws TalendException {
		globalMap.put("talendMeter_METTER_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;
		String currentVirtualComponent = null;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception()
						.getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				row_talendMeter_METTERStruct row_talendMeter_METTER = new row_talendMeter_METTERStruct();
				row_talendMeter_DBStruct row_talendMeter_DB = new row_talendMeter_DBStruct();

				/**
				 * [talendMeter_CONSOLE begin ] start
				 */

				ok_Hash.put("talendMeter_CONSOLE", false);
				start_Hash.put("talendMeter_CONSOLE",
						System.currentTimeMillis());

				currentVirtualComponent = "talendMeter_CONSOLE";

				currentComponent = "talendMeter_CONSOLE";

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null) {

						runStat.updateStatOnConnection("Main" + iterateId, 0, 0);

					}
				}

				int tos_count_talendMeter_CONSOLE = 0;

				class BytesLimit65535_talendMeter_CONSOLE {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_talendMeter_CONSOLE().limitLog4jByte();

				// /////////////////////

				final String OUTPUT_FIELD_SEPARATOR_talendMeter_CONSOLE = "|";
				java.io.PrintStream consoleOut_talendMeter_CONSOLE = null;

				StringBuilder strBuffer_talendMeter_CONSOLE = null;
				int nb_line_talendMeter_CONSOLE = 0;
				// /////////////////////

				/**
				 * [talendMeter_CONSOLE begin ] stop
				 */

				/**
				 * [talendMeter_DB begin ] start
				 */

				ok_Hash.put("talendMeter_DB", false);
				start_Hash.put("talendMeter_DB", System.currentTimeMillis());

				currentVirtualComponent = "talendMeter_DB";

				currentComponent = "talendMeter_DB";

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null) {

						runStat.updateStatOnConnection("Main" + iterateId, 0, 0);

					}
				}

				int tos_count_talendMeter_DB = 0;

				class BytesLimit65535_talendMeter_DB {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_talendMeter_DB().limitLog4jByte();

				int nb_line_talendMeter_DB = 0;
				int nb_line_update_talendMeter_DB = 0;
				int nb_line_inserted_talendMeter_DB = 0;
				int nb_line_deleted_talendMeter_DB = 0;
				int nb_line_rejected_talendMeter_DB = 0;

				int deletedCount_talendMeter_DB = 0;
				int updatedCount_talendMeter_DB = 0;
				int insertedCount_talendMeter_DB = 0;

				int rejectedCount_talendMeter_DB = 0;

				String tableName_talendMeter_DB = "jobMeters";
				boolean whetherReject_talendMeter_DB = false;

				java.util.Calendar calendar_talendMeter_DB = java.util.Calendar
						.getInstance();
				calendar_talendMeter_DB.set(1, 0, 1, 0, 0, 0);
				long year1_talendMeter_DB = calendar_talendMeter_DB.getTime()
						.getTime();
				calendar_talendMeter_DB.set(10000, 0, 1, 0, 0, 0);
				long year10000_talendMeter_DB = calendar_talendMeter_DB
						.getTime().getTime();
				long date_talendMeter_DB;

				java.sql.Connection conn_talendMeter_DB = null;
				conn_talendMeter_DB = (java.sql.Connection) globalMap
						.get("conn_connectionStatsLogs");

				int count_talendMeter_DB = 0;

				// [%connection%][checktable][tableName]
				String keyCheckTable_talendMeter_DB = conn_talendMeter_DB
						+ "[checktable]" + "[" + "jobMeters" + "]";

				if (GlobalResource.resourceMap
						.get(keyCheckTable_talendMeter_DB) == null) {// }

					synchronized (GlobalResource.resourceLockMap
							.get(keyCheckTable_talendMeter_DB)) {// }
						if (GlobalResource.resourceMap
								.get(keyCheckTable_talendMeter_DB) == null) {// }
							java.sql.DatabaseMetaData dbMetaData_talendMeter_DB = conn_talendMeter_DB
									.getMetaData();
							java.sql.ResultSet rsTable_talendMeter_DB = dbMetaData_talendMeter_DB
									.getTables(null, null, null,
											new String[] { "TABLE" });
							boolean whetherExist_talendMeter_DB = false;
							while (rsTable_talendMeter_DB.next()) {
								String table_talendMeter_DB = rsTable_talendMeter_DB
										.getString("TABLE_NAME");
								if (table_talendMeter_DB
										.equalsIgnoreCase("jobMeters")) {
									whetherExist_talendMeter_DB = true;
									break;
								}
							}
							rsTable_talendMeter_DB.close();
							if (!whetherExist_talendMeter_DB) {
								java.sql.Statement stmtCreate_talendMeter_DB = conn_talendMeter_DB
										.createStatement();
								stmtCreate_talendMeter_DB
										.execute("CREATE TABLE `"
												+ tableName_talendMeter_DB
												+ "`(`moment` DATETIME ,`pid` VARCHAR(20)  ,`father_pid` VARCHAR(20)  ,`root_pid` VARCHAR(20)  ,`system_pid` BIGINT(8)  ,`project` VARCHAR(50)  ,`job` VARCHAR(255)  ,`job_repository_id` VARCHAR(255)  ,`job_version` VARCHAR(255)  ,`context` VARCHAR(50)  ,`origin` VARCHAR(255)  ,`label` VARCHAR(255)  ,`count` INT(3)  ,`reference` INT(3)  ,`thresholds` VARCHAR(255)  )");
								stmtCreate_talendMeter_DB.close();
							}
							GlobalResource.resourceMap.put(
									keyCheckTable_talendMeter_DB, true);
							// {{{
						} // end of if
					} // end synchronized
				}

				String insert_talendMeter_DB = "INSERT INTO `"
						+ "jobMeters"
						+ "` (`moment`,`pid`,`father_pid`,`root_pid`,`system_pid`,`project`,`job`,`job_repository_id`,`job_version`,`context`,`origin`,`label`,`count`,`reference`,`thresholds`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

				java.sql.PreparedStatement pstmt_talendMeter_DB = null;
				// [%connection%][psmt][tableName]
				String keyPsmt_talendMeter_DB = conn_talendMeter_DB + "[psmt]"
						+ "[" + "jobMeters" + "]";
				pstmt_talendMeter_DB = SharedDBPreparedStatement
						.getSharedPreparedStatement(conn_talendMeter_DB,
								insert_talendMeter_DB, keyPsmt_talendMeter_DB);

				/**
				 * [talendMeter_DB begin ] stop
				 */

				/**
				 * [talendMeter_METTER begin ] start
				 */

				ok_Hash.put("talendMeter_METTER", false);
				start_Hash
						.put("talendMeter_METTER", System.currentTimeMillis());

				currentVirtualComponent = "talendMeter_METTER";

				currentComponent = "talendMeter_METTER";

				int tos_count_talendMeter_METTER = 0;

				class BytesLimit65535_talendMeter_METTER {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_talendMeter_METTER().limitLog4jByte();

				for (MetterCatcherUtils.MetterCatcherMessage mcm : talendMeter_METTER
						.getMessages()) {
					row_talendMeter_METTER.pid = pid;
					row_talendMeter_METTER.root_pid = rootPid;
					row_talendMeter_METTER.father_pid = fatherPid;
					row_talendMeter_METTER.project = projectName;
					row_talendMeter_METTER.job = jobName;
					row_talendMeter_METTER.context = contextStr;
					row_talendMeter_METTER.origin = (mcm.getOrigin() == null
							|| mcm.getOrigin().length() < 1 ? null : mcm
							.getOrigin());
					row_talendMeter_METTER.moment = mcm.getMoment();
					row_talendMeter_METTER.job_version = mcm.getJobVersion();
					row_talendMeter_METTER.job_repository_id = mcm.getJobId();
					row_talendMeter_METTER.system_pid = mcm.getSystemPid();
					row_talendMeter_METTER.label = mcm.getLabel();
					row_talendMeter_METTER.count = mcm.getCount();
					row_talendMeter_METTER.reference = talendMeter_METTER
							.getConnLinesCount(mcm.getReferense() + "_count");
					row_talendMeter_METTER.thresholds = mcm.getThresholds();

					/**
					 * [talendMeter_METTER begin ] stop
					 */

					/**
					 * [talendMeter_METTER main ] start
					 */

					currentVirtualComponent = "talendMeter_METTER";

					currentComponent = "talendMeter_METTER";

					tos_count_talendMeter_METTER++;

					/**
					 * [talendMeter_METTER main ] stop
					 */

					/**
					 * [talendMeter_METTER process_data_begin ] start
					 */

					currentVirtualComponent = "talendMeter_METTER";

					currentComponent = "talendMeter_METTER";

					/**
					 * [talendMeter_METTER process_data_begin ] stop
					 */

					/**
					 * [talendMeter_DB main ] start
					 */

					currentVirtualComponent = "talendMeter_DB";

					currentComponent = "talendMeter_DB";

					// Main
					// row_talendMeter_METTER

					if (execStat) {
						runStat.updateStatOnConnection("Main" + iterateId, 1, 1);
					}

					row_talendMeter_DB = null;
					whetherReject_talendMeter_DB = false;
					if (row_talendMeter_METTER.moment != null) {
						date_talendMeter_DB = row_talendMeter_METTER.moment
								.getTime();
						if (date_talendMeter_DB < year1_talendMeter_DB
								|| date_talendMeter_DB >= year10000_talendMeter_DB) {
							pstmt_talendMeter_DB.setString(1,
									"0000-00-00 00:00:00");
						} else {
							pstmt_talendMeter_DB
									.setTimestamp(1, new java.sql.Timestamp(
											date_talendMeter_DB));
						}
					} else {
						pstmt_talendMeter_DB.setNull(1, java.sql.Types.DATE);
					}

					if (row_talendMeter_METTER.pid == null) {
						pstmt_talendMeter_DB.setNull(2, java.sql.Types.VARCHAR);
					} else {
						pstmt_talendMeter_DB.setString(2,
								row_talendMeter_METTER.pid);
					}

					if (row_talendMeter_METTER.father_pid == null) {
						pstmt_talendMeter_DB.setNull(3, java.sql.Types.VARCHAR);
					} else {
						pstmt_talendMeter_DB.setString(3,
								row_talendMeter_METTER.father_pid);
					}

					if (row_talendMeter_METTER.root_pid == null) {
						pstmt_talendMeter_DB.setNull(4, java.sql.Types.VARCHAR);
					} else {
						pstmt_talendMeter_DB.setString(4,
								row_talendMeter_METTER.root_pid);
					}

					if (row_talendMeter_METTER.system_pid == null) {
						pstmt_talendMeter_DB.setNull(5, java.sql.Types.INTEGER);
					} else {
						pstmt_talendMeter_DB.setLong(5,
								row_talendMeter_METTER.system_pid);
					}

					if (row_talendMeter_METTER.project == null) {
						pstmt_talendMeter_DB.setNull(6, java.sql.Types.VARCHAR);
					} else {
						pstmt_talendMeter_DB.setString(6,
								row_talendMeter_METTER.project);
					}

					if (row_talendMeter_METTER.job == null) {
						pstmt_talendMeter_DB.setNull(7, java.sql.Types.VARCHAR);
					} else {
						pstmt_talendMeter_DB.setString(7,
								row_talendMeter_METTER.job);
					}

					if (row_talendMeter_METTER.job_repository_id == null) {
						pstmt_talendMeter_DB.setNull(8, java.sql.Types.VARCHAR);
					} else {
						pstmt_talendMeter_DB.setString(8,
								row_talendMeter_METTER.job_repository_id);
					}

					if (row_talendMeter_METTER.job_version == null) {
						pstmt_talendMeter_DB.setNull(9, java.sql.Types.VARCHAR);
					} else {
						pstmt_talendMeter_DB.setString(9,
								row_talendMeter_METTER.job_version);
					}

					if (row_talendMeter_METTER.context == null) {
						pstmt_talendMeter_DB
								.setNull(10, java.sql.Types.VARCHAR);
					} else {
						pstmt_talendMeter_DB.setString(10,
								row_talendMeter_METTER.context);
					}

					if (row_talendMeter_METTER.origin == null) {
						pstmt_talendMeter_DB
								.setNull(11, java.sql.Types.VARCHAR);
					} else {
						pstmt_talendMeter_DB.setString(11,
								row_talendMeter_METTER.origin);
					}

					if (row_talendMeter_METTER.label == null) {
						pstmt_talendMeter_DB
								.setNull(12, java.sql.Types.VARCHAR);
					} else {
						pstmt_talendMeter_DB.setString(12,
								row_talendMeter_METTER.label);
					}

					if (row_talendMeter_METTER.count == null) {
						pstmt_talendMeter_DB
								.setNull(13, java.sql.Types.INTEGER);
					} else {
						pstmt_talendMeter_DB.setInt(13,
								row_talendMeter_METTER.count);
					}

					if (row_talendMeter_METTER.reference == null) {
						pstmt_talendMeter_DB
								.setNull(14, java.sql.Types.INTEGER);
					} else {
						pstmt_talendMeter_DB.setInt(14,
								row_talendMeter_METTER.reference);
					}

					if (row_talendMeter_METTER.thresholds == null) {
						pstmt_talendMeter_DB
								.setNull(15, java.sql.Types.VARCHAR);
					} else {
						pstmt_talendMeter_DB.setString(15,
								row_talendMeter_METTER.thresholds);
					}

					try {
						nb_line_talendMeter_DB++;
						insertedCount_talendMeter_DB = insertedCount_talendMeter_DB
								+ pstmt_talendMeter_DB.executeUpdate();
					} catch (java.lang.Exception e) {
						whetherReject_talendMeter_DB = true;
						System.err.print(e.getMessage());
					}
					if (!whetherReject_talendMeter_DB) {
						row_talendMeter_DB = new row_talendMeter_DBStruct();
						row_talendMeter_DB.moment = row_talendMeter_METTER.moment;
						row_talendMeter_DB.pid = row_talendMeter_METTER.pid;
						row_talendMeter_DB.father_pid = row_talendMeter_METTER.father_pid;
						row_talendMeter_DB.root_pid = row_talendMeter_METTER.root_pid;
						row_talendMeter_DB.system_pid = row_talendMeter_METTER.system_pid;
						row_talendMeter_DB.project = row_talendMeter_METTER.project;
						row_talendMeter_DB.job = row_talendMeter_METTER.job;
						row_talendMeter_DB.job_repository_id = row_talendMeter_METTER.job_repository_id;
						row_talendMeter_DB.job_version = row_talendMeter_METTER.job_version;
						row_talendMeter_DB.context = row_talendMeter_METTER.context;
						row_talendMeter_DB.origin = row_talendMeter_METTER.origin;
						row_talendMeter_DB.label = row_talendMeter_METTER.label;
						row_talendMeter_DB.count = row_talendMeter_METTER.count;
						row_talendMeter_DB.reference = row_talendMeter_METTER.reference;
						row_talendMeter_DB.thresholds = row_talendMeter_METTER.thresholds;
					}

					tos_count_talendMeter_DB++;

					/**
					 * [talendMeter_DB main ] stop
					 */

					/**
					 * [talendMeter_DB process_data_begin ] start
					 */

					currentVirtualComponent = "talendMeter_DB";

					currentComponent = "talendMeter_DB";

					/**
					 * [talendMeter_DB process_data_begin ] stop
					 */
					// Start of branch "row_talendMeter_DB"
					if (row_talendMeter_DB != null) {

						/**
						 * [talendMeter_CONSOLE main ] start
						 */

						currentVirtualComponent = "talendMeter_CONSOLE";

						currentComponent = "talendMeter_CONSOLE";

						// Main
						// row_talendMeter_DB

						if (execStat) {
							runStat.updateStatOnConnection("Main" + iterateId,
									1, 1);
						}

						// /////////////////////

						strBuffer_talendMeter_CONSOLE = new StringBuilder();

						if (row_talendMeter_DB.moment != null) { //

							strBuffer_talendMeter_CONSOLE.append(FormatterUtils
									.format_Date(row_talendMeter_DB.moment,
											"yyyy-MM-dd HH:mm:ss"));

						} //

						strBuffer_talendMeter_CONSOLE.append("|");

						if (row_talendMeter_DB.pid != null) { //

							strBuffer_talendMeter_CONSOLE.append(String
									.valueOf(row_talendMeter_DB.pid));

						} //

						strBuffer_talendMeter_CONSOLE.append("|");

						if (row_talendMeter_DB.father_pid != null) { //

							strBuffer_talendMeter_CONSOLE.append(String
									.valueOf(row_talendMeter_DB.father_pid));

						} //

						strBuffer_talendMeter_CONSOLE.append("|");

						if (row_talendMeter_DB.root_pid != null) { //

							strBuffer_talendMeter_CONSOLE.append(String
									.valueOf(row_talendMeter_DB.root_pid));

						} //

						strBuffer_talendMeter_CONSOLE.append("|");

						if (row_talendMeter_DB.system_pid != null) { //

							strBuffer_talendMeter_CONSOLE.append(String
									.valueOf(row_talendMeter_DB.system_pid));

						} //

						strBuffer_talendMeter_CONSOLE.append("|");

						if (row_talendMeter_DB.project != null) { //

							strBuffer_talendMeter_CONSOLE.append(String
									.valueOf(row_talendMeter_DB.project));

						} //

						strBuffer_talendMeter_CONSOLE.append("|");

						if (row_talendMeter_DB.job != null) { //

							strBuffer_talendMeter_CONSOLE.append(String
									.valueOf(row_talendMeter_DB.job));

						} //

						strBuffer_talendMeter_CONSOLE.append("|");

						if (row_talendMeter_DB.job_repository_id != null) { //

							strBuffer_talendMeter_CONSOLE
									.append(String
											.valueOf(row_talendMeter_DB.job_repository_id));

						} //

						strBuffer_talendMeter_CONSOLE.append("|");

						if (row_talendMeter_DB.job_version != null) { //

							strBuffer_talendMeter_CONSOLE.append(String
									.valueOf(row_talendMeter_DB.job_version));

						} //

						strBuffer_talendMeter_CONSOLE.append("|");

						if (row_talendMeter_DB.context != null) { //

							strBuffer_talendMeter_CONSOLE.append(String
									.valueOf(row_talendMeter_DB.context));

						} //

						strBuffer_talendMeter_CONSOLE.append("|");

						if (row_talendMeter_DB.origin != null) { //

							strBuffer_talendMeter_CONSOLE.append(String
									.valueOf(row_talendMeter_DB.origin));

						} //

						strBuffer_talendMeter_CONSOLE.append("|");

						if (row_talendMeter_DB.label != null) { //

							strBuffer_talendMeter_CONSOLE.append(String
									.valueOf(row_talendMeter_DB.label));

						} //

						strBuffer_talendMeter_CONSOLE.append("|");

						if (row_talendMeter_DB.count != null) { //

							strBuffer_talendMeter_CONSOLE.append(String
									.valueOf(row_talendMeter_DB.count));

						} //

						strBuffer_talendMeter_CONSOLE.append("|");

						if (row_talendMeter_DB.reference != null) { //

							strBuffer_talendMeter_CONSOLE.append(String
									.valueOf(row_talendMeter_DB.reference));

						} //

						strBuffer_talendMeter_CONSOLE.append("|");

						if (row_talendMeter_DB.thresholds != null) { //

							strBuffer_talendMeter_CONSOLE.append(String
									.valueOf(row_talendMeter_DB.thresholds));

						} //

						if (globalMap.get("tLogRow_CONSOLE") != null) {
							consoleOut_talendMeter_CONSOLE = (java.io.PrintStream) globalMap
									.get("tLogRow_CONSOLE");
						} else {
							consoleOut_talendMeter_CONSOLE = new java.io.PrintStream(
									new java.io.BufferedOutputStream(System.out));
							globalMap.put("tLogRow_CONSOLE",
									consoleOut_talendMeter_CONSOLE);
						}
						consoleOut_talendMeter_CONSOLE
								.println(strBuffer_talendMeter_CONSOLE
										.toString());
						consoleOut_talendMeter_CONSOLE.flush();
						nb_line_talendMeter_CONSOLE++;
						// ////

						// ////

						// /////////////////////

						tos_count_talendMeter_CONSOLE++;

						/**
						 * [talendMeter_CONSOLE main ] stop
						 */

						/**
						 * [talendMeter_CONSOLE process_data_begin ] start
						 */

						currentVirtualComponent = "talendMeter_CONSOLE";

						currentComponent = "talendMeter_CONSOLE";

						/**
						 * [talendMeter_CONSOLE process_data_begin ] stop
						 */

						/**
						 * [talendMeter_CONSOLE process_data_end ] start
						 */

						currentVirtualComponent = "talendMeter_CONSOLE";

						currentComponent = "talendMeter_CONSOLE";

						/**
						 * [talendMeter_CONSOLE process_data_end ] stop
						 */

					} // End of branch "row_talendMeter_DB"

					/**
					 * [talendMeter_DB process_data_end ] start
					 */

					currentVirtualComponent = "talendMeter_DB";

					currentComponent = "talendMeter_DB";

					/**
					 * [talendMeter_DB process_data_end ] stop
					 */

					/**
					 * [talendMeter_METTER process_data_end ] start
					 */

					currentVirtualComponent = "talendMeter_METTER";

					currentComponent = "talendMeter_METTER";

					/**
					 * [talendMeter_METTER process_data_end ] stop
					 */

					/**
					 * [talendMeter_METTER end ] start
					 */

					currentVirtualComponent = "talendMeter_METTER";

					currentComponent = "talendMeter_METTER";

				}

				ok_Hash.put("talendMeter_METTER", true);
				end_Hash.put("talendMeter_METTER", System.currentTimeMillis());

				/**
				 * [talendMeter_METTER end ] stop
				 */

				/**
				 * [talendMeter_DB end ] start
				 */

				currentVirtualComponent = "talendMeter_DB";

				currentComponent = "talendMeter_DB";

				if (pstmt_talendMeter_DB != null) {

					SharedDBPreparedStatement
							.releasePreparedStatement(keyPsmt_talendMeter_DB);

				}

				nb_line_deleted_talendMeter_DB = nb_line_deleted_talendMeter_DB
						+ deletedCount_talendMeter_DB;
				nb_line_update_talendMeter_DB = nb_line_update_talendMeter_DB
						+ updatedCount_talendMeter_DB;
				nb_line_inserted_talendMeter_DB = nb_line_inserted_talendMeter_DB
						+ insertedCount_talendMeter_DB;
				nb_line_rejected_talendMeter_DB = nb_line_rejected_talendMeter_DB
						+ rejectedCount_talendMeter_DB;

				globalMap.put("talendMeter_DB_NB_LINE", nb_line_talendMeter_DB);
				globalMap.put("talendMeter_DB_NB_LINE_UPDATED",
						nb_line_update_talendMeter_DB);
				globalMap.put("talendMeter_DB_NB_LINE_INSERTED",
						nb_line_inserted_talendMeter_DB);
				globalMap.put("talendMeter_DB_NB_LINE_DELETED",
						nb_line_deleted_talendMeter_DB);
				globalMap.put("talendMeter_DB_NB_LINE_REJECTED",
						nb_line_rejected_talendMeter_DB);

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null
							|| !((Boolean) resourceMap.get("inIterateVComp"))) {
						runStat.updateStatOnConnection("Main" + iterateId, 2, 0);
					}
				}

				ok_Hash.put("talendMeter_DB", true);
				end_Hash.put("talendMeter_DB", System.currentTimeMillis());

				/**
				 * [talendMeter_DB end ] stop
				 */

				/**
				 * [talendMeter_CONSOLE end ] start
				 */

				currentVirtualComponent = "talendMeter_CONSOLE";

				currentComponent = "talendMeter_CONSOLE";

				// ////
				// ////
				globalMap.put("talendMeter_CONSOLE_NB_LINE",
						nb_line_talendMeter_CONSOLE);

				// /////////////////////

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null
							|| !((Boolean) resourceMap.get("inIterateVComp"))) {
						runStat.updateStatOnConnection("Main" + iterateId, 2, 0);
					}
				}

				ok_Hash.put("talendMeter_CONSOLE", true);
				end_Hash.put("talendMeter_CONSOLE", System.currentTimeMillis());

				/**
				 * [talendMeter_CONSOLE end ] stop
				 */

			}// end the resume

			if (resumeEntryMethodName == null || globalResumeTicket) {
				resumeUtil
						.addLog("CHECKPOINT",
								"CONNECTION:SUBJOB_OK:talendMeter_METTER:sub_ok_talendMeter_connectionStatsLogs_Commit",
								"", Thread.currentThread().getId() + "", "",
								"", "", "", "");
			}

			if (execStat) {
				runStat.updateStatOnConnection(
						"sub_ok_talendMeter_connectionStatsLogs_Commit", 0,
						"ok");
			}

			connectionStatsLogs_CommitProcess(globalMap);

		} catch (java.lang.Exception e) {

			TalendException te = new TalendException(e, currentComponent,
					globalMap);

			te.setVirtualComponentName(currentVirtualComponent);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			try {

				/**
				 * [talendMeter_METTER finally ] start
				 */

				currentVirtualComponent = "talendMeter_METTER";

				currentComponent = "talendMeter_METTER";

				/**
				 * [talendMeter_METTER finally ] stop
				 */

				/**
				 * [talendMeter_DB finally ] start
				 */

				currentVirtualComponent = "talendMeter_DB";

				currentComponent = "talendMeter_DB";

				/**
				 * [talendMeter_DB finally ] stop
				 */

				/**
				 * [talendMeter_CONSOLE finally ] start
				 */

				currentVirtualComponent = "talendMeter_CONSOLE";

				currentComponent = "talendMeter_CONSOLE";

				/**
				 * [talendMeter_CONSOLE finally ] stop
				 */

			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("talendMeter_METTER_SUBPROCESS_STATE", 1);
	}

	public String resuming_logs_dir_path = null;
	public String resuming_checkpoint_path = null;
	public String parent_part_launcher = null;
	private String resumeEntryMethodName = null;
	private boolean globalResumeTicket = false;

	public boolean watch = false;
	// portStats is null, it means don't execute the statistics
	public Integer portStats = null;
	public int portTraces = 4334;
	public String clientHost;
	public String defaultClientHost = "localhost";
	public String contextStr = "PR";
	public boolean isDefaultContext = true;
	public String pid = "0";
	public String rootPid = null;
	public String fatherPid = null;
	public String fatherNode = null;
	public long startTime = 0;
	public boolean isChildJob = false;
	public String log4jLevel = "";

	private boolean execStat = true;

	private ThreadLocal<java.util.Map<String, String>> threadLocal = new ThreadLocal<java.util.Map<String, String>>() {
		protected java.util.Map<String, String> initialValue() {
			java.util.Map<String, String> threadRunResultMap = new java.util.HashMap<String, String>();
			threadRunResultMap.put("errorCode", null);
			threadRunResultMap.put("status", "");
			return threadRunResultMap;
		};
	};

	private PropertiesWithType context_param = new PropertiesWithType();
	public java.util.Map<String, Object> parentContextMap = new java.util.HashMap<String, Object>();

	public String status = "";

	public static void main(String[] args) {
		final PaymentRequestAllJobs PaymentRequestAllJobsClass = new PaymentRequestAllJobs();

		int exitCode = PaymentRequestAllJobsClass.runJobInTOS(args);

		System.exit(exitCode);
	}

	public String[][] runJob(String[] args) {

		int exitCode = runJobInTOS(args);
		String[][] bufferValue = new String[][] { { Integer.toString(exitCode) } };

		return bufferValue;
	}

	public boolean hastBufferOutputComponent() {
		boolean hastBufferOutput = false;

		return hastBufferOutput;
	}

	public int runJobInTOS(String[] args) {
		// reset status
		status = "";

		String lastStr = "";
		for (String arg : args) {
			if (arg.equalsIgnoreCase("--context_param")) {
				lastStr = arg;
			} else if (lastStr.equals("")) {
				evalParam(arg);
			} else {
				evalParam(lastStr + " " + arg);
				lastStr = "";
			}
		}

		if (clientHost == null) {
			clientHost = defaultClientHost;
		}

		if (pid == null || "0".equals(pid)) {
			pid = TalendString.getAsciiRandomString(6);
		}

		if (rootPid == null) {
			rootPid = pid;
		}
		if (fatherPid == null) {
			fatherPid = pid;
		} else {
			isChildJob = true;
		}

		if (portStats != null) {
			// portStats = -1; //for testing
			if (portStats < 0 || portStats > 65535) {
				// issue:10869, the portStats is invalid, so this client socket
				// can't open
				System.err.println("The statistics socket port " + portStats
						+ " is invalid.");
				execStat = false;
			}
		} else {
			execStat = false;
		}

		try {
			// call job/subjob with an existing context, like:
			// --context=production. if without this parameter, there will use
			// the default context instead.
			java.io.InputStream inContext = PaymentRequestAllJobs.class
					.getClassLoader().getResourceAsStream(
							"gifmis/paymentrequestalljobs_0_1/contexts/"
									+ contextStr + ".properties");
			if (inContext == null) {
				inContext = PaymentRequestAllJobs.class
						.getClassLoader()
						.getResourceAsStream(
								"config/contexts/" + contextStr + ".properties");
			}
			if (inContext != null) {
				// defaultProps is in order to keep the original context value
				defaultProps.load(inContext);
				inContext.close();
				context = new ContextProperties(defaultProps);
			} else if (!isDefaultContext) {
				// print info and job continue to run, for case: context_param
				// is not empty.
				System.err.println("Could not find the context " + contextStr);
			}

			if (!context_param.isEmpty()) {
				context.putAll(context_param);
				// set types for params from parentJobs
				for (Object key : context_param.keySet()) {
					String context_key = key.toString();
					String context_type = context_param
							.getContextType(context_key);
					context.setContextType(context_key, context_type);

				}
			}
			context.setContextType("filePath", "id_String");

			context.filePath = (String) context.getProperty("filePath");
			context.setContextType("tableName", "id_String");

			context.tableName = (String) context.getProperty("tableName");
			context.setContextType("Vertica_DWH_AdditionalParams", "id_String");

			context.Vertica_DWH_AdditionalParams = (String) context
					.getProperty("Vertica_DWH_AdditionalParams");
			context.setContextType("Vertica_DWH_Database", "id_String");

			context.Vertica_DWH_Database = (String) context
					.getProperty("Vertica_DWH_Database");
			context.setContextType("Vertica_DWH_Login", "id_String");

			context.Vertica_DWH_Login = (String) context
					.getProperty("Vertica_DWH_Login");
			context.setContextType("Vertica_DWH_Password", "id_Password");

			String pwd_Vertica_DWH_Password_value = context
					.getProperty("Vertica_DWH_Password");
			context.Vertica_DWH_Password = null;
			if (pwd_Vertica_DWH_Password_value != null) {
				if (context_param.containsKey("Vertica_DWH_Password")) {// no
																		// need
																		// to
																		// decrypt
																		// if it
																		// come
																		// from
																		// program
																		// argument
																		// or
																		// parent
																		// job
																		// runtime
					context.Vertica_DWH_Password = pwd_Vertica_DWH_Password_value;
				} else if (!pwd_Vertica_DWH_Password_value.isEmpty()) {
					try {
						context.Vertica_DWH_Password = routines.system.PasswordEncryptUtil
								.decryptPassword(pwd_Vertica_DWH_Password_value);
						context.put("Vertica_DWH_Password",
								context.Vertica_DWH_Password);
					} catch (java.lang.RuntimeException e) {
						// do nothing
					}
				}
			}
			context.setContextType("Vertica_DWH_Port", "id_String");

			context.Vertica_DWH_Port = (String) context
					.getProperty("Vertica_DWH_Port");
			context.setContextType("Vertica_DWH_Schema", "id_String");

			context.Vertica_DWH_Schema = (String) context
					.getProperty("Vertica_DWH_Schema");
			context.setContextType("Vertica_DWH_Server", "id_String");

			context.Vertica_DWH_Server = (String) context
					.getProperty("Vertica_DWH_Server");
			context.setContextType("Vertica_ODS_AdditionalParams", "id_String");

			context.Vertica_ODS_AdditionalParams = (String) context
					.getProperty("Vertica_ODS_AdditionalParams");
			context.setContextType("Vertica_ODS_Database", "id_String");

			context.Vertica_ODS_Database = (String) context
					.getProperty("Vertica_ODS_Database");
			context.setContextType("Vertica_ODS_Login", "id_String");

			context.Vertica_ODS_Login = (String) context
					.getProperty("Vertica_ODS_Login");
			context.setContextType("Vertica_ODS_Password", "id_Password");

			String pwd_Vertica_ODS_Password_value = context
					.getProperty("Vertica_ODS_Password");
			context.Vertica_ODS_Password = null;
			if (pwd_Vertica_ODS_Password_value != null) {
				if (context_param.containsKey("Vertica_ODS_Password")) {// no
																		// need
																		// to
																		// decrypt
																		// if it
																		// come
																		// from
																		// program
																		// argument
																		// or
																		// parent
																		// job
																		// runtime
					context.Vertica_ODS_Password = pwd_Vertica_ODS_Password_value;
				} else if (!pwd_Vertica_ODS_Password_value.isEmpty()) {
					try {
						context.Vertica_ODS_Password = routines.system.PasswordEncryptUtil
								.decryptPassword(pwd_Vertica_ODS_Password_value);
						context.put("Vertica_ODS_Password",
								context.Vertica_ODS_Password);
					} catch (java.lang.RuntimeException e) {
						// do nothing
					}
				}
			}
			context.setContextType("Vertica_ODS_Port", "id_String");

			context.Vertica_ODS_Port = (String) context
					.getProperty("Vertica_ODS_Port");
			context.setContextType("Vertica_ODS_Schema", "id_String");

			context.Vertica_ODS_Schema = (String) context
					.getProperty("Vertica_ODS_Schema");
			context.setContextType("Vertica_ODS_Server", "id_String");

			context.Vertica_ODS_Server = (String) context
					.getProperty("Vertica_ODS_Server");
		} catch (java.io.IOException ie) {
			System.err.println("Could not load context " + contextStr);
			ie.printStackTrace();
		}

		// get context value from parent directly
		if (parentContextMap != null && !parentContextMap.isEmpty()) {
			if (parentContextMap.containsKey("filePath")) {
				context.filePath = (String) parentContextMap.get("filePath");
			}
			if (parentContextMap.containsKey("tableName")) {
				context.tableName = (String) parentContextMap.get("tableName");
			}
			if (parentContextMap.containsKey("Vertica_DWH_AdditionalParams")) {
				context.Vertica_DWH_AdditionalParams = (String) parentContextMap
						.get("Vertica_DWH_AdditionalParams");
			}
			if (parentContextMap.containsKey("Vertica_DWH_Database")) {
				context.Vertica_DWH_Database = (String) parentContextMap
						.get("Vertica_DWH_Database");
			}
			if (parentContextMap.containsKey("Vertica_DWH_Login")) {
				context.Vertica_DWH_Login = (String) parentContextMap
						.get("Vertica_DWH_Login");
			}
			if (parentContextMap.containsKey("Vertica_DWH_Password")) {
				context.Vertica_DWH_Password = (java.lang.String) parentContextMap
						.get("Vertica_DWH_Password");
			}
			if (parentContextMap.containsKey("Vertica_DWH_Port")) {
				context.Vertica_DWH_Port = (String) parentContextMap
						.get("Vertica_DWH_Port");
			}
			if (parentContextMap.containsKey("Vertica_DWH_Schema")) {
				context.Vertica_DWH_Schema = (String) parentContextMap
						.get("Vertica_DWH_Schema");
			}
			if (parentContextMap.containsKey("Vertica_DWH_Server")) {
				context.Vertica_DWH_Server = (String) parentContextMap
						.get("Vertica_DWH_Server");
			}
			if (parentContextMap.containsKey("Vertica_ODS_AdditionalParams")) {
				context.Vertica_ODS_AdditionalParams = (String) parentContextMap
						.get("Vertica_ODS_AdditionalParams");
			}
			if (parentContextMap.containsKey("Vertica_ODS_Database")) {
				context.Vertica_ODS_Database = (String) parentContextMap
						.get("Vertica_ODS_Database");
			}
			if (parentContextMap.containsKey("Vertica_ODS_Login")) {
				context.Vertica_ODS_Login = (String) parentContextMap
						.get("Vertica_ODS_Login");
			}
			if (parentContextMap.containsKey("Vertica_ODS_Password")) {
				context.Vertica_ODS_Password = (java.lang.String) parentContextMap
						.get("Vertica_ODS_Password");
			}
			if (parentContextMap.containsKey("Vertica_ODS_Port")) {
				context.Vertica_ODS_Port = (String) parentContextMap
						.get("Vertica_ODS_Port");
			}
			if (parentContextMap.containsKey("Vertica_ODS_Schema")) {
				context.Vertica_ODS_Schema = (String) parentContextMap
						.get("Vertica_ODS_Schema");
			}
			if (parentContextMap.containsKey("Vertica_ODS_Server")) {
				context.Vertica_ODS_Server = (String) parentContextMap
						.get("Vertica_ODS_Server");
			}
		}

		// Resume: init the resumeUtil
		resumeEntryMethodName = ResumeUtil
				.getResumeEntryMethodName(resuming_checkpoint_path);
		resumeUtil = new ResumeUtil(resuming_logs_dir_path, isChildJob, rootPid);
		resumeUtil.initCommonInfo(pid, rootPid, fatherPid, projectName,
				jobName, contextStr, jobVersion);

		List<String> parametersToEncrypt = new java.util.ArrayList<String>();
		parametersToEncrypt.add("Vertica_DWH_Password");
		parametersToEncrypt.add("Vertica_ODS_Password");
		// Resume: jobStart
		resumeUtil.addLog("JOB_STARTED", "JOB:" + jobName,
				parent_part_launcher, Thread.currentThread().getId() + "", "",
				"", "", "",
				resumeUtil.convertToJsonText(context, parametersToEncrypt));

		if (execStat) {
			try {
				runStat.openSocket(!isChildJob);
				runStat.setAllPID(rootPid, fatherPid, pid, jobName);
				runStat.startThreadStat(clientHost, portStats);
				runStat.updateStatOnJob(RunStat.JOBSTART, fatherNode);
			} catch (java.io.IOException ioException) {
				ioException.printStackTrace();
			}
		}

		java.util.concurrent.ConcurrentHashMap<Object, Object> concurrentHashMap = new java.util.concurrent.ConcurrentHashMap<Object, Object>();
		globalMap.put("concurrentHashMap", concurrentHashMap);

		long startUsedMemory = Runtime.getRuntime().totalMemory()
				- Runtime.getRuntime().freeMemory();
		long endUsedMemory = 0;
		long end = 0;

		startTime = System.currentTimeMillis();
		talendStats_STATS.addMessage("begin");

		this.globalResumeTicket = true;// to run tPreJob

		try {
			errorCode = null;
			preStaLogConProcess(globalMap);
			if (!"failure".equals(status)) {
				status = "end";
			}
		} catch (TalendException e_preStaLogCon) {
			globalMap.put("preStaLogCon_SUBPROCESS_STATE", -1);

			e_preStaLogCon.printStackTrace();

		}
		try {
			errorCode = null;
			tPrejob_1Process(globalMap);
			if (!"failure".equals(status)) {
				status = "end";
			}
		} catch (TalendException e_tPrejob_1) {
			globalMap.put("tPrejob_1_SUBPROCESS_STATE", -1);

			e_tPrejob_1.printStackTrace();

		}

		try {
			talendStats_STATSProcess(globalMap);
		} catch (java.lang.Exception e) {
			e.printStackTrace();
		}

		this.globalResumeTicket = false;// to run others jobs

		this.globalResumeTicket = true;// to run tPostJob

		end = System.currentTimeMillis();

		if (watch) {
			System.out.println((end - startTime) + " milliseconds");
		}

		endUsedMemory = Runtime.getRuntime().totalMemory()
				- Runtime.getRuntime().freeMemory();
		if (false) {
			System.out
					.println((endUsedMemory - startUsedMemory)
							+ " bytes memory increase when running : PaymentRequestAllJobs");
		}
		talendStats_STATS.addMessage(status == "" ? "end" : status,
				(end - startTime));
		try {
			talendStats_STATSProcess(globalMap);
		} catch (java.lang.Exception e) {
			e.printStackTrace();
		}

		if (execStat) {
			runStat.updateStatOnJob(RunStat.JOBEND, fatherNode);
			runStat.stopThreadStat();
		}
		int returnCode = 0;
		if (errorCode == null) {
			returnCode = status != null && status.equals("failure") ? 1 : 0;
		} else {
			returnCode = errorCode.intValue();
		}
		resumeUtil.addLog("JOB_ENDED", "JOB:" + jobName, parent_part_launcher,
				Thread.currentThread().getId() + "", "", "" + returnCode, "",
				"", "");

		return returnCode;

	}

	// only for OSGi env
	public void destroy() {
		closeSqlDbConnections();

	}

	private void closeSqlDbConnections() {
		try {
			Object obj_conn;
			obj_conn = globalMap.remove("conn_connectionStatsLogs");
			if (null != obj_conn) {
				((java.sql.Connection) obj_conn).close();
			}
		} catch (java.lang.Exception e) {
		}
	}

	private java.util.Map<String, Object> getSharedConnections4REST() {
		java.util.Map<String, Object> connections = new java.util.HashMap<String, Object>();
		connections.put("conn_connectionStatsLogs",
				globalMap.get("conn_connectionStatsLogs"));

		return connections;
	}

	private void evalParam(String arg) {
		if (arg.startsWith("--resuming_logs_dir_path")) {
			resuming_logs_dir_path = arg.substring(25);
		} else if (arg.startsWith("--resuming_checkpoint_path")) {
			resuming_checkpoint_path = arg.substring(27);
		} else if (arg.startsWith("--parent_part_launcher")) {
			parent_part_launcher = arg.substring(23);
		} else if (arg.startsWith("--watch")) {
			watch = true;
		} else if (arg.startsWith("--stat_port=")) {
			String portStatsStr = arg.substring(12);
			if (portStatsStr != null && !portStatsStr.equals("null")) {
				portStats = Integer.parseInt(portStatsStr);
			}
		} else if (arg.startsWith("--trace_port=")) {
			portTraces = Integer.parseInt(arg.substring(13));
		} else if (arg.startsWith("--client_host=")) {
			clientHost = arg.substring(14);
		} else if (arg.startsWith("--context=")) {
			contextStr = arg.substring(10);
			isDefaultContext = false;
		} else if (arg.startsWith("--father_pid=")) {
			fatherPid = arg.substring(13);
		} else if (arg.startsWith("--root_pid=")) {
			rootPid = arg.substring(11);
		} else if (arg.startsWith("--father_node=")) {
			fatherNode = arg.substring(14);
		} else if (arg.startsWith("--pid=")) {
			pid = arg.substring(6);
		} else if (arg.startsWith("--context_type")) {
			String keyValue = arg.substring(15);
			int index = -1;
			if (keyValue != null && (index = keyValue.indexOf('=')) > -1) {
				if (fatherPid == null) {
					context_param.setContextType(keyValue.substring(0, index),
							replaceEscapeChars(keyValue.substring(index + 1)));
				} else { // the subjob won't escape the especial chars
					context_param.setContextType(keyValue.substring(0, index),
							keyValue.substring(index + 1));
				}

			}

		} else if (arg.startsWith("--context_param")) {
			String keyValue = arg.substring(16);
			int index = -1;
			if (keyValue != null && (index = keyValue.indexOf('=')) > -1) {
				if (fatherPid == null) {
					context_param.put(keyValue.substring(0, index),
							replaceEscapeChars(keyValue.substring(index + 1)));
				} else { // the subjob won't escape the especial chars
					context_param.put(keyValue.substring(0, index),
							keyValue.substring(index + 1));
				}
			}
		} else if (arg.startsWith("--log4jLevel=")) {
			log4jLevel = arg.substring(13);
		}

	}

	private static final String NULL_VALUE_EXPRESSION_IN_COMMAND_STRING_FOR_CHILD_JOB_ONLY = "<TALEND_NULL>";

	private final String[][] escapeChars = { { "\\\\", "\\" }, { "\\n", "\n" },
			{ "\\'", "\'" }, { "\\r", "\r" }, { "\\f", "\f" }, { "\\b", "\b" },
			{ "\\t", "\t" } };

	private String replaceEscapeChars(String keyValue) {

		if (keyValue == null || ("").equals(keyValue.trim())) {
			return keyValue;
		}

		StringBuilder result = new StringBuilder();
		int currIndex = 0;
		while (currIndex < keyValue.length()) {
			int index = -1;
			// judege if the left string includes escape chars
			for (String[] strArray : escapeChars) {
				index = keyValue.indexOf(strArray[0], currIndex);
				if (index >= 0) {

					result.append(keyValue.substring(currIndex,
							index + strArray[0].length()).replace(strArray[0],
							strArray[1]));
					currIndex = index + strArray[0].length();
					break;
				}
			}
			// if the left string doesn't include escape chars, append the left
			// into the result
			if (index < 0) {
				result.append(keyValue.substring(currIndex));
				currIndex = currIndex + keyValue.length();
			}
		}

		return result.toString();
	}

	public Integer getErrorCode() {
		return errorCode;
	}

	public String getStatus() {
		return status;
	}

	ResumeUtil resumeUtil = null;
}
/************************************************************************************************
 * 262884 characters generated by Talend Data Integration on the February 5,
 * 2019 10:18:06 AM EET
 ************************************************************************************************/
