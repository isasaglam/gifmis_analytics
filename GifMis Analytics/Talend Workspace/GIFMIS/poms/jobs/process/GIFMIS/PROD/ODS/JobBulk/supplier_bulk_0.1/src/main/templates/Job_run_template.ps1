$fileDir = Split-Path -Parent $MyInvocation.MyCommand.Path
cd $fileDir
java '-Xms4096M' '-Xmx4096M' -cp '.;../lib/routines.jar;../lib/dom4j-1.6.1.jar;../lib/log4j-1.2.16.jar;../lib/mysql-connector-java-5.1.30-bin.jar;../lib/ojdbc7.jar;../lib/talend_file_enhanced_20070724.jar;../lib/talendcsv.jar;../lib/vertica-jdbc-9.0.0-0.jar;supplier_bulk_0_1.jar;' gifmis.supplier_bulk_0_1.supplier_bulk  %* 