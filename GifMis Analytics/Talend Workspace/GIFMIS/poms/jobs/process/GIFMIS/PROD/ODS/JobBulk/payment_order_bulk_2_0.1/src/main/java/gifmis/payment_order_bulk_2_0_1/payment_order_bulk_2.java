
package gifmis.payment_order_bulk_2_0_1;

import routines.Numeric;
import routines.DataOperation;
import routines.TalendDataGenerator;
import routines.TalendStringUtil;
import routines.TalendString;
import routines.StringHandling;
import routines.Relational;
import routines.TalendDate;
import routines.Mathematical;
import routines.SQLike;
import routines.system.*;
import routines.system.api.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.math.BigDecimal;
import java.io.ByteArrayOutputStream;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.io.IOException;
import java.util.Comparator;
 




	//the import part of tJava_1
	//import java.util.List;


@SuppressWarnings("unused")

/**
 * Job: payment_order_bulk_2 Purpose: <br>
 * Description:  <br>
 * @author isa.saglam@oredata.com
 * @version 7.0.1.20180411_1414
 * @status 
 */
public class payment_order_bulk_2 implements TalendJob {

protected static void logIgnoredError(String message, Throwable cause) {
       System.err.println(message);
       if (cause != null) {
               cause.printStackTrace();
       }

}


	public final Object obj = new Object();

	// for transmiting parameters purpose
	private Object valueObject = null;

	public Object getValueObject() {
		return this.valueObject;
	}

	public void setValueObject(Object valueObject) {
		this.valueObject = valueObject;
	}
	
	private final static String defaultCharset = java.nio.charset.Charset.defaultCharset().name();

	
	private final static String utf8Charset = "UTF-8";
	//contains type for every context property
	public class PropertiesWithType extends java.util.Properties {
		private static final long serialVersionUID = 1L;
		private java.util.Map<String,String> propertyTypes = new java.util.HashMap<>();
		
		public PropertiesWithType(java.util.Properties properties){
			super(properties);
		}
		public PropertiesWithType(){
			super();
		}
		
		public void setContextType(String key, String type) {
			propertyTypes.put(key,type);
		}
	
		public String getContextType(String key) {
			return propertyTypes.get(key);
		}
	}
	
	// create and load default properties
	private java.util.Properties defaultProps = new java.util.Properties();
	// create application properties with default
	public class ContextProperties extends PropertiesWithType {

		private static final long serialVersionUID = 1L;

		public ContextProperties(java.util.Properties properties){
			super(properties);
		}
		public ContextProperties(){
			super();
		}

		public void synchronizeContext(){
			
			if(filePath != null){
				
					this.setProperty("filePath", filePath.toString());
				
			}
			
			if(tableName != null){
				
					this.setProperty("tableName", tableName.toString());
				
			}
			
			if(Oracle_AdditionalParams != null){
				
					this.setProperty("Oracle_AdditionalParams", Oracle_AdditionalParams.toString());
				
			}
			
			if(Oracle_Login != null){
				
					this.setProperty("Oracle_Login", Oracle_Login.toString());
				
			}
			
			if(Oracle_Password != null){
				
					this.setProperty("Oracle_Password", Oracle_Password.toString());
				
			}
			
			if(Oracle_Port != null){
				
					this.setProperty("Oracle_Port", Oracle_Port.toString());
				
			}
			
			if(Oracle_Schema != null){
				
					this.setProperty("Oracle_Schema", Oracle_Schema.toString());
				
			}
			
			if(Oracle_Server != null){
				
					this.setProperty("Oracle_Server", Oracle_Server.toString());
				
			}
			
			if(Oracle_ServiceName != null){
				
					this.setProperty("Oracle_ServiceName", Oracle_ServiceName.toString());
				
			}
			
			if(Vertica_DWH_AdditionalParams != null){
				
					this.setProperty("Vertica_DWH_AdditionalParams", Vertica_DWH_AdditionalParams.toString());
				
			}
			
			if(Vertica_DWH_Database != null){
				
					this.setProperty("Vertica_DWH_Database", Vertica_DWH_Database.toString());
				
			}
			
			if(Vertica_DWH_Login != null){
				
					this.setProperty("Vertica_DWH_Login", Vertica_DWH_Login.toString());
				
			}
			
			if(Vertica_DWH_Password != null){
				
					this.setProperty("Vertica_DWH_Password", Vertica_DWH_Password.toString());
				
			}
			
			if(Vertica_DWH_Port != null){
				
					this.setProperty("Vertica_DWH_Port", Vertica_DWH_Port.toString());
				
			}
			
			if(Vertica_DWH_Schema != null){
				
					this.setProperty("Vertica_DWH_Schema", Vertica_DWH_Schema.toString());
				
			}
			
			if(Vertica_DWH_Server != null){
				
					this.setProperty("Vertica_DWH_Server", Vertica_DWH_Server.toString());
				
			}
			
			if(Vertica_ODS_AdditionalParams != null){
				
					this.setProperty("Vertica_ODS_AdditionalParams", Vertica_ODS_AdditionalParams.toString());
				
			}
			
			if(Vertica_ODS_Database != null){
				
					this.setProperty("Vertica_ODS_Database", Vertica_ODS_Database.toString());
				
			}
			
			if(Vertica_ODS_Login != null){
				
					this.setProperty("Vertica_ODS_Login", Vertica_ODS_Login.toString());
				
			}
			
			if(Vertica_ODS_Password != null){
				
					this.setProperty("Vertica_ODS_Password", Vertica_ODS_Password.toString());
				
			}
			
			if(Vertica_ODS_Port != null){
				
					this.setProperty("Vertica_ODS_Port", Vertica_ODS_Port.toString());
				
			}
			
			if(Vertica_ODS_Schema != null){
				
					this.setProperty("Vertica_ODS_Schema", Vertica_ODS_Schema.toString());
				
			}
			
			if(Vertica_ODS_Server != null){
				
					this.setProperty("Vertica_ODS_Server", Vertica_ODS_Server.toString());
				
			}
			
		}

public String filePath;
public String getFilePath(){
	return this.filePath;
}
public String tableName;
public String getTableName(){
	return this.tableName;
}
public String Oracle_AdditionalParams;
public String getOracle_AdditionalParams(){
	return this.Oracle_AdditionalParams;
}
public String Oracle_Login;
public String getOracle_Login(){
	return this.Oracle_Login;
}
public java.lang.String Oracle_Password;
public java.lang.String getOracle_Password(){
	return this.Oracle_Password;
}
public String Oracle_Port;
public String getOracle_Port(){
	return this.Oracle_Port;
}
public String Oracle_Schema;
public String getOracle_Schema(){
	return this.Oracle_Schema;
}
public String Oracle_Server;
public String getOracle_Server(){
	return this.Oracle_Server;
}
public String Oracle_ServiceName;
public String getOracle_ServiceName(){
	return this.Oracle_ServiceName;
}
public String Vertica_DWH_AdditionalParams;
public String getVertica_DWH_AdditionalParams(){
	return this.Vertica_DWH_AdditionalParams;
}
public String Vertica_DWH_Database;
public String getVertica_DWH_Database(){
	return this.Vertica_DWH_Database;
}
public String Vertica_DWH_Login;
public String getVertica_DWH_Login(){
	return this.Vertica_DWH_Login;
}
public java.lang.String Vertica_DWH_Password;
public java.lang.String getVertica_DWH_Password(){
	return this.Vertica_DWH_Password;
}
public String Vertica_DWH_Port;
public String getVertica_DWH_Port(){
	return this.Vertica_DWH_Port;
}
public String Vertica_DWH_Schema;
public String getVertica_DWH_Schema(){
	return this.Vertica_DWH_Schema;
}
public String Vertica_DWH_Server;
public String getVertica_DWH_Server(){
	return this.Vertica_DWH_Server;
}
public String Vertica_ODS_AdditionalParams;
public String getVertica_ODS_AdditionalParams(){
	return this.Vertica_ODS_AdditionalParams;
}
public String Vertica_ODS_Database;
public String getVertica_ODS_Database(){
	return this.Vertica_ODS_Database;
}
public String Vertica_ODS_Login;
public String getVertica_ODS_Login(){
	return this.Vertica_ODS_Login;
}
public java.lang.String Vertica_ODS_Password;
public java.lang.String getVertica_ODS_Password(){
	return this.Vertica_ODS_Password;
}
public String Vertica_ODS_Port;
public String getVertica_ODS_Port(){
	return this.Vertica_ODS_Port;
}
public String Vertica_ODS_Schema;
public String getVertica_ODS_Schema(){
	return this.Vertica_ODS_Schema;
}
public String Vertica_ODS_Server;
public String getVertica_ODS_Server(){
	return this.Vertica_ODS_Server;
}
	}
	private ContextProperties context = new ContextProperties();
	public ContextProperties getContext() {
		return this.context;
	}
	private final String jobVersion = "0.1";
	private final String jobName = "payment_order_bulk_2";
	private final String projectName = "GIFMIS";
	public Integer errorCode = null;
	private String currentComponent = "";
	
		private final java.util.Map<String, Object> globalMap = new java.util.HashMap<String, Object>();
        private final static java.util.Map<String, Object> junitGlobalMap = new java.util.HashMap<String, Object>();
	
		private final java.util.Map<String, Long> start_Hash = new java.util.HashMap<String, Long>();
		private final java.util.Map<String, Long> end_Hash = new java.util.HashMap<String, Long>();
		private final java.util.Map<String, Boolean> ok_Hash = new java.util.HashMap<String, Boolean>();
		public  final java.util.List<String[]> globalBuffer = new java.util.ArrayList<String[]>();
	

private RunStat runStat = new RunStat();

	// OSGi DataSource
	private final static String KEY_DB_DATASOURCES = "KEY_DB_DATASOURCES";
	
	private final static String KEY_DB_DATASOURCES_RAW = "KEY_DB_DATASOURCES_RAW";

	public void setDataSources(java.util.Map<String, javax.sql.DataSource> dataSources) {
		java.util.Map<String, routines.system.TalendDataSource> talendDataSources = new java.util.HashMap<String, routines.system.TalendDataSource>();
		for (java.util.Map.Entry<String, javax.sql.DataSource> dataSourceEntry : dataSources.entrySet()) {
			talendDataSources.put(dataSourceEntry.getKey(), new routines.system.TalendDataSource(dataSourceEntry.getValue()));
		}
		globalMap.put(KEY_DB_DATASOURCES, talendDataSources);
		globalMap.put(KEY_DB_DATASOURCES_RAW, new java.util.HashMap<String, javax.sql.DataSource>(dataSources));
	}

	LogCatcherUtils talendLogs_LOGS = new LogCatcherUtils();
	StatCatcherUtils talendStats_STATS = new StatCatcherUtils("_b9WqcPfCEei9mKFTYyYBTw", "0.1");
	MetterCatcherUtils talendMeter_METTER = new MetterCatcherUtils("_b9WqcPfCEei9mKFTYyYBTw", "0.1");

private final java.io.ByteArrayOutputStream baos = new java.io.ByteArrayOutputStream();
private final java.io.PrintStream errorMessagePS = new java.io.PrintStream(new java.io.BufferedOutputStream(baos));

public String getExceptionStackTrace() {
	if ("failure".equals(this.getStatus())) {
		errorMessagePS.flush();
		return baos.toString();
	}
	return null;
}

private Exception exception;

public Exception getException() {
	if ("failure".equals(this.getStatus())) {
		return this.exception;
	}
	return null;
}

private class TalendException extends Exception {

	private static final long serialVersionUID = 1L;

	private java.util.Map<String, Object> globalMap = null;
	private Exception e = null;
	private String currentComponent = null;
	private String virtualComponentName = null;
	
	public void setVirtualComponentName (String virtualComponentName){
		this.virtualComponentName = virtualComponentName;
	}

	private TalendException(Exception e, String errorComponent, final java.util.Map<String, Object> globalMap) {
		this.currentComponent= errorComponent;
		this.globalMap = globalMap;
		this.e = e;
	}

	public Exception getException() {
		return this.e;
	}

	public String getCurrentComponent() {
		return this.currentComponent;
	}

	
    public String getExceptionCauseMessage(Exception e){
        Throwable cause = e;
        String message = null;
        int i = 10;
        while (null != cause && 0 < i--) {
            message = cause.getMessage();
            if (null == message) {
                cause = cause.getCause();
            } else {
                break;          
            }
        }
        if (null == message) {
            message = e.getClass().getName();
        }   
        return message;
    }

	@Override
	public void printStackTrace() {
		if (!(e instanceof TalendException || e instanceof TDieException)) {
			if(virtualComponentName!=null && currentComponent.indexOf(virtualComponentName+"_")==0){
				globalMap.put(virtualComponentName+"_ERROR_MESSAGE",getExceptionCauseMessage(e));
			}
			globalMap.put(currentComponent+"_ERROR_MESSAGE",getExceptionCauseMessage(e));
			System.err.println("Exception in component " + currentComponent + " (" + jobName + ")");
		}
		if (!(e instanceof TDieException)) {
			if(e instanceof TalendException){
				e.printStackTrace();
			} else {
				e.printStackTrace();
				e.printStackTrace(errorMessagePS);
				payment_order_bulk_2.this.exception = e;
			}
		}
		if (!(e instanceof TalendException)) {
		try {
			for (java.lang.reflect.Method m : this.getClass().getEnclosingClass().getMethods()) {
				if (m.getName().compareTo(currentComponent + "_error") == 0) {
					m.invoke(payment_order_bulk_2.this, new Object[] { e , currentComponent, globalMap});
					break;
				}
			}

			if(!(e instanceof TDieException)){
				talendLogs_LOGS.addMessage("Java Exception", currentComponent, 6, e.getClass().getName() + ":" + e.getMessage(), 1);
				talendLogs_LOGSProcess(globalMap);
			}
				} catch (TalendException e) {
					// do nothing
				
		} catch (Exception e) {
			this.e.printStackTrace();
		}
		}
	}
}

			public void preStaLogCon_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					preStaLogCon_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tPrejob_1_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
							talendStats_STATS.addMessage("failure",errorComponent, end_Hash.get(errorComponent)-start_Hash.get(errorComponent));
							talendStats_STATSProcess(globalMap);
							
				status = "failure";
				
					tPrejob_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tJava_1_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
							talendStats_STATS.addMessage("failure",errorComponent, end_Hash.get(errorComponent)-start_Hash.get(errorComponent));
							talendStats_STATSProcess(globalMap);
							
				status = "failure";
				
					tJava_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tDBInput_2_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
							talendStats_STATS.addMessage("failure",errorComponent, end_Hash.get(errorComponent)-start_Hash.get(errorComponent));
							talendStats_STATSProcess(globalMap);
							
				status = "failure";
				
					tDBInput_2_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tConvertType_1_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
							talendStats_STATS.addMessage("failure",errorComponent, end_Hash.get(errorComponent)-start_Hash.get(errorComponent));
							talendStats_STATSProcess(globalMap);
							
				status = "failure";
				
					tDBInput_2_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tSetGlobalVar_1_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
							talendStats_STATS.addMessage("failure",errorComponent, end_Hash.get(errorComponent)-start_Hash.get(errorComponent));
							talendStats_STATSProcess(globalMap);
							
				status = "failure";
				
					tDBInput_2_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tLogRow_1_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
							talendStats_STATS.addMessage("failure",errorComponent, end_Hash.get(errorComponent)-start_Hash.get(errorComponent));
							talendStats_STATSProcess(globalMap);
							
				status = "failure";
				
					tDBInput_2_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tDBInput_3_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
							talendStats_STATS.addMessage("failure",errorComponent, end_Hash.get(errorComponent)-start_Hash.get(errorComponent));
							talendStats_STATSProcess(globalMap);
							
				status = "failure";
				
					tDBInput_3_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tMap_1_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
							talendStats_STATS.addMessage("failure",errorComponent, end_Hash.get(errorComponent)-start_Hash.get(errorComponent));
							talendStats_STATSProcess(globalMap);
							
				status = "failure";
				
					tDBInput_3_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tDBOutputBulk_1_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
							talendStats_STATS.addMessage("failure",errorComponent, end_Hash.get(errorComponent)-start_Hash.get(errorComponent));
							talendStats_STATSProcess(globalMap);
							
				status = "failure";
				
					tDBInput_3_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tFileInputDelimited_1_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
							talendStats_STATS.addMessage("failure",errorComponent, end_Hash.get(errorComponent)-start_Hash.get(errorComponent));
							talendStats_STATSProcess(globalMap);
							
				status = "failure";
				
					tFileInputDelimited_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tMap_2_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tFileInputDelimited_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tDBOutput_1_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tFileInputDelimited_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void connectionStatsLogs_Commit_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					connectionStatsLogs_Commit_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void connectionStatsLogs_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					connectionStatsLogs_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void talendStats_STATS_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
							talendStats_DB_error(exception, errorComponent, globalMap);
						
						}
					
			public void talendStats_DB_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
							talendStats_CONSOLE_error(exception, errorComponent, globalMap);
						
						}
					
			public void talendStats_CONSOLE_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					talendStats_STATS_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void talendLogs_LOGS_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
							talendLogs_DB_error(exception, errorComponent, globalMap);
						
						}
					
			public void talendLogs_DB_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
							talendLogs_CONSOLE_error(exception, errorComponent, globalMap);
						
						}
					
			public void talendLogs_CONSOLE_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					talendLogs_LOGS_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void talendMeter_METTER_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
							talendMeter_DB_error(exception, errorComponent, globalMap);
						
						}
					
			public void talendMeter_DB_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
							talendMeter_CONSOLE_error(exception, errorComponent, globalMap);
						
						}
					
			public void talendMeter_CONSOLE_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					talendMeter_METTER_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void preStaLogCon_onSubJobError(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {

resumeUtil.addLog("SYSTEM_LOG", "NODE:"+ errorComponent, "", Thread.currentThread().getId()+ "", "FATAL", "", exception.getMessage(), ResumeUtil.getExceptionStackTrace(exception),"");

			}
			public void tPrejob_1_onSubJobError(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {

resumeUtil.addLog("SYSTEM_LOG", "NODE:"+ errorComponent, "", Thread.currentThread().getId()+ "", "FATAL", "", exception.getMessage(), ResumeUtil.getExceptionStackTrace(exception),"");

			}
			public void tJava_1_onSubJobError(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {

resumeUtil.addLog("SYSTEM_LOG", "NODE:"+ errorComponent, "", Thread.currentThread().getId()+ "", "FATAL", "", exception.getMessage(), ResumeUtil.getExceptionStackTrace(exception),"");

			}
			public void tDBInput_2_onSubJobError(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {

resumeUtil.addLog("SYSTEM_LOG", "NODE:"+ errorComponent, "", Thread.currentThread().getId()+ "", "FATAL", "", exception.getMessage(), ResumeUtil.getExceptionStackTrace(exception),"");

			}
			public void tDBInput_3_onSubJobError(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {

resumeUtil.addLog("SYSTEM_LOG", "NODE:"+ errorComponent, "", Thread.currentThread().getId()+ "", "FATAL", "", exception.getMessage(), ResumeUtil.getExceptionStackTrace(exception),"");

			}
			public void tFileInputDelimited_1_onSubJobError(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {

resumeUtil.addLog("SYSTEM_LOG", "NODE:"+ errorComponent, "", Thread.currentThread().getId()+ "", "FATAL", "", exception.getMessage(), ResumeUtil.getExceptionStackTrace(exception),"");

			}
			public void connectionStatsLogs_Commit_onSubJobError(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {

resumeUtil.addLog("SYSTEM_LOG", "NODE:"+ errorComponent, "", Thread.currentThread().getId()+ "", "FATAL", "", exception.getMessage(), ResumeUtil.getExceptionStackTrace(exception),"");

			}
			public void connectionStatsLogs_onSubJobError(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {

resumeUtil.addLog("SYSTEM_LOG", "NODE:"+ errorComponent, "", Thread.currentThread().getId()+ "", "FATAL", "", exception.getMessage(), ResumeUtil.getExceptionStackTrace(exception),"");

			}
			public void talendStats_STATS_onSubJobError(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {

resumeUtil.addLog("SYSTEM_LOG", "NODE:"+ errorComponent, "", Thread.currentThread().getId()+ "", "FATAL", "", exception.getMessage(), ResumeUtil.getExceptionStackTrace(exception),"");

			}
			public void talendLogs_LOGS_onSubJobError(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {

resumeUtil.addLog("SYSTEM_LOG", "NODE:"+ errorComponent, "", Thread.currentThread().getId()+ "", "FATAL", "", exception.getMessage(), ResumeUtil.getExceptionStackTrace(exception),"");

			}
			public void talendMeter_METTER_onSubJobError(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {

resumeUtil.addLog("SYSTEM_LOG", "NODE:"+ errorComponent, "", Thread.currentThread().getId()+ "", "FATAL", "", exception.getMessage(), ResumeUtil.getExceptionStackTrace(exception),"");

			}
			





public void preStaLogConProcess(final java.util.Map<String, Object> globalMap) throws TalendException {
	globalMap.put("preStaLogCon_SUBPROCESS_STATE", 0);

 final boolean execStat = this.execStat;
	
		String iterateId = "";
	
	
	String currentComponent = "";
	java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

	try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception().getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { //start the resume
				globalResumeTicket = true;



		


	
	/**
	 * [preStaLogCon begin ] start
	 */

	

	
		
		ok_Hash.put("preStaLogCon", false);
		start_Hash.put("preStaLogCon", System.currentTimeMillis());
		
	
	currentComponent="preStaLogCon";

	
		int tos_count_preStaLogCon = 0;
		
    	class BytesLimit65535_preStaLogCon{
    		public void limitLog4jByte() throws Exception{
    			
    		}
    	}
    	
        new BytesLimit65535_preStaLogCon().limitLog4jByte();

 



/**
 * [preStaLogCon begin ] stop
 */
	
	/**
	 * [preStaLogCon main ] start
	 */

	

	
	
	currentComponent="preStaLogCon";

	

 


	tos_count_preStaLogCon++;

/**
 * [preStaLogCon main ] stop
 */
	
	/**
	 * [preStaLogCon process_data_begin ] start
	 */

	

	
	
	currentComponent="preStaLogCon";

	

 



/**
 * [preStaLogCon process_data_begin ] stop
 */
	
	/**
	 * [preStaLogCon process_data_end ] start
	 */

	

	
	
	currentComponent="preStaLogCon";

	

 



/**
 * [preStaLogCon process_data_end ] stop
 */
	
	/**
	 * [preStaLogCon end ] start
	 */

	

	
	
	currentComponent="preStaLogCon";

	

 

ok_Hash.put("preStaLogCon", true);
end_Hash.put("preStaLogCon", System.currentTimeMillis());

				if(execStat){   
   	 				runStat.updateStatOnConnection("after_preStaLogCon_connectionStatsLogs", 0, "ok");
				}
				connectionStatsLogsProcess(globalMap);



/**
 * [preStaLogCon end ] stop
 */
				}//end the resume

				



	
			}catch(java.lang.Exception e){	
				
				TalendException te = new TalendException(e, currentComponent, globalMap);
				
				throw te;
			}catch(java.lang.Error error){	
				
					runStat.stopThreadStat();
				
				throw error;
			}finally{
				
				try{
					
	
	/**
	 * [preStaLogCon finally ] start
	 */

	

	
	
	currentComponent="preStaLogCon";

	

 



/**
 * [preStaLogCon finally ] stop
 */
				}catch(java.lang.Exception e){	
					//ignore
				}catch(java.lang.Error error){
					//ignore
				}
				resourceMap = null;
			}
		

		globalMap.put("preStaLogCon_SUBPROCESS_STATE", 1);
	}
	

public void tPrejob_1Process(final java.util.Map<String, Object> globalMap) throws TalendException {
	globalMap.put("tPrejob_1_SUBPROCESS_STATE", 0);

 final boolean execStat = this.execStat;
	
		String iterateId = "";
	
	
	String currentComponent = "";
	java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

	try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception().getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { //start the resume
				globalResumeTicket = true;



		


	
	/**
	 * [tPrejob_1 begin ] start
	 */

	

	
		
		ok_Hash.put("tPrejob_1", false);
		start_Hash.put("tPrejob_1", System.currentTimeMillis());
		
				talendStats_STATS.addMessage("begin","tPrejob_1");
				talendStats_STATSProcess(globalMap);
			
	
	currentComponent="tPrejob_1";

	
		int tos_count_tPrejob_1 = 0;
		
    	class BytesLimit65535_tPrejob_1{
    		public void limitLog4jByte() throws Exception{
    			
    		}
    	}
    	
        new BytesLimit65535_tPrejob_1().limitLog4jByte();

 



/**
 * [tPrejob_1 begin ] stop
 */
	
	/**
	 * [tPrejob_1 main ] start
	 */

	

	
	
	currentComponent="tPrejob_1";

	

 


	tos_count_tPrejob_1++;

/**
 * [tPrejob_1 main ] stop
 */
	
	/**
	 * [tPrejob_1 process_data_begin ] start
	 */

	

	
	
	currentComponent="tPrejob_1";

	

 



/**
 * [tPrejob_1 process_data_begin ] stop
 */
	
	/**
	 * [tPrejob_1 process_data_end ] start
	 */

	

	
	
	currentComponent="tPrejob_1";

	

 



/**
 * [tPrejob_1 process_data_end ] stop
 */
	
	/**
	 * [tPrejob_1 end ] start
	 */

	

	
	
	currentComponent="tPrejob_1";

	

 

ok_Hash.put("tPrejob_1", true);
end_Hash.put("tPrejob_1", System.currentTimeMillis());

talendStats_STATS.addMessage("end","tPrejob_1", end_Hash.get("tPrejob_1")-start_Hash.get("tPrejob_1"));
talendStats_STATSProcess(globalMap);
				if(execStat){   
   	 				runStat.updateStatOnConnection("OnComponentOk1", 0, "ok");
				}
				tJava_1Process(globalMap);



/**
 * [tPrejob_1 end ] stop
 */
				}//end the resume

				



	
			}catch(java.lang.Exception e){	
				
				TalendException te = new TalendException(e, currentComponent, globalMap);
				
				throw te;
			}catch(java.lang.Error error){	
				
					runStat.stopThreadStat();
				
				throw error;
			}finally{
				
				try{
					
	
	/**
	 * [tPrejob_1 finally ] start
	 */

	

	
	
	currentComponent="tPrejob_1";

	

 



/**
 * [tPrejob_1 finally ] stop
 */
				}catch(java.lang.Exception e){	
					//ignore
				}catch(java.lang.Error error){
					//ignore
				}
				resourceMap = null;
			}
		

		globalMap.put("tPrejob_1_SUBPROCESS_STATE", 1);
	}
	

public void tJava_1Process(final java.util.Map<String, Object> globalMap) throws TalendException {
	globalMap.put("tJava_1_SUBPROCESS_STATE", 0);

 final boolean execStat = this.execStat;
	
		String iterateId = "";
	
	
	String currentComponent = "";
	java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

	try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception().getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { //start the resume
				globalResumeTicket = true;



		


	
	/**
	 * [tJava_1 begin ] start
	 */

	

	
		
		ok_Hash.put("tJava_1", false);
		start_Hash.put("tJava_1", System.currentTimeMillis());
		
				talendStats_STATS.addMessage("begin","tJava_1");
				talendStats_STATSProcess(globalMap);
			
	
	currentComponent="tJava_1";

	
		int tos_count_tJava_1 = 0;
		
    	class BytesLimit65535_tJava_1{
    		public void limitLog4jByte() throws Exception{
    			
    		}
    	}
    	
        new BytesLimit65535_tJava_1().limitLog4jByte();


String foo = "bar";
String os = System.getProperty("os.name").toLowerCase();
context.tableName="PAYMENT_ORDER";

System.out.println("Operating System Is: "+os);
System.out.println("Table Name Is: "+ context.tableName);

if(os.indexOf("win") >= 0){
context.filePath=System.getProperty("user.home")+"/Logs";
}else if (os.indexOf("mac os x")>=0){ 
context.filePath=System.getProperty("user.home")+"/Documents/Logs";
}else {
context.filePath="/Talend/Jobs/Logs";
}
 



/**
 * [tJava_1 begin ] stop
 */
	
	/**
	 * [tJava_1 main ] start
	 */

	

	
	
	currentComponent="tJava_1";

	

 


	tos_count_tJava_1++;

/**
 * [tJava_1 main ] stop
 */
	
	/**
	 * [tJava_1 process_data_begin ] start
	 */

	

	
	
	currentComponent="tJava_1";

	

 



/**
 * [tJava_1 process_data_begin ] stop
 */
	
	/**
	 * [tJava_1 process_data_end ] start
	 */

	

	
	
	currentComponent="tJava_1";

	

 



/**
 * [tJava_1 process_data_end ] stop
 */
	
	/**
	 * [tJava_1 end ] start
	 */

	

	
	
	currentComponent="tJava_1";

	

 

ok_Hash.put("tJava_1", true);
end_Hash.put("tJava_1", System.currentTimeMillis());

talendStats_STATS.addMessage("end","tJava_1", end_Hash.get("tJava_1")-start_Hash.get("tJava_1"));
talendStats_STATSProcess(globalMap);



/**
 * [tJava_1 end ] stop
 */
				}//end the resume

				
				    			if(resumeEntryMethodName == null || globalResumeTicket){
				    				resumeUtil.addLog("CHECKPOINT", "CONNECTION:SUBJOB_OK:tJava_1:OnSubjobOk", "", Thread.currentThread().getId() + "", "", "", "", "", "");
								}	    				    			
					    	
								if(execStat){    	
									runStat.updateStatOnConnection("OnSubjobOk2", 0, "ok");
								} 
							
							tDBInput_2Process(globalMap); 
						



	
			}catch(java.lang.Exception e){	
				
				TalendException te = new TalendException(e, currentComponent, globalMap);
				
				throw te;
			}catch(java.lang.Error error){	
				
					runStat.stopThreadStat();
				
				throw error;
			}finally{
				
				try{
					
	
	/**
	 * [tJava_1 finally ] start
	 */

	

	
	
	currentComponent="tJava_1";

	

 



/**
 * [tJava_1 finally ] stop
 */
				}catch(java.lang.Exception e){	
					//ignore
				}catch(java.lang.Error error){
					//ignore
				}
				resourceMap = null;
			}
		

		globalMap.put("tJava_1_SUBPROCESS_STATE", 1);
	}
	


public static class row3Struct implements routines.system.IPersistableRow<row3Struct> {
    final static byte[] commonByteArrayLock_GIFMIS_payment_order_bulk_2 = new byte[0];
    static byte[] commonByteArray_GIFMIS_payment_order_bulk_2 = new byte[0];

	
			    public String max_ID;

				public String getMax_ID () {
					return this.max_ID;
				}
				



	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_GIFMIS_payment_order_bulk_2.length) {
				if(length < 1024 && commonByteArray_GIFMIS_payment_order_bulk_2.length == 0) {
   					commonByteArray_GIFMIS_payment_order_bulk_2 = new byte[1024];
				} else {
   					commonByteArray_GIFMIS_payment_order_bulk_2 = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_GIFMIS_payment_order_bulk_2, 0, length);
			strReturn = new String(commonByteArray_GIFMIS_payment_order_bulk_2, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_GIFMIS_payment_order_bulk_2) {

        	try {

        		int length = 0;
		
					this.max_ID = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// String
				
						writeString(this.max_ID,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("max_ID="+max_ID);
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(row3Struct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}

public static class row4Struct implements routines.system.IPersistableRow<row4Struct> {
    final static byte[] commonByteArrayLock_GIFMIS_payment_order_bulk_2 = new byte[0];
    static byte[] commonByteArray_GIFMIS_payment_order_bulk_2 = new byte[0];

	
			    public String max_ID;

				public String getMax_ID () {
					return this.max_ID;
				}
				



	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_GIFMIS_payment_order_bulk_2.length) {
				if(length < 1024 && commonByteArray_GIFMIS_payment_order_bulk_2.length == 0) {
   					commonByteArray_GIFMIS_payment_order_bulk_2 = new byte[1024];
				} else {
   					commonByteArray_GIFMIS_payment_order_bulk_2 = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_GIFMIS_payment_order_bulk_2, 0, length);
			strReturn = new String(commonByteArray_GIFMIS_payment_order_bulk_2, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_GIFMIS_payment_order_bulk_2) {

        	try {

        		int length = 0;
		
					this.max_ID = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// String
				
						writeString(this.max_ID,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("max_ID="+max_ID);
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(row4Struct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}

public static class row2Struct implements routines.system.IPersistableRow<row2Struct> {
    final static byte[] commonByteArrayLock_GIFMIS_payment_order_bulk_2 = new byte[0];
    static byte[] commonByteArray_GIFMIS_payment_order_bulk_2 = new byte[0];

	
			    public Integer max_ID;

				public Integer getMax_ID () {
					return this.max_ID;
				}
				


	private Integer readInteger(ObjectInputStream dis) throws IOException{
		Integer intReturn;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			intReturn = null;
		} else {
	    	intReturn = dis.readInt();
		}
		return intReturn;
	}

	private void writeInteger(Integer intNum, ObjectOutputStream dos) throws IOException{
		if(intNum == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeInt(intNum);
    	}
	}

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_GIFMIS_payment_order_bulk_2) {

        	try {

        		int length = 0;
		
						this.max_ID = readInteger(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// Integer
				
						writeInteger(this.max_ID,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("max_ID="+String.valueOf(max_ID));
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(row2Struct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}
public void tDBInput_2Process(final java.util.Map<String, Object> globalMap) throws TalendException {
	globalMap.put("tDBInput_2_SUBPROCESS_STATE", 0);

 final boolean execStat = this.execStat;
	
		String iterateId = "";
	
	
	String currentComponent = "";
	java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

	try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception().getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { //start the resume
				globalResumeTicket = true;



		row2Struct row2 = new row2Struct();
row4Struct row4 = new row4Struct();
row4Struct row3 = row4;






	
	/**
	 * [tLogRow_1 begin ] start
	 */

	

	
		
		ok_Hash.put("tLogRow_1", false);
		start_Hash.put("tLogRow_1", System.currentTimeMillis());
		
				talendStats_STATS.addMessage("begin","tLogRow_1");
				talendStats_STATSProcess(globalMap);
			
	
	currentComponent="tLogRow_1";

	
			if (execStat) {
				if(resourceMap.get("inIterateVComp") == null){
					
						runStat.updateStatOnConnection("row3" + iterateId, 0, 0);
					
				}
			} 

		
		int tos_count_tLogRow_1 = 0;
		
    	class BytesLimit65535_tLogRow_1{
    		public void limitLog4jByte() throws Exception{
    			
    		}
    	}
    	
        new BytesLimit65535_tLogRow_1().limitLog4jByte();

	///////////////////////
	
         class Util_tLogRow_1 {

        String[] des_top = { ".", ".", "-", "+" };

        String[] des_head = { "|=", "=|", "-", "+" };

        String[] des_bottom = { "'", "'", "-", "+" };

        String name="";

        java.util.List<String[]> list = new java.util.ArrayList<String[]>();

        int[] colLengths = new int[1];

        public void addRow(String[] row) {

            for (int i = 0; i < 1; i++) {
                if (row[i]!=null) {
                  colLengths[i] = Math.max(colLengths[i], row[i].length());
                }
            }
            list.add(row);
        }

        public void setTableName(String name) {

            this.name = name;
        }

            public StringBuilder format() {
            
                StringBuilder sb = new StringBuilder();
  
            
                    sb.append(print(des_top));
    
                    int totals = 0;
                    for (int i = 0; i < colLengths.length; i++) {
                        totals = totals + colLengths[i];
                    }
    
                    // name
                    sb.append("|");
                    int k = 0;
                    for (k = 0; k < (totals + 0 - name.length()) / 2; k++) {
                        sb.append(' ');
                    }
                    sb.append(name);
                    for (int i = 0; i < totals + 0 - name.length() - k; i++) {
                        sb.append(' ');
                    }
                    sb.append("|\n");

                    // head and rows
                    sb.append(print(des_head));
                    for (int i = 0; i < list.size(); i++) {
    
                        String[] row = list.get(i);
    
                        java.util.Formatter formatter = new java.util.Formatter(new StringBuilder());
                        
                        StringBuilder sbformat = new StringBuilder();                                             
        			        sbformat.append("|%1$-");
        			        sbformat.append(colLengths[0]);
        			        sbformat.append("s");
        			                      
                        sbformat.append("|\n");                    
       
                        formatter.format(sbformat.toString(), (Object[])row);	
                                
                        sb.append(formatter.toString());
                        if (i == 0)
                            sb.append(print(des_head)); // print the head
                    }
    
                    // end
                    sb.append(print(des_bottom));
                    return sb;
                }
            

            private StringBuilder print(String[] fillChars) {
                StringBuilder sb = new StringBuilder();
                //first column
                sb.append(fillChars[0]);	                

                  
                    //last column
                    for (int i = 0; i < colLengths[0] - fillChars[0].length() - fillChars[1].length()+2; i++) {
                        sb.append(fillChars[2]);
                    }         
                sb.append(fillChars[1]);
                sb.append("\n");               
                return sb;
            }
            
            public boolean isTableEmpty(){
            	if (list.size() > 1)
            		return false;
            	return true;
            }
        }
        Util_tLogRow_1 util_tLogRow_1 = new Util_tLogRow_1();
        util_tLogRow_1.setTableName("tLogRow_1");
        util_tLogRow_1.addRow(new String[]{"max_ID",});        
 		StringBuilder strBuffer_tLogRow_1 = null;
		int nb_line_tLogRow_1 = 0;
///////////////////////    			



 



/**
 * [tLogRow_1 begin ] stop
 */



	
	/**
	 * [tSetGlobalVar_1 begin ] start
	 */

	

	
		
		ok_Hash.put("tSetGlobalVar_1", false);
		start_Hash.put("tSetGlobalVar_1", System.currentTimeMillis());
		
				talendStats_STATS.addMessage("begin","tSetGlobalVar_1");
				talendStats_STATSProcess(globalMap);
			
	
	currentComponent="tSetGlobalVar_1";

	
			if (execStat) {
				if(resourceMap.get("inIterateVComp") == null){
					
						runStat.updateStatOnConnection("row4" + iterateId, 0, 0);
					
				}
			} 

		
		int tos_count_tSetGlobalVar_1 = 0;
		
    	class BytesLimit65535_tSetGlobalVar_1{
    		public void limitLog4jByte() throws Exception{
    			
    		}
    	}
    	
        new BytesLimit65535_tSetGlobalVar_1().limitLog4jByte();

 



/**
 * [tSetGlobalVar_1 begin ] stop
 */



	
	/**
	 * [tConvertType_1 begin ] start
	 */

	

	
		
		ok_Hash.put("tConvertType_1", false);
		start_Hash.put("tConvertType_1", System.currentTimeMillis());
		
				talendStats_STATS.addMessage("begin","tConvertType_1");
				talendStats_STATSProcess(globalMap);
			
	
	currentComponent="tConvertType_1";

	
			if (execStat) {
				if(resourceMap.get("inIterateVComp") == null){
					
						runStat.updateStatOnConnection("row2" + iterateId, 0, 0);
					
				}
			} 

		
		int tos_count_tConvertType_1 = 0;
		
    	class BytesLimit65535_tConvertType_1{
    		public void limitLog4jByte() throws Exception{
    			
    		}
    	}
    	
        new BytesLimit65535_tConvertType_1().limitLog4jByte();
	int nb_line_tConvertType_1 = 0;  
 



/**
 * [tConvertType_1 begin ] stop
 */



	
	/**
	 * [tDBInput_2 begin ] start
	 */

	

	
		
		ok_Hash.put("tDBInput_2", false);
		start_Hash.put("tDBInput_2", System.currentTimeMillis());
		
				talendStats_STATS.addMessage("begin","tDBInput_2");
				talendStats_STATSProcess(globalMap);
			
	
	currentComponent="tDBInput_2";

	
		int tos_count_tDBInput_2 = 0;
		
    	class BytesLimit65535_tDBInput_2{
    		public void limitLog4jByte() throws Exception{
    			
    		}
    	}
    	
        new BytesLimit65535_tDBInput_2().limitLog4jByte();
	
    
	
		    int nb_line_tDBInput_2 = 0;
		    java.sql.Connection conn_tDBInput_2 = null;
				String driverClass_tDBInput_2 = "com.vertica.jdbc.Driver";
			    java.lang.Class.forName(driverClass_tDBInput_2);
			   	String dbUser_tDBInput_2 = context.Vertica_ODS_Login;
			   	
        		
        		
        		
	final String decryptedPassword_tDBInput_2 = context.Vertica_ODS_Password; 
			   	
		        String dbPwd_tDBInput_2 = decryptedPassword_tDBInput_2;
		        
				
				String url_tDBInput_2 = "jdbc:vertica://" + context.Vertica_ODS_Server + ":" + context.Vertica_ODS_Port + "/" + context.Vertica_ODS_Database + "?" + context.Vertica_ODS_AdditionalParams;
				
				conn_tDBInput_2 = java.sql.DriverManager.getConnection(url_tDBInput_2,dbUser_tDBInput_2,dbPwd_tDBInput_2);
		        
		    
			java.sql.Statement stmt_tDBInput_2 = conn_tDBInput_2.createStatement();

		    String dbquery_tDBInput_2 = "SELECT IFNULL(MAX(ID),-1) as max_ID from ODS."+context.tableName;
			

            	globalMap.put("tDBInput_2_QUERY",dbquery_tDBInput_2);
		    java.sql.ResultSet rs_tDBInput_2 = null;

		    try {
		    	rs_tDBInput_2 = stmt_tDBInput_2.executeQuery(dbquery_tDBInput_2);
		    	java.sql.ResultSetMetaData rsmd_tDBInput_2 = rs_tDBInput_2.getMetaData();
		    	int colQtyInRs_tDBInput_2 = rsmd_tDBInput_2.getColumnCount();

		    String tmpContent_tDBInput_2 = null;
		    
		    
		    while (rs_tDBInput_2.next()) {
		        nb_line_tDBInput_2++;
		        
							if(colQtyInRs_tDBInput_2 < 1) {
								row2.max_ID = null;
							} else {
		                          
            if(rs_tDBInput_2.getObject(1) != null) {
                row2.max_ID = rs_tDBInput_2.getInt(1);
            } else {
                    row2.max_ID = null;
            }
		                    }
					


 



/**
 * [tDBInput_2 begin ] stop
 */
	
	/**
	 * [tDBInput_2 main ] start
	 */

	

	
	
	currentComponent="tDBInput_2";

	

 


	tos_count_tDBInput_2++;

/**
 * [tDBInput_2 main ] stop
 */
	
	/**
	 * [tDBInput_2 process_data_begin ] start
	 */

	

	
	
	currentComponent="tDBInput_2";

	

 



/**
 * [tDBInput_2 process_data_begin ] stop
 */

	
	/**
	 * [tConvertType_1 main ] start
	 */

	

	
	
	currentComponent="tConvertType_1";

	

			//row2
			//row2


			
				if(execStat){
					runStat.updateStatOnConnection("row2"+iterateId,1, 1);
				} 
			

		


  row4 = new row4Struct();
  boolean bHasError_tConvertType_1 = false;             
          try {
              row4.max_ID=TypeConvert.Integer2String(row2.max_ID);            
          } catch(java.lang.Exception e){
            bHasError_tConvertType_1 = true;            
              System.err.println(e.getMessage());          
          }
      if (bHasError_tConvertType_1) {row4 = null;}

  nb_line_tConvertType_1 ++ ;
 


	tos_count_tConvertType_1++;

/**
 * [tConvertType_1 main ] stop
 */
	
	/**
	 * [tConvertType_1 process_data_begin ] start
	 */

	

	
	
	currentComponent="tConvertType_1";

	

 



/**
 * [tConvertType_1 process_data_begin ] stop
 */
// Start of branch "row4"
if(row4 != null) { 



	
	/**
	 * [tSetGlobalVar_1 main ] start
	 */

	

	
	
	currentComponent="tSetGlobalVar_1";

	

			//row4
			//row4


			
				if(execStat){
					runStat.updateStatOnConnection("row4"+iterateId,1, 1);
				} 
			

		

globalMap.put("myKey", row4.max_ID);

 
     row3 = row4;


	tos_count_tSetGlobalVar_1++;

/**
 * [tSetGlobalVar_1 main ] stop
 */
	
	/**
	 * [tSetGlobalVar_1 process_data_begin ] start
	 */

	

	
	
	currentComponent="tSetGlobalVar_1";

	

 



/**
 * [tSetGlobalVar_1 process_data_begin ] stop
 */

	
	/**
	 * [tLogRow_1 main ] start
	 */

	

	
	
	currentComponent="tLogRow_1";

	

			//row3
			//row3


			
				if(execStat){
					runStat.updateStatOnConnection("row3"+iterateId,1, 1);
				} 
			

		
///////////////////////		
						

				
				String[] row_tLogRow_1 = new String[1];
   				
	    		if(row3.max_ID != null) { //              
                 row_tLogRow_1[0]=    						    
				                String.valueOf(row3.max_ID)			
					          ;	
							
	    		} //			
    			 

				util_tLogRow_1.addRow(row_tLogRow_1);	
				nb_line_tLogRow_1++;
//////

//////                    
                    
///////////////////////    			

 


	tos_count_tLogRow_1++;

/**
 * [tLogRow_1 main ] stop
 */
	
	/**
	 * [tLogRow_1 process_data_begin ] start
	 */

	

	
	
	currentComponent="tLogRow_1";

	

 



/**
 * [tLogRow_1 process_data_begin ] stop
 */
	
	/**
	 * [tLogRow_1 process_data_end ] start
	 */

	

	
	
	currentComponent="tLogRow_1";

	

 



/**
 * [tLogRow_1 process_data_end ] stop
 */



	
	/**
	 * [tSetGlobalVar_1 process_data_end ] start
	 */

	

	
	
	currentComponent="tSetGlobalVar_1";

	

 



/**
 * [tSetGlobalVar_1 process_data_end ] stop
 */

} // End of branch "row4"




	
	/**
	 * [tConvertType_1 process_data_end ] start
	 */

	

	
	
	currentComponent="tConvertType_1";

	

 



/**
 * [tConvertType_1 process_data_end ] stop
 */



	
	/**
	 * [tDBInput_2 process_data_end ] start
	 */

	

	
	
	currentComponent="tDBInput_2";

	

 



/**
 * [tDBInput_2 process_data_end ] stop
 */
	
	/**
	 * [tDBInput_2 end ] start
	 */

	

	
	
	currentComponent="tDBInput_2";

	

	}
}finally{
	stmt_tDBInput_2.close();

		if(conn_tDBInput_2 != null && !conn_tDBInput_2.isClosed()) {
			
			conn_tDBInput_2.close();
			
		}
}
globalMap.put("tDBInput_2_NB_LINE",nb_line_tDBInput_2);

 

ok_Hash.put("tDBInput_2", true);
end_Hash.put("tDBInput_2", System.currentTimeMillis());

talendStats_STATS.addMessage("end","tDBInput_2", end_Hash.get("tDBInput_2")-start_Hash.get("tDBInput_2"));
talendStats_STATSProcess(globalMap);



/**
 * [tDBInput_2 end ] stop
 */

	
	/**
	 * [tConvertType_1 end ] start
	 */

	

	
	
	currentComponent="tConvertType_1";

	
      globalMap.put("tConvertType_1_NB_LINE", nb_line_tConvertType_1);
			if(execStat){
				if(resourceMap.get("inIterateVComp") == null || !((Boolean)resourceMap.get("inIterateVComp"))){
			 		runStat.updateStatOnConnection("row2"+iterateId,2, 0); 
			 	}
			}
		
 

ok_Hash.put("tConvertType_1", true);
end_Hash.put("tConvertType_1", System.currentTimeMillis());

talendStats_STATS.addMessage("end","tConvertType_1", end_Hash.get("tConvertType_1")-start_Hash.get("tConvertType_1"));
talendStats_STATSProcess(globalMap);



/**
 * [tConvertType_1 end ] stop
 */

	
	/**
	 * [tSetGlobalVar_1 end ] start
	 */

	

	
	
	currentComponent="tSetGlobalVar_1";

	

			if(execStat){
				if(resourceMap.get("inIterateVComp") == null || !((Boolean)resourceMap.get("inIterateVComp"))){
			 		runStat.updateStatOnConnection("row4"+iterateId,2, 0); 
			 	}
			}
		
 

ok_Hash.put("tSetGlobalVar_1", true);
end_Hash.put("tSetGlobalVar_1", System.currentTimeMillis());

talendStats_STATS.addMessage("end","tSetGlobalVar_1", end_Hash.get("tSetGlobalVar_1")-start_Hash.get("tSetGlobalVar_1"));
talendStats_STATSProcess(globalMap);



/**
 * [tSetGlobalVar_1 end ] stop
 */

	
	/**
	 * [tLogRow_1 end ] start
	 */

	

	
	
	currentComponent="tLogRow_1";

	


//////

                    
                    java.io.PrintStream consoleOut_tLogRow_1 = null;
                    if (globalMap.get("tLogRow_CONSOLE")!=null)
                    {
                    	consoleOut_tLogRow_1 = (java.io.PrintStream) globalMap.get("tLogRow_CONSOLE");
                    }
                    else
                    {
                    	consoleOut_tLogRow_1 = new java.io.PrintStream(new java.io.BufferedOutputStream(System.out));
                    	globalMap.put("tLogRow_CONSOLE",consoleOut_tLogRow_1);
                    }
                    
                    consoleOut_tLogRow_1.println(util_tLogRow_1.format().toString());
                    consoleOut_tLogRow_1.flush();
//////
globalMap.put("tLogRow_1_NB_LINE",nb_line_tLogRow_1);

///////////////////////    			

			if(execStat){
				if(resourceMap.get("inIterateVComp") == null || !((Boolean)resourceMap.get("inIterateVComp"))){
			 		runStat.updateStatOnConnection("row3"+iterateId,2, 0); 
			 	}
			}
		
 

ok_Hash.put("tLogRow_1", true);
end_Hash.put("tLogRow_1", System.currentTimeMillis());

talendStats_STATS.addMessage("end","tLogRow_1", end_Hash.get("tLogRow_1")-start_Hash.get("tLogRow_1"));
talendStats_STATSProcess(globalMap);



/**
 * [tLogRow_1 end ] stop
 */









				}//end the resume

				
				    			if(resumeEntryMethodName == null || globalResumeTicket){
				    				resumeUtil.addLog("CHECKPOINT", "CONNECTION:SUBJOB_OK:tDBInput_2:OnSubjobOk", "", Thread.currentThread().getId() + "", "", "", "", "", "");
								}	    				    			
					    	
								if(execStat){    	
									runStat.updateStatOnConnection("OnSubjobOk1", 0, "ok");
								} 
							
							tDBInput_3Process(globalMap); 
						



	
			}catch(java.lang.Exception e){	
				
				TalendException te = new TalendException(e, currentComponent, globalMap);
				
				throw te;
			}catch(java.lang.Error error){	
				
					runStat.stopThreadStat();
				
				throw error;
			}finally{
				
				try{
					
	
	/**
	 * [tDBInput_2 finally ] start
	 */

	

	
	
	currentComponent="tDBInput_2";

	

 



/**
 * [tDBInput_2 finally ] stop
 */

	
	/**
	 * [tConvertType_1 finally ] start
	 */

	

	
	
	currentComponent="tConvertType_1";

	

 



/**
 * [tConvertType_1 finally ] stop
 */

	
	/**
	 * [tSetGlobalVar_1 finally ] start
	 */

	

	
	
	currentComponent="tSetGlobalVar_1";

	

 



/**
 * [tSetGlobalVar_1 finally ] stop
 */

	
	/**
	 * [tLogRow_1 finally ] start
	 */

	

	
	
	currentComponent="tLogRow_1";

	

 



/**
 * [tLogRow_1 finally ] stop
 */









				}catch(java.lang.Exception e){	
					//ignore
				}catch(java.lang.Error error){
					//ignore
				}
				resourceMap = null;
			}
		

		globalMap.put("tDBInput_2_SUBPROCESS_STATE", 1);
	}
	


public static class outputTableStruct implements routines.system.IPersistableRow<outputTableStruct> {
    final static byte[] commonByteArrayLock_GIFMIS_payment_order_bulk_2 = new byte[0];
    static byte[] commonByteArray_GIFMIS_payment_order_bulk_2 = new byte[0];
	protected static final int DEFAULT_HASHCODE = 1;
    protected static final int PRIME = 31;
    protected int hashCode = DEFAULT_HASHCODE;
    public boolean hashCodeDirty = true;

    public String loopKey;



	
			    public long ID;

				public long getID () {
					return this.ID;
				}
				
			    public long REF_PAYMENT_BATCH;

				public long getREF_PAYMENT_BATCH () {
					return this.REF_PAYMENT_BATCH;
				}
				
			    public long LINE_NO;

				public long getLINE_NO () {
					return this.LINE_NO;
				}
				
			    public long REF_ORGANIZATION;

				public long getREF_ORGANIZATION () {
					return this.REF_ORGANIZATION;
				}
				
			    public String BENEFICIARY_TYPE;

				public String getBENEFICIARY_TYPE () {
					return this.BENEFICIARY_TYPE;
				}
				
			    public String BENEFICIARY_TIN;

				public String getBENEFICIARY_TIN () {
					return this.BENEFICIARY_TIN;
				}
				
			    public String BENEFICIARY_NAME;

				public String getBENEFICIARY_NAME () {
					return this.BENEFICIARY_NAME;
				}
				
			    public String BENEFICIARY_BANK_CODE;

				public String getBENEFICIARY_BANK_CODE () {
					return this.BENEFICIARY_BANK_CODE;
				}
				
			    public String BENEFICIARY_BANK_ACCOUNT;

				public String getBENEFICIARY_BANK_ACCOUNT () {
					return this.BENEFICIARY_BANK_ACCOUNT;
				}
				
			    public String BENEFICIARY_EMAIL;

				public String getBENEFICIARY_EMAIL () {
					return this.BENEFICIARY_EMAIL;
				}
				
			    public String BENEFICIARY_PHONE;

				public String getBENEFICIARY_PHONE () {
					return this.BENEFICIARY_PHONE;
				}
				
			    public String BENEFICIARY_ADDRESS;

				public String getBENEFICIARY_ADDRESS () {
					return this.BENEFICIARY_ADDRESS;
				}
				
			    public long REF_CURRENCY;

				public long getREF_CURRENCY () {
					return this.REF_CURRENCY;
				}
				
			    public BigDecimal CURRENCY_RATE;

				public BigDecimal getCURRENCY_RATE () {
					return this.CURRENCY_RATE;
				}
				
			    public String PAYMENT_REFERENCE_NO;

				public String getPAYMENT_REFERENCE_NO () {
					return this.PAYMENT_REFERENCE_NO;
				}
				
			    public BigDecimal PAYMENT_AMOUNT;

				public BigDecimal getPAYMENT_AMOUNT () {
					return this.PAYMENT_AMOUNT;
				}
				
			    public BigDecimal PAYMENT_AMOUNT_BASE;

				public BigDecimal getPAYMENT_AMOUNT_BASE () {
					return this.PAYMENT_AMOUNT_BASE;
				}
				
			    public String DESCRIPTION;

				public String getDESCRIPTION () {
					return this.DESCRIPTION;
				}
				
			    public String SUPPLIER_INVOICE_NO;

				public String getSUPPLIER_INVOICE_NO () {
					return this.SUPPLIER_INVOICE_NO;
				}
				
			    public java.util.Date SUPPLIER_INVOICE_DATE;

				public java.util.Date getSUPPLIER_INVOICE_DATE () {
					return this.SUPPLIER_INVOICE_DATE;
				}
				
			    public java.util.Date SUPPLIER_INVOICE_DUEDATE;

				public java.util.Date getSUPPLIER_INVOICE_DUEDATE () {
					return this.SUPPLIER_INVOICE_DUEDATE;
				}
				
			    public Long REF_SUPPLIER;

				public Long getREF_SUPPLIER () {
					return this.REF_SUPPLIER;
				}
				
			    public Long REF_EMPLOYEE;

				public Long getREF_EMPLOYEE () {
					return this.REF_EMPLOYEE;
				}
				
			    public Long REF_CUSTOMER;

				public Long getREF_CUSTOMER () {
					return this.REF_CUSTOMER;
				}
				
			    public String PAYMENT_STATUS;

				public String getPAYMENT_STATUS () {
					return this.PAYMENT_STATUS;
				}
				
			    public Long REF_ORG_DEPARTMENT;

				public Long getREF_ORG_DEPARTMENT () {
					return this.REF_ORG_DEPARTMENT;
				}
				
			    public long REF_CLIENT;

				public long getREF_CLIENT () {
					return this.REF_CLIENT;
				}
				
			    public long VERSION;

				public long getVERSION () {
					return this.VERSION;
				}
				
			    public long CREATED_BY;

				public long getCREATED_BY () {
					return this.CREATED_BY;
				}
				
			    public long UPDATED_BY;

				public long getUPDATED_BY () {
					return this.UPDATED_BY;
				}
				
			    public java.util.Date CREATED_DATE;

				public java.util.Date getCREATED_DATE () {
					return this.CREATED_DATE;
				}
				
			    public java.util.Date UPDATED_DATE;

				public java.util.Date getUPDATED_DATE () {
					return this.UPDATED_DATE;
				}
				
			    public String BENEFICIARY_BANK_NAME;

				public String getBENEFICIARY_BANK_NAME () {
					return this.BENEFICIARY_BANK_NAME;
				}
				
			    public String BENEFICIARY_BANK_ACCOUNT_DESC;

				public String getBENEFICIARY_BANK_ACCOUNT_DESC () {
					return this.BENEFICIARY_BANK_ACCOUNT_DESC;
				}
				
			    public Long REF_WARRANT_LINE;

				public Long getREF_WARRANT_LINE () {
					return this.REF_WARRANT_LINE;
				}
				
			    public String IPPIS_EMPLOYEE_ID;

				public String getIPPIS_EMPLOYEE_ID () {
					return this.IPPIS_EMPLOYEE_ID;
				}
				
			    public String IPPIS_BENEFICIARY_CODE;

				public String getIPPIS_BENEFICIARY_CODE () {
					return this.IPPIS_BENEFICIARY_CODE;
				}
				
			    public String IPPIS_PAYMENT_TYPE_CODE;

				public String getIPPIS_PAYMENT_TYPE_CODE () {
					return this.IPPIS_PAYMENT_TYPE_CODE;
				}
				
			    public String LEDGER_STATUS;

				public String getLEDGER_STATUS () {
					return this.LEDGER_STATUS;
				}
				
			    public long IN_SETTLEMENT;

				public long getIN_SETTLEMENT () {
					return this.IN_SETTLEMENT;
				}
				
			    public Long REF_AUTHORIZING_ORG;

				public Long getREF_AUTHORIZING_ORG () {
					return this.REF_AUTHORIZING_ORG;
				}
				
			    public Long REF_FUND_MDA;

				public Long getREF_FUND_MDA () {
					return this.REF_FUND_MDA;
				}
				
			    public long PAYMENT_BATCH_NUMBER;

				public long getPAYMENT_BATCH_NUMBER () {
					return this.PAYMENT_BATCH_NUMBER;
				}
				
			    public Long REF_ORIGINAL_PAYMENT_ORDER;

				public Long getREF_ORIGINAL_PAYMENT_ORDER () {
					return this.REF_ORIGINAL_PAYMENT_ORDER;
				}
				
			    public Long REF_CREDITED_PAYMENT_ORDER;

				public Long getREF_CREDITED_PAYMENT_ORDER () {
					return this.REF_CREDITED_PAYMENT_ORDER;
				}
				
			    public String RESENDING_STATUS;

				public String getRESENDING_STATUS () {
					return this.RESENDING_STATUS;
				}
				
			    public Long IS_REFUND;

				public Long getIS_REFUND () {
					return this.IS_REFUND;
				}
				
			    public String REFUND_REASON;

				public String getREFUND_REASON () {
					return this.REFUND_REASON;
				}
				
			    public String REFUND_ORIGINATING_DOCUMENT;

				public String getREFUND_ORIGINATING_DOCUMENT () {
					return this.REFUND_ORIGINATING_DOCUMENT;
				}
				
			    public java.util.Date etl_date;

				public java.util.Date getEtl_date () {
					return this.etl_date;
				}
				


	@Override
	public int hashCode() {
		if (this.hashCodeDirty) {
			final int prime = PRIME;
			int result = DEFAULT_HASHCODE;
	
							result = prime * result + (int) this.ID;
						
    		this.hashCode = result;
    		this.hashCodeDirty = false;
		}
		return this.hashCode;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		final outputTableStruct other = (outputTableStruct) obj;
		
						if (this.ID != other.ID)
							return false;
					

		return true;
    }

	public void copyDataTo(outputTableStruct other) {

		other.ID = this.ID;
	            other.REF_PAYMENT_BATCH = this.REF_PAYMENT_BATCH;
	            other.LINE_NO = this.LINE_NO;
	            other.REF_ORGANIZATION = this.REF_ORGANIZATION;
	            other.BENEFICIARY_TYPE = this.BENEFICIARY_TYPE;
	            other.BENEFICIARY_TIN = this.BENEFICIARY_TIN;
	            other.BENEFICIARY_NAME = this.BENEFICIARY_NAME;
	            other.BENEFICIARY_BANK_CODE = this.BENEFICIARY_BANK_CODE;
	            other.BENEFICIARY_BANK_ACCOUNT = this.BENEFICIARY_BANK_ACCOUNT;
	            other.BENEFICIARY_EMAIL = this.BENEFICIARY_EMAIL;
	            other.BENEFICIARY_PHONE = this.BENEFICIARY_PHONE;
	            other.BENEFICIARY_ADDRESS = this.BENEFICIARY_ADDRESS;
	            other.REF_CURRENCY = this.REF_CURRENCY;
	            other.CURRENCY_RATE = this.CURRENCY_RATE;
	            other.PAYMENT_REFERENCE_NO = this.PAYMENT_REFERENCE_NO;
	            other.PAYMENT_AMOUNT = this.PAYMENT_AMOUNT;
	            other.PAYMENT_AMOUNT_BASE = this.PAYMENT_AMOUNT_BASE;
	            other.DESCRIPTION = this.DESCRIPTION;
	            other.SUPPLIER_INVOICE_NO = this.SUPPLIER_INVOICE_NO;
	            other.SUPPLIER_INVOICE_DATE = this.SUPPLIER_INVOICE_DATE;
	            other.SUPPLIER_INVOICE_DUEDATE = this.SUPPLIER_INVOICE_DUEDATE;
	            other.REF_SUPPLIER = this.REF_SUPPLIER;
	            other.REF_EMPLOYEE = this.REF_EMPLOYEE;
	            other.REF_CUSTOMER = this.REF_CUSTOMER;
	            other.PAYMENT_STATUS = this.PAYMENT_STATUS;
	            other.REF_ORG_DEPARTMENT = this.REF_ORG_DEPARTMENT;
	            other.REF_CLIENT = this.REF_CLIENT;
	            other.VERSION = this.VERSION;
	            other.CREATED_BY = this.CREATED_BY;
	            other.UPDATED_BY = this.UPDATED_BY;
	            other.CREATED_DATE = this.CREATED_DATE;
	            other.UPDATED_DATE = this.UPDATED_DATE;
	            other.BENEFICIARY_BANK_NAME = this.BENEFICIARY_BANK_NAME;
	            other.BENEFICIARY_BANK_ACCOUNT_DESC = this.BENEFICIARY_BANK_ACCOUNT_DESC;
	            other.REF_WARRANT_LINE = this.REF_WARRANT_LINE;
	            other.IPPIS_EMPLOYEE_ID = this.IPPIS_EMPLOYEE_ID;
	            other.IPPIS_BENEFICIARY_CODE = this.IPPIS_BENEFICIARY_CODE;
	            other.IPPIS_PAYMENT_TYPE_CODE = this.IPPIS_PAYMENT_TYPE_CODE;
	            other.LEDGER_STATUS = this.LEDGER_STATUS;
	            other.IN_SETTLEMENT = this.IN_SETTLEMENT;
	            other.REF_AUTHORIZING_ORG = this.REF_AUTHORIZING_ORG;
	            other.REF_FUND_MDA = this.REF_FUND_MDA;
	            other.PAYMENT_BATCH_NUMBER = this.PAYMENT_BATCH_NUMBER;
	            other.REF_ORIGINAL_PAYMENT_ORDER = this.REF_ORIGINAL_PAYMENT_ORDER;
	            other.REF_CREDITED_PAYMENT_ORDER = this.REF_CREDITED_PAYMENT_ORDER;
	            other.RESENDING_STATUS = this.RESENDING_STATUS;
	            other.IS_REFUND = this.IS_REFUND;
	            other.REFUND_REASON = this.REFUND_REASON;
	            other.REFUND_ORIGINATING_DOCUMENT = this.REFUND_ORIGINATING_DOCUMENT;
	            other.etl_date = this.etl_date;
	            
	}

	public void copyKeysDataTo(outputTableStruct other) {

		other.ID = this.ID;
	            	
	}




	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_GIFMIS_payment_order_bulk_2.length) {
				if(length < 1024 && commonByteArray_GIFMIS_payment_order_bulk_2.length == 0) {
   					commonByteArray_GIFMIS_payment_order_bulk_2 = new byte[1024];
				} else {
   					commonByteArray_GIFMIS_payment_order_bulk_2 = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_GIFMIS_payment_order_bulk_2, 0, length);
			strReturn = new String(commonByteArray_GIFMIS_payment_order_bulk_2, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }

	private java.util.Date readDate(ObjectInputStream dis) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(dis.readLong());
		}
		return dateReturn;
	}

    private void writeDate(java.util.Date date1, ObjectOutputStream dos) throws IOException{
		if(date1 == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeLong(date1.getTime());
    	}
    }

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_GIFMIS_payment_order_bulk_2) {

        	try {

        		int length = 0;
		
			        this.ID = dis.readLong();
					
			        this.REF_PAYMENT_BATCH = dis.readLong();
					
			        this.LINE_NO = dis.readLong();
					
			        this.REF_ORGANIZATION = dis.readLong();
					
					this.BENEFICIARY_TYPE = readString(dis);
					
					this.BENEFICIARY_TIN = readString(dis);
					
					this.BENEFICIARY_NAME = readString(dis);
					
					this.BENEFICIARY_BANK_CODE = readString(dis);
					
					this.BENEFICIARY_BANK_ACCOUNT = readString(dis);
					
					this.BENEFICIARY_EMAIL = readString(dis);
					
					this.BENEFICIARY_PHONE = readString(dis);
					
					this.BENEFICIARY_ADDRESS = readString(dis);
					
			        this.REF_CURRENCY = dis.readLong();
					
						this.CURRENCY_RATE = (BigDecimal) dis.readObject();
					
					this.PAYMENT_REFERENCE_NO = readString(dis);
					
						this.PAYMENT_AMOUNT = (BigDecimal) dis.readObject();
					
						this.PAYMENT_AMOUNT_BASE = (BigDecimal) dis.readObject();
					
					this.DESCRIPTION = readString(dis);
					
					this.SUPPLIER_INVOICE_NO = readString(dis);
					
					this.SUPPLIER_INVOICE_DATE = readDate(dis);
					
					this.SUPPLIER_INVOICE_DUEDATE = readDate(dis);
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.REF_SUPPLIER = null;
           				} else {
           			    	this.REF_SUPPLIER = dis.readLong();
           				}
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.REF_EMPLOYEE = null;
           				} else {
           			    	this.REF_EMPLOYEE = dis.readLong();
           				}
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.REF_CUSTOMER = null;
           				} else {
           			    	this.REF_CUSTOMER = dis.readLong();
           				}
					
					this.PAYMENT_STATUS = readString(dis);
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.REF_ORG_DEPARTMENT = null;
           				} else {
           			    	this.REF_ORG_DEPARTMENT = dis.readLong();
           				}
					
			        this.REF_CLIENT = dis.readLong();
					
			        this.VERSION = dis.readLong();
					
			        this.CREATED_BY = dis.readLong();
					
			        this.UPDATED_BY = dis.readLong();
					
					this.CREATED_DATE = readDate(dis);
					
					this.UPDATED_DATE = readDate(dis);
					
					this.BENEFICIARY_BANK_NAME = readString(dis);
					
					this.BENEFICIARY_BANK_ACCOUNT_DESC = readString(dis);
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.REF_WARRANT_LINE = null;
           				} else {
           			    	this.REF_WARRANT_LINE = dis.readLong();
           				}
					
					this.IPPIS_EMPLOYEE_ID = readString(dis);
					
					this.IPPIS_BENEFICIARY_CODE = readString(dis);
					
					this.IPPIS_PAYMENT_TYPE_CODE = readString(dis);
					
					this.LEDGER_STATUS = readString(dis);
					
			        this.IN_SETTLEMENT = dis.readLong();
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.REF_AUTHORIZING_ORG = null;
           				} else {
           			    	this.REF_AUTHORIZING_ORG = dis.readLong();
           				}
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.REF_FUND_MDA = null;
           				} else {
           			    	this.REF_FUND_MDA = dis.readLong();
           				}
					
			        this.PAYMENT_BATCH_NUMBER = dis.readLong();
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.REF_ORIGINAL_PAYMENT_ORDER = null;
           				} else {
           			    	this.REF_ORIGINAL_PAYMENT_ORDER = dis.readLong();
           				}
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.REF_CREDITED_PAYMENT_ORDER = null;
           				} else {
           			    	this.REF_CREDITED_PAYMENT_ORDER = dis.readLong();
           				}
					
					this.RESENDING_STATUS = readString(dis);
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.IS_REFUND = null;
           				} else {
           			    	this.IS_REFUND = dis.readLong();
           				}
					
					this.REFUND_REASON = readString(dis);
					
					this.REFUND_ORIGINATING_DOCUMENT = readString(dis);
					
					this.etl_date = readDate(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		
			} catch(ClassNotFoundException eCNFE) {
				 throw new RuntimeException(eCNFE);
		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// long
				
		            	dos.writeLong(this.ID);
					
					// long
				
		            	dos.writeLong(this.REF_PAYMENT_BATCH);
					
					// long
				
		            	dos.writeLong(this.LINE_NO);
					
					// long
				
		            	dos.writeLong(this.REF_ORGANIZATION);
					
					// String
				
						writeString(this.BENEFICIARY_TYPE,dos);
					
					// String
				
						writeString(this.BENEFICIARY_TIN,dos);
					
					// String
				
						writeString(this.BENEFICIARY_NAME,dos);
					
					// String
				
						writeString(this.BENEFICIARY_BANK_CODE,dos);
					
					// String
				
						writeString(this.BENEFICIARY_BANK_ACCOUNT,dos);
					
					// String
				
						writeString(this.BENEFICIARY_EMAIL,dos);
					
					// String
				
						writeString(this.BENEFICIARY_PHONE,dos);
					
					// String
				
						writeString(this.BENEFICIARY_ADDRESS,dos);
					
					// long
				
		            	dos.writeLong(this.REF_CURRENCY);
					
					// BigDecimal
				
       			    	dos.writeObject(this.CURRENCY_RATE);
					
					// String
				
						writeString(this.PAYMENT_REFERENCE_NO,dos);
					
					// BigDecimal
				
       			    	dos.writeObject(this.PAYMENT_AMOUNT);
					
					// BigDecimal
				
       			    	dos.writeObject(this.PAYMENT_AMOUNT_BASE);
					
					// String
				
						writeString(this.DESCRIPTION,dos);
					
					// String
				
						writeString(this.SUPPLIER_INVOICE_NO,dos);
					
					// java.util.Date
				
						writeDate(this.SUPPLIER_INVOICE_DATE,dos);
					
					// java.util.Date
				
						writeDate(this.SUPPLIER_INVOICE_DUEDATE,dos);
					
					// Long
				
						if(this.REF_SUPPLIER == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeLong(this.REF_SUPPLIER);
		            	}
					
					// Long
				
						if(this.REF_EMPLOYEE == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeLong(this.REF_EMPLOYEE);
		            	}
					
					// Long
				
						if(this.REF_CUSTOMER == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeLong(this.REF_CUSTOMER);
		            	}
					
					// String
				
						writeString(this.PAYMENT_STATUS,dos);
					
					// Long
				
						if(this.REF_ORG_DEPARTMENT == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeLong(this.REF_ORG_DEPARTMENT);
		            	}
					
					// long
				
		            	dos.writeLong(this.REF_CLIENT);
					
					// long
				
		            	dos.writeLong(this.VERSION);
					
					// long
				
		            	dos.writeLong(this.CREATED_BY);
					
					// long
				
		            	dos.writeLong(this.UPDATED_BY);
					
					// java.util.Date
				
						writeDate(this.CREATED_DATE,dos);
					
					// java.util.Date
				
						writeDate(this.UPDATED_DATE,dos);
					
					// String
				
						writeString(this.BENEFICIARY_BANK_NAME,dos);
					
					// String
				
						writeString(this.BENEFICIARY_BANK_ACCOUNT_DESC,dos);
					
					// Long
				
						if(this.REF_WARRANT_LINE == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeLong(this.REF_WARRANT_LINE);
		            	}
					
					// String
				
						writeString(this.IPPIS_EMPLOYEE_ID,dos);
					
					// String
				
						writeString(this.IPPIS_BENEFICIARY_CODE,dos);
					
					// String
				
						writeString(this.IPPIS_PAYMENT_TYPE_CODE,dos);
					
					// String
				
						writeString(this.LEDGER_STATUS,dos);
					
					// long
				
		            	dos.writeLong(this.IN_SETTLEMENT);
					
					// Long
				
						if(this.REF_AUTHORIZING_ORG == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeLong(this.REF_AUTHORIZING_ORG);
		            	}
					
					// Long
				
						if(this.REF_FUND_MDA == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeLong(this.REF_FUND_MDA);
		            	}
					
					// long
				
		            	dos.writeLong(this.PAYMENT_BATCH_NUMBER);
					
					// Long
				
						if(this.REF_ORIGINAL_PAYMENT_ORDER == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeLong(this.REF_ORIGINAL_PAYMENT_ORDER);
		            	}
					
					// Long
				
						if(this.REF_CREDITED_PAYMENT_ORDER == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeLong(this.REF_CREDITED_PAYMENT_ORDER);
		            	}
					
					// String
				
						writeString(this.RESENDING_STATUS,dos);
					
					// Long
				
						if(this.IS_REFUND == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeLong(this.IS_REFUND);
		            	}
					
					// String
				
						writeString(this.REFUND_REASON,dos);
					
					// String
				
						writeString(this.REFUND_ORIGINATING_DOCUMENT,dos);
					
					// java.util.Date
				
						writeDate(this.etl_date,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("ID="+String.valueOf(ID));
		sb.append(",REF_PAYMENT_BATCH="+String.valueOf(REF_PAYMENT_BATCH));
		sb.append(",LINE_NO="+String.valueOf(LINE_NO));
		sb.append(",REF_ORGANIZATION="+String.valueOf(REF_ORGANIZATION));
		sb.append(",BENEFICIARY_TYPE="+BENEFICIARY_TYPE);
		sb.append(",BENEFICIARY_TIN="+BENEFICIARY_TIN);
		sb.append(",BENEFICIARY_NAME="+BENEFICIARY_NAME);
		sb.append(",BENEFICIARY_BANK_CODE="+BENEFICIARY_BANK_CODE);
		sb.append(",BENEFICIARY_BANK_ACCOUNT="+BENEFICIARY_BANK_ACCOUNT);
		sb.append(",BENEFICIARY_EMAIL="+BENEFICIARY_EMAIL);
		sb.append(",BENEFICIARY_PHONE="+BENEFICIARY_PHONE);
		sb.append(",BENEFICIARY_ADDRESS="+BENEFICIARY_ADDRESS);
		sb.append(",REF_CURRENCY="+String.valueOf(REF_CURRENCY));
		sb.append(",CURRENCY_RATE="+String.valueOf(CURRENCY_RATE));
		sb.append(",PAYMENT_REFERENCE_NO="+PAYMENT_REFERENCE_NO);
		sb.append(",PAYMENT_AMOUNT="+String.valueOf(PAYMENT_AMOUNT));
		sb.append(",PAYMENT_AMOUNT_BASE="+String.valueOf(PAYMENT_AMOUNT_BASE));
		sb.append(",DESCRIPTION="+DESCRIPTION);
		sb.append(",SUPPLIER_INVOICE_NO="+SUPPLIER_INVOICE_NO);
		sb.append(",SUPPLIER_INVOICE_DATE="+String.valueOf(SUPPLIER_INVOICE_DATE));
		sb.append(",SUPPLIER_INVOICE_DUEDATE="+String.valueOf(SUPPLIER_INVOICE_DUEDATE));
		sb.append(",REF_SUPPLIER="+String.valueOf(REF_SUPPLIER));
		sb.append(",REF_EMPLOYEE="+String.valueOf(REF_EMPLOYEE));
		sb.append(",REF_CUSTOMER="+String.valueOf(REF_CUSTOMER));
		sb.append(",PAYMENT_STATUS="+PAYMENT_STATUS);
		sb.append(",REF_ORG_DEPARTMENT="+String.valueOf(REF_ORG_DEPARTMENT));
		sb.append(",REF_CLIENT="+String.valueOf(REF_CLIENT));
		sb.append(",VERSION="+String.valueOf(VERSION));
		sb.append(",CREATED_BY="+String.valueOf(CREATED_BY));
		sb.append(",UPDATED_BY="+String.valueOf(UPDATED_BY));
		sb.append(",CREATED_DATE="+String.valueOf(CREATED_DATE));
		sb.append(",UPDATED_DATE="+String.valueOf(UPDATED_DATE));
		sb.append(",BENEFICIARY_BANK_NAME="+BENEFICIARY_BANK_NAME);
		sb.append(",BENEFICIARY_BANK_ACCOUNT_DESC="+BENEFICIARY_BANK_ACCOUNT_DESC);
		sb.append(",REF_WARRANT_LINE="+String.valueOf(REF_WARRANT_LINE));
		sb.append(",IPPIS_EMPLOYEE_ID="+IPPIS_EMPLOYEE_ID);
		sb.append(",IPPIS_BENEFICIARY_CODE="+IPPIS_BENEFICIARY_CODE);
		sb.append(",IPPIS_PAYMENT_TYPE_CODE="+IPPIS_PAYMENT_TYPE_CODE);
		sb.append(",LEDGER_STATUS="+LEDGER_STATUS);
		sb.append(",IN_SETTLEMENT="+String.valueOf(IN_SETTLEMENT));
		sb.append(",REF_AUTHORIZING_ORG="+String.valueOf(REF_AUTHORIZING_ORG));
		sb.append(",REF_FUND_MDA="+String.valueOf(REF_FUND_MDA));
		sb.append(",PAYMENT_BATCH_NUMBER="+String.valueOf(PAYMENT_BATCH_NUMBER));
		sb.append(",REF_ORIGINAL_PAYMENT_ORDER="+String.valueOf(REF_ORIGINAL_PAYMENT_ORDER));
		sb.append(",REF_CREDITED_PAYMENT_ORDER="+String.valueOf(REF_CREDITED_PAYMENT_ORDER));
		sb.append(",RESENDING_STATUS="+RESENDING_STATUS);
		sb.append(",IS_REFUND="+String.valueOf(IS_REFUND));
		sb.append(",REFUND_REASON="+REFUND_REASON);
		sb.append(",REFUND_ORIGINATING_DOCUMENT="+REFUND_ORIGINATING_DOCUMENT);
		sb.append(",etl_date="+String.valueOf(etl_date));
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(outputTableStruct other) {

		int returnValue = -1;
		
						returnValue = checkNullsAndCompare(this.ID, other.ID);
						if(returnValue != 0) {
							return returnValue;
						}

					
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}

public static class row5Struct implements routines.system.IPersistableRow<row5Struct> {
    final static byte[] commonByteArrayLock_GIFMIS_payment_order_bulk_2 = new byte[0];
    static byte[] commonByteArray_GIFMIS_payment_order_bulk_2 = new byte[0];

	
			    public long ID;

				public long getID () {
					return this.ID;
				}
				
			    public long REF_PAYMENT_BATCH;

				public long getREF_PAYMENT_BATCH () {
					return this.REF_PAYMENT_BATCH;
				}
				
			    public long LINE_NO;

				public long getLINE_NO () {
					return this.LINE_NO;
				}
				
			    public long REF_ORGANIZATION;

				public long getREF_ORGANIZATION () {
					return this.REF_ORGANIZATION;
				}
				
			    public String BENEFICIARY_TYPE;

				public String getBENEFICIARY_TYPE () {
					return this.BENEFICIARY_TYPE;
				}
				
			    public String BENEFICIARY_TIN;

				public String getBENEFICIARY_TIN () {
					return this.BENEFICIARY_TIN;
				}
				
			    public String BENEFICIARY_NAME;

				public String getBENEFICIARY_NAME () {
					return this.BENEFICIARY_NAME;
				}
				
			    public String BENEFICIARY_BANK_CODE;

				public String getBENEFICIARY_BANK_CODE () {
					return this.BENEFICIARY_BANK_CODE;
				}
				
			    public String BENEFICIARY_BANK_ACCOUNT;

				public String getBENEFICIARY_BANK_ACCOUNT () {
					return this.BENEFICIARY_BANK_ACCOUNT;
				}
				
			    public String BENEFICIARY_EMAIL;

				public String getBENEFICIARY_EMAIL () {
					return this.BENEFICIARY_EMAIL;
				}
				
			    public String BENEFICIARY_PHONE;

				public String getBENEFICIARY_PHONE () {
					return this.BENEFICIARY_PHONE;
				}
				
			    public String BENEFICIARY_ADDRESS;

				public String getBENEFICIARY_ADDRESS () {
					return this.BENEFICIARY_ADDRESS;
				}
				
			    public long REF_CURRENCY;

				public long getREF_CURRENCY () {
					return this.REF_CURRENCY;
				}
				
			    public BigDecimal CURRENCY_RATE;

				public BigDecimal getCURRENCY_RATE () {
					return this.CURRENCY_RATE;
				}
				
			    public String PAYMENT_REFERENCE_NO;

				public String getPAYMENT_REFERENCE_NO () {
					return this.PAYMENT_REFERENCE_NO;
				}
				
			    public BigDecimal PAYMENT_AMOUNT;

				public BigDecimal getPAYMENT_AMOUNT () {
					return this.PAYMENT_AMOUNT;
				}
				
			    public BigDecimal PAYMENT_AMOUNT_BASE;

				public BigDecimal getPAYMENT_AMOUNT_BASE () {
					return this.PAYMENT_AMOUNT_BASE;
				}
				
			    public String DESCRIPTION;

				public String getDESCRIPTION () {
					return this.DESCRIPTION;
				}
				
			    public String SUPPLIER_INVOICE_NO;

				public String getSUPPLIER_INVOICE_NO () {
					return this.SUPPLIER_INVOICE_NO;
				}
				
			    public java.util.Date SUPPLIER_INVOICE_DATE;

				public java.util.Date getSUPPLIER_INVOICE_DATE () {
					return this.SUPPLIER_INVOICE_DATE;
				}
				
			    public java.util.Date SUPPLIER_INVOICE_DUEDATE;

				public java.util.Date getSUPPLIER_INVOICE_DUEDATE () {
					return this.SUPPLIER_INVOICE_DUEDATE;
				}
				
			    public Long REF_SUPPLIER;

				public Long getREF_SUPPLIER () {
					return this.REF_SUPPLIER;
				}
				
			    public Long REF_EMPLOYEE;

				public Long getREF_EMPLOYEE () {
					return this.REF_EMPLOYEE;
				}
				
			    public Long REF_CUSTOMER;

				public Long getREF_CUSTOMER () {
					return this.REF_CUSTOMER;
				}
				
			    public String PAYMENT_STATUS;

				public String getPAYMENT_STATUS () {
					return this.PAYMENT_STATUS;
				}
				
			    public Long REF_ORG_DEPARTMENT;

				public Long getREF_ORG_DEPARTMENT () {
					return this.REF_ORG_DEPARTMENT;
				}
				
			    public long REF_CLIENT;

				public long getREF_CLIENT () {
					return this.REF_CLIENT;
				}
				
			    public long VERSION;

				public long getVERSION () {
					return this.VERSION;
				}
				
			    public long CREATED_BY;

				public long getCREATED_BY () {
					return this.CREATED_BY;
				}
				
			    public long UPDATED_BY;

				public long getUPDATED_BY () {
					return this.UPDATED_BY;
				}
				
			    public java.util.Date CREATED_DATE;

				public java.util.Date getCREATED_DATE () {
					return this.CREATED_DATE;
				}
				
			    public java.util.Date UPDATED_DATE;

				public java.util.Date getUPDATED_DATE () {
					return this.UPDATED_DATE;
				}
				
			    public String BENEFICIARY_BANK_NAME;

				public String getBENEFICIARY_BANK_NAME () {
					return this.BENEFICIARY_BANK_NAME;
				}
				
			    public String BENEFICIARY_BANK_ACCOUNT_DESC;

				public String getBENEFICIARY_BANK_ACCOUNT_DESC () {
					return this.BENEFICIARY_BANK_ACCOUNT_DESC;
				}
				
			    public Long REF_WARRANT_LINE;

				public Long getREF_WARRANT_LINE () {
					return this.REF_WARRANT_LINE;
				}
				
			    public String IPPIS_EMPLOYEE_ID;

				public String getIPPIS_EMPLOYEE_ID () {
					return this.IPPIS_EMPLOYEE_ID;
				}
				
			    public String IPPIS_BENEFICIARY_CODE;

				public String getIPPIS_BENEFICIARY_CODE () {
					return this.IPPIS_BENEFICIARY_CODE;
				}
				
			    public String IPPIS_PAYMENT_TYPE_CODE;

				public String getIPPIS_PAYMENT_TYPE_CODE () {
					return this.IPPIS_PAYMENT_TYPE_CODE;
				}
				
			    public String LEDGER_STATUS;

				public String getLEDGER_STATUS () {
					return this.LEDGER_STATUS;
				}
				
			    public long IN_SETTLEMENT;

				public long getIN_SETTLEMENT () {
					return this.IN_SETTLEMENT;
				}
				
			    public Long REF_AUTHORIZING_ORG;

				public Long getREF_AUTHORIZING_ORG () {
					return this.REF_AUTHORIZING_ORG;
				}
				
			    public Long REF_FUND_MDA;

				public Long getREF_FUND_MDA () {
					return this.REF_FUND_MDA;
				}
				
			    public long PAYMENT_BATCH_NUMBER;

				public long getPAYMENT_BATCH_NUMBER () {
					return this.PAYMENT_BATCH_NUMBER;
				}
				
			    public Long REF_ORIGINAL_PAYMENT_ORDER;

				public Long getREF_ORIGINAL_PAYMENT_ORDER () {
					return this.REF_ORIGINAL_PAYMENT_ORDER;
				}
				
			    public Long REF_CREDITED_PAYMENT_ORDER;

				public Long getREF_CREDITED_PAYMENT_ORDER () {
					return this.REF_CREDITED_PAYMENT_ORDER;
				}
				
			    public String RESENDING_STATUS;

				public String getRESENDING_STATUS () {
					return this.RESENDING_STATUS;
				}
				
			    public Long IS_REFUND;

				public Long getIS_REFUND () {
					return this.IS_REFUND;
				}
				
			    public String REFUND_REASON;

				public String getREFUND_REASON () {
					return this.REFUND_REASON;
				}
				
			    public String REFUND_ORIGINATING_DOCUMENT;

				public String getREFUND_ORIGINATING_DOCUMENT () {
					return this.REFUND_ORIGINATING_DOCUMENT;
				}
				



	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_GIFMIS_payment_order_bulk_2.length) {
				if(length < 1024 && commonByteArray_GIFMIS_payment_order_bulk_2.length == 0) {
   					commonByteArray_GIFMIS_payment_order_bulk_2 = new byte[1024];
				} else {
   					commonByteArray_GIFMIS_payment_order_bulk_2 = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_GIFMIS_payment_order_bulk_2, 0, length);
			strReturn = new String(commonByteArray_GIFMIS_payment_order_bulk_2, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }

	private java.util.Date readDate(ObjectInputStream dis) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(dis.readLong());
		}
		return dateReturn;
	}

    private void writeDate(java.util.Date date1, ObjectOutputStream dos) throws IOException{
		if(date1 == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeLong(date1.getTime());
    	}
    }

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_GIFMIS_payment_order_bulk_2) {

        	try {

        		int length = 0;
		
			        this.ID = dis.readLong();
					
			        this.REF_PAYMENT_BATCH = dis.readLong();
					
			        this.LINE_NO = dis.readLong();
					
			        this.REF_ORGANIZATION = dis.readLong();
					
					this.BENEFICIARY_TYPE = readString(dis);
					
					this.BENEFICIARY_TIN = readString(dis);
					
					this.BENEFICIARY_NAME = readString(dis);
					
					this.BENEFICIARY_BANK_CODE = readString(dis);
					
					this.BENEFICIARY_BANK_ACCOUNT = readString(dis);
					
					this.BENEFICIARY_EMAIL = readString(dis);
					
					this.BENEFICIARY_PHONE = readString(dis);
					
					this.BENEFICIARY_ADDRESS = readString(dis);
					
			        this.REF_CURRENCY = dis.readLong();
					
						this.CURRENCY_RATE = (BigDecimal) dis.readObject();
					
					this.PAYMENT_REFERENCE_NO = readString(dis);
					
						this.PAYMENT_AMOUNT = (BigDecimal) dis.readObject();
					
						this.PAYMENT_AMOUNT_BASE = (BigDecimal) dis.readObject();
					
					this.DESCRIPTION = readString(dis);
					
					this.SUPPLIER_INVOICE_NO = readString(dis);
					
					this.SUPPLIER_INVOICE_DATE = readDate(dis);
					
					this.SUPPLIER_INVOICE_DUEDATE = readDate(dis);
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.REF_SUPPLIER = null;
           				} else {
           			    	this.REF_SUPPLIER = dis.readLong();
           				}
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.REF_EMPLOYEE = null;
           				} else {
           			    	this.REF_EMPLOYEE = dis.readLong();
           				}
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.REF_CUSTOMER = null;
           				} else {
           			    	this.REF_CUSTOMER = dis.readLong();
           				}
					
					this.PAYMENT_STATUS = readString(dis);
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.REF_ORG_DEPARTMENT = null;
           				} else {
           			    	this.REF_ORG_DEPARTMENT = dis.readLong();
           				}
					
			        this.REF_CLIENT = dis.readLong();
					
			        this.VERSION = dis.readLong();
					
			        this.CREATED_BY = dis.readLong();
					
			        this.UPDATED_BY = dis.readLong();
					
					this.CREATED_DATE = readDate(dis);
					
					this.UPDATED_DATE = readDate(dis);
					
					this.BENEFICIARY_BANK_NAME = readString(dis);
					
					this.BENEFICIARY_BANK_ACCOUNT_DESC = readString(dis);
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.REF_WARRANT_LINE = null;
           				} else {
           			    	this.REF_WARRANT_LINE = dis.readLong();
           				}
					
					this.IPPIS_EMPLOYEE_ID = readString(dis);
					
					this.IPPIS_BENEFICIARY_CODE = readString(dis);
					
					this.IPPIS_PAYMENT_TYPE_CODE = readString(dis);
					
					this.LEDGER_STATUS = readString(dis);
					
			        this.IN_SETTLEMENT = dis.readLong();
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.REF_AUTHORIZING_ORG = null;
           				} else {
           			    	this.REF_AUTHORIZING_ORG = dis.readLong();
           				}
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.REF_FUND_MDA = null;
           				} else {
           			    	this.REF_FUND_MDA = dis.readLong();
           				}
					
			        this.PAYMENT_BATCH_NUMBER = dis.readLong();
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.REF_ORIGINAL_PAYMENT_ORDER = null;
           				} else {
           			    	this.REF_ORIGINAL_PAYMENT_ORDER = dis.readLong();
           				}
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.REF_CREDITED_PAYMENT_ORDER = null;
           				} else {
           			    	this.REF_CREDITED_PAYMENT_ORDER = dis.readLong();
           				}
					
					this.RESENDING_STATUS = readString(dis);
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.IS_REFUND = null;
           				} else {
           			    	this.IS_REFUND = dis.readLong();
           				}
					
					this.REFUND_REASON = readString(dis);
					
					this.REFUND_ORIGINATING_DOCUMENT = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		
			} catch(ClassNotFoundException eCNFE) {
				 throw new RuntimeException(eCNFE);
		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// long
				
		            	dos.writeLong(this.ID);
					
					// long
				
		            	dos.writeLong(this.REF_PAYMENT_BATCH);
					
					// long
				
		            	dos.writeLong(this.LINE_NO);
					
					// long
				
		            	dos.writeLong(this.REF_ORGANIZATION);
					
					// String
				
						writeString(this.BENEFICIARY_TYPE,dos);
					
					// String
				
						writeString(this.BENEFICIARY_TIN,dos);
					
					// String
				
						writeString(this.BENEFICIARY_NAME,dos);
					
					// String
				
						writeString(this.BENEFICIARY_BANK_CODE,dos);
					
					// String
				
						writeString(this.BENEFICIARY_BANK_ACCOUNT,dos);
					
					// String
				
						writeString(this.BENEFICIARY_EMAIL,dos);
					
					// String
				
						writeString(this.BENEFICIARY_PHONE,dos);
					
					// String
				
						writeString(this.BENEFICIARY_ADDRESS,dos);
					
					// long
				
		            	dos.writeLong(this.REF_CURRENCY);
					
					// BigDecimal
				
       			    	dos.writeObject(this.CURRENCY_RATE);
					
					// String
				
						writeString(this.PAYMENT_REFERENCE_NO,dos);
					
					// BigDecimal
				
       			    	dos.writeObject(this.PAYMENT_AMOUNT);
					
					// BigDecimal
				
       			    	dos.writeObject(this.PAYMENT_AMOUNT_BASE);
					
					// String
				
						writeString(this.DESCRIPTION,dos);
					
					// String
				
						writeString(this.SUPPLIER_INVOICE_NO,dos);
					
					// java.util.Date
				
						writeDate(this.SUPPLIER_INVOICE_DATE,dos);
					
					// java.util.Date
				
						writeDate(this.SUPPLIER_INVOICE_DUEDATE,dos);
					
					// Long
				
						if(this.REF_SUPPLIER == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeLong(this.REF_SUPPLIER);
		            	}
					
					// Long
				
						if(this.REF_EMPLOYEE == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeLong(this.REF_EMPLOYEE);
		            	}
					
					// Long
				
						if(this.REF_CUSTOMER == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeLong(this.REF_CUSTOMER);
		            	}
					
					// String
				
						writeString(this.PAYMENT_STATUS,dos);
					
					// Long
				
						if(this.REF_ORG_DEPARTMENT == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeLong(this.REF_ORG_DEPARTMENT);
		            	}
					
					// long
				
		            	dos.writeLong(this.REF_CLIENT);
					
					// long
				
		            	dos.writeLong(this.VERSION);
					
					// long
				
		            	dos.writeLong(this.CREATED_BY);
					
					// long
				
		            	dos.writeLong(this.UPDATED_BY);
					
					// java.util.Date
				
						writeDate(this.CREATED_DATE,dos);
					
					// java.util.Date
				
						writeDate(this.UPDATED_DATE,dos);
					
					// String
				
						writeString(this.BENEFICIARY_BANK_NAME,dos);
					
					// String
				
						writeString(this.BENEFICIARY_BANK_ACCOUNT_DESC,dos);
					
					// Long
				
						if(this.REF_WARRANT_LINE == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeLong(this.REF_WARRANT_LINE);
		            	}
					
					// String
				
						writeString(this.IPPIS_EMPLOYEE_ID,dos);
					
					// String
				
						writeString(this.IPPIS_BENEFICIARY_CODE,dos);
					
					// String
				
						writeString(this.IPPIS_PAYMENT_TYPE_CODE,dos);
					
					// String
				
						writeString(this.LEDGER_STATUS,dos);
					
					// long
				
		            	dos.writeLong(this.IN_SETTLEMENT);
					
					// Long
				
						if(this.REF_AUTHORIZING_ORG == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeLong(this.REF_AUTHORIZING_ORG);
		            	}
					
					// Long
				
						if(this.REF_FUND_MDA == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeLong(this.REF_FUND_MDA);
		            	}
					
					// long
				
		            	dos.writeLong(this.PAYMENT_BATCH_NUMBER);
					
					// Long
				
						if(this.REF_ORIGINAL_PAYMENT_ORDER == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeLong(this.REF_ORIGINAL_PAYMENT_ORDER);
		            	}
					
					// Long
				
						if(this.REF_CREDITED_PAYMENT_ORDER == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeLong(this.REF_CREDITED_PAYMENT_ORDER);
		            	}
					
					// String
				
						writeString(this.RESENDING_STATUS,dos);
					
					// Long
				
						if(this.IS_REFUND == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeLong(this.IS_REFUND);
		            	}
					
					// String
				
						writeString(this.REFUND_REASON,dos);
					
					// String
				
						writeString(this.REFUND_ORIGINATING_DOCUMENT,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("ID="+String.valueOf(ID));
		sb.append(",REF_PAYMENT_BATCH="+String.valueOf(REF_PAYMENT_BATCH));
		sb.append(",LINE_NO="+String.valueOf(LINE_NO));
		sb.append(",REF_ORGANIZATION="+String.valueOf(REF_ORGANIZATION));
		sb.append(",BENEFICIARY_TYPE="+BENEFICIARY_TYPE);
		sb.append(",BENEFICIARY_TIN="+BENEFICIARY_TIN);
		sb.append(",BENEFICIARY_NAME="+BENEFICIARY_NAME);
		sb.append(",BENEFICIARY_BANK_CODE="+BENEFICIARY_BANK_CODE);
		sb.append(",BENEFICIARY_BANK_ACCOUNT="+BENEFICIARY_BANK_ACCOUNT);
		sb.append(",BENEFICIARY_EMAIL="+BENEFICIARY_EMAIL);
		sb.append(",BENEFICIARY_PHONE="+BENEFICIARY_PHONE);
		sb.append(",BENEFICIARY_ADDRESS="+BENEFICIARY_ADDRESS);
		sb.append(",REF_CURRENCY="+String.valueOf(REF_CURRENCY));
		sb.append(",CURRENCY_RATE="+String.valueOf(CURRENCY_RATE));
		sb.append(",PAYMENT_REFERENCE_NO="+PAYMENT_REFERENCE_NO);
		sb.append(",PAYMENT_AMOUNT="+String.valueOf(PAYMENT_AMOUNT));
		sb.append(",PAYMENT_AMOUNT_BASE="+String.valueOf(PAYMENT_AMOUNT_BASE));
		sb.append(",DESCRIPTION="+DESCRIPTION);
		sb.append(",SUPPLIER_INVOICE_NO="+SUPPLIER_INVOICE_NO);
		sb.append(",SUPPLIER_INVOICE_DATE="+String.valueOf(SUPPLIER_INVOICE_DATE));
		sb.append(",SUPPLIER_INVOICE_DUEDATE="+String.valueOf(SUPPLIER_INVOICE_DUEDATE));
		sb.append(",REF_SUPPLIER="+String.valueOf(REF_SUPPLIER));
		sb.append(",REF_EMPLOYEE="+String.valueOf(REF_EMPLOYEE));
		sb.append(",REF_CUSTOMER="+String.valueOf(REF_CUSTOMER));
		sb.append(",PAYMENT_STATUS="+PAYMENT_STATUS);
		sb.append(",REF_ORG_DEPARTMENT="+String.valueOf(REF_ORG_DEPARTMENT));
		sb.append(",REF_CLIENT="+String.valueOf(REF_CLIENT));
		sb.append(",VERSION="+String.valueOf(VERSION));
		sb.append(",CREATED_BY="+String.valueOf(CREATED_BY));
		sb.append(",UPDATED_BY="+String.valueOf(UPDATED_BY));
		sb.append(",CREATED_DATE="+String.valueOf(CREATED_DATE));
		sb.append(",UPDATED_DATE="+String.valueOf(UPDATED_DATE));
		sb.append(",BENEFICIARY_BANK_NAME="+BENEFICIARY_BANK_NAME);
		sb.append(",BENEFICIARY_BANK_ACCOUNT_DESC="+BENEFICIARY_BANK_ACCOUNT_DESC);
		sb.append(",REF_WARRANT_LINE="+String.valueOf(REF_WARRANT_LINE));
		sb.append(",IPPIS_EMPLOYEE_ID="+IPPIS_EMPLOYEE_ID);
		sb.append(",IPPIS_BENEFICIARY_CODE="+IPPIS_BENEFICIARY_CODE);
		sb.append(",IPPIS_PAYMENT_TYPE_CODE="+IPPIS_PAYMENT_TYPE_CODE);
		sb.append(",LEDGER_STATUS="+LEDGER_STATUS);
		sb.append(",IN_SETTLEMENT="+String.valueOf(IN_SETTLEMENT));
		sb.append(",REF_AUTHORIZING_ORG="+String.valueOf(REF_AUTHORIZING_ORG));
		sb.append(",REF_FUND_MDA="+String.valueOf(REF_FUND_MDA));
		sb.append(",PAYMENT_BATCH_NUMBER="+String.valueOf(PAYMENT_BATCH_NUMBER));
		sb.append(",REF_ORIGINAL_PAYMENT_ORDER="+String.valueOf(REF_ORIGINAL_PAYMENT_ORDER));
		sb.append(",REF_CREDITED_PAYMENT_ORDER="+String.valueOf(REF_CREDITED_PAYMENT_ORDER));
		sb.append(",RESENDING_STATUS="+RESENDING_STATUS);
		sb.append(",IS_REFUND="+String.valueOf(IS_REFUND));
		sb.append(",REFUND_REASON="+REFUND_REASON);
		sb.append(",REFUND_ORIGINATING_DOCUMENT="+REFUND_ORIGINATING_DOCUMENT);
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(row5Struct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}
public void tDBInput_3Process(final java.util.Map<String, Object> globalMap) throws TalendException {
	globalMap.put("tDBInput_3_SUBPROCESS_STATE", 0);

 final boolean execStat = this.execStat;
	
		String iterateId = "";
	
	
	String currentComponent = "";
	java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

	try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception().getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { //start the resume
				globalResumeTicket = true;



		row5Struct row5 = new row5Struct();
outputTableStruct outputTable = new outputTableStruct();





	
	/**
	 * [tDBOutputBulk_1 begin ] start
	 */

	

	
		
		ok_Hash.put("tDBOutputBulk_1", false);
		start_Hash.put("tDBOutputBulk_1", System.currentTimeMillis());
		
				talendStats_STATS.addMessage("begin","tDBOutputBulk_1");
				talendStats_STATSProcess(globalMap);
			
	
	currentComponent="tDBOutputBulk_1";

	
			if (execStat) {
				if(resourceMap.get("inIterateVComp") == null){
					
						runStat.updateStatOnConnection("outputTable" + iterateId, 0, 0);
					
				}
			} 

		
		int tos_count_tDBOutputBulk_1 = 0;
		
    	class BytesLimit65535_tDBOutputBulk_1{
    		public void limitLog4jByte() throws Exception{
    			
    		}
    	}
    	
        new BytesLimit65535_tDBOutputBulk_1().limitLog4jByte();

		int nb_line_tDBOutputBulk_1 = 0;
		
		final Character OUT_DELIM_tDBOutputBulk_1 = /** Start field tDBOutputBulk_1:FIELDSEPARATOR */";"/** End field tDBOutputBulk_1:FIELDSEPARATOR */.charAt(0);
		
		final String OUT_DELIM_ROWSEP_tDBOutputBulk_1 = /** Start field tDBOutputBulk_1:ROWSEPARATOR */"\n"/** End field tDBOutputBulk_1:ROWSEPARATOR */;
		
		java.io.File file_tDBOutputBulk_1=new java.io.File(/** Start field tDBOutputBulk_1:FILENAME */context.filePath+"/"+jobName+"_out.csv"/** End field tDBOutputBulk_1:FILENAME */);
		final java.io.BufferedWriter outtDBOutputBulk_1 = new java.io.BufferedWriter(new java.io.OutputStreamWriter(
        		new java.io.FileOutputStream(file_tDBOutputBulk_1, false),"UTF-8"));
        resourceMap.put("outtDBOutputBulk_1", outtDBOutputBulk_1); 
		if(file_tDBOutputBulk_1.length()==0){
    			outtDBOutputBulk_1.write("ID");
    		outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());
    			outtDBOutputBulk_1.write("REF_PAYMENT_BATCH");
    		outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());
    			outtDBOutputBulk_1.write("LINE_NO");
    		outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());
    			outtDBOutputBulk_1.write("REF_ORGANIZATION");
    		outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());
    			outtDBOutputBulk_1.write("BENEFICIARY_TYPE");
    		outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());
    			outtDBOutputBulk_1.write("BENEFICIARY_TIN");
    		outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());
    			outtDBOutputBulk_1.write("BENEFICIARY_NAME");
    		outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());
    			outtDBOutputBulk_1.write("BENEFICIARY_BANK_CODE");
    		outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());
    			outtDBOutputBulk_1.write("BENEFICIARY_BANK_ACCOUNT");
    		outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());
    			outtDBOutputBulk_1.write("BENEFICIARY_EMAIL");
    		outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());
    			outtDBOutputBulk_1.write("BENEFICIARY_PHONE");
    		outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());
    			outtDBOutputBulk_1.write("BENEFICIARY_ADDRESS");
    		outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());
    			outtDBOutputBulk_1.write("REF_CURRENCY");
    		outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());
    			outtDBOutputBulk_1.write("CURRENCY_RATE");
    		outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());
    			outtDBOutputBulk_1.write("PAYMENT_REFERENCE_NO");
    		outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());
    			outtDBOutputBulk_1.write("PAYMENT_AMOUNT");
    		outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());
    			outtDBOutputBulk_1.write("PAYMENT_AMOUNT_BASE");
    		outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());
    			outtDBOutputBulk_1.write("DESCRIPTION");
    		outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());
    			outtDBOutputBulk_1.write("SUPPLIER_INVOICE_NO");
    		outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());
    			outtDBOutputBulk_1.write("SUPPLIER_INVOICE_DATE");
    		outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());
    			outtDBOutputBulk_1.write("SUPPLIER_INVOICE_DUEDATE");
    		outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());
    			outtDBOutputBulk_1.write("REF_SUPPLIER");
    		outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());
    			outtDBOutputBulk_1.write("REF_EMPLOYEE");
    		outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());
    			outtDBOutputBulk_1.write("REF_CUSTOMER");
    		outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());
    			outtDBOutputBulk_1.write("PAYMENT_STATUS");
    		outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());
    			outtDBOutputBulk_1.write("REF_ORG_DEPARTMENT");
    		outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());
    			outtDBOutputBulk_1.write("REF_CLIENT");
    		outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());
    			outtDBOutputBulk_1.write("VERSION");
    		outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());
    			outtDBOutputBulk_1.write("CREATED_BY");
    		outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());
    			outtDBOutputBulk_1.write("UPDATED_BY");
    		outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());
    			outtDBOutputBulk_1.write("CREATED_DATE");
    		outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());
    			outtDBOutputBulk_1.write("UPDATED_DATE");
    		outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());
    			outtDBOutputBulk_1.write("BENEFICIARY_BANK_NAME");
    		outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());
    			outtDBOutputBulk_1.write("BENEFICIARY_BANK_ACCOUNT_DESC");
    		outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());
    			outtDBOutputBulk_1.write("REF_WARRANT_LINE");
    		outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());
    			outtDBOutputBulk_1.write("IPPIS_EMPLOYEE_ID");
    		outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());
    			outtDBOutputBulk_1.write("IPPIS_BENEFICIARY_CODE");
    		outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());
    			outtDBOutputBulk_1.write("IPPIS_PAYMENT_TYPE_CODE");
    		outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());
    			outtDBOutputBulk_1.write("LEDGER_STATUS");
    		outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());
    			outtDBOutputBulk_1.write("IN_SETTLEMENT");
    		outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());
    			outtDBOutputBulk_1.write("REF_AUTHORIZING_ORG");
    		outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());
    			outtDBOutputBulk_1.write("REF_FUND_MDA");
    		outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());
    			outtDBOutputBulk_1.write("PAYMENT_BATCH_NUMBER");
    		outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());
    			outtDBOutputBulk_1.write("REF_ORIGINAL_PAYMENT_ORDER");
    		outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());
    			outtDBOutputBulk_1.write("REF_CREDITED_PAYMENT_ORDER");
    		outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());
    			outtDBOutputBulk_1.write("RESENDING_STATUS");
    		outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());
    			outtDBOutputBulk_1.write("IS_REFUND");
    		outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());
    			outtDBOutputBulk_1.write("REFUND_REASON");
    		outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());
    			outtDBOutputBulk_1.write("REFUND_ORIGINATING_DOCUMENT");
    		outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());
    			outtDBOutputBulk_1.write("etl_date");
    		
    		outtDBOutputBulk_1.write(OUT_DELIM_ROWSEP_tDBOutputBulk_1);
    	}

 



/**
 * [tDBOutputBulk_1 begin ] stop
 */



	
	/**
	 * [tMap_1 begin ] start
	 */

	

	
		
		ok_Hash.put("tMap_1", false);
		start_Hash.put("tMap_1", System.currentTimeMillis());
		
				talendStats_STATS.addMessage("begin","tMap_1");
				talendStats_STATSProcess(globalMap);
			
	
	currentComponent="tMap_1";

	
			if (execStat) {
				if(resourceMap.get("inIterateVComp") == null){
					
						runStat.updateStatOnConnection("row5" + iterateId, 0, 0);
					
				}
			} 

		
		int tos_count_tMap_1 = 0;
		
    	class BytesLimit65535_tMap_1{
    		public void limitLog4jByte() throws Exception{
    			
    		}
    	}
    	
        new BytesLimit65535_tMap_1().limitLog4jByte();




// ###############################
// # Lookup's keys initialization
// ###############################        

// ###############################
// # Vars initialization
class  Var__tMap_1__Struct  {
}
Var__tMap_1__Struct Var__tMap_1 = new Var__tMap_1__Struct();
// ###############################

// ###############################
// # Outputs initialization
outputTableStruct outputTable_tmp = new outputTableStruct();
// ###############################

        
        



        









 



/**
 * [tMap_1 begin ] stop
 */



	
	/**
	 * [tDBInput_3 begin ] start
	 */

	

	
		
		ok_Hash.put("tDBInput_3", false);
		start_Hash.put("tDBInput_3", System.currentTimeMillis());
		
				talendStats_STATS.addMessage("begin","tDBInput_3");
				talendStats_STATSProcess(globalMap);
			
	
	currentComponent="tDBInput_3";

	
		int tos_count_tDBInput_3 = 0;
		
    	class BytesLimit65535_tDBInput_3{
    		public void limitLog4jByte() throws Exception{
    			
    		}
    	}
    	
        new BytesLimit65535_tDBInput_3().limitLog4jByte();
	


	
		    int nb_line_tDBInput_3 = 0;
		    java.sql.Connection conn_tDBInput_3 = null;
				String driverClass_tDBInput_3 = "oracle.jdbc.OracleDriver";
				java.lang.Class.forName(driverClass_tDBInput_3);
				
			String url_tDBInput_3 = null;
				url_tDBInput_3 = "jdbc:oracle:thin:@(description=(address=(protocol=tcp)(host=" + context.Oracle_Server + ")(port=" + context.Oracle_Port + "))(connect_data=(service_name=" + context.Oracle_ServiceName + ")))";

				String dbUser_tDBInput_3 = context.Oracle_Login;

				

				
	final String decryptedPassword_tDBInput_3 = context.Oracle_Password; 

				String dbPwd_tDBInput_3 = decryptedPassword_tDBInput_3;

				
					java.util.Properties atnParamsPrope_tDBInput_3 = new java.util.Properties();
					atnParamsPrope_tDBInput_3.put("user",dbUser_tDBInput_3);
					atnParamsPrope_tDBInput_3.put("password",dbPwd_tDBInput_3);
                    if(context.Oracle_AdditionalParams != null && !"\"\"".equals(context.Oracle_AdditionalParams) && !"".equals(context.Oracle_AdditionalParams)){
                        atnParamsPrope_tDBInput_3.load(new java.io.ByteArrayInputStream(context.Oracle_AdditionalParams.replace("&", "\n").getBytes()));
                    }
					conn_tDBInput_3 = java.sql.DriverManager.getConnection(url_tDBInput_3, atnParamsPrope_tDBInput_3);
				java.sql.Statement stmtGetTZ_tDBInput_3 = conn_tDBInput_3.createStatement();
				java.sql.ResultSet rsGetTZ_tDBInput_3 = stmtGetTZ_tDBInput_3.executeQuery("select sessiontimezone from dual");
				String sessionTimezone_tDBInput_3 = java.util.TimeZone.getDefault().getID();
				while (rsGetTZ_tDBInput_3.next()) {
					sessionTimezone_tDBInput_3 = rsGetTZ_tDBInput_3.getString(1);
				}
                                if (!(conn_tDBInput_3 instanceof oracle.jdbc.OracleConnection) &&
                                        conn_tDBInput_3.isWrapperFor(oracle.jdbc.OracleConnection.class)) {
                                    if (conn_tDBInput_3.unwrap(oracle.jdbc.OracleConnection.class) != null) {
                                        ((oracle.jdbc.OracleConnection)conn_tDBInput_3.unwrap(oracle.jdbc.OracleConnection.class)).setSessionTimeZone(sessionTimezone_tDBInput_3);
                                    }
                                } else {
                                    ((oracle.jdbc.OracleConnection)conn_tDBInput_3).setSessionTimeZone(sessionTimezone_tDBInput_3);
                                }
		    
			java.sql.Statement stmt_tDBInput_3 = conn_tDBInput_3.createStatement();

		    String dbquery_tDBInput_3 = "select * from PFM."+context.tableName+" where ID Between TO_NUMBER("+"'"+globalMap.get("myKey").toString()+"')+1" + " and TO_NUMBER("+"'"+globalMap.get("myKey").toString()+"')+20000000 and rownum<50001";
			

            	globalMap.put("tDBInput_3_QUERY",dbquery_tDBInput_3);
		    java.sql.ResultSet rs_tDBInput_3 = null;

		    try {
		    	rs_tDBInput_3 = stmt_tDBInput_3.executeQuery(dbquery_tDBInput_3);
		    	java.sql.ResultSetMetaData rsmd_tDBInput_3 = rs_tDBInput_3.getMetaData();
		    	int colQtyInRs_tDBInput_3 = rsmd_tDBInput_3.getColumnCount();

		    String tmpContent_tDBInput_3 = null;
		    
		    
		    while (rs_tDBInput_3.next()) {
		        nb_line_tDBInput_3++;
		        
							if(colQtyInRs_tDBInput_3 < 1) {
								row5.ID = 0;
							} else {
		                          
					if(rs_tDBInput_3.getObject(1) != null) {
						row5.ID = rs_tDBInput_3.getLong(1);
					} else {
				
 	                	throw new RuntimeException("Null value in non-Nullable column");
					}
		                    }
							if(colQtyInRs_tDBInput_3 < 2) {
								row5.REF_PAYMENT_BATCH = 0;
							} else {
		                          
					if(rs_tDBInput_3.getObject(2) != null) {
						row5.REF_PAYMENT_BATCH = rs_tDBInput_3.getLong(2);
					} else {
				
 	                	throw new RuntimeException("Null value in non-Nullable column");
					}
		                    }
							if(colQtyInRs_tDBInput_3 < 3) {
								row5.LINE_NO = 0;
							} else {
		                          
					if(rs_tDBInput_3.getObject(3) != null) {
						row5.LINE_NO = rs_tDBInput_3.getLong(3);
					} else {
				
 	                	throw new RuntimeException("Null value in non-Nullable column");
					}
		                    }
							if(colQtyInRs_tDBInput_3 < 4) {
								row5.REF_ORGANIZATION = 0;
							} else {
		                          
					if(rs_tDBInput_3.getObject(4) != null) {
						row5.REF_ORGANIZATION = rs_tDBInput_3.getLong(4);
					} else {
				
 	                	throw new RuntimeException("Null value in non-Nullable column");
					}
		                    }
							if(colQtyInRs_tDBInput_3 < 5) {
								row5.BENEFICIARY_TYPE = null;
							} else {
	                         		
        	row5.BENEFICIARY_TYPE = routines.system.JDBCUtil.getString(rs_tDBInput_3, 5, false);
		                    }
							if(colQtyInRs_tDBInput_3 < 6) {
								row5.BENEFICIARY_TIN = null;
							} else {
	                         		
        	row5.BENEFICIARY_TIN = routines.system.JDBCUtil.getString(rs_tDBInput_3, 6, false);
		                    }
							if(colQtyInRs_tDBInput_3 < 7) {
								row5.BENEFICIARY_NAME = null;
							} else {
	                         		
        	row5.BENEFICIARY_NAME = routines.system.JDBCUtil.getString(rs_tDBInput_3, 7, false);
		                    }
							if(colQtyInRs_tDBInput_3 < 8) {
								row5.BENEFICIARY_BANK_CODE = null;
							} else {
	                         		
        	row5.BENEFICIARY_BANK_CODE = routines.system.JDBCUtil.getString(rs_tDBInput_3, 8, false);
		                    }
							if(colQtyInRs_tDBInput_3 < 9) {
								row5.BENEFICIARY_BANK_ACCOUNT = null;
							} else {
	                         		
        	row5.BENEFICIARY_BANK_ACCOUNT = routines.system.JDBCUtil.getString(rs_tDBInput_3, 9, false);
		                    }
							if(colQtyInRs_tDBInput_3 < 10) {
								row5.BENEFICIARY_EMAIL = null;
							} else {
	                         		
        	row5.BENEFICIARY_EMAIL = routines.system.JDBCUtil.getString(rs_tDBInput_3, 10, false);
		                    }
							if(colQtyInRs_tDBInput_3 < 11) {
								row5.BENEFICIARY_PHONE = null;
							} else {
	                         		
        	row5.BENEFICIARY_PHONE = routines.system.JDBCUtil.getString(rs_tDBInput_3, 11, false);
		                    }
							if(colQtyInRs_tDBInput_3 < 12) {
								row5.BENEFICIARY_ADDRESS = null;
							} else {
	                         		
        	row5.BENEFICIARY_ADDRESS = routines.system.JDBCUtil.getString(rs_tDBInput_3, 12, false);
		                    }
							if(colQtyInRs_tDBInput_3 < 13) {
								row5.REF_CURRENCY = 0;
							} else {
		                          
					if(rs_tDBInput_3.getObject(13) != null) {
						row5.REF_CURRENCY = rs_tDBInput_3.getLong(13);
					} else {
				
 	                	throw new RuntimeException("Null value in non-Nullable column");
					}
		                    }
							if(colQtyInRs_tDBInput_3 < 14) {
								row5.CURRENCY_RATE = null;
							} else {
		                          
					if(rs_tDBInput_3.getObject(14) != null) {
						row5.CURRENCY_RATE = rs_tDBInput_3.getBigDecimal(14);
					} else {
				
 	                	throw new RuntimeException("Null value in non-Nullable column");
					}
		                    }
							if(colQtyInRs_tDBInput_3 < 15) {
								row5.PAYMENT_REFERENCE_NO = null;
							} else {
	                         		
        	row5.PAYMENT_REFERENCE_NO = routines.system.JDBCUtil.getString(rs_tDBInput_3, 15, false);
		                    }
							if(colQtyInRs_tDBInput_3 < 16) {
								row5.PAYMENT_AMOUNT = null;
							} else {
		                          
					if(rs_tDBInput_3.getObject(16) != null) {
						row5.PAYMENT_AMOUNT = rs_tDBInput_3.getBigDecimal(16);
					} else {
				
 	                	throw new RuntimeException("Null value in non-Nullable column");
					}
		                    }
							if(colQtyInRs_tDBInput_3 < 17) {
								row5.PAYMENT_AMOUNT_BASE = null;
							} else {
		                          
					if(rs_tDBInput_3.getObject(17) != null) {
						row5.PAYMENT_AMOUNT_BASE = rs_tDBInput_3.getBigDecimal(17);
					} else {
				
 	                	throw new RuntimeException("Null value in non-Nullable column");
					}
		                    }
							if(colQtyInRs_tDBInput_3 < 18) {
								row5.DESCRIPTION = null;
							} else {
	                         		
        	row5.DESCRIPTION = routines.system.JDBCUtil.getString(rs_tDBInput_3, 18, false);
		                    }
							if(colQtyInRs_tDBInput_3 < 19) {
								row5.SUPPLIER_INVOICE_NO = null;
							} else {
	                         		
        	row5.SUPPLIER_INVOICE_NO = routines.system.JDBCUtil.getString(rs_tDBInput_3, 19, false);
		                    }
							if(colQtyInRs_tDBInput_3 < 20) {
								row5.SUPPLIER_INVOICE_DATE = null;
							} else {
										
			row5.SUPPLIER_INVOICE_DATE = routines.system.JDBCUtil.getDate(rs_tDBInput_3, 20);
		                    }
							if(colQtyInRs_tDBInput_3 < 21) {
								row5.SUPPLIER_INVOICE_DUEDATE = null;
							} else {
										
			row5.SUPPLIER_INVOICE_DUEDATE = routines.system.JDBCUtil.getDate(rs_tDBInput_3, 21);
		                    }
							if(colQtyInRs_tDBInput_3 < 22) {
								row5.REF_SUPPLIER = null;
							} else {
		                          
					if(rs_tDBInput_3.getObject(22) != null) {
						row5.REF_SUPPLIER = rs_tDBInput_3.getLong(22);
					} else {
				
						row5.REF_SUPPLIER = null;
					}
		                    }
							if(colQtyInRs_tDBInput_3 < 23) {
								row5.REF_EMPLOYEE = null;
							} else {
		                          
					if(rs_tDBInput_3.getObject(23) != null) {
						row5.REF_EMPLOYEE = rs_tDBInput_3.getLong(23);
					} else {
				
						row5.REF_EMPLOYEE = null;
					}
		                    }
							if(colQtyInRs_tDBInput_3 < 24) {
								row5.REF_CUSTOMER = null;
							} else {
		                          
					if(rs_tDBInput_3.getObject(24) != null) {
						row5.REF_CUSTOMER = rs_tDBInput_3.getLong(24);
					} else {
				
						row5.REF_CUSTOMER = null;
					}
		                    }
							if(colQtyInRs_tDBInput_3 < 25) {
								row5.PAYMENT_STATUS = null;
							} else {
	                         		
        	row5.PAYMENT_STATUS = routines.system.JDBCUtil.getString(rs_tDBInput_3, 25, false);
		                    }
							if(colQtyInRs_tDBInput_3 < 26) {
								row5.REF_ORG_DEPARTMENT = null;
							} else {
		                          
					if(rs_tDBInput_3.getObject(26) != null) {
						row5.REF_ORG_DEPARTMENT = rs_tDBInput_3.getLong(26);
					} else {
				
						row5.REF_ORG_DEPARTMENT = null;
					}
		                    }
							if(colQtyInRs_tDBInput_3 < 27) {
								row5.REF_CLIENT = 0;
							} else {
		                          
					if(rs_tDBInput_3.getObject(27) != null) {
						row5.REF_CLIENT = rs_tDBInput_3.getLong(27);
					} else {
				
 	                	throw new RuntimeException("Null value in non-Nullable column");
					}
		                    }
							if(colQtyInRs_tDBInput_3 < 28) {
								row5.VERSION = 0;
							} else {
		                          
					if(rs_tDBInput_3.getObject(28) != null) {
						row5.VERSION = rs_tDBInput_3.getLong(28);
					} else {
				
 	                	throw new RuntimeException("Null value in non-Nullable column");
					}
		                    }
							if(colQtyInRs_tDBInput_3 < 29) {
								row5.CREATED_BY = 0;
							} else {
		                          
					if(rs_tDBInput_3.getObject(29) != null) {
						row5.CREATED_BY = rs_tDBInput_3.getLong(29);
					} else {
				
 	                	throw new RuntimeException("Null value in non-Nullable column");
					}
		                    }
							if(colQtyInRs_tDBInput_3 < 30) {
								row5.UPDATED_BY = 0;
							} else {
		                          
					if(rs_tDBInput_3.getObject(30) != null) {
						row5.UPDATED_BY = rs_tDBInput_3.getLong(30);
					} else {
				
 	                	throw new RuntimeException("Null value in non-Nullable column");
					}
		                    }
							if(colQtyInRs_tDBInput_3 < 31) {
								row5.CREATED_DATE = null;
							} else {
										
			row5.CREATED_DATE = routines.system.JDBCUtil.getDate(rs_tDBInput_3, 31);
		                    }
							if(colQtyInRs_tDBInput_3 < 32) {
								row5.UPDATED_DATE = null;
							} else {
										
			row5.UPDATED_DATE = routines.system.JDBCUtil.getDate(rs_tDBInput_3, 32);
		                    }
							if(colQtyInRs_tDBInput_3 < 33) {
								row5.BENEFICIARY_BANK_NAME = null;
							} else {
	                         		
        	row5.BENEFICIARY_BANK_NAME = routines.system.JDBCUtil.getString(rs_tDBInput_3, 33, false);
		                    }
							if(colQtyInRs_tDBInput_3 < 34) {
								row5.BENEFICIARY_BANK_ACCOUNT_DESC = null;
							} else {
	                         		
        	row5.BENEFICIARY_BANK_ACCOUNT_DESC = routines.system.JDBCUtil.getString(rs_tDBInput_3, 34, false);
		                    }
							if(colQtyInRs_tDBInput_3 < 35) {
								row5.REF_WARRANT_LINE = null;
							} else {
		                          
					if(rs_tDBInput_3.getObject(35) != null) {
						row5.REF_WARRANT_LINE = rs_tDBInput_3.getLong(35);
					} else {
				
						row5.REF_WARRANT_LINE = null;
					}
		                    }
							if(colQtyInRs_tDBInput_3 < 36) {
								row5.IPPIS_EMPLOYEE_ID = null;
							} else {
	                         		
        	row5.IPPIS_EMPLOYEE_ID = routines.system.JDBCUtil.getString(rs_tDBInput_3, 36, false);
		                    }
							if(colQtyInRs_tDBInput_3 < 37) {
								row5.IPPIS_BENEFICIARY_CODE = null;
							} else {
	                         		
        	row5.IPPIS_BENEFICIARY_CODE = routines.system.JDBCUtil.getString(rs_tDBInput_3, 37, false);
		                    }
							if(colQtyInRs_tDBInput_3 < 38) {
								row5.IPPIS_PAYMENT_TYPE_CODE = null;
							} else {
	                         		
        	row5.IPPIS_PAYMENT_TYPE_CODE = routines.system.JDBCUtil.getString(rs_tDBInput_3, 38, false);
		                    }
							if(colQtyInRs_tDBInput_3 < 39) {
								row5.LEDGER_STATUS = null;
							} else {
	                         		
        	row5.LEDGER_STATUS = routines.system.JDBCUtil.getString(rs_tDBInput_3, 39, false);
		                    }
							if(colQtyInRs_tDBInput_3 < 40) {
								row5.IN_SETTLEMENT = 0;
							} else {
		                          
					if(rs_tDBInput_3.getObject(40) != null) {
						row5.IN_SETTLEMENT = rs_tDBInput_3.getLong(40);
					} else {
				
 	                	throw new RuntimeException("Null value in non-Nullable column");
					}
		                    }
							if(colQtyInRs_tDBInput_3 < 41) {
								row5.REF_AUTHORIZING_ORG = null;
							} else {
		                          
					if(rs_tDBInput_3.getObject(41) != null) {
						row5.REF_AUTHORIZING_ORG = rs_tDBInput_3.getLong(41);
					} else {
				
						row5.REF_AUTHORIZING_ORG = null;
					}
		                    }
							if(colQtyInRs_tDBInput_3 < 42) {
								row5.REF_FUND_MDA = null;
							} else {
		                          
					if(rs_tDBInput_3.getObject(42) != null) {
						row5.REF_FUND_MDA = rs_tDBInput_3.getLong(42);
					} else {
				
						row5.REF_FUND_MDA = null;
					}
		                    }
							if(colQtyInRs_tDBInput_3 < 43) {
								row5.PAYMENT_BATCH_NUMBER = 0;
							} else {
		                          
					if(rs_tDBInput_3.getObject(43) != null) {
						row5.PAYMENT_BATCH_NUMBER = rs_tDBInput_3.getLong(43);
					} else {
				
 	                	throw new RuntimeException("Null value in non-Nullable column");
					}
		                    }
							if(colQtyInRs_tDBInput_3 < 44) {
								row5.REF_ORIGINAL_PAYMENT_ORDER = null;
							} else {
		                          
					if(rs_tDBInput_3.getObject(44) != null) {
						row5.REF_ORIGINAL_PAYMENT_ORDER = rs_tDBInput_3.getLong(44);
					} else {
				
						row5.REF_ORIGINAL_PAYMENT_ORDER = null;
					}
		                    }
							if(colQtyInRs_tDBInput_3 < 45) {
								row5.REF_CREDITED_PAYMENT_ORDER = null;
							} else {
		                          
					if(rs_tDBInput_3.getObject(45) != null) {
						row5.REF_CREDITED_PAYMENT_ORDER = rs_tDBInput_3.getLong(45);
					} else {
				
						row5.REF_CREDITED_PAYMENT_ORDER = null;
					}
		                    }
							if(colQtyInRs_tDBInput_3 < 46) {
								row5.RESENDING_STATUS = null;
							} else {
	                         		
        	row5.RESENDING_STATUS = routines.system.JDBCUtil.getString(rs_tDBInput_3, 46, false);
		                    }
							if(colQtyInRs_tDBInput_3 < 47) {
								row5.IS_REFUND = null;
							} else {
		                          
					if(rs_tDBInput_3.getObject(47) != null) {
						row5.IS_REFUND = rs_tDBInput_3.getLong(47);
					} else {
				
						row5.IS_REFUND = null;
					}
		                    }
							if(colQtyInRs_tDBInput_3 < 48) {
								row5.REFUND_REASON = null;
							} else {
	                         		
        	row5.REFUND_REASON = routines.system.JDBCUtil.getString(rs_tDBInput_3, 48, false);
		                    }
							if(colQtyInRs_tDBInput_3 < 49) {
								row5.REFUND_ORIGINATING_DOCUMENT = null;
							} else {
	                         		
        	row5.REFUND_ORIGINATING_DOCUMENT = routines.system.JDBCUtil.getString(rs_tDBInput_3, 49, false);
		                    }
					




 



/**
 * [tDBInput_3 begin ] stop
 */
	
	/**
	 * [tDBInput_3 main ] start
	 */

	

	
	
	currentComponent="tDBInput_3";

	

 


	tos_count_tDBInput_3++;

/**
 * [tDBInput_3 main ] stop
 */
	
	/**
	 * [tDBInput_3 process_data_begin ] start
	 */

	

	
	
	currentComponent="tDBInput_3";

	

 



/**
 * [tDBInput_3 process_data_begin ] stop
 */

	
	/**
	 * [tMap_1 main ] start
	 */

	

	
	
	currentComponent="tMap_1";

	

			//row5
			//row5


			
				if(execStat){
					runStat.updateStatOnConnection("row5"+iterateId,1, 1);
				} 
			

		

			


		
		
		boolean hasCasePrimitiveKeyWithNull_tMap_1 = false;
		
        // ###############################
        // # Input tables (lookups)
		  boolean rejectedInnerJoin_tMap_1 = false;
		  boolean mainRowRejected_tMap_1 = false;
            				    								  
		
			try {
		// ###############################
        { // start of Var scope
        
	        // ###############################
        	// # Vars tables
        
Var__tMap_1__Struct Var = Var__tMap_1;// ###############################
        // ###############################
        // # Output tables

outputTable = null;


// # Output table : 'outputTable'
outputTable_tmp.ID = row5.ID ;
outputTable_tmp.REF_PAYMENT_BATCH = row5.REF_PAYMENT_BATCH ;
outputTable_tmp.LINE_NO = row5.LINE_NO ;
outputTable_tmp.REF_ORGANIZATION = row5.REF_ORGANIZATION ;
outputTable_tmp.BENEFICIARY_TYPE = row5.BENEFICIARY_TYPE ;
outputTable_tmp.BENEFICIARY_TIN = row5.BENEFICIARY_TIN ;
outputTable_tmp.BENEFICIARY_NAME = row5.BENEFICIARY_NAME ;
outputTable_tmp.BENEFICIARY_BANK_CODE = row5.BENEFICIARY_BANK_CODE ;
outputTable_tmp.BENEFICIARY_BANK_ACCOUNT = row5.BENEFICIARY_BANK_ACCOUNT ;
outputTable_tmp.BENEFICIARY_EMAIL = row5.BENEFICIARY_EMAIL ;
outputTable_tmp.BENEFICIARY_PHONE = row5.BENEFICIARY_PHONE ;
outputTable_tmp.BENEFICIARY_ADDRESS = row5.BENEFICIARY_ADDRESS ;
outputTable_tmp.REF_CURRENCY = row5.REF_CURRENCY ;
outputTable_tmp.CURRENCY_RATE = row5.CURRENCY_RATE ;
outputTable_tmp.PAYMENT_REFERENCE_NO = row5.PAYMENT_REFERENCE_NO ;
outputTable_tmp.PAYMENT_AMOUNT = row5.PAYMENT_AMOUNT ;
outputTable_tmp.PAYMENT_AMOUNT_BASE = row5.PAYMENT_AMOUNT_BASE ;
outputTable_tmp.DESCRIPTION = row5.DESCRIPTION ;
outputTable_tmp.SUPPLIER_INVOICE_NO = row5.SUPPLIER_INVOICE_NO ;
outputTable_tmp.SUPPLIER_INVOICE_DATE = row5.SUPPLIER_INVOICE_DATE ;
outputTable_tmp.SUPPLIER_INVOICE_DUEDATE = row5.SUPPLIER_INVOICE_DUEDATE ;
outputTable_tmp.REF_SUPPLIER = row5.REF_SUPPLIER ;
outputTable_tmp.REF_EMPLOYEE = row5.REF_EMPLOYEE ;
outputTable_tmp.REF_CUSTOMER = row5.REF_CUSTOMER ;
outputTable_tmp.PAYMENT_STATUS = row5.PAYMENT_STATUS ;
outputTable_tmp.REF_ORG_DEPARTMENT = row5.REF_ORG_DEPARTMENT ;
outputTable_tmp.REF_CLIENT = row5.REF_CLIENT ;
outputTable_tmp.VERSION = row5.VERSION ;
outputTable_tmp.CREATED_BY = row5.CREATED_BY ;
outputTable_tmp.UPDATED_BY = row5.UPDATED_BY ;
outputTable_tmp.CREATED_DATE = row5.CREATED_DATE ;
outputTable_tmp.UPDATED_DATE = row5.UPDATED_DATE ;
outputTable_tmp.BENEFICIARY_BANK_NAME = row5.BENEFICIARY_BANK_NAME ;
outputTable_tmp.BENEFICIARY_BANK_ACCOUNT_DESC = row5.BENEFICIARY_BANK_ACCOUNT_DESC ;
outputTable_tmp.REF_WARRANT_LINE = row5.REF_WARRANT_LINE ;
outputTable_tmp.IPPIS_EMPLOYEE_ID = row5.IPPIS_EMPLOYEE_ID ;
outputTable_tmp.IPPIS_BENEFICIARY_CODE = row5.IPPIS_BENEFICIARY_CODE ;
outputTable_tmp.IPPIS_PAYMENT_TYPE_CODE = row5.IPPIS_PAYMENT_TYPE_CODE ;
outputTable_tmp.LEDGER_STATUS = row5.LEDGER_STATUS ;
outputTable_tmp.IN_SETTLEMENT = row5.IN_SETTLEMENT ;
outputTable_tmp.REF_AUTHORIZING_ORG = row5.REF_AUTHORIZING_ORG ;
outputTable_tmp.REF_FUND_MDA = row5.REF_FUND_MDA ;
outputTable_tmp.PAYMENT_BATCH_NUMBER = row5.PAYMENT_BATCH_NUMBER ;
outputTable_tmp.REF_ORIGINAL_PAYMENT_ORDER = row5.REF_ORIGINAL_PAYMENT_ORDER ;
outputTable_tmp.REF_CREDITED_PAYMENT_ORDER = row5.REF_CREDITED_PAYMENT_ORDER ;
outputTable_tmp.RESENDING_STATUS = row5.RESENDING_STATUS ;
outputTable_tmp.IS_REFUND = row5.IS_REFUND ;
outputTable_tmp.REFUND_REASON = row5.REFUND_REASON ;
outputTable_tmp.REFUND_ORIGINATING_DOCUMENT = row5.REFUND_ORIGINATING_DOCUMENT ;
outputTable_tmp.etl_date = TalendDate.parseDate("yyyy-MM-dd HH:mm:ss",TalendDate.getDate("yyyy-MM-DD hh:mm:ss")) ;
outputTable = outputTable_tmp;
// ###############################

} // end of Var scope

rejectedInnerJoin_tMap_1 = false;


		} catch (java.lang.Exception e) {
			//if anohter java.lang.Exception when processing an java.lang.Exception
    		
			try{//EE
Var__tMap_1__Struct Var = Var__tMap_1;
			
			
    					
                		outputTable = null;
         } catch (java.lang.Exception ee) {//EE
        		
				ee.printStackTrace();					
                		outputTable = null;
		}//EE  			
	}//end catch








 


	tos_count_tMap_1++;

/**
 * [tMap_1 main ] stop
 */
	
	/**
	 * [tMap_1 process_data_begin ] start
	 */

	

	
	
	currentComponent="tMap_1";

	

 



/**
 * [tMap_1 process_data_begin ] stop
 */
// Start of branch "outputTable"
if(outputTable != null) { 



	
	/**
	 * [tDBOutputBulk_1 main ] start
	 */

	

	
	
	currentComponent="tDBOutputBulk_1";

	

			//outputTable
			//outputTable


			
				if(execStat){
					runStat.updateStatOnConnection("outputTable"+iterateId,1, 1);
				} 
			

		

 
    						outtDBOutputBulk_1.write(
    				
								String.valueOf(outputTable.ID)
							);					
							outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());
    						outtDBOutputBulk_1.write(
    				
								String.valueOf(outputTable.REF_PAYMENT_BATCH)
							);					
							outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());
    						outtDBOutputBulk_1.write(
    				
								String.valueOf(outputTable.LINE_NO)
							);					
							outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());
    						outtDBOutputBulk_1.write(
    				
								String.valueOf(outputTable.REF_ORGANIZATION)
							);					
							outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());   				
	    				if(outputTable.BENEFICIARY_TYPE != null) {
    				
    						outtDBOutputBulk_1.write(
    				
								outputTable.BENEFICIARY_TYPE
							);
	    				} 
										
							outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());   				
	    				if(outputTable.BENEFICIARY_TIN != null) {
    				
    						outtDBOutputBulk_1.write(
    				
								outputTable.BENEFICIARY_TIN
							);
	    				} 
										
							outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());   				
	    				if(outputTable.BENEFICIARY_NAME != null) {
    				
    						outtDBOutputBulk_1.write(
    				
								outputTable.BENEFICIARY_NAME
							);
	    				} 
										
							outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());   				
	    				if(outputTable.BENEFICIARY_BANK_CODE != null) {
    				
    						outtDBOutputBulk_1.write(
    				
								outputTable.BENEFICIARY_BANK_CODE
							);
	    				} 
										
							outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());   				
	    				if(outputTable.BENEFICIARY_BANK_ACCOUNT != null) {
    				
    						outtDBOutputBulk_1.write(
    				
								outputTable.BENEFICIARY_BANK_ACCOUNT
							);
	    				} 
										
							outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());   				
	    				if(outputTable.BENEFICIARY_EMAIL != null) {
    				
    						outtDBOutputBulk_1.write(
    				
								outputTable.BENEFICIARY_EMAIL
							);
	    				} 
										
							outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());   				
	    				if(outputTable.BENEFICIARY_PHONE != null) {
    				
    						outtDBOutputBulk_1.write(
    				
								outputTable.BENEFICIARY_PHONE
							);
	    				} 
										
							outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());   				
	    				if(outputTable.BENEFICIARY_ADDRESS != null) {
    				
    						outtDBOutputBulk_1.write(
    				
								outputTable.BENEFICIARY_ADDRESS
							);
	    				} 
										
							outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());
    						outtDBOutputBulk_1.write(
    				
								String.valueOf(outputTable.REF_CURRENCY)
							);					
							outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());   				
	    				if(outputTable.CURRENCY_RATE != null) {
    				
    						outtDBOutputBulk_1.write(
    				
								String.valueOf(outputTable.CURRENCY_RATE)
							);
	    				} 
										
							outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());   				
	    				if(outputTable.PAYMENT_REFERENCE_NO != null) {
    				
    						outtDBOutputBulk_1.write(
    				
								outputTable.PAYMENT_REFERENCE_NO
							);
	    				} 
										
							outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());   				
	    				if(outputTable.PAYMENT_AMOUNT != null) {
    				
    						outtDBOutputBulk_1.write(
    				
								String.valueOf(outputTable.PAYMENT_AMOUNT)
							);
	    				} 
										
							outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());   				
	    				if(outputTable.PAYMENT_AMOUNT_BASE != null) {
    				
    						outtDBOutputBulk_1.write(
    				
								String.valueOf(outputTable.PAYMENT_AMOUNT_BASE)
							);
	    				} 
										
							outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());   				
	    				if(outputTable.DESCRIPTION != null) {
    				
    						outtDBOutputBulk_1.write(
    				
								outputTable.DESCRIPTION
							);
	    				} 
										
							outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());   				
	    				if(outputTable.SUPPLIER_INVOICE_NO != null) {
    				
    						outtDBOutputBulk_1.write(
    				
								outputTable.SUPPLIER_INVOICE_NO
							);
	    				} 
										
							outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());   				
	    				if(outputTable.SUPPLIER_INVOICE_DATE != null) {
    				
    						outtDBOutputBulk_1.write(
    				
								FormatterUtils.format_Date(outputTable.SUPPLIER_INVOICE_DATE, "yyyy-MM-dd HH:mm:ss")
							);
	    				} 
										
							outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());   				
	    				if(outputTable.SUPPLIER_INVOICE_DUEDATE != null) {
    				
    						outtDBOutputBulk_1.write(
    				
								FormatterUtils.format_Date(outputTable.SUPPLIER_INVOICE_DUEDATE, "yyyy-MM-dd HH:mm:ss")
							);
	    				} 
										
							outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());   				
	    				if(outputTable.REF_SUPPLIER != null) {
    				
    						outtDBOutputBulk_1.write(
    				
								String.valueOf(outputTable.REF_SUPPLIER)
							);
	    				} 
										
							outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());   				
	    				if(outputTable.REF_EMPLOYEE != null) {
    				
    						outtDBOutputBulk_1.write(
    				
								String.valueOf(outputTable.REF_EMPLOYEE)
							);
	    				} 
										
							outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());   				
	    				if(outputTable.REF_CUSTOMER != null) {
    				
    						outtDBOutputBulk_1.write(
    				
								String.valueOf(outputTable.REF_CUSTOMER)
							);
	    				} 
										
							outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());   				
	    				if(outputTable.PAYMENT_STATUS != null) {
    				
    						outtDBOutputBulk_1.write(
    				
								outputTable.PAYMENT_STATUS
							);
	    				} 
										
							outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());   				
	    				if(outputTable.REF_ORG_DEPARTMENT != null) {
    				
    						outtDBOutputBulk_1.write(
    				
								String.valueOf(outputTable.REF_ORG_DEPARTMENT)
							);
	    				} 
										
							outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());
    						outtDBOutputBulk_1.write(
    				
								String.valueOf(outputTable.REF_CLIENT)
							);					
							outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());
    						outtDBOutputBulk_1.write(
    				
								String.valueOf(outputTable.VERSION)
							);					
							outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());
    						outtDBOutputBulk_1.write(
    				
								String.valueOf(outputTable.CREATED_BY)
							);					
							outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());
    						outtDBOutputBulk_1.write(
    				
								String.valueOf(outputTable.UPDATED_BY)
							);					
							outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());   				
	    				if(outputTable.CREATED_DATE != null) {
    				
    						outtDBOutputBulk_1.write(
    				
								FormatterUtils.format_Date(outputTable.CREATED_DATE, "yyyy-MM-dd HH:mm:ss")
							);
	    				} 
										
							outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());   				
	    				if(outputTable.UPDATED_DATE != null) {
    				
    						outtDBOutputBulk_1.write(
    				
								FormatterUtils.format_Date(outputTable.UPDATED_DATE, "yyyy-MM-dd HH:mm:ss")
							);
	    				} 
										
							outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());   				
	    				if(outputTable.BENEFICIARY_BANK_NAME != null) {
    				
    						outtDBOutputBulk_1.write(
    				
								outputTable.BENEFICIARY_BANK_NAME
							);
	    				} 
										
							outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());   				
	    				if(outputTable.BENEFICIARY_BANK_ACCOUNT_DESC != null) {
    				
    						outtDBOutputBulk_1.write(
    				
								outputTable.BENEFICIARY_BANK_ACCOUNT_DESC
							);
	    				} 
										
							outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());   				
	    				if(outputTable.REF_WARRANT_LINE != null) {
    				
    						outtDBOutputBulk_1.write(
    				
								String.valueOf(outputTable.REF_WARRANT_LINE)
							);
	    				} 
										
							outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());   				
	    				if(outputTable.IPPIS_EMPLOYEE_ID != null) {
    				
    						outtDBOutputBulk_1.write(
    				
								outputTable.IPPIS_EMPLOYEE_ID
							);
	    				} 
										
							outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());   				
	    				if(outputTable.IPPIS_BENEFICIARY_CODE != null) {
    				
    						outtDBOutputBulk_1.write(
    				
								outputTable.IPPIS_BENEFICIARY_CODE
							);
	    				} 
										
							outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());   				
	    				if(outputTable.IPPIS_PAYMENT_TYPE_CODE != null) {
    				
    						outtDBOutputBulk_1.write(
    				
								outputTable.IPPIS_PAYMENT_TYPE_CODE
							);
	    				} 
										
							outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());   				
	    				if(outputTable.LEDGER_STATUS != null) {
    				
    						outtDBOutputBulk_1.write(
    				
								outputTable.LEDGER_STATUS
							);
	    				} 
										
							outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());
    						outtDBOutputBulk_1.write(
    				
								String.valueOf(outputTable.IN_SETTLEMENT)
							);					
							outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());   				
	    				if(outputTable.REF_AUTHORIZING_ORG != null) {
    				
    						outtDBOutputBulk_1.write(
    				
								String.valueOf(outputTable.REF_AUTHORIZING_ORG)
							);
	    				} 
										
							outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());   				
	    				if(outputTable.REF_FUND_MDA != null) {
    				
    						outtDBOutputBulk_1.write(
    				
								String.valueOf(outputTable.REF_FUND_MDA)
							);
	    				} 
										
							outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());
    						outtDBOutputBulk_1.write(
    				
								String.valueOf(outputTable.PAYMENT_BATCH_NUMBER)
							);					
							outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());   				
	    				if(outputTable.REF_ORIGINAL_PAYMENT_ORDER != null) {
    				
    						outtDBOutputBulk_1.write(
    				
								String.valueOf(outputTable.REF_ORIGINAL_PAYMENT_ORDER)
							);
	    				} 
										
							outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());   				
	    				if(outputTable.REF_CREDITED_PAYMENT_ORDER != null) {
    				
    						outtDBOutputBulk_1.write(
    				
								String.valueOf(outputTable.REF_CREDITED_PAYMENT_ORDER)
							);
	    				} 
										
							outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());   				
	    				if(outputTable.RESENDING_STATUS != null) {
    				
    						outtDBOutputBulk_1.write(
    				
								outputTable.RESENDING_STATUS
							);
	    				} 
										
							outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());   				
	    				if(outputTable.IS_REFUND != null) {
    				
    						outtDBOutputBulk_1.write(
    				
								String.valueOf(outputTable.IS_REFUND)
							);
	    				} 
										
							outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());   				
	    				if(outputTable.REFUND_REASON != null) {
    				
    						outtDBOutputBulk_1.write(
    				
								outputTable.REFUND_REASON
							);
	    				} 
										
							outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());   				
	    				if(outputTable.REFUND_ORIGINATING_DOCUMENT != null) {
    				
    						outtDBOutputBulk_1.write(
    				
								outputTable.REFUND_ORIGINATING_DOCUMENT
							);
	    				} 
										
							outtDBOutputBulk_1.write(OUT_DELIM_tDBOutputBulk_1.toString());   				
	    				if(outputTable.etl_date != null) {
    				
    						outtDBOutputBulk_1.write(
    				
								FormatterUtils.format_Date(outputTable.etl_date, "yyyy-MM-dd HH:mm:ss")
							);
	    				} 
					
    			outtDBOutputBulk_1.write(OUT_DELIM_ROWSEP_tDBOutputBulk_1);
    			nb_line_tDBOutputBulk_1++;
    			

 


	tos_count_tDBOutputBulk_1++;

/**
 * [tDBOutputBulk_1 main ] stop
 */
	
	/**
	 * [tDBOutputBulk_1 process_data_begin ] start
	 */

	

	
	
	currentComponent="tDBOutputBulk_1";

	

 



/**
 * [tDBOutputBulk_1 process_data_begin ] stop
 */
	
	/**
	 * [tDBOutputBulk_1 process_data_end ] start
	 */

	

	
	
	currentComponent="tDBOutputBulk_1";

	

 



/**
 * [tDBOutputBulk_1 process_data_end ] stop
 */

} // End of branch "outputTable"




	
	/**
	 * [tMap_1 process_data_end ] start
	 */

	

	
	
	currentComponent="tMap_1";

	

 



/**
 * [tMap_1 process_data_end ] stop
 */



	
	/**
	 * [tDBInput_3 process_data_end ] start
	 */

	

	
	
	currentComponent="tDBInput_3";

	

 



/**
 * [tDBInput_3 process_data_end ] stop
 */
	
	/**
	 * [tDBInput_3 end ] start
	 */

	

	
	
	currentComponent="tDBInput_3";

	

}
}finally{
stmt_tDBInput_3.close();

	if(conn_tDBInput_3 != null && !conn_tDBInput_3.isClosed()) {
	
			conn_tDBInput_3.close();
			
	}
	
}

globalMap.put("tDBInput_3_NB_LINE",nb_line_tDBInput_3);
 

ok_Hash.put("tDBInput_3", true);
end_Hash.put("tDBInput_3", System.currentTimeMillis());

talendStats_STATS.addMessage("end","tDBInput_3", end_Hash.get("tDBInput_3")-start_Hash.get("tDBInput_3"));
talendStats_STATSProcess(globalMap);



/**
 * [tDBInput_3 end ] stop
 */

	
	/**
	 * [tMap_1 end ] start
	 */

	

	
	
	currentComponent="tMap_1";

	


// ###############################
// # Lookup hashes releasing
// ###############################      





			if(execStat){
				if(resourceMap.get("inIterateVComp") == null || !((Boolean)resourceMap.get("inIterateVComp"))){
			 		runStat.updateStatOnConnection("row5"+iterateId,2, 0); 
			 	}
			}
		
 

ok_Hash.put("tMap_1", true);
end_Hash.put("tMap_1", System.currentTimeMillis());

talendStats_STATS.addMessage("end","tMap_1", end_Hash.get("tMap_1")-start_Hash.get("tMap_1"));
talendStats_STATSProcess(globalMap);



/**
 * [tMap_1 end ] stop
 */

	
	/**
	 * [tDBOutputBulk_1 end ] start
	 */

	

	
	
	currentComponent="tDBOutputBulk_1";

	

	outtDBOutputBulk_1.close();
	resourceMap.put("finish_tDBOutputBulk_1", true); 
	globalMap.put("tDBOutputBulk_1_NB_LINE",nb_line_tDBOutputBulk_1);

	

			if(execStat){
				if(resourceMap.get("inIterateVComp") == null || !((Boolean)resourceMap.get("inIterateVComp"))){
			 		runStat.updateStatOnConnection("outputTable"+iterateId,2, 0); 
			 	}
			}
		
 

ok_Hash.put("tDBOutputBulk_1", true);
end_Hash.put("tDBOutputBulk_1", System.currentTimeMillis());

talendStats_STATS.addMessage("end","tDBOutputBulk_1", end_Hash.get("tDBOutputBulk_1")-start_Hash.get("tDBOutputBulk_1"));
talendStats_STATSProcess(globalMap);



/**
 * [tDBOutputBulk_1 end ] stop
 */






				}//end the resume

				
				    			if(resumeEntryMethodName == null || globalResumeTicket){
				    				resumeUtil.addLog("CHECKPOINT", "CONNECTION:SUBJOB_OK:tDBInput_3:OnSubjobOk", "", Thread.currentThread().getId() + "", "", "", "", "", "");
								}	    				    			
					    	
								if(execStat){    	
									runStat.updateStatOnConnection("OnSubjobOk3", 0, "ok");
								} 
							
							tFileInputDelimited_1Process(globalMap); 
						



	
			}catch(java.lang.Exception e){	
				
				TalendException te = new TalendException(e, currentComponent, globalMap);
				
				throw te;
			}catch(java.lang.Error error){	
				
					runStat.stopThreadStat();
				
				throw error;
			}finally{
				
				try{
					
	
	/**
	 * [tDBInput_3 finally ] start
	 */

	

	
	
	currentComponent="tDBInput_3";

	

 



/**
 * [tDBInput_3 finally ] stop
 */

	
	/**
	 * [tMap_1 finally ] start
	 */

	

	
	
	currentComponent="tMap_1";

	

 



/**
 * [tMap_1 finally ] stop
 */

	
	/**
	 * [tDBOutputBulk_1 finally ] start
	 */

	

	
	
	currentComponent="tDBOutputBulk_1";

	
	if(resourceMap.get("finish_tDBOutputBulk_1") == null){
		if(resourceMap.get("outtDBOutputBulk_1") != null){
			((java.io.BufferedWriter)resourceMap.get("outtDBOutputBulk_1")).close();
		}
	}

 



/**
 * [tDBOutputBulk_1 finally ] stop
 */






				}catch(java.lang.Exception e){	
					//ignore
				}catch(java.lang.Error error){
					//ignore
				}
				resourceMap = null;
			}
		

		globalMap.put("tDBInput_3_SUBPROCESS_STATE", 1);
	}
	


public static class ltStruct implements routines.system.IPersistableRow<ltStruct> {
    final static byte[] commonByteArrayLock_GIFMIS_payment_order_bulk_2 = new byte[0];
    static byte[] commonByteArray_GIFMIS_payment_order_bulk_2 = new byte[0];
	protected static final int DEFAULT_HASHCODE = 1;
    protected static final int PRIME = 31;
    protected int hashCode = DEFAULT_HASHCODE;
    public boolean hashCodeDirty = true;

    public String loopKey;



	
			    public int ID;

				public int getID () {
					return this.ID;
				}
				
			    public int REF_PAYMENT_BATCH;

				public int getREF_PAYMENT_BATCH () {
					return this.REF_PAYMENT_BATCH;
				}
				
			    public int LINE_NO;

				public int getLINE_NO () {
					return this.LINE_NO;
				}
				
			    public int REF_ORGANIZATION;

				public int getREF_ORGANIZATION () {
					return this.REF_ORGANIZATION;
				}
				
			    public String BENEFICIARY_TYPE;

				public String getBENEFICIARY_TYPE () {
					return this.BENEFICIARY_TYPE;
				}
				
			    public String BENEFICIARY_TIN;

				public String getBENEFICIARY_TIN () {
					return this.BENEFICIARY_TIN;
				}
				
			    public String BENEFICIARY_NAME;

				public String getBENEFICIARY_NAME () {
					return this.BENEFICIARY_NAME;
				}
				
			    public String BENEFICIARY_BANK_CODE;

				public String getBENEFICIARY_BANK_CODE () {
					return this.BENEFICIARY_BANK_CODE;
				}
				
			    public String BENEFICIARY_BANK_ACCOUNT;

				public String getBENEFICIARY_BANK_ACCOUNT () {
					return this.BENEFICIARY_BANK_ACCOUNT;
				}
				
			    public String BENEFICIARY_EMAIL;

				public String getBENEFICIARY_EMAIL () {
					return this.BENEFICIARY_EMAIL;
				}
				
			    public String BENEFICIARY_PHONE;

				public String getBENEFICIARY_PHONE () {
					return this.BENEFICIARY_PHONE;
				}
				
			    public String BENEFICIARY_ADDRESS;

				public String getBENEFICIARY_ADDRESS () {
					return this.BENEFICIARY_ADDRESS;
				}
				
			    public int REF_CURRENCY;

				public int getREF_CURRENCY () {
					return this.REF_CURRENCY;
				}
				
			    public BigDecimal CURRENCY_RATE;

				public BigDecimal getCURRENCY_RATE () {
					return this.CURRENCY_RATE;
				}
				
			    public String PAYMENT_REFERENCE_NO;

				public String getPAYMENT_REFERENCE_NO () {
					return this.PAYMENT_REFERENCE_NO;
				}
				
			    public BigDecimal PAYMENT_AMOUNT;

				public BigDecimal getPAYMENT_AMOUNT () {
					return this.PAYMENT_AMOUNT;
				}
				
			    public BigDecimal PAYMENT_AMOUNT_BASE;

				public BigDecimal getPAYMENT_AMOUNT_BASE () {
					return this.PAYMENT_AMOUNT_BASE;
				}
				
			    public String DESCRIPTION;

				public String getDESCRIPTION () {
					return this.DESCRIPTION;
				}
				
			    public String SUPPLIER_INVOICE_NO;

				public String getSUPPLIER_INVOICE_NO () {
					return this.SUPPLIER_INVOICE_NO;
				}
				
			    public java.util.Date SUPPLIER_INVOICE_DATE;

				public java.util.Date getSUPPLIER_INVOICE_DATE () {
					return this.SUPPLIER_INVOICE_DATE;
				}
				
			    public java.util.Date SUPPLIER_INVOICE_DUEDATE;

				public java.util.Date getSUPPLIER_INVOICE_DUEDATE () {
					return this.SUPPLIER_INVOICE_DUEDATE;
				}
				
			    public Integer REF_SUPPLIER;

				public Integer getREF_SUPPLIER () {
					return this.REF_SUPPLIER;
				}
				
			    public Integer REF_EMPLOYEE;

				public Integer getREF_EMPLOYEE () {
					return this.REF_EMPLOYEE;
				}
				
			    public Integer REF_CUSTOMER;

				public Integer getREF_CUSTOMER () {
					return this.REF_CUSTOMER;
				}
				
			    public String PAYMENT_STATUS;

				public String getPAYMENT_STATUS () {
					return this.PAYMENT_STATUS;
				}
				
			    public Integer REF_ORG_DEPARTMENT;

				public Integer getREF_ORG_DEPARTMENT () {
					return this.REF_ORG_DEPARTMENT;
				}
				
			    public int REF_CLIENT;

				public int getREF_CLIENT () {
					return this.REF_CLIENT;
				}
				
			    public int VERSION;

				public int getVERSION () {
					return this.VERSION;
				}
				
			    public int CREATED_BY;

				public int getCREATED_BY () {
					return this.CREATED_BY;
				}
				
			    public int UPDATED_BY;

				public int getUPDATED_BY () {
					return this.UPDATED_BY;
				}
				
			    public java.util.Date CREATED_DATE;

				public java.util.Date getCREATED_DATE () {
					return this.CREATED_DATE;
				}
				
			    public java.util.Date UPDATED_DATE;

				public java.util.Date getUPDATED_DATE () {
					return this.UPDATED_DATE;
				}
				
			    public String BENEFICIARY_BANK_NAME;

				public String getBENEFICIARY_BANK_NAME () {
					return this.BENEFICIARY_BANK_NAME;
				}
				
			    public String BENEFICIARY_BANK_ACCOUNT_DESC;

				public String getBENEFICIARY_BANK_ACCOUNT_DESC () {
					return this.BENEFICIARY_BANK_ACCOUNT_DESC;
				}
				
			    public Integer REF_WARRANT_LINE;

				public Integer getREF_WARRANT_LINE () {
					return this.REF_WARRANT_LINE;
				}
				
			    public String IPPIS_EMPLOYEE_ID;

				public String getIPPIS_EMPLOYEE_ID () {
					return this.IPPIS_EMPLOYEE_ID;
				}
				
			    public String IPPIS_BENEFICIARY_CODE;

				public String getIPPIS_BENEFICIARY_CODE () {
					return this.IPPIS_BENEFICIARY_CODE;
				}
				
			    public String IPPIS_PAYMENT_TYPE_CODE;

				public String getIPPIS_PAYMENT_TYPE_CODE () {
					return this.IPPIS_PAYMENT_TYPE_CODE;
				}
				
			    public String LEDGER_STATUS;

				public String getLEDGER_STATUS () {
					return this.LEDGER_STATUS;
				}
				
			    public int IN_SETTLEMENT;

				public int getIN_SETTLEMENT () {
					return this.IN_SETTLEMENT;
				}
				
			    public Integer REF_AUTHORIZING_ORG;

				public Integer getREF_AUTHORIZING_ORG () {
					return this.REF_AUTHORIZING_ORG;
				}
				
			    public Integer REF_FUND_MDA;

				public Integer getREF_FUND_MDA () {
					return this.REF_FUND_MDA;
				}
				
			    public int PAYMENT_BATCH_NUMBER;

				public int getPAYMENT_BATCH_NUMBER () {
					return this.PAYMENT_BATCH_NUMBER;
				}
				
			    public Integer REF_ORIGINAL_PAYMENT_ORDER;

				public Integer getREF_ORIGINAL_PAYMENT_ORDER () {
					return this.REF_ORIGINAL_PAYMENT_ORDER;
				}
				
			    public Integer REF_CREDITED_PAYMENT_ORDER;

				public Integer getREF_CREDITED_PAYMENT_ORDER () {
					return this.REF_CREDITED_PAYMENT_ORDER;
				}
				
			    public String RESENDING_STATUS;

				public String getRESENDING_STATUS () {
					return this.RESENDING_STATUS;
				}
				
			    public Integer IS_REFUND;

				public Integer getIS_REFUND () {
					return this.IS_REFUND;
				}
				
			    public String REFUND_REASON;

				public String getREFUND_REASON () {
					return this.REFUND_REASON;
				}
				
			    public String REFUND_ORIGINATING_DOCUMENT;

				public String getREFUND_ORIGINATING_DOCUMENT () {
					return this.REFUND_ORIGINATING_DOCUMENT;
				}
				
			    public java.util.Date etl_date;

				public java.util.Date getEtl_date () {
					return this.etl_date;
				}
				


	@Override
	public int hashCode() {
		if (this.hashCodeDirty) {
			final int prime = PRIME;
			int result = DEFAULT_HASHCODE;
	
							result = prime * result + (int) this.ID;
						
    		this.hashCode = result;
    		this.hashCodeDirty = false;
		}
		return this.hashCode;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		final ltStruct other = (ltStruct) obj;
		
						if (this.ID != other.ID)
							return false;
					

		return true;
    }

	public void copyDataTo(ltStruct other) {

		other.ID = this.ID;
	            other.REF_PAYMENT_BATCH = this.REF_PAYMENT_BATCH;
	            other.LINE_NO = this.LINE_NO;
	            other.REF_ORGANIZATION = this.REF_ORGANIZATION;
	            other.BENEFICIARY_TYPE = this.BENEFICIARY_TYPE;
	            other.BENEFICIARY_TIN = this.BENEFICIARY_TIN;
	            other.BENEFICIARY_NAME = this.BENEFICIARY_NAME;
	            other.BENEFICIARY_BANK_CODE = this.BENEFICIARY_BANK_CODE;
	            other.BENEFICIARY_BANK_ACCOUNT = this.BENEFICIARY_BANK_ACCOUNT;
	            other.BENEFICIARY_EMAIL = this.BENEFICIARY_EMAIL;
	            other.BENEFICIARY_PHONE = this.BENEFICIARY_PHONE;
	            other.BENEFICIARY_ADDRESS = this.BENEFICIARY_ADDRESS;
	            other.REF_CURRENCY = this.REF_CURRENCY;
	            other.CURRENCY_RATE = this.CURRENCY_RATE;
	            other.PAYMENT_REFERENCE_NO = this.PAYMENT_REFERENCE_NO;
	            other.PAYMENT_AMOUNT = this.PAYMENT_AMOUNT;
	            other.PAYMENT_AMOUNT_BASE = this.PAYMENT_AMOUNT_BASE;
	            other.DESCRIPTION = this.DESCRIPTION;
	            other.SUPPLIER_INVOICE_NO = this.SUPPLIER_INVOICE_NO;
	            other.SUPPLIER_INVOICE_DATE = this.SUPPLIER_INVOICE_DATE;
	            other.SUPPLIER_INVOICE_DUEDATE = this.SUPPLIER_INVOICE_DUEDATE;
	            other.REF_SUPPLIER = this.REF_SUPPLIER;
	            other.REF_EMPLOYEE = this.REF_EMPLOYEE;
	            other.REF_CUSTOMER = this.REF_CUSTOMER;
	            other.PAYMENT_STATUS = this.PAYMENT_STATUS;
	            other.REF_ORG_DEPARTMENT = this.REF_ORG_DEPARTMENT;
	            other.REF_CLIENT = this.REF_CLIENT;
	            other.VERSION = this.VERSION;
	            other.CREATED_BY = this.CREATED_BY;
	            other.UPDATED_BY = this.UPDATED_BY;
	            other.CREATED_DATE = this.CREATED_DATE;
	            other.UPDATED_DATE = this.UPDATED_DATE;
	            other.BENEFICIARY_BANK_NAME = this.BENEFICIARY_BANK_NAME;
	            other.BENEFICIARY_BANK_ACCOUNT_DESC = this.BENEFICIARY_BANK_ACCOUNT_DESC;
	            other.REF_WARRANT_LINE = this.REF_WARRANT_LINE;
	            other.IPPIS_EMPLOYEE_ID = this.IPPIS_EMPLOYEE_ID;
	            other.IPPIS_BENEFICIARY_CODE = this.IPPIS_BENEFICIARY_CODE;
	            other.IPPIS_PAYMENT_TYPE_CODE = this.IPPIS_PAYMENT_TYPE_CODE;
	            other.LEDGER_STATUS = this.LEDGER_STATUS;
	            other.IN_SETTLEMENT = this.IN_SETTLEMENT;
	            other.REF_AUTHORIZING_ORG = this.REF_AUTHORIZING_ORG;
	            other.REF_FUND_MDA = this.REF_FUND_MDA;
	            other.PAYMENT_BATCH_NUMBER = this.PAYMENT_BATCH_NUMBER;
	            other.REF_ORIGINAL_PAYMENT_ORDER = this.REF_ORIGINAL_PAYMENT_ORDER;
	            other.REF_CREDITED_PAYMENT_ORDER = this.REF_CREDITED_PAYMENT_ORDER;
	            other.RESENDING_STATUS = this.RESENDING_STATUS;
	            other.IS_REFUND = this.IS_REFUND;
	            other.REFUND_REASON = this.REFUND_REASON;
	            other.REFUND_ORIGINATING_DOCUMENT = this.REFUND_ORIGINATING_DOCUMENT;
	            other.etl_date = this.etl_date;
	            
	}

	public void copyKeysDataTo(ltStruct other) {

		other.ID = this.ID;
	            	
	}




	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_GIFMIS_payment_order_bulk_2.length) {
				if(length < 1024 && commonByteArray_GIFMIS_payment_order_bulk_2.length == 0) {
   					commonByteArray_GIFMIS_payment_order_bulk_2 = new byte[1024];
				} else {
   					commonByteArray_GIFMIS_payment_order_bulk_2 = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_GIFMIS_payment_order_bulk_2, 0, length);
			strReturn = new String(commonByteArray_GIFMIS_payment_order_bulk_2, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }

	private java.util.Date readDate(ObjectInputStream dis) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(dis.readLong());
		}
		return dateReturn;
	}

    private void writeDate(java.util.Date date1, ObjectOutputStream dos) throws IOException{
		if(date1 == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeLong(date1.getTime());
    	}
    }
	private Integer readInteger(ObjectInputStream dis) throws IOException{
		Integer intReturn;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			intReturn = null;
		} else {
	    	intReturn = dis.readInt();
		}
		return intReturn;
	}

	private void writeInteger(Integer intNum, ObjectOutputStream dos) throws IOException{
		if(intNum == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeInt(intNum);
    	}
	}

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_GIFMIS_payment_order_bulk_2) {

        	try {

        		int length = 0;
		
			        this.ID = dis.readInt();
					
			        this.REF_PAYMENT_BATCH = dis.readInt();
					
			        this.LINE_NO = dis.readInt();
					
			        this.REF_ORGANIZATION = dis.readInt();
					
					this.BENEFICIARY_TYPE = readString(dis);
					
					this.BENEFICIARY_TIN = readString(dis);
					
					this.BENEFICIARY_NAME = readString(dis);
					
					this.BENEFICIARY_BANK_CODE = readString(dis);
					
					this.BENEFICIARY_BANK_ACCOUNT = readString(dis);
					
					this.BENEFICIARY_EMAIL = readString(dis);
					
					this.BENEFICIARY_PHONE = readString(dis);
					
					this.BENEFICIARY_ADDRESS = readString(dis);
					
			        this.REF_CURRENCY = dis.readInt();
					
						this.CURRENCY_RATE = (BigDecimal) dis.readObject();
					
					this.PAYMENT_REFERENCE_NO = readString(dis);
					
						this.PAYMENT_AMOUNT = (BigDecimal) dis.readObject();
					
						this.PAYMENT_AMOUNT_BASE = (BigDecimal) dis.readObject();
					
					this.DESCRIPTION = readString(dis);
					
					this.SUPPLIER_INVOICE_NO = readString(dis);
					
					this.SUPPLIER_INVOICE_DATE = readDate(dis);
					
					this.SUPPLIER_INVOICE_DUEDATE = readDate(dis);
					
						this.REF_SUPPLIER = readInteger(dis);
					
						this.REF_EMPLOYEE = readInteger(dis);
					
						this.REF_CUSTOMER = readInteger(dis);
					
					this.PAYMENT_STATUS = readString(dis);
					
						this.REF_ORG_DEPARTMENT = readInteger(dis);
					
			        this.REF_CLIENT = dis.readInt();
					
			        this.VERSION = dis.readInt();
					
			        this.CREATED_BY = dis.readInt();
					
			        this.UPDATED_BY = dis.readInt();
					
					this.CREATED_DATE = readDate(dis);
					
					this.UPDATED_DATE = readDate(dis);
					
					this.BENEFICIARY_BANK_NAME = readString(dis);
					
					this.BENEFICIARY_BANK_ACCOUNT_DESC = readString(dis);
					
						this.REF_WARRANT_LINE = readInteger(dis);
					
					this.IPPIS_EMPLOYEE_ID = readString(dis);
					
					this.IPPIS_BENEFICIARY_CODE = readString(dis);
					
					this.IPPIS_PAYMENT_TYPE_CODE = readString(dis);
					
					this.LEDGER_STATUS = readString(dis);
					
			        this.IN_SETTLEMENT = dis.readInt();
					
						this.REF_AUTHORIZING_ORG = readInteger(dis);
					
						this.REF_FUND_MDA = readInteger(dis);
					
			        this.PAYMENT_BATCH_NUMBER = dis.readInt();
					
						this.REF_ORIGINAL_PAYMENT_ORDER = readInteger(dis);
					
						this.REF_CREDITED_PAYMENT_ORDER = readInteger(dis);
					
					this.RESENDING_STATUS = readString(dis);
					
						this.IS_REFUND = readInteger(dis);
					
					this.REFUND_REASON = readString(dis);
					
					this.REFUND_ORIGINATING_DOCUMENT = readString(dis);
					
					this.etl_date = readDate(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		
			} catch(ClassNotFoundException eCNFE) {
				 throw new RuntimeException(eCNFE);
		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// int
				
		            	dos.writeInt(this.ID);
					
					// int
				
		            	dos.writeInt(this.REF_PAYMENT_BATCH);
					
					// int
				
		            	dos.writeInt(this.LINE_NO);
					
					// int
				
		            	dos.writeInt(this.REF_ORGANIZATION);
					
					// String
				
						writeString(this.BENEFICIARY_TYPE,dos);
					
					// String
				
						writeString(this.BENEFICIARY_TIN,dos);
					
					// String
				
						writeString(this.BENEFICIARY_NAME,dos);
					
					// String
				
						writeString(this.BENEFICIARY_BANK_CODE,dos);
					
					// String
				
						writeString(this.BENEFICIARY_BANK_ACCOUNT,dos);
					
					// String
				
						writeString(this.BENEFICIARY_EMAIL,dos);
					
					// String
				
						writeString(this.BENEFICIARY_PHONE,dos);
					
					// String
				
						writeString(this.BENEFICIARY_ADDRESS,dos);
					
					// int
				
		            	dos.writeInt(this.REF_CURRENCY);
					
					// BigDecimal
				
       			    	dos.writeObject(this.CURRENCY_RATE);
					
					// String
				
						writeString(this.PAYMENT_REFERENCE_NO,dos);
					
					// BigDecimal
				
       			    	dos.writeObject(this.PAYMENT_AMOUNT);
					
					// BigDecimal
				
       			    	dos.writeObject(this.PAYMENT_AMOUNT_BASE);
					
					// String
				
						writeString(this.DESCRIPTION,dos);
					
					// String
				
						writeString(this.SUPPLIER_INVOICE_NO,dos);
					
					// java.util.Date
				
						writeDate(this.SUPPLIER_INVOICE_DATE,dos);
					
					// java.util.Date
				
						writeDate(this.SUPPLIER_INVOICE_DUEDATE,dos);
					
					// Integer
				
						writeInteger(this.REF_SUPPLIER,dos);
					
					// Integer
				
						writeInteger(this.REF_EMPLOYEE,dos);
					
					// Integer
				
						writeInteger(this.REF_CUSTOMER,dos);
					
					// String
				
						writeString(this.PAYMENT_STATUS,dos);
					
					// Integer
				
						writeInteger(this.REF_ORG_DEPARTMENT,dos);
					
					// int
				
		            	dos.writeInt(this.REF_CLIENT);
					
					// int
				
		            	dos.writeInt(this.VERSION);
					
					// int
				
		            	dos.writeInt(this.CREATED_BY);
					
					// int
				
		            	dos.writeInt(this.UPDATED_BY);
					
					// java.util.Date
				
						writeDate(this.CREATED_DATE,dos);
					
					// java.util.Date
				
						writeDate(this.UPDATED_DATE,dos);
					
					// String
				
						writeString(this.BENEFICIARY_BANK_NAME,dos);
					
					// String
				
						writeString(this.BENEFICIARY_BANK_ACCOUNT_DESC,dos);
					
					// Integer
				
						writeInteger(this.REF_WARRANT_LINE,dos);
					
					// String
				
						writeString(this.IPPIS_EMPLOYEE_ID,dos);
					
					// String
				
						writeString(this.IPPIS_BENEFICIARY_CODE,dos);
					
					// String
				
						writeString(this.IPPIS_PAYMENT_TYPE_CODE,dos);
					
					// String
				
						writeString(this.LEDGER_STATUS,dos);
					
					// int
				
		            	dos.writeInt(this.IN_SETTLEMENT);
					
					// Integer
				
						writeInteger(this.REF_AUTHORIZING_ORG,dos);
					
					// Integer
				
						writeInteger(this.REF_FUND_MDA,dos);
					
					// int
				
		            	dos.writeInt(this.PAYMENT_BATCH_NUMBER);
					
					// Integer
				
						writeInteger(this.REF_ORIGINAL_PAYMENT_ORDER,dos);
					
					// Integer
				
						writeInteger(this.REF_CREDITED_PAYMENT_ORDER,dos);
					
					// String
				
						writeString(this.RESENDING_STATUS,dos);
					
					// Integer
				
						writeInteger(this.IS_REFUND,dos);
					
					// String
				
						writeString(this.REFUND_REASON,dos);
					
					// String
				
						writeString(this.REFUND_ORIGINATING_DOCUMENT,dos);
					
					// java.util.Date
				
						writeDate(this.etl_date,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("ID="+String.valueOf(ID));
		sb.append(",REF_PAYMENT_BATCH="+String.valueOf(REF_PAYMENT_BATCH));
		sb.append(",LINE_NO="+String.valueOf(LINE_NO));
		sb.append(",REF_ORGANIZATION="+String.valueOf(REF_ORGANIZATION));
		sb.append(",BENEFICIARY_TYPE="+BENEFICIARY_TYPE);
		sb.append(",BENEFICIARY_TIN="+BENEFICIARY_TIN);
		sb.append(",BENEFICIARY_NAME="+BENEFICIARY_NAME);
		sb.append(",BENEFICIARY_BANK_CODE="+BENEFICIARY_BANK_CODE);
		sb.append(",BENEFICIARY_BANK_ACCOUNT="+BENEFICIARY_BANK_ACCOUNT);
		sb.append(",BENEFICIARY_EMAIL="+BENEFICIARY_EMAIL);
		sb.append(",BENEFICIARY_PHONE="+BENEFICIARY_PHONE);
		sb.append(",BENEFICIARY_ADDRESS="+BENEFICIARY_ADDRESS);
		sb.append(",REF_CURRENCY="+String.valueOf(REF_CURRENCY));
		sb.append(",CURRENCY_RATE="+String.valueOf(CURRENCY_RATE));
		sb.append(",PAYMENT_REFERENCE_NO="+PAYMENT_REFERENCE_NO);
		sb.append(",PAYMENT_AMOUNT="+String.valueOf(PAYMENT_AMOUNT));
		sb.append(",PAYMENT_AMOUNT_BASE="+String.valueOf(PAYMENT_AMOUNT_BASE));
		sb.append(",DESCRIPTION="+DESCRIPTION);
		sb.append(",SUPPLIER_INVOICE_NO="+SUPPLIER_INVOICE_NO);
		sb.append(",SUPPLIER_INVOICE_DATE="+String.valueOf(SUPPLIER_INVOICE_DATE));
		sb.append(",SUPPLIER_INVOICE_DUEDATE="+String.valueOf(SUPPLIER_INVOICE_DUEDATE));
		sb.append(",REF_SUPPLIER="+String.valueOf(REF_SUPPLIER));
		sb.append(",REF_EMPLOYEE="+String.valueOf(REF_EMPLOYEE));
		sb.append(",REF_CUSTOMER="+String.valueOf(REF_CUSTOMER));
		sb.append(",PAYMENT_STATUS="+PAYMENT_STATUS);
		sb.append(",REF_ORG_DEPARTMENT="+String.valueOf(REF_ORG_DEPARTMENT));
		sb.append(",REF_CLIENT="+String.valueOf(REF_CLIENT));
		sb.append(",VERSION="+String.valueOf(VERSION));
		sb.append(",CREATED_BY="+String.valueOf(CREATED_BY));
		sb.append(",UPDATED_BY="+String.valueOf(UPDATED_BY));
		sb.append(",CREATED_DATE="+String.valueOf(CREATED_DATE));
		sb.append(",UPDATED_DATE="+String.valueOf(UPDATED_DATE));
		sb.append(",BENEFICIARY_BANK_NAME="+BENEFICIARY_BANK_NAME);
		sb.append(",BENEFICIARY_BANK_ACCOUNT_DESC="+BENEFICIARY_BANK_ACCOUNT_DESC);
		sb.append(",REF_WARRANT_LINE="+String.valueOf(REF_WARRANT_LINE));
		sb.append(",IPPIS_EMPLOYEE_ID="+IPPIS_EMPLOYEE_ID);
		sb.append(",IPPIS_BENEFICIARY_CODE="+IPPIS_BENEFICIARY_CODE);
		sb.append(",IPPIS_PAYMENT_TYPE_CODE="+IPPIS_PAYMENT_TYPE_CODE);
		sb.append(",LEDGER_STATUS="+LEDGER_STATUS);
		sb.append(",IN_SETTLEMENT="+String.valueOf(IN_SETTLEMENT));
		sb.append(",REF_AUTHORIZING_ORG="+String.valueOf(REF_AUTHORIZING_ORG));
		sb.append(",REF_FUND_MDA="+String.valueOf(REF_FUND_MDA));
		sb.append(",PAYMENT_BATCH_NUMBER="+String.valueOf(PAYMENT_BATCH_NUMBER));
		sb.append(",REF_ORIGINAL_PAYMENT_ORDER="+String.valueOf(REF_ORIGINAL_PAYMENT_ORDER));
		sb.append(",REF_CREDITED_PAYMENT_ORDER="+String.valueOf(REF_CREDITED_PAYMENT_ORDER));
		sb.append(",RESENDING_STATUS="+RESENDING_STATUS);
		sb.append(",IS_REFUND="+String.valueOf(IS_REFUND));
		sb.append(",REFUND_REASON="+REFUND_REASON);
		sb.append(",REFUND_ORIGINATING_DOCUMENT="+REFUND_ORIGINATING_DOCUMENT);
		sb.append(",etl_date="+String.valueOf(etl_date));
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(ltStruct other) {

		int returnValue = -1;
		
						returnValue = checkNullsAndCompare(this.ID, other.ID);
						if(returnValue != 0) {
							return returnValue;
						}

					
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}

public static class row6Struct implements routines.system.IPersistableRow<row6Struct> {
    final static byte[] commonByteArrayLock_GIFMIS_payment_order_bulk_2 = new byte[0];
    static byte[] commonByteArray_GIFMIS_payment_order_bulk_2 = new byte[0];

	
			    public int ID;

				public int getID () {
					return this.ID;
				}
				
			    public int REF_PAYMENT_BATCH;

				public int getREF_PAYMENT_BATCH () {
					return this.REF_PAYMENT_BATCH;
				}
				
			    public int LINE_NO;

				public int getLINE_NO () {
					return this.LINE_NO;
				}
				
			    public int REF_ORGANIZATION;

				public int getREF_ORGANIZATION () {
					return this.REF_ORGANIZATION;
				}
				
			    public String BENEFICIARY_TYPE;

				public String getBENEFICIARY_TYPE () {
					return this.BENEFICIARY_TYPE;
				}
				
			    public String BENEFICIARY_TIN;

				public String getBENEFICIARY_TIN () {
					return this.BENEFICIARY_TIN;
				}
				
			    public String BENEFICIARY_NAME;

				public String getBENEFICIARY_NAME () {
					return this.BENEFICIARY_NAME;
				}
				
			    public String BENEFICIARY_BANK_CODE;

				public String getBENEFICIARY_BANK_CODE () {
					return this.BENEFICIARY_BANK_CODE;
				}
				
			    public String BENEFICIARY_BANK_ACCOUNT;

				public String getBENEFICIARY_BANK_ACCOUNT () {
					return this.BENEFICIARY_BANK_ACCOUNT;
				}
				
			    public String BENEFICIARY_EMAIL;

				public String getBENEFICIARY_EMAIL () {
					return this.BENEFICIARY_EMAIL;
				}
				
			    public String BENEFICIARY_PHONE;

				public String getBENEFICIARY_PHONE () {
					return this.BENEFICIARY_PHONE;
				}
				
			    public String BENEFICIARY_ADDRESS;

				public String getBENEFICIARY_ADDRESS () {
					return this.BENEFICIARY_ADDRESS;
				}
				
			    public int REF_CURRENCY;

				public int getREF_CURRENCY () {
					return this.REF_CURRENCY;
				}
				
			    public BigDecimal CURRENCY_RATE;

				public BigDecimal getCURRENCY_RATE () {
					return this.CURRENCY_RATE;
				}
				
			    public String PAYMENT_REFERENCE_NO;

				public String getPAYMENT_REFERENCE_NO () {
					return this.PAYMENT_REFERENCE_NO;
				}
				
			    public BigDecimal PAYMENT_AMOUNT;

				public BigDecimal getPAYMENT_AMOUNT () {
					return this.PAYMENT_AMOUNT;
				}
				
			    public BigDecimal PAYMENT_AMOUNT_BASE;

				public BigDecimal getPAYMENT_AMOUNT_BASE () {
					return this.PAYMENT_AMOUNT_BASE;
				}
				
			    public String DESCRIPTION;

				public String getDESCRIPTION () {
					return this.DESCRIPTION;
				}
				
			    public String SUPPLIER_INVOICE_NO;

				public String getSUPPLIER_INVOICE_NO () {
					return this.SUPPLIER_INVOICE_NO;
				}
				
			    public java.util.Date SUPPLIER_INVOICE_DATE;

				public java.util.Date getSUPPLIER_INVOICE_DATE () {
					return this.SUPPLIER_INVOICE_DATE;
				}
				
			    public java.util.Date SUPPLIER_INVOICE_DUEDATE;

				public java.util.Date getSUPPLIER_INVOICE_DUEDATE () {
					return this.SUPPLIER_INVOICE_DUEDATE;
				}
				
			    public Integer REF_SUPPLIER;

				public Integer getREF_SUPPLIER () {
					return this.REF_SUPPLIER;
				}
				
			    public Integer REF_EMPLOYEE;

				public Integer getREF_EMPLOYEE () {
					return this.REF_EMPLOYEE;
				}
				
			    public Integer REF_CUSTOMER;

				public Integer getREF_CUSTOMER () {
					return this.REF_CUSTOMER;
				}
				
			    public String PAYMENT_STATUS;

				public String getPAYMENT_STATUS () {
					return this.PAYMENT_STATUS;
				}
				
			    public Integer REF_ORG_DEPARTMENT;

				public Integer getREF_ORG_DEPARTMENT () {
					return this.REF_ORG_DEPARTMENT;
				}
				
			    public int REF_CLIENT;

				public int getREF_CLIENT () {
					return this.REF_CLIENT;
				}
				
			    public int VERSION;

				public int getVERSION () {
					return this.VERSION;
				}
				
			    public int CREATED_BY;

				public int getCREATED_BY () {
					return this.CREATED_BY;
				}
				
			    public int UPDATED_BY;

				public int getUPDATED_BY () {
					return this.UPDATED_BY;
				}
				
			    public java.util.Date CREATED_DATE;

				public java.util.Date getCREATED_DATE () {
					return this.CREATED_DATE;
				}
				
			    public java.util.Date UPDATED_DATE;

				public java.util.Date getUPDATED_DATE () {
					return this.UPDATED_DATE;
				}
				
			    public String BENEFICIARY_BANK_NAME;

				public String getBENEFICIARY_BANK_NAME () {
					return this.BENEFICIARY_BANK_NAME;
				}
				
			    public String BENEFICIARY_BANK_ACCOUNT_DESC;

				public String getBENEFICIARY_BANK_ACCOUNT_DESC () {
					return this.BENEFICIARY_BANK_ACCOUNT_DESC;
				}
				
			    public Integer REF_WARRANT_LINE;

				public Integer getREF_WARRANT_LINE () {
					return this.REF_WARRANT_LINE;
				}
				
			    public String IPPIS_EMPLOYEE_ID;

				public String getIPPIS_EMPLOYEE_ID () {
					return this.IPPIS_EMPLOYEE_ID;
				}
				
			    public String IPPIS_BENEFICIARY_CODE;

				public String getIPPIS_BENEFICIARY_CODE () {
					return this.IPPIS_BENEFICIARY_CODE;
				}
				
			    public String IPPIS_PAYMENT_TYPE_CODE;

				public String getIPPIS_PAYMENT_TYPE_CODE () {
					return this.IPPIS_PAYMENT_TYPE_CODE;
				}
				
			    public String LEDGER_STATUS;

				public String getLEDGER_STATUS () {
					return this.LEDGER_STATUS;
				}
				
			    public int IN_SETTLEMENT;

				public int getIN_SETTLEMENT () {
					return this.IN_SETTLEMENT;
				}
				
			    public Integer REF_AUTHORIZING_ORG;

				public Integer getREF_AUTHORIZING_ORG () {
					return this.REF_AUTHORIZING_ORG;
				}
				
			    public Integer REF_FUND_MDA;

				public Integer getREF_FUND_MDA () {
					return this.REF_FUND_MDA;
				}
				
			    public int PAYMENT_BATCH_NUMBER;

				public int getPAYMENT_BATCH_NUMBER () {
					return this.PAYMENT_BATCH_NUMBER;
				}
				
			    public Integer REF_ORIGINAL_PAYMENT_ORDER;

				public Integer getREF_ORIGINAL_PAYMENT_ORDER () {
					return this.REF_ORIGINAL_PAYMENT_ORDER;
				}
				
			    public Integer REF_CREDITED_PAYMENT_ORDER;

				public Integer getREF_CREDITED_PAYMENT_ORDER () {
					return this.REF_CREDITED_PAYMENT_ORDER;
				}
				
			    public String RESENDING_STATUS;

				public String getRESENDING_STATUS () {
					return this.RESENDING_STATUS;
				}
				
			    public Integer IS_REFUND;

				public Integer getIS_REFUND () {
					return this.IS_REFUND;
				}
				
			    public String REFUND_REASON;

				public String getREFUND_REASON () {
					return this.REFUND_REASON;
				}
				
			    public String REFUND_ORIGINATING_DOCUMENT;

				public String getREFUND_ORIGINATING_DOCUMENT () {
					return this.REFUND_ORIGINATING_DOCUMENT;
				}
				
			    public java.util.Date etl_date;

				public java.util.Date getEtl_date () {
					return this.etl_date;
				}
				



	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_GIFMIS_payment_order_bulk_2.length) {
				if(length < 1024 && commonByteArray_GIFMIS_payment_order_bulk_2.length == 0) {
   					commonByteArray_GIFMIS_payment_order_bulk_2 = new byte[1024];
				} else {
   					commonByteArray_GIFMIS_payment_order_bulk_2 = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_GIFMIS_payment_order_bulk_2, 0, length);
			strReturn = new String(commonByteArray_GIFMIS_payment_order_bulk_2, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }

	private java.util.Date readDate(ObjectInputStream dis) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(dis.readLong());
		}
		return dateReturn;
	}

    private void writeDate(java.util.Date date1, ObjectOutputStream dos) throws IOException{
		if(date1 == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeLong(date1.getTime());
    	}
    }
	private Integer readInteger(ObjectInputStream dis) throws IOException{
		Integer intReturn;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			intReturn = null;
		} else {
	    	intReturn = dis.readInt();
		}
		return intReturn;
	}

	private void writeInteger(Integer intNum, ObjectOutputStream dos) throws IOException{
		if(intNum == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeInt(intNum);
    	}
	}

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_GIFMIS_payment_order_bulk_2) {

        	try {

        		int length = 0;
		
			        this.ID = dis.readInt();
					
			        this.REF_PAYMENT_BATCH = dis.readInt();
					
			        this.LINE_NO = dis.readInt();
					
			        this.REF_ORGANIZATION = dis.readInt();
					
					this.BENEFICIARY_TYPE = readString(dis);
					
					this.BENEFICIARY_TIN = readString(dis);
					
					this.BENEFICIARY_NAME = readString(dis);
					
					this.BENEFICIARY_BANK_CODE = readString(dis);
					
					this.BENEFICIARY_BANK_ACCOUNT = readString(dis);
					
					this.BENEFICIARY_EMAIL = readString(dis);
					
					this.BENEFICIARY_PHONE = readString(dis);
					
					this.BENEFICIARY_ADDRESS = readString(dis);
					
			        this.REF_CURRENCY = dis.readInt();
					
						this.CURRENCY_RATE = (BigDecimal) dis.readObject();
					
					this.PAYMENT_REFERENCE_NO = readString(dis);
					
						this.PAYMENT_AMOUNT = (BigDecimal) dis.readObject();
					
						this.PAYMENT_AMOUNT_BASE = (BigDecimal) dis.readObject();
					
					this.DESCRIPTION = readString(dis);
					
					this.SUPPLIER_INVOICE_NO = readString(dis);
					
					this.SUPPLIER_INVOICE_DATE = readDate(dis);
					
					this.SUPPLIER_INVOICE_DUEDATE = readDate(dis);
					
						this.REF_SUPPLIER = readInteger(dis);
					
						this.REF_EMPLOYEE = readInteger(dis);
					
						this.REF_CUSTOMER = readInteger(dis);
					
					this.PAYMENT_STATUS = readString(dis);
					
						this.REF_ORG_DEPARTMENT = readInteger(dis);
					
			        this.REF_CLIENT = dis.readInt();
					
			        this.VERSION = dis.readInt();
					
			        this.CREATED_BY = dis.readInt();
					
			        this.UPDATED_BY = dis.readInt();
					
					this.CREATED_DATE = readDate(dis);
					
					this.UPDATED_DATE = readDate(dis);
					
					this.BENEFICIARY_BANK_NAME = readString(dis);
					
					this.BENEFICIARY_BANK_ACCOUNT_DESC = readString(dis);
					
						this.REF_WARRANT_LINE = readInteger(dis);
					
					this.IPPIS_EMPLOYEE_ID = readString(dis);
					
					this.IPPIS_BENEFICIARY_CODE = readString(dis);
					
					this.IPPIS_PAYMENT_TYPE_CODE = readString(dis);
					
					this.LEDGER_STATUS = readString(dis);
					
			        this.IN_SETTLEMENT = dis.readInt();
					
						this.REF_AUTHORIZING_ORG = readInteger(dis);
					
						this.REF_FUND_MDA = readInteger(dis);
					
			        this.PAYMENT_BATCH_NUMBER = dis.readInt();
					
						this.REF_ORIGINAL_PAYMENT_ORDER = readInteger(dis);
					
						this.REF_CREDITED_PAYMENT_ORDER = readInteger(dis);
					
					this.RESENDING_STATUS = readString(dis);
					
						this.IS_REFUND = readInteger(dis);
					
					this.REFUND_REASON = readString(dis);
					
					this.REFUND_ORIGINATING_DOCUMENT = readString(dis);
					
					this.etl_date = readDate(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		
			} catch(ClassNotFoundException eCNFE) {
				 throw new RuntimeException(eCNFE);
		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// int
				
		            	dos.writeInt(this.ID);
					
					// int
				
		            	dos.writeInt(this.REF_PAYMENT_BATCH);
					
					// int
				
		            	dos.writeInt(this.LINE_NO);
					
					// int
				
		            	dos.writeInt(this.REF_ORGANIZATION);
					
					// String
				
						writeString(this.BENEFICIARY_TYPE,dos);
					
					// String
				
						writeString(this.BENEFICIARY_TIN,dos);
					
					// String
				
						writeString(this.BENEFICIARY_NAME,dos);
					
					// String
				
						writeString(this.BENEFICIARY_BANK_CODE,dos);
					
					// String
				
						writeString(this.BENEFICIARY_BANK_ACCOUNT,dos);
					
					// String
				
						writeString(this.BENEFICIARY_EMAIL,dos);
					
					// String
				
						writeString(this.BENEFICIARY_PHONE,dos);
					
					// String
				
						writeString(this.BENEFICIARY_ADDRESS,dos);
					
					// int
				
		            	dos.writeInt(this.REF_CURRENCY);
					
					// BigDecimal
				
       			    	dos.writeObject(this.CURRENCY_RATE);
					
					// String
				
						writeString(this.PAYMENT_REFERENCE_NO,dos);
					
					// BigDecimal
				
       			    	dos.writeObject(this.PAYMENT_AMOUNT);
					
					// BigDecimal
				
       			    	dos.writeObject(this.PAYMENT_AMOUNT_BASE);
					
					// String
				
						writeString(this.DESCRIPTION,dos);
					
					// String
				
						writeString(this.SUPPLIER_INVOICE_NO,dos);
					
					// java.util.Date
				
						writeDate(this.SUPPLIER_INVOICE_DATE,dos);
					
					// java.util.Date
				
						writeDate(this.SUPPLIER_INVOICE_DUEDATE,dos);
					
					// Integer
				
						writeInteger(this.REF_SUPPLIER,dos);
					
					// Integer
				
						writeInteger(this.REF_EMPLOYEE,dos);
					
					// Integer
				
						writeInteger(this.REF_CUSTOMER,dos);
					
					// String
				
						writeString(this.PAYMENT_STATUS,dos);
					
					// Integer
				
						writeInteger(this.REF_ORG_DEPARTMENT,dos);
					
					// int
				
		            	dos.writeInt(this.REF_CLIENT);
					
					// int
				
		            	dos.writeInt(this.VERSION);
					
					// int
				
		            	dos.writeInt(this.CREATED_BY);
					
					// int
				
		            	dos.writeInt(this.UPDATED_BY);
					
					// java.util.Date
				
						writeDate(this.CREATED_DATE,dos);
					
					// java.util.Date
				
						writeDate(this.UPDATED_DATE,dos);
					
					// String
				
						writeString(this.BENEFICIARY_BANK_NAME,dos);
					
					// String
				
						writeString(this.BENEFICIARY_BANK_ACCOUNT_DESC,dos);
					
					// Integer
				
						writeInteger(this.REF_WARRANT_LINE,dos);
					
					// String
				
						writeString(this.IPPIS_EMPLOYEE_ID,dos);
					
					// String
				
						writeString(this.IPPIS_BENEFICIARY_CODE,dos);
					
					// String
				
						writeString(this.IPPIS_PAYMENT_TYPE_CODE,dos);
					
					// String
				
						writeString(this.LEDGER_STATUS,dos);
					
					// int
				
		            	dos.writeInt(this.IN_SETTLEMENT);
					
					// Integer
				
						writeInteger(this.REF_AUTHORIZING_ORG,dos);
					
					// Integer
				
						writeInteger(this.REF_FUND_MDA,dos);
					
					// int
				
		            	dos.writeInt(this.PAYMENT_BATCH_NUMBER);
					
					// Integer
				
						writeInteger(this.REF_ORIGINAL_PAYMENT_ORDER,dos);
					
					// Integer
				
						writeInteger(this.REF_CREDITED_PAYMENT_ORDER,dos);
					
					// String
				
						writeString(this.RESENDING_STATUS,dos);
					
					// Integer
				
						writeInteger(this.IS_REFUND,dos);
					
					// String
				
						writeString(this.REFUND_REASON,dos);
					
					// String
				
						writeString(this.REFUND_ORIGINATING_DOCUMENT,dos);
					
					// java.util.Date
				
						writeDate(this.etl_date,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("ID="+String.valueOf(ID));
		sb.append(",REF_PAYMENT_BATCH="+String.valueOf(REF_PAYMENT_BATCH));
		sb.append(",LINE_NO="+String.valueOf(LINE_NO));
		sb.append(",REF_ORGANIZATION="+String.valueOf(REF_ORGANIZATION));
		sb.append(",BENEFICIARY_TYPE="+BENEFICIARY_TYPE);
		sb.append(",BENEFICIARY_TIN="+BENEFICIARY_TIN);
		sb.append(",BENEFICIARY_NAME="+BENEFICIARY_NAME);
		sb.append(",BENEFICIARY_BANK_CODE="+BENEFICIARY_BANK_CODE);
		sb.append(",BENEFICIARY_BANK_ACCOUNT="+BENEFICIARY_BANK_ACCOUNT);
		sb.append(",BENEFICIARY_EMAIL="+BENEFICIARY_EMAIL);
		sb.append(",BENEFICIARY_PHONE="+BENEFICIARY_PHONE);
		sb.append(",BENEFICIARY_ADDRESS="+BENEFICIARY_ADDRESS);
		sb.append(",REF_CURRENCY="+String.valueOf(REF_CURRENCY));
		sb.append(",CURRENCY_RATE="+String.valueOf(CURRENCY_RATE));
		sb.append(",PAYMENT_REFERENCE_NO="+PAYMENT_REFERENCE_NO);
		sb.append(",PAYMENT_AMOUNT="+String.valueOf(PAYMENT_AMOUNT));
		sb.append(",PAYMENT_AMOUNT_BASE="+String.valueOf(PAYMENT_AMOUNT_BASE));
		sb.append(",DESCRIPTION="+DESCRIPTION);
		sb.append(",SUPPLIER_INVOICE_NO="+SUPPLIER_INVOICE_NO);
		sb.append(",SUPPLIER_INVOICE_DATE="+String.valueOf(SUPPLIER_INVOICE_DATE));
		sb.append(",SUPPLIER_INVOICE_DUEDATE="+String.valueOf(SUPPLIER_INVOICE_DUEDATE));
		sb.append(",REF_SUPPLIER="+String.valueOf(REF_SUPPLIER));
		sb.append(",REF_EMPLOYEE="+String.valueOf(REF_EMPLOYEE));
		sb.append(",REF_CUSTOMER="+String.valueOf(REF_CUSTOMER));
		sb.append(",PAYMENT_STATUS="+PAYMENT_STATUS);
		sb.append(",REF_ORG_DEPARTMENT="+String.valueOf(REF_ORG_DEPARTMENT));
		sb.append(",REF_CLIENT="+String.valueOf(REF_CLIENT));
		sb.append(",VERSION="+String.valueOf(VERSION));
		sb.append(",CREATED_BY="+String.valueOf(CREATED_BY));
		sb.append(",UPDATED_BY="+String.valueOf(UPDATED_BY));
		sb.append(",CREATED_DATE="+String.valueOf(CREATED_DATE));
		sb.append(",UPDATED_DATE="+String.valueOf(UPDATED_DATE));
		sb.append(",BENEFICIARY_BANK_NAME="+BENEFICIARY_BANK_NAME);
		sb.append(",BENEFICIARY_BANK_ACCOUNT_DESC="+BENEFICIARY_BANK_ACCOUNT_DESC);
		sb.append(",REF_WARRANT_LINE="+String.valueOf(REF_WARRANT_LINE));
		sb.append(",IPPIS_EMPLOYEE_ID="+IPPIS_EMPLOYEE_ID);
		sb.append(",IPPIS_BENEFICIARY_CODE="+IPPIS_BENEFICIARY_CODE);
		sb.append(",IPPIS_PAYMENT_TYPE_CODE="+IPPIS_PAYMENT_TYPE_CODE);
		sb.append(",LEDGER_STATUS="+LEDGER_STATUS);
		sb.append(",IN_SETTLEMENT="+String.valueOf(IN_SETTLEMENT));
		sb.append(",REF_AUTHORIZING_ORG="+String.valueOf(REF_AUTHORIZING_ORG));
		sb.append(",REF_FUND_MDA="+String.valueOf(REF_FUND_MDA));
		sb.append(",PAYMENT_BATCH_NUMBER="+String.valueOf(PAYMENT_BATCH_NUMBER));
		sb.append(",REF_ORIGINAL_PAYMENT_ORDER="+String.valueOf(REF_ORIGINAL_PAYMENT_ORDER));
		sb.append(",REF_CREDITED_PAYMENT_ORDER="+String.valueOf(REF_CREDITED_PAYMENT_ORDER));
		sb.append(",RESENDING_STATUS="+RESENDING_STATUS);
		sb.append(",IS_REFUND="+String.valueOf(IS_REFUND));
		sb.append(",REFUND_REASON="+REFUND_REASON);
		sb.append(",REFUND_ORIGINATING_DOCUMENT="+REFUND_ORIGINATING_DOCUMENT);
		sb.append(",etl_date="+String.valueOf(etl_date));
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(row6Struct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}
public void tFileInputDelimited_1Process(final java.util.Map<String, Object> globalMap) throws TalendException {
	globalMap.put("tFileInputDelimited_1_SUBPROCESS_STATE", 0);

 final boolean execStat = this.execStat;
	
		String iterateId = "";
	
	
	String currentComponent = "";
	java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

	try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception().getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { //start the resume
				globalResumeTicket = true;



		row6Struct row6 = new row6Struct();
ltStruct lt = new ltStruct();





	
	/**
	 * [tDBOutput_1 begin ] start
	 */

	

	
		
		ok_Hash.put("tDBOutput_1", false);
		start_Hash.put("tDBOutput_1", System.currentTimeMillis());
		
	
	currentComponent="tDBOutput_1";

	
			if (execStat) {
				if(resourceMap.get("inIterateVComp") == null){
					
						runStat.updateStatOnConnection("lt" + iterateId, 0, 0);
					
				}
			} 

		
		int tos_count_tDBOutput_1 = 0;
		
    	class BytesLimit65535_tDBOutput_1{
    		public void limitLog4jByte() throws Exception{
    			
    		}
    	}
    	
        new BytesLimit65535_tDBOutput_1().limitLog4jByte();




int nb_line_tDBOutput_1 = 0;
int nb_line_update_tDBOutput_1 = 0;
int nb_line_inserted_tDBOutput_1 = 0;
int nb_line_deleted_tDBOutput_1 = 0;
int nb_line_rejected_tDBOutput_1 = 0;
int nb_line_copied_tDBOutput_1 = 0;

int deletedCount_tDBOutput_1=0;
int updatedCount_tDBOutput_1=0;
int insertedCount_tDBOutput_1=0;
int rejectedCount_tDBOutput_1=0;
int copiedCount_tDBOutput_1=0;

String tableName_tDBOutput_1 = null;
String dbschema_tDBOutput_1 = null;
boolean whetherReject_tDBOutput_1 = false;

		    java.sql.Connection conn_tDBOutput_1 = null;
		   
		   		 dbschema_tDBOutput_1 = context.Vertica_ODS_Schema;
			
					String driverClass_tDBOutput_1 = "com.vertica.jdbc.Driver";
			
				java.lang.Class.forName(driverClass_tDBOutput_1);
        		
				
				String url_tDBOutput_1 = "jdbc:vertica://" + context.Vertica_ODS_Server + ":" + context.Vertica_ODS_Port + "/" + context.Vertica_ODS_Database + "?" + context.Vertica_ODS_AdditionalParams;
				

				String dbUser_tDBOutput_1 = context.Vertica_ODS_Login;
				

				
	final String decryptedPassword_tDBOutput_1 = context.Vertica_ODS_Password; 

				String dbPwd_tDBOutput_1 = decryptedPassword_tDBOutput_1;
				
				conn_tDBOutput_1 = java.sql.DriverManager.getConnection(url_tDBOutput_1,dbUser_tDBOutput_1,dbPwd_tDBOutput_1);
				
				resourceMap.put("conn_tDBOutput_1", conn_tDBOutput_1);
			

if(dbschema_tDBOutput_1 == null || dbschema_tDBOutput_1.trim().length() == 0) {
    tableName_tDBOutput_1 = context.tableName;
} else {
    tableName_tDBOutput_1 = dbschema_tDBOutput_1 + "." + context.tableName;
}
conn_tDBOutput_1.setAutoCommit(false);

int commitEvery_tDBOutput_1 = 1000;

int commitCounter_tDBOutput_1 = 0;
   int batchSize_tDBOutput_1 = 5000;
   int batchSizeCounter_tDBOutput_1=0;

	int count_tDBOutput_1=0;
	
                                java.sql.DatabaseMetaData dbMetaData_tDBOutput_1 = conn_tDBOutput_1.getMetaData();
                                java.sql.ResultSet rsTable_tDBOutput_1 = dbMetaData_tDBOutput_1.getTables(null, null, null, new String[]{"TABLE"});
                                boolean whetherExist_tDBOutput_1 = false;
                                String defaultSchema_tDBOutput_1 = "public";
                                if(dbschema_tDBOutput_1 == null || dbschema_tDBOutput_1.trim().length() == 0) {
                                    java.sql.Statement stmtSchema_tDBOutput_1 = conn_tDBOutput_1.createStatement();
                                    java.sql.ResultSet rsSchema_tDBOutput_1 = stmtSchema_tDBOutput_1.executeQuery("select current_schema() ");
                                    while(rsSchema_tDBOutput_1.next()){
                                        defaultSchema_tDBOutput_1 = rsSchema_tDBOutput_1.getString("current_schema");
                                    }
                                    rsSchema_tDBOutput_1.close();
                                    stmtSchema_tDBOutput_1.close();
                                }
                                while(rsTable_tDBOutput_1.next()) {
                                    String table_tDBOutput_1 = rsTable_tDBOutput_1.getString("TABLE_NAME");
                                    String schema_tDBOutput_1 = rsTable_tDBOutput_1.getString("TABLE_SCHEM");
                                    if(table_tDBOutput_1.equalsIgnoreCase(context.tableName)
                                        && (schema_tDBOutput_1.equalsIgnoreCase(dbschema_tDBOutput_1) || ((dbschema_tDBOutput_1 ==null || dbschema_tDBOutput_1.trim().length() ==0) && defaultSchema_tDBOutput_1.equalsIgnoreCase(schema_tDBOutput_1)))) {
                                        whetherExist_tDBOutput_1 = true;
                                        break;
                                    }
                                }
                                rsTable_tDBOutput_1.close();
                                if(!whetherExist_tDBOutput_1) {
                                    java.sql.Statement stmtCreate_tDBOutput_1 = conn_tDBOutput_1.createStatement();
                                        stmtCreate_tDBOutput_1.execute("CREATE TABLE " + tableName_tDBOutput_1 + "(ID INTEGER  not null ,REF_PAYMENT_BATCH INTEGER  not null ,LINE_NO INTEGER  not null ,REF_ORGANIZATION INTEGER  not null ,BENEFICIARY_TYPE VARCHAR(10)   not null ,BENEFICIARY_TIN VARCHAR(20)  ,BENEFICIARY_NAME VARCHAR(200)   not null ,BENEFICIARY_BANK_CODE VARCHAR(20)   not null ,BENEFICIARY_BANK_ACCOUNT VARCHAR(40)   not null ,BENEFICIARY_EMAIL VARCHAR(100)  ,BENEFICIARY_PHONE VARCHAR(20)  ,BENEFICIARY_ADDRESS VARCHAR(50)  ,REF_CURRENCY INTEGER  not null ,CURRENCY_RATE NUMERIC(20,8)   not null ,PAYMENT_REFERENCE_NO VARCHAR(20)  ,PAYMENT_AMOUNT NUMERIC(20,4)   not null ,PAYMENT_AMOUNT_BASE NUMERIC(20,4)   not null ,DESCRIPTION VARCHAR(2000)  ,SUPPLIER_INVOICE_NO VARCHAR(20)  ,SUPPLIER_INVOICE_DATE TIMESTAMP ,SUPPLIER_INVOICE_DUEDATE TIMESTAMP ,REF_SUPPLIER INTEGER ,REF_EMPLOYEE INTEGER ,REF_CUSTOMER INTEGER ,PAYMENT_STATUS VARCHAR(25)   not null ,REF_ORG_DEPARTMENT INTEGER ,REF_CLIENT INTEGER  not null ,VERSION INTEGER  not null ,CREATED_BY INTEGER  not null ,UPDATED_BY INTEGER  not null ,CREATED_DATE TIMESTAMP  not null ,UPDATED_DATE TIMESTAMP  not null ,BENEFICIARY_BANK_NAME VARCHAR(1000)  ,BENEFICIARY_BANK_ACCOUNT_DESC VARCHAR(20)  ,REF_WARRANT_LINE INTEGER ,IPPIS_EMPLOYEE_ID VARCHAR(50)  ,IPPIS_BENEFICIARY_CODE VARCHAR(140)  ,IPPIS_PAYMENT_TYPE_CODE VARCHAR(20)  ,LEDGER_STATUS VARCHAR(20)   not null ,IN_SETTLEMENT INTEGER  not null ,REF_AUTHORIZING_ORG INTEGER ,REF_FUND_MDA INTEGER ,PAYMENT_BATCH_NUMBER INTEGER  not null ,REF_ORIGINAL_PAYMENT_ORDER INTEGER ,REF_CREDITED_PAYMENT_ORDER INTEGER ,RESENDING_STATUS VARCHAR(20)  ,IS_REFUND INTEGER ,REFUND_REASON VARCHAR(200)  ,REFUND_ORIGINATING_DOCUMENT VARCHAR(200)  ,etl_date TIMESTAMP ,primary key(ID))");
                                    stmtCreate_tDBOutput_1.close();
                                }
				if(!whetherExist_tDBOutput_1) {
				
				java.sql.Statement stmtCreateProjection_tDBOutput_1 = conn_tDBOutput_1.createStatement();
				stmtCreateProjection_tDBOutput_1.execute("CREATE PROJECTION " + tableName_tDBOutput_1 + "_proj (ID, REF_PAYMENT_BATCH, LINE_NO, REF_ORGANIZATION, BENEFICIARY_TYPE, BENEFICIARY_TIN, BENEFICIARY_NAME, BENEFICIARY_BANK_CODE, BENEFICIARY_BANK_ACCOUNT, BENEFICIARY_EMAIL, BENEFICIARY_PHONE, BENEFICIARY_ADDRESS, REF_CURRENCY, CURRENCY_RATE, PAYMENT_REFERENCE_NO, PAYMENT_AMOUNT, PAYMENT_AMOUNT_BASE, DESCRIPTION, SUPPLIER_INVOICE_NO, SUPPLIER_INVOICE_DATE, SUPPLIER_INVOICE_DUEDATE, REF_SUPPLIER, REF_EMPLOYEE, REF_CUSTOMER, PAYMENT_STATUS, REF_ORG_DEPARTMENT, REF_CLIENT, VERSION, CREATED_BY, UPDATED_BY, CREATED_DATE, UPDATED_DATE, BENEFICIARY_BANK_NAME, BENEFICIARY_BANK_ACCOUNT_DESC, REF_WARRANT_LINE, IPPIS_EMPLOYEE_ID, IPPIS_BENEFICIARY_CODE, IPPIS_PAYMENT_TYPE_CODE, LEDGER_STATUS, IN_SETTLEMENT, REF_AUTHORIZING_ORG, REF_FUND_MDA, PAYMENT_BATCH_NUMBER, REF_ORIGINAL_PAYMENT_ORDER, REF_CREDITED_PAYMENT_ORDER, RESENDING_STATUS, IS_REFUND, REFUND_REASON, REFUND_ORIGINATING_DOCUMENT, etl_date) AS SELECT ID, REF_PAYMENT_BATCH, LINE_NO, REF_ORGANIZATION, BENEFICIARY_TYPE, BENEFICIARY_TIN, BENEFICIARY_NAME, BENEFICIARY_BANK_CODE, BENEFICIARY_BANK_ACCOUNT, BENEFICIARY_EMAIL, BENEFICIARY_PHONE, BENEFICIARY_ADDRESS, REF_CURRENCY, CURRENCY_RATE, PAYMENT_REFERENCE_NO, PAYMENT_AMOUNT, PAYMENT_AMOUNT_BASE, DESCRIPTION, SUPPLIER_INVOICE_NO, SUPPLIER_INVOICE_DATE, SUPPLIER_INVOICE_DUEDATE, REF_SUPPLIER, REF_EMPLOYEE, REF_CUSTOMER, PAYMENT_STATUS, REF_ORG_DEPARTMENT, REF_CLIENT, VERSION, CREATED_BY, UPDATED_BY, CREATED_DATE, UPDATED_DATE, BENEFICIARY_BANK_NAME, BENEFICIARY_BANK_ACCOUNT_DESC, REF_WARRANT_LINE, IPPIS_EMPLOYEE_ID, IPPIS_BENEFICIARY_CODE, IPPIS_PAYMENT_TYPE_CODE, LEDGER_STATUS, IN_SETTLEMENT, REF_AUTHORIZING_ORG, REF_FUND_MDA, PAYMENT_BATCH_NUMBER, REF_ORIGINAL_PAYMENT_ORDER, REF_CREDITED_PAYMENT_ORDER, RESENDING_STATUS, IS_REFUND, REFUND_REASON, REFUND_ORIGINATING_DOCUMENT, etl_date FROM " + tableName_tDBOutput_1);
				stmtCreateProjection_tDBOutput_1.close();
			
				}
				
        String insert_tDBOutput_1 = "INSERT INTO " + tableName_tDBOutput_1 + " (ID,REF_PAYMENT_BATCH,LINE_NO,REF_ORGANIZATION,BENEFICIARY_TYPE,BENEFICIARY_TIN,BENEFICIARY_NAME,BENEFICIARY_BANK_CODE,BENEFICIARY_BANK_ACCOUNT,BENEFICIARY_EMAIL,BENEFICIARY_PHONE,BENEFICIARY_ADDRESS,REF_CURRENCY,CURRENCY_RATE,PAYMENT_REFERENCE_NO,PAYMENT_AMOUNT,PAYMENT_AMOUNT_BASE,DESCRIPTION,SUPPLIER_INVOICE_NO,SUPPLIER_INVOICE_DATE,SUPPLIER_INVOICE_DUEDATE,REF_SUPPLIER,REF_EMPLOYEE,REF_CUSTOMER,PAYMENT_STATUS,REF_ORG_DEPARTMENT,REF_CLIENT,VERSION,CREATED_BY,UPDATED_BY,CREATED_DATE,UPDATED_DATE,BENEFICIARY_BANK_NAME,BENEFICIARY_BANK_ACCOUNT_DESC,REF_WARRANT_LINE,IPPIS_EMPLOYEE_ID,IPPIS_BENEFICIARY_CODE,IPPIS_PAYMENT_TYPE_CODE,LEDGER_STATUS,IN_SETTLEMENT,REF_AUTHORIZING_ORG,REF_FUND_MDA,PAYMENT_BATCH_NUMBER,REF_ORIGINAL_PAYMENT_ORDER,REF_CREDITED_PAYMENT_ORDER,RESENDING_STATUS,IS_REFUND,REFUND_REASON,REFUND_ORIGINATING_DOCUMENT,etl_date) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        java.sql.PreparedStatement pstmt_tDBOutput_1 = conn_tDBOutput_1.prepareStatement(insert_tDBOutput_1);
        int batchCount_tDBOutput_1 = 0;

 



/**
 * [tDBOutput_1 begin ] stop
 */



	
	/**
	 * [tMap_2 begin ] start
	 */

	

	
		
		ok_Hash.put("tMap_2", false);
		start_Hash.put("tMap_2", System.currentTimeMillis());
		
	
	currentComponent="tMap_2";

	
			if (execStat) {
				if(resourceMap.get("inIterateVComp") == null){
					
						runStat.updateStatOnConnection("row6" + iterateId, 0, 0);
					
				}
			} 

		
		int tos_count_tMap_2 = 0;
		
    	class BytesLimit65535_tMap_2{
    		public void limitLog4jByte() throws Exception{
    			
    		}
    	}
    	
        new BytesLimit65535_tMap_2().limitLog4jByte();




// ###############################
// # Lookup's keys initialization
// ###############################        

// ###############################
// # Vars initialization
class  Var__tMap_2__Struct  {
}
Var__tMap_2__Struct Var__tMap_2 = new Var__tMap_2__Struct();
// ###############################

// ###############################
// # Outputs initialization
ltStruct lt_tmp = new ltStruct();
// ###############################

        
        



        









 



/**
 * [tMap_2 begin ] stop
 */



	
	/**
	 * [tFileInputDelimited_1 begin ] start
	 */

	

	
		
		ok_Hash.put("tFileInputDelimited_1", false);
		start_Hash.put("tFileInputDelimited_1", System.currentTimeMillis());
		
				talendStats_STATS.addMessage("begin","tFileInputDelimited_1");
				talendStats_STATSProcess(globalMap);
			
	
	currentComponent="tFileInputDelimited_1";

	
		int tos_count_tFileInputDelimited_1 = 0;
		
    	class BytesLimit65535_tFileInputDelimited_1{
    		public void limitLog4jByte() throws Exception{
    			
    		}
    	}
    	
        new BytesLimit65535_tFileInputDelimited_1().limitLog4jByte();
	
	
	
 
	
	
	final routines.system.RowState rowstate_tFileInputDelimited_1 = new routines.system.RowState();
	
	
				int nb_line_tFileInputDelimited_1 = 0;
				int footer_tFileInputDelimited_1 = 0;
				int totalLinetFileInputDelimited_1 = 0;
				int limittFileInputDelimited_1 = -1;
				int lastLinetFileInputDelimited_1 = -1;	
				
				char fieldSeparator_tFileInputDelimited_1[] = null;
				
				//support passing value (property: Field Separator) by 'context.fs' or 'globalMap.get("fs")'. 
				if ( ((String)";").length() > 0 ){
					fieldSeparator_tFileInputDelimited_1 = ((String)";").toCharArray();
				}else {			
					throw new IllegalArgumentException("Field Separator must be assigned a char."); 
				}
			
				char rowSeparator_tFileInputDelimited_1[] = null;
			
				//support passing value (property: Row Separator) by 'context.rs' or 'globalMap.get("rs")'. 
				if ( ((String)"\n").length() > 0 ){
					rowSeparator_tFileInputDelimited_1 = ((String)"\n").toCharArray();
				}else {
					throw new IllegalArgumentException("Row Separator must be assigned a char."); 
				}
			
				Object filename_tFileInputDelimited_1 = /** Start field tFileInputDelimited_1:FILENAME */context.filePath+"/"+jobName+"_out.csv"/** End field tFileInputDelimited_1:FILENAME */;		
				com.talend.csv.CSVReader csvReadertFileInputDelimited_1 = null;
	
				try{
					
						String[] rowtFileInputDelimited_1=null;
						int currentLinetFileInputDelimited_1 = 0;
	        			int outputLinetFileInputDelimited_1 = 0;
						try {//TD110 begin
							if(filename_tFileInputDelimited_1 instanceof java.io.InputStream){
							
			int footer_value_tFileInputDelimited_1 = 0;
			if(footer_value_tFileInputDelimited_1 > 0){
				throw new java.lang.Exception("When the input source is a stream,footer shouldn't be bigger than 0.");
			}
		
								csvReadertFileInputDelimited_1=new com.talend.csv.CSVReader((java.io.InputStream)filename_tFileInputDelimited_1, fieldSeparator_tFileInputDelimited_1[0], "ISO-8859-15");
							}else{
								csvReadertFileInputDelimited_1=new com.talend.csv.CSVReader(new java.io.BufferedReader(new java.io.InputStreamReader(
		                		new java.io.FileInputStream(String.valueOf(filename_tFileInputDelimited_1)),"ISO-8859-15")), fieldSeparator_tFileInputDelimited_1[0]);
		        			}
					
					
					csvReadertFileInputDelimited_1.setTrimWhitespace(false);
					if ( (rowSeparator_tFileInputDelimited_1[0] != '\n') && (rowSeparator_tFileInputDelimited_1[0] != '\r') )
	        			csvReadertFileInputDelimited_1.setLineEnd(""+rowSeparator_tFileInputDelimited_1[0]);
						
	        				csvReadertFileInputDelimited_1.setQuoteChar('"');
						
	            				csvReadertFileInputDelimited_1.setEscapeChar(csvReadertFileInputDelimited_1.getQuoteChar());
							      
		
			
						if(footer_tFileInputDelimited_1 > 0){
						for(totalLinetFileInputDelimited_1=0;totalLinetFileInputDelimited_1 < 1; totalLinetFileInputDelimited_1++){
							csvReadertFileInputDelimited_1.readNext();
						}
						csvReadertFileInputDelimited_1.setSkipEmptyRecords(true);
			            while (csvReadertFileInputDelimited_1.readNext()) {
							
								rowtFileInputDelimited_1=csvReadertFileInputDelimited_1.getValues();
								if(!(rowtFileInputDelimited_1.length == 1 && ("\015").equals(rowtFileInputDelimited_1[0]))){//empty line when row separator is '\n'
							
	                
	                		totalLinetFileInputDelimited_1++;
	                
							
								}
							
	                
			            }
	            		int lastLineTemptFileInputDelimited_1 = totalLinetFileInputDelimited_1 - footer_tFileInputDelimited_1   < 0? 0 : totalLinetFileInputDelimited_1 - footer_tFileInputDelimited_1 ;
	            		if(lastLinetFileInputDelimited_1 > 0){
	                		lastLinetFileInputDelimited_1 = lastLinetFileInputDelimited_1 < lastLineTemptFileInputDelimited_1 ? lastLinetFileInputDelimited_1 : lastLineTemptFileInputDelimited_1; 
	            		}else {
	                		lastLinetFileInputDelimited_1 = lastLineTemptFileInputDelimited_1;
	            		}
	         
			          	csvReadertFileInputDelimited_1.close();
				        if(filename_tFileInputDelimited_1 instanceof java.io.InputStream){
				 			csvReadertFileInputDelimited_1=new com.talend.csv.CSVReader((java.io.InputStream)filename_tFileInputDelimited_1, fieldSeparator_tFileInputDelimited_1[0], "ISO-8859-15");
		        		}else{
				 			csvReadertFileInputDelimited_1=new com.talend.csv.CSVReader(new java.io.BufferedReader(new java.io.InputStreamReader(
				          	new java.io.FileInputStream(String.valueOf(filename_tFileInputDelimited_1)),"ISO-8859-15")), fieldSeparator_tFileInputDelimited_1[0]);
						}
						csvReadertFileInputDelimited_1.setTrimWhitespace(false);
						if ( (rowSeparator_tFileInputDelimited_1[0] != '\n') && (rowSeparator_tFileInputDelimited_1[0] != '\r') )	
	        				csvReadertFileInputDelimited_1.setLineEnd(""+rowSeparator_tFileInputDelimited_1[0]);
						
							csvReadertFileInputDelimited_1.setQuoteChar('"');
						
	        				csvReadertFileInputDelimited_1.setEscapeChar(csvReadertFileInputDelimited_1.getQuoteChar());
							  
	        		}
	        
			        if(limittFileInputDelimited_1 != 0){
			        	for(currentLinetFileInputDelimited_1=0;currentLinetFileInputDelimited_1 < 1;currentLinetFileInputDelimited_1++){
			        		csvReadertFileInputDelimited_1.readNext();
			        	}
			        }
			        csvReadertFileInputDelimited_1.setSkipEmptyRecords(true);
	        
	    		} catch(java.lang.Exception e) {
					
						
						System.err.println(e.getMessage());
					
	    		}//TD110 end
	        
			    
	        	while ( limittFileInputDelimited_1 != 0 && csvReadertFileInputDelimited_1!=null && csvReadertFileInputDelimited_1.readNext() ) { 
	        		rowstate_tFileInputDelimited_1.reset();
	        
		        	rowtFileInputDelimited_1=csvReadertFileInputDelimited_1.getValues();
	        	
					
	        			if(rowtFileInputDelimited_1.length == 1 && ("\015").equals(rowtFileInputDelimited_1[0])){//empty line when row separator is '\n'
	        				continue;
	        			}
					
	        	
	        	
	        		currentLinetFileInputDelimited_1++;
	            
		            if(lastLinetFileInputDelimited_1 > -1 && currentLinetFileInputDelimited_1 > lastLinetFileInputDelimited_1) {
		                break;
	    	        }
	        	    outputLinetFileInputDelimited_1++;
	            	if (limittFileInputDelimited_1 > 0 && outputLinetFileInputDelimited_1 > limittFileInputDelimited_1) {
	                	break;
	            	}  
	                                                                      
					
	    							row6 = null;			
								
								boolean whetherReject_tFileInputDelimited_1 = false;
								row6 = new row6Struct();
								try {			
									
				char fieldSeparator_tFileInputDelimited_1_ListType[] = null;
				//support passing value (property: Field Separator) by 'context.fs' or 'globalMap.get("fs")'. 
				if ( ((String)";").length() > 0 ){
					fieldSeparator_tFileInputDelimited_1_ListType = ((String)";").toCharArray();
				}else {			
					throw new IllegalArgumentException("Field Separator must be assigned a char."); 
				}
				if(rowtFileInputDelimited_1.length == 1 && ("\015").equals(rowtFileInputDelimited_1[0])){//empty line when row separator is '\n'
					
							row6.ID = 0;
					
							row6.REF_PAYMENT_BATCH = 0;
					
							row6.LINE_NO = 0;
					
							row6.REF_ORGANIZATION = 0;
					
							row6.BENEFICIARY_TYPE = null;
					
							row6.BENEFICIARY_TIN = null;
					
							row6.BENEFICIARY_NAME = null;
					
							row6.BENEFICIARY_BANK_CODE = null;
					
							row6.BENEFICIARY_BANK_ACCOUNT = null;
					
							row6.BENEFICIARY_EMAIL = null;
					
							row6.BENEFICIARY_PHONE = null;
					
							row6.BENEFICIARY_ADDRESS = null;
					
							row6.REF_CURRENCY = 0;
					
							row6.CURRENCY_RATE = null;
					
							row6.PAYMENT_REFERENCE_NO = null;
					
							row6.PAYMENT_AMOUNT = null;
					
							row6.PAYMENT_AMOUNT_BASE = null;
					
							row6.DESCRIPTION = null;
					
							row6.SUPPLIER_INVOICE_NO = null;
					
							row6.SUPPLIER_INVOICE_DATE = null;
					
							row6.SUPPLIER_INVOICE_DUEDATE = null;
					
							row6.REF_SUPPLIER = null;
					
							row6.REF_EMPLOYEE = null;
					
							row6.REF_CUSTOMER = null;
					
							row6.PAYMENT_STATUS = null;
					
							row6.REF_ORG_DEPARTMENT = null;
					
							row6.REF_CLIENT = 0;
					
							row6.VERSION = 0;
					
							row6.CREATED_BY = 0;
					
							row6.UPDATED_BY = 0;
					
							row6.CREATED_DATE = null;
					
							row6.UPDATED_DATE = null;
					
							row6.BENEFICIARY_BANK_NAME = null;
					
							row6.BENEFICIARY_BANK_ACCOUNT_DESC = null;
					
							row6.REF_WARRANT_LINE = null;
					
							row6.IPPIS_EMPLOYEE_ID = null;
					
							row6.IPPIS_BENEFICIARY_CODE = null;
					
							row6.IPPIS_PAYMENT_TYPE_CODE = null;
					
							row6.LEDGER_STATUS = null;
					
							row6.IN_SETTLEMENT = 0;
					
							row6.REF_AUTHORIZING_ORG = null;
					
							row6.REF_FUND_MDA = null;
					
							row6.PAYMENT_BATCH_NUMBER = 0;
					
							row6.REF_ORIGINAL_PAYMENT_ORDER = null;
					
							row6.REF_CREDITED_PAYMENT_ORDER = null;
					
							row6.RESENDING_STATUS = null;
					
							row6.IS_REFUND = null;
					
							row6.REFUND_REASON = null;
					
							row6.REFUND_ORIGINATING_DOCUMENT = null;
					
							row6.etl_date = null;
					
				}else{
					
	                int columnIndexWithD_tFileInputDelimited_1 = 0; //Column Index 
	                
						columnIndexWithD_tFileInputDelimited_1 = 0;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
								
									if(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1].length() > 0) {
										try {
									
										row6.ID = ParserUtils.parseTo_int(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1]);
									
									
										} catch(java.lang.Exception ex_tFileInputDelimited_1) {
											rowstate_tFileInputDelimited_1.setException(new RuntimeException(String.format("Couldn't parse value for column '%s' in '%s', value is '%s'. Details: %s",
												"ID", "row6", rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1], ex_tFileInputDelimited_1), ex_tFileInputDelimited_1));
										}
    								}else{
    									
    										rowstate_tFileInputDelimited_1.setException(new RuntimeException("Value is empty for column : 'ID' in 'row6' connection, value is invalid or this column should be nullable or have a default value."));
    									
    								}
									
									
							
						
						}else{
						
							rowstate_tFileInputDelimited_1.setException(new RuntimeException("Value is empty for column : 'ID' in 'row6' connection, value is invalid or this column should be nullable or have a default value."));
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 1;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
								
									if(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1].length() > 0) {
										try {
									
										row6.REF_PAYMENT_BATCH = ParserUtils.parseTo_int(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1]);
									
									
										} catch(java.lang.Exception ex_tFileInputDelimited_1) {
											rowstate_tFileInputDelimited_1.setException(new RuntimeException(String.format("Couldn't parse value for column '%s' in '%s', value is '%s'. Details: %s",
												"REF_PAYMENT_BATCH", "row6", rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1], ex_tFileInputDelimited_1), ex_tFileInputDelimited_1));
										}
    								}else{
    									
    										rowstate_tFileInputDelimited_1.setException(new RuntimeException("Value is empty for column : 'REF_PAYMENT_BATCH' in 'row6' connection, value is invalid or this column should be nullable or have a default value."));
    									
    								}
									
									
							
						
						}else{
						
							rowstate_tFileInputDelimited_1.setException(new RuntimeException("Value is empty for column : 'REF_PAYMENT_BATCH' in 'row6' connection, value is invalid or this column should be nullable or have a default value."));
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 2;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
								
									if(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1].length() > 0) {
										try {
									
										row6.LINE_NO = ParserUtils.parseTo_int(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1]);
									
									
										} catch(java.lang.Exception ex_tFileInputDelimited_1) {
											rowstate_tFileInputDelimited_1.setException(new RuntimeException(String.format("Couldn't parse value for column '%s' in '%s', value is '%s'. Details: %s",
												"LINE_NO", "row6", rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1], ex_tFileInputDelimited_1), ex_tFileInputDelimited_1));
										}
    								}else{
    									
    										rowstate_tFileInputDelimited_1.setException(new RuntimeException("Value is empty for column : 'LINE_NO' in 'row6' connection, value is invalid or this column should be nullable or have a default value."));
    									
    								}
									
									
							
						
						}else{
						
							rowstate_tFileInputDelimited_1.setException(new RuntimeException("Value is empty for column : 'LINE_NO' in 'row6' connection, value is invalid or this column should be nullable or have a default value."));
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 3;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
								
									if(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1].length() > 0) {
										try {
									
										row6.REF_ORGANIZATION = ParserUtils.parseTo_int(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1]);
									
									
										} catch(java.lang.Exception ex_tFileInputDelimited_1) {
											rowstate_tFileInputDelimited_1.setException(new RuntimeException(String.format("Couldn't parse value for column '%s' in '%s', value is '%s'. Details: %s",
												"REF_ORGANIZATION", "row6", rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1], ex_tFileInputDelimited_1), ex_tFileInputDelimited_1));
										}
    								}else{
    									
    										rowstate_tFileInputDelimited_1.setException(new RuntimeException("Value is empty for column : 'REF_ORGANIZATION' in 'row6' connection, value is invalid or this column should be nullable or have a default value."));
    									
    								}
									
									
							
						
						}else{
						
							rowstate_tFileInputDelimited_1.setException(new RuntimeException("Value is empty for column : 'REF_ORGANIZATION' in 'row6' connection, value is invalid or this column should be nullable or have a default value."));
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 4;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
									row6.BENEFICIARY_TYPE = rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1];
									
							
						
						}else{
						
							
								row6.BENEFICIARY_TYPE = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 5;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
									row6.BENEFICIARY_TIN = rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1];
									
							
						
						}else{
						
							
								row6.BENEFICIARY_TIN = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 6;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
									row6.BENEFICIARY_NAME = rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1];
									
							
						
						}else{
						
							
								row6.BENEFICIARY_NAME = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 7;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
									row6.BENEFICIARY_BANK_CODE = rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1];
									
							
						
						}else{
						
							
								row6.BENEFICIARY_BANK_CODE = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 8;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
									row6.BENEFICIARY_BANK_ACCOUNT = rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1];
									
							
						
						}else{
						
							
								row6.BENEFICIARY_BANK_ACCOUNT = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 9;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
									row6.BENEFICIARY_EMAIL = rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1];
									
							
						
						}else{
						
							
								row6.BENEFICIARY_EMAIL = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 10;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
									row6.BENEFICIARY_PHONE = rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1];
									
							
						
						}else{
						
							
								row6.BENEFICIARY_PHONE = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 11;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
									row6.BENEFICIARY_ADDRESS = rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1];
									
							
						
						}else{
						
							
								row6.BENEFICIARY_ADDRESS = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 12;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
								
									if(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1].length() > 0) {
										try {
									
										row6.REF_CURRENCY = ParserUtils.parseTo_int(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1]);
									
									
										} catch(java.lang.Exception ex_tFileInputDelimited_1) {
											rowstate_tFileInputDelimited_1.setException(new RuntimeException(String.format("Couldn't parse value for column '%s' in '%s', value is '%s'. Details: %s",
												"REF_CURRENCY", "row6", rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1], ex_tFileInputDelimited_1), ex_tFileInputDelimited_1));
										}
    								}else{
    									
    										rowstate_tFileInputDelimited_1.setException(new RuntimeException("Value is empty for column : 'REF_CURRENCY' in 'row6' connection, value is invalid or this column should be nullable or have a default value."));
    									
    								}
									
									
							
						
						}else{
						
							rowstate_tFileInputDelimited_1.setException(new RuntimeException("Value is empty for column : 'REF_CURRENCY' in 'row6' connection, value is invalid or this column should be nullable or have a default value."));
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 13;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
								
									if(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1].length() > 0) {
										try {
									
										row6.CURRENCY_RATE = ParserUtils.parseTo_BigDecimal(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1]);
									
									
										} catch(java.lang.Exception ex_tFileInputDelimited_1) {
											rowstate_tFileInputDelimited_1.setException(new RuntimeException(String.format("Couldn't parse value for column '%s' in '%s', value is '%s'. Details: %s",
												"CURRENCY_RATE", "row6", rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1], ex_tFileInputDelimited_1), ex_tFileInputDelimited_1));
										}
    								}else{
    									
											
												row6.CURRENCY_RATE = null;
											
    									
    								}
									
									
							
						
						}else{
						
							
								row6.CURRENCY_RATE = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 14;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
									row6.PAYMENT_REFERENCE_NO = rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1];
									
							
						
						}else{
						
							
								row6.PAYMENT_REFERENCE_NO = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 15;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
								
									if(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1].length() > 0) {
										try {
									
										row6.PAYMENT_AMOUNT = ParserUtils.parseTo_BigDecimal(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1]);
									
									
										} catch(java.lang.Exception ex_tFileInputDelimited_1) {
											rowstate_tFileInputDelimited_1.setException(new RuntimeException(String.format("Couldn't parse value for column '%s' in '%s', value is '%s'. Details: %s",
												"PAYMENT_AMOUNT", "row6", rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1], ex_tFileInputDelimited_1), ex_tFileInputDelimited_1));
										}
    								}else{
    									
											
												row6.PAYMENT_AMOUNT = null;
											
    									
    								}
									
									
							
						
						}else{
						
							
								row6.PAYMENT_AMOUNT = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 16;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
								
									if(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1].length() > 0) {
										try {
									
										row6.PAYMENT_AMOUNT_BASE = ParserUtils.parseTo_BigDecimal(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1]);
									
									
										} catch(java.lang.Exception ex_tFileInputDelimited_1) {
											rowstate_tFileInputDelimited_1.setException(new RuntimeException(String.format("Couldn't parse value for column '%s' in '%s', value is '%s'. Details: %s",
												"PAYMENT_AMOUNT_BASE", "row6", rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1], ex_tFileInputDelimited_1), ex_tFileInputDelimited_1));
										}
    								}else{
    									
											
												row6.PAYMENT_AMOUNT_BASE = null;
											
    									
    								}
									
									
							
						
						}else{
						
							
								row6.PAYMENT_AMOUNT_BASE = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 17;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
									row6.DESCRIPTION = rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1];
									
							
						
						}else{
						
							
								row6.DESCRIPTION = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 18;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
									row6.SUPPLIER_INVOICE_NO = rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1];
									
							
						
						}else{
						
							
								row6.SUPPLIER_INVOICE_NO = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 19;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
								
									if(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1].length() > 0) {
										try {
									
											row6.SUPPLIER_INVOICE_DATE = ParserUtils.parseTo_Date(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1], "yyyy-MM-dd HH:mm:ss");
										
									
										} catch(java.lang.Exception ex_tFileInputDelimited_1) {
											rowstate_tFileInputDelimited_1.setException(new RuntimeException(String.format("Couldn't parse value for column '%s' in '%s', value is '%s'. Details: %s",
												"SUPPLIER_INVOICE_DATE", "row6", rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1], ex_tFileInputDelimited_1), ex_tFileInputDelimited_1));
										}
    								}else{
    									
											
												row6.SUPPLIER_INVOICE_DATE = null;
											
    									
    								}
									
									
							
						
						}else{
						
							
								row6.SUPPLIER_INVOICE_DATE = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 20;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
								
									if(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1].length() > 0) {
										try {
									
											row6.SUPPLIER_INVOICE_DUEDATE = ParserUtils.parseTo_Date(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1], "yyyy-MM-dd HH:mm:ss");
										
									
										} catch(java.lang.Exception ex_tFileInputDelimited_1) {
											rowstate_tFileInputDelimited_1.setException(new RuntimeException(String.format("Couldn't parse value for column '%s' in '%s', value is '%s'. Details: %s",
												"SUPPLIER_INVOICE_DUEDATE", "row6", rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1], ex_tFileInputDelimited_1), ex_tFileInputDelimited_1));
										}
    								}else{
    									
											
												row6.SUPPLIER_INVOICE_DUEDATE = null;
											
    									
    								}
									
									
							
						
						}else{
						
							
								row6.SUPPLIER_INVOICE_DUEDATE = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 21;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
								
									if(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1].length() > 0) {
										try {
									
										row6.REF_SUPPLIER = ParserUtils.parseTo_Integer(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1]);
									
									
										} catch(java.lang.Exception ex_tFileInputDelimited_1) {
											rowstate_tFileInputDelimited_1.setException(new RuntimeException(String.format("Couldn't parse value for column '%s' in '%s', value is '%s'. Details: %s",
												"REF_SUPPLIER", "row6", rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1], ex_tFileInputDelimited_1), ex_tFileInputDelimited_1));
										}
    								}else{
    									
											
												row6.REF_SUPPLIER = null;
											
    									
    								}
									
									
							
						
						}else{
						
							
								row6.REF_SUPPLIER = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 22;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
								
									if(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1].length() > 0) {
										try {
									
										row6.REF_EMPLOYEE = ParserUtils.parseTo_Integer(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1]);
									
									
										} catch(java.lang.Exception ex_tFileInputDelimited_1) {
											rowstate_tFileInputDelimited_1.setException(new RuntimeException(String.format("Couldn't parse value for column '%s' in '%s', value is '%s'. Details: %s",
												"REF_EMPLOYEE", "row6", rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1], ex_tFileInputDelimited_1), ex_tFileInputDelimited_1));
										}
    								}else{
    									
											
												row6.REF_EMPLOYEE = null;
											
    									
    								}
									
									
							
						
						}else{
						
							
								row6.REF_EMPLOYEE = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 23;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
								
									if(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1].length() > 0) {
										try {
									
										row6.REF_CUSTOMER = ParserUtils.parseTo_Integer(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1]);
									
									
										} catch(java.lang.Exception ex_tFileInputDelimited_1) {
											rowstate_tFileInputDelimited_1.setException(new RuntimeException(String.format("Couldn't parse value for column '%s' in '%s', value is '%s'. Details: %s",
												"REF_CUSTOMER", "row6", rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1], ex_tFileInputDelimited_1), ex_tFileInputDelimited_1));
										}
    								}else{
    									
											
												row6.REF_CUSTOMER = null;
											
    									
    								}
									
									
							
						
						}else{
						
							
								row6.REF_CUSTOMER = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 24;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
									row6.PAYMENT_STATUS = rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1];
									
							
						
						}else{
						
							
								row6.PAYMENT_STATUS = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 25;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
								
									if(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1].length() > 0) {
										try {
									
										row6.REF_ORG_DEPARTMENT = ParserUtils.parseTo_Integer(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1]);
									
									
										} catch(java.lang.Exception ex_tFileInputDelimited_1) {
											rowstate_tFileInputDelimited_1.setException(new RuntimeException(String.format("Couldn't parse value for column '%s' in '%s', value is '%s'. Details: %s",
												"REF_ORG_DEPARTMENT", "row6", rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1], ex_tFileInputDelimited_1), ex_tFileInputDelimited_1));
										}
    								}else{
    									
											
												row6.REF_ORG_DEPARTMENT = null;
											
    									
    								}
									
									
							
						
						}else{
						
							
								row6.REF_ORG_DEPARTMENT = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 26;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
								
									if(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1].length() > 0) {
										try {
									
										row6.REF_CLIENT = ParserUtils.parseTo_int(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1]);
									
									
										} catch(java.lang.Exception ex_tFileInputDelimited_1) {
											rowstate_tFileInputDelimited_1.setException(new RuntimeException(String.format("Couldn't parse value for column '%s' in '%s', value is '%s'. Details: %s",
												"REF_CLIENT", "row6", rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1], ex_tFileInputDelimited_1), ex_tFileInputDelimited_1));
										}
    								}else{
    									
    										rowstate_tFileInputDelimited_1.setException(new RuntimeException("Value is empty for column : 'REF_CLIENT' in 'row6' connection, value is invalid or this column should be nullable or have a default value."));
    									
    								}
									
									
							
						
						}else{
						
							rowstate_tFileInputDelimited_1.setException(new RuntimeException("Value is empty for column : 'REF_CLIENT' in 'row6' connection, value is invalid or this column should be nullable or have a default value."));
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 27;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
								
									if(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1].length() > 0) {
										try {
									
										row6.VERSION = ParserUtils.parseTo_int(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1]);
									
									
										} catch(java.lang.Exception ex_tFileInputDelimited_1) {
											rowstate_tFileInputDelimited_1.setException(new RuntimeException(String.format("Couldn't parse value for column '%s' in '%s', value is '%s'. Details: %s",
												"VERSION", "row6", rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1], ex_tFileInputDelimited_1), ex_tFileInputDelimited_1));
										}
    								}else{
    									
    										rowstate_tFileInputDelimited_1.setException(new RuntimeException("Value is empty for column : 'VERSION' in 'row6' connection, value is invalid or this column should be nullable or have a default value."));
    									
    								}
									
									
							
						
						}else{
						
							rowstate_tFileInputDelimited_1.setException(new RuntimeException("Value is empty for column : 'VERSION' in 'row6' connection, value is invalid or this column should be nullable or have a default value."));
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 28;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
								
									if(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1].length() > 0) {
										try {
									
										row6.CREATED_BY = ParserUtils.parseTo_int(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1]);
									
									
										} catch(java.lang.Exception ex_tFileInputDelimited_1) {
											rowstate_tFileInputDelimited_1.setException(new RuntimeException(String.format("Couldn't parse value for column '%s' in '%s', value is '%s'. Details: %s",
												"CREATED_BY", "row6", rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1], ex_tFileInputDelimited_1), ex_tFileInputDelimited_1));
										}
    								}else{
    									
    										rowstate_tFileInputDelimited_1.setException(new RuntimeException("Value is empty for column : 'CREATED_BY' in 'row6' connection, value is invalid or this column should be nullable or have a default value."));
    									
    								}
									
									
							
						
						}else{
						
							rowstate_tFileInputDelimited_1.setException(new RuntimeException("Value is empty for column : 'CREATED_BY' in 'row6' connection, value is invalid or this column should be nullable or have a default value."));
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 29;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
								
									if(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1].length() > 0) {
										try {
									
										row6.UPDATED_BY = ParserUtils.parseTo_int(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1]);
									
									
										} catch(java.lang.Exception ex_tFileInputDelimited_1) {
											rowstate_tFileInputDelimited_1.setException(new RuntimeException(String.format("Couldn't parse value for column '%s' in '%s', value is '%s'. Details: %s",
												"UPDATED_BY", "row6", rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1], ex_tFileInputDelimited_1), ex_tFileInputDelimited_1));
										}
    								}else{
    									
    										rowstate_tFileInputDelimited_1.setException(new RuntimeException("Value is empty for column : 'UPDATED_BY' in 'row6' connection, value is invalid or this column should be nullable or have a default value."));
    									
    								}
									
									
							
						
						}else{
						
							rowstate_tFileInputDelimited_1.setException(new RuntimeException("Value is empty for column : 'UPDATED_BY' in 'row6' connection, value is invalid or this column should be nullable or have a default value."));
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 30;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
								
									if(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1].length() > 0) {
										try {
									
											row6.CREATED_DATE = ParserUtils.parseTo_Date(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1], "yyyy-MM-dd HH:mm:ss");
										
									
										} catch(java.lang.Exception ex_tFileInputDelimited_1) {
											rowstate_tFileInputDelimited_1.setException(new RuntimeException(String.format("Couldn't parse value for column '%s' in '%s', value is '%s'. Details: %s",
												"CREATED_DATE", "row6", rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1], ex_tFileInputDelimited_1), ex_tFileInputDelimited_1));
										}
    								}else{
    									
											
												row6.CREATED_DATE = null;
											
    									
    								}
									
									
							
						
						}else{
						
							
								row6.CREATED_DATE = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 31;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
								
									if(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1].length() > 0) {
										try {
									
											row6.UPDATED_DATE = ParserUtils.parseTo_Date(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1], "yyyy-MM-dd HH:mm:ss");
										
									
										} catch(java.lang.Exception ex_tFileInputDelimited_1) {
											rowstate_tFileInputDelimited_1.setException(new RuntimeException(String.format("Couldn't parse value for column '%s' in '%s', value is '%s'. Details: %s",
												"UPDATED_DATE", "row6", rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1], ex_tFileInputDelimited_1), ex_tFileInputDelimited_1));
										}
    								}else{
    									
											
												row6.UPDATED_DATE = null;
											
    									
    								}
									
									
							
						
						}else{
						
							
								row6.UPDATED_DATE = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 32;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
									row6.BENEFICIARY_BANK_NAME = rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1];
									
							
						
						}else{
						
							
								row6.BENEFICIARY_BANK_NAME = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 33;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
									row6.BENEFICIARY_BANK_ACCOUNT_DESC = rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1];
									
							
						
						}else{
						
							
								row6.BENEFICIARY_BANK_ACCOUNT_DESC = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 34;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
								
									if(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1].length() > 0) {
										try {
									
										row6.REF_WARRANT_LINE = ParserUtils.parseTo_Integer(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1]);
									
									
										} catch(java.lang.Exception ex_tFileInputDelimited_1) {
											rowstate_tFileInputDelimited_1.setException(new RuntimeException(String.format("Couldn't parse value for column '%s' in '%s', value is '%s'. Details: %s",
												"REF_WARRANT_LINE", "row6", rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1], ex_tFileInputDelimited_1), ex_tFileInputDelimited_1));
										}
    								}else{
    									
											
												row6.REF_WARRANT_LINE = null;
											
    									
    								}
									
									
							
						
						}else{
						
							
								row6.REF_WARRANT_LINE = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 35;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
									row6.IPPIS_EMPLOYEE_ID = rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1];
									
							
						
						}else{
						
							
								row6.IPPIS_EMPLOYEE_ID = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 36;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
									row6.IPPIS_BENEFICIARY_CODE = rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1];
									
							
						
						}else{
						
							
								row6.IPPIS_BENEFICIARY_CODE = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 37;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
									row6.IPPIS_PAYMENT_TYPE_CODE = rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1];
									
							
						
						}else{
						
							
								row6.IPPIS_PAYMENT_TYPE_CODE = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 38;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
									row6.LEDGER_STATUS = rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1];
									
							
						
						}else{
						
							
								row6.LEDGER_STATUS = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 39;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
								
									if(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1].length() > 0) {
										try {
									
										row6.IN_SETTLEMENT = ParserUtils.parseTo_int(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1]);
									
									
										} catch(java.lang.Exception ex_tFileInputDelimited_1) {
											rowstate_tFileInputDelimited_1.setException(new RuntimeException(String.format("Couldn't parse value for column '%s' in '%s', value is '%s'. Details: %s",
												"IN_SETTLEMENT", "row6", rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1], ex_tFileInputDelimited_1), ex_tFileInputDelimited_1));
										}
    								}else{
    									
    										rowstate_tFileInputDelimited_1.setException(new RuntimeException("Value is empty for column : 'IN_SETTLEMENT' in 'row6' connection, value is invalid or this column should be nullable or have a default value."));
    									
    								}
									
									
							
						
						}else{
						
							rowstate_tFileInputDelimited_1.setException(new RuntimeException("Value is empty for column : 'IN_SETTLEMENT' in 'row6' connection, value is invalid or this column should be nullable or have a default value."));
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 40;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
								
									if(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1].length() > 0) {
										try {
									
										row6.REF_AUTHORIZING_ORG = ParserUtils.parseTo_Integer(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1]);
									
									
										} catch(java.lang.Exception ex_tFileInputDelimited_1) {
											rowstate_tFileInputDelimited_1.setException(new RuntimeException(String.format("Couldn't parse value for column '%s' in '%s', value is '%s'. Details: %s",
												"REF_AUTHORIZING_ORG", "row6", rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1], ex_tFileInputDelimited_1), ex_tFileInputDelimited_1));
										}
    								}else{
    									
											
												row6.REF_AUTHORIZING_ORG = null;
											
    									
    								}
									
									
							
						
						}else{
						
							
								row6.REF_AUTHORIZING_ORG = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 41;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
								
									if(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1].length() > 0) {
										try {
									
										row6.REF_FUND_MDA = ParserUtils.parseTo_Integer(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1]);
									
									
										} catch(java.lang.Exception ex_tFileInputDelimited_1) {
											rowstate_tFileInputDelimited_1.setException(new RuntimeException(String.format("Couldn't parse value for column '%s' in '%s', value is '%s'. Details: %s",
												"REF_FUND_MDA", "row6", rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1], ex_tFileInputDelimited_1), ex_tFileInputDelimited_1));
										}
    								}else{
    									
											
												row6.REF_FUND_MDA = null;
											
    									
    								}
									
									
							
						
						}else{
						
							
								row6.REF_FUND_MDA = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 42;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
								
									if(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1].length() > 0) {
										try {
									
										row6.PAYMENT_BATCH_NUMBER = ParserUtils.parseTo_int(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1]);
									
									
										} catch(java.lang.Exception ex_tFileInputDelimited_1) {
											rowstate_tFileInputDelimited_1.setException(new RuntimeException(String.format("Couldn't parse value for column '%s' in '%s', value is '%s'. Details: %s",
												"PAYMENT_BATCH_NUMBER", "row6", rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1], ex_tFileInputDelimited_1), ex_tFileInputDelimited_1));
										}
    								}else{
    									
    										rowstate_tFileInputDelimited_1.setException(new RuntimeException("Value is empty for column : 'PAYMENT_BATCH_NUMBER' in 'row6' connection, value is invalid or this column should be nullable or have a default value."));
    									
    								}
									
									
							
						
						}else{
						
							rowstate_tFileInputDelimited_1.setException(new RuntimeException("Value is empty for column : 'PAYMENT_BATCH_NUMBER' in 'row6' connection, value is invalid or this column should be nullable or have a default value."));
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 43;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
								
									if(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1].length() > 0) {
										try {
									
										row6.REF_ORIGINAL_PAYMENT_ORDER = ParserUtils.parseTo_Integer(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1]);
									
									
										} catch(java.lang.Exception ex_tFileInputDelimited_1) {
											rowstate_tFileInputDelimited_1.setException(new RuntimeException(String.format("Couldn't parse value for column '%s' in '%s', value is '%s'. Details: %s",
												"REF_ORIGINAL_PAYMENT_ORDER", "row6", rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1], ex_tFileInputDelimited_1), ex_tFileInputDelimited_1));
										}
    								}else{
    									
											
												row6.REF_ORIGINAL_PAYMENT_ORDER = null;
											
    									
    								}
									
									
							
						
						}else{
						
							
								row6.REF_ORIGINAL_PAYMENT_ORDER = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 44;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
								
									if(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1].length() > 0) {
										try {
									
										row6.REF_CREDITED_PAYMENT_ORDER = ParserUtils.parseTo_Integer(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1]);
									
									
										} catch(java.lang.Exception ex_tFileInputDelimited_1) {
											rowstate_tFileInputDelimited_1.setException(new RuntimeException(String.format("Couldn't parse value for column '%s' in '%s', value is '%s'. Details: %s",
												"REF_CREDITED_PAYMENT_ORDER", "row6", rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1], ex_tFileInputDelimited_1), ex_tFileInputDelimited_1));
										}
    								}else{
    									
											
												row6.REF_CREDITED_PAYMENT_ORDER = null;
											
    									
    								}
									
									
							
						
						}else{
						
							
								row6.REF_CREDITED_PAYMENT_ORDER = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 45;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
									row6.RESENDING_STATUS = rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1];
									
							
						
						}else{
						
							
								row6.RESENDING_STATUS = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 46;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
								
									if(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1].length() > 0) {
										try {
									
										row6.IS_REFUND = ParserUtils.parseTo_Integer(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1]);
									
									
										} catch(java.lang.Exception ex_tFileInputDelimited_1) {
											rowstate_tFileInputDelimited_1.setException(new RuntimeException(String.format("Couldn't parse value for column '%s' in '%s', value is '%s'. Details: %s",
												"IS_REFUND", "row6", rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1], ex_tFileInputDelimited_1), ex_tFileInputDelimited_1));
										}
    								}else{
    									
											
												row6.IS_REFUND = null;
											
    									
    								}
									
									
							
						
						}else{
						
							
								row6.IS_REFUND = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 47;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
									row6.REFUND_REASON = rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1];
									
							
						
						}else{
						
							
								row6.REFUND_REASON = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 48;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
									row6.REFUND_ORIGINATING_DOCUMENT = rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1];
									
							
						
						}else{
						
							
								row6.REFUND_ORIGINATING_DOCUMENT = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 49;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
								
									if(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1].length() > 0) {
										try {
									
											row6.etl_date = ParserUtils.parseTo_Date(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1], "yyyy-MM-dd HH:mm:ss");
										
									
										} catch(java.lang.Exception ex_tFileInputDelimited_1) {
											rowstate_tFileInputDelimited_1.setException(new RuntimeException(String.format("Couldn't parse value for column '%s' in '%s', value is '%s'. Details: %s",
												"etl_date", "row6", rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1], ex_tFileInputDelimited_1), ex_tFileInputDelimited_1));
										}
    								}else{
    									
											
												row6.etl_date = null;
											
    									
    								}
									
									
							
						
						}else{
						
							
								row6.etl_date = null;
							
						
						}
						
						
					
				}
				
									
									if(rowstate_tFileInputDelimited_1.getException()!=null) {
										throw rowstate_tFileInputDelimited_1.getException();
									}
									
									
	    						} catch (java.lang.Exception e) {
							        whetherReject_tFileInputDelimited_1 = true;
        							
                							System.err.println(e.getMessage());
                							row6 = null;
                						
	    						}
	
							

 



/**
 * [tFileInputDelimited_1 begin ] stop
 */
	
	/**
	 * [tFileInputDelimited_1 main ] start
	 */

	

	
	
	currentComponent="tFileInputDelimited_1";

	

 


	tos_count_tFileInputDelimited_1++;

/**
 * [tFileInputDelimited_1 main ] stop
 */
	
	/**
	 * [tFileInputDelimited_1 process_data_begin ] start
	 */

	

	
	
	currentComponent="tFileInputDelimited_1";

	

 



/**
 * [tFileInputDelimited_1 process_data_begin ] stop
 */
// Start of branch "row6"
if(row6 != null) { 



	
	/**
	 * [tMap_2 main ] start
	 */

	

	
	
	currentComponent="tMap_2";

	

			//row6
			//row6


			
				if(execStat){
					runStat.updateStatOnConnection("row6"+iterateId,1, 1);
				} 
			

		

		
		
		boolean hasCasePrimitiveKeyWithNull_tMap_2 = false;
		
        // ###############################
        // # Input tables (lookups)
		  boolean rejectedInnerJoin_tMap_2 = false;
		  boolean mainRowRejected_tMap_2 = false;
            				    								  
		// ###############################
        { // start of Var scope
        
	        // ###############################
        	// # Vars tables
        
Var__tMap_2__Struct Var = Var__tMap_2;// ###############################
        // ###############################
        // # Output tables

lt = null;


// # Output table : 'lt'
lt_tmp.ID = row6.ID ;
lt_tmp.REF_PAYMENT_BATCH = row6.REF_PAYMENT_BATCH ;
lt_tmp.LINE_NO = row6.LINE_NO ;
lt_tmp.REF_ORGANIZATION = row6.REF_ORGANIZATION ;
lt_tmp.BENEFICIARY_TYPE = row6.BENEFICIARY_TYPE ;
lt_tmp.BENEFICIARY_TIN = row6.BENEFICIARY_TIN ;
lt_tmp.BENEFICIARY_NAME = row6.BENEFICIARY_NAME ;
lt_tmp.BENEFICIARY_BANK_CODE = row6.BENEFICIARY_BANK_CODE ;
lt_tmp.BENEFICIARY_BANK_ACCOUNT = row6.BENEFICIARY_BANK_ACCOUNT ;
lt_tmp.BENEFICIARY_EMAIL = row6.BENEFICIARY_EMAIL ;
lt_tmp.BENEFICIARY_PHONE = row6.BENEFICIARY_PHONE ;
lt_tmp.BENEFICIARY_ADDRESS = row6.BENEFICIARY_ADDRESS ;
lt_tmp.REF_CURRENCY = row6.REF_CURRENCY ;
lt_tmp.CURRENCY_RATE = row6.CURRENCY_RATE ;
lt_tmp.PAYMENT_REFERENCE_NO = row6.PAYMENT_REFERENCE_NO ;
lt_tmp.PAYMENT_AMOUNT = row6.PAYMENT_AMOUNT ;
lt_tmp.PAYMENT_AMOUNT_BASE = row6.PAYMENT_AMOUNT_BASE ;
lt_tmp.DESCRIPTION = row6.DESCRIPTION ;
lt_tmp.SUPPLIER_INVOICE_NO = row6.SUPPLIER_INVOICE_NO ;
lt_tmp.SUPPLIER_INVOICE_DATE = row6.SUPPLIER_INVOICE_DATE ;
lt_tmp.SUPPLIER_INVOICE_DUEDATE = row6.SUPPLIER_INVOICE_DUEDATE ;
lt_tmp.REF_SUPPLIER = row6.REF_SUPPLIER ;
lt_tmp.REF_EMPLOYEE = row6.REF_EMPLOYEE ;
lt_tmp.REF_CUSTOMER = row6.REF_CUSTOMER ;
lt_tmp.PAYMENT_STATUS = row6.PAYMENT_STATUS ;
lt_tmp.REF_ORG_DEPARTMENT = row6.REF_ORG_DEPARTMENT ;
lt_tmp.REF_CLIENT = row6.REF_CLIENT ;
lt_tmp.VERSION = row6.VERSION ;
lt_tmp.CREATED_BY = row6.CREATED_BY ;
lt_tmp.UPDATED_BY = row6.UPDATED_BY ;
lt_tmp.CREATED_DATE = row6.CREATED_DATE ;
lt_tmp.UPDATED_DATE = row6.UPDATED_DATE ;
lt_tmp.BENEFICIARY_BANK_NAME = row6.BENEFICIARY_BANK_NAME ;
lt_tmp.BENEFICIARY_BANK_ACCOUNT_DESC = row6.BENEFICIARY_BANK_ACCOUNT_DESC ;
lt_tmp.REF_WARRANT_LINE = row6.REF_WARRANT_LINE ;
lt_tmp.IPPIS_EMPLOYEE_ID = row6.IPPIS_EMPLOYEE_ID ;
lt_tmp.IPPIS_BENEFICIARY_CODE = row6.IPPIS_BENEFICIARY_CODE ;
lt_tmp.IPPIS_PAYMENT_TYPE_CODE = row6.IPPIS_PAYMENT_TYPE_CODE ;
lt_tmp.LEDGER_STATUS = row6.LEDGER_STATUS ;
lt_tmp.IN_SETTLEMENT = row6.IN_SETTLEMENT ;
lt_tmp.REF_AUTHORIZING_ORG = row6.REF_AUTHORIZING_ORG ;
lt_tmp.REF_FUND_MDA = row6.REF_FUND_MDA ;
lt_tmp.PAYMENT_BATCH_NUMBER = row6.PAYMENT_BATCH_NUMBER ;
lt_tmp.REF_ORIGINAL_PAYMENT_ORDER = row6.REF_ORIGINAL_PAYMENT_ORDER ;
lt_tmp.REF_CREDITED_PAYMENT_ORDER = row6.REF_CREDITED_PAYMENT_ORDER ;
lt_tmp.RESENDING_STATUS = row6.RESENDING_STATUS ;
lt_tmp.IS_REFUND = row6.IS_REFUND ;
lt_tmp.REFUND_REASON = row6.REFUND_REASON ;
lt_tmp.REFUND_ORIGINATING_DOCUMENT = row6.REFUND_ORIGINATING_DOCUMENT ;
lt_tmp.etl_date = row6.etl_date ;
lt = lt_tmp;
// ###############################

} // end of Var scope

rejectedInnerJoin_tMap_2 = false;










 


	tos_count_tMap_2++;

/**
 * [tMap_2 main ] stop
 */
	
	/**
	 * [tMap_2 process_data_begin ] start
	 */

	

	
	
	currentComponent="tMap_2";

	

 



/**
 * [tMap_2 process_data_begin ] stop
 */
// Start of branch "lt"
if(lt != null) { 



	
	/**
	 * [tDBOutput_1 main ] start
	 */

	

	
	
	currentComponent="tDBOutput_1";

	

			//lt
			//lt


			
				if(execStat){
					runStat.updateStatOnConnection("lt"+iterateId,1, 1);
				} 
			

		



            whetherReject_tDBOutput_1 = false;
                        pstmt_tDBOutput_1.setLong(1, lt.ID);

                        pstmt_tDBOutput_1.setLong(2, lt.REF_PAYMENT_BATCH);

                        pstmt_tDBOutput_1.setLong(3, lt.LINE_NO);

                        pstmt_tDBOutput_1.setLong(4, lt.REF_ORGANIZATION);

                        if(lt.BENEFICIARY_TYPE == null) {
pstmt_tDBOutput_1.setNull(5, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_1.setString(5, lt.BENEFICIARY_TYPE);
}

                        if(lt.BENEFICIARY_TIN == null) {
pstmt_tDBOutput_1.setNull(6, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_1.setString(6, lt.BENEFICIARY_TIN);
}

                        if(lt.BENEFICIARY_NAME == null) {
pstmt_tDBOutput_1.setNull(7, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_1.setString(7, lt.BENEFICIARY_NAME);
}

                        if(lt.BENEFICIARY_BANK_CODE == null) {
pstmt_tDBOutput_1.setNull(8, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_1.setString(8, lt.BENEFICIARY_BANK_CODE);
}

                        if(lt.BENEFICIARY_BANK_ACCOUNT == null) {
pstmt_tDBOutput_1.setNull(9, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_1.setString(9, lt.BENEFICIARY_BANK_ACCOUNT);
}

                        if(lt.BENEFICIARY_EMAIL == null) {
pstmt_tDBOutput_1.setNull(10, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_1.setString(10, lt.BENEFICIARY_EMAIL);
}

                        if(lt.BENEFICIARY_PHONE == null) {
pstmt_tDBOutput_1.setNull(11, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_1.setString(11, lt.BENEFICIARY_PHONE);
}

                        if(lt.BENEFICIARY_ADDRESS == null) {
pstmt_tDBOutput_1.setNull(12, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_1.setString(12, lt.BENEFICIARY_ADDRESS);
}

                        pstmt_tDBOutput_1.setLong(13, lt.REF_CURRENCY);

                        pstmt_tDBOutput_1.setBigDecimal(14, lt.CURRENCY_RATE);

                        if(lt.PAYMENT_REFERENCE_NO == null) {
pstmt_tDBOutput_1.setNull(15, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_1.setString(15, lt.PAYMENT_REFERENCE_NO);
}

                        pstmt_tDBOutput_1.setBigDecimal(16, lt.PAYMENT_AMOUNT);

                        pstmt_tDBOutput_1.setBigDecimal(17, lt.PAYMENT_AMOUNT_BASE);

                        if(lt.DESCRIPTION == null) {
pstmt_tDBOutput_1.setNull(18, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_1.setString(18, lt.DESCRIPTION);
}

                        if(lt.SUPPLIER_INVOICE_NO == null) {
pstmt_tDBOutput_1.setNull(19, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_1.setString(19, lt.SUPPLIER_INVOICE_NO);
}

                        if(lt.SUPPLIER_INVOICE_DATE != null) {
pstmt_tDBOutput_1.setTimestamp(20, new java.sql.Timestamp(lt.SUPPLIER_INVOICE_DATE.getTime()));
} else {
pstmt_tDBOutput_1.setNull(20, java.sql.Types.TIMESTAMP);
}

                        if(lt.SUPPLIER_INVOICE_DUEDATE != null) {
pstmt_tDBOutput_1.setTimestamp(21, new java.sql.Timestamp(lt.SUPPLIER_INVOICE_DUEDATE.getTime()));
} else {
pstmt_tDBOutput_1.setNull(21, java.sql.Types.TIMESTAMP);
}

                        if(lt.REF_SUPPLIER == null) {
pstmt_tDBOutput_1.setNull(22, java.sql.Types.INTEGER);
} else {pstmt_tDBOutput_1.setLong(22, lt.REF_SUPPLIER);
}

                        if(lt.REF_EMPLOYEE == null) {
pstmt_tDBOutput_1.setNull(23, java.sql.Types.INTEGER);
} else {pstmt_tDBOutput_1.setLong(23, lt.REF_EMPLOYEE);
}

                        if(lt.REF_CUSTOMER == null) {
pstmt_tDBOutput_1.setNull(24, java.sql.Types.INTEGER);
} else {pstmt_tDBOutput_1.setLong(24, lt.REF_CUSTOMER);
}

                        if(lt.PAYMENT_STATUS == null) {
pstmt_tDBOutput_1.setNull(25, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_1.setString(25, lt.PAYMENT_STATUS);
}

                        if(lt.REF_ORG_DEPARTMENT == null) {
pstmt_tDBOutput_1.setNull(26, java.sql.Types.INTEGER);
} else {pstmt_tDBOutput_1.setLong(26, lt.REF_ORG_DEPARTMENT);
}

                        pstmt_tDBOutput_1.setLong(27, lt.REF_CLIENT);

                        pstmt_tDBOutput_1.setLong(28, lt.VERSION);

                        pstmt_tDBOutput_1.setLong(29, lt.CREATED_BY);

                        pstmt_tDBOutput_1.setLong(30, lt.UPDATED_BY);

                        if(lt.CREATED_DATE != null) {
pstmt_tDBOutput_1.setTimestamp(31, new java.sql.Timestamp(lt.CREATED_DATE.getTime()));
} else {
pstmt_tDBOutput_1.setNull(31, java.sql.Types.TIMESTAMP);
}

                        if(lt.UPDATED_DATE != null) {
pstmt_tDBOutput_1.setTimestamp(32, new java.sql.Timestamp(lt.UPDATED_DATE.getTime()));
} else {
pstmt_tDBOutput_1.setNull(32, java.sql.Types.TIMESTAMP);
}

                        if(lt.BENEFICIARY_BANK_NAME == null) {
pstmt_tDBOutput_1.setNull(33, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_1.setString(33, lt.BENEFICIARY_BANK_NAME);
}

                        if(lt.BENEFICIARY_BANK_ACCOUNT_DESC == null) {
pstmt_tDBOutput_1.setNull(34, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_1.setString(34, lt.BENEFICIARY_BANK_ACCOUNT_DESC);
}

                        if(lt.REF_WARRANT_LINE == null) {
pstmt_tDBOutput_1.setNull(35, java.sql.Types.INTEGER);
} else {pstmt_tDBOutput_1.setLong(35, lt.REF_WARRANT_LINE);
}

                        if(lt.IPPIS_EMPLOYEE_ID == null) {
pstmt_tDBOutput_1.setNull(36, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_1.setString(36, lt.IPPIS_EMPLOYEE_ID);
}

                        if(lt.IPPIS_BENEFICIARY_CODE == null) {
pstmt_tDBOutput_1.setNull(37, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_1.setString(37, lt.IPPIS_BENEFICIARY_CODE);
}

                        if(lt.IPPIS_PAYMENT_TYPE_CODE == null) {
pstmt_tDBOutput_1.setNull(38, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_1.setString(38, lt.IPPIS_PAYMENT_TYPE_CODE);
}

                        if(lt.LEDGER_STATUS == null) {
pstmt_tDBOutput_1.setNull(39, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_1.setString(39, lt.LEDGER_STATUS);
}

                        pstmt_tDBOutput_1.setLong(40, lt.IN_SETTLEMENT);

                        if(lt.REF_AUTHORIZING_ORG == null) {
pstmt_tDBOutput_1.setNull(41, java.sql.Types.INTEGER);
} else {pstmt_tDBOutput_1.setLong(41, lt.REF_AUTHORIZING_ORG);
}

                        if(lt.REF_FUND_MDA == null) {
pstmt_tDBOutput_1.setNull(42, java.sql.Types.INTEGER);
} else {pstmt_tDBOutput_1.setLong(42, lt.REF_FUND_MDA);
}

                        pstmt_tDBOutput_1.setLong(43, lt.PAYMENT_BATCH_NUMBER);

                        if(lt.REF_ORIGINAL_PAYMENT_ORDER == null) {
pstmt_tDBOutput_1.setNull(44, java.sql.Types.INTEGER);
} else {pstmt_tDBOutput_1.setLong(44, lt.REF_ORIGINAL_PAYMENT_ORDER);
}

                        if(lt.REF_CREDITED_PAYMENT_ORDER == null) {
pstmt_tDBOutput_1.setNull(45, java.sql.Types.INTEGER);
} else {pstmt_tDBOutput_1.setLong(45, lt.REF_CREDITED_PAYMENT_ORDER);
}

                        if(lt.RESENDING_STATUS == null) {
pstmt_tDBOutput_1.setNull(46, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_1.setString(46, lt.RESENDING_STATUS);
}

                        if(lt.IS_REFUND == null) {
pstmt_tDBOutput_1.setNull(47, java.sql.Types.INTEGER);
} else {pstmt_tDBOutput_1.setLong(47, lt.IS_REFUND);
}

                        if(lt.REFUND_REASON == null) {
pstmt_tDBOutput_1.setNull(48, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_1.setString(48, lt.REFUND_REASON);
}

                        if(lt.REFUND_ORIGINATING_DOCUMENT == null) {
pstmt_tDBOutput_1.setNull(49, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_1.setString(49, lt.REFUND_ORIGINATING_DOCUMENT);
}

                        if(lt.etl_date != null) {
pstmt_tDBOutput_1.setTimestamp(50, new java.sql.Timestamp(lt.etl_date.getTime()));
} else {
pstmt_tDBOutput_1.setNull(50, java.sql.Types.TIMESTAMP);
}

            pstmt_tDBOutput_1.addBatch();
            nb_line_tDBOutput_1++;
                batchSizeCounter_tDBOutput_1++;
                if ((batchSize_tDBOutput_1 > 0) && (batchSize_tDBOutput_1 <= batchSizeCounter_tDBOutput_1)) {
                try {
                        pstmt_tDBOutput_1.executeBatch();
                            insertedCount_tDBOutput_1 += pstmt_tDBOutput_1. getUpdateCount();
                        batchSizeCounter_tDBOutput_1 = 0;
                }catch (java.sql.SQLException e){
                        throw(e);
                }
            }
                commitCounter_tDBOutput_1++;
                if(commitEvery_tDBOutput_1 <= commitCounter_tDBOutput_1) {

                try {
                            boolean isCountResult_tDBOutput_1 = false;
                            if(batchSizeCounter_tDBOutput_1 > 0){
                                pstmt_tDBOutput_1.executeBatch();
                                isCountResult_tDBOutput_1 = true;
                                batchSizeCounter_tDBOutput_1 = 0;
                            }
                        if(isCountResult_tDBOutput_1){
                        insertedCount_tDBOutput_1 += pstmt_tDBOutput_1.getUpdateCount();
                        }
                }catch (java.sql.SQLException e){
                        throw(e);

                }
                    conn_tDBOutput_1.commit();
                    commitCounter_tDBOutput_1=0;
                }

 


	tos_count_tDBOutput_1++;

/**
 * [tDBOutput_1 main ] stop
 */
	
	/**
	 * [tDBOutput_1 process_data_begin ] start
	 */

	

	
	
	currentComponent="tDBOutput_1";

	

 



/**
 * [tDBOutput_1 process_data_begin ] stop
 */
	
	/**
	 * [tDBOutput_1 process_data_end ] start
	 */

	

	
	
	currentComponent="tDBOutput_1";

	

 



/**
 * [tDBOutput_1 process_data_end ] stop
 */

} // End of branch "lt"




	
	/**
	 * [tMap_2 process_data_end ] start
	 */

	

	
	
	currentComponent="tMap_2";

	

 



/**
 * [tMap_2 process_data_end ] stop
 */

} // End of branch "row6"




	
	/**
	 * [tFileInputDelimited_1 process_data_end ] start
	 */

	

	
	
	currentComponent="tFileInputDelimited_1";

	

 



/**
 * [tFileInputDelimited_1 process_data_end ] stop
 */
	
	/**
	 * [tFileInputDelimited_1 end ] start
	 */

	

	
	
	currentComponent="tFileInputDelimited_1";

	


				nb_line_tFileInputDelimited_1++;
			}
			
			}finally{
    			if(!(filename_tFileInputDelimited_1 instanceof java.io.InputStream)){
    				if(csvReadertFileInputDelimited_1!=null){
    					csvReadertFileInputDelimited_1.close();
    				}
    			}
    			if(csvReadertFileInputDelimited_1!=null){
    				globalMap.put("tFileInputDelimited_1_NB_LINE",nb_line_tFileInputDelimited_1);
    			}
				
			}
						  

 

ok_Hash.put("tFileInputDelimited_1", true);
end_Hash.put("tFileInputDelimited_1", System.currentTimeMillis());

talendStats_STATS.addMessage("end","tFileInputDelimited_1", end_Hash.get("tFileInputDelimited_1")-start_Hash.get("tFileInputDelimited_1"));
talendStats_STATSProcess(globalMap);



/**
 * [tFileInputDelimited_1 end ] stop
 */

	
	/**
	 * [tMap_2 end ] start
	 */

	

	
	
	currentComponent="tMap_2";

	


// ###############################
// # Lookup hashes releasing
// ###############################      





			if(execStat){
				if(resourceMap.get("inIterateVComp") == null || !((Boolean)resourceMap.get("inIterateVComp"))){
			 		runStat.updateStatOnConnection("row6"+iterateId,2, 0); 
			 	}
			}
		
 

ok_Hash.put("tMap_2", true);
end_Hash.put("tMap_2", System.currentTimeMillis());




/**
 * [tMap_2 end ] stop
 */

	
	/**
	 * [tDBOutput_1 end ] start
	 */

	

	
	
	currentComponent="tDBOutput_1";

	



			if (pstmt_tDBOutput_1 !=null && batchSizeCounter_tDBOutput_1 > 0 ) {  
	try {
			
			pstmt_tDBOutput_1.executeBatch();
			
			
				insertedCount_tDBOutput_1 +=pstmt_tDBOutput_1.getUpdateCount(); 
			
	}catch (java.sql.BatchUpdateException e){
		
			throw(e);
							
	}
	}
	
		
		pstmt_tDBOutput_1 = conn_tDBOutput_1.prepareStatement("COMMIT;");
		pstmt_tDBOutput_1.executeUpdate();
	
			if(pstmt_tDBOutput_1 != null) {
	
				pstmt_tDBOutput_1.close();
				
			}
		
			if (commitCounter_tDBOutput_1 > 0 ) {
				
				conn_tDBOutput_1.commit();
				
			}
				
		
		conn_tDBOutput_1.close();
		
		resourceMap.put("finish_tDBOutput_1", true);
	

	nb_line_deleted_tDBOutput_1=nb_line_deleted_tDBOutput_1+ deletedCount_tDBOutput_1;
	nb_line_update_tDBOutput_1=nb_line_update_tDBOutput_1 + updatedCount_tDBOutput_1;
	nb_line_inserted_tDBOutput_1=nb_line_inserted_tDBOutput_1 + insertedCount_tDBOutput_1;
	nb_line_rejected_tDBOutput_1=nb_line_rejected_tDBOutput_1 + rejectedCount_tDBOutput_1;
	
        globalMap.put("tDBOutput_1_NB_LINE",nb_line_tDBOutput_1);
        globalMap.put("tDBOutput_1_NB_LINE_UPDATED",nb_line_update_tDBOutput_1);
        globalMap.put("tDBOutput_1_NB_LINE_INSERTED",nb_line_inserted_tDBOutput_1);
        globalMap.put("tDBOutput_1_NB_LINE_DELETED",nb_line_deleted_tDBOutput_1);
        globalMap.put("tDBOutput_1_NB_LINE_REJECTED", nb_line_rejected_tDBOutput_1);
    
	

	nb_line_copied_tDBOutput_1=nb_line_copied_tDBOutput_1 + copiedCount_tDBOutput_1;
	globalMap.put("tDBOutput_1_NB_LINE_COPIED",nb_line_copied_tDBOutput_1);

			if(execStat){
				if(resourceMap.get("inIterateVComp") == null || !((Boolean)resourceMap.get("inIterateVComp"))){
			 		runStat.updateStatOnConnection("lt"+iterateId,2, 0); 
			 	}
			}
		
 

ok_Hash.put("tDBOutput_1", true);
end_Hash.put("tDBOutput_1", System.currentTimeMillis());




/**
 * [tDBOutput_1 end ] stop
 */






				}//end the resume

				



	
			}catch(java.lang.Exception e){	
				
				TalendException te = new TalendException(e, currentComponent, globalMap);
				
				throw te;
			}catch(java.lang.Error error){	
				
					runStat.stopThreadStat();
				
				throw error;
			}finally{
				
				try{
					
	
	/**
	 * [tFileInputDelimited_1 finally ] start
	 */

	

	
	
	currentComponent="tFileInputDelimited_1";

	

 



/**
 * [tFileInputDelimited_1 finally ] stop
 */

	
	/**
	 * [tMap_2 finally ] start
	 */

	

	
	
	currentComponent="tMap_2";

	

 



/**
 * [tMap_2 finally ] stop
 */

	
	/**
	 * [tDBOutput_1 finally ] start
	 */

	

	
	
	currentComponent="tDBOutput_1";

	



	
		if(resourceMap.get("finish_tDBOutput_1")==null){
			if(resourceMap.get("conn_tDBOutput_1")!=null){
				try {
					
					
					java.sql.Connection ctn_tDBOutput_1 = (java.sql.Connection)resourceMap.get("conn_tDBOutput_1");
					
					
            		
					ctn_tDBOutput_1.close();
					
				} catch (java.sql.SQLException sqlEx_tDBOutput_1) {
					String errorMessage_tDBOutput_1 = "failed to close the connection in tDBOutput_1 :" + sqlEx_tDBOutput_1.getMessage();
					
					System.err.println(errorMessage_tDBOutput_1);
				}
			}
		}
	

 



/**
 * [tDBOutput_1 finally ] stop
 */






				}catch(java.lang.Exception e){	
					//ignore
				}catch(java.lang.Error error){
					//ignore
				}
				resourceMap = null;
			}
		

		globalMap.put("tFileInputDelimited_1_SUBPROCESS_STATE", 1);
	}
	

public void connectionStatsLogs_CommitProcess(final java.util.Map<String, Object> globalMap) throws TalendException {
	globalMap.put("connectionStatsLogs_Commit_SUBPROCESS_STATE", 0);

 final boolean execStat = this.execStat;
	
		String iterateId = "";
	
	
	String currentComponent = "";
	java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

	try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception().getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { //start the resume
				globalResumeTicket = true;





	
	/**
	 * [connectionStatsLogs_Commit begin ] start
	 */

	

	
		
		ok_Hash.put("connectionStatsLogs_Commit", false);
		start_Hash.put("connectionStatsLogs_Commit", System.currentTimeMillis());
		
	
	currentComponent="connectionStatsLogs_Commit";

	
		int tos_count_connectionStatsLogs_Commit = 0;
		
    	class BytesLimit65535_connectionStatsLogs_Commit{
    		public void limitLog4jByte() throws Exception{
    			
    		}
    	}
    	
        new BytesLimit65535_connectionStatsLogs_Commit().limitLog4jByte();

 



/**
 * [connectionStatsLogs_Commit begin ] stop
 */
	
	/**
	 * [connectionStatsLogs_Commit main ] start
	 */

	

	
	
	currentComponent="connectionStatsLogs_Commit";

	

	java.sql.Connection conn_connectionStatsLogs_Commit = (java.sql.Connection)globalMap.get("conn_connectionStatsLogs");

if(conn_connectionStatsLogs_Commit != null && !conn_connectionStatsLogs_Commit.isClosed()) {
	
			
			conn_connectionStatsLogs_Commit.commit();
			
	
}

 


	tos_count_connectionStatsLogs_Commit++;

/**
 * [connectionStatsLogs_Commit main ] stop
 */
	
	/**
	 * [connectionStatsLogs_Commit process_data_begin ] start
	 */

	

	
	
	currentComponent="connectionStatsLogs_Commit";

	

 



/**
 * [connectionStatsLogs_Commit process_data_begin ] stop
 */
	
	/**
	 * [connectionStatsLogs_Commit process_data_end ] start
	 */

	

	
	
	currentComponent="connectionStatsLogs_Commit";

	

 



/**
 * [connectionStatsLogs_Commit process_data_end ] stop
 */
	
	/**
	 * [connectionStatsLogs_Commit end ] start
	 */

	

	
	
	currentComponent="connectionStatsLogs_Commit";

	

 

ok_Hash.put("connectionStatsLogs_Commit", true);
end_Hash.put("connectionStatsLogs_Commit", System.currentTimeMillis());




/**
 * [connectionStatsLogs_Commit end ] stop
 */
				}//end the resume

				



	
			}catch(java.lang.Exception e){	
				
				TalendException te = new TalendException(e, currentComponent, globalMap);
				
				throw te;
			}catch(java.lang.Error error){	
				
					runStat.stopThreadStat();
				
				throw error;
			}finally{
				
				try{
					
	
	/**
	 * [connectionStatsLogs_Commit finally ] start
	 */

	

	
	
	currentComponent="connectionStatsLogs_Commit";

	

 



/**
 * [connectionStatsLogs_Commit finally ] stop
 */
				}catch(java.lang.Exception e){	
					//ignore
				}catch(java.lang.Error error){
					//ignore
				}
				resourceMap = null;
			}
		

		globalMap.put("connectionStatsLogs_Commit_SUBPROCESS_STATE", 1);
	}
	

public void connectionStatsLogsProcess(final java.util.Map<String, Object> globalMap) throws TalendException {
	globalMap.put("connectionStatsLogs_SUBPROCESS_STATE", 0);

 final boolean execStat = this.execStat;
	
		String iterateId = "";
	
	
	String currentComponent = "";
	java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

	try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception().getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { //start the resume
				globalResumeTicket = true;





	
	/**
	 * [connectionStatsLogs begin ] start
	 */

	

	
		
		ok_Hash.put("connectionStatsLogs", false);
		start_Hash.put("connectionStatsLogs", System.currentTimeMillis());
		
	
	currentComponent="connectionStatsLogs";

	
		int tos_count_connectionStatsLogs = 0;
		
    	class BytesLimit65535_connectionStatsLogs{
    		public void limitLog4jByte() throws Exception{
    			
    		}
    	}
    	
        new BytesLimit65535_connectionStatsLogs().limitLog4jByte();
	

	
		String properties_connectionStatsLogs = "noDatetimeStringSync=true";
		if (properties_connectionStatsLogs == null || properties_connectionStatsLogs.trim().length() == 0) {
			properties_connectionStatsLogs += "rewriteBatchedStatements=true";
		}else if (properties_connectionStatsLogs != null && !properties_connectionStatsLogs.contains("rewriteBatchedStatements")) {
			properties_connectionStatsLogs += "&rewriteBatchedStatements=true";
		}
		
		String url_connectionStatsLogs = "jdbc:mysql://" + "localhost" + ":" + "3306" + "/" + "amc" + "?" + properties_connectionStatsLogs;

	String dbUser_connectionStatsLogs = "etluser";
	
	
		 
	final String decryptedPassword_connectionStatsLogs = routines.system.PasswordEncryptUtil.decryptPassword("72e87cf3a913e55bf4f7aba1746784ea");
		String dbPwd_connectionStatsLogs = decryptedPassword_connectionStatsLogs;
	

	java.sql.Connection conn_connectionStatsLogs = null;
	
	
			String sharedConnectionName_connectionStatsLogs = "StatsAndLog_Shared_Connection";
			conn_connectionStatsLogs = SharedDBConnection.getDBConnection("org.gjt.mm.mysql.Driver",url_connectionStatsLogs,dbUser_connectionStatsLogs , dbPwd_connectionStatsLogs , sharedConnectionName_connectionStatsLogs);
	if (null != conn_connectionStatsLogs) {
		
			conn_connectionStatsLogs.setAutoCommit(false);
	}

	globalMap.put("conn_connectionStatsLogs",conn_connectionStatsLogs);

	globalMap.put("db_connectionStatsLogs","amc");
 



/**
 * [connectionStatsLogs begin ] stop
 */
	
	/**
	 * [connectionStatsLogs main ] start
	 */

	

	
	
	currentComponent="connectionStatsLogs";

	

 


	tos_count_connectionStatsLogs++;

/**
 * [connectionStatsLogs main ] stop
 */
	
	/**
	 * [connectionStatsLogs process_data_begin ] start
	 */

	

	
	
	currentComponent="connectionStatsLogs";

	

 



/**
 * [connectionStatsLogs process_data_begin ] stop
 */
	
	/**
	 * [connectionStatsLogs process_data_end ] start
	 */

	

	
	
	currentComponent="connectionStatsLogs";

	

 



/**
 * [connectionStatsLogs process_data_end ] stop
 */
	
	/**
	 * [connectionStatsLogs end ] start
	 */

	

	
	
	currentComponent="connectionStatsLogs";

	

 

ok_Hash.put("connectionStatsLogs", true);
end_Hash.put("connectionStatsLogs", System.currentTimeMillis());




/**
 * [connectionStatsLogs end ] stop
 */
				}//end the resume

				



	
			}catch(java.lang.Exception e){	
				
				TalendException te = new TalendException(e, currentComponent, globalMap);
				
				throw te;
			}catch(java.lang.Error error){	
				
					runStat.stopThreadStat();
				
				throw error;
			}finally{
				
				try{
					
	
	/**
	 * [connectionStatsLogs finally ] start
	 */

	

	
	
	currentComponent="connectionStatsLogs";

	

 



/**
 * [connectionStatsLogs finally ] stop
 */
				}catch(java.lang.Exception e){	
					//ignore
				}catch(java.lang.Error error){
					//ignore
				}
				resourceMap = null;
			}
		

		globalMap.put("connectionStatsLogs_SUBPROCESS_STATE", 1);
	}
	


public static class row_talendStats_DBStruct implements routines.system.IPersistableRow<row_talendStats_DBStruct> {
    final static byte[] commonByteArrayLock_GIFMIS_payment_order_bulk_2 = new byte[0];
    static byte[] commonByteArray_GIFMIS_payment_order_bulk_2 = new byte[0];

	
			    public java.util.Date moment;

				public java.util.Date getMoment () {
					return this.moment;
				}
				
			    public String pid;

				public String getPid () {
					return this.pid;
				}
				
			    public String father_pid;

				public String getFather_pid () {
					return this.father_pid;
				}
				
			    public String root_pid;

				public String getRoot_pid () {
					return this.root_pid;
				}
				
			    public Long system_pid;

				public Long getSystem_pid () {
					return this.system_pid;
				}
				
			    public String project;

				public String getProject () {
					return this.project;
				}
				
			    public String job;

				public String getJob () {
					return this.job;
				}
				
			    public String job_repository_id;

				public String getJob_repository_id () {
					return this.job_repository_id;
				}
				
			    public String job_version;

				public String getJob_version () {
					return this.job_version;
				}
				
			    public String context;

				public String getContext () {
					return this.context;
				}
				
			    public String origin;

				public String getOrigin () {
					return this.origin;
				}
				
			    public String message_type;

				public String getMessage_type () {
					return this.message_type;
				}
				
			    public String message;

				public String getMessage () {
					return this.message;
				}
				
			    public Long duration;

				public Long getDuration () {
					return this.duration;
				}
				



	private java.util.Date readDate(ObjectInputStream dis) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(dis.readLong());
		}
		return dateReturn;
	}

    private void writeDate(java.util.Date date1, ObjectOutputStream dos) throws IOException{
		if(date1 == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeLong(date1.getTime());
    	}
    }

	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_GIFMIS_payment_order_bulk_2.length) {
				if(length < 1024 && commonByteArray_GIFMIS_payment_order_bulk_2.length == 0) {
   					commonByteArray_GIFMIS_payment_order_bulk_2 = new byte[1024];
				} else {
   					commonByteArray_GIFMIS_payment_order_bulk_2 = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_GIFMIS_payment_order_bulk_2, 0, length);
			strReturn = new String(commonByteArray_GIFMIS_payment_order_bulk_2, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_GIFMIS_payment_order_bulk_2) {

        	try {

        		int length = 0;
		
					this.moment = readDate(dis);
					
					this.pid = readString(dis);
					
					this.father_pid = readString(dis);
					
					this.root_pid = readString(dis);
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.system_pid = null;
           				} else {
           			    	this.system_pid = dis.readLong();
           				}
					
					this.project = readString(dis);
					
					this.job = readString(dis);
					
					this.job_repository_id = readString(dis);
					
					this.job_version = readString(dis);
					
					this.context = readString(dis);
					
					this.origin = readString(dis);
					
					this.message_type = readString(dis);
					
					this.message = readString(dis);
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.duration = null;
           				} else {
           			    	this.duration = dis.readLong();
           				}
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// java.util.Date
				
						writeDate(this.moment,dos);
					
					// String
				
						writeString(this.pid,dos);
					
					// String
				
						writeString(this.father_pid,dos);
					
					// String
				
						writeString(this.root_pid,dos);
					
					// Long
				
						if(this.system_pid == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeLong(this.system_pid);
		            	}
					
					// String
				
						writeString(this.project,dos);
					
					// String
				
						writeString(this.job,dos);
					
					// String
				
						writeString(this.job_repository_id,dos);
					
					// String
				
						writeString(this.job_version,dos);
					
					// String
				
						writeString(this.context,dos);
					
					// String
				
						writeString(this.origin,dos);
					
					// String
				
						writeString(this.message_type,dos);
					
					// String
				
						writeString(this.message,dos);
					
					// Long
				
						if(this.duration == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeLong(this.duration);
		            	}
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("moment="+String.valueOf(moment));
		sb.append(",pid="+pid);
		sb.append(",father_pid="+father_pid);
		sb.append(",root_pid="+root_pid);
		sb.append(",system_pid="+String.valueOf(system_pid));
		sb.append(",project="+project);
		sb.append(",job="+job);
		sb.append(",job_repository_id="+job_repository_id);
		sb.append(",job_version="+job_version);
		sb.append(",context="+context);
		sb.append(",origin="+origin);
		sb.append(",message_type="+message_type);
		sb.append(",message="+message);
		sb.append(",duration="+String.valueOf(duration));
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(row_talendStats_DBStruct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}

public static class row_talendStats_STATSStruct implements routines.system.IPersistableRow<row_talendStats_STATSStruct> {
    final static byte[] commonByteArrayLock_GIFMIS_payment_order_bulk_2 = new byte[0];
    static byte[] commonByteArray_GIFMIS_payment_order_bulk_2 = new byte[0];

	
			    public java.util.Date moment;

				public java.util.Date getMoment () {
					return this.moment;
				}
				
			    public String pid;

				public String getPid () {
					return this.pid;
				}
				
			    public String father_pid;

				public String getFather_pid () {
					return this.father_pid;
				}
				
			    public String root_pid;

				public String getRoot_pid () {
					return this.root_pid;
				}
				
			    public Long system_pid;

				public Long getSystem_pid () {
					return this.system_pid;
				}
				
			    public String project;

				public String getProject () {
					return this.project;
				}
				
			    public String job;

				public String getJob () {
					return this.job;
				}
				
			    public String job_repository_id;

				public String getJob_repository_id () {
					return this.job_repository_id;
				}
				
			    public String job_version;

				public String getJob_version () {
					return this.job_version;
				}
				
			    public String context;

				public String getContext () {
					return this.context;
				}
				
			    public String origin;

				public String getOrigin () {
					return this.origin;
				}
				
			    public String message_type;

				public String getMessage_type () {
					return this.message_type;
				}
				
			    public String message;

				public String getMessage () {
					return this.message;
				}
				
			    public Long duration;

				public Long getDuration () {
					return this.duration;
				}
				



	private java.util.Date readDate(ObjectInputStream dis) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(dis.readLong());
		}
		return dateReturn;
	}

    private void writeDate(java.util.Date date1, ObjectOutputStream dos) throws IOException{
		if(date1 == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeLong(date1.getTime());
    	}
    }

	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_GIFMIS_payment_order_bulk_2.length) {
				if(length < 1024 && commonByteArray_GIFMIS_payment_order_bulk_2.length == 0) {
   					commonByteArray_GIFMIS_payment_order_bulk_2 = new byte[1024];
				} else {
   					commonByteArray_GIFMIS_payment_order_bulk_2 = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_GIFMIS_payment_order_bulk_2, 0, length);
			strReturn = new String(commonByteArray_GIFMIS_payment_order_bulk_2, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_GIFMIS_payment_order_bulk_2) {

        	try {

        		int length = 0;
		
					this.moment = readDate(dis);
					
					this.pid = readString(dis);
					
					this.father_pid = readString(dis);
					
					this.root_pid = readString(dis);
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.system_pid = null;
           				} else {
           			    	this.system_pid = dis.readLong();
           				}
					
					this.project = readString(dis);
					
					this.job = readString(dis);
					
					this.job_repository_id = readString(dis);
					
					this.job_version = readString(dis);
					
					this.context = readString(dis);
					
					this.origin = readString(dis);
					
					this.message_type = readString(dis);
					
					this.message = readString(dis);
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.duration = null;
           				} else {
           			    	this.duration = dis.readLong();
           				}
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// java.util.Date
				
						writeDate(this.moment,dos);
					
					// String
				
						writeString(this.pid,dos);
					
					// String
				
						writeString(this.father_pid,dos);
					
					// String
				
						writeString(this.root_pid,dos);
					
					// Long
				
						if(this.system_pid == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeLong(this.system_pid);
		            	}
					
					// String
				
						writeString(this.project,dos);
					
					// String
				
						writeString(this.job,dos);
					
					// String
				
						writeString(this.job_repository_id,dos);
					
					// String
				
						writeString(this.job_version,dos);
					
					// String
				
						writeString(this.context,dos);
					
					// String
				
						writeString(this.origin,dos);
					
					// String
				
						writeString(this.message_type,dos);
					
					// String
				
						writeString(this.message,dos);
					
					// Long
				
						if(this.duration == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeLong(this.duration);
		            	}
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("moment="+String.valueOf(moment));
		sb.append(",pid="+pid);
		sb.append(",father_pid="+father_pid);
		sb.append(",root_pid="+root_pid);
		sb.append(",system_pid="+String.valueOf(system_pid));
		sb.append(",project="+project);
		sb.append(",job="+job);
		sb.append(",job_repository_id="+job_repository_id);
		sb.append(",job_version="+job_version);
		sb.append(",context="+context);
		sb.append(",origin="+origin);
		sb.append(",message_type="+message_type);
		sb.append(",message="+message);
		sb.append(",duration="+String.valueOf(duration));
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(row_talendStats_STATSStruct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}
public void talendStats_STATSProcess(final java.util.Map<String, Object> globalMap) throws TalendException {
	globalMap.put("talendStats_STATS_SUBPROCESS_STATE", 0);

 final boolean execStat = this.execStat;
		String currentVirtualComponent = null;
	
		String iterateId = "";
	
	
	String currentComponent = "";
	java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

	try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception().getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { //start the resume
				globalResumeTicket = true;



		row_talendStats_STATSStruct row_talendStats_STATS = new row_talendStats_STATSStruct();
row_talendStats_DBStruct row_talendStats_DB = new row_talendStats_DBStruct();





	
	/**
	 * [talendStats_CONSOLE begin ] start
	 */

	

	
		
		ok_Hash.put("talendStats_CONSOLE", false);
		start_Hash.put("talendStats_CONSOLE", System.currentTimeMillis());
		
	
		currentVirtualComponent = "talendStats_CONSOLE";
	
	currentComponent="talendStats_CONSOLE";

	
			if (execStat) {
				if(resourceMap.get("inIterateVComp") == null){
					
						runStat.updateStatOnConnection("Main" + iterateId, 0, 0);
					
				}
			} 

		
		int tos_count_talendStats_CONSOLE = 0;
		
    	class BytesLimit65535_talendStats_CONSOLE{
    		public void limitLog4jByte() throws Exception{
    			
    		}
    	}
    	
        new BytesLimit65535_talendStats_CONSOLE().limitLog4jByte();

	///////////////////////
	
		final String OUTPUT_FIELD_SEPARATOR_talendStats_CONSOLE = "|";
		java.io.PrintStream consoleOut_talendStats_CONSOLE = null;	

 		StringBuilder strBuffer_talendStats_CONSOLE = null;
		int nb_line_talendStats_CONSOLE = 0;
///////////////////////    			



 



/**
 * [talendStats_CONSOLE begin ] stop
 */



	
	/**
	 * [talendStats_DB begin ] start
	 */

	

	
		
		ok_Hash.put("talendStats_DB", false);
		start_Hash.put("talendStats_DB", System.currentTimeMillis());
		
	
		currentVirtualComponent = "talendStats_DB";
	
	currentComponent="talendStats_DB";

	
			if (execStat) {
				if(resourceMap.get("inIterateVComp") == null){
					
						runStat.updateStatOnConnection("Main" + iterateId, 0, 0);
					
				}
			} 

		
		int tos_count_talendStats_DB = 0;
		
    	class BytesLimit65535_talendStats_DB{
    		public void limitLog4jByte() throws Exception{
    			
    		}
    	}
    	
        new BytesLimit65535_talendStats_DB().limitLog4jByte();






int nb_line_talendStats_DB = 0;
int nb_line_update_talendStats_DB = 0;
int nb_line_inserted_talendStats_DB = 0;
int nb_line_deleted_talendStats_DB = 0;
int nb_line_rejected_talendStats_DB = 0;

int deletedCount_talendStats_DB=0;
int updatedCount_talendStats_DB=0;
int insertedCount_talendStats_DB=0;

int rejectedCount_talendStats_DB=0;

String tableName_talendStats_DB = "jobStats";
boolean whetherReject_talendStats_DB = false;

java.util.Calendar calendar_talendStats_DB = java.util.Calendar.getInstance();
calendar_talendStats_DB.set(1, 0, 1, 0, 0, 0);
long year1_talendStats_DB = calendar_talendStats_DB.getTime().getTime();
calendar_talendStats_DB.set(10000, 0, 1, 0, 0, 0);
long year10000_talendStats_DB = calendar_talendStats_DB.getTime().getTime();
long date_talendStats_DB;

java.sql.Connection conn_talendStats_DB = null;
	conn_talendStats_DB = (java.sql.Connection)globalMap.get("conn_connectionStatsLogs");
	

int count_talendStats_DB=0;
    	

                    // [%connection%][checktable][tableName]
                    String keyCheckTable_talendStats_DB = conn_talendStats_DB + "[checktable]" + "[" + "jobStats" + "]";

                if(GlobalResource.resourceMap.get(keyCheckTable_talendStats_DB)== null){//}

                    synchronized (GlobalResource.resourceLockMap.get(keyCheckTable_talendStats_DB)) {//}
                        if(GlobalResource.resourceMap.get(keyCheckTable_talendStats_DB)== null){//}
                                java.sql.DatabaseMetaData dbMetaData_talendStats_DB = conn_talendStats_DB.getMetaData();
                                java.sql.ResultSet rsTable_talendStats_DB = dbMetaData_talendStats_DB.getTables(null, null, null, new String[]{"TABLE"});
                                boolean whetherExist_talendStats_DB = false;
                                while(rsTable_talendStats_DB.next()) {
                                    String table_talendStats_DB = rsTable_talendStats_DB.getString("TABLE_NAME");
                                    if(table_talendStats_DB.equalsIgnoreCase("jobStats")) {
                                        whetherExist_talendStats_DB = true;
                                        break;
                                    }
                                }
                                rsTable_talendStats_DB.close();
                                if(!whetherExist_talendStats_DB) {
                                    java.sql.Statement stmtCreate_talendStats_DB = conn_talendStats_DB.createStatement();
                                        stmtCreate_talendStats_DB.execute("CREATE TABLE `" + tableName_talendStats_DB + "`(`moment` DATETIME ,`pid` VARCHAR(20)  ,`father_pid` VARCHAR(20)  ,`root_pid` VARCHAR(20)  ,`system_pid` BIGINT(8)  ,`project` VARCHAR(50)  ,`job` VARCHAR(255)  ,`job_repository_id` VARCHAR(255)  ,`job_version` VARCHAR(255)  ,`context` VARCHAR(50)  ,`origin` VARCHAR(255)  ,`message_type` VARCHAR(255)  ,`message` VARCHAR(255)  ,`duration` BIGINT(8)  )");
                                    stmtCreate_talendStats_DB.close();
                                }
                            GlobalResource.resourceMap.put(keyCheckTable_talendStats_DB, true);
            //{{{
                        } // end of if
                    } // end synchronized
                }

		        String insert_talendStats_DB = "INSERT INTO `" + "jobStats" + "` (`moment`,`pid`,`father_pid`,`root_pid`,`system_pid`,`project`,`job`,`job_repository_id`,`job_version`,`context`,`origin`,`message_type`,`message`,`duration`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
				    


                		java.sql.PreparedStatement pstmt_talendStats_DB = null;
                        // [%connection%][psmt][tableName]
                        String keyPsmt_talendStats_DB = conn_talendStats_DB + "[psmt]" + "[" + "jobStats" + "]";
                        pstmt_talendStats_DB = SharedDBPreparedStatement.getSharedPreparedStatement(conn_talendStats_DB,insert_talendStats_DB,keyPsmt_talendStats_DB);


 



/**
 * [talendStats_DB begin ] stop
 */



	
	/**
	 * [talendStats_STATS begin ] start
	 */

	

	
		
		ok_Hash.put("talendStats_STATS", false);
		start_Hash.put("talendStats_STATS", System.currentTimeMillis());
		
	
		currentVirtualComponent = "talendStats_STATS";
	
	currentComponent="talendStats_STATS";

	
		int tos_count_talendStats_STATS = 0;
		
    	class BytesLimit65535_talendStats_STATS{
    		public void limitLog4jByte() throws Exception{
    			
    		}
    	}
    	
        new BytesLimit65535_talendStats_STATS().limitLog4jByte();

	for (StatCatcherUtils.StatCatcherMessage scm : talendStats_STATS.getMessages()) {
		row_talendStats_STATS.pid = pid;
		row_talendStats_STATS.root_pid = rootPid;
		row_talendStats_STATS.father_pid = fatherPid;	
    	row_talendStats_STATS.project = projectName;
    	row_talendStats_STATS.job = jobName;
    	row_talendStats_STATS.context = contextStr;
		row_talendStats_STATS.origin = (scm.getOrigin()==null || scm.getOrigin().length()<1 ? null : scm.getOrigin());
		row_talendStats_STATS.message = scm.getMessage();
		row_talendStats_STATS.duration = scm.getDuration();
		row_talendStats_STATS.moment = scm.getMoment();
		row_talendStats_STATS.message_type = scm.getMessageType();
		row_talendStats_STATS.job_version = scm.getJobVersion();
		row_talendStats_STATS.job_repository_id = scm.getJobId();
		row_talendStats_STATS.system_pid = scm.getSystemPid();

 



/**
 * [talendStats_STATS begin ] stop
 */
	
	/**
	 * [talendStats_STATS main ] start
	 */

	

	
	
		currentVirtualComponent = "talendStats_STATS";
	
	currentComponent="talendStats_STATS";

	

 


	tos_count_talendStats_STATS++;

/**
 * [talendStats_STATS main ] stop
 */
	
	/**
	 * [talendStats_STATS process_data_begin ] start
	 */

	

	
	
		currentVirtualComponent = "talendStats_STATS";
	
	currentComponent="talendStats_STATS";

	

 



/**
 * [talendStats_STATS process_data_begin ] stop
 */

	
	/**
	 * [talendStats_DB main ] start
	 */

	

	
	
		currentVirtualComponent = "talendStats_DB";
	
	currentComponent="talendStats_DB";

	

			//Main
			//row_talendStats_STATS


			
				if(execStat){
					runStat.updateStatOnConnection("Main"+iterateId,1, 1);
				} 
			

		



            row_talendStats_DB = null;
        whetherReject_talendStats_DB = false;
                        if(row_talendStats_STATS.moment != null) {
date_talendStats_DB = row_talendStats_STATS.moment.getTime();
if(date_talendStats_DB < year1_talendStats_DB || date_talendStats_DB >= year10000_talendStats_DB) {
pstmt_talendStats_DB.setString(1, "0000-00-00 00:00:00");
} else {pstmt_talendStats_DB.setTimestamp(1, new java.sql.Timestamp(date_talendStats_DB));
}
} else {
pstmt_talendStats_DB.setNull(1, java.sql.Types.DATE);
}

                        if(row_talendStats_STATS.pid == null) {
pstmt_talendStats_DB.setNull(2, java.sql.Types.VARCHAR);
} else {pstmt_talendStats_DB.setString(2, row_talendStats_STATS.pid);
}

                        if(row_talendStats_STATS.father_pid == null) {
pstmt_talendStats_DB.setNull(3, java.sql.Types.VARCHAR);
} else {pstmt_talendStats_DB.setString(3, row_talendStats_STATS.father_pid);
}

                        if(row_talendStats_STATS.root_pid == null) {
pstmt_talendStats_DB.setNull(4, java.sql.Types.VARCHAR);
} else {pstmt_talendStats_DB.setString(4, row_talendStats_STATS.root_pid);
}

                        if(row_talendStats_STATS.system_pid == null) {
pstmt_talendStats_DB.setNull(5, java.sql.Types.INTEGER);
} else {pstmt_talendStats_DB.setLong(5, row_talendStats_STATS.system_pid);
}

                        if(row_talendStats_STATS.project == null) {
pstmt_talendStats_DB.setNull(6, java.sql.Types.VARCHAR);
} else {pstmt_talendStats_DB.setString(6, row_talendStats_STATS.project);
}

                        if(row_talendStats_STATS.job == null) {
pstmt_talendStats_DB.setNull(7, java.sql.Types.VARCHAR);
} else {pstmt_talendStats_DB.setString(7, row_talendStats_STATS.job);
}

                        if(row_talendStats_STATS.job_repository_id == null) {
pstmt_talendStats_DB.setNull(8, java.sql.Types.VARCHAR);
} else {pstmt_talendStats_DB.setString(8, row_talendStats_STATS.job_repository_id);
}

                        if(row_talendStats_STATS.job_version == null) {
pstmt_talendStats_DB.setNull(9, java.sql.Types.VARCHAR);
} else {pstmt_talendStats_DB.setString(9, row_talendStats_STATS.job_version);
}

                        if(row_talendStats_STATS.context == null) {
pstmt_talendStats_DB.setNull(10, java.sql.Types.VARCHAR);
} else {pstmt_talendStats_DB.setString(10, row_talendStats_STATS.context);
}

                        if(row_talendStats_STATS.origin == null) {
pstmt_talendStats_DB.setNull(11, java.sql.Types.VARCHAR);
} else {pstmt_talendStats_DB.setString(11, row_talendStats_STATS.origin);
}

                        if(row_talendStats_STATS.message_type == null) {
pstmt_talendStats_DB.setNull(12, java.sql.Types.VARCHAR);
} else {pstmt_talendStats_DB.setString(12, row_talendStats_STATS.message_type);
}

                        if(row_talendStats_STATS.message == null) {
pstmt_talendStats_DB.setNull(13, java.sql.Types.VARCHAR);
} else {pstmt_talendStats_DB.setString(13, row_talendStats_STATS.message);
}

                        if(row_talendStats_STATS.duration == null) {
pstmt_talendStats_DB.setNull(14, java.sql.Types.INTEGER);
} else {pstmt_talendStats_DB.setLong(14, row_talendStats_STATS.duration);
}

                try {
                    nb_line_talendStats_DB++;
                    insertedCount_talendStats_DB = insertedCount_talendStats_DB + pstmt_talendStats_DB.executeUpdate();
                } catch(java.lang.Exception e) {
                    whetherReject_talendStats_DB = true;
                            System.err.print(e.getMessage());
                }
            if(!whetherReject_talendStats_DB) {
                            row_talendStats_DB = new row_talendStats_DBStruct();
                                row_talendStats_DB.moment = row_talendStats_STATS.moment;
                                row_talendStats_DB.pid = row_talendStats_STATS.pid;
                                row_talendStats_DB.father_pid = row_talendStats_STATS.father_pid;
                                row_talendStats_DB.root_pid = row_talendStats_STATS.root_pid;
                                row_talendStats_DB.system_pid = row_talendStats_STATS.system_pid;
                                row_talendStats_DB.project = row_talendStats_STATS.project;
                                row_talendStats_DB.job = row_talendStats_STATS.job;
                                row_talendStats_DB.job_repository_id = row_talendStats_STATS.job_repository_id;
                                row_talendStats_DB.job_version = row_talendStats_STATS.job_version;
                                row_talendStats_DB.context = row_talendStats_STATS.context;
                                row_talendStats_DB.origin = row_talendStats_STATS.origin;
                                row_talendStats_DB.message_type = row_talendStats_STATS.message_type;
                                row_talendStats_DB.message = row_talendStats_STATS.message;
                                row_talendStats_DB.duration = row_talendStats_STATS.duration;
            }

 


	tos_count_talendStats_DB++;

/**
 * [talendStats_DB main ] stop
 */
	
	/**
	 * [talendStats_DB process_data_begin ] start
	 */

	

	
	
		currentVirtualComponent = "talendStats_DB";
	
	currentComponent="talendStats_DB";

	

 



/**
 * [talendStats_DB process_data_begin ] stop
 */
// Start of branch "row_talendStats_DB"
if(row_talendStats_DB != null) { 



	
	/**
	 * [talendStats_CONSOLE main ] start
	 */

	

	
	
		currentVirtualComponent = "talendStats_CONSOLE";
	
	currentComponent="talendStats_CONSOLE";

	

			//Main
			//row_talendStats_DB


			
				if(execStat){
					runStat.updateStatOnConnection("Main"+iterateId,1, 1);
				} 
			

		
///////////////////////		
						



				strBuffer_talendStats_CONSOLE = new StringBuilder();




   				
	    		if(row_talendStats_DB.moment != null) { //              
                    							
       
				strBuffer_talendStats_CONSOLE.append(
								FormatterUtils.format_Date(row_talendStats_DB.moment, "yyyy-MM-dd HH:mm:ss")				
				);


							
	    		} //  			

    			strBuffer_talendStats_CONSOLE.append("|");
    			


   				
	    		if(row_talendStats_DB.pid != null) { //              
                    							
       
				strBuffer_talendStats_CONSOLE.append(
				                String.valueOf(row_talendStats_DB.pid)							
				);


							
	    		} //  			

    			strBuffer_talendStats_CONSOLE.append("|");
    			


   				
	    		if(row_talendStats_DB.father_pid != null) { //              
                    							
       
				strBuffer_talendStats_CONSOLE.append(
				                String.valueOf(row_talendStats_DB.father_pid)							
				);


							
	    		} //  			

    			strBuffer_talendStats_CONSOLE.append("|");
    			


   				
	    		if(row_talendStats_DB.root_pid != null) { //              
                    							
       
				strBuffer_talendStats_CONSOLE.append(
				                String.valueOf(row_talendStats_DB.root_pid)							
				);


							
	    		} //  			

    			strBuffer_talendStats_CONSOLE.append("|");
    			


   				
	    		if(row_talendStats_DB.system_pid != null) { //              
                    							
       
				strBuffer_talendStats_CONSOLE.append(
				                String.valueOf(row_talendStats_DB.system_pid)							
				);


							
	    		} //  			

    			strBuffer_talendStats_CONSOLE.append("|");
    			


   				
	    		if(row_talendStats_DB.project != null) { //              
                    							
       
				strBuffer_talendStats_CONSOLE.append(
				                String.valueOf(row_talendStats_DB.project)							
				);


							
	    		} //  			

    			strBuffer_talendStats_CONSOLE.append("|");
    			


   				
	    		if(row_talendStats_DB.job != null) { //              
                    							
       
				strBuffer_talendStats_CONSOLE.append(
				                String.valueOf(row_talendStats_DB.job)							
				);


							
	    		} //  			

    			strBuffer_talendStats_CONSOLE.append("|");
    			


   				
	    		if(row_talendStats_DB.job_repository_id != null) { //              
                    							
       
				strBuffer_talendStats_CONSOLE.append(
				                String.valueOf(row_talendStats_DB.job_repository_id)							
				);


							
	    		} //  			

    			strBuffer_talendStats_CONSOLE.append("|");
    			


   				
	    		if(row_talendStats_DB.job_version != null) { //              
                    							
       
				strBuffer_talendStats_CONSOLE.append(
				                String.valueOf(row_talendStats_DB.job_version)							
				);


							
	    		} //  			

    			strBuffer_talendStats_CONSOLE.append("|");
    			


   				
	    		if(row_talendStats_DB.context != null) { //              
                    							
       
				strBuffer_talendStats_CONSOLE.append(
				                String.valueOf(row_talendStats_DB.context)							
				);


							
	    		} //  			

    			strBuffer_talendStats_CONSOLE.append("|");
    			


   				
	    		if(row_talendStats_DB.origin != null) { //              
                    							
       
				strBuffer_talendStats_CONSOLE.append(
				                String.valueOf(row_talendStats_DB.origin)							
				);


							
	    		} //  			

    			strBuffer_talendStats_CONSOLE.append("|");
    			


   				
	    		if(row_talendStats_DB.message_type != null) { //              
                    							
       
				strBuffer_talendStats_CONSOLE.append(
				                String.valueOf(row_talendStats_DB.message_type)							
				);


							
	    		} //  			

    			strBuffer_talendStats_CONSOLE.append("|");
    			


   				
	    		if(row_talendStats_DB.message != null) { //              
                    							
       
				strBuffer_talendStats_CONSOLE.append(
				                String.valueOf(row_talendStats_DB.message)							
				);


							
	    		} //  			

    			strBuffer_talendStats_CONSOLE.append("|");
    			


   				
	    		if(row_talendStats_DB.duration != null) { //              
                    							
       
				strBuffer_talendStats_CONSOLE.append(
				                String.valueOf(row_talendStats_DB.duration)							
				);


							
	    		} //  			
 

                    if (globalMap.get("tLogRow_CONSOLE")!=null)
                    {
                    	consoleOut_talendStats_CONSOLE = (java.io.PrintStream) globalMap.get("tLogRow_CONSOLE");
                    }
                    else
                    {
                    	consoleOut_talendStats_CONSOLE = new java.io.PrintStream(new java.io.BufferedOutputStream(System.out));
                    	globalMap.put("tLogRow_CONSOLE",consoleOut_talendStats_CONSOLE);
                    }
                    consoleOut_talendStats_CONSOLE.println(strBuffer_talendStats_CONSOLE.toString());
                    consoleOut_talendStats_CONSOLE.flush();
                    nb_line_talendStats_CONSOLE++;
//////

//////                    
                    
///////////////////////    			

 


	tos_count_talendStats_CONSOLE++;

/**
 * [talendStats_CONSOLE main ] stop
 */
	
	/**
	 * [talendStats_CONSOLE process_data_begin ] start
	 */

	

	
	
		currentVirtualComponent = "talendStats_CONSOLE";
	
	currentComponent="talendStats_CONSOLE";

	

 



/**
 * [talendStats_CONSOLE process_data_begin ] stop
 */
	
	/**
	 * [talendStats_CONSOLE process_data_end ] start
	 */

	

	
	
		currentVirtualComponent = "talendStats_CONSOLE";
	
	currentComponent="talendStats_CONSOLE";

	

 



/**
 * [talendStats_CONSOLE process_data_end ] stop
 */

} // End of branch "row_talendStats_DB"




	
	/**
	 * [talendStats_DB process_data_end ] start
	 */

	

	
	
		currentVirtualComponent = "talendStats_DB";
	
	currentComponent="talendStats_DB";

	

 



/**
 * [talendStats_DB process_data_end ] stop
 */



	
	/**
	 * [talendStats_STATS process_data_end ] start
	 */

	

	
	
		currentVirtualComponent = "talendStats_STATS";
	
	currentComponent="talendStats_STATS";

	

 



/**
 * [talendStats_STATS process_data_end ] stop
 */
	
	/**
	 * [talendStats_STATS end ] start
	 */

	

	
	
		currentVirtualComponent = "talendStats_STATS";
	
	currentComponent="talendStats_STATS";

	

	}


 

ok_Hash.put("talendStats_STATS", true);
end_Hash.put("talendStats_STATS", System.currentTimeMillis());




/**
 * [talendStats_STATS end ] stop
 */

	
	/**
	 * [talendStats_DB end ] start
	 */

	

	
	
		currentVirtualComponent = "talendStats_DB";
	
	currentComponent="talendStats_DB";

	



	

        if(pstmt_talendStats_DB != null) {
			
				SharedDBPreparedStatement.releasePreparedStatement(keyPsmt_talendStats_DB);
			
        }


	nb_line_deleted_talendStats_DB=nb_line_deleted_talendStats_DB+ deletedCount_talendStats_DB;
	nb_line_update_talendStats_DB=nb_line_update_talendStats_DB + updatedCount_talendStats_DB;
	nb_line_inserted_talendStats_DB=nb_line_inserted_talendStats_DB + insertedCount_talendStats_DB;
	nb_line_rejected_talendStats_DB=nb_line_rejected_talendStats_DB + rejectedCount_talendStats_DB;
	
        globalMap.put("talendStats_DB_NB_LINE",nb_line_talendStats_DB);
        globalMap.put("talendStats_DB_NB_LINE_UPDATED",nb_line_update_talendStats_DB);
        globalMap.put("talendStats_DB_NB_LINE_INSERTED",nb_line_inserted_talendStats_DB);
        globalMap.put("talendStats_DB_NB_LINE_DELETED",nb_line_deleted_talendStats_DB);
        globalMap.put("talendStats_DB_NB_LINE_REJECTED", nb_line_rejected_talendStats_DB);
    
	

			if(execStat){
				if(resourceMap.get("inIterateVComp") == null || !((Boolean)resourceMap.get("inIterateVComp"))){
			 		runStat.updateStatOnConnection("Main"+iterateId,2, 0); 
			 	}
			}
		
 

ok_Hash.put("talendStats_DB", true);
end_Hash.put("talendStats_DB", System.currentTimeMillis());




/**
 * [talendStats_DB end ] stop
 */

	
	/**
	 * [talendStats_CONSOLE end ] start
	 */

	

	
	
		currentVirtualComponent = "talendStats_CONSOLE";
	
	currentComponent="talendStats_CONSOLE";

	


//////
//////
globalMap.put("talendStats_CONSOLE_NB_LINE",nb_line_talendStats_CONSOLE);

///////////////////////    			

			if(execStat){
				if(resourceMap.get("inIterateVComp") == null || !((Boolean)resourceMap.get("inIterateVComp"))){
			 		runStat.updateStatOnConnection("Main"+iterateId,2, 0); 
			 	}
			}
		
 

ok_Hash.put("talendStats_CONSOLE", true);
end_Hash.put("talendStats_CONSOLE", System.currentTimeMillis());




/**
 * [talendStats_CONSOLE end ] stop
 */






				}//end the resume

				
				    			if(resumeEntryMethodName == null || globalResumeTicket){
				    				resumeUtil.addLog("CHECKPOINT", "CONNECTION:SUBJOB_OK:talendStats_STATS:sub_ok_talendStats_connectionStatsLogs_Commit", "", Thread.currentThread().getId() + "", "", "", "", "", "");
								}	    				    			
					    	
								if(execStat){    	
									runStat.updateStatOnConnection("sub_ok_talendStats_connectionStatsLogs_Commit", 0, "ok");
								} 
							
							connectionStatsLogs_CommitProcess(globalMap); 
						



	
			}catch(java.lang.Exception e){	
				
				TalendException te = new TalendException(e, currentComponent, globalMap);
				
					te.setVirtualComponentName(currentVirtualComponent);
				
				throw te;
			}catch(java.lang.Error error){	
				
					runStat.stopThreadStat();
				
				throw error;
			}finally{
				
				try{
					
	
	/**
	 * [talendStats_STATS finally ] start
	 */

	

	
	
		currentVirtualComponent = "talendStats_STATS";
	
	currentComponent="talendStats_STATS";

	

 



/**
 * [talendStats_STATS finally ] stop
 */

	
	/**
	 * [talendStats_DB finally ] start
	 */

	

	
	
		currentVirtualComponent = "talendStats_DB";
	
	currentComponent="talendStats_DB";

	



	

 



/**
 * [talendStats_DB finally ] stop
 */

	
	/**
	 * [talendStats_CONSOLE finally ] start
	 */

	

	
	
		currentVirtualComponent = "talendStats_CONSOLE";
	
	currentComponent="talendStats_CONSOLE";

	

 



/**
 * [talendStats_CONSOLE finally ] stop
 */






				}catch(java.lang.Exception e){	
					//ignore
				}catch(java.lang.Error error){
					//ignore
				}
				resourceMap = null;
			}
		

		globalMap.put("talendStats_STATS_SUBPROCESS_STATE", 1);
	}
	


public static class row_talendLogs_DBStruct implements routines.system.IPersistableRow<row_talendLogs_DBStruct> {
    final static byte[] commonByteArrayLock_GIFMIS_payment_order_bulk_2 = new byte[0];
    static byte[] commonByteArray_GIFMIS_payment_order_bulk_2 = new byte[0];

	
			    public java.util.Date moment;

				public java.util.Date getMoment () {
					return this.moment;
				}
				
			    public String pid;

				public String getPid () {
					return this.pid;
				}
				
			    public String root_pid;

				public String getRoot_pid () {
					return this.root_pid;
				}
				
			    public String father_pid;

				public String getFather_pid () {
					return this.father_pid;
				}
				
			    public String project;

				public String getProject () {
					return this.project;
				}
				
			    public String job;

				public String getJob () {
					return this.job;
				}
				
			    public String context;

				public String getContext () {
					return this.context;
				}
				
			    public Integer priority;

				public Integer getPriority () {
					return this.priority;
				}
				
			    public String type;

				public String getType () {
					return this.type;
				}
				
			    public String origin;

				public String getOrigin () {
					return this.origin;
				}
				
			    public String message;

				public String getMessage () {
					return this.message;
				}
				
			    public Integer code;

				public Integer getCode () {
					return this.code;
				}
				



	private java.util.Date readDate(ObjectInputStream dis) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(dis.readLong());
		}
		return dateReturn;
	}

    private void writeDate(java.util.Date date1, ObjectOutputStream dos) throws IOException{
		if(date1 == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeLong(date1.getTime());
    	}
    }

	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_GIFMIS_payment_order_bulk_2.length) {
				if(length < 1024 && commonByteArray_GIFMIS_payment_order_bulk_2.length == 0) {
   					commonByteArray_GIFMIS_payment_order_bulk_2 = new byte[1024];
				} else {
   					commonByteArray_GIFMIS_payment_order_bulk_2 = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_GIFMIS_payment_order_bulk_2, 0, length);
			strReturn = new String(commonByteArray_GIFMIS_payment_order_bulk_2, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }
	private Integer readInteger(ObjectInputStream dis) throws IOException{
		Integer intReturn;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			intReturn = null;
		} else {
	    	intReturn = dis.readInt();
		}
		return intReturn;
	}

	private void writeInteger(Integer intNum, ObjectOutputStream dos) throws IOException{
		if(intNum == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeInt(intNum);
    	}
	}

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_GIFMIS_payment_order_bulk_2) {

        	try {

        		int length = 0;
		
					this.moment = readDate(dis);
					
					this.pid = readString(dis);
					
					this.root_pid = readString(dis);
					
					this.father_pid = readString(dis);
					
					this.project = readString(dis);
					
					this.job = readString(dis);
					
					this.context = readString(dis);
					
						this.priority = readInteger(dis);
					
					this.type = readString(dis);
					
					this.origin = readString(dis);
					
					this.message = readString(dis);
					
						this.code = readInteger(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// java.util.Date
				
						writeDate(this.moment,dos);
					
					// String
				
						writeString(this.pid,dos);
					
					// String
				
						writeString(this.root_pid,dos);
					
					// String
				
						writeString(this.father_pid,dos);
					
					// String
				
						writeString(this.project,dos);
					
					// String
				
						writeString(this.job,dos);
					
					// String
				
						writeString(this.context,dos);
					
					// Integer
				
						writeInteger(this.priority,dos);
					
					// String
				
						writeString(this.type,dos);
					
					// String
				
						writeString(this.origin,dos);
					
					// String
				
						writeString(this.message,dos);
					
					// Integer
				
						writeInteger(this.code,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("moment="+String.valueOf(moment));
		sb.append(",pid="+pid);
		sb.append(",root_pid="+root_pid);
		sb.append(",father_pid="+father_pid);
		sb.append(",project="+project);
		sb.append(",job="+job);
		sb.append(",context="+context);
		sb.append(",priority="+String.valueOf(priority));
		sb.append(",type="+type);
		sb.append(",origin="+origin);
		sb.append(",message="+message);
		sb.append(",code="+String.valueOf(code));
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(row_talendLogs_DBStruct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}

public static class row_talendLogs_LOGSStruct implements routines.system.IPersistableRow<row_talendLogs_LOGSStruct> {
    final static byte[] commonByteArrayLock_GIFMIS_payment_order_bulk_2 = new byte[0];
    static byte[] commonByteArray_GIFMIS_payment_order_bulk_2 = new byte[0];

	
			    public java.util.Date moment;

				public java.util.Date getMoment () {
					return this.moment;
				}
				
			    public String pid;

				public String getPid () {
					return this.pid;
				}
				
			    public String root_pid;

				public String getRoot_pid () {
					return this.root_pid;
				}
				
			    public String father_pid;

				public String getFather_pid () {
					return this.father_pid;
				}
				
			    public String project;

				public String getProject () {
					return this.project;
				}
				
			    public String job;

				public String getJob () {
					return this.job;
				}
				
			    public String context;

				public String getContext () {
					return this.context;
				}
				
			    public Integer priority;

				public Integer getPriority () {
					return this.priority;
				}
				
			    public String type;

				public String getType () {
					return this.type;
				}
				
			    public String origin;

				public String getOrigin () {
					return this.origin;
				}
				
			    public String message;

				public String getMessage () {
					return this.message;
				}
				
			    public Integer code;

				public Integer getCode () {
					return this.code;
				}
				



	private java.util.Date readDate(ObjectInputStream dis) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(dis.readLong());
		}
		return dateReturn;
	}

    private void writeDate(java.util.Date date1, ObjectOutputStream dos) throws IOException{
		if(date1 == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeLong(date1.getTime());
    	}
    }

	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_GIFMIS_payment_order_bulk_2.length) {
				if(length < 1024 && commonByteArray_GIFMIS_payment_order_bulk_2.length == 0) {
   					commonByteArray_GIFMIS_payment_order_bulk_2 = new byte[1024];
				} else {
   					commonByteArray_GIFMIS_payment_order_bulk_2 = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_GIFMIS_payment_order_bulk_2, 0, length);
			strReturn = new String(commonByteArray_GIFMIS_payment_order_bulk_2, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }
	private Integer readInteger(ObjectInputStream dis) throws IOException{
		Integer intReturn;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			intReturn = null;
		} else {
	    	intReturn = dis.readInt();
		}
		return intReturn;
	}

	private void writeInteger(Integer intNum, ObjectOutputStream dos) throws IOException{
		if(intNum == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeInt(intNum);
    	}
	}

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_GIFMIS_payment_order_bulk_2) {

        	try {

        		int length = 0;
		
					this.moment = readDate(dis);
					
					this.pid = readString(dis);
					
					this.root_pid = readString(dis);
					
					this.father_pid = readString(dis);
					
					this.project = readString(dis);
					
					this.job = readString(dis);
					
					this.context = readString(dis);
					
						this.priority = readInteger(dis);
					
					this.type = readString(dis);
					
					this.origin = readString(dis);
					
					this.message = readString(dis);
					
						this.code = readInteger(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// java.util.Date
				
						writeDate(this.moment,dos);
					
					// String
				
						writeString(this.pid,dos);
					
					// String
				
						writeString(this.root_pid,dos);
					
					// String
				
						writeString(this.father_pid,dos);
					
					// String
				
						writeString(this.project,dos);
					
					// String
				
						writeString(this.job,dos);
					
					// String
				
						writeString(this.context,dos);
					
					// Integer
				
						writeInteger(this.priority,dos);
					
					// String
				
						writeString(this.type,dos);
					
					// String
				
						writeString(this.origin,dos);
					
					// String
				
						writeString(this.message,dos);
					
					// Integer
				
						writeInteger(this.code,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("moment="+String.valueOf(moment));
		sb.append(",pid="+pid);
		sb.append(",root_pid="+root_pid);
		sb.append(",father_pid="+father_pid);
		sb.append(",project="+project);
		sb.append(",job="+job);
		sb.append(",context="+context);
		sb.append(",priority="+String.valueOf(priority));
		sb.append(",type="+type);
		sb.append(",origin="+origin);
		sb.append(",message="+message);
		sb.append(",code="+String.valueOf(code));
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(row_talendLogs_LOGSStruct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}
public void talendLogs_LOGSProcess(final java.util.Map<String, Object> globalMap) throws TalendException {
	globalMap.put("talendLogs_LOGS_SUBPROCESS_STATE", 0);

 final boolean execStat = this.execStat;
		String currentVirtualComponent = null;
	
		String iterateId = "";
	
	
	String currentComponent = "";
	java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

	try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception().getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { //start the resume
				globalResumeTicket = true;



		row_talendLogs_LOGSStruct row_talendLogs_LOGS = new row_talendLogs_LOGSStruct();
row_talendLogs_DBStruct row_talendLogs_DB = new row_talendLogs_DBStruct();





	
	/**
	 * [talendLogs_CONSOLE begin ] start
	 */

	

	
		
		ok_Hash.put("talendLogs_CONSOLE", false);
		start_Hash.put("talendLogs_CONSOLE", System.currentTimeMillis());
		
	
		currentVirtualComponent = "talendLogs_CONSOLE";
	
	currentComponent="talendLogs_CONSOLE";

	
			if (execStat) {
				if(resourceMap.get("inIterateVComp") == null){
					
						runStat.updateStatOnConnection("Main" + iterateId, 0, 0);
					
				}
			} 

		
		int tos_count_talendLogs_CONSOLE = 0;
		
    	class BytesLimit65535_talendLogs_CONSOLE{
    		public void limitLog4jByte() throws Exception{
    			
    		}
    	}
    	
        new BytesLimit65535_talendLogs_CONSOLE().limitLog4jByte();

	///////////////////////
	
		final String OUTPUT_FIELD_SEPARATOR_talendLogs_CONSOLE = "|";
		java.io.PrintStream consoleOut_talendLogs_CONSOLE = null;	

 		StringBuilder strBuffer_talendLogs_CONSOLE = null;
		int nb_line_talendLogs_CONSOLE = 0;
///////////////////////    			



 



/**
 * [talendLogs_CONSOLE begin ] stop
 */



	
	/**
	 * [talendLogs_DB begin ] start
	 */

	

	
		
		ok_Hash.put("talendLogs_DB", false);
		start_Hash.put("talendLogs_DB", System.currentTimeMillis());
		
	
		currentVirtualComponent = "talendLogs_DB";
	
	currentComponent="talendLogs_DB";

	
			if (execStat) {
				if(resourceMap.get("inIterateVComp") == null){
					
						runStat.updateStatOnConnection("Main" + iterateId, 0, 0);
					
				}
			} 

		
		int tos_count_talendLogs_DB = 0;
		
    	class BytesLimit65535_talendLogs_DB{
    		public void limitLog4jByte() throws Exception{
    			
    		}
    	}
    	
        new BytesLimit65535_talendLogs_DB().limitLog4jByte();






int nb_line_talendLogs_DB = 0;
int nb_line_update_talendLogs_DB = 0;
int nb_line_inserted_talendLogs_DB = 0;
int nb_line_deleted_talendLogs_DB = 0;
int nb_line_rejected_talendLogs_DB = 0;

int deletedCount_talendLogs_DB=0;
int updatedCount_talendLogs_DB=0;
int insertedCount_talendLogs_DB=0;

int rejectedCount_talendLogs_DB=0;

String tableName_talendLogs_DB = "jobLogs";
boolean whetherReject_talendLogs_DB = false;

java.util.Calendar calendar_talendLogs_DB = java.util.Calendar.getInstance();
calendar_talendLogs_DB.set(1, 0, 1, 0, 0, 0);
long year1_talendLogs_DB = calendar_talendLogs_DB.getTime().getTime();
calendar_talendLogs_DB.set(10000, 0, 1, 0, 0, 0);
long year10000_talendLogs_DB = calendar_talendLogs_DB.getTime().getTime();
long date_talendLogs_DB;

java.sql.Connection conn_talendLogs_DB = null;
	conn_talendLogs_DB = (java.sql.Connection)globalMap.get("conn_connectionStatsLogs");
	

int count_talendLogs_DB=0;
    	

                    // [%connection%][checktable][tableName]
                    String keyCheckTable_talendLogs_DB = conn_talendLogs_DB + "[checktable]" + "[" + "jobLogs" + "]";

                if(GlobalResource.resourceMap.get(keyCheckTable_talendLogs_DB)== null){//}

                    synchronized (GlobalResource.resourceLockMap.get(keyCheckTable_talendLogs_DB)) {//}
                        if(GlobalResource.resourceMap.get(keyCheckTable_talendLogs_DB)== null){//}
                                java.sql.DatabaseMetaData dbMetaData_talendLogs_DB = conn_talendLogs_DB.getMetaData();
                                java.sql.ResultSet rsTable_talendLogs_DB = dbMetaData_talendLogs_DB.getTables(null, null, null, new String[]{"TABLE"});
                                boolean whetherExist_talendLogs_DB = false;
                                while(rsTable_talendLogs_DB.next()) {
                                    String table_talendLogs_DB = rsTable_talendLogs_DB.getString("TABLE_NAME");
                                    if(table_talendLogs_DB.equalsIgnoreCase("jobLogs")) {
                                        whetherExist_talendLogs_DB = true;
                                        break;
                                    }
                                }
                                rsTable_talendLogs_DB.close();
                                if(!whetherExist_talendLogs_DB) {
                                    java.sql.Statement stmtCreate_talendLogs_DB = conn_talendLogs_DB.createStatement();
                                        stmtCreate_talendLogs_DB.execute("CREATE TABLE `" + tableName_talendLogs_DB + "`(`moment` DATETIME ,`pid` VARCHAR(20)  ,`root_pid` VARCHAR(20)  ,`father_pid` VARCHAR(20)  ,`project` VARCHAR(50)  ,`job` VARCHAR(255)  ,`context` VARCHAR(50)  ,`priority` INT(3)  ,`type` VARCHAR(255)  ,`origin` VARCHAR(255)  ,`message` VARCHAR(255)  ,`code` INT(3)  )");
                                    stmtCreate_talendLogs_DB.close();
                                }
                            GlobalResource.resourceMap.put(keyCheckTable_talendLogs_DB, true);
            //{{{
                        } // end of if
                    } // end synchronized
                }

		        String insert_talendLogs_DB = "INSERT INTO `" + "jobLogs" + "` (`moment`,`pid`,`root_pid`,`father_pid`,`project`,`job`,`context`,`priority`,`type`,`origin`,`message`,`code`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
				    


                		java.sql.PreparedStatement pstmt_talendLogs_DB = null;
                        // [%connection%][psmt][tableName]
                        String keyPsmt_talendLogs_DB = conn_talendLogs_DB + "[psmt]" + "[" + "jobLogs" + "]";
                        pstmt_talendLogs_DB = SharedDBPreparedStatement.getSharedPreparedStatement(conn_talendLogs_DB,insert_talendLogs_DB,keyPsmt_talendLogs_DB);


 



/**
 * [talendLogs_DB begin ] stop
 */



	
	/**
	 * [talendLogs_LOGS begin ] start
	 */

	

	
		
		ok_Hash.put("talendLogs_LOGS", false);
		start_Hash.put("talendLogs_LOGS", System.currentTimeMillis());
		
	
		currentVirtualComponent = "talendLogs_LOGS";
	
	currentComponent="talendLogs_LOGS";

	
		int tos_count_talendLogs_LOGS = 0;
		
    	class BytesLimit65535_talendLogs_LOGS{
    		public void limitLog4jByte() throws Exception{
    			
    		}
    	}
    	
        new BytesLimit65535_talendLogs_LOGS().limitLog4jByte();

try {
	for (LogCatcherUtils.LogCatcherMessage lcm : talendLogs_LOGS.getMessages()) {
		row_talendLogs_LOGS.type = lcm.getType();
		row_talendLogs_LOGS.origin = (lcm.getOrigin()==null || lcm.getOrigin().length()<1 ? null : lcm.getOrigin());
		row_talendLogs_LOGS.priority = lcm.getPriority();
		row_talendLogs_LOGS.message = lcm.getMessage();
		row_talendLogs_LOGS.code = lcm.getCode();
		
		row_talendLogs_LOGS.moment = java.util.Calendar.getInstance().getTime();
	
    	row_talendLogs_LOGS.pid = pid;
		row_talendLogs_LOGS.root_pid = rootPid;
		row_talendLogs_LOGS.father_pid = fatherPid;
	
    	row_talendLogs_LOGS.project = projectName;
    	row_talendLogs_LOGS.job = jobName;
    	row_talendLogs_LOGS.context = contextStr;
    		
 



/**
 * [talendLogs_LOGS begin ] stop
 */
	
	/**
	 * [talendLogs_LOGS main ] start
	 */

	

	
	
		currentVirtualComponent = "talendLogs_LOGS";
	
	currentComponent="talendLogs_LOGS";

	

 


	tos_count_talendLogs_LOGS++;

/**
 * [talendLogs_LOGS main ] stop
 */
	
	/**
	 * [talendLogs_LOGS process_data_begin ] start
	 */

	

	
	
		currentVirtualComponent = "talendLogs_LOGS";
	
	currentComponent="talendLogs_LOGS";

	

 



/**
 * [talendLogs_LOGS process_data_begin ] stop
 */

	
	/**
	 * [talendLogs_DB main ] start
	 */

	

	
	
		currentVirtualComponent = "talendLogs_DB";
	
	currentComponent="talendLogs_DB";

	

			//Main
			//row_talendLogs_LOGS


			
				if(execStat){
					runStat.updateStatOnConnection("Main"+iterateId,1, 1);
				} 
			

		



            row_talendLogs_DB = null;
        whetherReject_talendLogs_DB = false;
                        if(row_talendLogs_LOGS.moment != null) {
date_talendLogs_DB = row_talendLogs_LOGS.moment.getTime();
if(date_talendLogs_DB < year1_talendLogs_DB || date_talendLogs_DB >= year10000_talendLogs_DB) {
pstmt_talendLogs_DB.setString(1, "0000-00-00 00:00:00");
} else {pstmt_talendLogs_DB.setTimestamp(1, new java.sql.Timestamp(date_talendLogs_DB));
}
} else {
pstmt_talendLogs_DB.setNull(1, java.sql.Types.DATE);
}

                        if(row_talendLogs_LOGS.pid == null) {
pstmt_talendLogs_DB.setNull(2, java.sql.Types.VARCHAR);
} else {pstmt_talendLogs_DB.setString(2, row_talendLogs_LOGS.pid);
}

                        if(row_talendLogs_LOGS.root_pid == null) {
pstmt_talendLogs_DB.setNull(3, java.sql.Types.VARCHAR);
} else {pstmt_talendLogs_DB.setString(3, row_talendLogs_LOGS.root_pid);
}

                        if(row_talendLogs_LOGS.father_pid == null) {
pstmt_talendLogs_DB.setNull(4, java.sql.Types.VARCHAR);
} else {pstmt_talendLogs_DB.setString(4, row_talendLogs_LOGS.father_pid);
}

                        if(row_talendLogs_LOGS.project == null) {
pstmt_talendLogs_DB.setNull(5, java.sql.Types.VARCHAR);
} else {pstmt_talendLogs_DB.setString(5, row_talendLogs_LOGS.project);
}

                        if(row_talendLogs_LOGS.job == null) {
pstmt_talendLogs_DB.setNull(6, java.sql.Types.VARCHAR);
} else {pstmt_talendLogs_DB.setString(6, row_talendLogs_LOGS.job);
}

                        if(row_talendLogs_LOGS.context == null) {
pstmt_talendLogs_DB.setNull(7, java.sql.Types.VARCHAR);
} else {pstmt_talendLogs_DB.setString(7, row_talendLogs_LOGS.context);
}

                        if(row_talendLogs_LOGS.priority == null) {
pstmt_talendLogs_DB.setNull(8, java.sql.Types.INTEGER);
} else {pstmt_talendLogs_DB.setInt(8, row_talendLogs_LOGS.priority);
}

                        if(row_talendLogs_LOGS.type == null) {
pstmt_talendLogs_DB.setNull(9, java.sql.Types.VARCHAR);
} else {pstmt_talendLogs_DB.setString(9, row_talendLogs_LOGS.type);
}

                        if(row_talendLogs_LOGS.origin == null) {
pstmt_talendLogs_DB.setNull(10, java.sql.Types.VARCHAR);
} else {pstmt_talendLogs_DB.setString(10, row_talendLogs_LOGS.origin);
}

                        if(row_talendLogs_LOGS.message == null) {
pstmt_talendLogs_DB.setNull(11, java.sql.Types.VARCHAR);
} else {pstmt_talendLogs_DB.setString(11, row_talendLogs_LOGS.message);
}

                        if(row_talendLogs_LOGS.code == null) {
pstmt_talendLogs_DB.setNull(12, java.sql.Types.INTEGER);
} else {pstmt_talendLogs_DB.setInt(12, row_talendLogs_LOGS.code);
}

                try {
                    nb_line_talendLogs_DB++;
                    insertedCount_talendLogs_DB = insertedCount_talendLogs_DB + pstmt_talendLogs_DB.executeUpdate();
                } catch(java.lang.Exception e) {
                    whetherReject_talendLogs_DB = true;
                            System.err.print(e.getMessage());
                }
            if(!whetherReject_talendLogs_DB) {
                            row_talendLogs_DB = new row_talendLogs_DBStruct();
                                row_talendLogs_DB.moment = row_talendLogs_LOGS.moment;
                                row_talendLogs_DB.pid = row_talendLogs_LOGS.pid;
                                row_talendLogs_DB.root_pid = row_talendLogs_LOGS.root_pid;
                                row_talendLogs_DB.father_pid = row_talendLogs_LOGS.father_pid;
                                row_talendLogs_DB.project = row_talendLogs_LOGS.project;
                                row_talendLogs_DB.job = row_talendLogs_LOGS.job;
                                row_talendLogs_DB.context = row_talendLogs_LOGS.context;
                                row_talendLogs_DB.priority = row_talendLogs_LOGS.priority;
                                row_talendLogs_DB.type = row_talendLogs_LOGS.type;
                                row_talendLogs_DB.origin = row_talendLogs_LOGS.origin;
                                row_talendLogs_DB.message = row_talendLogs_LOGS.message;
                                row_talendLogs_DB.code = row_talendLogs_LOGS.code;
            }

 


	tos_count_talendLogs_DB++;

/**
 * [talendLogs_DB main ] stop
 */
	
	/**
	 * [talendLogs_DB process_data_begin ] start
	 */

	

	
	
		currentVirtualComponent = "talendLogs_DB";
	
	currentComponent="talendLogs_DB";

	

 



/**
 * [talendLogs_DB process_data_begin ] stop
 */
// Start of branch "row_talendLogs_DB"
if(row_talendLogs_DB != null) { 



	
	/**
	 * [talendLogs_CONSOLE main ] start
	 */

	

	
	
		currentVirtualComponent = "talendLogs_CONSOLE";
	
	currentComponent="talendLogs_CONSOLE";

	

			//Main
			//row_talendLogs_DB


			
				if(execStat){
					runStat.updateStatOnConnection("Main"+iterateId,1, 1);
				} 
			

		
///////////////////////		
						



				strBuffer_talendLogs_CONSOLE = new StringBuilder();




   				
	    		if(row_talendLogs_DB.moment != null) { //              
                    							
       
				strBuffer_talendLogs_CONSOLE.append(
								FormatterUtils.format_Date(row_talendLogs_DB.moment, "yyyy-MM-dd HH:mm:ss")				
				);


							
	    		} //  			

    			strBuffer_talendLogs_CONSOLE.append("|");
    			


   				
	    		if(row_talendLogs_DB.pid != null) { //              
                    							
       
				strBuffer_talendLogs_CONSOLE.append(
				                String.valueOf(row_talendLogs_DB.pid)							
				);


							
	    		} //  			

    			strBuffer_talendLogs_CONSOLE.append("|");
    			


   				
	    		if(row_talendLogs_DB.root_pid != null) { //              
                    							
       
				strBuffer_talendLogs_CONSOLE.append(
				                String.valueOf(row_talendLogs_DB.root_pid)							
				);


							
	    		} //  			

    			strBuffer_talendLogs_CONSOLE.append("|");
    			


   				
	    		if(row_talendLogs_DB.father_pid != null) { //              
                    							
       
				strBuffer_talendLogs_CONSOLE.append(
				                String.valueOf(row_talendLogs_DB.father_pid)							
				);


							
	    		} //  			

    			strBuffer_talendLogs_CONSOLE.append("|");
    			


   				
	    		if(row_talendLogs_DB.project != null) { //              
                    							
       
				strBuffer_talendLogs_CONSOLE.append(
				                String.valueOf(row_talendLogs_DB.project)							
				);


							
	    		} //  			

    			strBuffer_talendLogs_CONSOLE.append("|");
    			


   				
	    		if(row_talendLogs_DB.job != null) { //              
                    							
       
				strBuffer_talendLogs_CONSOLE.append(
				                String.valueOf(row_talendLogs_DB.job)							
				);


							
	    		} //  			

    			strBuffer_talendLogs_CONSOLE.append("|");
    			


   				
	    		if(row_talendLogs_DB.context != null) { //              
                    							
       
				strBuffer_talendLogs_CONSOLE.append(
				                String.valueOf(row_talendLogs_DB.context)							
				);


							
	    		} //  			

    			strBuffer_talendLogs_CONSOLE.append("|");
    			


   				
	    		if(row_talendLogs_DB.priority != null) { //              
                    							
       
				strBuffer_talendLogs_CONSOLE.append(
				                String.valueOf(row_talendLogs_DB.priority)							
				);


							
	    		} //  			

    			strBuffer_talendLogs_CONSOLE.append("|");
    			


   				
	    		if(row_talendLogs_DB.type != null) { //              
                    							
       
				strBuffer_talendLogs_CONSOLE.append(
				                String.valueOf(row_talendLogs_DB.type)							
				);


							
	    		} //  			

    			strBuffer_talendLogs_CONSOLE.append("|");
    			


   				
	    		if(row_talendLogs_DB.origin != null) { //              
                    							
       
				strBuffer_talendLogs_CONSOLE.append(
				                String.valueOf(row_talendLogs_DB.origin)							
				);


							
	    		} //  			

    			strBuffer_talendLogs_CONSOLE.append("|");
    			


   				
	    		if(row_talendLogs_DB.message != null) { //              
                    							
       
				strBuffer_talendLogs_CONSOLE.append(
				                String.valueOf(row_talendLogs_DB.message)							
				);


							
	    		} //  			

    			strBuffer_talendLogs_CONSOLE.append("|");
    			


   				
	    		if(row_talendLogs_DB.code != null) { //              
                    							
       
				strBuffer_talendLogs_CONSOLE.append(
				                String.valueOf(row_talendLogs_DB.code)							
				);


							
	    		} //  			
 

                    if (globalMap.get("tLogRow_CONSOLE")!=null)
                    {
                    	consoleOut_talendLogs_CONSOLE = (java.io.PrintStream) globalMap.get("tLogRow_CONSOLE");
                    }
                    else
                    {
                    	consoleOut_talendLogs_CONSOLE = new java.io.PrintStream(new java.io.BufferedOutputStream(System.out));
                    	globalMap.put("tLogRow_CONSOLE",consoleOut_talendLogs_CONSOLE);
                    }
                    consoleOut_talendLogs_CONSOLE.println(strBuffer_talendLogs_CONSOLE.toString());
                    consoleOut_talendLogs_CONSOLE.flush();
                    nb_line_talendLogs_CONSOLE++;
//////

//////                    
                    
///////////////////////    			

 


	tos_count_talendLogs_CONSOLE++;

/**
 * [talendLogs_CONSOLE main ] stop
 */
	
	/**
	 * [talendLogs_CONSOLE process_data_begin ] start
	 */

	

	
	
		currentVirtualComponent = "talendLogs_CONSOLE";
	
	currentComponent="talendLogs_CONSOLE";

	

 



/**
 * [talendLogs_CONSOLE process_data_begin ] stop
 */
	
	/**
	 * [talendLogs_CONSOLE process_data_end ] start
	 */

	

	
	
		currentVirtualComponent = "talendLogs_CONSOLE";
	
	currentComponent="talendLogs_CONSOLE";

	

 



/**
 * [talendLogs_CONSOLE process_data_end ] stop
 */

} // End of branch "row_talendLogs_DB"




	
	/**
	 * [talendLogs_DB process_data_end ] start
	 */

	

	
	
		currentVirtualComponent = "talendLogs_DB";
	
	currentComponent="talendLogs_DB";

	

 



/**
 * [talendLogs_DB process_data_end ] stop
 */



	
	/**
	 * [talendLogs_LOGS process_data_end ] start
	 */

	

	
	
		currentVirtualComponent = "talendLogs_LOGS";
	
	currentComponent="talendLogs_LOGS";

	

 



/**
 * [talendLogs_LOGS process_data_end ] stop
 */
	
	/**
	 * [talendLogs_LOGS end ] start
	 */

	

	
	
		currentVirtualComponent = "talendLogs_LOGS";
	
	currentComponent="talendLogs_LOGS";

	
	}
} catch (Exception e_talendLogs_LOGS) {
	logIgnoredError(String.format("talendLogs_LOGS - tLogCatcher failed to process log message(s) due to internal error: %s", e_talendLogs_LOGS), e_talendLogs_LOGS);
}

 

ok_Hash.put("talendLogs_LOGS", true);
end_Hash.put("talendLogs_LOGS", System.currentTimeMillis());




/**
 * [talendLogs_LOGS end ] stop
 */

	
	/**
	 * [talendLogs_DB end ] start
	 */

	

	
	
		currentVirtualComponent = "talendLogs_DB";
	
	currentComponent="talendLogs_DB";

	



	

        if(pstmt_talendLogs_DB != null) {
			
				SharedDBPreparedStatement.releasePreparedStatement(keyPsmt_talendLogs_DB);
			
        }


	nb_line_deleted_talendLogs_DB=nb_line_deleted_talendLogs_DB+ deletedCount_talendLogs_DB;
	nb_line_update_talendLogs_DB=nb_line_update_talendLogs_DB + updatedCount_talendLogs_DB;
	nb_line_inserted_talendLogs_DB=nb_line_inserted_talendLogs_DB + insertedCount_talendLogs_DB;
	nb_line_rejected_talendLogs_DB=nb_line_rejected_talendLogs_DB + rejectedCount_talendLogs_DB;
	
        globalMap.put("talendLogs_DB_NB_LINE",nb_line_talendLogs_DB);
        globalMap.put("talendLogs_DB_NB_LINE_UPDATED",nb_line_update_talendLogs_DB);
        globalMap.put("talendLogs_DB_NB_LINE_INSERTED",nb_line_inserted_talendLogs_DB);
        globalMap.put("talendLogs_DB_NB_LINE_DELETED",nb_line_deleted_talendLogs_DB);
        globalMap.put("talendLogs_DB_NB_LINE_REJECTED", nb_line_rejected_talendLogs_DB);
    
	

			if(execStat){
				if(resourceMap.get("inIterateVComp") == null || !((Boolean)resourceMap.get("inIterateVComp"))){
			 		runStat.updateStatOnConnection("Main"+iterateId,2, 0); 
			 	}
			}
		
 

ok_Hash.put("talendLogs_DB", true);
end_Hash.put("talendLogs_DB", System.currentTimeMillis());




/**
 * [talendLogs_DB end ] stop
 */

	
	/**
	 * [talendLogs_CONSOLE end ] start
	 */

	

	
	
		currentVirtualComponent = "talendLogs_CONSOLE";
	
	currentComponent="talendLogs_CONSOLE";

	


//////
//////
globalMap.put("talendLogs_CONSOLE_NB_LINE",nb_line_talendLogs_CONSOLE);

///////////////////////    			

			if(execStat){
				if(resourceMap.get("inIterateVComp") == null || !((Boolean)resourceMap.get("inIterateVComp"))){
			 		runStat.updateStatOnConnection("Main"+iterateId,2, 0); 
			 	}
			}
		
 

ok_Hash.put("talendLogs_CONSOLE", true);
end_Hash.put("talendLogs_CONSOLE", System.currentTimeMillis());




/**
 * [talendLogs_CONSOLE end ] stop
 */






				}//end the resume

				
				    			if(resumeEntryMethodName == null || globalResumeTicket){
				    				resumeUtil.addLog("CHECKPOINT", "CONNECTION:SUBJOB_OK:talendLogs_LOGS:sub_ok_talendLogs_connectionStatsLogs_Commit", "", Thread.currentThread().getId() + "", "", "", "", "", "");
								}	    				    			
					    	
								if(execStat){    	
									runStat.updateStatOnConnection("sub_ok_talendLogs_connectionStatsLogs_Commit", 0, "ok");
								} 
							
							connectionStatsLogs_CommitProcess(globalMap); 
						



	
			}catch(java.lang.Exception e){	
				
				TalendException te = new TalendException(e, currentComponent, globalMap);
				
					te.setVirtualComponentName(currentVirtualComponent);
				
				throw te;
			}catch(java.lang.Error error){	
				
					runStat.stopThreadStat();
				
				throw error;
			}finally{
				
				try{
					
	
	/**
	 * [talendLogs_LOGS finally ] start
	 */

	

	
	
		currentVirtualComponent = "talendLogs_LOGS";
	
	currentComponent="talendLogs_LOGS";

	

 



/**
 * [talendLogs_LOGS finally ] stop
 */

	
	/**
	 * [talendLogs_DB finally ] start
	 */

	

	
	
		currentVirtualComponent = "talendLogs_DB";
	
	currentComponent="talendLogs_DB";

	



	

 



/**
 * [talendLogs_DB finally ] stop
 */

	
	/**
	 * [talendLogs_CONSOLE finally ] start
	 */

	

	
	
		currentVirtualComponent = "talendLogs_CONSOLE";
	
	currentComponent="talendLogs_CONSOLE";

	

 



/**
 * [talendLogs_CONSOLE finally ] stop
 */






				}catch(java.lang.Exception e){	
					//ignore
				}catch(java.lang.Error error){
					//ignore
				}
				resourceMap = null;
			}
		

		globalMap.put("talendLogs_LOGS_SUBPROCESS_STATE", 1);
	}
	


public static class row_talendMeter_DBStruct implements routines.system.IPersistableRow<row_talendMeter_DBStruct> {
    final static byte[] commonByteArrayLock_GIFMIS_payment_order_bulk_2 = new byte[0];
    static byte[] commonByteArray_GIFMIS_payment_order_bulk_2 = new byte[0];

	
			    public java.util.Date moment;

				public java.util.Date getMoment () {
					return this.moment;
				}
				
			    public String pid;

				public String getPid () {
					return this.pid;
				}
				
			    public String father_pid;

				public String getFather_pid () {
					return this.father_pid;
				}
				
			    public String root_pid;

				public String getRoot_pid () {
					return this.root_pid;
				}
				
			    public Long system_pid;

				public Long getSystem_pid () {
					return this.system_pid;
				}
				
			    public String project;

				public String getProject () {
					return this.project;
				}
				
			    public String job;

				public String getJob () {
					return this.job;
				}
				
			    public String job_repository_id;

				public String getJob_repository_id () {
					return this.job_repository_id;
				}
				
			    public String job_version;

				public String getJob_version () {
					return this.job_version;
				}
				
			    public String context;

				public String getContext () {
					return this.context;
				}
				
			    public String origin;

				public String getOrigin () {
					return this.origin;
				}
				
			    public String label;

				public String getLabel () {
					return this.label;
				}
				
			    public Integer count;

				public Integer getCount () {
					return this.count;
				}
				
			    public Integer reference;

				public Integer getReference () {
					return this.reference;
				}
				
			    public String thresholds;

				public String getThresholds () {
					return this.thresholds;
				}
				



	private java.util.Date readDate(ObjectInputStream dis) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(dis.readLong());
		}
		return dateReturn;
	}

    private void writeDate(java.util.Date date1, ObjectOutputStream dos) throws IOException{
		if(date1 == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeLong(date1.getTime());
    	}
    }

	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_GIFMIS_payment_order_bulk_2.length) {
				if(length < 1024 && commonByteArray_GIFMIS_payment_order_bulk_2.length == 0) {
   					commonByteArray_GIFMIS_payment_order_bulk_2 = new byte[1024];
				} else {
   					commonByteArray_GIFMIS_payment_order_bulk_2 = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_GIFMIS_payment_order_bulk_2, 0, length);
			strReturn = new String(commonByteArray_GIFMIS_payment_order_bulk_2, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }
	private Integer readInteger(ObjectInputStream dis) throws IOException{
		Integer intReturn;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			intReturn = null;
		} else {
	    	intReturn = dis.readInt();
		}
		return intReturn;
	}

	private void writeInteger(Integer intNum, ObjectOutputStream dos) throws IOException{
		if(intNum == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeInt(intNum);
    	}
	}

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_GIFMIS_payment_order_bulk_2) {

        	try {

        		int length = 0;
		
					this.moment = readDate(dis);
					
					this.pid = readString(dis);
					
					this.father_pid = readString(dis);
					
					this.root_pid = readString(dis);
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.system_pid = null;
           				} else {
           			    	this.system_pid = dis.readLong();
           				}
					
					this.project = readString(dis);
					
					this.job = readString(dis);
					
					this.job_repository_id = readString(dis);
					
					this.job_version = readString(dis);
					
					this.context = readString(dis);
					
					this.origin = readString(dis);
					
					this.label = readString(dis);
					
						this.count = readInteger(dis);
					
						this.reference = readInteger(dis);
					
					this.thresholds = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// java.util.Date
				
						writeDate(this.moment,dos);
					
					// String
				
						writeString(this.pid,dos);
					
					// String
				
						writeString(this.father_pid,dos);
					
					// String
				
						writeString(this.root_pid,dos);
					
					// Long
				
						if(this.system_pid == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeLong(this.system_pid);
		            	}
					
					// String
				
						writeString(this.project,dos);
					
					// String
				
						writeString(this.job,dos);
					
					// String
				
						writeString(this.job_repository_id,dos);
					
					// String
				
						writeString(this.job_version,dos);
					
					// String
				
						writeString(this.context,dos);
					
					// String
				
						writeString(this.origin,dos);
					
					// String
				
						writeString(this.label,dos);
					
					// Integer
				
						writeInteger(this.count,dos);
					
					// Integer
				
						writeInteger(this.reference,dos);
					
					// String
				
						writeString(this.thresholds,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("moment="+String.valueOf(moment));
		sb.append(",pid="+pid);
		sb.append(",father_pid="+father_pid);
		sb.append(",root_pid="+root_pid);
		sb.append(",system_pid="+String.valueOf(system_pid));
		sb.append(",project="+project);
		sb.append(",job="+job);
		sb.append(",job_repository_id="+job_repository_id);
		sb.append(",job_version="+job_version);
		sb.append(",context="+context);
		sb.append(",origin="+origin);
		sb.append(",label="+label);
		sb.append(",count="+String.valueOf(count));
		sb.append(",reference="+String.valueOf(reference));
		sb.append(",thresholds="+thresholds);
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(row_talendMeter_DBStruct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}

public static class row_talendMeter_METTERStruct implements routines.system.IPersistableRow<row_talendMeter_METTERStruct> {
    final static byte[] commonByteArrayLock_GIFMIS_payment_order_bulk_2 = new byte[0];
    static byte[] commonByteArray_GIFMIS_payment_order_bulk_2 = new byte[0];

	
			    public java.util.Date moment;

				public java.util.Date getMoment () {
					return this.moment;
				}
				
			    public String pid;

				public String getPid () {
					return this.pid;
				}
				
			    public String father_pid;

				public String getFather_pid () {
					return this.father_pid;
				}
				
			    public String root_pid;

				public String getRoot_pid () {
					return this.root_pid;
				}
				
			    public Long system_pid;

				public Long getSystem_pid () {
					return this.system_pid;
				}
				
			    public String project;

				public String getProject () {
					return this.project;
				}
				
			    public String job;

				public String getJob () {
					return this.job;
				}
				
			    public String job_repository_id;

				public String getJob_repository_id () {
					return this.job_repository_id;
				}
				
			    public String job_version;

				public String getJob_version () {
					return this.job_version;
				}
				
			    public String context;

				public String getContext () {
					return this.context;
				}
				
			    public String origin;

				public String getOrigin () {
					return this.origin;
				}
				
			    public String label;

				public String getLabel () {
					return this.label;
				}
				
			    public Integer count;

				public Integer getCount () {
					return this.count;
				}
				
			    public Integer reference;

				public Integer getReference () {
					return this.reference;
				}
				
			    public String thresholds;

				public String getThresholds () {
					return this.thresholds;
				}
				



	private java.util.Date readDate(ObjectInputStream dis) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(dis.readLong());
		}
		return dateReturn;
	}

    private void writeDate(java.util.Date date1, ObjectOutputStream dos) throws IOException{
		if(date1 == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeLong(date1.getTime());
    	}
    }

	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_GIFMIS_payment_order_bulk_2.length) {
				if(length < 1024 && commonByteArray_GIFMIS_payment_order_bulk_2.length == 0) {
   					commonByteArray_GIFMIS_payment_order_bulk_2 = new byte[1024];
				} else {
   					commonByteArray_GIFMIS_payment_order_bulk_2 = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_GIFMIS_payment_order_bulk_2, 0, length);
			strReturn = new String(commonByteArray_GIFMIS_payment_order_bulk_2, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }
	private Integer readInteger(ObjectInputStream dis) throws IOException{
		Integer intReturn;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			intReturn = null;
		} else {
	    	intReturn = dis.readInt();
		}
		return intReturn;
	}

	private void writeInteger(Integer intNum, ObjectOutputStream dos) throws IOException{
		if(intNum == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeInt(intNum);
    	}
	}

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_GIFMIS_payment_order_bulk_2) {

        	try {

        		int length = 0;
		
					this.moment = readDate(dis);
					
					this.pid = readString(dis);
					
					this.father_pid = readString(dis);
					
					this.root_pid = readString(dis);
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.system_pid = null;
           				} else {
           			    	this.system_pid = dis.readLong();
           				}
					
					this.project = readString(dis);
					
					this.job = readString(dis);
					
					this.job_repository_id = readString(dis);
					
					this.job_version = readString(dis);
					
					this.context = readString(dis);
					
					this.origin = readString(dis);
					
					this.label = readString(dis);
					
						this.count = readInteger(dis);
					
						this.reference = readInteger(dis);
					
					this.thresholds = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// java.util.Date
				
						writeDate(this.moment,dos);
					
					// String
				
						writeString(this.pid,dos);
					
					// String
				
						writeString(this.father_pid,dos);
					
					// String
				
						writeString(this.root_pid,dos);
					
					// Long
				
						if(this.system_pid == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeLong(this.system_pid);
		            	}
					
					// String
				
						writeString(this.project,dos);
					
					// String
				
						writeString(this.job,dos);
					
					// String
				
						writeString(this.job_repository_id,dos);
					
					// String
				
						writeString(this.job_version,dos);
					
					// String
				
						writeString(this.context,dos);
					
					// String
				
						writeString(this.origin,dos);
					
					// String
				
						writeString(this.label,dos);
					
					// Integer
				
						writeInteger(this.count,dos);
					
					// Integer
				
						writeInteger(this.reference,dos);
					
					// String
				
						writeString(this.thresholds,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("moment="+String.valueOf(moment));
		sb.append(",pid="+pid);
		sb.append(",father_pid="+father_pid);
		sb.append(",root_pid="+root_pid);
		sb.append(",system_pid="+String.valueOf(system_pid));
		sb.append(",project="+project);
		sb.append(",job="+job);
		sb.append(",job_repository_id="+job_repository_id);
		sb.append(",job_version="+job_version);
		sb.append(",context="+context);
		sb.append(",origin="+origin);
		sb.append(",label="+label);
		sb.append(",count="+String.valueOf(count));
		sb.append(",reference="+String.valueOf(reference));
		sb.append(",thresholds="+thresholds);
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(row_talendMeter_METTERStruct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}
public void talendMeter_METTERProcess(final java.util.Map<String, Object> globalMap) throws TalendException {
	globalMap.put("talendMeter_METTER_SUBPROCESS_STATE", 0);

 final boolean execStat = this.execStat;
		String currentVirtualComponent = null;
	
		String iterateId = "";
	
	
	String currentComponent = "";
	java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

	try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception().getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { //start the resume
				globalResumeTicket = true;



		row_talendMeter_METTERStruct row_talendMeter_METTER = new row_talendMeter_METTERStruct();
row_talendMeter_DBStruct row_talendMeter_DB = new row_talendMeter_DBStruct();





	
	/**
	 * [talendMeter_CONSOLE begin ] start
	 */

	

	
		
		ok_Hash.put("talendMeter_CONSOLE", false);
		start_Hash.put("talendMeter_CONSOLE", System.currentTimeMillis());
		
	
		currentVirtualComponent = "talendMeter_CONSOLE";
	
	currentComponent="talendMeter_CONSOLE";

	
			if (execStat) {
				if(resourceMap.get("inIterateVComp") == null){
					
						runStat.updateStatOnConnection("Main" + iterateId, 0, 0);
					
				}
			} 

		
		int tos_count_talendMeter_CONSOLE = 0;
		
    	class BytesLimit65535_talendMeter_CONSOLE{
    		public void limitLog4jByte() throws Exception{
    			
    		}
    	}
    	
        new BytesLimit65535_talendMeter_CONSOLE().limitLog4jByte();

	///////////////////////
	
		final String OUTPUT_FIELD_SEPARATOR_talendMeter_CONSOLE = "|";
		java.io.PrintStream consoleOut_talendMeter_CONSOLE = null;	

 		StringBuilder strBuffer_talendMeter_CONSOLE = null;
		int nb_line_talendMeter_CONSOLE = 0;
///////////////////////    			



 



/**
 * [talendMeter_CONSOLE begin ] stop
 */



	
	/**
	 * [talendMeter_DB begin ] start
	 */

	

	
		
		ok_Hash.put("talendMeter_DB", false);
		start_Hash.put("talendMeter_DB", System.currentTimeMillis());
		
	
		currentVirtualComponent = "talendMeter_DB";
	
	currentComponent="talendMeter_DB";

	
			if (execStat) {
				if(resourceMap.get("inIterateVComp") == null){
					
						runStat.updateStatOnConnection("Main" + iterateId, 0, 0);
					
				}
			} 

		
		int tos_count_talendMeter_DB = 0;
		
    	class BytesLimit65535_talendMeter_DB{
    		public void limitLog4jByte() throws Exception{
    			
    		}
    	}
    	
        new BytesLimit65535_talendMeter_DB().limitLog4jByte();






int nb_line_talendMeter_DB = 0;
int nb_line_update_talendMeter_DB = 0;
int nb_line_inserted_talendMeter_DB = 0;
int nb_line_deleted_talendMeter_DB = 0;
int nb_line_rejected_talendMeter_DB = 0;

int deletedCount_talendMeter_DB=0;
int updatedCount_talendMeter_DB=0;
int insertedCount_talendMeter_DB=0;

int rejectedCount_talendMeter_DB=0;

String tableName_talendMeter_DB = "jobMeters";
boolean whetherReject_talendMeter_DB = false;

java.util.Calendar calendar_talendMeter_DB = java.util.Calendar.getInstance();
calendar_talendMeter_DB.set(1, 0, 1, 0, 0, 0);
long year1_talendMeter_DB = calendar_talendMeter_DB.getTime().getTime();
calendar_talendMeter_DB.set(10000, 0, 1, 0, 0, 0);
long year10000_talendMeter_DB = calendar_talendMeter_DB.getTime().getTime();
long date_talendMeter_DB;

java.sql.Connection conn_talendMeter_DB = null;
	conn_talendMeter_DB = (java.sql.Connection)globalMap.get("conn_connectionStatsLogs");
	

int count_talendMeter_DB=0;
    	

                    // [%connection%][checktable][tableName]
                    String keyCheckTable_talendMeter_DB = conn_talendMeter_DB + "[checktable]" + "[" + "jobMeters" + "]";

                if(GlobalResource.resourceMap.get(keyCheckTable_talendMeter_DB)== null){//}

                    synchronized (GlobalResource.resourceLockMap.get(keyCheckTable_talendMeter_DB)) {//}
                        if(GlobalResource.resourceMap.get(keyCheckTable_talendMeter_DB)== null){//}
                                java.sql.DatabaseMetaData dbMetaData_talendMeter_DB = conn_talendMeter_DB.getMetaData();
                                java.sql.ResultSet rsTable_talendMeter_DB = dbMetaData_talendMeter_DB.getTables(null, null, null, new String[]{"TABLE"});
                                boolean whetherExist_talendMeter_DB = false;
                                while(rsTable_talendMeter_DB.next()) {
                                    String table_talendMeter_DB = rsTable_talendMeter_DB.getString("TABLE_NAME");
                                    if(table_talendMeter_DB.equalsIgnoreCase("jobMeters")) {
                                        whetherExist_talendMeter_DB = true;
                                        break;
                                    }
                                }
                                rsTable_talendMeter_DB.close();
                                if(!whetherExist_talendMeter_DB) {
                                    java.sql.Statement stmtCreate_talendMeter_DB = conn_talendMeter_DB.createStatement();
                                        stmtCreate_talendMeter_DB.execute("CREATE TABLE `" + tableName_talendMeter_DB + "`(`moment` DATETIME ,`pid` VARCHAR(20)  ,`father_pid` VARCHAR(20)  ,`root_pid` VARCHAR(20)  ,`system_pid` BIGINT(8)  ,`project` VARCHAR(50)  ,`job` VARCHAR(255)  ,`job_repository_id` VARCHAR(255)  ,`job_version` VARCHAR(255)  ,`context` VARCHAR(50)  ,`origin` VARCHAR(255)  ,`label` VARCHAR(255)  ,`count` INT(3)  ,`reference` INT(3)  ,`thresholds` VARCHAR(255)  )");
                                    stmtCreate_talendMeter_DB.close();
                                }
                            GlobalResource.resourceMap.put(keyCheckTable_talendMeter_DB, true);
            //{{{
                        } // end of if
                    } // end synchronized
                }

		        String insert_talendMeter_DB = "INSERT INTO `" + "jobMeters" + "` (`moment`,`pid`,`father_pid`,`root_pid`,`system_pid`,`project`,`job`,`job_repository_id`,`job_version`,`context`,`origin`,`label`,`count`,`reference`,`thresholds`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
				    


                		java.sql.PreparedStatement pstmt_talendMeter_DB = null;
                        // [%connection%][psmt][tableName]
                        String keyPsmt_talendMeter_DB = conn_talendMeter_DB + "[psmt]" + "[" + "jobMeters" + "]";
                        pstmt_talendMeter_DB = SharedDBPreparedStatement.getSharedPreparedStatement(conn_talendMeter_DB,insert_talendMeter_DB,keyPsmt_talendMeter_DB);


 



/**
 * [talendMeter_DB begin ] stop
 */



	
	/**
	 * [talendMeter_METTER begin ] start
	 */

	

	
		
		ok_Hash.put("talendMeter_METTER", false);
		start_Hash.put("talendMeter_METTER", System.currentTimeMillis());
		
	
		currentVirtualComponent = "talendMeter_METTER";
	
	currentComponent="talendMeter_METTER";

	
		int tos_count_talendMeter_METTER = 0;
		
    	class BytesLimit65535_talendMeter_METTER{
    		public void limitLog4jByte() throws Exception{
    			
    		}
    	}
    	
        new BytesLimit65535_talendMeter_METTER().limitLog4jByte();

	for (MetterCatcherUtils.MetterCatcherMessage mcm : talendMeter_METTER.getMessages()) {
		row_talendMeter_METTER.pid = pid;
		row_talendMeter_METTER.root_pid = rootPid;
		row_talendMeter_METTER.father_pid = fatherPid;	
        row_talendMeter_METTER.project = projectName;
        row_talendMeter_METTER.job = jobName;
        row_talendMeter_METTER.context = contextStr;
		row_talendMeter_METTER.origin = (mcm.getOrigin()==null || mcm.getOrigin().length()<1 ? null : mcm.getOrigin());
		row_talendMeter_METTER.moment = mcm.getMoment();
		row_talendMeter_METTER.job_version = mcm.getJobVersion();
		row_talendMeter_METTER.job_repository_id = mcm.getJobId();
		row_talendMeter_METTER.system_pid = mcm.getSystemPid();
		row_talendMeter_METTER.label = mcm.getLabel();
		row_talendMeter_METTER.count = mcm.getCount();
		row_talendMeter_METTER.reference = talendMeter_METTER.getConnLinesCount(mcm.getReferense()+"_count");
		row_talendMeter_METTER.thresholds = mcm.getThresholds();
		

 



/**
 * [talendMeter_METTER begin ] stop
 */
	
	/**
	 * [talendMeter_METTER main ] start
	 */

	

	
	
		currentVirtualComponent = "talendMeter_METTER";
	
	currentComponent="talendMeter_METTER";

	

 


	tos_count_talendMeter_METTER++;

/**
 * [talendMeter_METTER main ] stop
 */
	
	/**
	 * [talendMeter_METTER process_data_begin ] start
	 */

	

	
	
		currentVirtualComponent = "talendMeter_METTER";
	
	currentComponent="talendMeter_METTER";

	

 



/**
 * [talendMeter_METTER process_data_begin ] stop
 */

	
	/**
	 * [talendMeter_DB main ] start
	 */

	

	
	
		currentVirtualComponent = "talendMeter_DB";
	
	currentComponent="talendMeter_DB";

	

			//Main
			//row_talendMeter_METTER


			
				if(execStat){
					runStat.updateStatOnConnection("Main"+iterateId,1, 1);
				} 
			

		



            row_talendMeter_DB = null;
        whetherReject_talendMeter_DB = false;
                        if(row_talendMeter_METTER.moment != null) {
date_talendMeter_DB = row_talendMeter_METTER.moment.getTime();
if(date_talendMeter_DB < year1_talendMeter_DB || date_talendMeter_DB >= year10000_talendMeter_DB) {
pstmt_talendMeter_DB.setString(1, "0000-00-00 00:00:00");
} else {pstmt_talendMeter_DB.setTimestamp(1, new java.sql.Timestamp(date_talendMeter_DB));
}
} else {
pstmt_talendMeter_DB.setNull(1, java.sql.Types.DATE);
}

                        if(row_talendMeter_METTER.pid == null) {
pstmt_talendMeter_DB.setNull(2, java.sql.Types.VARCHAR);
} else {pstmt_talendMeter_DB.setString(2, row_talendMeter_METTER.pid);
}

                        if(row_talendMeter_METTER.father_pid == null) {
pstmt_talendMeter_DB.setNull(3, java.sql.Types.VARCHAR);
} else {pstmt_talendMeter_DB.setString(3, row_talendMeter_METTER.father_pid);
}

                        if(row_talendMeter_METTER.root_pid == null) {
pstmt_talendMeter_DB.setNull(4, java.sql.Types.VARCHAR);
} else {pstmt_talendMeter_DB.setString(4, row_talendMeter_METTER.root_pid);
}

                        if(row_talendMeter_METTER.system_pid == null) {
pstmt_talendMeter_DB.setNull(5, java.sql.Types.INTEGER);
} else {pstmt_talendMeter_DB.setLong(5, row_talendMeter_METTER.system_pid);
}

                        if(row_talendMeter_METTER.project == null) {
pstmt_talendMeter_DB.setNull(6, java.sql.Types.VARCHAR);
} else {pstmt_talendMeter_DB.setString(6, row_talendMeter_METTER.project);
}

                        if(row_talendMeter_METTER.job == null) {
pstmt_talendMeter_DB.setNull(7, java.sql.Types.VARCHAR);
} else {pstmt_talendMeter_DB.setString(7, row_talendMeter_METTER.job);
}

                        if(row_talendMeter_METTER.job_repository_id == null) {
pstmt_talendMeter_DB.setNull(8, java.sql.Types.VARCHAR);
} else {pstmt_talendMeter_DB.setString(8, row_talendMeter_METTER.job_repository_id);
}

                        if(row_talendMeter_METTER.job_version == null) {
pstmt_talendMeter_DB.setNull(9, java.sql.Types.VARCHAR);
} else {pstmt_talendMeter_DB.setString(9, row_talendMeter_METTER.job_version);
}

                        if(row_talendMeter_METTER.context == null) {
pstmt_talendMeter_DB.setNull(10, java.sql.Types.VARCHAR);
} else {pstmt_talendMeter_DB.setString(10, row_talendMeter_METTER.context);
}

                        if(row_talendMeter_METTER.origin == null) {
pstmt_talendMeter_DB.setNull(11, java.sql.Types.VARCHAR);
} else {pstmt_talendMeter_DB.setString(11, row_talendMeter_METTER.origin);
}

                        if(row_talendMeter_METTER.label == null) {
pstmt_talendMeter_DB.setNull(12, java.sql.Types.VARCHAR);
} else {pstmt_talendMeter_DB.setString(12, row_talendMeter_METTER.label);
}

                        if(row_talendMeter_METTER.count == null) {
pstmt_talendMeter_DB.setNull(13, java.sql.Types.INTEGER);
} else {pstmt_talendMeter_DB.setInt(13, row_talendMeter_METTER.count);
}

                        if(row_talendMeter_METTER.reference == null) {
pstmt_talendMeter_DB.setNull(14, java.sql.Types.INTEGER);
} else {pstmt_talendMeter_DB.setInt(14, row_talendMeter_METTER.reference);
}

                        if(row_talendMeter_METTER.thresholds == null) {
pstmt_talendMeter_DB.setNull(15, java.sql.Types.VARCHAR);
} else {pstmt_talendMeter_DB.setString(15, row_talendMeter_METTER.thresholds);
}

                try {
                    nb_line_talendMeter_DB++;
                    insertedCount_talendMeter_DB = insertedCount_talendMeter_DB + pstmt_talendMeter_DB.executeUpdate();
                } catch(java.lang.Exception e) {
                    whetherReject_talendMeter_DB = true;
                            System.err.print(e.getMessage());
                }
            if(!whetherReject_talendMeter_DB) {
                            row_talendMeter_DB = new row_talendMeter_DBStruct();
                                row_talendMeter_DB.moment = row_talendMeter_METTER.moment;
                                row_talendMeter_DB.pid = row_talendMeter_METTER.pid;
                                row_talendMeter_DB.father_pid = row_talendMeter_METTER.father_pid;
                                row_talendMeter_DB.root_pid = row_talendMeter_METTER.root_pid;
                                row_talendMeter_DB.system_pid = row_talendMeter_METTER.system_pid;
                                row_talendMeter_DB.project = row_talendMeter_METTER.project;
                                row_talendMeter_DB.job = row_talendMeter_METTER.job;
                                row_talendMeter_DB.job_repository_id = row_talendMeter_METTER.job_repository_id;
                                row_talendMeter_DB.job_version = row_talendMeter_METTER.job_version;
                                row_talendMeter_DB.context = row_talendMeter_METTER.context;
                                row_talendMeter_DB.origin = row_talendMeter_METTER.origin;
                                row_talendMeter_DB.label = row_talendMeter_METTER.label;
                                row_talendMeter_DB.count = row_talendMeter_METTER.count;
                                row_talendMeter_DB.reference = row_talendMeter_METTER.reference;
                                row_talendMeter_DB.thresholds = row_talendMeter_METTER.thresholds;
            }

 


	tos_count_talendMeter_DB++;

/**
 * [talendMeter_DB main ] stop
 */
	
	/**
	 * [talendMeter_DB process_data_begin ] start
	 */

	

	
	
		currentVirtualComponent = "talendMeter_DB";
	
	currentComponent="talendMeter_DB";

	

 



/**
 * [talendMeter_DB process_data_begin ] stop
 */
// Start of branch "row_talendMeter_DB"
if(row_talendMeter_DB != null) { 



	
	/**
	 * [talendMeter_CONSOLE main ] start
	 */

	

	
	
		currentVirtualComponent = "talendMeter_CONSOLE";
	
	currentComponent="talendMeter_CONSOLE";

	

			//Main
			//row_talendMeter_DB


			
				if(execStat){
					runStat.updateStatOnConnection("Main"+iterateId,1, 1);
				} 
			

		
///////////////////////		
						



				strBuffer_talendMeter_CONSOLE = new StringBuilder();




   				
	    		if(row_talendMeter_DB.moment != null) { //              
                    							
       
				strBuffer_talendMeter_CONSOLE.append(
								FormatterUtils.format_Date(row_talendMeter_DB.moment, "yyyy-MM-dd HH:mm:ss")				
				);


							
	    		} //  			

    			strBuffer_talendMeter_CONSOLE.append("|");
    			


   				
	    		if(row_talendMeter_DB.pid != null) { //              
                    							
       
				strBuffer_talendMeter_CONSOLE.append(
				                String.valueOf(row_talendMeter_DB.pid)							
				);


							
	    		} //  			

    			strBuffer_talendMeter_CONSOLE.append("|");
    			


   				
	    		if(row_talendMeter_DB.father_pid != null) { //              
                    							
       
				strBuffer_talendMeter_CONSOLE.append(
				                String.valueOf(row_talendMeter_DB.father_pid)							
				);


							
	    		} //  			

    			strBuffer_talendMeter_CONSOLE.append("|");
    			


   				
	    		if(row_talendMeter_DB.root_pid != null) { //              
                    							
       
				strBuffer_talendMeter_CONSOLE.append(
				                String.valueOf(row_talendMeter_DB.root_pid)							
				);


							
	    		} //  			

    			strBuffer_talendMeter_CONSOLE.append("|");
    			


   				
	    		if(row_talendMeter_DB.system_pid != null) { //              
                    							
       
				strBuffer_talendMeter_CONSOLE.append(
				                String.valueOf(row_talendMeter_DB.system_pid)							
				);


							
	    		} //  			

    			strBuffer_talendMeter_CONSOLE.append("|");
    			


   				
	    		if(row_talendMeter_DB.project != null) { //              
                    							
       
				strBuffer_talendMeter_CONSOLE.append(
				                String.valueOf(row_talendMeter_DB.project)							
				);


							
	    		} //  			

    			strBuffer_talendMeter_CONSOLE.append("|");
    			


   				
	    		if(row_talendMeter_DB.job != null) { //              
                    							
       
				strBuffer_talendMeter_CONSOLE.append(
				                String.valueOf(row_talendMeter_DB.job)							
				);


							
	    		} //  			

    			strBuffer_talendMeter_CONSOLE.append("|");
    			


   				
	    		if(row_talendMeter_DB.job_repository_id != null) { //              
                    							
       
				strBuffer_talendMeter_CONSOLE.append(
				                String.valueOf(row_talendMeter_DB.job_repository_id)							
				);


							
	    		} //  			

    			strBuffer_talendMeter_CONSOLE.append("|");
    			


   				
	    		if(row_talendMeter_DB.job_version != null) { //              
                    							
       
				strBuffer_talendMeter_CONSOLE.append(
				                String.valueOf(row_talendMeter_DB.job_version)							
				);


							
	    		} //  			

    			strBuffer_talendMeter_CONSOLE.append("|");
    			


   				
	    		if(row_talendMeter_DB.context != null) { //              
                    							
       
				strBuffer_talendMeter_CONSOLE.append(
				                String.valueOf(row_talendMeter_DB.context)							
				);


							
	    		} //  			

    			strBuffer_talendMeter_CONSOLE.append("|");
    			


   				
	    		if(row_talendMeter_DB.origin != null) { //              
                    							
       
				strBuffer_talendMeter_CONSOLE.append(
				                String.valueOf(row_talendMeter_DB.origin)							
				);


							
	    		} //  			

    			strBuffer_talendMeter_CONSOLE.append("|");
    			


   				
	    		if(row_talendMeter_DB.label != null) { //              
                    							
       
				strBuffer_talendMeter_CONSOLE.append(
				                String.valueOf(row_talendMeter_DB.label)							
				);


							
	    		} //  			

    			strBuffer_talendMeter_CONSOLE.append("|");
    			


   				
	    		if(row_talendMeter_DB.count != null) { //              
                    							
       
				strBuffer_talendMeter_CONSOLE.append(
				                String.valueOf(row_talendMeter_DB.count)							
				);


							
	    		} //  			

    			strBuffer_talendMeter_CONSOLE.append("|");
    			


   				
	    		if(row_talendMeter_DB.reference != null) { //              
                    							
       
				strBuffer_talendMeter_CONSOLE.append(
				                String.valueOf(row_talendMeter_DB.reference)							
				);


							
	    		} //  			

    			strBuffer_talendMeter_CONSOLE.append("|");
    			


   				
	    		if(row_talendMeter_DB.thresholds != null) { //              
                    							
       
				strBuffer_talendMeter_CONSOLE.append(
				                String.valueOf(row_talendMeter_DB.thresholds)							
				);


							
	    		} //  			
 

                    if (globalMap.get("tLogRow_CONSOLE")!=null)
                    {
                    	consoleOut_talendMeter_CONSOLE = (java.io.PrintStream) globalMap.get("tLogRow_CONSOLE");
                    }
                    else
                    {
                    	consoleOut_talendMeter_CONSOLE = new java.io.PrintStream(new java.io.BufferedOutputStream(System.out));
                    	globalMap.put("tLogRow_CONSOLE",consoleOut_talendMeter_CONSOLE);
                    }
                    consoleOut_talendMeter_CONSOLE.println(strBuffer_talendMeter_CONSOLE.toString());
                    consoleOut_talendMeter_CONSOLE.flush();
                    nb_line_talendMeter_CONSOLE++;
//////

//////                    
                    
///////////////////////    			

 


	tos_count_talendMeter_CONSOLE++;

/**
 * [talendMeter_CONSOLE main ] stop
 */
	
	/**
	 * [talendMeter_CONSOLE process_data_begin ] start
	 */

	

	
	
		currentVirtualComponent = "talendMeter_CONSOLE";
	
	currentComponent="talendMeter_CONSOLE";

	

 



/**
 * [talendMeter_CONSOLE process_data_begin ] stop
 */
	
	/**
	 * [talendMeter_CONSOLE process_data_end ] start
	 */

	

	
	
		currentVirtualComponent = "talendMeter_CONSOLE";
	
	currentComponent="talendMeter_CONSOLE";

	

 



/**
 * [talendMeter_CONSOLE process_data_end ] stop
 */

} // End of branch "row_talendMeter_DB"




	
	/**
	 * [talendMeter_DB process_data_end ] start
	 */

	

	
	
		currentVirtualComponent = "talendMeter_DB";
	
	currentComponent="talendMeter_DB";

	

 



/**
 * [talendMeter_DB process_data_end ] stop
 */



	
	/**
	 * [talendMeter_METTER process_data_end ] start
	 */

	

	
	
		currentVirtualComponent = "talendMeter_METTER";
	
	currentComponent="talendMeter_METTER";

	

 



/**
 * [talendMeter_METTER process_data_end ] stop
 */
	
	/**
	 * [talendMeter_METTER end ] start
	 */

	

	
	
		currentVirtualComponent = "talendMeter_METTER";
	
	currentComponent="talendMeter_METTER";

	

	}


 

ok_Hash.put("talendMeter_METTER", true);
end_Hash.put("talendMeter_METTER", System.currentTimeMillis());




/**
 * [talendMeter_METTER end ] stop
 */

	
	/**
	 * [talendMeter_DB end ] start
	 */

	

	
	
		currentVirtualComponent = "talendMeter_DB";
	
	currentComponent="talendMeter_DB";

	



	

        if(pstmt_talendMeter_DB != null) {
			
				SharedDBPreparedStatement.releasePreparedStatement(keyPsmt_talendMeter_DB);
			
        }


	nb_line_deleted_talendMeter_DB=nb_line_deleted_talendMeter_DB+ deletedCount_talendMeter_DB;
	nb_line_update_talendMeter_DB=nb_line_update_talendMeter_DB + updatedCount_talendMeter_DB;
	nb_line_inserted_talendMeter_DB=nb_line_inserted_talendMeter_DB + insertedCount_talendMeter_DB;
	nb_line_rejected_talendMeter_DB=nb_line_rejected_talendMeter_DB + rejectedCount_talendMeter_DB;
	
        globalMap.put("talendMeter_DB_NB_LINE",nb_line_talendMeter_DB);
        globalMap.put("talendMeter_DB_NB_LINE_UPDATED",nb_line_update_talendMeter_DB);
        globalMap.put("talendMeter_DB_NB_LINE_INSERTED",nb_line_inserted_talendMeter_DB);
        globalMap.put("talendMeter_DB_NB_LINE_DELETED",nb_line_deleted_talendMeter_DB);
        globalMap.put("talendMeter_DB_NB_LINE_REJECTED", nb_line_rejected_talendMeter_DB);
    
	

			if(execStat){
				if(resourceMap.get("inIterateVComp") == null || !((Boolean)resourceMap.get("inIterateVComp"))){
			 		runStat.updateStatOnConnection("Main"+iterateId,2, 0); 
			 	}
			}
		
 

ok_Hash.put("talendMeter_DB", true);
end_Hash.put("talendMeter_DB", System.currentTimeMillis());




/**
 * [talendMeter_DB end ] stop
 */

	
	/**
	 * [talendMeter_CONSOLE end ] start
	 */

	

	
	
		currentVirtualComponent = "talendMeter_CONSOLE";
	
	currentComponent="talendMeter_CONSOLE";

	


//////
//////
globalMap.put("talendMeter_CONSOLE_NB_LINE",nb_line_talendMeter_CONSOLE);

///////////////////////    			

			if(execStat){
				if(resourceMap.get("inIterateVComp") == null || !((Boolean)resourceMap.get("inIterateVComp"))){
			 		runStat.updateStatOnConnection("Main"+iterateId,2, 0); 
			 	}
			}
		
 

ok_Hash.put("talendMeter_CONSOLE", true);
end_Hash.put("talendMeter_CONSOLE", System.currentTimeMillis());




/**
 * [talendMeter_CONSOLE end ] stop
 */






				}//end the resume

				
				    			if(resumeEntryMethodName == null || globalResumeTicket){
				    				resumeUtil.addLog("CHECKPOINT", "CONNECTION:SUBJOB_OK:talendMeter_METTER:sub_ok_talendMeter_connectionStatsLogs_Commit", "", Thread.currentThread().getId() + "", "", "", "", "", "");
								}	    				    			
					    	
								if(execStat){    	
									runStat.updateStatOnConnection("sub_ok_talendMeter_connectionStatsLogs_Commit", 0, "ok");
								} 
							
							connectionStatsLogs_CommitProcess(globalMap); 
						



	
			}catch(java.lang.Exception e){	
				
				TalendException te = new TalendException(e, currentComponent, globalMap);
				
					te.setVirtualComponentName(currentVirtualComponent);
				
				throw te;
			}catch(java.lang.Error error){	
				
					runStat.stopThreadStat();
				
				throw error;
			}finally{
				
				try{
					
	
	/**
	 * [talendMeter_METTER finally ] start
	 */

	

	
	
		currentVirtualComponent = "talendMeter_METTER";
	
	currentComponent="talendMeter_METTER";

	

 



/**
 * [talendMeter_METTER finally ] stop
 */

	
	/**
	 * [talendMeter_DB finally ] start
	 */

	

	
	
		currentVirtualComponent = "talendMeter_DB";
	
	currentComponent="talendMeter_DB";

	



	

 



/**
 * [talendMeter_DB finally ] stop
 */

	
	/**
	 * [talendMeter_CONSOLE finally ] start
	 */

	

	
	
		currentVirtualComponent = "talendMeter_CONSOLE";
	
	currentComponent="talendMeter_CONSOLE";

	

 



/**
 * [talendMeter_CONSOLE finally ] stop
 */






				}catch(java.lang.Exception e){	
					//ignore
				}catch(java.lang.Error error){
					//ignore
				}
				resourceMap = null;
			}
		

		globalMap.put("talendMeter_METTER_SUBPROCESS_STATE", 1);
	}
	
    public String resuming_logs_dir_path = null;
    public String resuming_checkpoint_path = null;
    public String parent_part_launcher = null;
    private String resumeEntryMethodName = null;
    private boolean globalResumeTicket = false;

    public boolean watch = false;
    // portStats is null, it means don't execute the statistics
    public Integer portStats = null;
    public int portTraces = 4334;
    public String clientHost;
    public String defaultClientHost = "localhost";
    public String contextStr = "PR";
    public boolean isDefaultContext = true;
    public String pid = "0";
    public String rootPid = null;
    public String fatherPid = null;
    public String fatherNode = null;
    public long startTime = 0;
    public boolean isChildJob = false;
    public String log4jLevel = "";

    private boolean execStat = true;

    private ThreadLocal<java.util.Map<String, String>> threadLocal = new ThreadLocal<java.util.Map<String, String>>() {
        protected java.util.Map<String, String> initialValue() {
            java.util.Map<String,String> threadRunResultMap = new java.util.HashMap<String, String>();
            threadRunResultMap.put("errorCode", null);
            threadRunResultMap.put("status", "");
            return threadRunResultMap;
        };
    };


    private PropertiesWithType context_param = new PropertiesWithType();
    public java.util.Map<String, Object> parentContextMap = new java.util.HashMap<String, Object>();

    public String status= "";
    

    public static void main(String[] args){
        final payment_order_bulk_2 payment_order_bulk_2Class = new payment_order_bulk_2();

        int exitCode = payment_order_bulk_2Class.runJobInTOS(args);

        System.exit(exitCode);
    }


    public String[][] runJob(String[] args) {

        int exitCode = runJobInTOS(args);
        String[][] bufferValue = new String[][] { { Integer.toString(exitCode) } };

        return bufferValue;
    }

    public boolean hastBufferOutputComponent() {
		boolean hastBufferOutput = false;
    	
        return hastBufferOutput;
    }

    public int runJobInTOS(String[] args) {
	   	// reset status
	   	status = "";

        String lastStr = "";
        for (String arg : args) {
            if (arg.equalsIgnoreCase("--context_param")) {
                lastStr = arg;
            } else if (lastStr.equals("")) {
                evalParam(arg);
            } else {
                evalParam(lastStr + " " + arg);
                lastStr = "";
            }
        }


        if(clientHost == null) {
            clientHost = defaultClientHost;
        }

        if(pid == null || "0".equals(pid)) {
            pid = TalendString.getAsciiRandomString(6);
        }

        if (rootPid==null) {
            rootPid = pid;
        }
        if (fatherPid==null) {
            fatherPid = pid;
        }else{
            isChildJob = true;
        }

        if (portStats != null) {
            // portStats = -1; //for testing
            if (portStats < 0 || portStats > 65535) {
                // issue:10869, the portStats is invalid, so this client socket can't open
                System.err.println("The statistics socket port " + portStats + " is invalid.");
                execStat = false;
            }
        } else {
            execStat = false;
        }

        try {
            //call job/subjob with an existing context, like: --context=production. if without this parameter, there will use the default context instead.
            java.io.InputStream inContext = payment_order_bulk_2.class.getClassLoader().getResourceAsStream("gifmis/payment_order_bulk_2_0_1/contexts/" + contextStr + ".properties");
            if (inContext == null) {
                inContext = payment_order_bulk_2.class.getClassLoader().getResourceAsStream("config/contexts/" + contextStr + ".properties");
            }
            if (inContext != null) {
                //defaultProps is in order to keep the original context value
                defaultProps.load(inContext);
                inContext.close();
                context = new ContextProperties(defaultProps);
            } else if (!isDefaultContext) {
                //print info and job continue to run, for case: context_param is not empty.
                System.err.println("Could not find the context " + contextStr);
            }

            if(!context_param.isEmpty()) {
                context.putAll(context_param);
				//set types for params from parentJobs
				for (Object key: context_param.keySet()){
					String context_key = key.toString();
					String context_type = context_param.getContextType(context_key);
					context.setContextType(context_key, context_type);

				}
            }
				    context.setContextType("filePath", "id_String");
				
                context.filePath=(String) context.getProperty("filePath");
				    context.setContextType("tableName", "id_String");
				
                context.tableName=(String) context.getProperty("tableName");
				    context.setContextType("Oracle_AdditionalParams", "id_String");
				
                context.Oracle_AdditionalParams=(String) context.getProperty("Oracle_AdditionalParams");
				    context.setContextType("Oracle_Login", "id_String");
				
                context.Oracle_Login=(String) context.getProperty("Oracle_Login");
				    context.setContextType("Oracle_Password", "id_Password");
				
            		String pwd_Oracle_Password_value = context.getProperty("Oracle_Password");
            		context.Oracle_Password = null;
            		if(pwd_Oracle_Password_value!=null) {
            			if(context_param.containsKey("Oracle_Password")) {//no need to decrypt if it come from program argument or parent job runtime
            				context.Oracle_Password = pwd_Oracle_Password_value;
            			} else if (!pwd_Oracle_Password_value.isEmpty()) {
            				try {
            					context.Oracle_Password = routines.system.PasswordEncryptUtil.decryptPassword(pwd_Oracle_Password_value);
            					context.put("Oracle_Password",context.Oracle_Password);
            				} catch (java.lang.RuntimeException e) {
            					//do nothing
            				}
            			}
            		}
				    context.setContextType("Oracle_Port", "id_String");
				
                context.Oracle_Port=(String) context.getProperty("Oracle_Port");
				    context.setContextType("Oracle_Schema", "id_String");
				
                context.Oracle_Schema=(String) context.getProperty("Oracle_Schema");
				    context.setContextType("Oracle_Server", "id_String");
				
                context.Oracle_Server=(String) context.getProperty("Oracle_Server");
				    context.setContextType("Oracle_ServiceName", "id_String");
				
                context.Oracle_ServiceName=(String) context.getProperty("Oracle_ServiceName");
				    context.setContextType("Vertica_DWH_AdditionalParams", "id_String");
				
                context.Vertica_DWH_AdditionalParams=(String) context.getProperty("Vertica_DWH_AdditionalParams");
				    context.setContextType("Vertica_DWH_Database", "id_String");
				
                context.Vertica_DWH_Database=(String) context.getProperty("Vertica_DWH_Database");
				    context.setContextType("Vertica_DWH_Login", "id_String");
				
                context.Vertica_DWH_Login=(String) context.getProperty("Vertica_DWH_Login");
				    context.setContextType("Vertica_DWH_Password", "id_Password");
				
            		String pwd_Vertica_DWH_Password_value = context.getProperty("Vertica_DWH_Password");
            		context.Vertica_DWH_Password = null;
            		if(pwd_Vertica_DWH_Password_value!=null) {
            			if(context_param.containsKey("Vertica_DWH_Password")) {//no need to decrypt if it come from program argument or parent job runtime
            				context.Vertica_DWH_Password = pwd_Vertica_DWH_Password_value;
            			} else if (!pwd_Vertica_DWH_Password_value.isEmpty()) {
            				try {
            					context.Vertica_DWH_Password = routines.system.PasswordEncryptUtil.decryptPassword(pwd_Vertica_DWH_Password_value);
            					context.put("Vertica_DWH_Password",context.Vertica_DWH_Password);
            				} catch (java.lang.RuntimeException e) {
            					//do nothing
            				}
            			}
            		}
				    context.setContextType("Vertica_DWH_Port", "id_String");
				
                context.Vertica_DWH_Port=(String) context.getProperty("Vertica_DWH_Port");
				    context.setContextType("Vertica_DWH_Schema", "id_String");
				
                context.Vertica_DWH_Schema=(String) context.getProperty("Vertica_DWH_Schema");
				    context.setContextType("Vertica_DWH_Server", "id_String");
				
                context.Vertica_DWH_Server=(String) context.getProperty("Vertica_DWH_Server");
				    context.setContextType("Vertica_ODS_AdditionalParams", "id_String");
				
                context.Vertica_ODS_AdditionalParams=(String) context.getProperty("Vertica_ODS_AdditionalParams");
				    context.setContextType("Vertica_ODS_Database", "id_String");
				
                context.Vertica_ODS_Database=(String) context.getProperty("Vertica_ODS_Database");
				    context.setContextType("Vertica_ODS_Login", "id_String");
				
                context.Vertica_ODS_Login=(String) context.getProperty("Vertica_ODS_Login");
				    context.setContextType("Vertica_ODS_Password", "id_Password");
				
            		String pwd_Vertica_ODS_Password_value = context.getProperty("Vertica_ODS_Password");
            		context.Vertica_ODS_Password = null;
            		if(pwd_Vertica_ODS_Password_value!=null) {
            			if(context_param.containsKey("Vertica_ODS_Password")) {//no need to decrypt if it come from program argument or parent job runtime
            				context.Vertica_ODS_Password = pwd_Vertica_ODS_Password_value;
            			} else if (!pwd_Vertica_ODS_Password_value.isEmpty()) {
            				try {
            					context.Vertica_ODS_Password = routines.system.PasswordEncryptUtil.decryptPassword(pwd_Vertica_ODS_Password_value);
            					context.put("Vertica_ODS_Password",context.Vertica_ODS_Password);
            				} catch (java.lang.RuntimeException e) {
            					//do nothing
            				}
            			}
            		}
				    context.setContextType("Vertica_ODS_Port", "id_String");
				
                context.Vertica_ODS_Port=(String) context.getProperty("Vertica_ODS_Port");
				    context.setContextType("Vertica_ODS_Schema", "id_String");
				
                context.Vertica_ODS_Schema=(String) context.getProperty("Vertica_ODS_Schema");
				    context.setContextType("Vertica_ODS_Server", "id_String");
				
                context.Vertica_ODS_Server=(String) context.getProperty("Vertica_ODS_Server");
        } catch (java.io.IOException ie) {
            System.err.println("Could not load context "+contextStr);
            ie.printStackTrace();
        }


        // get context value from parent directly
        if (parentContextMap != null && !parentContextMap.isEmpty()) {if (parentContextMap.containsKey("filePath")) {
                context.filePath = (String) parentContextMap.get("filePath");
            }if (parentContextMap.containsKey("tableName")) {
                context.tableName = (String) parentContextMap.get("tableName");
            }if (parentContextMap.containsKey("Oracle_AdditionalParams")) {
                context.Oracle_AdditionalParams = (String) parentContextMap.get("Oracle_AdditionalParams");
            }if (parentContextMap.containsKey("Oracle_Login")) {
                context.Oracle_Login = (String) parentContextMap.get("Oracle_Login");
            }if (parentContextMap.containsKey("Oracle_Password")) {
                context.Oracle_Password = (java.lang.String) parentContextMap.get("Oracle_Password");
            }if (parentContextMap.containsKey("Oracle_Port")) {
                context.Oracle_Port = (String) parentContextMap.get("Oracle_Port");
            }if (parentContextMap.containsKey("Oracle_Schema")) {
                context.Oracle_Schema = (String) parentContextMap.get("Oracle_Schema");
            }if (parentContextMap.containsKey("Oracle_Server")) {
                context.Oracle_Server = (String) parentContextMap.get("Oracle_Server");
            }if (parentContextMap.containsKey("Oracle_ServiceName")) {
                context.Oracle_ServiceName = (String) parentContextMap.get("Oracle_ServiceName");
            }if (parentContextMap.containsKey("Vertica_DWH_AdditionalParams")) {
                context.Vertica_DWH_AdditionalParams = (String) parentContextMap.get("Vertica_DWH_AdditionalParams");
            }if (parentContextMap.containsKey("Vertica_DWH_Database")) {
                context.Vertica_DWH_Database = (String) parentContextMap.get("Vertica_DWH_Database");
            }if (parentContextMap.containsKey("Vertica_DWH_Login")) {
                context.Vertica_DWH_Login = (String) parentContextMap.get("Vertica_DWH_Login");
            }if (parentContextMap.containsKey("Vertica_DWH_Password")) {
                context.Vertica_DWH_Password = (java.lang.String) parentContextMap.get("Vertica_DWH_Password");
            }if (parentContextMap.containsKey("Vertica_DWH_Port")) {
                context.Vertica_DWH_Port = (String) parentContextMap.get("Vertica_DWH_Port");
            }if (parentContextMap.containsKey("Vertica_DWH_Schema")) {
                context.Vertica_DWH_Schema = (String) parentContextMap.get("Vertica_DWH_Schema");
            }if (parentContextMap.containsKey("Vertica_DWH_Server")) {
                context.Vertica_DWH_Server = (String) parentContextMap.get("Vertica_DWH_Server");
            }if (parentContextMap.containsKey("Vertica_ODS_AdditionalParams")) {
                context.Vertica_ODS_AdditionalParams = (String) parentContextMap.get("Vertica_ODS_AdditionalParams");
            }if (parentContextMap.containsKey("Vertica_ODS_Database")) {
                context.Vertica_ODS_Database = (String) parentContextMap.get("Vertica_ODS_Database");
            }if (parentContextMap.containsKey("Vertica_ODS_Login")) {
                context.Vertica_ODS_Login = (String) parentContextMap.get("Vertica_ODS_Login");
            }if (parentContextMap.containsKey("Vertica_ODS_Password")) {
                context.Vertica_ODS_Password = (java.lang.String) parentContextMap.get("Vertica_ODS_Password");
            }if (parentContextMap.containsKey("Vertica_ODS_Port")) {
                context.Vertica_ODS_Port = (String) parentContextMap.get("Vertica_ODS_Port");
            }if (parentContextMap.containsKey("Vertica_ODS_Schema")) {
                context.Vertica_ODS_Schema = (String) parentContextMap.get("Vertica_ODS_Schema");
            }if (parentContextMap.containsKey("Vertica_ODS_Server")) {
                context.Vertica_ODS_Server = (String) parentContextMap.get("Vertica_ODS_Server");
            }
        }

        //Resume: init the resumeUtil
        resumeEntryMethodName = ResumeUtil.getResumeEntryMethodName(resuming_checkpoint_path);
        resumeUtil = new ResumeUtil(resuming_logs_dir_path, isChildJob, rootPid);
        resumeUtil.initCommonInfo(pid, rootPid, fatherPid, projectName, jobName, contextStr, jobVersion);

		List<String> parametersToEncrypt = new java.util.ArrayList<String>();
			parametersToEncrypt.add("Oracle_Password");
			parametersToEncrypt.add("Vertica_DWH_Password");
			parametersToEncrypt.add("Vertica_ODS_Password");
        //Resume: jobStart
        resumeUtil.addLog("JOB_STARTED", "JOB:" + jobName, parent_part_launcher, Thread.currentThread().getId() + "", "","","","",resumeUtil.convertToJsonText(context,parametersToEncrypt));

if(execStat) {
    try {
        runStat.openSocket(!isChildJob);
        runStat.setAllPID(rootPid, fatherPid, pid, jobName);
        runStat.startThreadStat(clientHost, portStats);
        runStat.updateStatOnJob(RunStat.JOBSTART, fatherNode);
    } catch (java.io.IOException ioException) {
        ioException.printStackTrace();
    }
}



	
	    java.util.concurrent.ConcurrentHashMap<Object, Object> concurrentHashMap = new java.util.concurrent.ConcurrentHashMap<Object, Object>();
	    globalMap.put("concurrentHashMap", concurrentHashMap);
	

    long startUsedMemory = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
    long endUsedMemory = 0;
    long end = 0;

    startTime = System.currentTimeMillis();
        talendStats_STATS.addMessage("begin");




this.globalResumeTicket = true;//to run tPreJob

try {
errorCode = null;preStaLogConProcess(globalMap);
if(!"failure".equals(status)) { status = "end"; }
}catch (TalendException e_preStaLogCon) {
globalMap.put("preStaLogCon_SUBPROCESS_STATE", -1);

e_preStaLogCon.printStackTrace();

}
try {
errorCode = null;tPrejob_1Process(globalMap);
if(!"failure".equals(status)) { status = "end"; }
}catch (TalendException e_tPrejob_1) {
globalMap.put("tPrejob_1_SUBPROCESS_STATE", -1);

e_tPrejob_1.printStackTrace();

}


        try {
            talendStats_STATSProcess(globalMap);
        } catch (java.lang.Exception e) {
            e.printStackTrace();
        }

this.globalResumeTicket = false;//to run others jobs


this.globalResumeTicket = true;//to run tPostJob




        end = System.currentTimeMillis();

        if (watch) {
            System.out.println((end-startTime)+" milliseconds");
        }

        endUsedMemory = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
        if (false) {
            System.out.println((endUsedMemory - startUsedMemory) + " bytes memory increase when running : payment_order_bulk_2");
        }
        talendStats_STATS.addMessage(status==""?"end":status, (end-startTime));
        try {
            talendStats_STATSProcess(globalMap);
        } catch (java.lang.Exception e) {
            e.printStackTrace();
        }



if (execStat) {
    runStat.updateStatOnJob(RunStat.JOBEND, fatherNode);
    runStat.stopThreadStat();
}
    int returnCode = 0;
    if(errorCode == null) {
         returnCode = status != null && status.equals("failure") ? 1 : 0;
    } else {
         returnCode = errorCode.intValue();
    }
    resumeUtil.addLog("JOB_ENDED", "JOB:" + jobName, parent_part_launcher, Thread.currentThread().getId() + "", "","" + returnCode,"","","");

    return returnCode;

  }

    // only for OSGi env
    public void destroy() {
    closeSqlDbConnections();


    }



    private void closeSqlDbConnections() {
        try {
            Object obj_conn;
            obj_conn = globalMap.remove("conn_connectionStatsLogs");
            if (null != obj_conn) {
                ((java.sql.Connection) obj_conn).close();
            }
        } catch (java.lang.Exception e) {
        }
    }











    private java.util.Map<String, Object> getSharedConnections4REST() {
        java.util.Map<String, Object> connections = new java.util.HashMap<String, Object>();
            connections.put("conn_connectionStatsLogs", globalMap.get("conn_connectionStatsLogs"));







        return connections;
    }

    private void evalParam(String arg) {
        if (arg.startsWith("--resuming_logs_dir_path")) {
            resuming_logs_dir_path = arg.substring(25);
        } else if (arg.startsWith("--resuming_checkpoint_path")) {
            resuming_checkpoint_path = arg.substring(27);
        } else if (arg.startsWith("--parent_part_launcher")) {
            parent_part_launcher = arg.substring(23);
        } else if (arg.startsWith("--watch")) {
            watch = true;
        } else if (arg.startsWith("--stat_port=")) {
            String portStatsStr = arg.substring(12);
            if (portStatsStr != null && !portStatsStr.equals("null")) {
                portStats = Integer.parseInt(portStatsStr);
            }
        } else if (arg.startsWith("--trace_port=")) {
            portTraces = Integer.parseInt(arg.substring(13));
        } else if (arg.startsWith("--client_host=")) {
            clientHost = arg.substring(14);
        } else if (arg.startsWith("--context=")) {
            contextStr = arg.substring(10);
            isDefaultContext = false;
        } else if (arg.startsWith("--father_pid=")) {
            fatherPid = arg.substring(13);
        } else if (arg.startsWith("--root_pid=")) {
            rootPid = arg.substring(11);
        } else if (arg.startsWith("--father_node=")) {
            fatherNode = arg.substring(14);
        } else if (arg.startsWith("--pid=")) {
            pid = arg.substring(6);
        } else if (arg.startsWith("--context_type")) {
            String keyValue = arg.substring(15);
			int index = -1;
            if (keyValue != null && (index = keyValue.indexOf('=')) > -1) {
                if (fatherPid==null) {
                    context_param.setContextType(keyValue.substring(0, index), replaceEscapeChars(keyValue.substring(index + 1)));
                } else { // the subjob won't escape the especial chars
                    context_param.setContextType(keyValue.substring(0, index), keyValue.substring(index + 1) );
                }

            }

		} else if (arg.startsWith("--context_param")) {
            String keyValue = arg.substring(16);
            int index = -1;
            if (keyValue != null && (index = keyValue.indexOf('=')) > -1) {
                if (fatherPid==null) {
                    context_param.put(keyValue.substring(0, index), replaceEscapeChars(keyValue.substring(index + 1)));
                } else { // the subjob won't escape the especial chars
                    context_param.put(keyValue.substring(0, index), keyValue.substring(index + 1) );
                }
            }
        }else if (arg.startsWith("--log4jLevel=")) {
            log4jLevel = arg.substring(13);
		}

    }
    
    private static final String NULL_VALUE_EXPRESSION_IN_COMMAND_STRING_FOR_CHILD_JOB_ONLY = "<TALEND_NULL>";

    private final String[][] escapeChars = {
        {"\\\\","\\"},{"\\n","\n"},{"\\'","\'"},{"\\r","\r"},
        {"\\f","\f"},{"\\b","\b"},{"\\t","\t"}
        };
    private String replaceEscapeChars (String keyValue) {

		if (keyValue == null || ("").equals(keyValue.trim())) {
			return keyValue;
		}

		StringBuilder result = new StringBuilder();
		int currIndex = 0;
		while (currIndex < keyValue.length()) {
			int index = -1;
			// judege if the left string includes escape chars
			for (String[] strArray : escapeChars) {
				index = keyValue.indexOf(strArray[0],currIndex);
				if (index>=0) {

					result.append(keyValue.substring(currIndex, index + strArray[0].length()).replace(strArray[0], strArray[1]));
					currIndex = index + strArray[0].length();
					break;
				}
			}
			// if the left string doesn't include escape chars, append the left into the result
			if (index < 0) {
				result.append(keyValue.substring(currIndex));
				currIndex = currIndex + keyValue.length();
			}
		}

		return result.toString();
    }

    public Integer getErrorCode() {
        return errorCode;
    }


    public String getStatus() {
        return status;
    }

    ResumeUtil resumeUtil = null;
}
/************************************************************************************************
 *     439747 characters generated by Talend Data Integration 
 *     on the December 4, 2018 2:48:26 PM EET
 ************************************************************************************************/